package net.spirangle.awakening.players;

import com.wurmonline.server.players.Achievement;
import com.wurmonline.server.players.AchievementTemplate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Achievements {

    private static final Logger logger = Logger.getLogger(Achievements.class.getName());

    public static final int ACH_INFESTATION1 = 1001;
    public static final int ACH_INFESTATION2 = 1002;
    public static final int ACH_INFESTATION3 = 1003;
    public static final int ACH_INFESTATION4 = 1004;
    public static final int ACH_INFESTATION5 = 1005;
    public static final int ACH_INFESTATIONS = 1006;

    private static Achievements instance = null;

    public static Achievements getInstance() {
        if(instance == null) instance = new Achievements();
        return instance;
    }

    private Achievements() {

    }

    public void init() {
        try {
            Method addTemplateMethod = Achievement.class.getDeclaredMethod("addTemplate", AchievementTemplate.class);
            addTemplateMethod.setAccessible(true);

            ConcurrentHashMap<Integer, AchievementTemplate> generatedTemplates = new ConcurrentHashMap<>();
            AchievementTemplate template;
            template = createAchievement(ACH_INFESTATION1, "Banish Weak Infestation", false, 1, (byte) 3,
                                         "You banished at least one weak curse infestation.",
                                         false, generatedTemplates, true, false,
                                         "Banish a weak curse infestation.");
            template.setAchievementsTriggered(new int[]{ ACH_INFESTATIONS });
            template = createAchievement(ACH_INFESTATION2, "Banish Strong Infestation", false, 1, (byte) 3,
                                         "You banished at least one strong curse infestation.",
                                         false, generatedTemplates, true, false,
                                         "Banish a strong curse infestation.");
            template.setAchievementsTriggered(new int[]{ ACH_INFESTATIONS });
            template = createAchievement(ACH_INFESTATION3, "Banish Terrifying Infestation", false, 1, (byte) 3,
                                         "You banished at least one terrifying curse infestation.",
                                         false, generatedTemplates, true, false,
                                         "Banish a terrifying curse infestation.");
            template.setAchievementsTriggered(new int[]{ ACH_INFESTATIONS });
            template = createAchievement(ACH_INFESTATION4, "Banish Demonic Infestation", false, 1, (byte) 3,
                                         "You banished at least one demonic curse infestation.",
                                         false, generatedTemplates, true, false,
                                         "Banish a demonic curse infestation.");
            template.setAchievementsTriggered(new int[]{ ACH_INFESTATIONS });
            template = createAchievement(ACH_INFESTATION5, "Banish Infernal Infestation", false, 1, (byte) 3,
                                         "You banished at least one infernal curse infestation.",
                                         false, generatedTemplates, true, false,
                                         "Banish a infernal curse infestation.");
            template.setAchievementsTriggered(new int[]{ ACH_INFESTATIONS });
            template = createAchievement(ACH_INFESTATIONS, "Banish Each Infestation Level", false, 1, (byte) 3,
                                         "You banished at least one of each power level of curse infestations.",
                                         false, generatedTemplates, true, false,
                                         "Banish at least one of these infestation levels: Weak, Strong, Terrifying, Demonic and Infernal.");
            template.setRequiredAchievements(new int[]{ ACH_INFESTATION1, ACH_INFESTATION2, ACH_INFESTATION3, ACH_INFESTATION4, ACH_INFESTATION5 });

            for(AchievementTemplate t : generatedTemplates.values()) {
                addTemplateMethod.invoke(null, t);
            }
            for(AchievementTemplate t : generatedTemplates.values()) {
                for(int i : t.getAchievementsTriggered()) {
                    Achievement.getTemplate(i).addTriggeredByAchievement(t.getNumber());
                }
            }
        } catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private AchievementTemplate createAchievement(int number, String name, boolean isInvis, int triggerOn, byte type,
                                                  String description, boolean isForCooking, ConcurrentHashMap<Integer, AchievementTemplate> templates,
                                                  boolean playSoundOnUpdate, boolean isOneTimer, String requirement) {
        final AchievementTemplate template = new AchievementTemplate(number, name, isInvis, triggerOn, type, playSoundOnUpdate, isOneTimer, requirement);
        template.setDescription(description);
        template.setIsForCooking(isForCooking);
        templates.put(number, template);
        return template;
    }
}
