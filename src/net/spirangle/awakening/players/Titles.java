package net.spirangle.awakening.players;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.NoSuchPlayerException;
import com.wurmonline.server.Players;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.Titles.Title;
import com.wurmonline.server.villages.Citizen;
import com.wurmonline.server.villages.Village;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;
import net.spirangle.awakening.misc.ExtendTitleEnum;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Titles {

    private static final Logger logger = Logger.getLogger(Titles.class.getName());

    public static final int LORD = 1100;
    public static final int BARONET = 1101;
    public static final int BARON = 1102;
    public static final int VISCOUNT = 1103;
    public static final int COUNT = 1104;
    public static final int MARQUESS = 1105;
    public static final int DUKE = 1106;
    public static final int GRAND_DUKE = 1107;
    public static final int ARCHDUKE = 1108;

    public static final int CORPORAL = 1200;
    public static final int SERGEANT = 1201;
    public static final int LIEUTENANT = 1202;
    public static final int CAPTAIN = 1203;
    public static final int MAJOR = 1204;
    public static final int COLONEL = 1205;
    public static final int BRIGADIER = 1206;
    public static final int GENERAL = 1207;

    public static final int LORD_SIZE = 31 * 31;
    public static final int BARONET_SIZE = 41 * 41;
    public static final int BARON_SIZE = 51 * 51;
    public static final int VISCOUNT_SIZE = 61 * 61;
    public static final int COUNT_SIZE = 71 * 71;
    public static final int MARQUESS_SIZE = 81 * 81;
    public static final int DUKE_SIZE = 91 * 91;
    public static final int GRAND_DUKE_SIZE = 121 * 121;
    public static final int ARCHDUKE_SIZE = 151 * 151;

    public static final int CORPORAL_LEVEL = 100;
    public static final int SERGEANT_LEVEL = 500;
    public static final int LIEUTENANT_LEVEL = 2500;
    public static final int CAPTAIN_LEVEL = 10000;
    public static final int MAJOR_LEVEL = 50000;
    public static final int COLONEL_LEVEL = 250000;
    public static final int BRIGADIER_LEVEL = 1000000;
    public static final int GENERAL_LEVEL = 5000000;

    public static void addNewTitles() {
        try {
            ExtendTitleEnum.builder("com.wurmonline.server.players.Titles$Title");
            ExtendTitleEnum titles = ExtendTitleEnum.getInstance();

            titles.addExtendEntry("Nobility_Lord", LORD, "Lord", "Lady", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Baronet", BARONET, "Baronet", "Baronetess", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Baron", BARON, "Baron", "Baroness", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Viscount", VISCOUNT, "Viscount", "Viscountess", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Count", COUNT, "Count", "Countess", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Marquess", MARQUESS, "Marquess", "Marchioness", -1, "NORMAL");
            titles.addExtendEntry("Nobility_Duke", DUKE, "Duke", "Duchess", -1, "NORMAL");
            titles.addExtendEntry("Nobility_GrandDuke", GRAND_DUKE, "Grand Duke", "Grand Duchess", -1, "NORMAL");
            titles.addExtendEntry("Nobility_ArchDuke", ARCHDUKE, "Archduke", "Archduchess", -1, "NORMAL");

            titles.addExtendEntry("Military_Corporal", CORPORAL, "Corporal", "Corporal", -1, "NORMAL");
            titles.addExtendEntry("Military_Sergeant", SERGEANT, "Sergeant", "Sergeant", -1, "NORMAL");
            titles.addExtendEntry("Military_Lieutenant", LIEUTENANT, "Lieutenant", "Lieutenant", -1, "NORMAL");
            titles.addExtendEntry("Military_Captain", CAPTAIN, "Captain", "Captain", -1, "NORMAL");
            titles.addExtendEntry("Military_Major", MAJOR, "Major", "Major", -1, "NORMAL");
            titles.addExtendEntry("Military_Colonel", COLONEL, "Colonel", "Colonel", -1, "NORMAL");
            titles.addExtendEntry("Military_Brigadier", BRIGADIER, "Brigadier", "Brigadier", -1, "NORMAL");
            titles.addExtendEntry("Military_General", GENERAL, "General", "General", -1, "NORMAL");

            titles.ExtendEnumEntries();
        } catch(BadBytecode | ClassNotFoundException | NotFoundException | CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused")
    public static void updateNobilityRank(Village village) {
        if(village == null) return;
        int size = (village.getEndX() - village.getStartX() + 1) * (village.getEndY() - village.getStartY() + 1);
        if(size < LORD_SIZE) return;
        logger.info("Nobility: village size=" + size);
        Citizen mayor = village.getMayor();
        if(mayor == null) return;
        Player player;
        try {
            player = Players.getInstance().getPlayer(mayor.getId());
        } catch(NoSuchPlayerException e) {
            return;
        }
        if(player == null) return;
        logger.info("Nobility: mayor=" + player.getName());
        if(size >= LORD_SIZE) {
            addTitle(player, LORD);
            if(size >= BARONET_SIZE) {
                addTitle(player, BARONET);
                if(size >= BARON_SIZE) {
                    addTitle(player, BARON);
                    if(size >= VISCOUNT_SIZE) {
                        addTitle(player, VISCOUNT);
                        if(size >= COUNT_SIZE) {
                            addTitle(player, COUNT);
                            if(size >= MARQUESS_SIZE) {
                                addTitle(player, MARQUESS);
                                if(size >= DUKE_SIZE) {
                                    addTitle(player, DUKE);
                                    if(size >= GRAND_DUKE_SIZE) {
                                        addTitle(player, GRAND_DUKE);
                                        if(size >= ARCHDUKE_SIZE) {
                                            addTitle(player, ARCHDUKE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void updateMilitaryRank(Player player, int from, int to) {
        if(to > from) {
            if(from < CORPORAL_LEVEL && to >= CORPORAL_LEVEL) {
                addTitle(player, CORPORAL);
            }
            if(from < SERGEANT_LEVEL && to >= SERGEANT_LEVEL) {
                addTitle(player, SERGEANT);
            }
            if(from < LIEUTENANT_LEVEL && to >= LIEUTENANT_LEVEL) {
                addTitle(player, LIEUTENANT);
            }
            if(from < CAPTAIN_LEVEL && to >= CAPTAIN_LEVEL) {
                addTitle(player, CAPTAIN);
            }
            if(from < MAJOR_LEVEL && to >= MAJOR_LEVEL) {
                addTitle(player, MAJOR);
            }
            if(from < COLONEL_LEVEL && to >= COLONEL_LEVEL) {
                addTitle(player, COLONEL);
            }
            if(from < BRIGADIER_LEVEL && to >= BRIGADIER_LEVEL) {
                addTitle(player, BRIGADIER);
            }
            if(from < GENERAL_LEVEL && to >= GENERAL_LEVEL) {
                addTitle(player, GENERAL);
            }
        } else if(to < from) {
            if(to < CORPORAL_LEVEL && from >= CORPORAL_LEVEL) {
                removeTitle(player, CORPORAL);
            }
            if(to < SERGEANT_LEVEL && from >= SERGEANT_LEVEL) {
                removeTitle(player, SERGEANT);
            }
            if(to < LIEUTENANT_LEVEL && from >= LIEUTENANT_LEVEL) {
                removeTitle(player, LIEUTENANT);
            }
            if(to < CAPTAIN_LEVEL && from >= CAPTAIN_LEVEL) {
                removeTitle(player, CAPTAIN);
            }
            if(to < MAJOR_LEVEL && from >= MAJOR_LEVEL) {
                removeTitle(player, MAJOR);
            }
            if(to < COLONEL_LEVEL && from >= COLONEL_LEVEL) {
                removeTitle(player, COLONEL);
            }
            if(to < BRIGADIER_LEVEL && from >= BRIGADIER_LEVEL) {
                removeTitle(player, BRIGADIER);
            }
            if(to < GENERAL_LEVEL && from >= GENERAL_LEVEL) {
                removeTitle(player, GENERAL);
            }
        }
    }

    private static void addTitle(Player player, int id) {
        final com.wurmonline.server.players.Titles.Title title = com.wurmonline.server.players.Titles.Title.getTitle(id);
        player.addTitle(title);
    }

    private static void removeTitle(Player player, int id) {
        final com.wurmonline.server.players.Titles.Title title = com.wurmonline.server.players.Titles.Title.getTitle(id);
        player.removeTitle(title);
    }

    @SuppressWarnings("unused")
    public static void addTitle(Player player, Title title) {
        if((title.getSkillId() != -1 || isNobilityRank(title)) && player.getPower() < MiscConstants.POWER_HERO) {
            LeaderBoard lb = PlayersData.getInstance().getLeaderBoard(player);
            if(title.getSkillId() == -1 || lb == null || !lb.isHidden(title.getSkillId())) {
                ChatChannels.getInstance().sendTitle(player, title);
            }
        }
    }

    public static boolean isNobilityRank(Title title) {
        return title.id >= LORD && title.id <= ARCHDUKE;
    }

    @SuppressWarnings("unused")
    public static boolean isMilitaryRank(Title title) {
        return title.id >= CORPORAL && title.id <= GENERAL;
    }
}
