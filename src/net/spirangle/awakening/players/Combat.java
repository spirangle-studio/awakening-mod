package net.spirangle.awakening.players;

import com.wurmonline.server.Message;
import com.wurmonline.server.NoSuchPlayerException;
import com.wurmonline.server.Players;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.NoSuchSkillException;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.skills.Skills;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.zones.Infestations;
import net.spirangle.awakening.zones.Treasures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class Combat {

    private static final Logger logger = Logger.getLogger(Combat.class.getName());

    private static float getCreatureKarma(CreatureTemplate template) {
        float combatRating = template.getBaseCombatRating() + template.getBonusCombatRating();
        float maxDmg = Math.max(template.getBreathDamage(), template.getHandDamage());
        maxDmg = Math.max(maxDmg, template.getBiteDamage());
        maxDmg = Math.max(maxDmg, template.getKickDamage());
        maxDmg = Math.max(maxDmg, template.getHeadButtDamage());
        Skill fightingSkill = null, weaponlessFightingSkill = null, soulStrengthSkill = null;
        try {
            Skills skills = template.getSkills();
            try { fightingSkill = skills.getSkill(SkillList.GROUP_FIGHTING); } catch(NoSuchSkillException e) {
                fightingSkill = null;
            }
            try {
                weaponlessFightingSkill = skills.getSkill(SkillList.WEAPONLESS_FIGHTING);
            } catch(NoSuchSkillException e) { weaponlessFightingSkill = null; }
            try { soulStrengthSkill = skills.getSkill(SkillList.SOUL_STRENGTH); } catch(NoSuchSkillException e) {
                soulStrengthSkill = null;
            }
        } catch(Exception e) { }
        float fighting = fightingSkill != null? (float) fightingSkill.getKnowledge() : 1.0f;
        float weaponlessFighting = weaponlessFightingSkill != null? (float) weaponlessFightingSkill.getKnowledge() : 1.0f;
        float soulStr = soulStrengthSkill != null? (float) soulStrengthSkill.getKnowledge() : 1.0f;
        float fs = Math.max(fighting, weaponlessFighting);
        float str = (combatRating + maxDmg + soulStr + fs) * 0.0025f;
        float karma = str * str * 200.0f;
        logger.info("combatRating=" + combatRating + ", maxDmg=" + maxDmg + ", fighting=" + fighting + ", weaponlessFighting=" + weaponlessFighting + ", soulStr=" + soulStr + ", str=" + str + ", karma=" + karma);
        return karma;
    }

    public static int hasCorpseItems(Creature performer, Item corpse, CreatureTemplate template, short action) {
        if(action != Actions.BURY || template.isHuman() || template.isUnique()) return -1;
        try {
            Item parent = corpse.getParent();
            if((parent != null && parent.isInventory())) return -1;
        } catch(Exception e) { }
        return corpse.isEmpty(false)? 0 : 1;
    }

    private static Combat instance = null;

    public static Combat getInstance() {
        if(instance == null) instance = new Combat();
        return instance;
    }

    @SuppressWarnings("unused")
    private final Map<Integer, Long> creatureBounties;
    @SuppressWarnings("unused")
    private final Map<Byte, Float> typeModifiers;

    private Combat() {
        creatureBounties = new HashMap<Integer, Long>();
        typeModifiers = new HashMap<Byte, Float>();
    }

    @SuppressWarnings("unused")
    public void die(Creature creature, Map<Long, Long> attackers) {
        if(creature.isPlayer()) {
            CombatData data = PlayersData.getInstance().getCombatData((Player) creature);
            data.addDeath((Player) creature);
        }
        Infestations.getInstance().creatureKilled(creature, attackers);
        Treasures.getInstance().creatureKilled(creature, attackers);
        if(attackers == null || attackers.size() == 0) return;
        long now = System.currentTimeMillis();
        long attackerId, attackTime;
        int num = 0;
        CreatureTemplate template = creature.getTemplate();
        List<Player> killers = new ArrayList<>();
        for(Map.Entry<Long, Long> entry : attackers.entrySet()) {
            attackerId = entry.getKey();
            attackTime = entry.getValue();
            if(now - attackTime >= 600000L) continue;
            ++num;
            try {
                Player player = Players.getInstance().getPlayer(attackerId);
                if(!creature.isDuelOrSpar(player)) {
                    killers.add(player);
                }
            } catch(NoSuchPlayerException e) { }
        }
        if(killers.size() == 0) return;
        for(Player player : killers) {
            if(player.getDeity() != null && player.getDeity().isAllowsButchering() && !template.isUnique())
                gainKarma(player, template, false, num);
            killedEnemy(player, creature, num);
            CombatData data = PlayersData.getInstance().getCombatData(player);
            data.addKill(player, creature);
        }
    }

    @SuppressWarnings("unused")
    public void modifyFightSkill(Creature performer, Creature creature, double pskill, double skillGained) {
        if(!performer.isPlayer()) return;
        Communicator communicator = performer.getCommunicator();
        CombatData data = PlayersData.getInstance().getCombatData((Player) performer);
        //		logger.info("Combat: pskill="+pskill+", skillGained="+skillGained);
        data.addKillSkill((Player) performer, creature, pskill, skillGained);
    }

    public void bury(Creature performer, Item corpse, int corpseItems, short action) {
        if(!performer.isPlayer() || corpseItems > 0) return;
        if(performer.getDeity() != null)
            if(performer.getDeity().isAllowsButchering()) return;
        int templateId = corpse.getData1();
        gainKarma(performer, templateId, true, 1);
    }

    private void gainKarma(Creature performer, int templateId, boolean bury, int killers) {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(templateId);
            gainKarma(performer, template, bury, killers);
        } catch(NoSuchCreatureTemplateException e) {
            logger.log(Level.SEVERE, "Failed to give karma: " + e.getMessage(), e);
        }
    }

    private void gainKarma(Creature performer, CreatureTemplate template, boolean bury, int killers) {
        if(!performer.isPlayer()) return;
        Communicator communicator = performer.getCommunicator();
        float creatureKarma = getCreatureKarma(template);
        int karma = (int) Math.max(1.0f, creatureKarma * (0.8f + Server.rand.nextFloat() * 0.4f));
        performer.setKarma(performer.getKarma() + karma);
        String message;
        if(bury) {
            message = "The spirit of the dead awards you " + karma + " karma for burying the corpse in a respectful manner.";
        } else {
            message = "You drain " + karma + " karma from the spirit of the dead.";
        }
        communicator.sendSafeServerMessage(message);

        CombatData data = PlayersData.getInstance().getCombatData((Player) performer);
        data.addBury(template, karma);
    }

    private void killedEnemy(Player player, Creature creature, int killers) {
        if(creature.isPlayer() || creature.isHuman() || creature.isGuard()) {
            byte pk = player.getKingdomId();
            byte ek = creature.getKingdomId();
            if(pk != ek) {
                Kingdom kingdom = Kingdoms.getKingdomOrNull(pk);
                if(kingdom == null || kingdom.isAllied(ek)) return;
                PlayerData pd = PlayersData.getInstance().get(player);
                int salary = 0;
                int warPoints = 0;
                if(creature.isPlayer()) {
                    salary += Config.kingdomEnemyPlayerBounty[pk];
                    Player enemy = (Player) creature;
                    PlayerData epd = PlayersData.getInstance().get(enemy);
                    int pw = pd.getWarPoints(), pr = getRank(pw), ew = epd.getWarPoints(), er = getRank(ew);
                    if(er >= 1) {
                        if(pr >= er) pr = er - 1;
                        warPoints = er * er * (er * er - pr * pr) * 21;
                        epd.addWarPoints(enemy, -(warPoints / 2));
                    } else {
                        if(pr >= 1) warPoints = -(pr * pr * pr * pr * 21);
                        else warPoints = 5;
                    }
                } else {
                    salary += Config.kingdomEnemyBounty[pk];
                    if(creature.isWarGuard()) warPoints = 15;
                    else if(creature.isSpiritGuard()) warPoints = 12;
                    else if(creature.isKingdomGuard()) warPoints = 10;
                    else warPoints = 5;
                    warPoints = (int) Math.ceil((double) warPoints / (double) killers);
                }
                pd.addWarPoints(player, warPoints);
                if(salary > 0) {
                    EconomyData economyData = pd.getEconomyData();
                    if(economyData.paySalary(player, salary)) {
                        String m = new Change(salary).getChangeString();
                        player.getCommunicator().sendSafeServerMessage("Bounty of " + m + " gained for killing enemy.", Message.KINGDOM);
                    }
                }
            }
        }
    }

    private int getRank(int warPoints) {
        if(warPoints < Titles.CORPORAL_LEVEL) return 0;
        else if(warPoints < Titles.SERGEANT_LEVEL) return 1;
        else if(warPoints < Titles.LIEUTENANT_LEVEL) return 2;
        else if(warPoints < Titles.CAPTAIN_LEVEL) return 3;
        else if(warPoints < Titles.MAJOR_LEVEL) return 4;
        else if(warPoints < Titles.COLONEL_LEVEL) return 5;
        else if(warPoints < Titles.BRIGADIER_LEVEL) return 6;
        else if(warPoints < Titles.GENERAL_LEVEL) return 7;
        else return 8;
    }
}
