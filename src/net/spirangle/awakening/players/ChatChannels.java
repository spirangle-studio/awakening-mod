package net.spirangle.awakening.players;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.players.Titles.Title;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.zones.VirtualZone;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.shared.util.MulticolorLineSegment;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.Presence;
import net.spirangle.awakening.AwakeningMod;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.util.CharacterLineSegment;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modloader.interfaces.MessagePolicy;

import javax.security.auth.login.LoginException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChatChannels {

    private static final Logger logger = Logger.getLogger(ChatChannels.class.getName());

    private static final int[] POWER_R = { 100, 255, 255, 20, 187, 128, 255 };
    private static final int[] POWER_G = { 220, 128, 175, 200, 1, 250, 242 };
    private static final int[] POWER_B = { 200, 1, 200, 255, 187, 20, 1 };

    public static final String DISCORD_CHANNELS = "trade|world|help|tirvalon|cellimdar|shodroq|thekandre|staff";
    public static final String PUBLIC_CHANNELS = "Trade|World|Roleplay|Help|MGMT";
    public static final String KINGDOM_CHANNELS = "Tirvalon|Cellimdar|Shodroq|Thekandre";

    public static final String TRADE_DISCORD = "trade";
    public static final String WORLD_DISCORD = "world";
    public static final String TIRVALON_DISCORD = "tirvalon";
    public static final String CELLIMDAR_DISCORD = "cellimdar";
    public static final String SHODROQ_DISCORD = "shodroq";
    public static final String THEKANDRE_DISCORD = "thekandre";
    public static final String NEWS_DISCORD = "news";
    public static final String HISTORY_DISCORD = "history";
    public static final String STAFF_DISCORD = "staff";
    public static final String HELP_DISCORD = "help";
    public static final String BUGS_DISCORD = "bugs";
    public static final String SUGGESTIONS_DISCORD = "suggestions";
    public static final String REPORTS_DISCORD = "reports";
    public static final String LOCAL = ":Local";
    public static final String TRADE = "Trade";
    public static final String WORLD = "World";
    public static final String TIRVALON = "Tirvalon";
    public static final String CELLIMDAR = "Cellimdar";
    public static final String SHODROQ = "Shodroq";
    public static final String THEKANDRE = "Thekandre";
    public static final String ROLEPLAY = "Roleplay";
    public static final String HELP = "Help";
    public static final String HISTORY = "History";
    public static final String STAFF = "MGMT";

    private static final byte[] pas = "Help".getBytes();


    private static final Map<String, String> emojis = new HashMap<>();

    static {
        emojis.put("\uD83D\uDE00", ":-)");
        emojis.put("\uD83D\uDE03", ":-)");
        emojis.put("\uD83D\uDE04", ":-D");
        emojis.put("\uD83D\uDE09", ";-)");
        emojis.put("\uD83D\uDE0A", ":-3");
        emojis.put("\uD83D\uDE0E", "B-)");
        emojis.put("\uD83D\uDE1B", ":-P");
        emojis.put("\uD83D\uDE26", ":-(");
        emojis.put("\uD83D\uDE22", ":`(");
        emojis.put("\uD83D\uDE17", ":-*");
        emojis.put("\uD83D\uDC94", "</3");
        emojis.put("\u2764", "<3");
    }

    private class DiscordListener extends ListenerAdapter {
        @Override
        public void onMessageReceived(MessageReceivedEvent event) {
            super.onMessageReceived(event);
            if(event.isFromType(ChannelType.TEXT) && !event.getAuthor().isBot()) {
                String title = event.getTextChannel().getName();
                if(DISCORD_CHANNELS.indexOf(title) != -1) {
                    String name = com.wurmonline.server.LoginHandler.raiseFirstLetter(event.getMember().getEffectiveName());
                    String message = event.getMessage().getContentDisplay().trim();
                    for(Entry<String, String> p : emojis.entrySet())
                        message = message.replace(p.getKey(), p.getValue());
                    if(!message.contains("\n")) sendMessage(title, name, message);
                    else for(String line : message.split("\n")) {
                        line = line.trim();
                        if(!line.isEmpty()) sendMessage(title, name, line);
                    }
                }
            }
        }

        private void sendMessage(String title, String name, String message) {
            PlayerData pd = PlayersData.getInstance().get(name);
            Player player = Players.getInstance().getPlayerOrNull(name);
            PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithName(name);
            if(pd == null && player == null && pi == null) name = "@" + name;
            if(title.equals(TRADE_DISCORD)) {
                sendTradeMessage(player, Message.TRADE, TRADE, "<" + name + "> " + message, -1, -1, -1);
            } else if(title.equals(HELP_DISCORD)) {
                if(player != null) {
                    if(player.getPower() >= MiscConstants.POWER_DEMIGOD) name = "GM " + name;
                    else if(player.isPlayerAssistant()) name = "CA " + name;
                }
                sendHelpMessage(player, Message.CA, HELP, "<" + name + "> " + message, -1, -1, -1);
            } else if(title.equals(STAFF_DISCORD)) {
                if(player != null && player.mayHearMgmtTalk()) {
                    int i = player.getPower() + 1;
                    if(i < 2) i = player.mayHearDevTalk()? 1 : 0;
                    Players.getInstance().sendMgmtMessage(player, name, message, false, false, POWER_R[i], POWER_G[i], POWER_B[i]);
                }
            } else {
                String channel = WORLD;
                if(title.equals(TIRVALON_DISCORD)) channel = TIRVALON;
                else if(title.equals(CELLIMDAR_DISCORD)) channel = CELLIMDAR;
                else if(title.equals(SHODROQ_DISCORD)) channel = SHODROQ;
                else if(title.equals(THEKANDRE_DISCORD)) channel = THEKANDRE;
                if(channel == WORLD)
                    sendWorldMessage(null, Message.KINGDOM, channel, "<" + name + "> " + message, -1, -1, -1);
                else sendKingdomMessage(null, Message.KINGDOM, channel, "<" + name + "> " + message, -1, -1, -1);
            }
        }
    }

    private static ChatChannels instance = null;

    public static ChatChannels getInstance() {
        if(instance == null) instance = new ChatChannels();
        return instance;
    }

    public static void broadCastMessage(String message, int color) {
        int r = ((color >> 16) & 0xff), g = ((color >> 8) & 0xff), b = (color & 0xff);
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            p.getCommunicator().sendServerMessage(message, r, g, b);
        }
    }

    public static void broadCastSystemMessage(String message) {
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            p.getCommunicator().sendSystemMessage(message);
        }
    }

    @SuppressWarnings("unused")
    public static void sendPAWindow(Player player) {
        String chan = "Help";
        Communicator communicator = player.getCommunicator();
        communicator.sendMessage(new Message(player, Message.CA, HELP, "<System> This is the Help channel. Type your questions here.", 250, 150, 250));
        communicator.sendMessage(new Message(player, Message.CA, HELP, "<System> You can also find this channel on our Discord: https://discord.gg/h6ey4eM", 250, 150, 250));
        Players.getInstance().joinPAChannel(player);
    }

    @SuppressWarnings("unused")
    public static void sendAddPa(Communicator communicator, String name, long wurmid) {
        Player player = communicator.getPlayer();
        if(player != null && player.hasLink()) {
            try {
                byte[] tempStringArr = name.getBytes(StandardCharsets.UTF_8);
                ByteBuffer bb = communicator.getConnection().getBuffer();
                bb.put((byte) -13);
                bb.put((byte) pas.length);
                bb.put(pas);
                bb.put((byte) tempStringArr.length);
                bb.put(tempStringArr);
                bb.putLong(wurmid);
                communicator.getConnection().flush();
            } catch(Exception e) {
                logger.log(Level.WARNING, "ChatChannels::sendAddPa - " + player.getName() + ": wurmId " + wurmid + " " + e.getMessage(), e);
                player.setLink(false);
            }
        }

    }

    @SuppressWarnings("unused")
    public static void sendRemovePa(Communicator communicator, String name) {
        Player player = communicator.getPlayer();
        if(player != null && player.hasLink()) {
            try {
                byte[] tempStringArr = name.getBytes(StandardCharsets.UTF_8);
                ByteBuffer bb = communicator.getConnection().getBuffer();
                bb.put((byte) 114);
                bb.put((byte) pas.length);
                bb.put(pas);
                bb.put((byte) tempStringArr.length);
                bb.put(tempStringArr);
                communicator.getConnection().flush();
            } catch(Exception e) {
                logger.log(Level.WARNING, "ChatChannels::sendRemovePa - " + player.getName() + ": name " + name + " " + e.getMessage(), e);
                player.setLink(false);
            }

        }
    }

    protected JDA jda;
    protected String botToken;
    protected String serverName;
    protected boolean showConnectedPlayers = true;
    protected long connectedPlayerUpdateInterval = 120L;
    protected long lastPolledPlayers = 0L;
    protected long pollPlayerInterval = TimeConstants.SECOND_MILLIS * connectedPlayerUpdateInterval;
    protected DiscordListener discordListener = null;

    private ChatChannels() {
    }

    public void init() {
        switch(Servers.getLocalServerId()) {
            case AwakeningMod.DEV_SERVER_ID:
                botToken = "NDgzNTgxNzk5MTQ2OTc5MzM4.DmVimw.mMbwqAmRPHAhPfizuF4f-QA1n88";
                serverName = "Awakening Development";
                break;

            case AwakeningMod.AWAKENING_SERVER_ID:
            default:
                botToken = "NTAxNTUzNDg5NTE2ODIyNTM5.DqbFIw.Al0YbNLMEnLe3gQ7uxlS0T6gKRo";
                serverName = "Awakening Wurm Unlimited";
                break;
        }
        discordListener = new DiscordListener();
        try {
            jda = JDABuilder.createDefault(botToken).addEventListeners(discordListener).build();
        } catch(LoginException e) {
            logger.log(Level.SEVERE, "ChatChannels: " + e.getMessage(), e);
        }
    }

    public void onPlayerLogin(Player player) {
        PlayerData pd = PlayersData.getInstance().get(player);
        Communicator communicator = player.getCommunicator();
        communicator.sendMessage(new Message(player, Message.SAY, LOCAL, "<System> Welcome to Awakening, " + player.getName() + "!", 250, 150, 250));
        communicator.sendMessage(new Message(player, Message.SAY, LOCAL, "<System> Website: " + Config.webPageHost, 250, 150, 250));
        communicator.sendMessage(new Message(player, Message.SAY, LOCAL, "<System> Map: " + Config.webPageHost + "/map/Awakening/", 250, 150, 250));
        communicator.sendMessage(new Message(player, Message.SAY, LOCAL, "<System> Vote: https://wurm-unlimited.com/server/6797/", 250, 150, 250));
        if(!pd.isWorldChannelMuted()) {
            communicator.sendMessage(new Message(player, Message.SAY, WORLD, "<System> This is the World channel.", 250, 150, 250));
            communicator.sendMessage(new Message(player, Message.SAY, WORLD, "<System> Citizens of all kingdoms can join this channel.", 250, 150, 250));
            communicator.sendMessage(new Message(player, Message.SAY, WORLD, "<System> You can also find this channel on our Discord: https://discord.gg/h6ey4eM", 250, 150, 250));
        }
        if(!pd.isRoleplayChannelMuted()) {
            communicator.sendMessage(new Message(player, Message.SAY, ROLEPLAY, "<System> This is the Roleplay channel.", 250, 150, 250));
            communicator.sendMessage(new Message(player, Message.SAY, ROLEPLAY, "<System> Here should only be posted dialogue expressed in voice by your character.", 250, 150, 250));
            communicator.sendMessage(new Message(player, Message.SAY, ROLEPLAY, "<System> Gestures, facial expressions and body language should be described between stars: *smiles* or *scratches his ears*", 250, 150, 250));
            communicator.sendMessage(new Message(player, Message.SAY, ROLEPLAY, "<System> Anything OOC (out-of-character) should be enclosed in square brackets: [time for dinner, brb]", 250, 150, 250));
        }
    }

    public MessagePolicy onPlayerMessage(Communicator communicator, String message, String title) {
        if(PUBLIC_CHANNELS.indexOf(title) != -1) {
            if(title.equals(TRADE)) {
                return onTradeMessage(communicator, message, title);
            } else if(title.equals(ROLEPLAY)) {
                return onRoleplayMessage(communicator, message, title);
            } else if(title.equals(HELP)) {
                return onHelpMessage(communicator, message, title);
            } else if(title.equals(STAFF)) {
                return onMgmtMessage(communicator, message);
            } else {
                return onWorldMessage(communicator, message, title);
            }
        }
        return MessagePolicy.PASS;
    }

    public MessagePolicy onKingdomMessage(Message message) {
        String title = message.getWindow();
        if(KINGDOM_CHANNELS.indexOf(title) != -1) {
            String discord;
            if(title.equals(TIRVALON)) discord = TIRVALON_DISCORD;
            else if(title.equals(CELLIMDAR)) discord = CELLIMDAR_DISCORD;
            else if(title.equals(SHODROQ)) discord = SHODROQ_DISCORD;
            else if(title.equals(THEKANDRE)) discord = THEKANDRE_DISCORD;
            else return MessagePolicy.PASS;
            sendToDiscord(discord, message.getMessage(), false);
        }
        return MessagePolicy.PASS;
    }

    public MessagePolicy onTradeMessage(Communicator communicator, String message, String title) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            boolean emote = false;
            if(message.charAt(0) == '/') {
                if(message.startsWith("/me ")) emote = true;
                else return MessagePolicy.PASS;
            } else if(message.charAt(0) == '#') {
                return MessagePolicy.PASS;
            }
            if(player.isDead()) communicator.sendNormalServerMessage("Nobody can hear you now.");
            else if(emote) communicator.sendNormalServerMessage("You can not emote in that window.");
            else {
                if(Servers.localServer.entryServer && !player.isReallyPaying() && !player.mayMute()) {
                    communicator.sendNormalServerMessage("You may not use global Trade chat as a non-premium until you use a portal.");
                } else if(communicator.isInvulnerable()) {
                    communicator.sendAlertServerMessage("You may not use Trade chat until you have moved and lost invulnerability.");
                } else if(!player.isTradeChannel()) {
                    communicator.sendNormalServerMessage("You must toggle Trade channel on from your profile.");
                } else {
                    int r = player.hasColoredChat()? player.getCustomRedChat() : -1;
                    int g = player.hasColoredChat()? player.getCustomGreenChat() : -1;
                    int b = player.hasColoredChat()? player.getCustomBlueChat() : -1;
                    message = "<" + player.getName() + "> (" + player.getKingdomName().substring(0, 3) + ") " + message;
                    sendTradeMessage(player, Message.TRADE, TRADE, message, r, g, b);
                    player.chatted();
                    sendToDiscord(TRADE_DISCORD, message, false);
                }
            }
        }
        return MessagePolicy.DISCARD;
    }

    public MessagePolicy onWorldMessage(Communicator communicator, String message, String title) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            boolean emote = false;
            if(message.charAt(0) == '/') {
                if(message.startsWith("/me ")) emote = true;
                else return MessagePolicy.PASS;
            } else if(message.charAt(0) == '#') {
                return MessagePolicy.PASS;
            }
            if(player.isDead()) communicator.sendNormalServerMessage("Nobody can hear you now.");
            else if(emote) communicator.sendNormalServerMessage("You can not emote in that window.");
            else if(Servers.localServer.entryServer && !player.isReallyPaying() && !player.mayMute()) {
                communicator.sendNormalServerMessage("You may not use the " + title + " chat as a non-premium until you use a portal.");
            } else if(communicator.isInvulnerable()) {
                communicator.sendAlertServerMessage("You may not use the " + title + " chat until you have moved and lost invulnerability.");
            } else {
                message = "<" + player.getName() + "> (" + player.getKingdomName().substring(0, 3) + ") " + message;
                sendWorldMessage(player, Message.KINGDOM, title, message, -1, -1, -1);
                player.chatted();
                sendToDiscord(WORLD_DISCORD, message, false);
            }
        }
        return MessagePolicy.DISCARD;
    }

    public MessagePolicy onRoleplayMessage(Communicator communicator, String message, String title) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            if(player.isUndead()) {
                communicator.sendNormalServerMessage("Nobody can hear you now.");
            } else if(player.isDead()) {
                communicator.sendNormalServerMessage("Nobody can see you now.");
            } else if(communicator.isInvulnerable()) {
                communicator.sendAlertServerMessage("You may not use the " + title + " chat until you have moved and lost invulnerability.");
            } else {
                PlayerData pd = PlayersData.getInstance().get(player);
                Character character = pd.getCharacter();
                boolean emote = false;
                if(message.charAt(0) == '/') {
                    if(!message.startsWith("/me ")) return MessagePolicy.PASS;
                    emote = true;
                } else if(message.charAt(0) == '#') {
                    return MessagePolicy.PASS;
                }
                if(player.isStealth()) player.setStealth(false);
                final VolaTile tile = player.getCurrentTile();
                if(tile != null) {
                    if(emote) {
                        String emSend = (character.getName() != null? "<" + player.getName() + "> " + character.getName() : player.getName()) + " " + message.substring(4);
                        Message mess = new Message(player, Message.EMOTE, ROLEPLAY, emSend);
                        tile.broadCastMessage(mess);
                    } else {
                        VirtualZone[] watchers = tile.getWatchers();
                        if(watchers != null) {
                            ArrayList<MulticolorLineSegment> segments;
                            message = drunkGarble(player, message);
                            segments = new ArrayList<>();
                            if(character.getName() != null)
                                segments.add(new MulticolorLineSegment("<" + player.getName() + "> ", (byte) 0));
                            segments.add(new CharacterLineSegment(player));
                            segments.add(new MulticolorLineSegment(" says: ", (byte) 0));
                            segments.add(new MulticolorLineSegment(message, (byte) 0));
                            for(VirtualZone z : watchers)
                                if(z.getWatcher().isPlayer() && z.getWatcher() != player) {
                                    Player p = (Player) z.getWatcher();
                                    PlayerData zpd = PlayersData.getInstance().get(p);
                                    if(!zpd.isRoleplayChannelMuted())
                                        p.getCommunicator().sendColoredMessage(ROLEPLAY, segments);
                                }
                            if(!pd.isRoleplayChannelMuted()) {
                                segments = new ArrayList<>();
                                segments.add(new CharacterLineSegment(player));
                                segments.add(new MulticolorLineSegment(" say: ", (byte) 0));
                                segments.add(new MulticolorLineSegment(message, (byte) 0));
                                player.getCommunicator().sendColoredMessage(ROLEPLAY, segments);
                            }
                        }
                    }
                }
            }
            return MessagePolicy.DISCARD;
        }
        return MessagePolicy.PASS;
    }

    public MessagePolicy onHelpMessage(Communicator communicator, String message, String title) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            boolean emote = false;
            if(message.charAt(0) == '/') {
                if(message.startsWith("/me ")) emote = true;
                else return MessagePolicy.PASS;
            } else if(message.charAt(0) == '#') {
                return MessagePolicy.PASS;
            }
            if(emote) communicator.sendNormalServerMessage("You can not emote in that window.");
            else {
                String name = player.getName();
                if(player.getPower() >= MiscConstants.POWER_DEMIGOD) name = "GM " + name;
                else if(player.mayMute()) name = "CM " + name;
                else if(player.isPlayerAssistant()) name = "CA " + name;
                message = "<" + name + "> (" + player.getKingdomName().substring(0, 3) + ") " + message;
                sendHelpMessage(player, Message.CA, title, message, -1, -1, -1);
                player.chatted();
                sendToDiscord(HELP_DISCORD, message, false);
            }
        }
        return MessagePolicy.DISCARD;
    }

    public MessagePolicy onMgmtMessage(Communicator communicator, String message) {
        if(message.charAt(0) != '/' && message.charAt(0) != '#') {
            Player player = communicator.getPlayer();
            String name = player.getName();
            message = "<" + name + "> " + message;
            sendStaffMessage(message);
        }
        return MessagePolicy.PASS;
    }

    public MessagePolicy onBugMessage(Communicator communicator, String message) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            message = "<" + player.getName() + "> " + message;
            player.chatted();
            sendToDiscord(BUGS_DISCORD, message, false);
            communicator.sendNormalServerMessage("Your message has been posted to the #bugs channel on Discord.");
        }
        return MessagePolicy.DISCARD;
    }

    public MessagePolicy onSuggestionMessage(Communicator communicator, String message) {
        Player player = communicator.getPlayer();
        if(!player.isMute()) {
            message = "<" + player.getName() + "> " + message;
            player.chatted();
            sendToDiscord(SUGGESTIONS_DISCORD, message, false);
            communicator.sendNormalServerMessage("Your message has been posted to the #suggestions channel on Discord.");
        }
        return MessagePolicy.DISCARD;
    }

    public MessagePolicy onReportsMessage(String message) {
        sendToDiscord(REPORTS_DISCORD, message, false);
        return MessagePolicy.DISCARD;
    }

    public void onServerPoll() {
        if(showConnectedPlayers) {
            if(System.currentTimeMillis() > lastPolledPlayers + pollPlayerInterval) {
                if(Servers.localServer.LOGINSERVER) {
                    try {
                        Presence presence = jda.getPresence();
                        if(presence != null) {
                            long n = Players.getInstance().getPlayerMap().values().stream().filter(p -> p.isFullyLoaded()).count();
                            presence.setActivity(Activity.playing(n + " online!"));
                        }
                    } catch(Exception e) { }
                }
                lastPolledPlayers = System.currentTimeMillis();
            }
        }
    }

    public void sendTradeMessage(Player player, byte type, String title, String message, int r, int g, int b) {
        Message msg = new Message(player, type, title, message, r, g, b);
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            if(!p.getCommunicator().isInvulnerable() && p.isTradeChannel() && (player == null || !p.isIgnored(player.getWurmId()))) {
                if(player == null || !player.hasColoredChat) {
                    msg.setColorR(150);
                    msg.setColorG(230);
                    msg.setColorB(255);
                }
                p.getCommunicator().sendMessage(msg);
            }
        }
    }

    public void sendHelpMessage(Player player, byte type, String title, String message, int r, int g, int b) {
        if(player.isPlayerAssistant() || player.getPower() >= MiscConstants.POWER_HERO) {
            r = 105;
            g = 231;
            b = 210;
        }
        Message msg = new Message(player, type, title, message, r, g, b);
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            if((player == null || !p.isIgnored(player.getWurmId()))) {
                p.getCommunicator().sendMessage(msg);
            }
        }
    }

    public void sendWorldMessage(Player player, byte type, String title, String message, int r, int g, int b) {
        Message msg = new Message(player, type, title, message, r, g, b);
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            if(!p.getCommunicator().isInvulnerable() && (player == null || !p.isIgnored(player.getWurmId()))) {
                PlayerData pd = PlayersData.getInstance().get(p);
                if(!pd.isWorldChannelMuted())
                    p.getCommunicator().sendMessage(msg);
            }
        }
    }

    public void sendKingdomMessage(Player player, byte type, String title, String message, int r, int g, int b) {
        Kingdom k = Kingdoms.getKingdomWithChatTitle(title);
        if(k == null) return;
        Message msg = new Message(player, type, title, message, r, g, b);
        final Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            if(p.getKingdomId() != k.getId()) continue;
            if(!p.getCommunicator().isInvulnerable() && (player == null || !p.isIgnored(player.getWurmId()))) {
                p.getCommunicator().sendMessage(msg);
            }
        }
    }

    public void sendNewsMessage(String message) {
        sendToDiscord(NEWS_DISCORD, message, false);
    }

    public void sendHistoryMessage(String message) {
        broadCastSystemMessage(message);
        sendToDiscord(HISTORY_DISCORD, message, false);
    }

    @SuppressWarnings("unused")
    public void sendRumour(Creature creature) {
        sendHistoryMessage("Rumours of " + creature.getName() + " are starting to spread.");
    }

    public void sendNewPlayer(Player player) {
        sendHistoryMessage("Welcome to our new player " + player.getName() + "! (" + player.getKingdomName() + ")");
    }

    public void sendNewAdept(Player player) {
        sendHistoryMessage(player.getName() + " is now an adept of " + player.getDeity().name + "! (" + player.getKingdomName() + ")");
    }

    public void sendNewGuardTower(Player player, Item tower) {
        sendHistoryMessage(player.getName() + " has raised a new guard tower! (" + player.getKingdomName() + ")");
    }

    public void sendTitle(Player player, Title title) {
        boolean isMale = player.getSex() == MiscConstants.SEX_MALE;
        try {
            Skill skill = player.getSkills().getSkill(title.getSkillId());
            sendHistoryMessage(player.getName() + " just received the title '" + title.getName(isMale) + "', for gaining " +
                               StringUtils.decimalFormat.format(skill.getKnowledge()) + " in " + skill.getName() + "! (" + player.getKingdomName() + ")");
        } catch(Exception e) {
            sendHistoryMessage(player.getName() + " just received the title '" + title.getName(isMale) + "'! (" + player.getKingdomName() + ")");
        }
    }

    @SuppressWarnings("unused")
    public void sendNewVillage(Village village, Creature founder) {
        sendHistoryMessage("The settlement of " + village.getName() + " has just been founded by " + founder.getName() + ". (" + Kingdoms.getNameFor(village.kingdom) + ")");
    }

    public void sendStaffMessage(String message) {
        sendToDiscord(STAFF_DISCORD, message, false);
    }

    public void sendToDiscord(String channel, String message, boolean includeMap) {
        MessageBuilder builder = new MessageBuilder();
        if(includeMap) message = message + " (" + Servers.localServer.mapname + ")";
        builder.append(message);
        try {
            List<Guild> guilds = jda.getGuildsByName(serverName, true);
            if(guilds != null && !guilds.isEmpty()) {
                List<TextChannel> channels = guilds.get(0).getTextChannelsByName(channel, true);
                if(channels != null && !channels.isEmpty()) {
                    channels.get(0).sendMessage(builder.build()).queue();
                }
            }
        } catch(Exception e) { }
    }

    private String drunkGarble(Player player, String string) {
        String garbledString = string;
        if(player.isUndead()) {
            StringTokenizer st = new StringTokenizer(string);
            while(st.hasMoreTokens()) {
                String s = st.nextToken();
                int start;
                if(Server.rand.nextBoolean()) {
                    if(string.length() > 6) {
                        start = Server.rand.nextInt(s.length() - 6);
                        if(Server.rand.nextBoolean()) {
                            garbledString = s.substring(0, start) + "HRR";
                        } else {
                            garbledString = "HSS" + s.substring(start + 6);
                        }
                    }
                } else if(string.length() > 5) {
                    start = Server.rand.nextInt(s.length() - 5);
                    if(Server.rand.nextBoolean()) {
                        garbledString = "UUH" + s.substring(start + 5);
                    } else {
                        garbledString = s.substring(0, start) + "GHH*";
                    }
                }
            }
        }
        if(player.getAlcohol() > 50.0F && Server.rand.nextInt(Math.max(2, 10 - (int) (player.getAlcohol() / 10.0F))) == 0) {
            int start;
            if(Server.rand.nextBoolean()) {
                if(string.length() > 6) {
                    start = Server.rand.nextInt(string.length() - 6);
                    if(Server.rand.nextInt(2) == 0) {
                        garbledString = string.substring(0, start) + "*burp*" + string.substring(start + 6);
                    } else {
                        garbledString = string.substring(0, start) + "*BURP*" + string.substring(start + 6);
                    }
                }
            } else if(string.length() > 5) {
                start = Server.rand.nextInt(string.length() - 5);
                if(Server.rand.nextInt(2) == 0) {
                    garbledString = string.substring(0, start) + "*hic*" + string.substring(start + 5);
                } else {
                    garbledString = string.substring(0, start) + "*HIC*" + string.substring(start + 5);
                }
            }
        }
        return garbledString;
    }
}
