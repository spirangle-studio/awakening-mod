package net.spirangle.awakening.players;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.support.JSONString;

import java.util.logging.Logger;


public class CombatData implements JSONString {

    private static final Logger logger = Logger.getLogger(CombatData.class.getName());

    private final PlayerData playerData;
    private boolean hide;
    private int deaths;
    private int kills;
    private int buries;
    private int karma;
    private int killPlayers;
    private int killGuards;
    private int killDragons;
    private int killUniques;
    private int killUndead;
    private int inf;
    private int infSumKills;
    private int infSumScore;
    private int infLastKills;
    private int infLastScore;
    private double startFightSkill;
    private double lastFightSkill;
    private double fightSkillGain;
    private double maxFightSkillGain;
    private int maxFightSkillGainTemplateId;
    private int sumKills;
    private int sumBuries;
    private long sumKarma;
    private long maxKarma;
    private int maxKarmaTemplateId;
    long lastReset;
    private long targetStatusTimer;


    public CombatData(PlayerData playerData) {
        this.playerData = playerData;
        this.hide = false;
        this.deaths = 0;
        this.kills = 0;
        this.buries = 0;
        this.karma = 0;
        this.killPlayers = 0;
        this.killGuards = 0;
        this.killDragons = 0;
        this.killUniques = 0;
        this.killUndead = 0;
        this.inf = 0;
        this.infSumKills = 0;
        this.infSumScore = 0;
        this.infLastKills = 0;
        this.infLastScore = 0;
        reset();
        this.targetStatusTimer = 0L;
    }

    public CombatData(PlayerData playerData, JSONObject jo) {
        this.playerData = playerData;
        this.hide = jo.optBoolean("hide", false);
        this.deaths = jo.optInt("deaths", 0);
        this.kills = jo.optInt("kills", 0);
        this.buries = jo.optInt("buries", 0);
        this.karma = jo.optInt("karma", 0);
        this.killPlayers = jo.optInt("killPlayers", 0);
        this.killGuards = jo.optInt("killGuards", 0);
        this.killDragons = jo.optInt("killDragons", 0);
        this.killUniques = jo.optInt("killUniques", 0);
        this.killUndead = jo.optInt("killUndead", 0);
        this.inf = 0;
        this.infSumKills = 0;
        this.infSumScore = 0;
        this.infLastKills = 0;
        this.infLastScore = 0;
        JSONArray inf = jo.optJSONArray("inf");
        if(inf != null) {
            this.inf = inf.optInt(0, 0);
            this.infSumKills = inf.optInt(1, 0);
            this.infSumScore = inf.optInt(2, 0);
            this.infLastKills = inf.optInt(3, 0);
            this.infLastScore = inf.optInt(4, 0);
        }
        reset();
        this.targetStatusTimer = 0L;
    }

    @Override
    public String toJSONString() {
        StringBuilder data = new StringBuilder();
        if(this.hide) {
            if(data.length() > 0) data.append(',');
            data.append("hide:true");
        }
        if(this.deaths > 0) {
            if(data.length() > 0) data.append(',');
            data.append("deaths:").append(this.deaths);
        }
        if(this.kills > 0) {
            if(data.length() > 0) data.append(',');
            data.append("kills:").append(this.kills);
        }
        if(this.buries > 0) {
            if(data.length() > 0) data.append(',');
            data.append("buries:").append(this.buries);
        }
        if(this.karma > 0) {
            if(data.length() > 0) data.append(',');
            data.append("karma:").append(this.karma);
        }
        if(this.killPlayers > 0) {
            if(data.length() > 0) data.append(',');
            data.append("killPlayers:").append(this.killPlayers);
        }
        if(this.killGuards > 0) {
            if(data.length() > 0) data.append(',');
            data.append("killGuards:").append(this.killGuards);
        }
        if(this.killDragons > 0) {
            if(data.length() > 0) data.append(',');
            data.append("killDragons:").append(this.killDragons);
        }
        if(this.killUniques > 0) {
            if(data.length() > 0) data.append(',');
            data.append("killUniques:").append(this.killUniques);
        }
        if(this.killUndead > 0) {
            if(data.length() > 0) data.append(',');
            data.append("killUndead:").append(this.killUndead);
        }
        if(this.inf > 0) {
            if(data.length() > 0) data.append(',');
            data.append("inf: [")
                .append(this.inf).append(',')
                .append(this.infSumKills).append(',')
                .append(this.infSumScore).append(',')
                .append(this.infLastKills).append(',')
                .append(this.infLastScore)
                .append(']');
        }
        if(data.length() == 0) return null;
        return "combat:{" + data + "}";
    }

    public boolean setHidden(boolean hide) {
        if(this.hide != hide) {
            this.hide = hide;
            playerData.changed = true;
            return true;
        }
        return false;
    }

    public boolean isHidden() {
        return this.hide;
    }

    public void reset() {
        startFightSkill = 0.0d;
        lastFightSkill = 0.0d;
        maxFightSkillGain = 0.0d;
        maxFightSkillGainTemplateId = -1;
        sumKills = 0;
        sumBuries = 0;
        sumKarma = 0;
        maxKarma = 0;
        maxKarmaTemplateId = -1;
        lastReset = System.currentTimeMillis();
    }

    public void addDeath(Player player) {
        deaths = getDeaths() + 1;
        playerData.changed = true;
    }

    public void addKill(Player player, Creature creature) {
        int templateId = creature.getTemplateId();
        kills = getKills() + 1;
        sumKills = getSumKills() + 1;
        if(creature.isPlayer()) ++killPlayers;
        else if(creature.isKingdomGuard()) ++killGuards;
        else if(creature.isDragon()) ++killDragons;
        else if(creature.isUnique()) ++killUniques;
        else if(creature.isUndead()) ++killUndead;
        playerData.changed = true;
    }

    public void addKillSkill(Player player, Creature creature, double pskill, double skillGained) {
        if(getStartFightSkill() == 0.0d) startFightSkill = pskill;
        lastFightSkill = pskill;
        fightSkillGain = (pskill + skillGained > 100.0d? pskill + (100.0d - pskill) / 100.0d : pskill + skillGained) - getStartFightSkill();
        if(skillGained >= getMaxFightSkillGain()) {
            CreatureTemplate template = creature.getTemplate();
            int templateId = template.getTemplateId();
            maxFightSkillGain = skillGained;
            maxFightSkillGainTemplateId = templateId;
        }
    }

    public void addBury(CreatureTemplate template, int karma) {
        buries = getBuries() + 1;
        this.karma = this.getKarma() + karma;
        sumBuries = getSumBuries() + 1;
        sumKarma = getSumKarma() + karma;
        if(karma >= getMaxKarma()) {
            maxKarma = karma;
            maxKarmaTemplateId = template.getTemplateId();
        }
        playerData.changed = true;
    }

    public void addCompletedInfestation(int kills, int score) {
        if(kills == 0 && score == 0) return;
        ++inf;
        infSumKills += kills;
        infSumScore += score;
        infLastKills = kills;
        infLastScore = score;
        playerData.changed = true;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getKills() {
        return kills;
    }

    public int getBuries() {
        return buries;
    }

    public int getKarma() {
        return karma;
    }

    public int getKillPlayers() {
        return killPlayers;
    }

    public int getKillGuards() {
        return killGuards;
    }

    public int getKillDragons() {
        return killDragons;
    }

    public int getKillUniques() {
        return killUniques;
    }

    public int getKillUndead() {
        return killUndead;
    }

    public int getInfestations() {
        return inf;
    }

    public int getInfestationKills() {
        return infSumKills;
    }

    public int getInfestationScore() {
        return infSumScore;
    }

    public int getLastInfestationKills() {
        return infLastKills;
    }

    public int getLastInfestationScore() {
        return infLastScore;
    }

    public double getStartFightSkill() {
        return startFightSkill;
    }

    public double getLastFightSkill() {
        return lastFightSkill;
    }

    public double getFightSkillGain() {
        return fightSkillGain;
    }

    public double getMaxFightSkillGain() {
        return maxFightSkillGain;
    }

    public int getMaxFightSkillGainTemplateId() {
        return maxFightSkillGainTemplateId;
    }

    public int getSumKills() {
        return sumKills;
    }

    public int getSumBuries() {
        return sumBuries;
    }

    public long getSumKarma() {
        return sumKarma;
    }

    public long getMaxKarma() {
        return maxKarma;
    }

    public int getMaxKarmaTemplateId() {
        return maxKarmaTemplateId;
    }

    public void updateTargetStatus(Player player) {
        if(player.getTarget() == null || targetStatusTimer > System.currentTimeMillis() - 300000L) return;
        targetStatusTimer = System.currentTimeMillis();
        if(player.getTarget() != player.opponent) {
            Creature target = player.getTarget();
            if(target.getTarget() == null || target.getTarget().getWurmId() != player.getWurmId()) {
                player.setTarget(player.opponent.getWurmId(), true);
            }
        }
    }
}
