package net.spirangle.awakening.players;

import com.google.common.base.Strings;
import com.wurmonline.server.Players;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.support.JSONString;

import java.util.logging.Logger;


public class Character implements JSONString {

    private static final Logger logger = Logger.getLogger(Character.class.getName());

    private final PlayerData playerData;
    private boolean hide;
    private String name;
    private long spouseId;
    private long motherId;
    private long fatherId;
    private String personality;
    private String background;

    public Character(PlayerData playerData) {
        this.playerData = playerData;
        this.hide = false;
        this.name = null;
        this.spouseId = -10L;
        this.motherId = -10L;
        this.fatherId = -10L;
        this.personality = null;
        this.background = null;
    }

    public Character(PlayerData playerData, JSONObject jo) {
        this.playerData = playerData;
        this.hide = jo.optBoolean("hide", false);
        this.name = jo.optString("name", null);
        this.spouseId = jo.optLong("spouse", -10L);
        this.motherId = jo.optLong("mother", -10L);
        this.fatherId = jo.optLong("father", -10L);
        this.personality = jo.optString("personality", null);
        this.background = jo.optString("background", null);
    }

    @Override
    public String toJSONString() {
        StringBuilder data = new StringBuilder();
        if(this.hide) {
            if(data.length() > 0) data.append(',');
            data.append("hide:true");
        }
        if(!Strings.isNullOrEmpty(this.name)) {
            if(data.length() > 0) data.append(',');
            data.append("name:").append(JSONObject.quote(this.name));
        }
        if(this.spouseId != -10L) {
            if(data.length() > 0) data.append(',');
            data.append("spouse:").append(this.spouseId);
        }
        if(this.motherId != -10L) {
            if(data.length() > 0) data.append(',');
            data.append("mother:").append(this.motherId);
        }
        if(this.fatherId != -10L) {
            if(data.length() > 0) data.append(',');
            data.append("father:").append(this.fatherId);
        }
        if(!Strings.isNullOrEmpty(this.personality)) {
            if(data.length() > 0) data.append(',');
            data.append("personality:").append(JSONObject.quote(this.personality));
        }
        if(!Strings.isNullOrEmpty(this.background)) {
            if(data.length() > 0) data.append(',');
            data.append("background:").append(JSONObject.quote(this.background));
        }
        if(data.length() == 0) return null;
        return "character:{" + data + "}";
    }

    public boolean setHidden(boolean hide) {
        if(this.hide != hide) {
            this.hide = hide;
            playerData.changed = true;
            return true;
        }
        return false;
    }

    public boolean isHidden() {
        return this.hide;
    }

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        if(this.name == null || !this.name.equals(name)) {
            if(name == null || name.length() == 0) this.name = null;
            else this.name = name;
            playerData.changed = true;
            return true;
        }
        return false;
    }

    public long getSpouse() {
        return this.spouseId;
    }

    public boolean setSpouse(Player spouse) {
        if((spouse != null && this.spouseId != spouse.getWurmId()) || (spouse == null && this.spouseId != -10L)) {
            if(hasSpouse()) {
                PlayerData pd = PlayersData.getInstance().get(this.spouseId);
                if(pd != null) {
                    Character oldSpouse = pd.getCharacter();
                    oldSpouse.spouseId = -10L;
                    pd.changed = true;
                }
            }
            if(spouse == null) {
                this.spouseId = -10L;
            } else {
                this.spouseId = spouse.getWurmId();
            }
            playerData.changed = true;
            return true;
        }
        return false;
    }

    public boolean isSpouse(Player spouse) {
        return spouse != null && spouse.getWurmId() == this.spouseId;
    }

    public boolean hasSpouse() {
        return this.spouseId != -10L;
    }

    @SuppressWarnings("unused")
    public Player getMother() {
        if(this.motherId == -10L) return null;
        return Players.getInstance().getPlayerOrNull(this.motherId);
    }

    @SuppressWarnings("unused")
    public boolean setMother(Player mother) {
        if((mother != null && this.motherId != mother.getWurmId()) || (mother == null && this.motherId != -10L)) {
            if(mother == null) this.motherId = -10L;
            else this.motherId = mother.getWurmId();
            playerData.changed = true;
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public boolean isMother(Player mother) {
        return mother != null && mother.getWurmId() == this.motherId;
    }

    @SuppressWarnings("unused")
    public boolean hasMother() {
        return this.motherId != -10L;
    }

    @SuppressWarnings("unused")
    public Player getFather() {
        if(this.fatherId == -10L) return null;
        return Players.getInstance().getPlayerOrNull(this.fatherId);
    }

    @SuppressWarnings("unused")
    public boolean setFather(Player father) {
        if((father != null && this.fatherId != father.getWurmId()) || (father == null && this.fatherId != -10L)) {
            if(father == null) this.fatherId = -10L;
            else this.fatherId = father.getWurmId();
            playerData.changed = true;
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public boolean isFather(Player father) {
        return father != null && father.getWurmId() == this.fatherId;
    }

    @SuppressWarnings("unused")
    public boolean hasFather() {
        return this.fatherId != -10L;
    }

    public String getPersonality() {
        return personality;
    }

    public boolean setPersonality(String personality) {
        logger.info("set personality (" + this.personality + ") to " + personality);
        if(this.personality == null || !this.personality.equals(personality)) {
            if(personality == null || personality.length() == 0) this.personality = null;
            else this.personality = personality;
            playerData.changed = true;
            return true;
        }
        return false;
    }

    public String getBackground() {
        return background;
    }

    public boolean setBackground(String background) {
        logger.info("set background (" + this.background + ") to " + background);
        if(this.background == null || !this.background.equals(background)) {
            if(background == null || background.length() == 0) this.background = null;
            else this.background = background;
            playerData.changed = true;
            return true;
        }
        return false;
    }
}
