package net.spirangle.awakening.players;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.WurmColor;
import com.wurmonline.server.players.Player;

import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class StarterGear {

    private static final Logger logger = Logger.getLogger(StarterGear.class.getName());

    private StarterGear() {
    }

    public static Item insertStarterItem(Item inventory, int templateId, float qualityLevel, byte material) throws Exception {
        Item c = ItemFactory.createItem(templateId, qualityLevel, material, (byte) 0, null);
        inventory.insertItem(c);
        c.setAuxData((byte) 1);
        return c;
    }

    public static Item insertStarterItem(Item inventory, int templateId, float qualityLevel) throws Exception {
        Item c = Player.createItem(templateId, qualityLevel);
        inventory.insertItem(c);
        c.setAuxData((byte) 1);
        return c;
    }

    public static Item insertRealItem(Item inventory, int templateId, float qualityLevel) throws Exception {
        Item c = Player.createItem(templateId, qualityLevel);
        inventory.insertItem(c);
        return c;
    }

    public static void createSomeItems(Player player, int kingdom) {
        if(kingdom < 0) {
            kingdom = player.getKingdomId();
        }
        player.getCommunicator().sendNormalServerMessage("Creating new player items [kingdom=" + kingdom + ", x=" + player.getTileX() + ", y=" + player.getTileY() + "].");
        if(!player.isUndead()) {
            Item inventory = player.getInventory();
            try {
                float toolQuality = 10.0f;
                float armourQuality = 10.0f;
                float weaponQuality = 10.0f;
                float hatchetQuality = 10.0f;
                float flintQuality = 50.0f;
                float ql = 10.0f;
                byte armourMaterial = 0;
                byte weaponMaterial = 0;
                int head = ItemList.leatherCap;
                int legs = ItemList.leatherHose;
                int body = ItemList.leatherJacket;
                int arms = ItemList.leatherSleeve;
                int hands = ItemList.leatherGlove;
                int feet = ItemList.leatherBoot;
                int weapon = ItemList.swordLong;
                int shield = ItemList.shieldMediumWood;
                int hatchet = ItemList.hatchet;
                int bowl = ItemList.bowlPottery;
                int rope = ItemList.rope;
                int[] tools = {
                        ItemList.knifeCarving,
                        ItemList.backPack,
                        ItemList.shovel,
                        ItemList.saw,
                        ItemList.pickAxe,
                        ItemList.rake,
                        ItemList.compass,
                        ItemList.tent,
                        ItemList.deedStake,
                        -1, -1, -1
                };
                switch(kingdom) {
                    case 1: /* Tirvalon */
                        weapon = ItemList.maulMedium;
                        tools[9] = ItemList.sickle;
                        tools[10] = ItemList.groomingBrush;
                        break;
                    case 2: /* Cellimdar */
                        tools[9] = ItemList.file;
                        tools[10] = ItemList.needleIron;
                        break;
                    case 3: /* Shodroq */
                        weapon = ItemList.axeMedium;
                        tools[9] = ItemList.file;
                        tools[10] = ItemList.pelt;
                        break;
                    case 4: /* Thekandre */
                        weapon = ItemList.halberd;
                        tools[9] = ItemList.sickle;
                        tools[10] = ItemList.pelt;
                        break;
                    default: /* Awakening */
                        break;
                }
                if(head >= 0) insertStarterItem(inventory, head, armourQuality, armourMaterial);
                if(legs >= 0) insertStarterItem(inventory, legs, armourQuality, armourMaterial);
                if(body >= 0) insertStarterItem(inventory, body, armourQuality, armourMaterial);
                for(int i = 0; i < 2; ++i) {
                    if(feet >= 0) insertStarterItem(inventory, feet, armourQuality, armourMaterial);
                    if(arms >= 0) insertStarterItem(inventory, arms, armourQuality, armourMaterial);
                    if(hands >= 0) insertStarterItem(inventory, hands, armourQuality, armourMaterial);
                }
                if(weapon >= 0) insertStarterItem(inventory, weapon, weaponQuality, weaponMaterial);
                if(shield >= 0) insertStarterItem(inventory, shield, armourQuality);
                ql = 20.0f;
                insertStarterItem(inventory, ItemList.toolbelt, ql);
                player.wearItems();
                if(hatchet >= 0) insertStarterItem(inventory, ItemList.hatchet, hatchetQuality);
                ql = 2.0f;
                if(bowl >= 0) insertStarterItem(inventory, bowl, ql);
                ql = 10.0f;
                if(rope >= 0) insertStarterItem(inventory, rope, ql);
                for(int i = 0; i < tools.length; ++i) {
                    if(tools[i] >= 0) insertStarterItem(inventory, tools[i], toolQuality);
                }
                insertStarterItem(inventory, ItemList.flintSteel, flintQuality);

                insertRealItem(inventory, ItemList.handMirror, 20.0f);

            } catch(Exception e) {
                logger.log(Level.WARNING, "Failed to create some starter gear items.", e);
            }
            try {
                float papyrusQuality = 10.0f;
                String inscription;
                String inscriber;
                int penColour = 0;
                switch(kingdom) {
                    case 1: /* Tirvalon */
                        inscription = "Dear " + player.getName() + "! I write to you in a time of great need. Trivalon is asking for your help. We ask of " +
                                      "you to go out and explore this world, and to be our eyes and our hands. Make it your home. This is your quest, and " +
                                      "we hope that soon we will again prosper. Best wishes!";
                        inscriber = "Bradon";
                        penColour = WurmColor.createColor(50, 128, 50);
                        break;
                    case 2: /* Cellimdar */
                        inscription = "Dear " + player.getName() + "! I write to you in a time of great need. Cellimdar is asking for your help. We ask of " +
                                      "you to go out and explore this world, and to be our eyes and our hands. Make it your home. This is your quest, and " +
                                      "we hope that soon we will again prosper. Yours sincerely!";
                        inscriber = "Tekarim";
                        penColour = WurmColor.createColor(128, 50, 50);
                        break;
                    case 3: /* Shodroq */
                        inscription = "Servant " + player.getName() + "! You are commanded to do our bidding and go forth and lay this world beneath your feet. " +
                                      "Do as you must, but do not disappoint us. We expect your reports regularly. We'll be watching your progress.";
                        inscriber = "Radrigun";
                        penColour = WurmColor.createColor(128, 50, 128);
                        break;
                    case 4: /* Thekandre */
                        inscription = "Child " + player.getName() + "! In the name of our beloved god, Qaleshin, the queen of all dragons, go out in this world and " +
                                      "cleanse it of the darkness that has taken hold of the land. Please be careful in your quest, there are dangers everywhere. " +
                                      "Do not trust all dragons, but know that most of them are our friends. Blessings upon you!";
                        inscriber = "Galoden";
                        penColour = WurmColor.createColor(128, 128, 50);
                        break;
                    default: /* Awakening */
                        inscription = "Dear " + player.getName() + "! You have chosen to join this world on your own, with no support of any kingdom. If this was a " +
                                      "mistake, please contact a GM. Otherwise, best wishes and good luck! In any case, you are sincerely welcome! Kind regards...";
                        inscriber = "Unknown";
                        break;
                }
                Item papyrus = insertStarterItem(inventory, ItemList.papyrusSheet, papyrusQuality);
                papyrus.setAuxData((byte) 0);
                papyrus.setName("letter");
                papyrus.setInscription(inscription, inscriber, penColour);
            } catch(Exception e) {
                logger.log(Level.WARNING, "Failed to create papyrus sheet with starter instructions..", e);
            }

            ChatChannels.getInstance().sendNewPlayer(player);
            player.setFlag(MiscConstants.FLAG_CROSS_KINGDOM, true);
        }
    }
}
