package net.spirangle.awakening.players;

import com.wurmonline.server.players.JournalTier;
import com.wurmonline.server.players.PlayerJournal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wurmonline.server.players.AchievementList.*;
import static com.wurmonline.server.players.PlayerJournal.JOUR_TIER_INTERMEDIATE2;
import static net.spirangle.awakening.players.Achievements.ACH_INFESTATIONS;

public class Journal {

    private static final Logger logger = Logger.getLogger(Journal.class.getName());

    private static Journal instance = null;

    public static Journal getInstance() {
        if(instance == null) instance = new Journal();
        return instance;
    }

    private Journal() {

    }

    public void init() {
        try {
            HashMap<Byte, JournalTier> allTiers;
            Field allTiersField = PlayerJournal.class.getDeclaredField("allTiers");
            allTiersField.setAccessible(true);
            //noinspection unchecked
            allTiers = (HashMap<Byte, JournalTier>) allTiersField.get(null);

            Field achievementListField = JournalTier.class.getDeclaredField("achievementList");
            achievementListField.setAccessible(true);

            JournalTier tier;
            ArrayList<Integer> achievementList;

            tier = allTiers.get(JOUR_TIER_INTERMEDIATE2);
            //noinspection unchecked
            achievementList = (ArrayList<Integer>) achievementListField.get(tier);
            achievementList.clear();
            achievementList.addAll(Arrays.asList(ACH_USE_RUNE,
                                                 ACH_INFESTATIONS,
                                                 ACH_MEDITATEL7,
                                                 ACH_EQUIP_ENCHWEP,
                                                 ACH_ARCH_CACHE,
                                                 ACH_TITLES30,
                                                 ACH_RIDE_FASTHORSE,
                                                 ACH_CREATE_WAGON,
                                                 ACH_HARVEST_HERB50,
                                                 ACH_MISSION10,
                                                 ACH_CATCH_FISH10KG,
                                                 ACH_SLAY_CHAMPION));

        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
