package net.spirangle.awakening.players;

import com.wurmonline.server.Message;
import com.wurmonline.server.Players;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.economy.Economy;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.Titles.Title;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.support.JSONString;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.util.StringUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EconomyData implements JSONString {

    private static final Logger logger = Logger.getLogger(EconomyData.class.getName());

    public static void poll() {
        Map<String, Player> players = Players.getInstance().getPlayerMap();
        for(Player p : players.values()) {
            EconomyData economyData = PlayersData.getInstance().getBankAccount(p);
            int salary = economyData.getSalary(p);
            if(economyData.paySalary(p, salary)) {
                String m = new Change(salary).getChangeString();
                int k = p.getKingdomId();
                if(Config.kingdomSalaryMessage[k] != null) {
                    m = StringUtils.format(Config.kingdomSalaryMessage[k], m);
                } else {
                    m = "You gain the daily kingdom salary of " + m + ".";
                }
                p.getCommunicator().sendSafeServerMessage(m, Message.KINGDOM);
            }
        }
    }

    public static void pollNewYear() {
        for(PlayerData pd : PlayersData.getInstance().getPlayerData()) {
            EconomyData account = pd.getEconomyData();
            if(account.yearIncome > 0L) {
                account.yearIncome = 0L;
                account.yearTax = 0L;
                pd.changed = true;
            }
        }
    }

    public static long getTax(long money, boolean tariff) {
        float tax = Config.kingdomTax;
        if(tariff) tax += Config.kingdomTariff;
        return (long) ((float) money * tax);
    }

    private final PlayerData playerData;
    public int salary;
    long lastReset;
    long totalIncome;
    long totalTax;
    long yearIncome;
    long yearTax;
    long totalUpkeep;

    public EconomyData(PlayerData playerData) {
        this.playerData = playerData;
        reset();
    }

    public EconomyData(PlayerData playerData, JSONObject jo) {
        this.playerData = playerData;
        reset();
        totalIncome = jo.optLong("tot", 0L);
        totalTax = jo.optLong("tax", 0L);
        yearIncome = jo.optLong("year", 0L);
        yearTax = jo.optLong("ytax", 0L);
        totalUpkeep = jo.optLong("uk", 0L);
    }

    @Override
    public String toJSONString() {
        if(totalIncome == 0L) return null;
        StringBuilder sb = new StringBuilder();
        sb.append("tot:").append(totalIncome)
          .append(",tax:").append(totalTax)
          .append(",year:").append(yearIncome)
          .append(",ytax:").append(yearTax);
        if(totalUpkeep > 0L) {
            sb.append(",uk:").append(totalUpkeep);
        }
        return "economy:{" + sb + "}";
    }

    public void reset() {
        salary = -1;
        lastReset = System.currentTimeMillis();
    }

    public int getSalary(Player player) {
        if(salary < 0) {
            int k = player.getKingdomId();
            if(k >= 0 && k < Config.kingdomSalary.length && Config.kingdomSalary[k] > 0) {
                //            if(k==4 && (player.getDeity()!=null || player.getCultist()!=null)) return 0;
                salary = Config.kingdomSalary[k];
                if(Config.kingdomSkillSalary[k] > 0 || Config.kingdomMasterSalary[k] > 0) {
                    Title[] titles = player.getTitles();
                    for(Title t : titles) {
                        if(t.getSkillId() != -1) {
                            salary += Config.kingdomSkillSalary[k] + t.getTitleType().id * Config.kingdomMasterSalary[k];
                        }
                    }
                }
            }
            if(salary < 0) salary = 0;
        }
        return salary;
    }

    public boolean paySalary(Player player, int salary) {
        if(salary <= 0) return false;
        try {
            player.addMoney(salary);
        } catch(IOException e) {
            logger.log(Level.WARNING, "Failed paying salary: " + e.getMessage(), e);
            return false;
        }
        return true;
    }

    public void addMoney(Player player, long money, boolean tariff) {
        if(money <= 0L) return;
        final Shop kingsMoney = Economy.getEconomy().getKingsShop();
        try {
            long tax = getTax(money, tariff);
            player.addMoney(money - tax);
            payTax(player, tax);
        } catch(IOException e) {
            logger.log(Level.WARNING, "Error adding money to bank: " + e.getMessage(), e);
        }
    }

    public void payTax(Player player, long tax) {
        if(tax <= 0L) return;
        final Shop kingsMoney = Economy.getEconomy().getKingsShop();
        kingsMoney.setMoney(kingsMoney.getMoney() + tax);
        if(player != null) {
            player.getCommunicator().sendNormalServerMessage("You pay a kingdom tax of " + new Change(tax).getChangeString() + ".");
        }
    }

    public void addTransaction(Connection con, Player player, int templateId, int type, int category, Creature responder, float ql, float amount, int weight, int material, int aux, long price, boolean tariff) throws SQLException {
        long tax = 0L;
        if(price > 0L) {
            tax = getTax(price, tariff);
            totalIncome += price;
            totalTax += tax;
            yearIncome += price;
            yearTax += tax;
            playerData.changed = true;
        }
        PreparedStatement ps = con.prepareStatement("INSERT INTO TRANSACTIONS (ID,WURMID,TEMPLATEID,TYPE,CATEGORY,RESPONDER,DATE,QUALITYLEVEL,AMOUNT,WEIGHT,POSX,POSY,MATERIAL,AUX,PRICE,TAX) VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setLong(1, player.getWurmId());
        ps.setInt(2, templateId);
        ps.setInt(3, 1);
        ps.setInt(4, category);
        ps.setLong(5, responder.getWurmId());
        ps.setLong(6, System.currentTimeMillis());
        ps.setFloat(7, ql);
        ps.setFloat(8, amount);
        ps.setInt(9, weight);
        ps.setFloat(10, responder.getPosX());
        ps.setFloat(11, responder.getPosY());
        ps.setInt(12, material);
        ps.setInt(13, aux);
        ps.setLong(14, price);
        ps.setLong(15, tax);
        ps.execute();
    }

    public long getTotalIncome() {
        return totalIncome;
    }

    public long getTotalTax() {
        return totalTax;
    }

    public long getYearIncome() {
        return yearIncome;
    }

    public long getYearTax() {
        return yearTax;
    }

    @SuppressWarnings("unused")
    public long getTotalUpkeep() {
        return totalUpkeep;
    }
}
