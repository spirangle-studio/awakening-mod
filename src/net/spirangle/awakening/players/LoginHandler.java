package net.spirangle.awakening.players;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.players.Spawnpoint;
import com.wurmonline.server.steam.SteamId;
import com.wurmonline.server.utils.DbUtilities;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import net.spirangle.awakening.AwakeningMod;
import net.spirangle.awakening.Config;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static net.spirangle.awakening.AwakeningConstants.DAY_AS_MILLIS;


public class LoginHandler {

    private static final Logger logger = Logger.getLogger(LoginHandler.class.getName());

    private static final int SPAWNPOINT_DEV_X = 1316;
    private static final int SPAWNPOINT_DEV_Y = 946;

    private static final int SPAWNPOINT_AWAKENING_X = 1456;
    private static final int SPAWNPOINT_AWAKENING_Y = 1435;
    private static final int SPAWNPOINT_TIRVALON_X = 1464;
    private static final int SPAWNPOINT_TIRVALON_Y = 1519;
    private static final int SPAWNPOINT_CELLIMDAR_X = 1891;
    private static final int SPAWNPOINT_CELLIMDAR_Y = 3225;
    private static final int SPAWNPOINT_SHODROQ_X = 3389;
    private static final int SPAWNPOINT_SHODROQ_Y = 1361;
    private static final int SPAWNPOINT_THEKANDRE_X = 2970;
    private static final int SPAWNPOINT_THEKANDRE_Y = 2660;

    private static LoginHandler instance = null;

    public static LoginHandler getInstance() {
        if(instance == null) instance = new LoginHandler();
        return instance;
    }

    private final Map<Long, PlayerInfo> mainBySteamId;

    private LoginHandler() {
        mainBySteamId = new HashMap<>();
    }

    @SuppressWarnings("unused")
    public static byte playerSpawnKingdom(Player player, byte kingdom) {
        if(kingdom < 1 || kingdom > 4) kingdom = (byte) (1 + Server.rand.nextInt(4));
        return kingdom;
    }

    @SuppressWarnings("unused")
    public static String playerLoginTest(final String name, final String password, final String steamIDAsString) {
        final SteamId steamId = SteamId.fromAnyString(steamIDAsString);
        logger.info("LoginHandler: name=" + name + ", password=" + password + ", steamIDAsString=" + steamIDAsString + ", steamId=" + steamId.getSteamID64());
        PlayerInfo playerInfo = PlayerInfoFactory.getPlayerInfoWithName(name);
        if(playerInfo == null) return null;
        try {
            playerInfo.load();
            if(playerInfo.getPower() >= MiscConstants.POWER_DEMIGOD) return null;
        } catch(IOException ioe) { }

        PlayerInfo main = getInstance().getMainPlayerInfo(playerInfo, steamId);
        if(playerInfo.wurmId != main.wurmId) {
            if(main.getPower() >= MiscConstants.POWER_DEMIGOD) return null;
            if(playerInfo.getChaosKingdom() != main.getChaosKingdom()) {
                PlayerData pd = PlayersData.getInstance().get(playerInfo.wurmId);
                if(pd.isKingdomChangeLoginDone()) {
                    return "You may not login with an alt being citizen of a different kingdom than your main character.";
                }
            }
        }
        return null;
    }

    @SuppressWarnings("unused")
    public static Spawnpoint getInitialSpawnPoint(final byte kingdom) {
        if(Servers.getLocalServerId() == AwakeningMod.DEV_SERVER_ID) {
            return new Spawnpoint(null, (byte) 1, null, (short) SPAWNPOINT_DEV_X, (short) SPAWNPOINT_DEV_Y, true, kingdom);
        }
        if(!Servers.localServer.entryServer || Server.getInstance().isPS()) {
            final Village[] villages = Villages.getPermanentVillages(kingdom);
            if(villages.length > 0) {
                final Village chosen = villages[Server.rand.nextInt(villages.length)];
                int x = chosen.getTokenX();
                int y = chosen.getTokenY();
                if(Servers.getLocalServerId() == AwakeningMod.AWAKENING_SERVER_ID) {
                    if(chosen.kingdom == 1) {
                        x = SPAWNPOINT_TIRVALON_X;
                        y = SPAWNPOINT_TIRVALON_Y;
                    } else if(chosen.kingdom == 2) {
                        x = SPAWNPOINT_CELLIMDAR_X;
                        y = SPAWNPOINT_CELLIMDAR_Y;
                    } else if(chosen.kingdom == 3) {
                        x = SPAWNPOINT_SHODROQ_X;
                        y = SPAWNPOINT_SHODROQ_Y;
                    } else if(chosen.kingdom == 4) {
                        x = SPAWNPOINT_THEKANDRE_X;
                        y = SPAWNPOINT_THEKANDRE_Y;
                    }
                }
                return new Spawnpoint(chosen.getName(), (byte) 1, chosen.getMotto(), (short) x, (short) y, true, chosen.kingdom);
            } else {
                return new Spawnpoint(null, (byte) 1, null, (short) SPAWNPOINT_AWAKENING_X, (short) SPAWNPOINT_AWAKENING_Y, true, kingdom);
            }
        }
        return null;
    }

    @SuppressWarnings("unused")
    public static void sendAltarsToPlayer(final Player player) {
        PlayerData pd = PlayersData.getInstance().get(player);
        if(!pd.hideBeams()) {
            if(Servers.getLocalServerId() == AwakeningMod.DEV_SERVER_ID) {
            } else if(Servers.getLocalServerId() == AwakeningMod.AWAKENING_SERVER_ID) {
                for(Config.LoginEffect loginEffect : Config.loginEffects) {
                    try {
                        sendEffect(player, Items.getItem(loginEffect.wurmId), loginEffect.effect);
                    } catch(NoSuchItemException e) {
                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                }
            }
        }
    }

    public static void sendEffect(final Player player, final Item item, short effect) {
        if(item == null) return;
        player.getCommunicator().sendAddEffect(item.getWurmId(), effect, item.getPosX(), item.getPosY(), item.getPosZ(), (byte) 0);
    }

    @SuppressWarnings("unused")
    public static void sendWho(final Player player, final boolean loggingin) {
        if(player.isUndead()) return;
        final boolean gm = player.getPower() >= MiscConstants.POWER_HERO;
        final boolean cm = player.mayMute();
        final boolean ca = player.isPlayerAssistant();
        final Collection<Player> players = Players.getInstance().getPlayerMap().values();
        final Communicator comm = player.getCommunicator();
        String localServerName = Servers.localServer.name;
        if(localServerName.length() > 1) {
            localServerName = localServerName.toLowerCase();
            localServerName = java.lang.Character.toUpperCase(localServerName.charAt(0)) + localServerName.substring(1);
        }
        int otherServers = 0;
        int epic = 0;
        for(final ServerEntry entry : Servers.getAllServers()) {
            if(entry.EPIC) epic += entry.currentPlayers;
            else if(!entry.isLocal) otherServers += entry.currentPlayers;
        }
        List<Player> sorted = players.stream().sorted((p1, p2) -> {
            if(p1.getKingdomId() != p2.getKingdomId()) return p1.getKingdomId() - p2.getKingdomId();
            return p1.getName().compareTo(p2.getName());
        }).collect(Collectors.toList());
        if(gm || cm) {
            comm.sendSafeServerMessage("These players are online on " + localServerName + ":");
            StringBuilder list = new StringBuilder();
            int i = 0, n = 0, x = 0, k = -1;
            for(final Player p : sorted) {
                if(p.getKingdomId() != k) {
                    if(list.length() > 0) {
                        comm.sendSafeServerMessage(list.toString());
                        list = new StringBuilder();
                    }
                    list.append(p.getKingdomName()).append(": ");
                    k = p.getKingdomId();
                    n = 0;
                }
                String name = getNameWithTitle(p);
                if(!p.isFullyLoaded() && p.getWurmId() != player.getWurmId()) {
                    name += "*";
                    ++x;
                }
                if(n > 0 && list.length() + name.length() + 2 > 200) {
                    comm.sendSafeServerMessage(list.toString());
                    list = new StringBuilder();
                    n = 0;
                }
                if(n > 0 && list.length() > 0) list.append(", ");
                list.append(name);
                ++n;
                ++i;
            }
            if(n > 0 && list.length() > 0) {
                comm.sendSafeServerMessage(list.toString());
            }
            i -= x;
            comm.sendSafeServerMessage((i - 1) + " other player" + (i > 1? "s" : "") + " on this server. (" + (i + otherServers + epic) + " totally in Wurm)");
            if(x > 0) comm.sendSafeServerMessage("Names marked with an asterisk * are not completed characters.");
        } else if(players.size() > 1) {
            long n = players.stream().filter(p -> p.isFullyLoaded()).count();
            comm.sendSafeServerMessage((n - 1) + " other player" + (n > 1? "s" : "") + " are online. You are on " + localServerName + " (" + (n + otherServers + epic) + " totally in Wurm).");
        } else {
            comm.sendSafeServerMessage("No other players are online on " + localServerName + " (" + (1 + otherServers) + " totally in Wurm).");
        }
        if(!gm && !cm) {
            int n = 0;
            String gmsMessage = "These staff members are currently online:";
            StringBuilder list = new StringBuilder();
            for(final Player p : sorted) {
                if(p.getPower() >= MiscConstants.POWER_HERO || p.mayMute() || p.isPlayerAssistant()) {
                    String kingdom = p.getKingdomName().substring(0, 3);
                    String name = getNameWithTitle(p) + " (" + kingdom + ")";
                    if(n > 0 && list.length() + name.length() + 2 > 200) {
                        if(gmsMessage != null) {
                            comm.sendSafeServerMessage(gmsMessage);
                            gmsMessage = null;
                        }
                        comm.sendSafeServerMessage(list.toString());
                        list = new StringBuilder();
                    }
                    if(list.length() > 0) list.append(", ");
                    list.append(name);
                    ++n;
                }
            }
            if(n > 0 && list.length() > 0) {
                if(gmsMessage != null)
                    comm.sendSafeServerMessage(gmsMessage);
                comm.sendSafeServerMessage(list.toString());
            }
            if(n == 0) {
                comm.sendSafeServerMessage("There is currently no staff members online. If you have any issue, type your message in the Help chat or open a support ticket (/support <message>).");
            }
            comm.sendSafeServerMessage("These " + player.getKingdomName() + " citizens are currently online:");
            list = new StringBuilder();
            n = 0;
            for(final Player p : sorted) {
                if(p.getKingdomId() != player.getKingdomId() ||
                   (!p.isFullyLoaded() && p.getWurmId() != player.getWurmId())) continue;
                String name = getNameWithTitle(p);
                if(n > 0 && list.length() + name.length() + 2 > 200) {
                    comm.sendSafeServerMessage(list.toString());
                    list = new StringBuilder();
                    n = 0;
                }
                if(n > 0 && list.length() > 0) list.append(", ");
                list.append(name);
                ++n;
            }
            if(n > 0 && list.length() > 0) {
                comm.sendSafeServerMessage(list.toString());
            }
        }
        if(loggingin) {
            if((Config.sendGMLoginMessage && gm) ||
               (Config.sendCMLoginMessage && cm) ||
               (Config.sendCALoginMessage && ca)) {
                Server.getInstance().broadCastSafe(getNameWithTitle(player) + " just logged in.", false);
            }
            PlayerInfo playerInfo = player.getSaveFile();
            PlayerInfo main = getInstance().getMainPlayerInfo(playerInfo, playerInfo.getSteamId());
            if(playerInfo.wurmId != main.wurmId && main.getPower() < MiscConstants.POWER_DEMIGOD) {
                if(playerInfo.getChaosKingdom() != main.getChaosKingdom()) {
                    PlayerData pd = PlayersData.getInstance().get(playerInfo.wurmId);
                    if(pd != null && !pd.isKingdomChangeLoginDone()) {
                        Kingdom mainKingdom = Kingdoms.getKingdom(main.getChaosKingdom());
                        if(mainKingdom != null) {
                            comm.sendAlertServerMessage("The kingdom of " + playerInfo.getName() + " is different from your main character " +
                                                        main.getName() + ". You now have a one time chance to change kingdom to " +
                                                        mainKingdom.getName() + ". If you don't know how, please ask in the help channel or PM staff.");
                            pd.setKingdomChangeLogin(true);
                        }
                    }
                }
            }
        }
    }

    @SuppressWarnings("unused")
    public static void sendPvPList(final Player player) {
        if(player.isUndead()) return;
        final Communicator comm = player.getCommunicator();
        String localServerName = Servers.localServer.name;
        if(localServerName.length() > 1) {
            localServerName = localServerName.toLowerCase();
            localServerName = java.lang.Character.toUpperCase(localServerName.charAt(0)) + localServerName.substring(1);
        }
        long now = System.currentTimeMillis();
        List<PlayerData> pvp = PlayersData.getInstance().getPlayerData().stream().filter(pd -> {
            if(pd.isPvP()) {
                PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithWurmId(pd.wurmId);
                if(pi != null) {
                    try {
                        pi.load();
                        if(pi.lastLogin >= now - 60L * DAY_AS_MILLIS || (pi.lastLogin == 0L && pi.lastLogout >= now - 60L * DAY_AS_MILLIS)) {
                            pd.setKingdom(pi.getChaosKingdom());
                            return true;
                        }
                    } catch(IOException ioe) { }
                }
            }
            return false;
        }).sorted((p1, p2) -> {
            if(p1.getKingdom() != p2.getKingdom()) return p1.getKingdom() - p2.getKingdom();
            return p1.name.compareTo(p2.name);
        }).collect(Collectors.toList());
        if(!pvp.isEmpty()) {
            comm.sendSafeServerMessage("These are active PvP players on " + localServerName + ":");
            StringBuilder list = new StringBuilder();
            int i = 0, n = 0, k = -1;
            Kingdom kingdom = Kingdoms.getKingdom(player.getKingdomId());
            for(final PlayerData pd : pvp) {
                if(pd.getKingdom() != k) {
                    if(list.length() > 0) {
                        comm.sendSafeServerMessage(list.toString());
                        list = new StringBuilder();
                    }
                    list.append(Kingdoms.getNameFor(pd.getKingdom()));
                    if(kingdom.getId() == pd.getKingdom()) list.append(" (home)");
                    else if(kingdom.isAllied(pd.getKingdom())) list.append(" (allied)");
                    else list.append(" (enemy)");
                    list.append(": ");
                    k = pd.getKingdom();
                    n = 0;
                }
                if(n > 0 && list.length() + pd.name.length() + 2 > 200) {
                    comm.sendSafeServerMessage(list.toString());
                    list = new StringBuilder();
                    n = 0;
                }
                if(n > 0 && list.length() > 0) list.append(", ");
                list.append(pd.name);
                ++n;
                ++i;
            }
            if(n > 0 && list.length() > 0) {
                comm.sendSafeServerMessage(list.toString());
            }
        } else {
            comm.sendSafeServerMessage("There are currently no active PvP players on " + localServerName + ".");
        }
    }

    private static String getNameWithTitle(Player player) {
        boolean gm = player.getPower() >= MiscConstants.POWER_HERO;
        boolean cm = player.mayMute();
        boolean ca = player.isPlayerAssistant();
        String title = gm? "GM " : (cm? "CM " : (ca? "CA " : ""));
        return title + player.getName();
    }

    @SuppressWarnings("unused")
    public static int getClientKingdomId(Creature creature) {
        if(creature instanceof Player && creature.getDeity() != null && creature.isPriest())
            return creature.getDeity().getNumber();
        return creature.getKingdomId();
    }

    public PlayerInfo getMainPlayerInfo(final SteamId steamId) {
        return getMainPlayerInfo(null, steamId);
    }

    private PlayerInfo getMainPlayerInfo(final PlayerInfo playerInfo, final SteamId steamId) {
        if(playerInfo == null) return null;
        PlayerInfo main = mainBySteamId.get(steamId.getSteamID64());
        if(main != null) return main;
        else main = playerInfo;
        Connection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = DbConnector.getPlayerDbCon();
            ps = db.prepareStatement("SELECT DISTINCT PLAYER_ID FROM STEAM_IDS WHERE STEAM_ID=? AND PLAYER_ID!=?");
            ps.setLong(1, steamId.getSteamID64());
            ps.setLong(2, playerInfo.wurmId);
            rs = ps.executeQuery();
            while(rs.next()) {
                long wurmId = rs.getLong("PLAYER_ID");
                PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithWurmId(wurmId);
                if(pi == null) continue;
                try {
                    pi.load();
                    if(main == null || pi.getPower() > main.getPower() ||
                       (pi.getPower() == main.getPower() && pi.playingTime > main.playingTime)) main = pi;
                } catch(IOException ioe) { }
            }
        } catch(SQLException e) {
        } finally {
            DbUtilities.closeDatabaseObjects(ps, rs);
            DbConnector.returnConnection(db);
        }
        if(main != null) mainBySteamId.put(steamId.getSteamID64(), main);
        return main;
    }
}
