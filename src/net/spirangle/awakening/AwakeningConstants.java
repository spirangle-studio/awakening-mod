package net.spirangle.awakening;

import com.wurmonline.server.items.ItemList;

public abstract class AwakeningConstants {
    public static final int BML_EDITOR_QUESTION_TYPE = 0x3501;
    public static final int ROBOT_QUESTION_TYPE = 0x3502;

    public static final int MANAGE_SERVANT_QUESTION_TYPE = 0x3701;
    public static final int TALK_TO_SERVANT_QUESTION_TYPE = 0x3702;
    public static final int INSTRUCT_TASK_QUESTION_TYPE = 0x3801;
    public static final int SERVANT_TASK_QUESTION_TYPE = 0x3802;
    public static final int EDIT_SERVANT_TASKS_QUESTION_TYPE = 0x3803;

    public static final int ECONOMY_QUESTION_TYPE = 0x3901;
    public static final int COMBAT_QUESTION_TYPE = 0x3902;
    public static final int LEADERBOARD_QUESTION_TYPE = 0x3903;
    public static final int CHARACTER_QUESTION_TYPE = 0x3904;
    public static final int SETTINGS_QUESTION_TYPE = 0x3905;
    public static final int SPOUSE_QUESTION_TYPE = 0x3906;
    public static final int KINGDOM_INVITATION_QUESTION_TYPE = 0x3907;
    public static final int SELL_MAP_QUESTION_TYPE = 0x3908;
    public static final int MARKET_QUESTION_TYPE = 0x3909;

    public static final long DAY_AS_MILLIS = 86400000L;
    public static final long MONTH_AS_MILLIS = 30L * DAY_AS_MILLIS;

    public static final int TASK_ANNOUNCE = 0x00000101;
    public static final int TASK_MESSAGE = 0x00000102;
    public static final int TASK_MERCHANT = 0x00000201;
    public static final int TASK_VENDOR = 0x00000202;
    public static final int TASK_BARTENDER = 0x00000211;
    public static final int TASK_WAITER = 0x00000212;
    public static final int TASK_CONVERT_FOLLOWER = 0x00000301;
    public static final int TASK_INITIATE_ADEPT = 0x00000302;
    public static final int TASK_KINGDOM_APPOINTMENTS = 0x00000303;
    public static final int TASK_MISSION = 0x00001001;
    public static final int TASK_CHRISTMAS_FOOD = 0x00002001;
    public static final int TASK_HOLIDAY_COIN = 0x00002011;

    public static final byte KENDRALON = 0;
    public static final byte TIRVALON = 1;
    public static final byte CELLIMDAR = 2;
    public static final byte SHODROQ = 3;
    public static final byte THEKANDRE = 4;

    public static final int[] moonMetals = {
            ItemList.seryllBar,
            ItemList.adamantineBar,
            ItemList.glimmerSteelBar
    };
}
