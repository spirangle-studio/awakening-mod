package net.spirangle.awakening;

import com.wurmonline.communication.SocketConnection;
import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.time.Scheduler;
import net.spirangle.awakening.util.ServerChecksum;
import org.gotti.wurmunlimited.modcomm.PacketReader;
import org.gotti.wurmunlimited.modcomm.PacketWriter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.DAY_AS_MILLIS;


public class ClientConnection {

    private static final Logger logger = Logger.getLogger(ClientConnection.class.getName());

    public static final int STATUS_NONE = -1;
    public static final int STATUS_HANDSHAKE_1 = 0;
    public static final int STATUS_HANDSHAKE_2 = 1;
    public static final int STATUS_FAIL_1 = 2;
    public static final int STATUS_FAIL_2 = 3;
    public static final int STATUS_OK = 5;


    private static ClientConnection instance = null;

    public static ClientConnection getInstance() {
        if(instance == null) instance = new ClientConnection();
        return instance;
    }

    private final Map<Integer, Object> hashers;

    private ClientConnection() {
        hashers = new HashMap<>();
    }

    @SuppressWarnings("unused")
    public static void handlePacket(final Player player, final ByteBuffer msg) {
        try {
            final byte type = msg.get();
            switch(type) {
                case 1:
                    handlePacketHandshake(player, msg);
                    break;

                case 2:
                    handlePacketMods(player, msg);
                    break;

                default:
                    logger.log(Level.WARNING, "Unknown packet from player " + player.getName() + " (" + type + ")");
                    break;
            }
        } catch(Exception e) {
            logger.log(Level.WARNING, "Error handling packet from player " + player.getName() + ": " + e.getMessage(), e);
        }
    }

    private static void handlePacketHandshake(final Player player, final ByteBuffer msg) throws IOException {
        final PlayerData pd = PlayersData.getInstance().get(player);
        logger.info("Received handshake from " + player.getName());
        pd.clientModsStatus = STATUS_HANDSHAKE_1;
        try(final PacketWriter writer = new PacketWriter()) {
            writer.writeByte(-101);
            writer.writeByte(1);
            final SocketConnection conn = player.getCommunicator().getConnection();
            final ByteBuffer buff = conn.getBuffer();
            buff.put(writer.getBytes());
            conn.flush();
        }
    }

    private static void handlePacketMods(final Player player, final ByteBuffer msg) throws IOException {
        if(!Config.antiCheatHandling) return;
        final PlayerData pd = PlayersData.getInstance().get(player);
        final PacketReader reader = new PacketReader(msg);
        final byte version = reader.readByte();
        int i = reader.readInt();
        int n = 0;
        logger.info("Received " + i + " client mods from " + player.getName());
        final List<HashMap<String, Object>> mods = new ArrayList<>();
        final StringBuffer forbiddenMods = new StringBuffer();
        while(i-- > 0) {
            final String name = reader.readUTF();
            final String hash = reader.readUTF();
            HashMap<String, Object> mod = new HashMap<>();
            mod.put("name", name);
            mod.put("hash", hash);
            mods.add(mod);
            logger.info("Name: " + name + ", hash: " + hash);
            if(!Config.acceptedClientMods.containsKey(hash)) {
                if(forbiddenMods.length() > 0) forbiddenMods.append(", ");
                forbiddenMods.append(name);
                ++n;
            }
        }
        final String checksum = reader.readUTF();
        final String checksum2 = ServerChecksum.getChecksum(mods);
        logger.info("ServerChecksum: " + checksum + ", result: " + checksum2);
        if(!checksum.equals(checksum2)) {
            player.getCommunicator().sendAlertServerMessage("You are using a version of the Awakening client mod that isn't permitted. " +
                                                            "Please install the latest version and restart the client.");
            player.getCommunicator().sendNormalServerMessage("The Awakening client mod can be found here: https://awakening.spirangle.net/awakening.zip");
            if(Config.antiCheatHandling_informStaff)
                ChatChannels.getInstance().sendStaffMessage(player.getName() + " did not send the correct checksum, could be an old version of the mod or a hacked version.");
            n = -1;
        } else if(n > 0) {
            String fmods = forbiddenMods.toString();
            if(n > 1) {
                player.getCommunicator().sendAlertServerMessage("You have installed a few forbidden client mods: " + fmods + ". " +
                                                                "Please remove them and restart the client.");
            } else if(n == 1) {
                if(fmods.equals("awakening")) n = 0;
                else {
                    player.getCommunicator().sendAlertServerMessage("You have installed the client mod: " + fmods + ". It isn't permitted on the Awakening server. " +
                                                                    "Please remove it and restart the client.");
                }
            }
            if(Config.antiCheatHandling_informStaff)
                ChatChannels.getInstance().sendStaffMessage(player.getName() + " has the following mods installed: " + fmods);
        }
        if(n != 0) {
            pd.clientModsStatus = STATUS_FAIL_1;
        } else {
            player.getCommunicator().sendSafeServerMessage("Your client has been confirmed to have only permitted mods. Have a good day!");
            pd.clientModsStatus = STATUS_OK;
            removeHeavyConscience(player, true);
        }
        pd.handshakeSeconds = 0;
    }

    @SuppressWarnings("unused")
    public static void handleClientModHandshake(final Player player) {
        if(!Config.antiCheatHandling) return;
        final PlayerData pd = PlayersData.getInstance().get(player);
        logger.info("Received Mod Launcher handshake...");
        if(pd.clientModsStatus == STATUS_NONE || pd.clientModsStatus == STATUS_HANDSHAKE_1) {
            pd.clientModsStatus = STATUS_HANDSHAKE_2;
            pd.handshakeSeconds = 0;
        }
    }

    @SuppressWarnings("unused")
    public static void handleClientModPoll(final Player player) {
        if(!Config.antiCheatHandling) return;
        final PlayerData pd = PlayersData.getInstance().get(player);
        ++pd.handshakeSeconds;
        if(pd.handshakeSeconds == 20) {
            logger.info("Handle Client Mod poll... [" + pd.clientModsStatus + "]");
            if(pd.clientModsStatus == STATUS_OK) pd.clientModsStatus = STATUS_NONE;
            else if(pd.clientModsStatus != STATUS_NONE) {
                if(pd.clientModsStatus == STATUS_FAIL_1 || pd.clientModsStatus == STATUS_FAIL_2) {
                    if(player.getPlayingTime() >= DAY_AS_MILLIS) {
                        if(Config.antiCheatHandling_guilt) giveHeavyConscience(player, 0, true, "Client cheat mod");
                    }
                } else {
                    if(player.getPlayingTime() < DAY_AS_MILLIS) {
                        player.getCommunicator().sendSafeServerMessage("You have the client mod launcher installed. Before your newbie buffs run out, make sure to install the Awakening client mod.");
                    } else {
                        player.getCommunicator().sendAlertServerMessage("Please install the Awakening client mod and/or remove forbidden client mods.");
                        player.getCommunicator().sendNormalServerMessage("The Awakening client mod can be found here: https://awakening.spirangle.net/awakening.zip");
                        if(Config.antiCheatHandling_informStaff)
                            ChatChannels.getInstance().sendStaffMessage(player.getName() + " has the client mod launcher installed but not the Awakening client mod.");
                        if(Config.antiCheatHandling_guilt)
                            giveHeavyConscience(player, 0, true, "Missing Awakening client mod");
                    }
                }
                pd.clientModsStatus = STATUS_FAIL_2;
            }
        }
    }

    public static boolean giveHeavyConscience(final Player player, int time, boolean notify, String reason) {
        Item item = player.getInventory().findItem(ItemTemplateCreatorAwakening.heavyConscience, true);
        if(item == null) {
            try {
                if(notify)
                    player.getCommunicator().sendAlertServerMessage("You are carrying a heavy conscience on your mind.");
                item = ItemFactory.createItem(ItemTemplateCreatorAwakening.heavyConscience, 1.0f, MiscConstants.COMMON, player.getName());
                player.getInventory().insertItem(item, true);
                if(time > 0) Scheduler.getInstance().startRemoveGuilt(player, item, time, reason);
                return true;
            } catch(FailedException | NoSuchTemplateException e) { }
        }
        return false;
    }

    public static void removeHeavyConscience(final Player player, boolean notify) {
        Item item = player.getInventory().findItem(ItemTemplateCreatorAwakening.heavyConscience, true);
        if(item != null) {
            if(notify) player.getCommunicator().sendSafeServerMessage("A heavy conscience is lifted from your mind.");
            Items.destroyItem(item.getWurmId());
        }
    }

    @SuppressWarnings("unused")
    private byte[] getHasher() {
        byte[] hasher = (byte[]) hashers.get(0);
        if(hasher == null) {
            try {
                logger.info("Reading hasher...");
                InputStream in = getClass().getResourceAsStream("/net/spirangle/awakening/util/GenericHash.class");
                hasher = new byte[in.available()];
                in.read(hasher);
                hashers.put(0, hasher);
                logger.info("Read data: " + hasher.length);
            } catch(IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return hasher;
    }
}
