package net.spirangle.awakening.actions;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Npc;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.tutorial.MissionTrigger;
import com.wurmonline.server.tutorial.MissionTriggers;
import net.spirangle.awakening.creatures.Servant;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TalkToAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(TalkToAction.class.getName());

    public TalkToAction() {
        // 25=ACTION_TYPE_MISSION
        super("Talk", "talking", new int[]{ 25 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Creature target) {
        return getBehavioursFor(performer, null, target);
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Creature target) {
        if(!(performer instanceof Player) || !(target instanceof Npc)) return null;
        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null) {
            if(subject != null) {
                MissionTrigger[] m2 = MissionTriggers.getMissionTriggersWith(subject.getTemplateId(), Actions.TALK, target.getWurmId());
                if(m2.length > 0) return null;
            }
            return getActionEntryList();
        }
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Creature target, short num, float counter) {
        return action(action, performer, null, target, num, counter);
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        if(!(performer instanceof Player) || !(target instanceof Npc)) return true;
        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null) {
            try {
                TalkToServantQuestion ttq = new TalkToServantQuestion(performer, source, servant);
                ttq.sendQuestion();
            } catch(Throwable e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                performer.getCommunicator().sendAlertServerMessage("Something went wrong, please report to a GM.");
            }
        }
        return true;
    }
}
