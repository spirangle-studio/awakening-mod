package net.spirangle.awakening.actions;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.NoSuchTemplateException;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.zones.Treasure;
import net.spirangle.awakening.zones.Treasures;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ActionPropagation;
import org.gotti.wurmunlimited.modsupport.actions.BehaviourProvider;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CombineFragmentsAction implements ModAction, BehaviourProvider, ActionPerformer {

    private static final Logger logger = Logger.getLogger(CombineFragmentsAction.class.getName());

    private final List<ActionEntry> actionEntry;

    public CombineFragmentsAction() {
        actionEntry = Collections.singletonList(Actions.actionEntrys[Actions.COMBINE_FRAGMENT]);
    }

    @Override
    public short getActionId() {
        return Actions.COMBINE_FRAGMENT;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item source, Item target) {
        if(performer.isPlayer() && isCombinableMapFragments(source, target)) return actionEntry;
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        if(!performer.isPlayer()) return this.defaultPropagation(action);

        Communicator communicator = performer.getCommunicator();
        ActionPropagation actionReturnValue = ActionPropagation.CONTINUE_ACTION;
        if(isCombinableMapFragments(source, target)) {
            byte a1 = source.getAuxData();
            byte a2 = target.getAuxData();
            int d1 = source.getData1();
            int d2 = target.getData1();
            int n = 0;
            for(int i = 0; i < 8; ++i) {
                if((d1 & (1 << i)) != 0) ++n;
                if((d2 & (1 << i)) != 0) ++n;
            }
            if((a1 != a2) || ((d1 | d2) != (d1 ^ d2)) || (n > 4)) {
                communicator.sendNormalServerMessage("The map fragments doesn't match, they don't seem to belong to the same map.");
                actionReturnValue = ActionPropagation.FINISH_ACTION;
            } else if(counter == 1.0f) {
                communicator.sendNormalServerMessage("You start to combine the two map fragments.");
                Server.getInstance().broadCastAction(performer.getName() + " starts to combine two map fragments.", performer, 5);
                performer.sendActionControl("Combining map fragments", true, 50);
                performer.getStatus().modifyStamina(-400.0f);
                action.setTimeLeft(50);
            } else if(counter * 10.0f > action.getTimeLeft()) {
                Treasures treasures = Treasures.getInstance();
                if(n < 4) {
                    source.setData1(d1 | d2);
                    source.setWeight(n * 10, true);
                    if(target.rarity > source.rarity) source.setRarity(target.rarity);
                    treasures.removeMapFragment(target);
                    communicator.sendNormalServerMessage("You combine the fragments, but still need to find " +
                                                         (n == 2? "two more map fragments" : "another map fragment") +
                                                         " for a complete treasure map.");
                    Server.getInstance().broadCastAction(performer.getName() + " combines two map fragments.", performer, 5);
                } else {
                    try {
                        float ql = (source.getQualityLevel() + target.getQualityLevel()) * 0.5f;
                        byte rarity = action.getRarity();
                        if(source.rarity > rarity) rarity = source.rarity;
                        if(target.rarity > rarity) rarity = target.rarity;
                        Item map = ItemFactory.createItem(ItemTemplateCreatorAwakening.treasureMap, ql, rarity, performer.getName());
                        if(map != null) {
                            map.setAuxData(a1);
                            Treasure treasure = treasures.createRandomTreasure(map);
                            if(treasure != null) {
                                map.setData(treasure.getChestId());
                                Item container;
                                try {
                                    container = target.getParent();
                                } catch(Exception e) {
                                    container = performer.getInventory();
                                }
                                container.insertItem(map, true);
                                treasures.removeMapFragment(source);
                                treasures.removeMapFragment(target);

                                communicator.sendNormalServerMessage("You combine the fragments into a treasure map.");
                                Server.getInstance().broadCastAction(performer.getName() + " combines fragments into a treasure map.", performer, 5);
                            } else {
                                Items.destroyItem(map.getWurmId());
                                communicator.sendNormalServerMessage("You could not get the map fragments to fit, perhaps you should try again.");
                            }
                        }
                    } catch(NoSuchTemplateException | FailedException e) {
                        logger.log(Level.SEVERE, "Failed to create map: " + e.getMessage(), e);
                    }
                }
                actionReturnValue = ActionPropagation.FINISH_ACTION;
            }
        } else {
            return this.defaultPropagation(action);
        }
        return this.propagate(action, actionReturnValue, ActionPropagation.NO_SERVER_PROPAGATION, ActionPropagation.NO_ACTION_PERFORMER_PROPAGATION);
    }

    private boolean isCombinableMapFragments(Item source, Item target) {
        int t1 = source.getTemplate().getTemplateId();
        int t2 = target.getTemplate().getTemplateId();
        return t1 == ItemTemplateCreatorAwakening.mapFragment && t2 == ItemTemplateCreatorAwakening.mapFragment;
    }
}
