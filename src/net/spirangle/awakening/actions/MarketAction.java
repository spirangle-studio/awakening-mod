package net.spirangle.awakening.actions;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.MarketQuestion;
import net.spirangle.awakening.tasks.MerchantTask;
import net.spirangle.awakening.tasks.VendorTask;

import java.util.List;


public class MarketAction extends AwakeningAction {

    public MarketAction() {
        super("Market", "checking ware", new int[]{ 0 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item source, Item target) {
        return this.getBehavioursFor(performer, target);
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item target) {
        if(performer instanceof Player && target != null) {
            int merchantWare = MerchantTask.getItemCategory(target.getTemplate().getTemplateId());
            if(merchantWare != MerchantTask.CATEGORY_NONE) return getActionEntryList();
            int vendorWare = VendorTask.getItemCategory(target);
            if(vendorWare != VendorTask.CATEGORY_NONE) return getActionEntryList();
        }
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Item target, short num, float counter) {
        return this.action(action, performer, null, target, num, counter);
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        if(performer.isPlayer()) {
            ItemTemplate template = target.getTemplate();
            MarketQuestion question = new MarketQuestion((Player) performer, template);
            question.sendQuestion();
        }
        return true;
    }
}
