package net.spirangle.awakening.actions;

import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.Skill;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class WriteTaskAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(WriteTaskAction.class.getName());

    public WriteTaskAction() {
        // 36=ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
        super("Write task", "writetask", new int[]{ 36 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Item target) {
        if((performer instanceof Player) &&
           subject.getTemplateId() == ItemList.reedPen &&
           isPaper(target.getTemplateId())) return getActionEntryList();
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        Communicator communicator = performer.getCommunicator();
        Item liquid = WriteTaskAction.testPenPaperInk(performer, source, target);
        if(liquid == null) return true;
        if(counter == 1.0f) {
            communicator.sendNormalServerMessage("You start to write a task instruction on the " + target.getActualName() + ".");
            Server.getInstance().broadCastAction(performer.getName() + " starts to write a task instruction.", performer, 5);
            performer.sendActionControl("Writing task", true, 50);
            performer.getStatus().modifyStamina(-400.0f);
            action.setTimeLeft(50);
        } else if(counter * 10.0f > action.getTimeLeft()) {
            Skill soulDepth = performer.getSoulDepth();
            Skill mindLogic = performer.getMindLogical();
            float skillMultiplier = 1.0f;
            double diff = 30.0;
            float alc = 0.0f;
            if(performer.isPlayer()) {
                alc = ((Player) performer).getAlcohol();
                if(alc <= 10.0f) alc = -alc;
            }
            double bonus = Math.max(0.0, soulDepth.skillCheck(soulDepth.getKnowledge(0.0), liquid, 0.0, false, 1.0f));
            float power = (float) mindLogic.skillCheck(Math.max(1.0, diff + alc), source, bonus, false, skillMultiplier);
            float ql = Math.min(target.getQualityLevel(), Math.max(1.0f, power/*knowledge+modify*/));
            communicator.sendNormalServerMessage("You carefully finish writing the task instruction and sign it.");
            Server.getInstance().broadCastAction(performer.getName() + " stops writing.", performer, 5);
            Item container = null;
            try { container = target.getParent(); } catch(Exception e) { }
            Items.destroyItem(target.getWurmId());

            if(liquid != null) liquid.setWeight(liquid.getWeightGrams() - 10, true);

            if(action.getRarity() != 0) performer.playPersonalSound("sound.fx.drumroll");

            try {
                Item task = ItemFactory.createItem(ItemTemplateCreatorAwakening.servantTask, ql, action.getRarity(), performer.getName());
                if(container == null) container = performer.getInventory();
                container.insertItem(task, true);
            } catch(Exception e) {
                logger.log(Level.SEVERE, "WriteTaskAction: " + e.getMessage(), e);
            }
            return true;
        }
        return false;
    }

    public static Item testPenPaperInk(Creature performer, Item source, Item target) {
        Communicator communicator = performer.getCommunicator();
        Item liquid = null;
        if(source == null) {
            communicator.sendNormalServerMessage("You fumble with the " + target.getName() + " but you cannot figure out how it works.");
        } else {
            int aux = target.getAuxData();
            if(!target.canHaveInscription() || aux == 2 || aux > 8) {
                communicator.sendNormalServerMessage("You cannot write a task instruction on that!");
            } else {
                Set<Item> items = source.getItems();
                for(Item i : items) {
                    if(!isValidColorant(i.getTemplateId())) {
                        communicator.sendNormalServerMessage("You cannot write a task instruction with " + i.getName() + ". You need to use something special for writing on " + target.getName() + ".");
                    } else {
                        liquid = i;
                        break;
                    }
                }
                if(liquid == null) {
                    communicator.sendNormalServerMessage("You need a colorant to be able to write a task instruction with the " + source.getName() + " on the " + target.getName() + ".");
                }
            }
        }
        return liquid;
    }

    private static boolean isPaper(int templateId) {
        switch(templateId) {
            case ItemList.papyrusSheet:
            case ItemList.paperSheet:
                return true;
            default:
                return false;
        }
    }

    private static boolean isValidColorant(int templateId) {
        switch(templateId) {
            case ItemList.dyeBlack:
            case ItemList.dyeWhite:
            case ItemList.dyeRed:
            case ItemList.dyeBlue:
            case ItemList.dyeGreen:
            case ItemList.dye:
            case ItemList.ink:
                return true;
            default:
                return false;
        }
    }
}
