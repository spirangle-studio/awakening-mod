package net.spirangle.awakening.actions;

import com.wurmonline.server.Items;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.behaviours.MethodsCreatures;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.endgames.EndGameItems;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.KingdomInvitationQuestion;
import net.spirangle.awakening.zones.Portals;
import net.spirangle.awakening.zones.Treasure;
import net.spirangle.awakening.zones.Treasures;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ActionPropagation;
import org.gotti.wurmunlimited.modsupport.actions.BehaviourProvider;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static net.spirangle.awakening.items.ItemTemplateCreatorAwakening.diplomaticPassport;
import static net.spirangle.awakening.items.ItemTemplateCreatorAwakening.treasureMap;
import static org.gotti.wurmunlimited.modsupport.actions.ActionPropagation.*;


public class UseAction implements ModAction, BehaviourProvider, ActionPerformer {

    private static final Logger logger = Logger.getLogger(UseAction.class.getName());

    private final List<ActionEntry> useActionEntry;
    private final List<ActionEntry> readMapActionEntry;

    public UseAction() {
        useActionEntry = Collections.singletonList(Actions.actionEntrys[Actions.USE]);
        readMapActionEntry = Collections.singletonList(new ActionEntry(Actions.USE, "Read map", "reading map"));
    }

    @Override
    public short getActionId() {
        return Actions.USE;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item target) {
        if(!performer.isPlayer()) return null;
        if(target.getTemplate().getTemplateId() == diplomaticPassport) return useActionEntry;
        return null;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item source, Item target) {
        if(!performer.isPlayer()) return null;
        if(target.getTemplate().getTemplateId() == diplomaticPassport) return useActionEntry;
        if(source.getTemplate().getTemplateId() == ItemList.compass &&
           target.getTemplate().getTemplateId() == treasureMap) return readMapActionEntry;
        return null;
    }

    @Override
    public boolean action(Action act, Creature performer, Item target, short action, float counter) {
        return this.action(act, performer, null, target, action, counter);
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        if(!performer.isPlayer()) return this.defaultPropagation(action);

        ActionPropagation actionReturnValue = CONTINUE_ACTION;
        if(target.isEpicPortal() || target.isServerPortal()) {
            Portals.getInstance().usePortal(performer, source, target);
            actionReturnValue = FINISH_ACTION;
        } else if(target.getTemplate().getTemplateId() == diplomaticPassport) {
            Player player = (Player) performer;
            KingdomInvitationQuestion question = new KingdomInvitationQuestion(player, target);
            question.sendQuestion();
            actionReturnValue = FINISH_ACTION;
        } else if(source != null && source.getTemplate().getTemplateId() == ItemList.compass &&
                  target.getTemplate().getTemplateId() == treasureMap) {
            Communicator communicator = performer.getCommunicator();
            if(counter == 1.0f) {
                communicator.sendNormalServerMessage("You start to read the map, using the compass.");
                Server.getInstance().broadCastAction(performer.getName() + " starts reading the map, using " + performer.getHisHerItsString() + " compass.", performer, 5);
                performer.sendActionControl("Reading map map", true, 50);
                performer.getStatus().modifyStamina(-400.0f);
                action.setTimeLeft(50);
            } else if(counter * 10.0f > action.getTimeLeft()) {
                Treasure treasure = Treasures.getInstance().getTreasure(target.getData());
                if(treasure == null) {
                    communicator.sendNormalServerMessage("The map has been ruined, you cannot find the treasure.");
                } else {
                    try {
                        Item chest = Items.getItem(treasure.getChestId());
                        int x = chest.getTileX();
                        int y = chest.getTileY();
                        int mindist = Math.max(Math.abs(x - performer.getTileX()), Math.abs(y - performer.getTileY()));
                        int dir = MethodsCreatures.getDir(performer, x, y);
                        String direction = MethodsCreatures.getLocationStringFor(performer.getStatus().getRotation(), dir, "you");
                        String distance = EndGameItems.getDistanceString(mindist, "treasure", direction, false);
                        if(performer.getPower() >= MiscConstants.POWER_DEMIGOD) distance += " [" + x + "," + y + "]";
                        communicator.sendNormalServerMessage(distance);
                    } catch(NoSuchItemException e) {

                    }
                }
                actionReturnValue = FINISH_ACTION;
            }
        } else {
            return this.defaultPropagation(action);
        }
        return this.propagate(action, actionReturnValue, NO_SERVER_PROPAGATION, NO_ACTION_PERFORMER_PROPAGATION);
    }
}
