package net.spirangle.awakening.actions;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Npc;
import com.wurmonline.server.creatures.SpellEffects;
import com.wurmonline.server.items.*;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.SkillSystem;
import com.wurmonline.server.spells.SpellEffect;
import com.wurmonline.shared.constants.Enchants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.time.Scheduler;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TrickOrTreatAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(TrickOrTreatAction.class.getName());

    private static final String[] noCostumeMessages = {
            "You don't scare me!",
            "That's so last year, to trick or treat without a costume.",
            "Go away, I'm busy.",
            "Aah! That was the ugliest masquerade costume I've ever seen!... oh, you wear no costume?... uh, this is awkward...",
            "Not funny.",
            "Why are you bothering me.",
            "No treat for you.",
            "...",
            };

    private static final String[] costumeMessages = {
            "Whoa! You scared me half to death! Here, take this.",
            "Very scary! I'll give you this treat.",
            "Well, you didn't scare me, I'm pretty tough you see, but it was worth your try.",
            "Please, don't do that again! Phew, my heart is pumping so fast.",
            "Aaah! I'm gonna die! I'm gonna die!... Oh, it's halloween already, wasn't it last week?",
            "Hah, that's a costume, I know it is, you don't trick me! I'm untrickable! Oh alright, have a treat then.",
            };

    private static class HalloweenCandy {
        public String name;
        public int templateId;
        public int recipeId;
        public int realTemplateId;
        public int foodXState;
        public int containerTemplateId;
        public float containerQualityLevel;
        public String containerName;

        public HalloweenCandy(String nm, int tid, int rid, int rtid, int xs, int ctid, float cql, String cnm) {
            name = nm;
            templateId = tid;
            recipeId = rid;
            realTemplateId = rtid;
            foodXState = xs;
            containerTemplateId = ctid;
            containerQualityLevel = cql;
            containerName = cnm;
        }
    }

    private static final HalloweenCandy[] candyTypes = {
            new HalloweenCandy("candied bugs", ItemList.sweet, 1124, ItemList.anyFruit, -1, -1, 0.0f, null),
            new HalloweenCandy("chocolate spider", ItemList.chocolate, 1041, -1, -1, -1, 0.0f, null),
            new HalloweenCandy("marshmallow skull", ItemList.sweet, 1126, ItemList.sugar, -1, -1, 0.0f, null),
            new HalloweenCandy("mint wurm", ItemList.sweet, 1129, ItemList.mint, -1, -1, 0.0f, null),
            new HalloweenCandy("hell nougat", ItemList.sweet, 1127, ItemList.anyFruit, -1, -1, 0.0f, null),
            new HalloweenCandy("blood toffee", ItemList.sweet, 1137, ItemList.anyMilk, -1, -1, 0.0f, null),
            new HalloweenCandy("slimy trifle", ItemList.pudding, 657, -1, -1, -1, 0.0f, null),
            };

    public TrickOrTreatAction() {
        // 25=ACTION_TYPE_MISSION
        super("Trick or treat", "trick or treating", new int[]{ 25 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Creature target) {
        return getBehavioursFor(performer, null, target);
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Creature target) {
        if(!(performer instanceof Player) || !(target instanceof Npc)) return null;
        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null && Scheduler.HALLOWEEN.isNow()) {
            return getActionEntryList();
        }
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Creature target, short num, float counter) {
        return action(action, performer, null, target, num, counter);
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        if(!(performer instanceof Player) || !(target instanceof Npc)) return true;

        logger.info(performer.getName() + " tricks or treats " + target.getName() + ".");

        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null && Scheduler.HALLOWEEN.isNow()) {
            final SpellEffects effs = performer.getSpellEffects();
            if(effs != null) {
                final SpellEffect effect = effs.getSpellEffect(Enchants.CRET_ILLUSION);
                if(effect != null) {
                    effs.removeSpellEffect(effect);
                    String message = costumeMessages[Server.rand.nextInt(costumeMessages.length)];
                    performer.getCommunicator().sendNormalServerMessage(target.getName() + " says: " + message);
                    giveCandyTo(performer, Server.rand.nextInt(candyTypes.length));
                    return true;
                }
            }
        }
        String message = noCostumeMessages[Server.rand.nextInt(noCostumeMessages.length)];
        performer.getCommunicator().sendNormalServerMessage(target.getName() + " says: " + message);
        return true;
    }

    public static Item giveCandyTo(Creature performer, int n) {
        ItemTemplateFactory itf = ItemTemplateFactory.getInstance();
        try {
            HalloweenCandy hc = candyTypes[n];
            ItemTemplate template = itf.getTemplate(hc.templateId);
            byte material = template.getMaterial();
            Recipe recipe = Recipes.getRecipeById((short) hc.recipeId);
            Item item = ItemFactory.createItem(template.getTemplateId(), 100.0f, material, (byte) 0, null);
            if(hc.name != null) item.setName(hc.name);
            byte stages = 20;
            byte ingredients = 20;
            if(item.isLiquid()) {
                stages = 10;
                ingredients = 10;
            }
            if(recipe != null) {
                short calories = template.getCalories();
                short carbs = template.getCarbs();
                short fats = template.getFats();
                short proteins = template.getProteins();
                byte bonus = (byte) Server.rand.nextInt(SkillSystem.getNumberOfSkillTemplates());
                ItemMealData.save(item.getWurmId(), recipe.getRecipeId(), calories, carbs, fats, proteins, bonus, stages, ingredients);
            }
            if(hc.realTemplateId != -1) {
                ItemTemplate rit = itf.getTemplate(hc.realTemplateId);
                if(rit != null) item.setRealTemplate(rit.getTemplateId());
            }
            if(hc.foodXState != -1) {
                item.setAuxData((byte) hc.foodXState);
            }
            item.setWeight(100, true);
            performer.getInventory().insertItem(item, true);
            return item;
        } catch(NoSuchTemplateException | FailedException e) {
            logger.log(Level.SEVERE, "Failed creating Haloween candy: " + e.getMessage(), e);
        }
        return null;
    }
}
