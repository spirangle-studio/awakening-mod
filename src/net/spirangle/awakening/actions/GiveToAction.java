package net.spirangle.awakening.actions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Npc;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.InstructTaskQuestion;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;

import java.util.List;
import java.util.logging.Logger;


public class GiveToAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(GiveToAction.class.getName());

    public GiveToAction() {
        // 25=ACTION_TYPE_MISSION, 33=ACTION_TYPE_BLOCKED_ALL_BUT_OPEN
        super("Give", "giving", new int[]{ 25, 33 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Creature target) {
        if(!(performer instanceof Player) || subject.isNoDrop() || !(target instanceof Npc) || performer.getPower() >= MiscConstants.POWER_DEMIGOD)
            return null;
        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null)
            return getActionEntryList();
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        if(!(performer instanceof Player) || source.isNoDrop() || !(target instanceof Npc) || performer.getPower() >= MiscConstants.POWER_DEMIGOD)
            return true;
        Communicator communicator = performer.getCommunicator();
        if(source.isTraded()) {
            communicator.sendNormalServerMessage("You cannot give an item while it's part of a trade.");
            return true;
        }
        logger.info(performer.getName() + " gives " + target.getName() + " the item " + source.getName() + ".");

        Servant servant = Servant.getServant(target);
        if(servant != null && servant.contract != null) {
            Item contract = servant.contract;
            if(contract.isTraded()) {
                communicator.sendNormalServerMessage("You cannot give an item while the contract is part of a trade.");
                return true;
            }

            if(servant.handleGiveTo(performer, source))
                return true;

            if(servant.acceptItem(performer, source)) {
                if(source.getTemplateId() == ItemTemplateCreatorAwakening.servantTask) {
                    InstructTaskQuestion question = new InstructTaskQuestion(performer, source, servant);
                    question.sendQuestion();
                } else {
                    communicator.sendNormalServerMessage(target.getName() + " accepts the " + source.getName() + ".");
                    source.putInVoid();
                    target.getInventory().insertItem(source, true);
                    target.wearItems();
                    servant.updateInventory();
                }
            }
        }
        return true;
    }
}
