package net.spirangle.awakening.actions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.EconomyQuestion;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EconomyAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(EconomyAction.class.getName());

    public EconomyAction() {
        super("Economy", "showing economy", new int[]{ 0 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item source, Item object) {
        return this.getBehavioursFor(performer, object);
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item object) {
        if(performer instanceof Player && object != null && (object.getTemplateId() == ItemList.bodyBody || object.getTemplateId() == ItemList.bodyHand)) {
            return getActionEntryList();
        }
        return null;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Creature target) {
        return this.getBehavioursFor(performer, null, target);
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Creature target) {
        if(performer instanceof Player && target != null && target instanceof Player && performer.getPower() >= MiscConstants.POWER_DEMIGOD) {
            return getActionEntryList();
        }
        return null;
    }

    @Override
    public boolean action(Action act, Creature performer, Item target, short action, float counter) {
        showEconomy(performer);
        return true;
    }

    @Override
    public boolean action(Action act, Creature performer, Item source, Item target, short action, float counter) {
        showEconomy(performer);
        return true;
    }

    private void showEconomy(Creature performer) {
        if(performer.isPlayer()) {
            EconomyQuestion question = new EconomyQuestion((Player) performer, EconomyQuestion.SUBJECT_ECONOMY);
            question.sendQuestion();
        }
    }

    @Override
    public boolean action(Action action, Creature performer, Creature target, short num, float counter) {
        return this.action(action, performer, null, target, num, counter);
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        if(performer.isPlayer() && performer.getPower() >= MiscConstants.POWER_DEMIGOD) {
            try {
                EconomyQuestion question = new EconomyQuestion((Player) performer, (Player) target, EconomyQuestion.SUBJECT_ECONOMY);
                question.sendQuestion();
            } catch(Throwable e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                performer.getCommunicator().sendAlertServerMessage("Something went wrong, please report to a GM.");
            }
        }
        return true;
    }
}
