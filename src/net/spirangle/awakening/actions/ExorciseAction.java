package net.spirangle.awakening.actions;

import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillList;
import net.spirangle.awakening.zones.Infestation;
import net.spirangle.awakening.zones.Infestations;

import java.util.List;
import java.util.logging.Logger;

public class ExorciseAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(GiveToAction.class.getName());

    public ExorciseAction() {
        // 4=ACTION_TYPE_FATIGUE, 5=ACTION_TYPE_POLICED
        super("Exorcise", "banishing curse", new int[]{ 4, 5 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Item target) {
        if(performer instanceof Player && subject != null && target != null) {
            int st = subject.getTemplateId();
            if((st == ItemList.sourceCrystal || st == ItemList.sourceSalt) && subject.getActualAuxData() == (byte) 1) {
                int tt = target.getTemplateId();
                if(tt >= ItemList.riftStone1 && tt <= ItemList.riftPlant4) {
                    Infestation infestation = Infestations.getInstance().getInfestation(target);
                    if(infestation != null && !infestation.isCompleted()) return getActionEntryList();
                }
            }
        }
        return null;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        return performExorcism(action, performer, source, target, num, counter);
    }

    private boolean performExorcism(Action action, Creature performer, Item source, Item target, short num, float counter) {
        if(!(performer instanceof Player) || source == null || target == null) {
            return true;
        }
        int st = source.getTemplateId();
        if((st != ItemList.sourceCrystal && st != ItemList.sourceSalt) || source.getActualAuxData() != (byte) 1) {
            return true;
        }
        int tt = target.getTemplateId();
        if(tt < ItemList.riftStone1 || tt > ItemList.riftPlant4) {
            return true;
        }
        Infestation infestation = Infestations.getInstance().getInfestation(target);
        if(infestation == null || infestation.isCompleted()) {
            return true;
        }
        if(counter == 1.0f || counter == 0.0f || action.justTickedSecond()) {
            if(!infestation.canActivateWave()) {
                performer.getCommunicator().sendNormalServerMessage("You cannot perform an exorcism while there still are cursed creatures defending it.");
                return true;
            }
        }
        Skill exorcism = performer.getSkills().getSkillOrLearn(SkillList.EXORCISM);
        int time = 0;
        if(counter == 1.0f) {
            time = (int) Math.max(30.0, 100.0 - exorcism.getKnowledge(source, 0.0));
            action.setTimeLeft(time);
            performer.getCommunicator().sendNormalServerMessage("You start to perform the exorcism ritual.");
            Server.getInstance().broadCastAction(performer.getName() + " starts to perform an exorcism ritual.", performer, 5);
            performer.sendActionControl(getActionEntry().getVerbString(), true, time);
        } else {
            time = action.getTimeLeft();
        }
        if(counter * 10.0f > time) {
            performer.getStatus().modifyStamina(-3000.0f);
            double difficulty = infestation.getPower() * infestation.getPower() * 3.0;
            double power = exorcism.skillCheck(difficulty, source, 0.0, false, counter);
            logger.info("Exorcism power " + power + " against infestation " + difficulty + ".");
            if(st == ItemList.sourceSalt && power <= 0.0) {
                performer.getCommunicator().sendNormalServerMessage("You fail with the exorcism.");
                Server.getInstance().broadCastAction(performer.getName() + " fails with the exorcism.", performer, 5);
                float dmg = (0.5f - source.getRarity() * 0.1f) * source.getDamageModifier();
                source.setDamage(source.getDamage() + dmg);
                logger.info("Damage to " + source.getName() + " " + dmg + ".");
            } else {
                performer.getCommunicator().sendNormalServerMessage("You succeed with the ritual.");
                Server.getInstance().broadCastAction(performer.getName() + " succeeds with the ritual.", performer, 5);
                Items.destroyItem(source.getWurmId());
                infestation.activateWave(performer, power);
            }
            return true;
        }
        return false;
    }
}
