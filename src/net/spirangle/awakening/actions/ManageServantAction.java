package net.spirangle.awakening.actions;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.ManageServantQuestion;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ManageServantAction extends AwakeningAction {

    private static final Logger logger = Logger.getLogger(ManageServantAction.class.getName());

    public ManageServantAction() {
        // 47=ACTION_TYPE_ENEMY_NEVER
        super("Manage servants", "managing servants", new int[]{ 47 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item target) {
        if(!(performer instanceof Player) ||
           target.getTemplateId() != ItemTemplateCreatorAwakening.servantContract || target.isTraded()) return null;
        return getActionEntryList();
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Item target) {
        return getBehavioursFor(performer, target);
    }

    @Override
    public boolean action(Action action, Creature performer, Item target, short num, float counter) {
        if(!(performer instanceof Player) ||
           target.getTemplateId() != ItemTemplateCreatorAwakening.servantContract) return true;
        if(target.isTraded()) {
            performer.getCommunicator().sendNormalServerMessage("You cannot summon a servant while the contract is part of a trade.");
            return true;
        }

        try {
            ManageServantQuestion question = new ManageServantQuestion(performer, target);
            question.sendQuestion();
        } catch(Throwable e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            performer.getCommunicator().sendAlertServerMessage("Something went wrong, please report to a GM.");
        }
        return true;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        return action(action, performer, target, num, counter);
    }
}
