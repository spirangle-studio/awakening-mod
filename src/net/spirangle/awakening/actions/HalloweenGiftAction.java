package net.spirangle.awakening.actions;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import net.spirangle.awakening.creatures.PumpkinKing;

import java.util.List;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.PUMPKIN_KING_CID;


@SuppressWarnings("unused")
public class HalloweenGiftAction extends AwakeningAction {

    public HalloweenGiftAction() {
        super("Ask for gift", "asking", new int[]{ 0 });
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Creature target) {
        if(target.getTemplate().getTemplateId() == PUMPKIN_KING_CID) return getActionEntryList();
        return null;
    }

    @Override
    public List<ActionEntry> getBehavioursFor(Creature performer, Item subject, Creature target) {
        return getBehavioursFor(performer, target);
    }

    @Override
    public boolean action(Action action, Creature performer, Creature target, short num, float counter) {
        if(target.getTemplate().getTemplateId() == PUMPKIN_KING_CID) {
            PumpkinKing.getInstance().giveHalloweenGift(performer, target);
        }
        return true;
    }

    @Override
    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        return action(action, performer, target, num, counter);
    }
}
