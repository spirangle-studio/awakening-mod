package net.spirangle.awakening.items;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.NoSuchEntryException;
import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.combat.Weapon;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.*;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.IconConstants;
import com.wurmonline.shared.constants.ItemMaterials;
import net.spirangle.awakening.Config;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.items.ModItems;
import org.gotti.wurmunlimited.modsupport.items.ModelNameProvider;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ItemTemplateCreatorAwakening {

    private static final Logger logger = Logger.getLogger(ItemTemplateCreatorAwakening.class.getName());

    public static final int bulkChest = 3501;
    public static final int warHammerHead = 3503;
    public static final int diamondLens = 3504;
    public static final int brassTube = 3505;

    public static final int coffeeBean = 3601;
    public static final int coffeeGround = 3602;
    public static final int coffee = 3603;
    public static final int espresso = 3604;
    public static final int peanut = 3621;
    public static final int peanutButter = 3622;
    public static final int tabasco = 3631;
    public static final int toffeeApple = 3641;
    public static final int cocktail = 3651;

    public static final int sedaesSword = 3701;
    public static final int payrenStaff = 3702;
    public static final int eilandaSpear = 3703;
    public static final int shoninSword = 3704;
    public static final int asmanStaff = 3705;
    public static final int kianeStaff = 3706;
    public static final int miridonAxe = 3707;
    public static final int laphanerStaff = 3708;
    public static final int drakkarKnife = 3709;
    public static final int qaleshinStaff = 3710;

    public static final int warHammer = 3711;

    public static final int clayGardenGnome = 3801;
    public static final int clayGardenGnomeGreen = 3802;
    public static final int clayMaskIsles = 3803;
    public static final int clayMidsummerMask = 3804;
    public static final int clayPaleMask = 3805;

    public static final int clayCoffeeCup = 3811;
    public static final int coffeeCup = 3812;

    public static final int mapFragment = 4071;
    public static final int treasureMap = 4072;

    public static final int diplomaticPassport = 4081;

    public static final int servantContract = 4097;
    public static final int servantTask = 4353;

    public static final int minorIllusionCreature = 4501;
    public static final int minorIllusionItem = 4502;

    public static final int heavyConscience = 5001;

    public static final int holidayCoin = 8193;

    private static final int[] colourIndexToTrait = { 0, 15, 16, 17, 18, 24, 25, 23, 30, 31, 32, 33, 34 };

    public static void initItemTemplates() {
        try {

            /* Bulk Chest: */
            final ItemTemplate bc = ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.bulkChest, 3,
                    "bulk chest", "bulk chests", "almost full", "somewhat occupied", "half-full", "emptyish",
                    "A sturdy chest made from planks and strengthened with iron ribbons.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_OWNER_DESTROYABLE,
                            ItemTypes.ITEM_TYPE_NOTAKE,
                            ItemTypes.ITEM_TYPE_WOOD,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_LOCKABLE,
                            ItemTypes.ITEM_TYPE_HOLLOW,
                            ItemTypes.ITEM_TYPE_BULKCONTAINER,
                            ItemTypes.ITEM_TYPE_TRANSPORTABLE,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_PLANTABLE,
                            ItemTypes.ITEM_TYPE_USES_SPECIFIED_CONTAINER_VOLUME
                    },
                    (short) IconConstants.ICON_CONTAINER_CHEST_LARGE,
                    BehaviourList.itemBehaviour, 0, 9072000L, 80, 70, 120, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.furniture.wooden.coffer.", 40.0f, 100000, ItemMaterials.MATERIAL_WOOD_BIRCH, 10000, true, -1);
            bc.setContainerSize(30, 50, 200);
            logger.info("Created template for: bulk chest [" + ItemTemplateCreatorAwakening.bulkChest + "]");

            /* Coffee Items: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.coffeeBean,
                    "coffee bean", "coffee beans", "excellent", "good", "ok", "poor",
                    "A coffee seed, commonly called coffee bean, is a seed of the coffee plant, and is the source for coffee.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_BULK,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION,
                            ItemTypes.ITEM_TYPE_FOOD,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_FOOD_COCOA_BEAN,
                    BehaviourList.itemBehaviour, 0, 28800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.resource.cocoabean.", 8.0f, 200, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: coffee bean [" + ItemTemplateCreatorAwakening.coffeeBean + "]");


            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.coffeeGround,
                    "ground coffee", "ground coffee", "excellent", "good", "ok", "poor",
                    "Ground and roasted coffee bean.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_BULK,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION,
                            ItemTypes.ITEM_TYPE_FOOD,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_LIQUID_TANNIN,
                    BehaviourList.itemBehaviour, 0, 172800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.food.cocoa.", 15.0f, 200, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: ground coffee [" + ItemTemplateCreatorAwakening.coffeeGround + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.coffee,
                    "coffee", "coffee", "excellent", "good", "ok", "poor",
                    "Coffee is a way of stealing time that should by rights belong to your older self.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_LIQUID,
                            ItemTypes.ITEM_TYPE_LIQUID_COOKING,
                            ItemTypes.ITEM_TYPE_LIQUID_DRINKABLE,
                            ItemTypes.ITEM_TYPE_DECAYDESTROYS,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION
                    },
                    (short) IconConstants.ICON_LIQUID_TANNIN,
                    BehaviourList.itemBehaviour, 0, 172800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.liquid.tannin.", 15.0f, 200, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: coffee [" + ItemTemplateCreatorAwakening.coffee + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.espresso,
                    "espresso", "espresso", "excellent", "good", "ok", "poor",
                    "Black as the devil, hot as hell, pure as an angel, sweet as love.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_LIQUID,
                            ItemTypes.ITEM_TYPE_LIQUID_COOKING,
                            ItemTypes.ITEM_TYPE_LIQUID_DRINKABLE,
                            ItemTypes.ITEM_TYPE_DECAYDESTROYS,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION
                    },
                    (short) IconConstants.ICON_LIQUID_TANNIN,
                    BehaviourList.itemBehaviour, 0, 172800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.liquid.tannin.", 35.0f, 1000, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: espresso [" + ItemTemplateCreatorAwakening.espresso + "]");

            /* Peanut: */
            ItemTemplateCreator.createItemTemplate(
                                       ItemTemplateCreatorAwakening.peanut,
                                       "peanut", "peanuts", "excellent", "good", "ok", "poor",
                                       "The peanut, also known as the groundnut and the goober, is a legume crop grown mainly for its edible seeds.",
                                       new short[]{
                                               ItemTypes.ITEM_TYPE_SEED,
                                               ItemTypes.ITEM_TYPE_LOWNUTRITION,
                                               ItemTypes.ITEM_TYPE_BULK,
                                               ItemTypes.ITEM_TYPE_FOOD,
                                               ItemTypes.ITEM_TYPE_FRUIT,
                                               ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                                       },
                                       (short) IconConstants.ICON_FOOD_PINENUT,
                                       BehaviourList.itemBehaviour, 0, 86400L, 3, 4, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                                       "model.food.pinenuts.", 15.0f, 200, ItemMaterials.MATERIAL_VEGETARIAN, 100, false)
                               .setNutritionValues(6730, 131, 684, 137);
            logger.info("Created template for: peanut [" + ItemTemplateCreatorAwakening.peanut + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.peanutButter,
                    "peanut butter", "peanut butter", "excellent", "good", "ok", "poor",
                    "Peanut butter is a food paste or spread made from ground dry roasted peanuts.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_DISH,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION,
                            ItemTypes.ITEM_TYPE_BULK,
                            ItemTypes.ITEM_TYPE_FOOD,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_FOOD_BUTTER,
                    BehaviourList.itemBehaviour, 0, 86400L, 2, 3, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.food.butter.", 15.0f, 200, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: peanut butter [" + ItemTemplateCreatorAwakening.peanutButter + "]");

            /* Tabasco: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.tabasco,
                    "tabasco", "tabasco", "excellent", "good", "ok", "poor",
                    "Tabasco sauce is a brand of hot sauce made exclusively from tabasco peppers.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_LIQUID,
                            ItemTypes.ITEM_TYPE_LIQUID_COOKING,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION,
                            ItemTypes.ITEM_TYPE_FERMENTED,
                            ItemTypes.ITEM_TYPE_DECAYDESTROYS,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_FOOD_KETCHUP,
                    BehaviourList.itemBehaviour, 0, 172800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.resource.passata.", 15.0f, 600, ItemMaterials.MATERIAL_VEGETARIAN, 100, false);
            logger.info("Created template for: tabasco [" + ItemTemplateCreatorAwakening.tabasco + "]");

            /* Toffee Apple: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.toffeeApple,
                    "toffee apple", "toffee apples", "delicious", "nice", "old", "rotten",
                    "Sweet and crunchy apple covered in toffee.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_FOOD,
                            ItemTypes.ITEM_TYPE_LOWNUTRITION,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_FOOD_APPLE_GREEN,
                    BehaviourList.itemBehaviour, 0, 604800L, 5, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.food.apple.green.", 100.0f, 300, ItemMaterials.MATERIAL_VEGETARIAN, 10, true);
            logger.info("Created template for: toffee apple [" + ItemTemplateCreatorAwakening.toffeeApple + "]");

            /* Cocktail: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.cocktail,
                    "cocktail", "cocktails", "excellent", "good", "ok", "poor",
                    "You can't buy happiness, but you can prepare a cocktail and that's kind of the same thing.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_LIQUID,
                            ItemTypes.ITEM_TYPE_LIQUID_COOKING,
                            ItemTypes.ITEM_TYPE_LIQUID_DRINKABLE,
                            ItemTypes.ITEM_TYPE_USES_FOOD_STATE
                    },
                    (short) IconConstants.ICON_LIQUID_FRUITJUICE,
                    BehaviourList.itemBehaviour, 0, 604800L, 2, 5, 5, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.liquid.juice.", 30.0f, 1000, ItemMaterials.MATERIAL_WATER, 100, false);
            logger.info("Created template for: cocktail [" + ItemTemplateCreatorAwakening.cocktail + "]");

            /* Holy items: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.sedaesSword,
                    "witch sword", "swords", "superb", "good", "ok", "poor",
                    "A long and slender sword, given to a witch upon becoming an adept of Sedaes.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_SWORD,
                            ItemTypes.ITEM_TYPE_WEAPON_SLASH,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WEAPON_SWORD_LONG,
                    BehaviourList.itemBehaviour, 40, Long.MAX_VALUE, 1, 6, 110, SkillList.SWORD_LONG, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.swordlong.", 30.0f, 1500, ItemMaterials.MATERIAL_IRON, 100, false);
            logger.info("Created template for: witch sword [" + ItemTemplateCreatorAwakening.sedaesSword + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.payrenStaff,
                    "druid staff", "staves", "fresh", "dry", "brittle", "rotten",
                    "A long staff, given to a druid upon becoming an adept of Payren.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_WOOD,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WOOD_STAFFSAPPHIRE,
                    BehaviourList.itemBehaviour, 45, Long.MAX_VALUE, 3, 4, 150, SkillList.STAFF, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.staffOfLand.", 10.0f, 1500, ItemMaterials.MATERIAL_WOOD_BIRCH, 100, false);
            logger.info("Created template for: druid staff [" + ItemTemplateCreatorAwakening.payrenStaff + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.eilandaSpear,
                    "shaman spear", "spears", "excellent", "good", "ok", "poor",
                    "A long spear, given to a shaman upon becoming an adept of Eilanda.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_PIERCE,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WOOD_STAFFSAPPHIRE,
                    BehaviourList.itemBehaviour, 90, Long.MAX_VALUE, 3, 5, 180, SkillList.SPEAR_LONG, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.polearm.spear.", 40.0f, 1850, ItemMaterials.MATERIAL_STEEL, 100, false);
            logger.info("Created template for: shaman spear [" + ItemTemplateCreatorAwakening.eilandaSpear + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.shoninSword,
                    "paladin sword", "swords", "excellent", "good", "ok", "poor",
                    "An elegant two-handed sword, given to a paladin upon becoming an adept of Shonin.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_SWORD,
                            ItemTypes.ITEM_TYPE_WEAPON_SLASH,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WEAPON_SWORD_TWO,
                    BehaviourList.itemBehaviour, 70, Long.MAX_VALUE, 1, 10, 140, SkillList.SWORD_TWOHANDED, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.swordlong.", 40.0f, 2000, ItemMaterials.MATERIAL_IRON, 100, false);
            logger.info("Created template for: witch sword [" + ItemTemplateCreatorAwakening.sedaesSword + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.asmanStaff,
                    "magus staff", "staves", "excellent", "good", "ok", "poor",
                    "A long staff, given to a magus upon becoming an adept of Asman.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_TOOL_RANGE_POLE,
                    BehaviourList.itemBehaviour, 45, Long.MAX_VALUE, 3, 4, 150, SkillList.STAFF, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.polearm.staff.", 10.0f, 1500, ItemMaterials.MATERIAL_STEEL, 100, false);
            logger.info("Created template for: magus staff [" + ItemTemplateCreatorAwakening.asmanStaff + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.kianeStaff,
                    "mystic staff", "staves", "fresh", "dry", "brittle", "rotten",
                    "A long staff, given to a mystic upon becoming an adept of Kiane.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_WOOD,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WOOD_STAFFDIAMOND,
                    BehaviourList.itemBehaviour, 35, Long.MAX_VALUE, 3, 4, 200, SkillList.STAFF, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.polearm.staff.diamond.", 10.0f, 1000, ItemMaterials.MATERIAL_WOOD_BIRCH, 100, false);
            logger.info("Created template for: mystic staff [" + ItemTemplateCreatorAwakening.kianeStaff + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.miridonAxe, 3,
                    "warlock axe", "axes", "excellent", "good", "ok", "poor",
                    "An elegant axe, given to a warlock upon becoming an adept of Miridon.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_AXE,
                            ItemTypes.ITEM_TYPE_WEAPON_SLASH,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WEAPON_AXE,
                    BehaviourList.itemBehaviour, 30, Long.MAX_VALUE, 4, 10, 80, SkillList.AXE_LARGE, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.axe.medium.", 15.0f, 1000, ItemMaterials.MATERIAL_IRON, 100, false, -1);
            logger.info("Created template for: warlock axe [" + ItemTemplateCreatorAwakening.miridonAxe + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.laphanerStaff,
                    "sorcerer staff", "staves", "fresh", "dry", "brittle", "rotten",
                    "A long staff, given to a sorcerer upon becoming an adept of Laphaner.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_WOOD,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WOOD_STAFFRUBY,
                    BehaviourList.itemBehaviour, 35, Long.MAX_VALUE, 3, 4, 200, SkillList.STAFF, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.polearm.staff.ruby.", 10.0f, 1000, ItemMaterials.MATERIAL_WOOD_BIRCH, 100, false);
            logger.info("Created template for: sorcerer staff [" + ItemTemplateCreatorAwakening.laphanerStaff + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.drakkarKnife,
                    "necromancer knife", "knives", "excellent", "good", "ok", "poor",
                    "A sharp curvy knife, given to a necromancer upon becoming an adept of Drakkar.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_PIERCE,
                            ItemTypes.ITEM_TYPE_WEAPON_KNIFE,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_TOOL_SACRIFICIAL_KNIFE,
                    BehaviourList.itemBehaviour, 1, Long.MAX_VALUE, 1, 3, 21, SkillList.KNIFE_BUTCHERING, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.knife.sacrificial.", 30.0f, 615, ItemMaterials.MATERIAL_SILVER, 100, false);
            logger.info("Created template for: necromancer knife [" + ItemTemplateCreatorAwakening.drakkarKnife + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.qaleshinStaff,
                    "dragonlord staff", "staves", "fresh", "dry", "brittle", "rotten",
                    "A long staff, given to a dragonlord upon becoming an adept of Qaleshin.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_WOOD,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH,
                            ItemTypes.ITEM_TYPE_WEAPON_POLEARM,
                            ItemTypes.ITEM_TYPE_TWOHANDED,
                            ItemTypes.ITEM_TYPE_DISARM_TRAP,
                            ItemTypes.ITEM_TYPE_MAGICAL_STAFF,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE,
                            ItemTypes.ITEM_TYPE_NOSELLBACK
                    },
                    (short) IconConstants.ICON_WOOD_STAFFSAPPHIRE,
                    BehaviourList.itemBehaviour, 35, Long.MAX_VALUE, 3, 4, 200, SkillList.STAFF, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.polearm.staff.sapphire.", 10.0f, 1000, ItemMaterials.MATERIAL_WOOD_BIRCH, 100, false);
            logger.info("Created template for: dragonlord staff [" + ItemTemplateCreatorAwakening.qaleshinStaff + "]");

            /* Weapons & Weapon components: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.warHammerHead,
                    "warhammer head", "warhammer heads", "excellent", "good", "ok", "poor",
                    "The thick, heavy, bronze head for a warhammer.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_METAL
                    },
                    (short) IconConstants.ICON_WEAPON_HAMMER_LARGE_HEAD,
                    BehaviourList.itemBehaviour, 0, 3024000L, 20, 20, 20, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.weapon.head.large.maul.", 35.0f, 4000, ItemMaterials.MATERIAL_BRONZE, 100, false);

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.warHammer,
                    "warhammer", "warhammers", "excellent", "good", "ok", "poor",
                    "A huge brutal warhammer made totally from bronze.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_METAL,
                            ItemTypes.ITEM_TYPE_WEAPON,
                            ItemTypes.ITEM_TYPE_WEAPON_CRUSH
                    },
                    (short) IconConstants.ICON_ARTIFACT_HAMMER_MAGRANON,
                    BehaviourList.itemBehaviour, 65, Long.MAX_VALUE, 5, 10, 80, SkillList.WARHAMMER, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.artifact.hammerhuge.", 50.0f, 5000, ItemMaterials.MATERIAL_BRONZE, 10000, true);
            logger.info("Created template for: warhammer [" + ItemTemplateCreatorAwakening.warHammer + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayGardenGnome,
                    "clay red garden gnome", "clay gnomes", "almost full", "somewhat occupied", "half-full", "emptyish",
                    "A small serious gnome stands here. It could be hardened by fire.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_HOLLOW,
                            ItemTypes.ITEM_TYPE_NOPUT,
                            ItemTypes.ITEM_TYPE_CONTAINER_LIQUID
                    },
                    (short) IconConstants.ICON_ICON_UNFINISHED_ITEM,
                    BehaviourList.itemBehaviour, 0, 172800L, 10, 10, 40, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.decoration.statue.garden.", 75.0f, 20000, ItemMaterials.MATERIAL_CLAY);
            logger.info("Created template for: clay red garden gnome [" + ItemTemplateCreatorAwakening.clayGardenGnome + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayGardenGnomeGreen,
                    "clay green garden gnome", "clay gnomes", "almost full", "somewhat occupied", "half-full", "emptyish",
                    "A small serious gnome stands here. It could be hardened by fire.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_HOLLOW,
                            ItemTypes.ITEM_TYPE_NOPUT,
                            ItemTypes.ITEM_TYPE_CONTAINER_LIQUID
                    },
                    (short) IconConstants.ICON_ICON_UNFINISHED_ITEM,
                    BehaviourList.itemBehaviour, 0, 172800L, 10, 10, 40, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.decoration.statue.garden.green.", 90.0f, 20000, ItemMaterials.MATERIAL_CLAY);
            logger.info("Created template for: clay green garden gnome [" + ItemTemplateCreatorAwakening.clayGardenGnomeGreen + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayMaskIsles,
                    "clay mask of the isles", "clay masks", "excellent", "good", "ok", "poor",
                    "A clay mask.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_NOPUT
                    },
                    (short) IconConstants.ICON_ICON_UNFINISHED_ITEM,
                    BehaviourList.itemBehaviour, 0, 172800L, 1, 10, 20, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.armour.head.mask.isles.", 50.0f, 200, ItemMaterials.MATERIAL_CLAY);
            logger.info("Created template for: clay mask of the isles [" + ItemTemplateCreatorAwakening.clayMaskIsles + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayMidsummerMask,
                    "clay mask of rebirth", "clay masks", "excellent", "good", "ok", "poor",
                    "A clay mask.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_NOPUT
                    },
                    (short) IconConstants.ICON_ICON_UNFINISHED_ITEM,
                    BehaviourList.itemBehaviour, 0, 172800L, 1, 10, 20, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.armour.head.mask.midsummer.", 60.0f, 200, ItemMaterials.MATERIAL_CLAY);
            logger.info("Created template for: clay mask of rebirth [" + ItemTemplateCreatorAwakening.clayMidsummerMask + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayPaleMask,
                    "clay pale mask", "clay masks", "excellent", "good", "ok", "poor",
                    "A clay mask.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_NOPUT
                    },
                    (short) IconConstants.ICON_ICON_UNFINISHED_ITEM,
                    BehaviourList.itemBehaviour, 0, 172800L, 1, 10, 20, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.armour.head.mask.pale.", 90.0f, 200, ItemMaterials.MATERIAL_CLAY);
            logger.info("Created template for: clay pale mask [" + ItemTemplateCreatorAwakening.clayPaleMask + "]");

            /* Coffee Cup: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.clayCoffeeCup,
                    "clay coffee cup", "clay coffee cups", "excellent", "good", "ok", "poor",
                    "A porcelain cup for drinking coffee in, with a saucer. Needs to be fired.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_UNFIRED,
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_NOPUT
                    },
                    (short) IconConstants.ICON_BEER_STEIN_CLAY,
                    BehaviourList.itemBehaviour, 0, 172800L, 17, 11, 15, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.coffeeCup.clay.", 65.0f, 300, ItemMaterials.MATERIAL_BONE);
            logger.info("Created template for: clay coffee cup [" + ItemTemplateCreatorAwakening.clayCoffeeCup + "]");

            ItemTemplate cc = ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.coffeeCup,
                    "coffee cup", "coffee cups", "excellent", "good", "ok", "poor",
                    "A porcelain cup for drinking coffee in, with a saucer.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_POTTERY,
                            ItemTypes.ITEM_TYPE_HOLLOW,
                            ItemTypes.ITEM_TYPE_CONTAINER_LIQUID,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_MISSION
                    },
                    (short) IconConstants.ICON_BEER_STEIN,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 17, 11, 15, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.coffeeCup.pottery.", 200.0f, 300, ItemMaterials.MATERIAL_BONE);
            cc.setContainerSize(5, 10, 5);
            logger.info("Created template for: coffee cup [" + ItemTemplateCreatorAwakening.coffeeCup + "]");

            /* Spyglass components: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.diamondLens,
                    "lens", "lenses", "excellent", "good", "ok", "poor",
                    "A lens made from a polished diamond.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_NO_IMPROVE,
                            },
                    (short) IconConstants.ICON_DECO_GEM_DIAMOND,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1, 1, 1, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.decoration.gem.diamond.", 70.0f, 100, ItemMaterials.MATERIAL_DIAMOND, 100000, false);
            logger.info("Created template for: diamond lens [" + ItemTemplateCreatorAwakening.diamondLens + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.brassTube,
                    "tube", "tubes", "excellent", "good", "ok", "poor",
                    "A tube made from brass.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_REPAIRABLE,
                            ItemTypes.ITEM_TYPE_MISSION,
                            ItemTypes.ITEM_TYPE_METAL,
                            },
                    (short) IconConstants.ICON_TOOL_SPYGLASS,
                    BehaviourList.itemBehaviour, 0, 2419200L, 3, 5, 40, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.decoration.statuette.", 50.0f, 2000, ItemMaterials.MATERIAL_BRASS, 500, false);
            logger.info("Created template for: brass tube [" + ItemTemplateCreatorAwakening.brassTube + "]");


            /* Treasure Map: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.mapFragment,
                    "map fragment", "map fragments", "new", "fancy", "ok", "old",
                    "This piece of paper looks like part of a map.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_SERVERBOUND,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_LOADED
                    },
                    (short) IconConstants.ICON_SCROLL_TEXT,
                    BehaviourList.itemBehaviour, 0, 3024000L, 1, 10, 10, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.writ.", 100.0f, 10, ItemMaterials.MATERIAL_PAPER, 100, true);
            logger.info("Created template for: map fragment [" + ItemTemplateCreatorAwakening.mapFragment + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.treasureMap,
                    "treasure map", "treasure maps", "new", "fancy", "ok", "old",
                    "This map clearly points out a location, perhaps it could lead to a treasure?",
                    new short[]{
                            ItemTypes.ITEM_TYPE_SERVERBOUND,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_LOADED
                    },
                    (short) IconConstants.ICON_SCROLL_TEXT,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1, 10, 10, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.writ.", 100.0f, 40, ItemMaterials.MATERIAL_PAPER, 100, true);
            logger.info("Created template for: treasure map [" + ItemTemplateCreatorAwakening.treasureMap + "]");


            /* Diplomatic Pass: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.diplomaticPassport,
                    "diplomatic passport", "diplomatic passports", "new", "fancy", "ok", "old",
                    "When used it grants diplomatic immunity in one kingdom for a limited amount of time.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_SERVERBOUND,
                            ItemTypes.ITEM_TYPE_FULLPRICE,
                            ItemTypes.ITEM_TYPE_LOADED
                    },
                    (short) IconConstants.ICON_SCROLL_TEXT,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1, 10, 10, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.writ.", 100.0f, 0, ItemMaterials.MATERIAL_PAPER, 50000, true);
            logger.info("Created template for: diplomatic pass [" + ItemTemplateCreatorAwakening.diplomaticPassport + "]");

            /* Servant items: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.servantContract,
                    "personal servant contract", "servant contracts", "new", "fancy", "ok", "old",
                    "A contract declaring the rights for a person called a servant to perform tasks on your behalf.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_SERVERBOUND,
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_FULLPRICE,
                            ItemTypes.ITEM_TYPE_LOADED
                    },
                    (short) IconConstants.ICON_TRADER_CONTRACT,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1, 10, 10, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.writ.merchant.", 100.0f, 0, ItemMaterials.MATERIAL_PAPER, 100000, true);
            logger.info("Created template for: personal servant contract [" + ItemTemplateCreatorAwakening.servantContract + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.servantTask,
                    "servant task", "servant tasks", "new", "fancy", "ok", "old",
                    "An instruction for a task, which is given to a servant to perform.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_SERVERBOUND,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_LOADED
                    },
                    (short) IconConstants.ICON_SCROLL_TEXT,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1, 10, 10, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.writ.", 100.0f, 0, ItemMaterials.MATERIAL_PAPER, 100, false);
            logger.info("Created template for: servant task [" + ItemTemplateCreatorAwakening.servantTask + "]");

            /* Minor illusion: */
            ModelNameProvider minorIllusionModelNameProvider = item -> {
                byte aux = item.getAuxData();
                int d1 = item.getData1();
                int d2 = item.getData2();
                if(aux == 0) {
                    try {
                        int t = d1 & 0xFFFF;
                        CreatureTemplate ct = CreatureTemplateFactory.getInstance().getTemplate(t);
                        String model = ct.getModelName() + ".";
                        if(t == CreatureTemplateIds.HORSE_CID || t == CreatureTemplateIds.FOAL_CID ||
                           t == CreatureTemplateIds.HELL_HORSE_CID || t == CreatureTemplateIds.HELL_FOAL_CID ||
                           t == CreatureTemplateIds.UNICORN_CID || t == CreatureTemplateIds.UNICORN_FOAL_CID) {
                            model += ct.getTemplateColourName(colourIndexToTrait[d2 >> 16]) + ".";
                        } else if(model.equals("model.creature.humanoid.human.player.")) {
                            model = "model.creature.humanoid.human.player.zombie.";
                        }
                        if(item.female) model += "female.";
                        else model += "male.";
                        byte kingdom = (byte) (d1 >> 16);
                        Kingdom k = Kingdoms.getKingdom(kingdom);
                        if(k != null && k.getTemplate() != kingdom) model += Kingdoms.getSuffixFor(k.getTemplate());
                        model += Kingdoms.getSuffixFor(kingdom);
                        return model;
                    } catch(NoSuchCreatureTemplateException e) { }
                } else if(aux == 1) {
                    try {
                        ItemTemplate template = ItemTemplateFactory.getInstance().getTemplate(d2);
                        return template.getModelName();
                    } catch(NoSuchTemplateException e) { }
                }
                return "model.light.token.";
            };
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.minorIllusionCreature,
                    "minor illusion", "minor illusions", "new", "fancy", "ok", "old",
                    "A miniature illusion of a once living creature.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_PLANTABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_NORENAME
                    },
                    (short) IconConstants.ICON_MISC_HANDMIRROR,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 2, 2, 2, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.light.token.", 100.0f, 0, ItemMaterials.MATERIAL_MAGIC, 1, false);
            ModItems.addModelNameProvider(ItemTemplateCreatorAwakening.minorIllusionCreature, minorIllusionModelNameProvider);
            logger.info("Created template for: minor creature illusion [" + ItemTemplateCreatorAwakening.minorIllusionCreature + "]");

            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.minorIllusionItem,
                    "minor illusion", "minor illusions", "new", "fancy", "ok", "old",
                    "An illusion of an item, produced by a spell.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_NAMED,
                            ItemTypes.ITEM_TYPE_HASDATA,
                            ItemTypes.ITEM_TYPE_PLANTABLE,
                            ItemTypes.ITEM_TYPE_DECORATION,
                            ItemTypes.ITEM_TYPE_TURNABLE,
                            ItemTypes.ITEM_TYPE_NORENAME
                    },
                    (short) IconConstants.ICON_MISC_HANDMIRROR,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 2, 2, 2, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.light.token.", 100.0f, 0, ItemMaterials.MATERIAL_MAGIC, 1, false);
            ModItems.addModelNameProvider(ItemTemplateCreatorAwakening.minorIllusionItem, minorIllusionModelNameProvider);
            logger.info("Created template for: minor item illusion [" + ItemTemplateCreatorAwakening.minorIllusionItem + "]");

            /* Heavy conscience: */
            ItemTemplateCreator.createItemTemplate(
                    ItemTemplateCreatorAwakening.heavyConscience,
                    "heavy conscience", "heavy consciences", "excellent", "good", "ok", "poor",
                    "The weight of your heavy conscience is making your knees buckle.",
                    new short[]{
                            ItemTypes.ITEM_TYPE_INDESTRUCTIBLE,
                            ItemTypes.ITEM_TYPE_NODROP,
                            ItemTypes.ITEM_TYPE_NOTRADE
                    },
                    (short) IconConstants.ICON_DECO_RES_STONE,
                    BehaviourList.itemBehaviour, 0, Long.MAX_VALUE, 1000, 1000, 1000, -10, MiscConstants.EMPTY_BYTE_PRIMITIVE_ARRAY,
                    "model.decoration.gem.resurrectionstone.", 100.0f, 1000000, ItemMaterials.MATERIAL_LEAD, 100, false);
            logger.info("Created template for: heavy conscience [" + ItemTemplateCreatorAwakening.heavyConscience + "]");

        } catch(IOException e) {
            logger.log(Level.SEVERE, "Initialize item templates error.", e);
        }

        ItemTemplateFactory itf = ItemTemplateFactory.getInstance();
        try {
            ItemTemplate skullMug = itf.getTemplate(ItemList.skullMug);
            ReflectionUtil.setPrivateField(skullMug, ReflectionUtil.getField(skullMug.getClass(), "hollow"), true);
            ReflectionUtil.setPrivateField(skullMug, ReflectionUtil.getField(skullMug.getClass(), "isFoodMaker"), true);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error.", e);
        }

        setMissionItem(ItemList.coinCopper);
        setMissionItem(ItemList.coinIron);
        setMissionItem(ItemList.coinSilver);
        setMissionItem(ItemList.coinGold);
        setMissionItem(ItemList.coinCopperFive);
        setMissionItem(ItemList.coinIronFive);
        setMissionItem(ItemList.coinSilverFive);
        setMissionItem(ItemList.coinGoldFive);
        setMissionItem(ItemList.coinCopperTwenty);
        setMissionItem(ItemList.coinIronTwenty);
        setMissionItem(ItemList.coinSilverTwenty);
        setMissionItem(ItemList.coinGoldTwenty);
        setMissionItem(ItemList.skullGoblin);

        setFullPrice(ItemList.handMirror, false);
    }

    private static void setMissionItem(int id) {
        try {
            ItemTemplate it = ItemTemplateFactory.getInstance().getTemplate(id);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "missions"), true);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "notMissions"), false);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error, for template ID " + id + " (mission): " + e.getMessage(), e);
        }
    }

    private static void setFullPrice(int id, boolean fullPrice) {
        try {
            ItemTemplate it = ItemTemplateFactory.getInstance().getTemplate(id);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "fullprice"), fullPrice);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error, for template ID " + id + " (fullprice): " + e.getMessage(), e);
        }
    }

    public static void modifyItems() {
        try {
            ItemTemplateFactory itf = ItemTemplateFactory.getInstance();

            // Resize mailbox containers
            ItemTemplate[] mailboxes = {
                    itf.getTemplate(ItemList.mailboxStone),
                    itf.getTemplate(ItemList.mailboxStoneTwo),
                    itf.getTemplate(ItemList.mailboxWood),
                    itf.getTemplate(ItemList.mailboxWoodTwo)
            };
            for(ItemTemplate it : mailboxes) {

            }

        } catch(NoSuchTemplateException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }

        createCustomWeapons();
        createCustomArmours();

        setCraftable(ItemList.shoulderPads7, 40.0f, true);
        setCraftable(ItemList.shoulderPads19, 40.0f, true);
        setCraftable(ItemList.shoulderPads9, 60.0f, true);
        setCraftable(ItemList.shoulderPads21, 60.0f, true);

        setCraftable(ItemList.maskEnlightended, 50.0f, true);
        setCraftable(ItemList.maskShadow, 40.0f, true);
        setCraftable(ItemList.maskOfTheReturner, 70.0f, true);
        setCraftable(ItemList.maskTrollHalloween, 80.0f, true);
        setCraftable(ItemList.maskSkullHalloween, 90.0f, false);
        setCraftable(ItemList.maskChallenge, 70.0f, true);
        setCraftable(ItemList.maskRavager, 90.0f, true);

        setCraftable(ItemList.summerHat, 60.0f, false);
        setCraftable(ItemList.strawBed, 30.0f, false);
        setCraftable(ItemList.xmasLunchbox, 50.0f, false);

        setCraftable(ItemList.yuleGoat, 35.0f, false);
        setCraftable(ItemList.santaHat, 35.0f, true);

        setCraftable(ItemList.spyglass, 70.0f, true);

        setCraftable(ItemList.clubHuge, 20.0f, true);

        setTransportable(ItemList.altarWood);
        setTransportable(ItemList.altarStone);
        setTransportable(ItemList.altarSilver);
        setTransportable(ItemList.altarGold);
        setTransportable(ItemList.mailboxWood);
        setTransportable(ItemList.mailboxWoodTwo);
        setTransportable(ItemList.mailboxStone);
        setTransportable(ItemList.mailboxStoneTwo);

        try {
            ItemTemplate summerHat = ItemTemplateFactory.getInstance().getTemplate(ItemList.summerHat);
            ReflectionUtil.setPrivateField(summerHat, ReflectionUtil.getField(summerHat.getClass(), "material"), ItemMaterials.MATERIAL_STRAW);
            ItemTemplate xmasLunchbox = ItemTemplateFactory.getInstance().getTemplate(ItemList.xmasLunchbox);
            ReflectionUtil.setPrivateField(xmasLunchbox, ReflectionUtil.getField(xmasLunchbox.getClass(), "itemDescriptionLong"), "A wicker basket with containers for storing a bit of food and drink for travelling.");

            ItemTemplate riftCrystal = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftCrystal);
            ReflectionUtil.setPrivateField(riftCrystal, ReflectionUtil.getField(riftCrystal.getClass(), "name"), "curse crystal");
            ReflectionUtil.setPrivateField(riftCrystal, ReflectionUtil.getField(riftCrystal.getClass(), "plural"), "curse crystals");
            ReflectionUtil.setPrivateField(riftCrystal, ReflectionUtil.getField(riftCrystal.getClass(), "itemDescriptionLong"), "Some pieces of crystal gathered from a defeated curse infestation. Can be crafted together with metal to create a rune that will attach to metal items.");
            ItemTemplate riftCrystal1 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftCrystal1);
            ReflectionUtil.setPrivateField(riftCrystal1, ReflectionUtil.getField(riftCrystal1.getClass(), "name"), "curse crystal");
            ItemTemplate riftCrystal2 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftCrystal2);
            ReflectionUtil.setPrivateField(riftCrystal2, ReflectionUtil.getField(riftCrystal2.getClass(), "name"), "curse crystal");
            ItemTemplate riftCrystal3 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftCrystal3);
            ReflectionUtil.setPrivateField(riftCrystal3, ReflectionUtil.getField(riftCrystal3.getClass(), "name"), "curse crystal");
            ItemTemplate riftCrystal4 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftCrystal4);
            ReflectionUtil.setPrivateField(riftCrystal4, ReflectionUtil.getField(riftCrystal4.getClass(), "name"), "curse crystal");

            ItemTemplate riftStone = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftStone);
            ReflectionUtil.setPrivateField(riftStone, ReflectionUtil.getField(riftStone.getClass(), "name"), "curse stone shard");
            ReflectionUtil.setPrivateField(riftStone, ReflectionUtil.getField(riftStone.getClass(), "plural"), "curse stone shards");
            ReflectionUtil.setPrivateField(riftStone, ReflectionUtil.getField(riftStone.getClass(), "itemDescriptionLong"), "Some pieces of stone gathered from a defeated curse infestation. Can be crafted together with metal to create a rune that will attach to stone, leather, cloth and pottery items.");
            ItemTemplate riftStone1 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftStone1);
            ReflectionUtil.setPrivateField(riftStone1, ReflectionUtil.getField(riftStone1.getClass(), "name"), "curse stone");
            ItemTemplate riftStone2 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftStone2);
            ReflectionUtil.setPrivateField(riftStone2, ReflectionUtil.getField(riftStone2.getClass(), "name"), "curse stone");
            ItemTemplate riftStone3 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftStone3);
            ReflectionUtil.setPrivateField(riftStone3, ReflectionUtil.getField(riftStone3.getClass(), "name"), "curse stone");
            ItemTemplate riftStone4 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftStone4);
            ReflectionUtil.setPrivateField(riftStone4, ReflectionUtil.getField(riftStone4.getClass(), "name"), "curse stone");

            ItemTemplate riftWood = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftWood);
            ReflectionUtil.setPrivateField(riftWood, ReflectionUtil.getField(riftWood.getClass(), "name"), "curse wood");
            ReflectionUtil.setPrivateField(riftWood, ReflectionUtil.getField(riftWood.getClass(), "plural"), "curse wood");
            ReflectionUtil.setPrivateField(riftWood, ReflectionUtil.getField(riftWood.getClass(), "itemDescriptionLong"), "Some pieces of hardened wood gathered from a defeated curse infestation. Can be crafted together with metal to create a rune that will attach to wooden items.");
            ItemTemplate riftPlant1 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftPlant1);
            ReflectionUtil.setPrivateField(riftPlant1, ReflectionUtil.getField(riftPlant1.getClass(), "name"), "cursed tree");
            ItemTemplate riftPlant2 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftPlant2);
            ReflectionUtil.setPrivateField(riftPlant2, ReflectionUtil.getField(riftPlant2.getClass(), "name"), "cursed tree");
            ItemTemplate riftPlant3 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftPlant3);
            ReflectionUtil.setPrivateField(riftPlant3, ReflectionUtil.getField(riftPlant3.getClass(), "name"), "cursed tree");
            ItemTemplate riftPlant4 = ItemTemplateFactory.getInstance().getTemplate(ItemList.riftPlant4);
            ReflectionUtil.setPrivateField(riftPlant4, ReflectionUtil.getField(riftPlant4.getClass(), "name"), "cursed tree");

            ItemTemplate gardenGnome = ItemTemplateFactory.getInstance().getTemplate(ItemList.gardenGnome);
            ReflectionUtil.setPrivateField(gardenGnome, ReflectionUtil.getField(gardenGnome.getClass(), "itemDescriptionLong"), "A small serious green gnome stands here.");
            ItemTemplate gardenGnomeGreen = ItemTemplateFactory.getInstance().getTemplate(ItemList.gardenGnomeGreen);
            ReflectionUtil.setPrivateField(gardenGnomeGreen, ReflectionUtil.getField(gardenGnomeGreen.getClass(), "itemDescriptionLong"), "A small serious green gnome stands here.");
            ItemTemplate yuleGoat = ItemTemplateFactory.getInstance().getTemplate(ItemList.yuleGoat);
            ReflectionUtil.setPrivateField(yuleGoat, ReflectionUtil.getField(yuleGoat.getClass(), "itemDescriptionLong"), "A popular decoration for the holiday of awakening is a goat made from straw.");
            ItemTemplate christmasTree = ItemTemplateFactory.getInstance().getTemplate(ItemList.christmasTree);
            ReflectionUtil.setPrivateField(christmasTree, ReflectionUtil.getField(christmasTree.getClass(), "name"), "yule tree");
            ReflectionUtil.setPrivateField(christmasTree, ReflectionUtil.getField(christmasTree.getClass(), "plural"), "yule trees");
            ReflectionUtil.setPrivateField(christmasTree, ReflectionUtil.getField(christmasTree.getClass(), "itemDescriptionLong"), "A beautiful yule tree, with colorful decoration.");

            ItemTemplate spyglass = ItemTemplateFactory.getInstance().getTemplate(ItemList.spyglass);
            ReflectionUtil.setPrivateField(spyglass, ReflectionUtil.getField(spyglass.getClass(), "nodrop"), false);

            ItemTemplate woad = ItemTemplateFactory.getInstance().getTemplate(ItemList.woad);
            ReflectionUtil.setPrivateField(woad, ReflectionUtil.getField(woad.getClass(), "isPotable"), true);
            ReflectionUtil.setPrivateField(woad, ReflectionUtil.getField(woad.getClass(), "canBeGrownInPot"), true);

            ItemTemplate rectMarbleTable = ItemTemplateFactory.getInstance().getTemplate(ItemList.rectMarbleTable);
            ReflectionUtil.setPrivateField(rectMarbleTable, ReflectionUtil.getField(rectMarbleTable.getClass(), "colorable"), true);
            ItemTemplate roundMarbleTable = ItemTemplateFactory.getInstance().getTemplate(ItemList.roundMarbleTable);
            ReflectionUtil.setPrivateField(roundMarbleTable, ReflectionUtil.getField(roundMarbleTable.getClass(), "colorable"), true);
            ItemTemplate marblePlanter = ItemTemplateFactory.getInstance().getTemplate(ItemList.marblePlanter);
            ReflectionUtil.setPrivateField(marblePlanter, ReflectionUtil.getField(marblePlanter.getClass(), "colorable"), true);
            ItemTemplate pillar = ItemTemplateFactory.getInstance().getTemplate(ItemList.pillarDecoration);
            ReflectionUtil.setPrivateField(pillar, ReflectionUtil.getField(pillar.getClass(), "colorable"), true);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error: " + e.getMessage(), e);
        }
    }

    public static void createCustomWeapons() {
        try {
            new Weapon(sedaesSword, 5.5f, 3.5f, 0.01f, 3, 2, 1.0f, 0.0);
            new Weapon(payrenStaff, 8.0f, 3.5f, 0.0f, 3, 2, 1.0f, 0.0);
            new Weapon(eilandaSpear, 9.0f, 4.0f, 0.06f, 7, 2, 1.0f, 0.0);
            new Weapon(shoninSword, 9.0f, 4.0f, 0.05f, 4, 3, 1.0f, 0.0);
            new Weapon(asmanStaff, 8.0f, 3.5f, 0.0f, 3, 2, 1.0f, 0.0);
            new Weapon(kianeStaff, 8.0f, 3.5f, 0.0f, 3, 2, 1.0f, 0.0);
            new Weapon(miridonAxe, 6.5f, 3.5f, 0.03f, 4, 3, 0.3f, 0.0);
            new Weapon(laphanerStaff, 8.0f, 3.5f, 0.0f, 3, 2, 1.0f, 0.0);
            new Weapon(drakkarKnife, 4.0f, 2.0f, 0.1f, 2, 1, 1.0f, 0.0);
            new Weapon(qaleshinStaff, 8.0f, 3.5f, 0.0f, 3, 2, 1.0f, 0.0);

            new Weapon(warHammer, 9.0F, 6.5F, 0.03F, 4, 4, 1.0F, 0.0);
        } catch(IllegalArgumentException | ClassCastException e) {
            logger.log(Level.SEVERE, "Error creating custom weapon: " + e.getMessage(), e);
        }
        logger.info("Created custom weapons.");
    }

    public static void createCustomArmours() {
    }

    public static void setCraftable(int id, float difficulty, boolean improve) {
        try {
            ItemTemplate it = ItemTemplateFactory.getInstance().getTemplate(id);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "difficulty"), difficulty);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "repairable"), true);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "noImprove"), !improve);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error, for template ID " + id + " (craftable): " + e.getMessage(), e);
        }
    }

    public static void setTransportable(int id) {
        try {
            ItemTemplate it = ItemTemplateFactory.getInstance().getTemplate(id);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "isTransportable"), true);
            ReflectionUtil.setPrivateField(it, ReflectionUtil.getField(it.getClass(), "useOnGroundOnly"), true);
        } catch(NoSuchTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error, for template ID " + id + " (transportable): " + e.getMessage(), e);
        }
    }

    public static void initCreationEntries() {
        /* Bulk Chest: */
        final AdvancedCreationEntry bc = CreationEntryCreator.createAdvancedEntry(
                SkillList.CARPENTRY_FINE,
                ItemList.ironBand, ItemList.plank, ItemTemplateCreatorAwakening.bulkChest,
                false, false, 0.0f, true, true, CreationCategories.STORAGE);
        bc.addRequirement(new CreationRequirement(1, ItemList.plank, 10, true));
        bc.addRequirement(new CreationRequirement(2, ItemList.ironBand, 3, true));
        bc.addRequirement(new CreationRequirement(3, ItemList.nailsIronSmall, 4, true));

        CreationEntryCreator.createSimpleEntry(
                SkillList.SMITHING_WEAPON_HEADS, ItemList.anvilLarge, ItemList.bronzeBar, warHammerHead, false, true,
                0.0f, false, false, CreationCategories.WEAPON_HEADS);

        final AdvancedCreationEntry wh = CreationEntryCreator.createAdvancedEntry(
                SkillList.GROUP_SMITHING_WEAPONSMITHING,
                ItemList.staffWood, warHammerHead, warHammer,
                false, false, 0.0f, true, false, CreationCategories.WEAPONS);
        wh.addRequirement(new CreationRequirement(1, ItemList.brassBand, 1, true));
        wh.addRequirement(new CreationRequirement(2, ItemList.nailsIronSmall, 2, true));

        CreationEntryCreator.createMetallicEntries(
                SkillList.SMITHING_ARMOUR_PLATE,
                ItemList.anvilLarge, ItemList.ironBar, ItemList.shoulderPads7,
                false, true, 10.0f, false, false, 0, 35.0, CreationCategories.ARMOUR);
        CreationEntryCreator.createMetallicEntries(
                SkillList.SMITHING_ARMOUR_PLATE,
                ItemList.anvilLarge, ItemList.ironBar, ItemList.shoulderPads19,
                false, true, 10.0f, false, false, 0, 35.0, CreationCategories.ARMOUR);

        CreationEntryCreator.createMetallicEntries(
                SkillList.SMITHING_GOLDSMITHING,
                ItemList.anvilSmall, ItemList.ironBar, ItemList.shoulderPads9,
                false, true, 10.0f, false, false, 0, 55.0, CreationCategories.ARMOUR);
        CreationEntryCreator.createMetallicEntries(
                SkillList.SMITHING_GOLDSMITHING,
                ItemList.anvilSmall, ItemList.ironBar, ItemList.shoulderPads21,
                false, true, 10.0f, false, false, 0, 55.0, CreationCategories.ARMOUR);

        CreationEntryCreator.createSimpleEntry(
                SkillList.LEATHERWORKING,
                ItemList.scissors, ItemList.leather, ItemList.maskEnlightended,
                false, true, 50.0f, false, false, 0, 40.0, CreationCategories.CLOTHES);
        final AdvancedCreationEntry maskShadow = CreationEntryCreator.createAdvancedEntry(
                SkillList.LEATHERWORKING,
                ItemList.scissors, ItemList.leather, ItemList.maskShadow,
                false, true, 10.0f, false, false, 0, 30.0, CreationCategories.CLOTHES);
        maskShadow.setUseTemplateWeight(true);
        maskShadow.setDepleteFromSource(0);
        maskShadow.setDepleteFromTarget(200);
        maskShadow.addRequirement(new CreationRequirement(1, ItemList.tooth, 7, true));
        maskShadow.addRequirement(new CreationRequirement(2, ItemList.tannin, 1, true));
        CreationEntryCreator.createSimpleEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayMaskIsles,
                false, true, 10.0f, false, false, 0, 40.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayMidsummerMask,
                false, true, 10.0f, false, false, 0, 50.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.LEATHERWORKING,
                ItemList.scissors, ItemList.leather, ItemList.maskOfTheReturner,
                false, true, 50.0f, false, false, 0, 60.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.LEATHERWORKING,
                ItemList.scissors, ItemList.leather, ItemList.maskTrollHalloween,
                false, true, 100.0f, false, false, 0, 70.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.CARPENTRY_FINE,
                ItemList.knifeCarving, ItemList.skullGoblin, ItemList.maskSkullHalloween,
                false, true, 300.0f, false, false, 0, 80.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.SMITHING_GOLDSMITHING,
                ItemList.anvilSmall, ItemList.silverBar, ItemList.maskChallenge,
                false, true, 10.0f, false, false, 0, 60.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.LEATHERWORKING,
                ItemList.scissors, ItemList.leather, ItemList.maskRavager,
                false, true, 100.0f, false, false, 0, 80.0, CreationCategories.CLOTHES);
        CreationEntryCreator.createSimpleEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayPaleMask,
                false, true, 10.0f, false, false, 0, 80.0, CreationCategories.CLOTHES);

        CreationEntryCreator.createSimpleEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayCoffeeCup,
                false, true, 10.0f, false, false, CreationCategories.POTTERY);

        final AdvancedCreationEntry summerHat = CreationEntryCreator.createAdvancedEntry(
                SkillList.THATCHING,
                ItemList.bodyHand, ItemList.thatch, ItemList.summerHat,
                false, true, 10.0f, false, false, 0, 50.0, CreationCategories.CLOTHES);
        summerHat.setUseTemplateWeight(true);
        summerHat.setDepleteFromSource(0);
        summerHat.setDepleteFromTarget(200);
        summerHat.addRequirement(new CreationRequirement(1, ItemList.clothYard, 1, false));
        summerHat.addRequirement(new CreationRequirement(2, ItemList.clothString, 1, true));
        summerHat.addRequirement(new CreationRequirement(3, ItemList.flower3, 1, true));
        ArmourTemplate.armourTemplates.remove(ItemList.summerHat);

        final AdvancedCreationEntry strawBed = CreationEntryCreator.createAdvancedEntry(
                SkillList.THATCHING,
                ItemList.bodyHand, ItemList.thatch, ItemList.strawBed,
                false, true, 0.0f, false, true, CreationCategories.DECORATION);
        strawBed.setUseTemplateWeight(false);
        strawBed.setDepleteFromSource(0);
        strawBed.setDepleteFromTarget(2500);
        strawBed.addRequirement(new CreationRequirement(1, ItemList.thatch, 15, false));

        final AdvancedCreationEntry picnicBasket = CreationEntryCreator.createAdvancedEntry(
                SkillList.THATCHING,
                ItemList.bodyHand, ItemList.thatch, ItemList.xmasLunchbox,
                false, true, 0.0f, false, false, 0, 40.0, CreationCategories.CONTAINER);
        strawBed.setUseTemplateWeight(false);
        strawBed.setDepleteFromSource(0);
        strawBed.setDepleteFromTarget(50);
        picnicBasket.addRequirement(new CreationRequirement(1, ItemList.jarPottery, 1, true));
        picnicBasket.addRequirement(new CreationRequirement(2, ItemList.thatch, 6, true));
        picnicBasket.addRequirement(new CreationRequirement(3, ItemList.shaft, 6, true));
        picnicBasket.addRequirement(new CreationRequirement(4, ItemList.leatherStrip, 2, true));
        picnicBasket.addRequirement(new CreationRequirement(5, ItemList.sheetLead, 1, true));
        picnicBasket.addRequirement(new CreationRequirement(6, ItemList.clothYard, 1, true));

        final AdvancedCreationEntry clayGardenGnome = CreationEntryCreator.createAdvancedEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayGardenGnome,
                false, true, 0.0f, false, false, CreationCategories.POTTERY);
        clayGardenGnome.addRequirement(new CreationRequirement(1, ItemList.sourceSalt, 5, true));

        final AdvancedCreationEntry clayGardenGnomeGreen = CreationEntryCreator.createAdvancedEntry(
                SkillList.POTTERY,
                ItemList.bodyHand, ItemList.clay, ItemTemplateCreatorAwakening.clayGardenGnomeGreen,
                false, true, 0.0f, false, false, CreationCategories.POTTERY);
        clayGardenGnomeGreen.addRequirement(new CreationRequirement(1, ItemList.sourceSalt, 5, true));

        final AdvancedCreationEntry yuleGoat = CreationEntryCreator.createAdvancedEntry(
                SkillList.THATCHING,
                ItemList.bodyHand, ItemList.thatch, ItemList.yuleGoat,
                false, true, 0.0f, false, false, CreationCategories.DECORATION);
        yuleGoat.setUseTemplateWeight(true);
        yuleGoat.setDepleteFromSource(0);
        yuleGoat.setDepleteFromTarget(480);
        yuleGoat.addRequirement(new CreationRequirement(1, ItemList.clothYard, 1, true));
        yuleGoat.addRequirement(new CreationRequirement(2, ItemList.sourceSalt, 5, true));

        final AdvancedCreationEntry santaHat = CreationEntryCreator.createAdvancedEntry(
                SkillList.CLOTHTAILORING,
                ItemList.needleIron, ItemList.clothYard, ItemList.santaHat,
                false, true, 5.0f, false, false, 0, 25.0, CreationCategories.CLOTHES);
        santaHat.setUseTemplateWeight(true);
        santaHat.setDepleteFromSource(0);
        santaHat.setDepleteFromTarget(30);
        santaHat.addRequirement(new CreationRequirement(1, ItemList.cochineal, 1, true));

        final AdvancedCreationEntry santaHat2 = CreationEntryCreator.createAdvancedEntry(
                SkillList.CLOTHTAILORING,
                ItemList.needleCopper, ItemList.clothYard, ItemList.santaHat,
                false, true, 5.0f, false, false, 0, 25.0, CreationCategories.CLOTHES);
        santaHat2.setUseTemplateWeight(true);
        santaHat2.setDepleteFromSource(0);
        santaHat2.setDepleteFromTarget(30);
        santaHat2.addRequirement(new CreationRequirement(1, ItemList.cochineal, 1, true));

        CreationEntryCreator.createSimpleEntry(
                SkillList.STONECUTTING,
                ItemList.whetStone, ItemList.diamond, ItemTemplateCreatorAwakening.diamondLens,
                false, true, 10.0f, false, false, 0, 60.0, CreationCategories.TOOL_PARTS);
        CreationEntryCreator.createSimpleEntry(
                SkillList.SMITHING_GOLDSMITHING,
                ItemList.anvilSmall, ItemList.brassBar, ItemTemplateCreatorAwakening.brassTube,
                false, true, 10.0f, false, false, CreationCategories.TOOL_PARTS);
        final AdvancedCreationEntry spyglass = CreationEntryCreator.createAdvancedEntry(
                SkillList.SMITHING_GOLDSMITHING,
                ItemTemplateCreatorAwakening.brassTube, ItemList.brassBand, ItemList.spyglass,
                false, false, 0.0f, true, false, CreationCategories.TOOLS);
        spyglass.addRequirement(new CreationRequirement(1, ItemList.brassBand, 1, true));
        spyglass.addRequirement(new CreationRequirement(2, ItemList.leather, 1, true));
        spyglass.addRequirement(new CreationRequirement(3, ItemTemplateCreatorAwakening.diamondLens, 2, true));

        final AdvancedCreationEntry shodClub = CreationEntryCreator.createAdvancedEntry(
                SkillList.GROUP_SMITHING_WEAPONSMITHING,
                ItemList.hatchet, ItemList.log, ItemList.clubHuge,
                false, true, 5.0f, false, false, CreationCategories.WEAPONS);
        shodClub.setUseTemplateWeight(true);
        shodClub.setDepleteFromSource(0);
        shodClub.setDepleteFromTarget(15000);
        shodClub.addRequirement(new CreationRequirement(1, ItemList.leatherStrip, 1, true));

        try {
            final AdvancedCreationEntry lunchbox = CreationMatrix.getInstance().getAdvancedCreationEntry(ItemList.lunchbox);
            lunchbox.setFinalMaterial(ItemMaterials.MATERIAL_TIN);
        } catch(NoSuchEntryException e) {
        }
    }

    @SuppressWarnings("unused")
    public static boolean isBulkContainer(Item item) {
        return item.getTemplateId() == ItemTemplateCreatorAwakening.bulkChest;
    }

    @SuppressWarnings("unused")
    public static float sizeMod(Item item) {
        if(item.getTemplateId() == minorIllusionCreature) {
            byte aux = item.getAuxData();
            if(aux == 0) {
                float scale = (float) (item.getData2() & 0xFFFF);
                float ql = item.getQualityLevel();
                if(scale > 0) return Config.minorIllusionSizeMod * (scale * 0.01f) * (1.0f + ql * 0.01f);
            }
        }
        return 1.0f;
    }

    static {
        TempStates.addState(new TempState(clayGardenGnome, ItemList.gardenGnome, (short) 10000, true, false, false));
        TempStates.addState(new TempState(clayGardenGnomeGreen, ItemList.gardenGnomeGreen, (short) 10000, true, false, false));
        TempStates.addState(new TempState(clayMaskIsles, ItemList.maskIsles, (short) 10000, true, false, false));
        TempStates.addState(new TempState(clayMidsummerMask, ItemList.midsummerMask, (short) 10000, true, false, false));
        TempStates.addState(new TempState(clayPaleMask, ItemList.maskPale, (short) 10000, true, false, false));
        TempStates.addState(new TempState(clayCoffeeCup, coffeeCup, (short) 10000, true, false, false));
    }
}
