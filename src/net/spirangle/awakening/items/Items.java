package net.spirangle.awakening.items;

import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.AdvancedCreationEntry;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.shared.util.MaterialUtilities;
import net.spirangle.awakening.zones.Infestation;
import net.spirangle.awakening.zones.Infestations;
import net.spirangle.awakening.zones.Treasure;
import net.spirangle.awakening.zones.Treasures;

public class Items {

    @SuppressWarnings("unused")
    public static String getImpDesc(Creature performer, Item target, String description) {
        int templId = target.getTemplate().getTemplateId();
        if(templId >= ItemList.riftStone1 && templId <= ItemList.riftPlant4) {
            Infestation infestation = Infestations.getInstance().getInfestation(target);
            if(infestation != null) {
                int level = (int) infestation.getPower();
                description = " The power of this infestation is " + Infestation.powerLevels[level] + "." + description;
            }
        } else if(templId == ItemTemplateCreatorAwakening.mapFragment) {
            description = Treasures.getInstance().getFragmentDescription(target) + description;
        } else if(templId == ItemTemplateCreatorAwakening.treasureMap) {
            description = Treasures.getInstance().getMapDescription(target) + description;
        }
        return description;
    }

    public static String getName(ItemTemplate template, boolean plural, byte material) {
        StringBuilder name = new StringBuilder();
        if(template.isBulk()) {
            name.append(template.sizeString);
            MaterialUtilities.appendNameWithMaterialSuffix(name, plural? template.getPlural() : template.getName(), material);
        } else {
            MaterialUtilities.appendNameWithMaterialSuffix(name, template.getName(), material);
        }
        return name.toString();
    }

    @SuppressWarnings("unused")
    public static boolean openContainer(Creature performer, Item item, short action) {
        if(action != Actions.OPEN && action != Actions.OPEN_INVENTORY_CONTAINER) return true;
        if(item.getTemplate().getTemplateId() == ItemList.treasureChest) {
            if(!performer.isPlayer()) return false;
            Treasure treasure = Treasures.getInstance().getTreasure(item);
            if(treasure == null) return true;
            return treasure.openTreasure(performer);
        }
        return true;
    }

    @SuppressWarnings("unused")
    public static Item handleFinishedAdvancedCreationEntry(AdvancedCreationEntry creationEntry, Item item) {
        if(item != null) {
            if(creationEntry.usesFinalMaterial())
                item.setMaterial(creationEntry.getFinalMaterial());
        }
        return item;
    }
}
