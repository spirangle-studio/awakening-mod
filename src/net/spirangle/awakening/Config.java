package net.spirangle.awakening;

import com.wurmonline.server.Constants;
import net.spirangle.awakening.tasks.MerchantTask;
import net.spirangle.awakening.tasks.VendorTask;
import net.spirangle.awakening.time.Seasons;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Config implements Configurable {

    private static final Logger logger = Logger.getLogger(Config.class.getName());

    public static class LoginEffect {
        public final long wurmId;
        public final short effect;

        private LoginEffect(long wurmId, short effect) {
            this.wurmId = wurmId;
            this.effect = effect;
        }
    }

    public static final String[] materials = {
            null,
            "flesh", "meat", "rye", "oat", "barley", "wheat", "gold", "silver", "steel", "copper",
            "iron", "lead", "zinc", "birchwood", "stone", "leather", "cotton", "clay", "pottery", "glass",
            "magic", "vegetarian", "fire", null, "oil", "water", "charcoal", "dairy", "honey", "brass",
            "bronze", "fat", "paper", "tin", "bone", "salt", "pinewood", "oakenwood", "cedarwood", "willow",
            "maplewood", "applewood", "lemonwood", "olivewood", "cherrywood", "lavenderwood", "rosewood", "thorn", "grapewood", "camelliawood",
            "oleanderwood", "crystal", "wemp", "diamond", "animal", "adamantine", "glimmersteel", "tar", "peat", "reed",
            "slate", "marble", "chestnut", "walnut", "firwood", "lindenwood", "seryll", "ivy", "wool", "straw",
            "hazelnutwood", "bear", "beef", "canine", "feline", "dragon", "fowl", "game", "horse", "human",
            "humanoid", "insect", "lamb", "pork", "seafood", "snake", "tough", "orangewood", null, "raspberrywood",
            "blueberrywood", "lingonberrywood", "metal", "alloy", "moonmetal", "electrum"

    };

    public static final String[] kingdomNames = { "Kendralon", "Tirvalon", "Cellimdar", "Shodroq", "Thekandre" };

    public static int remotePort = 4301;

    public static String webPageHost = "";

    public static int serverLagReportTime = 30;

    public static boolean antiCheatHandling = false;
    public static boolean antiCheatHandling_informStaff = false;
    public static boolean antiCheatHandling_guilt = false;
    public static boolean antiMacroHandling = true;
    public static boolean antiMacroPunishing = true;
    public static int antiMacroActionsFloodingCount = 20;
    public static long antiMacroPatternedActionTime = 30;
    public static int antiMacroPatternedActions = 100;
    public static int antiMacroRobotQuestionCounter = 30;
    public static int antiMacroRobotActionsCounter = 10;
    public static long antiMacroRobotTestTimeMin = 120;
    public static long antiMacroRobotTestTimeMax = 240;
    public static long antiMacroRobotPunishTime = 10;
    public static int antiMacroRandomActionsMin = 1000;
    public static int antiMacroRandomActionsMax = 2000;

    public static final Map<String, String> acceptedClientMods = new HashMap<>();

    public static int plagueRadius = 50;
    public static int plagueMinPopulation = 500;
    public static int plagueMaxCreaturesMargin = 1000;

    public static List<LoginEffect> loginEffects = new ArrayList<>();

    public static boolean sendGMLoginMessage = false;
    public static boolean sendCMLoginMessage = false;
    public static boolean sendCALoginMessage = false;

    public static long[] kingdomTraders = { 0L, 0L, 0L, 0L, 0L };

    public static int[] kingdomSalary = { 0, 0, 0, 0, 0 };
    public static String[] kingdomSalaryMessage = { null, null, null, null, null };
    public static int[] kingdomSkillSalary = { 0, 0, 0, 0, 0 };
    public static int[] kingdomMasterSalary = { 0, 0, 0, 0, 0 };
    public static int[] kingdomGuardTowerSalary = { 0, 0, 0, 0, 0 };
    public static int[] kingdomEnemyPlayerBounty = { 0, 0, 0, 0, 0 };
    public static int[] kingdomEnemyBounty = { 0, 0, 0, 0, 0 };

    public static float kingdomTax = 0.05f;
    public static float kingdomTariff = 0.2f;

    public static double deityFavorMultiplier = 2.5;
    public static double nightSteedDifficulty = 70.0;

    public static Set<String> excludedVillages = new HashSet<>();

    public static boolean showOnLogin = true;
    public static boolean showRemainingDays = true;
    public static int daysShifting = Seasons.DAYS_IN_WEEK;
    public static int daysColdBeforeSnow = Seasons.DAYS_IN_WEEK;
    public static int daysColdAfterSnow = Seasons.DAYS_IN_WEEK;
    public static String[] seasonNames = new String[Seasons.SEASONS];

    public static int infestations = 100;
    public static float infestationMinPower = 0.0f;
    public static float infestationMaxPower = 5.0f;
    public static float infestationResourceModifier = 2.5f;
    public static int infestationMinMetalWeight = 30;
    public static int infestationMaxMetalWeight = 200;

    public static float cursedBattleRating = 1.0f;
    public static float cursedDamage = 1.0f;
    public static float cursedParry = 1.0f;
    public static float cursedDodge = 1.0f;
    public static float cursedAgg = 1.0f;

    public static float minorIllusionSizeMod = 1.0f;
    public static int minorIllusionCreatureKarmaCost = 0;
    public static int minorIllusionItemKarmaCost = 0;
    public static int silverMirrorKarmaCost = 0;
    public static int goldMirrorKarmaCost = 0;
    public static int yellowPotionKarmaCost = 0;

    public static float[] materialValues;

    public static double merchantMaxAmount = 3000.0f;
    public static long merchantMaxPrice = 30000L;

    public static float basePrice1 = 10.0f;
    public static float basePrice10 = 19.0f;
    public static float basePrice20 = 26.0f;
    public static float basePrice50 = 38.0f;
    public static float basePrice100 = 53.0f;

    public static float basePriceMod1 = (basePrice10 - basePrice1) / 10.0f;
    public static float basePriceMod10 = (basePrice20 - basePrice10) / 10.0f;
    public static float basePriceMod20 = (basePrice50 - basePrice20) / 30.0f;
    public static float basePriceMod50 = (basePrice100 - basePrice50) / 50.0f;

    public static float vendorBasePrice = 1.0f;
    public static float vendorFoodPrice = 1.0f;

    private static Config instance = null;

    public static Config getInstance() {
        if(instance == null) instance = new Config();
        return instance;
    }

    private Config() {
    }

    @Override
    public void configure(Properties properties) {
        webPageHost = properties.getProperty("webPageHost", "");

        serverLagReportTime = Integer.parseInt(properties.getProperty("serverLagReportTime", "30"));

        antiCheatHandling = Boolean.parseBoolean(properties.getProperty("antiCheatHandling", "false"));
        antiCheatHandling_informStaff = Boolean.parseBoolean(properties.getProperty("antiCheatHandling_informStaff", "false"));
        antiCheatHandling_guilt = Boolean.parseBoolean(properties.getProperty("antiCheatHandling_guilt", "false"));
        antiMacroHandling = Boolean.parseBoolean(properties.getProperty("antiMacroHandling", "true"));
        antiMacroPunishing = Boolean.parseBoolean(properties.getProperty("antiMacroPunishing", "true"));
        antiMacroActionsFloodingCount = Integer.parseInt(properties.getProperty("antiMacroActionsFloodingCount", "20"));
        antiMacroPatternedActionTime = Long.parseLong(properties.getProperty("antiMacroPatternedActionTime", "300"));
        antiMacroPatternedActions = Integer.parseInt(properties.getProperty("antiMacroPatternedActions", "100"));
        antiMacroRobotQuestionCounter = Integer.parseInt(properties.getProperty("antiMacroRobotQuestionCounter", "30"));
        antiMacroRobotActionsCounter = Integer.parseInt(properties.getProperty("antiMacroRobotActionsCounter", "10"));
        antiMacroRobotTestTimeMin = Long.parseLong(properties.getProperty("antiMacroRobotTestTimeMin", "120"));
        antiMacroRobotTestTimeMax = Long.parseLong(properties.getProperty("antiMacroRobotTestTimeMax", "240"));
        antiMacroRobotPunishTime = Long.parseLong(properties.getProperty("antiMacroRobotPunishTime", "10"));
        antiMacroRandomActionsMin = Integer.parseInt(properties.getProperty("antiMacroRandomActionsMin", "1000"));
        antiMacroRandomActionsMax = Integer.parseInt(properties.getProperty("antiMacroRandomActionsMax", "2000"));

        for(int i = 1; true; ++i) {
            String value = properties.getProperty("clientMod_" + i, null);
            if(value == null) break;
            int n = value.indexOf(',');
            if(n == -1) continue;
            acceptedClientMods.put(value.substring(n + 1).trim(), value.substring(0, n).trim());
        }

        plagueRadius = Integer.parseInt(properties.getProperty("plagueRadius", "50"));
        plagueMinPopulation = Integer.parseInt(properties.getProperty("plagueMinPopulation", "500"));
        plagueMaxCreaturesMargin = Integer.parseInt(properties.getProperty("plagueMaxCreaturesMargin", "1000"));

        loginEffects.clear();
        for(int i = 1; true; ++i) {
            String value = properties.getProperty("loginEffect_" + i, null);
            if(value == null) break;
            int n = value.indexOf(',');
            if(n == -1) continue;
            loginEffects.add(new LoginEffect(Long.parseLong(value.substring(0, n).trim()), Short.parseShort(value.substring(n + 1).trim())));
        }

        sendGMLoginMessage = Boolean.parseBoolean(properties.getProperty("sendGMLoginMessage", "false"));
        sendCMLoginMessage = Boolean.parseBoolean(properties.getProperty("sendCMLoginMessage", "false"));
        sendCALoginMessage = Boolean.parseBoolean(properties.getProperty("sendCALoginMessage", "false"));

        for(int i = 0; i <= 4; ++i) {
            String kingdom = kingdomNames[i];

            kingdomTraders[i] = Long.parseLong(properties.getProperty("kingdomTrader_" + kingdom, "0"));

            kingdomSalary[i] = Integer.parseInt(properties.getProperty("kingdomSalary_" + kingdom, "0"));
            kingdomSalaryMessage[i] = properties.getProperty("kingdomSalaryMessage_" + kingdom, null);
            kingdomSkillSalary[i] = Integer.parseInt(properties.getProperty("kingdomSkillSalary_" + kingdom, "0"));
            kingdomMasterSalary[i] = Integer.parseInt(properties.getProperty("kingdomMasterSalary_" + kingdom, "0"));
            kingdomGuardTowerSalary[i] = Integer.parseInt(properties.getProperty("kingdomGuardTowerSalary_" + kingdom, "0"));
            kingdomEnemyPlayerBounty[i] = Integer.parseInt(properties.getProperty("kingdomEnemyPlayerBounty_" + kingdom, "0"));
            kingdomEnemyBounty[i] = Integer.parseInt(properties.getProperty("kingdomEnemyBounty_" + kingdom, "0"));
        }

        kingdomTax = Float.parseFloat(properties.getProperty("kingdomTax", "0.05"));
        kingdomTariff = Float.parseFloat(properties.getProperty("kingdomTariff", "0.2"));

        deityFavorMultiplier = Double.parseDouble(properties.getProperty("deityFavorMultiplier", "2.5"));
        nightSteedDifficulty = Double.parseDouble(properties.getProperty("nightSteedDifficulty", "70.0"));

        String s = properties.getProperty("deedSpawn_excludedVillages");
        if(s != null) {
            excludedVillages.clear();
            excludedVillages.addAll(Arrays.asList(s.split("\\s*;\\s*")));
        }

        materialValues = new float[128];
        for(int i = 0; i < 128; ++i) {
            float m = materialValues[i];
            materialValues[i] = 1.0f;
            if(i < materials.length && materials[i] != null) {
                materialValues[i] = Float.parseFloat(properties.getProperty("material_" + materials[i], "1.0"));
                if(m != materialValues[i]) {
                    logger.info("Set material value for " + materials[i] + " to " + materialValues[i] + ".");
                }
            }
        }

        showOnLogin = Boolean.parseBoolean(properties.getProperty("showOnLogin", "true"));
        showRemainingDays = Boolean.parseBoolean(properties.getProperty("showRemainingDays", "true"));
        daysShifting = Integer.parseInt(properties.getProperty("daysShifting", "7"));
        daysColdBeforeSnow = Integer.parseInt(properties.getProperty("daysColdBeforeSnow", "7"));
        daysColdAfterSnow = Integer.parseInt(properties.getProperty("daysColdAfterSnow", "7"));

        seasonNames[Seasons.SPRING] = properties.getProperty("springName", "spring");
        seasonNames[Seasons.EARLY_SUMMER] = properties.getProperty("earlySummerName", "early summer");
        seasonNames[Seasons.LATE_SUMMER] = properties.getProperty("lateSummerName", "late summer");
        seasonNames[Seasons.AUTUMN] = properties.getProperty("autumnName", "autumn");
        seasonNames[Seasons.WINTER] = properties.getProperty("winterName", "winter");

        infestations = Integer.parseInt(properties.getProperty("infestations", "100"));
        infestationMinPower = Float.parseFloat(properties.getProperty("infestationMinPower", "0.0"));
        infestationMaxPower = Float.parseFloat(properties.getProperty("infestationMaxPower", "5.0"));
        infestationResourceModifier = Float.parseFloat(properties.getProperty("infestationResourceModifier", "2.5"));
        infestationMinMetalWeight = Integer.parseInt(properties.getProperty("infestationMinMetalWeight", "30"));
        infestationMaxMetalWeight = Integer.parseInt(properties.getProperty("infestationMaxMetalWeight", "200"));

        cursedBattleRating = Float.parseFloat(properties.getProperty("cursedBattleRating", "1.0"));
        cursedDamage = Float.parseFloat(properties.getProperty("cursedDamage", "1.0"));
        cursedParry = Float.parseFloat(properties.getProperty("cursedParry", "1.0"));
        cursedDodge = Float.parseFloat(properties.getProperty("cursedDodge", "1.0"));
        cursedAgg = Float.parseFloat(properties.getProperty("cursedAgg", "1.0"));

        minorIllusionSizeMod = Float.parseFloat(properties.getProperty("minorIllusionSizeMod", "1.0"));
        minorIllusionCreatureKarmaCost = Integer.parseInt(properties.getProperty("minorIllusionCreatureKarmaCost", "0"));
        minorIllusionItemKarmaCost = Integer.parseInt(properties.getProperty("minorIllusionItemKarmaCost", "0"));
        silverMirrorKarmaCost = Integer.parseInt(properties.getProperty("silverMirrorKarmaCost", "0"));
        goldMirrorKarmaCost = Integer.parseInt(properties.getProperty("goldMirrorKarmaCost", "0"));
        yellowPotionKarmaCost = Integer.parseInt(properties.getProperty("yellowPotionKarmaCost", "0"));

        merchantMaxAmount = Double.parseDouble(properties.getProperty("merchant_maxAmount", "3000.0"));
        merchantMaxPrice = Long.parseLong(properties.getProperty("merchant_maxPrice", "30000"));

        basePrice1 = getMerchantPriceValue(properties, "basePrice1", basePrice1);
        basePrice10 = getMerchantPriceValue(properties, "basePrice10", basePrice10);
        basePrice20 = getMerchantPriceValue(properties, "basePrice20", basePrice20);
        basePrice50 = getMerchantPriceValue(properties, "basePrice50", basePrice50);
        basePrice100 = getMerchantPriceValue(properties, "basePrice100", basePrice100);
        basePriceMod1 = (basePrice10 - basePrice1) / 10.0f;
        basePriceMod10 = (basePrice20 - basePrice10) / 10.0f;
        basePriceMod20 = (basePrice50 - basePrice20) / 30.0f;
        basePriceMod50 = (basePrice100 - basePrice50) / 50.0f;
        for(int i = 0; i < MerchantTask.wares.length; ++i)
            updateMerchantWare(properties, MerchantTask.wares[i]);

        vendorBasePrice = Float.parseFloat(properties.getProperty("vendor_basePrice", String.valueOf(vendorBasePrice)));
        vendorFoodPrice = Float.parseFloat(properties.getProperty("vendor_foodPrice", String.valueOf(vendorFoodPrice)));
        for(int i = 0; i < VendorTask.categories.length; ++i)
            updateVendorCategory(properties, VendorTask.categories[i]);
    }

    public void loadWurmIni() {
        Properties wurmIni;
        try {
            Field field = Constants.class.getDeclaredField("props");
            field.setAccessible(true);
            wurmIni = (Properties) field.get(null);
        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Config.configure: " + e.getMessage(), e);
            wurmIni = new Properties();
        }

        remotePort = Integer.parseInt(wurmIni.getProperty("REMOTE_PORT", String.valueOf(remotePort)));
    }

    private static void updateMerchantWare(Properties properties, MerchantTask.Ware ware) {
        String value = properties.getProperty("merchant_" + ware.name);
        if(value == null) return;
        int i1 = value.indexOf(',');
        if(i1 == -1) return;
        int i2 = value.indexOf(',', i1 + 1);
        if(i2 == -1) return;
        int i3 = value.indexOf(',', i2 + 1);
        int i4 = i3 > i2? value.indexOf(',', i2 + 1) : -1;
        float price = getMerchantPriceValue(value.substring(0, i1).trim(), ware.price);
        double supplyDemandFactor = Math.sqrt(Integer.parseInt(value.substring(i1 + 1, i2).trim()));
        long decayFactor = Long.parseLong(value.substring(i2 + 1, i3 > i2? i3 : value.length()).trim());
        boolean buy = i3 <= i2 || Boolean.parseBoolean(value.substring(i3 + 1, i4 > i3? i4 : value.length()).trim());
        boolean sell = i4 <= i3 || Boolean.parseBoolean(value.substring(i4 + 1).trim());
        if(price != ware.price || supplyDemandFactor != ware.supplyDemandFactor || decayFactor != ware.decayFactor || buy != ware.buy || sell != ware.sell) {
            ware.price = price;
            ware.supplyDemandFactor = supplyDemandFactor;
            ware.decayFactor = decayFactor;
            ware.buy = buy;
            ware.sell = sell;
            logger.info("Set merchant price for " + ware.name + " to " + ware.price + ", supply & demand factor: " + ware.supplyDemandFactor + ", decay factor: " + ware.decayFactor + ", buy: " + ware.buy + ", sell: " + ware.sell);
        }
    }

    private static float getMerchantPriceValue(Properties properties, String key, float price) {
        float p = getMerchantPriceValue(properties.getProperty("merchant_" + key), price);
        if(p != price) {
            price = p;
            logger.info("Set price for " + key + " to " + price);
        }
        return price;
    }

    private static float getMerchantPriceValue(String value, float price) {
        if(value != null) {
            float p = Float.parseFloat(value);
            if(p != price) price = p;
        }
        return price;
    }

    private static void updateVendorCategory(Properties properties, VendorTask.Category category) {
        if(category.name == null) return;
        String value = properties.getProperty("vendor_" + category.name);
        if(value == null) return;
        int i1 = value.indexOf(',');
        if(i1 == -1) return;
        float price = getVendorPriceValue(value.substring(0, i1).trim(), category.price);
        long budget = Long.parseLong(value.substring(i1 + 1).trim());
        if(price != category.price || budget != category.budget) {
            category.price = price;
            category.budget = budget;
            logger.info("Set vendor price for " + category.name + " to " + category.price + ", budget: " + category.budget);
        }
    }

    private static float getVendorPriceValue(String value, float price) {
        if(value != null) {
            float p = Float.parseFloat(value);
            if(p != price) price = p;
        }
        return price;
    }
}
