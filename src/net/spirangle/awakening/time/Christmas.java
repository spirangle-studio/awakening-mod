package net.spirangle.awakening.time;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;

import java.util.logging.Logger;


@SuppressWarnings("unused")
public class Christmas {

    private static final Logger logger = Logger.getLogger(Christmas.class.getName());

    public static final int CHRISTMAS_GIFT_ID = 0x1002;
    public static final int CHRISTMAS_GIFT_MAX = 1;

    //	private static final int[] christmasPresents = { 0,4,6,7,8,9 };

    private static Christmas instance = null;

    public static Christmas getInstance() {
        if(instance == null) instance = new Christmas();
        return instance;
    }

    private Christmas() {

    }

    public static void loadChristmas() {
    }

    public static void handle_ASK_GIFT(Creature performer, Creature target) {
    }

    public static boolean handleChristmasTreeGift(Creature performer, Item target) {
        return true;
    }
}
