package net.spirangle.awakening.time;

import com.wurmonline.math.TilePos;
import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.Vehicle;
import com.wurmonline.server.behaviours.Vehicles;
import com.wurmonline.server.bodys.Body;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.NoArmourException;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.NoSpaceException;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.structures.Floor;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.constants.ItemMaterials;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.MessagePolicy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class Winter {

    private static final Logger logger = Logger.getLogger(Winter.class.getName());

    public static class TemperatureEffects {
        public double baseTemperatureDelta;
        public short swimMod;
        public short windMod;
        public short rainMod;
        public short altitudeMod;
        public short tileMod;
        public short averageModifiedTemperatureDelta;
        public short averageTemperature;

        public TemperatureEffects(double b, short s, short w, short r, short a, short t) {
            this.baseTemperatureDelta = b;
            this.swimMod = s;
            this.windMod = w;
            this.rainMod = r;
            this.altitudeMod = a;
            this.tileMod = t;
        }
    }


    private static final int[] bodyParts = {
            BodyTemplate.head,
            BodyTemplate.torso,
            BodyTemplate.leftArm,
            BodyTemplate.leftHand,
            BodyTemplate.rightArm,
            BodyTemplate.rightHand,
            BodyTemplate.legs,
            BodyTemplate.leftFoot,
            BodyTemplate.rightFoot
    };

    private static Winter instance = null;

    public static Winter getInstance() {
        if(instance == null) instance = new Winter();
        return instance;
    }

    public static boolean isWater(int tile, int tilex, int tiley, boolean surfaced) {
        if(surfaced) {
            for(int x = 0; x <= 1; ++x)
                for(int y = 0; y <= 1; ++y)
                    if(Tiles.decodeHeight(Server.surfaceMesh.getTile(tilex + x, tiley + y)) < 0) return true;
        } else {
            if(Tiles.isSolidCave(Tiles.decodeType(tile))) return false;
            for(int x = 0; x <= 1; ++x)
                for(int y = 0; y <= 1; ++y) {
                    int t = Server.caveMesh.getTile(tilex + x, tiley + y);
                    if(Tiles.decodeHeight(t) < 0) return true;
                }
        }
        return false;
    }

    public static boolean hasRoof(VolaTile tile) {
        Floor[] floors = tile.getFloors();
        for(int i = 0, n = floors.length; i < n; ++i)
            if(floors[i].isRoof()) return true;
        return false;
    }

    private boolean enableTemperatureSurvival;
    private boolean enableWaterDisease;
    private boolean newPlayerProtection;
    private boolean gmProtection;
    private boolean hardMode;
    private boolean noCropsInWinter;

    private Winter() {
        enableTemperatureSurvival = true;
        enableWaterDisease = true;
        newPlayerProtection = false;
        gmProtection = true;
        hardMode = false;
        noCropsInWinter = true;
    }

    public void configure(Properties properties) {
        enableTemperatureSurvival = Boolean.parseBoolean(properties.getProperty("enableTemperatureSurvival", "true"));
        enableWaterDisease = Boolean.parseBoolean(properties.getProperty("enableWaterDisease", "true"));
        newPlayerProtection = Boolean.parseBoolean(properties.getProperty("newPlayerProtection", "false"));
        gmProtection = Boolean.parseBoolean(properties.getProperty("gmProtection", "true"));
        hardMode = Boolean.parseBoolean(properties.getProperty("hardMode", "false"));
        noCropsInWinter = Boolean.parseBoolean(properties.getProperty("noCropsInWinter", "true"));
    }

    public MessagePolicy handleCommandMyTemp(Communicator communicator, String message) {
        Player player = communicator.getPlayer();
        if(enableTemperatureSurvival) {
            TemperatureEffects te = getTemperatureEffects(player);
            te = pollBodyPartTemperature(player, false, false, te);
            String m = "";
            if(!Seasons.getInstance().isCold()) m += "It is not cold enough to have to worry about it";
            else if(te.averageTemperature > 200) m += "You are hot.";
            else {
                if(te.averageTemperature == 0) m += "You are freezing cold,";
                else if(te.averageTemperature < 70) m += "You are very cold,";
                else if(te.averageTemperature < 130) m += "You are cold,";
                else if(te.averageTemperature < 180) m += "You are warm,";
                else m += "You are very warm,";

                m += (te.averageModifiedTemperatureDelta == 0 ||
                      (te.averageTemperature < 130 && te.averageModifiedTemperatureDelta < 0) ||
                      (te.averageTemperature >= 130 && te.averageModifiedTemperatureDelta > 0))? " and " : " but ";

                if(te.averageModifiedTemperatureDelta < -3) m += "you are rapidly getting colder.";
                else if(te.averageModifiedTemperatureDelta < 0) m += "you are getting colder.";
                else if(te.averageModifiedTemperatureDelta == 0) m += "this is unlikely to change.";
                else if(te.averageModifiedTemperatureDelta <= 3) m += "you are getting warmer.";
                else m += "you are rapidly getting warmer.";
            }
            communicator.sendNormalServerMessage(m);
        }
        return MessagePolicy.DISCARD;
    }

    public void init() {
        HookManager.getInstance().registerHook("com.wurmonline.server.players.Player", "poll", "()Z", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        if(enableTemperatureSurvival && Seasons.getInstance().isCold()) {
                            Player player = (Player) object;
							/*int toggleDay = (int)(WurmCalendar.currentTime%29030400L/86400L);
							toggleDay %= 28;
							double toggleStarfall = WurmCalendar.getStarfall();
							if((toggleStarfall!=11.0 || toggleDay<7) && toggleStarfall!=0.0) {
								enableTemperatureSurvival = false;
								AwakeningMod.info("Temperature survival mode disabled because starfall is " + toggleStarfall + " and day is " + toggleDay);
							}*/
                            if(!(newPlayerProtection && player.hasSpellEffect((byte) 75)) &&
                               !(gmProtection && player.getPower() >= MiscConstants.POWER_DEMIGOD) &&
                               !player.isDead() &&
                               ((int) player.getSecondsPlayed() % 15) == 0) {
/*							if((!player.hasSpellEffect((byte)75) || !newPlayerProtection) &&
							   (player.getPower()<MiscConstants.POWER_DEMIGOD || !gmProtection) && !player.isDead() &&
							   (player.secondsPlayed%15.0f)==0.0f) {*/
                                TemperatureEffects te = getTemperatureEffects(player);
                                pollBodyPartTemperature(player, true, true, te);
                            }
                        }
                        return method.invoke(object, args);
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.players.Player", "setDeathEffects", "(ZII)V", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Player player = (Player) object;
                        Body body = player.getBody();
                        for(int y : bodyParts) body.getBodyPart(y).setTemperature((short) 200);
                        return method.invoke(object, args);
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.MethodsItems", "eat", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;F)Z", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Creature player = (Creature) args[1];
                        if(!player.isPlayer()) return false;
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        if(enableTemperatureSurvival && !result) {
                            Action act = (Action) args[0];
                            Item food = (Item) args[2];
                            if(player.isPlayer() && (act.currentSecond() % 5) == 0 && food.getTemperature() > 1000) {
                                warmAllBodyParts((Player) player, (short) 5);
                                player.getCommunicator().sendNormalServerMessage("The " + food.getName() + " warms you up.");
                                logger.info(player.getName() + " is warmed by eating some " + food.getName());
                            }
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.MethodsItems", "drink", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;F)Z", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        Action act = (Action) args[0];
                        Creature player = (Creature) args[1];
                        Item drink = (Item) args[2];
                        if(enableTemperatureSurvival && !result && player.isPlayer() && (act.currentSecond() % 2) == 0 && drink.getTemperature() > 600) {
                            warmAllBodyParts((Player) player, (short) 5);
                            player.getCommunicator().sendNormalServerMessage("The " + drink.getName() + " warms you up.");
                            logger.info(player.getName() + " is warmed by drinking some " + drink.getName());
                        }
                        if(enableWaterDisease && (!player.hasSpellEffect((byte) 75) || !newPlayerProtection) &&
                           (player.getPower() < MiscConstants.POWER_DEMIGOD || !gmProtection) &&
                           !result && player.isPlayer() && (act.currentSecond() % 2) == 0 &&
                           drink.getTemplateId() == ItemList.water && drink.getCurrentQualityLevel() < 100.0f) {
                            player.setDisease((byte) (100.0f - drink.getCurrentQualityLevel()));
                            player.getCommunicator().sendNormalServerMessage("The " + drink.getName() + " tastes bad and you feel ill.", (byte) 4);
                            logger.info(player.getName() + " contracts a disease by drinking some bad " + drink.getName());
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.MethodsItems", "drink", "(Lcom/wurmonline/server/creatures/Creature;IIIFLcom/wurmonline/server/behaviours/Action;)Z", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        if(enableWaterDisease && !result) {
                            Action act = (Action) args[5];
                            Creature player = (Creature) args[0];
                            if(player.isPlayer() && (act.currentSecond() % 2) == 0) {
                                byte randomByte = (byte) Server.rand.nextInt(100);
                                byte diseaseAmount = (randomByte > player.getDisease())? randomByte : player.getDisease();
                                player.setDisease(diseaseAmount);
                                player.getCommunicator().sendNormalServerMessage("The water tastes bad and you feel ill.", (byte) 4);
                                logger.info(player.getName() + " contracts a disease by drinking some bad water.");
                            }
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.bodys.Body", "createBodyParts", "()V", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        if(enableTemperatureSurvival) {
                            Body body = (Body) proxy;
                            try {
                                Player nspe = Players.getInstance().getPlayer(body.getOwnerId());
                                if(nspe != null) {
                                    TemperatureEffects te = getTemperatureEffects(nspe);
                                    te = pollBodyPartTemperature(nspe, false, false, te);
                                    if(te.averageModifiedTemperatureDelta < 0) setTempAllBodyParts(body, (short) 90);
                                }
                            } catch(NoSuchPlayerException e) { }
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.MethodsItems", "fillContainer", "(Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/creatures/Creature;)V", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        if(enableWaterDisease) {
                            Item targetItem = (Item) args[0];
                            if(targetItem.getTemplateId() != ItemList.stoneWell && targetItem.getTemplateId() != ItemList.stoneFountain) {
                                for(Item contained : targetItem.getItems()) {
                                    if((!contained.isFood() && !contained.isLiquid()) ||
                                       (contained.isLiquid() && contained.getTemplateId() != ItemList.water))
                                        return result;
                                    if(!contained.isLiquid()) continue;
                                    contained.setQualityLevel(Math.max(1.0f, contained.getQualityLevel() - Server.rand.nextInt(10)));
                                }
                            }
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.items.Item", "modTemp", "(Lcom/wurmonline/server/items/Item;IZ)V", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Boolean result = (Boolean) method.invoke(proxy, args);
                        if(enableWaterDisease) {
                            Item item = (Item) args[0];
                            for(Item contained : item.getItems())
                                if((contained.isFood() || contained.isLiquid()) && (!contained.isLiquid() || contained.getTemplateId() == ItemList.water)) {
                                    if(contained.getTemperatureState(contained.getTemperature()) != 3 ||
                                       contained.getCurrentQualityLevel() >= 100.0f) continue;
                                    contained.setQualityLevel(100.0f);
                                }
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.behaviours.TileBehaviour", "action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;IIZISF)Z", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if(enableWaterDisease) {
                            Creature creature = (Creature) args[1];
                            int tileX = (int) args[2];
                            int tileY = (int) args[3];
                            int tile = (int) args[5];
                            short actionType = (short) args[6];
                            Communicator communicator = creature.getCommunicator();
                            switch(actionType) {
                                case 19:
                                    if(isWater(tile, tileX, tileY, creature.isOnSurface()))
                                        communicator.sendNormalServerMessage("The water tastes strange. It might need boiling.");
                                    else
                                        communicator.sendNormalServerMessage("The taste is very dry.");
                                    return true;
                            }
                        }
                        return method.invoke(proxy, args);
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.server.zones.CropTilePoller", "pollCropTiles", "()V", new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if(!noCropsInWinter || !Seasons.getInstance().isCold()) return method.invoke(proxy, args);
/*						int day = (int)(WurmCalendar.currentTime%29030400L/86400L);
						double starfall = WurmCalendar.getStarfall();
						if(!noCropsInWinter || (((day%28.0)<8.0 || starfall!=11.0) &&
						        ((day%28.0)>20.0 || starfall!=0.0))) return method.invoke(proxy,args);*/
                        return null;
                    }
                };
            }
        });
    }

    private void warmAllBodyParts(Player player, short change) {
        Body body = player.getBody();
        for(int y : bodyParts) {
            try {
                Item nse = body.getBodyPart(y);
                nse.setTemperature((short) (nse.getTemperature() + change));
            } catch(NoSpaceException e) {
                logger.log(Level.WARNING, "Winter: " + e.getMessage(), e);
            }
        }
    }

    private void setTempAllBodyParts(Body body, short temp) {
        for(int y : bodyParts) {
            try {
                Item nse = body.getBodyPart(y);
                nse.setTemperature(temp);
            } catch(NoSpaceException e) {
                logger.log(Level.WARNING, "Winter: " + e.getMessage(), e);
            }
        }
    }

    private TemperatureEffects pollBodyPartTemperature(Player player, boolean applyTemperatureAndWounds, boolean warningMessages, TemperatureEffects te) {
        try {
            String e = null;
            boolean urgentAlert = false;
            short totalTemperature = 0;
            double totalTemperatureDelta = 0.0;
            short countBodyParts = 0;
            Body body = player.getBody();
            for(int y : bodyParts) {
                try {
                    Item nse = body.getBodyPart(y);
                    short temperature = nse.getTemperature();
                    Item armour = null;
                    double armourGeneralBonus = 0.0;
                    double armourSwimBonus = 0.0;
                    double armourWindBonus = 0.0;
                    double armourRainBonus = 0.0;
                    double armourEffects = 0.0;
                    try {
                        armour = player.getArmour((byte) y);
                    } catch(NoArmourException e3) {
                        logger.info(e3.getMessage());
                    }
                    if(armour != null) {
                        switch(armour.getMaterial()) {
                            case ItemMaterials.MATERIAL_STEEL:
                            case ItemMaterials.MATERIAL_IRON:
                            case ItemMaterials.MATERIAL_ADAMANTINE:
                            case ItemMaterials.MATERIAL_GLIMMERSTEEL:
                            case ItemMaterials.MATERIAL_SERYLL:
                                armourGeneralBonus = 0.4;
                                break;
                            case ItemMaterials.MATERIAL_LEATHER:
                                armourGeneralBonus = 0.4;
                                armourWindBonus = 0.5;
                                armourRainBonus = 0.5;
                                break;
                            case ItemMaterials.MATERIAL_COTTON:
                                armourGeneralBonus = 0.6;
                                armourWindBonus = 0.25;
                                break;
                            case ItemMaterials.MATERIAL_WOOL:
                                armourGeneralBonus = 1.0;
                                armourRainBonus = 0.25;
                                armourSwimBonus = 1.0;
                                break;
                        }
                        switch(armour.getTemplateId()) {
                            case ItemList.brownBearHelm:
                                armourGeneralBonus = 1.0;
                                armourRainBonus = 0.25;
                                armourWindBonus = 0.0;
                                armourSwimBonus = 1.0;
                                break;
                            case ItemList.leatherHat0:
                                armourRainBonus = 0.75;
                                break;
                        }
                        armourGeneralBonus = armourGeneralBonus * 0.4 + armourGeneralBonus * 0.6 * (armour.getCurrentQualityLevel() / 100.0f);
                        armourRainBonus = armourRainBonus * 0.4 + armourRainBonus * 0.6 * (armour.getCurrentQualityLevel() / 100.0f);
                        armourWindBonus = armourWindBonus * 0.4 + armourWindBonus * 0.6 * (armour.getCurrentQualityLevel() / 100.0f);
                        armourSwimBonus = armourSwimBonus * 0.4 + armourSwimBonus * 0.6 * (armour.getCurrentQualityLevel() / 100.0f);
                        logger.info(player.getName() + " - " + nse.getName() + "(" + temperature + ") slot: " + armour.getName());
                    }
                    armourEffects = armourGeneralBonus
                                    + Math.min(0.0, te.swimMod + armourSwimBonus)
                                    + Math.min(0.0, te.rainMod + armourRainBonus)
                                    + Math.min(0.0, te.windMod + armourWindBonus)
                                    + te.altitudeMod
                                    + te.tileMod;
                    double doubleDelta = te.baseTemperatureDelta + armourEffects;
                    short temperatureDelta = (short) Math.round(doubleDelta);
                    totalTemperatureDelta += temperatureDelta;
                    temperature = (short) Math.min(250, Math.max(0, Math.round(temperature + temperatureDelta)));
                    logger.info(player.getName() + " - double delta: " + doubleDelta + " rounded to " + temperatureDelta + ", temperature " + temperature + " (" + applyTemperatureAndWounds + ", " + warningMessages + ")");
                    if(applyTemperatureAndWounds) nse.setTemperature(temperature);
                    totalTemperature += temperature;
                    ++countBodyParts;
                    if(temperatureDelta < 0) {
                        if(warningMessages && temperature < 50 && e == null) {
                            e = "You are very cold and should find warmth";
                            urgentAlert = true;
                        }
                    } else if(temperature < 50) {
                        e = "You are warming up.";
                    }
                    if(applyTemperatureAndWounds && temperature == 0 && Server.rand.nextInt(1000) > 750) {
                        int dmg = Server.rand.nextInt(2000);
                        if((player.secondsPlayed % 30.0f) == 0.0f)
                            player.addWoundOfType(null, (byte) 8, y, false, 1.0f, false, dmg, 0.0f, 0.0f, false, false);
                        if(warningMessages) {
                            e = "You are freezing cold! Find warmth quickly!";
                            urgentAlert = true;
                        }
                    }
                } catch(NoSpaceException e2) {
                    logger.log(Level.WARNING, "Winter: " + e2.getMessage(), e2);
                }
            }
            if(urgentAlert) player.getCommunicator().sendNormalServerMessage(e, (byte) 4);
            else if(e != null) player.getCommunicator().sendNormalServerMessage(e);
            te.averageModifiedTemperatureDelta = (short) (totalTemperatureDelta / countBodyParts);
            te.averageTemperature = (short) (totalTemperature / countBodyParts);
            return te;
        } catch(Exception e) {
            logger.log(Level.WARNING, "Winter: " + e.getMessage(), e);
            return null;
        }
    }

    private TemperatureEffects getTemperatureEffects(Player player) {
        try {
            int tileX = player.getCurrentTile().getTileX();
            int tileY = player.getCurrentTile().getTileY();
            double hour = WurmCalendar.getHour();
            int day = (int) (WurmCalendar.currentTime % 29030400L / 86400L);
            double starfall = WurmCalendar.getStarfall() + day % 28.0 / 28.0;
            boolean isIndoors = player.getCurrentTile().getStructure() != null && player.getCurrentTile().getStructure().isFinished();
            boolean isInCave = !player.getCurrentTile().isOnSurface();
            boolean isUnderRoof = hasRoof(player.getCurrentTile());
            boolean isOnBoat = false;
            if(player.getVehicle() != -10L) {
                Vehicle monthTempMod = Vehicles.getVehicleForId(player.getVehicle());
                if(!monthTempMod.isCreature() && Items.getItem(player.getVehicle()).isBoat()) isOnBoat = true;
            }
            double arg40 = 4.0 * Math.sin((starfall - 3.0) / 1.91);
            double hourTempMod = 2.0 * Math.sin((hour - 6.0) / 3.82);
            int windMod = (!isIndoors && !isInCave && Math.abs(Server.getWeather().getWindPower()) > 0.3)? -1 : 0;
            int swimMod = (!isOnBoat && Zones.calculateHeight(player.getPosX(), player.getPosY(), player.isOnSurface()) < 0.0f)? -3 : 0;
            int rainMod = (!isInCave && !isUnderRoof && Server.getWeather().getRain() > 0.5)? -1 : 0;
            int altitudeMod = (Zones.calculateHeight(player.getPosX(), player.getPosY(), player.isOnSurface()) > 180.0f)? -1 : 0;
            int tileMod = (player.isOnSurface() && Tiles.decodeType(Server.surfaceMesh.getTile(tileX, tileY)) == Tiles.Tile.TILE_SNOW.id)? -1 : 0;
            double baseTemperatureDelta = arg40 + hourTempMod;
            if(!hardMode) ++baseTemperatureDelta;
            logger.info(player.getName() + " has following modifiers... calendar mod: " + arg40 + ", day/night mod: " + hourTempMod + ", windMod : " + windMod + ", swimMod: " + swimMod + ", rainMod: " + rainMod + ", altitudeMod: " + altitudeMod + ", tileMod: " + tileMod + ", hardMode: " + hardMode + ", in cave: " + isInCave + ", indoors: " + isIndoors + ", roof: " + isUnderRoof);
            byte dist = 5;
            int x1 = Zones.safeTileX(tileX - dist);
            int x2 = Zones.safeTileX(tileX + dist);
            int y1 = Zones.safeTileY(tileY - dist);
            int y2 = Zones.safeTileY(tileY + dist);
            short targetTemperature = 0;
            for(TilePos tPos : TilePos.areaIterator(x1, y1, x2, y2)) {
                int xx = tPos.x;
                int yy = tPos.y;
                VolaTile t = Zones.getTileOrNull(xx, yy, player.isOnSurface());
                if(t != null) {
                    for(Item item : t.getItems()) {
                        short effectiveTemperature = 0;
                        if(item.isOnFire()) {
                            effectiveTemperature = (short) (item.getTemperature() * (1.0 / Math.pow(Math.max(1.0, Math.sqrt(Math.pow(Math.abs(tileX - xx), 2.0) + Math.pow(Math.abs(tileY - yy), 2.0))), 2.0)));
                            if(item.isLight()) effectiveTemperature = (short) Math.round(effectiveTemperature / 6);
                        }
                        if(effectiveTemperature > targetTemperature) targetTemperature = effectiveTemperature;
                    }
                }
            }
            baseTemperatureDelta += (short) Math.round(Math.min(7.0, targetTemperature / 1800.0));
            return new TemperatureEffects(baseTemperatureDelta, (short) swimMod, (short) windMod, (short) rainMod, (short) altitudeMod, (short) tileMod);
        } catch(Exception e) {
            logger.log(Level.WARNING, "Winter: " + e.getMessage(), e);
            return null;
        }
    }
}
