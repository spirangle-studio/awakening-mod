package net.spirangle.awakening.time;

import com.wurmonline.server.Server;
import com.wurmonline.server.WurmCalendar;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.zones.CropTilePoller;
import net.spirangle.awakening.AwakeningMod;
import net.spirangle.awakening.Config;
import org.gotti.wurmunlimited.modloader.interfaces.MessagePolicy;

import java.lang.reflect.Field;
import java.time.Year;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.DAY_AS_MILLIS;


public class Seasons {

    private static final Logger logger = Logger.getLogger(Seasons.class.getName());

    public static final int SPRING = 0;
    public static final int EARLY_SUMMER = 1;
    public static final int LATE_SUMMER = 2;
    public static final int AUTUMN = 3;
    public static final int WINTER = 4;
    public static final int SEASONS = 5;

    public static final int DAYS_IN_WEEK = 7;
    public static final int DAYS_IN_YEAR = 7 * 4 * 12;


    private static Field lastPolledTilesField;

    public static int getDayOfYear() {
        return WurmCalendar.getStarfall() * 28 + (int) (WurmCalendar.getCurrentTime() % 2419200L / 86400L);
    }

    @SuppressWarnings("unused")
    public static boolean isHalloween() {
        int year = Year.now().getValue();
        if(AwakeningMod.debug) return true;
        return WurmCalendar.nowIsBetween(0, 0, 29, Calendar.OCTOBER, year, 23, 59, 4, Calendar.NOVEMBER, year);
    }

    private static Seasons instance = null;

    public static Seasons getInstance() {
        if(instance == null) instance = new Seasons();
        return instance;
    }

    private class Day {
        public final int day;
        public int season;
        public boolean cold;

        public Day(int d) { day = d; }
    }


    private int year;
    private float averageTemperature;
    private float summerWinterDeltaT;
    private final Day[] almanac;

    private Seasons() {
        averageTemperature = 9.0f;
        summerWinterDeltaT = 24.0f;
        almanac = new Day[DAYS_IN_YEAR];
        for(int i = 0; i < almanac.length; ++i) almanac[i] = new Day(i);
    }

    public void init() {
        long time = WurmCalendar.getCurrentTime();
        Random random = new Random();
        random.setSeed(time / 29030400L);
        averageTemperature = 9.0f + (random.nextFloat() - 0.5f);
        summerWinterDeltaT = 24.0f + (random.nextFloat() - 0.5f);
        year = WurmCalendar.getYear();
        long t = time - (time % 29030400L);
        float temp;
        int d, i, s, n;
        for(d = 0; d < DAYS_IN_YEAR; ++d, t += 86400L) {
            temp = getYearCycleTemp(t);
            almanac[d].season = selectSeason(t, temp);
        }
        for(d = 0; d < DAYS_IN_YEAR; ++d) {
            s = almanac[d].season;
            if(s == WINTER) almanac[d].cold = true;
            else if(s == SPRING) {
                for(i = d - 1, n = 1; almanac[i].season == SPRING; --i) ++n;
                almanac[d].cold = (n <= Config.daysColdAfterSnow);
            } else if(s == AUTUMN) {
                for(i = d + 1, n = 1; almanac[i].season == AUTUMN; ++i) ++n;
                almanac[d].cold = (n <= Config.daysColdAfterSnow);
            } else almanac[d].cold = false;
        }
        String m = "";
        String c = "SELAW";
        for(d = 0; d < DAYS_IN_YEAR; ++d) {
            m += " " + c.charAt(almanac[d].season) + (almanac[d].cold? '-' : '+');
            if((d % 28) == 27) {
                logger.info("Month:" + m);
                m = "";
            } else if((d % 7) == 6) m += "; ";
        }

        try {
            lastPolledTilesField = CropTilePoller.class.getDeclaredField("lastPolledTiles");
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Seasons: " + e.getMessage(), e);
        }
    }

    private float getYearCycleTemp(long time) {
        float yearFraction = time % 2.90304E7f / 2.90304E7f;
        return averageTemperature + summerWinterDeltaT * 0.5f * (float) (-Math.cos(6.283185307179586 * yearFraction));
    }

    private int selectSeason(long time, float yearCycleTemp) {
        int week = (int) (time % 29030400L / 604800L);
        if(yearCycleTemp > 10.0f) return week < 24? EARLY_SUMMER : LATE_SUMMER;
        else if(yearCycleTemp < -2.0f) return WINTER;
        return week < 24? SPRING : AUTUMN;
    }

    public void onPlayerLogin(Player player) {
        if(Config.showOnLogin && player != null) sendSeasonMessage(player.getCommunicator());
    }

    public static MessagePolicy handleCommandTime(Communicator communicator, String message) {
        getInstance().sendSeasonMessage(communicator);
        getInstance().sendFarmTickMessage(communicator);
        return MessagePolicy.PASS;
    }

    private void sendSeasonMessage(Communicator communicator) {
        if(WurmCalendar.getYear() != year) init();
        Day d = almanac[getDayOfYear()];
        int i, s = d.season, n = (s + 1) % SEASONS, l = getRemainingDays(d.day);
        String message = "";
        if(l <= Config.daysShifting) {
            message += "The seasons are shifting. " + Config.seasonNames[s] + " is fading away as " + Config.seasonNames[n] + " approaches";
            if(Config.showRemainingDays) message += " (" + l + (l == 1? " day remains" : " days remain") + ")";
            message += ".";
        } else {
            message += "It is currently " + Config.seasonNames[s] + ".";
            if(Config.showRemainingDays)
                message += " " + l + (l == 1? " day remains until " : " days remain until ") + Config.seasonNames[n] + ".";
        }
        if(d.cold) {
            for(i = (d.day + 1 < DAYS_IN_YEAR - 1? d.day + 1 : 0), n = 1; almanac[i].cold; i = (i < DAYS_IN_YEAR - 1? i + 1 : 0))
                ++n;
            message += " It is cold, and will remain so for " + n + " days.";
        } else if(s == AUTUMN) {
            for(i = (d.day + 1 < DAYS_IN_YEAR - 1? d.day + 1 : 0), n = 1; !almanac[i].cold; ++i) ++n;
            message += " Cold season will arrive in " + n + " days.";
        }
        communicator.sendNormalServerMessage(message);
    }

    private void sendFarmTickMessage(Communicator communicator) {
        try {
            long t = System.currentTimeMillis() - lastPolledTilesField.getLong(null);
            long h = t % DAY_AS_MILLIS;
            communicator.sendNormalServerMessage("Time until next farm tick is " + Server.getTimeFor(DAY_AS_MILLIS - h) + ".");
        } catch(IllegalAccessException e) {
        }
    }

    private int getRemainingDays(int day) {
        int s = almanac[day].season;
        for(int i = day + 1; i < almanac.length; ++i)
            if(almanac[i].season != s) return i - day;
        for(int i = 0; i < day; ++i)
            if(almanac[i].season != s) return (almanac.length - day) + i;
        return 0;
    }

    @SuppressWarnings("unused")
    public boolean isSpring() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].season == SPRING;
    }

    @SuppressWarnings("unused")
    public boolean isEarlySummer() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].season == EARLY_SUMMER;
    }

    @SuppressWarnings("unused")
    public boolean isLateSummer() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].season == LATE_SUMMER;
    }

    @SuppressWarnings("unused")
    public boolean isAutumn() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].season == AUTUMN;
    }

    public boolean isWinter() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].season == WINTER;
    }

    public boolean isCold() {
        if(WurmCalendar.getYear() != year) init();
        return almanac[getDayOfYear()].cold;
    }
}
