package net.spirangle.awakening.util;

import com.wurmonline.server.players.Player;
import com.wurmonline.server.utils.CreatureLineSegment;
import net.spirangle.awakening.players.Character;
import net.spirangle.awakening.players.PlayersData;


public class CharacterLineSegment extends CreatureLineSegment {
    private final Character character;

    public CharacterLineSegment(final Player player) {
        super(player);
        this.character = PlayersData.getInstance().getCharacter(player);
        if(this.character.getName() != null) {
            setText(this.character.getName());
        }
    }
}
