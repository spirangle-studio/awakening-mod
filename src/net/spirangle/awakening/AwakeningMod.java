package net.spirangle.awakening;

import com.wurmonline.server.Items;
import com.wurmonline.server.Message;
import com.wurmonline.server.Servers;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.Creatures;
import com.wurmonline.server.deities.Religion;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.ModSkills;
import com.wurmonline.server.villages.PvPAlliance;
import com.wurmonline.server.villages.Village;
import net.spirangle.awakening.actions.*;
import net.spirangle.awakening.creatures.*;
import net.spirangle.awakening.items.InventorySupplier;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.items.Vehicle;
import net.spirangle.awakening.kingdom.GuardTowers;
import net.spirangle.awakening.misc.PvpServerFix;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.players.Journal;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.players.Titles;
import net.spirangle.awakening.tasks.*;
import net.spirangle.awakening.time.Scheduler;
import net.spirangle.awakening.time.Seasons;
import net.spirangle.awakening.zones.Infestations;
import net.spirangle.awakening.zones.Portals;
import net.spirangle.awakening.zones.Treasures;
import org.gotti.wurmunlimited.modloader.interfaces.*;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreatures;
import org.gotti.wurmunlimited.modsupport.vehicles.ModVehicleBehaviours;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.*;


public class AwakeningMod implements WurmServerMod, Configurable, PreInitable, Initable,
        ItemTemplatesCreatedListener, ServerStartedListener, ServerShutdownListener,
        PlayerLoginListener, PlayerMessageListener,
        ChannelMessageListener, ServerPollListener {

    private static final Logger logger = Logger.getLogger(AwakeningMod.class.getName());

    public static final int DEV_SERVER_ID = 6083;
    public static final int AWAKENING_SERVER_ID = 205601;

    public static final int version = 0x000300;

    public static boolean verbose = false;
    public static boolean debug = false;

    @SuppressWarnings("unused")
    public static int[] parseIntegerArray(String str) {
        try {
            String[] arr = str.replace(" ", "").split(",");
            int[] iarr = new int[arr.length];
            for(int i = 0; i < arr.length; ++i)
                iarr[i] = Integer.parseInt(arr[i].trim());
            return iarr;
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Could not parse '" + str + "'.", e);
        }
        return null;
    }

    @SuppressWarnings("unused")
    public static int getConfigGroupCount(Properties properties, String id) {
        int i;
        for(i = 0; ; i += 10) {
            if(!properties.containsKey(id + i)) break;
        }
        if(i == 0) return -1;
        i -= 5;
        if(properties.containsKey(id + i)) {
            for(; ; ++i) if(!properties.containsKey(id + (i + 1))) return i;
        } else {
            for(--i; ; --i) if(properties.containsKey(id + i)) return i;
        }
    }

    @Override
    public void configure(Properties properties) {
        verbose = Boolean.parseBoolean(properties.getProperty("verbose", Boolean.toString(verbose)));
        debug = Boolean.parseBoolean(properties.getProperty("debug", Boolean.toString(debug)));
        if(debug) {
            try {
                logger.info("Sleeping 4 seconds...");
                Thread.sleep(4000);
            } catch(InterruptedException e) {
                logger.log(Level.SEVERE, "Could not pause: " + e.getMessage(), e);
            }
        }
        Config.getInstance().configure(properties);
    }

    @Override
    public void preInit() {
        CodeInjections.preInit();
        PvpServerFix.preInit();
        Titles.addNewTitles();
        ModActions.init();
    }

    @Override
    public void init() {
        ModVehicleBehaviours.init();
        ModCreatures.init();
        ModCreatures.addCreature(PumpkinKing.getInstance());
        ModCreatures.addCreature(new ShadeHound());
        ModCreatures.addCreature(new RedDeer());
        ModCreatures.addCreature(new Shade());
        ModCreatures.addCreature(new NightSteed());
        ModCreatures.addCreature(new DragonQueen());
        ModCreatures.addCreature(new DragonetBlack());
        ModCreatures.addCreature(new DragonetRed());
        ModCreatures.addCreature(new DragonetGreen());
        ModCreatures.addCreature(new Tarantula());

        ModCreatures.addCreature(new VillageGuard((byte) 1));
        ModCreatures.addCreature(new VillageGuard((byte) 2));
        ModCreatures.addCreature(new VillageGuard((byte) 3));
        ModCreatures.addCreature(new VillageGuard((byte) 4));
    }

    @Override
    public void onItemTemplatesCreated() {
        ItemTemplateCreatorAwakening.initItemTemplates();
        ItemTemplateCreatorAwakening.modifyItems();
    }

    @Override
    public void onServerStarted() {
        Config.getInstance().loadWurmIni();
        RemoteHandler.getInstance().start();

        switch(Servers.getLocalServerId()) {
            case AwakeningMod.DEV_SERVER_ID:
                verbose = true;
                debug = true;
                break;

            case AwakeningMod.AWAKENING_SERVER_ID:
            default:
                verbose = false;
                debug = false;
                break;
        }

        Task.registerTask(TASK_MESSAGE, MessageTask.class);
        Task.registerTask(TASK_ANNOUNCE, AnnounceTask.class);
        Task.registerTask(TASK_MERCHANT, MerchantTask.class);
        Task.registerTask(TASK_BARTENDER, BartenderTask.class);
        Task.registerTask(TASK_WAITER, WaiterTask.class);
        Task.registerTask(TASK_MISSION, MissionTask.class);
        Task.registerTask(TASK_VENDOR, VendorTask.class);
        Task.registerTask(TASK_KINGDOM_APPOINTMENTS, KingdomAppointmentsTask.class);
        Task.registerTask(TASK_CONVERT_FOLLOWER, ConvertFollowerTask.class);
        Task.registerTask(TASK_INITIATE_ADEPT, InitiateAdeptTask.class);

        AwakeningDb.init();
        PvpServerFix.init();
        ModSkills.init();
        Servant.init();
        GuardTowers.init();
        Scheduler.getInstance().start();
        Seasons.getInstance().init();
        InventorySupplier.getInstance().init();
        ChatChannels.getInstance().init();
        Infestations.getInstance().init();
        Journal.getInstance().init();

        CreatureTemplateCreatorAwakening.init();

        ItemTemplateCreatorAwakening.initCreationEntries();

        ShadeHound.init();
        RedDeer.init();
        Shade.init();
        NightSteed.init();
        DragonQueen.init();
        DragonetBlack.init();
        DragonetRed.init();
        DragonetGreen.init();
        Tarantula.init();
        VillageGuard.init((byte) 1);
        VillageGuard.init((byte) 2);
        VillageGuard.init((byte) 3);
        VillageGuard.init((byte) 4);

        ModActions.registerAction(new UseAction());
        ModActions.registerAction(new CombineFragmentsAction());
        ModActions.registerAction(new LowerCeilingCornerAction());

        ModActions.registerAction(new EconomyAction());
        ModActions.registerAction(new CharacterAction());
        ModActions.registerAction(new CombatAction());
        ModActions.registerAction(new LeaderBoardAction());
        ModActions.registerAction(new SettingsAction());
        ModActions.registerAction(new MarketAction());


        // Servant actions:
        ModActions.registerAction(new ManageServantAction());
        ModActions.registerAction(new GiveToAction());
        ModActions.registerAction(new TalkToAction());
        ModActions.registerAction(new WriteTaskAction());
        ModActions.registerAction(new TrickOrTreatAction());

        ModActions.registerAction(new ExorciseAction());

        Religion.init();

        PlayersData.getInstance().loadPlayersData();
        KingdomAppointmentsTask.loadApplications();

        long lockDecayTime = System.currentTimeMillis() - 90L * AwakeningConstants.DAY_AS_MILLIS;
        Treasures treasures = Treasures.getInstance();
        treasures.init();
        for(Item item : Items.getAllItems()) {
            int templId = item.getTemplate().getTemplateId();
            Vehicle.decayAbandonedLock(item, lockDecayTime);
            if(templId == ItemTemplateCreatorAwakening.mapFragment)
                treasures.addMapFragment(item);
            else if(templId == ItemTemplateCreatorAwakening.treasureMap)
                treasures.addTreasureMap(item);
            else if(item.isEpicPortal() || item.isServerPortal())
                Portals.getInstance().addPortal(item);
        }

        for(Creature creature : Creatures.getInstance().getCreatures()) {
            int templId = creature.getTemplate().getTemplateId();
            if(templId == CreatureTemplateCreatorAwakening.DRAGON_QUEEN_CID)
                DragonQueen.addDragonQueen(creature);
        }

        logger.info("Local server Id: " + Servers.getLocalServerId());
    }

    @Override
    public void onServerShutdown() {
        try(Connection con = ModSupportDb.getModSupportDb()) {
            while(!ModSkills.getInstance().saveSkills(con)) ;
            while(!PlayersData.getInstance().savePlayersData(con)) ;
            while(!Infestations.getInstance().saveInfestations(con)) ;
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save data: " + e.getMessage(), e);
        }
    }

    @Override
    public void onPlayerLogin(Player player) {
        PlayersData.getInstance().onPlayerLogin(player);
        ChatChannels.getInstance().onPlayerLogin(player);
        Seasons.getInstance().onPlayerLogin(player);
    }

    @Override
    public MessagePolicy onPlayerMessage(Communicator communicator, String message, String title) {
        char c = message.charAt(0);
        MessagePolicy m = MessagePolicy.PASS;
        if(c == '#' || c == '/') {
            m = CommandHandler.getInstance().handleCommand(communicator, message, title);
        }
        if(m == MessagePolicy.DISCARD) return m;
        return ChatChannels.getInstance().onPlayerMessage(communicator, message, title);
    }

    @Deprecated
    @Override
    public boolean onPlayerMessage(Communicator communicator, String msg) { return false; }

    @Override
    public MessagePolicy onKingdomMessage(Message message) {
        return ChatChannels.getInstance().onKingdomMessage(message);
    }

    @Override
    public MessagePolicy onVillageMessage(Village village, Message message) {
        return MessagePolicy.PASS;
    }

    @Override
    public MessagePolicy onAllianceMessage(PvPAlliance alliance, Message message) {
        return MessagePolicy.PASS;
    }

    @Override
    public void onServerPoll() {
        ChatChannels.getInstance().onServerPoll();
    }
}
