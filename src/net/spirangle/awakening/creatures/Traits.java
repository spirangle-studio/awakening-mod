package net.spirangle.awakening.creatures;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.items.Item;
import com.wurmonline.shared.constants.CreatureTypes;
import net.spirangle.awakening.Config;


public class Traits {

    public static final byte C_MOD_CURSED = 12;

    private static final String[] modtypes = {
            null, "fierce ", "angry ", "raging ", "slow ", "alert ", "greenish ", "lurking ", "sly ", "hardened ", "scared ", "diseased ", "cursed ",
            };

    private static final int[] modtypesColorR = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 250, };
    private static final int[] modtypesColorG = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 128, };
    private static final int[] modtypesColorB = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 201, };

    @SuppressWarnings("unused")
    public static void milk(Item milk, Creature performer, Creature target) {
        if(!target.hasTraits()) return;
        float weightMod = 1.0f;
        float qlMod = 1.0f;
        if(target.hasTrait(0)) weightMod *= 0.9f;    /* It will fight fiercely. */
        if(target.hasTrait(2)) weightMod *= 0.95f;   /* It is a tough bugger. */
        if(target.hasTrait(3)) weightMod *= 1.3f;    /* It has a strong body. */
        if(target.hasTrait(5)) weightMod *= 1.4f;    /* It can carry more than average. */
        if(target.hasTrait(6)) weightMod *= 1.25f;   /* It has very strong leg muscles. */
        if(target.hasTrait(8)) weightMod *= 0.8f;    /* It has malformed hindlegs. */
        if(target.hasTrait(9)) weightMod *= 0.8f;    /* The legs are of different length. */
        if(target.hasTrait(10)) weightMod *= 0.95f;  /* It seems overly aggressive. */
        if(target.hasTrait(11)) weightMod *= 1.2f;   /* It looks very unmotivated. */
        if(target.hasTrait(12)) qlMod *= 1.25f;      /* It is unusually strong willed. */
        if(target.hasTrait(13)) qlMod *= 0.8f;       /* It has some illness. */
        if(target.hasTrait(14)) weightMod *= 1.5f;   /* It looks constantly hungry. */
        if(target.hasTrait(19)) qlMod *= 0.95f;      /* It looks feeble and unhealthy. */
        if(target.hasTrait(20)) qlMod *= 1.4f;       /* It looks unusually strong and healthy. */
        if(target.hasTrait(21)) qlMod *= 1.3f;       /* It has a certain spark in its eyes. */
        if(weightMod != 1.0f) milk.setWeight((int) (weightMod * (float) milk.getWeightGrams()), true);
        if(qlMod != 1.0f) milk.setQualityLevel(Math.max(1.0f, Math.min(100.0f, qlMod * milk.getQualityLevel())));
    }

    @SuppressWarnings("unused")
    public static void shear(Item wool, Creature performer, Creature target) {
        if(!target.hasTraits()) return;
        float weightMod = 1.0f;
        float qlMod = 1.0f;
        if(target.hasTrait(0)) weightMod *= 1.2f;   /* It will fight fiercely. */
        if(target.hasTrait(1)) weightMod *= 0.85f;  /* It has fleeter movement than normal. */
        if(target.hasTrait(2)) weightMod *= 1.25f;  /* It is a tough bugger. */
        if(target.hasTrait(3)) qlMod *= 1.2f;       /* It has a strong body. */
        if(target.hasTrait(4)) weightMod *= 0.75f;  /* It has lightning movement. */
        if(target.hasTrait(7)) weightMod *= 0.75f;  /* It has keen senses. */
        if(target.hasTrait(10)) weightMod *= 1.3f;  /* It seems overly aggressive. */
        if(target.hasTrait(11)) weightMod *= 1.5f;  /* It looks very unmotivated. */
        if(target.hasTrait(12)) weightMod *= 1.4f;  /* It is unusually strong willed. */
        if(target.hasTrait(13)) qlMod *= 0.9f;      /* It has some illness. */
        if(target.hasTrait(19)) qlMod *= 0.85f;     /* It looks feeble and unhealthy. */
        if(target.hasTrait(20)) qlMod *= 1.5f;      /* It looks unusually strong and healthy. */
        if(target.hasTrait(21)) qlMod *= 1.3f;      /* It has a certain spark in its eyes. */
        if(weightMod != 1.0f) wool.setWeight((int) (weightMod * (float) wool.getWeightGrams()), true);
        if(qlMod != 1.0f) wool.setQualityLevel(Math.max(1.0f, Math.min(100.0f, qlMod * wool.getQualityLevel())));
    }

    @SuppressWarnings("unused")
    public static float getSizeMod(Creature creature) {
        int templId = creature.getTemplate().getTemplateId();
        float sizeMod = 1.0f;
        byte modtype = creature.getStatus().getModType();
        if(modtype == C_MOD_CURSED) sizeMod *= 1.5f;
        if(!creature.hasTraits() || creature.getStatus().getModType() != 0 || creature.isVehicle() ||
           templId == CreatureTemplateIds.FOAL_CID || templId == CreatureTemplateIds.HELL_FOAL_CID || templId == CreatureTemplateIds.UNICORN_FOAL_CID)
            return sizeMod;
        if(creature.hasTrait(3)) sizeMod *= 1.06f;   /* It has a strong body. */
        if(creature.hasTrait(5)) sizeMod *= 1.06f;   /* It can carry more than average. */
        if(creature.hasTrait(6)) sizeMod *= 1.06f;   /* It has very strong leg muscles. */
        if(creature.hasTrait(8)) sizeMod *= 0.95f;   /* It has malformed hindlegs. */
        if(creature.hasTrait(10)) sizeMod *= 0.85f;  /* It seems overly aggressive. */
        if(creature.hasTrait(12)) sizeMod *= 0.95f;  /* It is unusually strong willed. */
        if(creature.hasTrait(13)) sizeMod *= 0.90f;  /* It has some illness. */
        if(creature.hasTrait(14)) sizeMod *= 1.11f;  /* It looks constantly hungry. */
        if(creature.hasTrait(19)) sizeMod *= 0.95f;  /* It looks feeble and unhealthy. */
        if(creature.hasTrait(20)) sizeMod *= 1.08f;  /* It looks unusually strong and healthy. */
        if(sizeMod != 1.0f) {
            if(templId == CreatureTemplateIds.BISON_CID || templId == CreatureTemplateCreatorAwakening.RED_DEER_CID) {
                sizeMod = 1.0f + (sizeMod - 1.0f) * 0.35f;
            }
        }
        return sizeMod;
    }

    @SuppressWarnings("unused")
    public static String getTypeString(byte modtype) {
        if(modtype == CreatureTypes.C_MOD_CHAMPION) return "champion ";
        if(modtype <= CreatureTypes.C_MOD_NONE || modtype >= modtypes.length) return "";
        return modtypes[modtype];
    }

    @SuppressWarnings("unused")
    public static final byte getModtypeForString(String corpseString) {
        if(corpseString.contains(" fierce ")) return CreatureTypes.C_MOD_FIERCE;
        if(corpseString.contains(" angry ")) return CreatureTypes.C_MOD_ANGRY;
        if(corpseString.contains(" raging ")) return CreatureTypes.C_MOD_RAGING;
        if(corpseString.contains(" slow ")) return CreatureTypes.C_MOD_SLOW;
        if(corpseString.contains(" alert ")) return CreatureTypes.C_MOD_ALERT;
        if(corpseString.contains(" greenish ")) return CreatureTypes.C_MOD_GREENISH;
        if(corpseString.contains(" lurking ")) return CreatureTypes.C_MOD_LURKING;
        if(corpseString.contains(" sly ")) return CreatureTypes.C_MOD_SLY;
        if(corpseString.contains(" hardened ")) return CreatureTypes.C_MOD_HARDENED;
        if(corpseString.contains(" scared ")) return CreatureTypes.C_MOD_SCARED;
        if(corpseString.contains(" diseased ")) return CreatureTypes.C_MOD_DISEASED;
        if(corpseString.contains(" cursed ")) return C_MOD_CURSED;
        if(corpseString.contains(" champion ")) return CreatureTypes.C_MOD_CHAMPION;
        return CreatureTypes.C_MOD_NONE;
    }

    @SuppressWarnings("unused")
    public static boolean hasCustomColor(byte modtype) {
        return modtype >= CreatureTypes.C_MOD_FIERCE && modtype <= C_MOD_CURSED;
    }

    @SuppressWarnings("unused")
    public static byte getColorRed(byte modtype) {
        if(modtype >= 0 && modtype < modtypesColorR.length) return (byte) modtypesColorR[modtype];
        return -1;
    }

    @SuppressWarnings("unused")
    public static byte getColorGreen(byte modtype) {
        if(modtype >= 0 && modtype < modtypesColorG.length) return (byte) modtypesColorG[modtype];
        return -1;
    }

    @SuppressWarnings("unused")
    public static byte getColorBlue(byte modtype) {
        if(modtype >= 0 && modtype < modtypesColorB.length) return (byte) modtypesColorB[modtype];
        return -1;
    }

    @SuppressWarnings("unused")
    public static float getBattleRatingTypeModifier(byte modtype, float br) {
        if(modtype == C_MOD_CURSED) return br * Config.cursedBattleRating;
        return br;
    }

    @SuppressWarnings("unused")
    public static float getDamageTypeModifier(byte modtype, float d) {
        if(modtype == C_MOD_CURSED) return d * Config.cursedDamage;
        return d;
    }

    @SuppressWarnings("unused")
    public static float getParryTypeModifier(byte modtype, float p) {
        if(modtype == C_MOD_CURSED) return p * Config.cursedParry;
        return p;
    }

    @SuppressWarnings("unused")
    public static float getDodgeTypeModifier(byte modtype, float d) {
        if(modtype == C_MOD_CURSED) return d * Config.cursedDodge;
        return d;
    }

    @SuppressWarnings("unused")
    public static float getAggTypeModifier(byte modtype, float a) {
        if(modtype == C_MOD_CURSED) return a * Config.cursedAgg;
        return a;
    }
}
