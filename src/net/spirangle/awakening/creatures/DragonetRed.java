package net.spirangle.awakening.creatures;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.EncounterType;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.EncounterBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.DRAGONET_RED_CID;


public class DragonetRed implements ModCreature {

    private static final Logger logger = Logger.getLogger(DragonetRed.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        int[] types = {
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_AGG_HUMAN,
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_MONSTER,
                CreatureTypes.C_TYPE_FENCEBREAKER,
                CreatureTypes.C_TYPE_ANIMAL
        };
        int[] itemsButchered = {
                ItemList.animalHide,
                ItemList.tail,
                ItemList.tooth,
                ItemList.heart
        };

        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(DRAGONET_RED_CID);

        builder.name("Red dragonet");
        builder.description("The dragonet is a smaller relative of the mighty dragons. Among dragonets, the red ones are most vicious.");
        builder.modelName("model.creature.dragon.red");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_DRAGON);
        builder.vision((short) 8);
        builder.sex((byte) 0);
        builder.dimension((short) 120, (short) 90, (short) 285);
        builder.sizeModifier(32, 32, 32);
        builder.deathSounds(SoundNames.DEATH_DRAGON_SND, SoundNames.DEATH_DRAGON_SND);
        builder.hitSounds(SoundNames.HIT_DRAGON_SND, SoundNames.HIT_DRAGON_SND);
        builder.naturalArmour(0.25f);
        builder.damages(17.0f, 19.0f, 46.0f, 45.0f, 0.0f);
        builder.speed(1.8f);
        builder.moveRate(500);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(40);
        builder.aggressive(99);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_DRAGON);
        builder.headbuttDamString("tailwhip");
        builder.handDamString("bite");
        builder.kickDamString("wingbuff");
        builder.alignment(-60.0f);
        builder.maxAge(200);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_LEATHER_DRAGON);
        builder.combatDamageType(Wound.TYPE_PIERCE);
        builder.maxGroupAttackSize(10);
        builder.baseCombatRating(32.0f);
        builder.bonusCombatRating(7);
        builder.maxPercentOfCreatures(0.00025f);

        builder.eggLayer(DRAGONET_RED_CID);

        builder.setCombatMoves(new int[]{ 1, 2, 3 });

        builder.skill(SkillList.BODY_STRENGTH, 38.0f);
        builder.skill(SkillList.BODY_CONTROL, 65.0f);
        builder.skill(SkillList.BODY_STAMINA, 57.0f);
        builder.skill(SkillList.MIND_LOGICAL, 30.0f);
        builder.skill(SkillList.MIND_SPEED, 35.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 50.0f);
        builder.skill(SkillList.SOUL_DEPTH, 28.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 70.0f);

        return builder;
    }

    @Override
    public void addEncounters() {
        new EncounterBuilder(Tiles.Tile.TILE_SAND.id, EncounterType.ELEVATION_BEACH).addCreatures(DRAGONET_RED_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_CAVE.id, EncounterType.ELEVATION_CAVES).addCreatures(DRAGONET_RED_CID, 1).build(1);
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(DRAGONET_RED_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "reddragon.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "DragonetRed: " + e.getMessage(), e);
        }
    }
}
