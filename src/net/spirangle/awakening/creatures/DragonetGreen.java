package net.spirangle.awakening.creatures;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.EncounterType;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.EncounterBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.DRAGONET_GREEN_CID;


public class DragonetGreen implements ModCreature {

    private static final Logger logger = Logger.getLogger(DragonetGreen.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        int[] types = {
                CreatureTypes.C_TYPE_OMNIVORE,
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_CLIMBER,
                CreatureTypes.C_TYPE_DOMINATABLE,
                CreatureTypes.C_TYPE_FLEEING,
                CreatureTypes.C_TYPE_MONSTER,
                CreatureTypes.C_TYPE_ANIMAL
        };
        int[] itemsButchered = {
                ItemList.animalHide,
                ItemList.tail,
                ItemList.tooth
        };

        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(DRAGONET_GREEN_CID);

        builder.name("Green dragonet");
        builder.description("The dragonet is a smaller relative of the mighty dragons. Among dragonets, the green ones are smallest.");
        builder.modelName("model.creature.drake.green");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_DRAGON);
        builder.vision((short) 7);
        builder.sex((byte) 0);
        builder.dimension((short) 80, (short) 50, (short) 215);
        builder.sizeModifier(24, 24, 24);
        builder.color(100, 180, 255);
        builder.deathSounds(SoundNames.DEATH_DRAGON_SND, SoundNames.DEATH_DRAGON_SND);
        builder.hitSounds(SoundNames.HIT_DRAGON_SND, SoundNames.HIT_DRAGON_SND);
        builder.naturalArmour(0.3f);
        builder.damages(3.0f, 3.0f, 12.0f, 13.0f, 0.0f);
        builder.speed(1.2f);
        builder.moveRate(300);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(10);
        builder.aggressive(10);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_DRAGON);
        builder.headbuttDamString("tailwhip");
        builder.handDamString("bite");
        builder.kickDamString("wingbuff");
        builder.alignment(0.0f);
        builder.maxAge(200);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_LEATHER);
        builder.combatDamageType(Wound.TYPE_PIERCE);
        builder.maxGroupAttackSize(3);
        builder.baseCombatRating(12.0f);
        builder.maxPercentOfCreatures(0.00025f);

        builder.eggLayer(DRAGONET_GREEN_CID);

        builder.setCombatMoves(new int[]{ 1, 2, 5 });

        builder.skill(SkillList.BODY_STRENGTH, 22.0f);
        builder.skill(SkillList.BODY_CONTROL, 6.0f);
        builder.skill(SkillList.BODY_STAMINA, 5.0f);
        builder.skill(SkillList.MIND_LOGICAL, 14.0f);
        builder.skill(SkillList.MIND_SPEED, 6.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 50.0f);
        builder.skill(SkillList.SOUL_DEPTH, 24.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 36.0f);

        return builder;
    }

    @Override
    public void addEncounters() {
        new EncounterBuilder(Tiles.Tile.TILE_MARSH.id, EncounterType.ELEVATION_GROUND).addCreatures(DRAGONET_GREEN_CID, 1).build(1);
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(DRAGONET_GREEN_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "greendragon.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "DragonetGreen: " + e.getMessage(), e);
        }
    }
}
