package net.spirangle.awakening.creatures;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.EncounterType;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.EncounterBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.SHADE_HOUND_CID;


public class ShadeHound implements ModCreature {

    private static final Logger logger = Logger.getLogger(ShadeHound.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_AGG_HUMAN,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_ANIMAL,
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_DETECTINVIS,
                CreatureTypes.C_TYPE_NON_NEWBIE
        };
        int[] itemsButchered = {
                ItemList.tooth,
                ItemList.heart,
                ItemList.paw
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(SHADE_HOUND_CID);

        builder.name("Shade Hound");
        builder.description("This wolf like creature has been tainted by the curse and wears a shroud of shadow. They are commonly used by Shades as spies.");
        builder.modelName("model.creature.quadraped.wolf.worg");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_DOG);
        builder.vision((short) 7);
        builder.sex((byte) 0);
        builder.dimension((short) 50, (short) 25, (short) 125);
        builder.sizeModifier(80, 80, 80);
        builder.color(50, 1, 30);
        builder.deathSounds(SoundNames.DEATH_WOLF_SND, SoundNames.DEATH_WOLF_SND);
        builder.hitSounds(SoundNames.HIT_WOLF_SND, SoundNames.HIT_WOLF_SND);
        builder.naturalArmour(0.35f);
        builder.damages(8.0f, 4.0f, 12.0f, 0.0f, 0.0f);
        builder.speed(1.7f);
        builder.moveRate(1700);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(40);
        builder.aggressive(100);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_CANINE);
        builder.handDamString("claw");
        builder.alignment(-60.0f);
        builder.maxAge(200);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_STUDDED);
        builder.combatDamageType(Wound.TYPE_PIERCE);
        builder.maxGroupAttackSize(6);
        builder.baseCombatRating(12.0f);
        builder.bonusCombatRating(5);
        builder.maxPercentOfCreatures(0.01f);
        builder.waterVulnerability(1.0f);

        builder.usesNewAttacks(true);
        builder.addPrimaryAttack(new AttackAction("bite", AttackIdentifier.BITE, new AttackValues(10.0f, 0.05f, 6.0f, 2, 1, Wound.TYPE_BITE, false, 3, 1.1f)));
        builder.addPrimaryAttack(new AttackAction("strike", AttackIdentifier.STRIKE, new AttackValues(7.0f, 0.01f, 6.0f, 3, 1, Wound.TYPE_SLASH, false, 2, 1.0f)));
        builder.addSecondaryAttack(new AttackAction("claw", AttackIdentifier.CLAW, new AttackValues(7.0f, 0.05f, 6.0f, 2, 1, Wound.TYPE_PIERCE, false, 8, 1.0f)));

        builder.skill(SkillList.BODY_STRENGTH, 35.0f);
        builder.skill(SkillList.BODY_CONTROL, 25.0f);
        builder.skill(SkillList.BODY_STAMINA, 40.0f);
        builder.skill(SkillList.MIND_LOGICAL, 10.0f);
        builder.skill(SkillList.MIND_SPEED, 15.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 40.0f);
        builder.skill(SkillList.SOUL_DEPTH, 12.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 50.0f);
        return builder;
    }

    @Override
    public void addEncounters() {
        new EncounterBuilder(Tiles.Tile.TILE_GRASS.id, EncounterType.ELEVATION_GROUND).addCreatures(SHADE_HOUND_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_TREE.id, EncounterType.ELEVATION_GROUND).addCreatures(SHADE_HOUND_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_MYCELIUM.id, EncounterType.ELEVATION_GROUND).addCreatures(SHADE_HOUND_CID, 1).build(1);
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(SHADE_HOUND_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "worg.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "ShadeHound: " + e.getMessage(), e);
        }
    }
}
