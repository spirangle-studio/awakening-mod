package net.spirangle.awakening.creatures;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.DRAGON_QUEEN_CID;


public class DragonQueen implements ModCreature {

    private static final Logger logger = Logger.getLogger(DragonQueen.class.getName());

    private static final List<Creature> dragonQueens = new ArrayList<>();

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        int[] types = {
                CreatureTypes.C_TYPE_SENTINEL,
                CreatureTypes.C_TYPE_INVULNERABLE,
                CreatureTypes.C_TYPE_UNIQUE,
                CreatureTypes.C_TYPE_DRAGON,
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_MONSTER,
                CreatureTypes.C_TYPE_DETECTINVIS,
                CreatureTypes.C_TYPE_FENCEBREAKER
        };
        int[] itemsButchered = {
                ItemList.boneCollar,
                ItemList.skull,
                ItemList.tooth,
                ItemList.eye,
                ItemList.eye,
                ItemList.gland
        };

        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(DRAGON_QUEEN_CID);

        builder.name("Dragon Queen");
        builder.description("The majestic Queen of the dragons, although she doesn't rule them, she speaks for all dragonkind.");
        builder.modelName("model.creature.dragon.white");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_DRAGON);
        builder.vision((short) 22);
        builder.sex((byte) 0);
        builder.dimension((short) 350, (short) 263, (short) 833);
        builder.sizeModifier(80, 80, 80);
        builder.color(255, 180, 50);
        builder.deathSounds(SoundNames.DEATH_DRAGON_SND, SoundNames.DEATH_DRAGON_SND);
        builder.hitSounds(SoundNames.HIT_DRAGON_SND, SoundNames.HIT_DRAGON_SND);
        builder.naturalArmour(0.25f);
        builder.damages(44.0f, 48.0f, 69.0f, 75.0f, 0.0f);
        builder.speed(1.5f);
        builder.moveRate(500);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(40);
        builder.aggressive(99);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_DRAGON);
        builder.headbuttDamString("tailwhip");
        builder.handDamString("bite");
        builder.kickDamString("wingbuff");
        builder.alignment(100.0f);
        builder.maxAge(200);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_SCALE_DRAGON);
        builder.combatDamageType(Wound.TYPE_PIERCE);
        builder.baseCombatRating(100.0f);
        builder.bonusCombatRating(50);

        builder.eggLayer(-1);

        builder.setCombatMoves(new int[]{ 1, 2, 3 });

        builder.skill(SkillList.BODY_STRENGTH, 100.0f);
        builder.skill(SkillList.BODY_CONTROL, 100.0f);
        builder.skill(SkillList.BODY_STAMINA, 100.0f);
        builder.skill(SkillList.MIND_LOGICAL, 100.0f);
        builder.skill(SkillList.MIND_SPEED, 100.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 100.0f);
        builder.skill(SkillList.SOUL_DEPTH, 100.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 100.0f);

        return builder;
    }

    @Override
    public void addEncounters() {
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(DRAGON_QUEEN_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "whitedragon.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "DragonQueen: " + e.getMessage(), e);
        }
    }

    public static void addDragonQueen(Creature creature) {
        if(creature.getTemplate().getTemplateId() == DRAGON_QUEEN_CID)
            dragonQueens.add(creature);
    }

    public static void poll() {
        dragonQueens.forEach(dragonQueen -> {
            if(dragonQueen.isHungry()) {
                try {
                    ItemTemplate template = ItemTemplateFactory.getInstance().getTemplate(ItemList.meat);
                    byte material = Materials.MATERIAL_MEAT_GAME;
                    Item dragonFood = ItemFactory.createItem(template.getTemplateId(), 100.0f, material, (byte) 3, null);
                    dragonFood.setWeight(5000, true);
                    dragonQueen.getInventory().insertItem(dragonFood);
                    dragonQueen.eat(dragonFood);
                } catch(NoSuchTemplateException | FailedException e) { }
            }
        });
    }
}
