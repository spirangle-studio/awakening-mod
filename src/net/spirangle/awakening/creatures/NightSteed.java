package net.spirangle.awakening.creatures;

import com.wurmonline.server.behaviours.Vehicle;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;
import org.gotti.wurmunlimited.modsupport.vehicles.ModVehicleBehaviour;
import org.gotti.wurmunlimited.modsupport.vehicles.VehicleFacade;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.NIGHT_STEED_CID;


public class NightSteed implements ModCreature {

    private static final Logger logger = Logger.getLogger(NightSteed.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_VEHICLE,
                CreatureTypes.C_TYPE_DOMESTIC,
                CreatureTypes.C_TYPE_ANIMAL,
                CreatureTypes.C_TYPE_LEADABLE,
                CreatureTypes.C_TYPE_GRAZER,
                CreatureTypes.C_TYPE_OMNIVORE,
                CreatureTypes.C_TYPE_DOMINATABLE,
                CreatureTypes.C_TYPE_NON_NEWBIE
        };
        int[] itemsButchered = {
                ItemList.tail,
                ItemList.hoof,
                ItemList.tallow,
                ItemList.animalHide,
                ItemList.bladder,
                ItemList.eye,
                ItemList.eye
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(NIGHT_STEED_CID);
        builder.name("Night Steed");
        builder.description("The Night Steed is a domesticated hell horse, with less demonic influence.");
        builder.modelName("model.creature.quadraped.horse.hell");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_HORSE);
        builder.vision((short) 3);
        builder.sex((byte) 0);
        builder.dimension((short) 180, (short) 50, (short) 250);
        builder.deathSounds(SoundNames.DEATH_HORSE_SND, SoundNames.DEATH_HORSE_SND);
        builder.hitSounds(SoundNames.HIT_HORSE_SND, SoundNames.HIT_HORSE_SND);
        builder.naturalArmour(1.0f);
        builder.damages(5.0f, 7.0f, 10.0f, 0.0f, 0.0f);
        builder.speed(1.7f);
        builder.moveRate(100);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(5);
        builder.aggressive(0);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_HORSE);
        builder.maxAge(200);
        builder.baseCombatRating(9.0f);
        builder.combatDamageType(Wound.TYPE_CRUSH);
        builder.alignment(100.0f);
        builder.maxGroupAttackSize(4);
        builder.onFire(false, (byte) 0);
        builder.glowing(false);
        builder.childTemplate(CreatureTemplateIds.HELL_FOAL_CID);
        builder.handDamString("kick");
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_CLOTH);
        builder.isHorse(true);

        builder.skill(SkillList.BODY_STRENGTH, 35.0f);
        builder.skill(SkillList.BODY_CONTROL, 20.0f);
        builder.skill(SkillList.BODY_STAMINA, 40.0f);
        builder.skill(SkillList.MIND_LOGICAL, 7.0f);
        builder.skill(SkillList.MIND_SPEED, 7.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 72.0f);
        builder.skill(SkillList.SOUL_DEPTH, 5.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 38.0f);
        return builder;
    }

    @Override
    public ModVehicleBehaviour getVehicleBehaviour() {
        return new ModVehicleBehaviour() {
            @Override
            public void setSettingsForVehicle(Item item, Vehicle vehicle) {
            }

            @Override
            public void setSettingsForVehicle(Creature creature, Vehicle v) {
                VehicleFacade vehicle = wrap(v);
                vehicle.createPassengerSeats(0);
                vehicle.setSeatFightMod(0, 0.7f, 0.9f);
                vehicle.setSeatOffset(0, 0.0f, 0.0f, 0.0f);
                vehicle.setCreature(true);
                vehicle.setSkillNeeded(31.0f);
                vehicle.setName(creature.getName());
                vehicle.setMaxDepth(-0.7f);
                vehicle.setMaxHeightDiff(0.04f);
                vehicle.setMaxSpeed(31.5f);
                vehicle.setCommandType((byte) 3);
                vehicle.setCanHaveEquipment(true);
            }
        };
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(NIGHT_STEED_CID);
            template.setColourNames(new String[]{ "ash", "cinder", "envious", "shadow", "pestilential", "nightshade", "incandescent", "molten" });
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "hellhorse.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "NightSteed: " + e.getMessage(), e);
        }
    }
}
