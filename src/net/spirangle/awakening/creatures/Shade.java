package net.spirangle.awakening.creatures;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Server;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.Materials;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.SHADE_CID;


public class Shade implements ModCreature {

    private static final Logger logger = Logger.getLogger(Shade.class.getName());


    private static class ShadeParty {
        long id;
        int num;
        int tileX;
        int tileY;
        long[] shades;

        public ShadeParty(long id, int num, int tileX, int tileY, long[] shades) {
            this.id = id;
            this.num = num;
            this.tileX = tileX;
            this.tileY = tileY;
            this.shades = shades;
        }
    }


    private static class ShadeItem {
        int templateId;
        float minQl;
        float maxQl;
        byte material;
        float chance;
        int rarityChance;

        public ShadeItem(int templateId, float minQl, float maxQl, byte material, float chance, int rarityChance) {
            this.templateId = templateId;
            this.minQl = minQl;
            this.maxQl = maxQl;
            this.material = material;
            this.chance = chance;
            this.rarityChance = rarityChance;
        }
    }


    private static final HashMap<Long, ShadeParty> shadeParties = new HashMap<Long, ShadeParty>();

    private static final ShadeItem[] weaponsRank1 = {
            new ShadeItem(ItemList.swordLong, 10.0f, 30.0f, Materials.MATERIAL_IRON, 40.0f, 100),
            new ShadeItem(ItemList.swordLong, 10.0f, 30.0f, Materials.MATERIAL_STEEL, 4.0f, 100),
            new ShadeItem(ItemList.axeSmall, 10.0f, 30.0f, Materials.MATERIAL_IRON, 25.0f, 120),
            new ShadeItem(ItemList.axeSmall, 10.0f, 30.0f, Materials.MATERIAL_STEEL, 3.0f, 120),
            new ShadeItem(ItemList.maulMedium, 10.0f, 30.0f, Materials.MATERIAL_IRON, 15.0f, 150),
            new ShadeItem(ItemList.maulMedium, 10.0f, 30.0f, Materials.MATERIAL_STEEL, 2.0f, 150),
            new ShadeItem(ItemList.swordTwoHander, 10.0f, 30.0f, Materials.MATERIAL_IRON, 10.0f, 200),
            new ShadeItem(ItemList.swordTwoHander, 10.0f, 30.0f, Materials.MATERIAL_STEEL, 1.0f, 200),
            };

    private static final ShadeItem[] weaponsRank2 = {
            new ShadeItem(ItemList.swordLong, 10.0f, 20.0f, Materials.MATERIAL_IRON, 50.0f, 100),
            new ShadeItem(ItemList.axeSmall, 10.0f, 20.0f, Materials.MATERIAL_IRON, 25.0f, 120),
            new ShadeItem(ItemList.maulMedium, 10.0f, 20.0f, Materials.MATERIAL_IRON, 15.0f, 150),
            new ShadeItem(ItemList.swordTwoHander, 10.0f, 20.0f, Materials.MATERIAL_IRON, 10.0f, 200),
            };

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_MOVE_GLOBAL,
                CreatureTypes.C_TYPE_AGG_HUMAN,
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_HUMAN,
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_OPENDOORS,
                CreatureTypes.C_TYPE_FENCEBREAKER,
                CreatureTypes.C_TYPE_REGENERATING,
                CreatureTypes.C_TYPE_CLIMBER,
                CreatureTypes.C_TYPE_UNDEAD,
                CreatureTypes.C_TYPE_GHOST,
                CreatureTypes.C_TYPE_DETECTINVIS,
                CreatureTypes.C_TYPE_NO_REBIRTH
        };
        int[] itemsButchered = {
                ItemList.heart
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(SHADE_CID);
        builder.name("Shade");
        builder.description("A member of a Nikodem shade party, out on a reconnaissance mission.");
        builder.modelName("model.creature.humanoid.human.guard.tower");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_HUMAN);
        builder.vision((short) 10);
        builder.sex((byte) 0);
        builder.dimension((short) 180, (short) 20, (short) 35);
        builder.deathSounds(SoundNames.DEATH_MALE_SND, SoundNames.DEATH_FEMALE_SND);
        builder.hitSounds(SoundNames.HIT_MALE_SND, SoundNames.HIT_FEMALE_SND);
        builder.naturalArmour(0.3f);
        builder.damages(6.0f, 7.0f, 0.0f, 0.0f, 0.0f);
        builder.speed(1.0f);
        builder.moveRate(200);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(30);
        builder.aggressive(80);
        builder.meatMaterial(Materials.MATERIAL_MEAT_HUMAN);
        builder.maxAge(200);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_CHAIN);
        builder.combatDamageType(Wound.TYPE_POISON);
        builder.maxGroupAttackSize(6);
        builder.baseCombatRating(12.0f);
        builder.hasHands(true);

        builder.skill(SkillList.BODY_STRENGTH, 20.0f);
        builder.skill(SkillList.BODY_CONTROL, 17.0f);
        builder.skill(SkillList.BODY_STAMINA, 21.0f);
        builder.skill(SkillList.MIND_LOGICAL, 15.0f);
        builder.skill(SkillList.MIND_SPEED, 15.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 15.0f);
        builder.skill(SkillList.SOUL_DEPTH, 17.0f);
        builder.skill(SkillList.SWORD_LONG, 45.0f);
        builder.skill(SkillList.AXE_LARGE, 45.0f);
        builder.skill(SkillList.MAUL_LARGE, 45.0f);
        builder.skill(SkillList.SWORD_TWOHANDED, 45.0f);
        builder.skill(SkillList.SHIELD_MEDIUM_WOOD, 45.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 45.0f);
        return builder;
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(SHADE_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "hordeofthesummonedtowerguard.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "Shade: " + e.getMessage(), e);
        }

        try(Connection con = ModSupportDb.getModSupportDb()) {
            try(PreparedStatement ps = con.prepareStatement("SELECT ID,NUMBER,TILEX,TILEY,CREATED FROM SCOUTPARTIES");
                PreparedStatement ps2 = con.prepareStatement("SELECT WURMID,PARTYID,CREATED FROM SCOUTS")) {
                try(ResultSet rs = ps.executeQuery()) {
                    while(rs.next()) {
                        long partyId = rs.getLong(1);
                        int num = rs.getInt(2);
                        int tilex = rs.getInt(3);
                        int tiley = rs.getInt(4);
                        long[] shades = new long[num];
                        for(int i = 0; i < num; ++i) shades[i] = -1;
                        ShadeParty sp = new ShadeParty(partyId, num, tilex, tiley, shades);
                        shadeParties.put(partyId, sp);
                        logger.info("Loaded shade party " + partyId + " of " + num + " members, at " + tilex + "," + tiley + ".");
                    }
                }
                try(ResultSet rs2 = ps2.executeQuery()) {
                    while(rs2.next()) {
                        long wurmId = rs2.getLong(1);
                        long partyId = rs2.getLong(2);
                        ShadeParty sp = shadeParties.get(partyId);
                        if(sp == null) continue;
                        for(int i = 0; i < sp.num; ++i)
                            if(sp.shades[i] < 0) {
                                sp.shades[i] = wurmId;
                                break;
                            }
                        logger.info("Loaded shade " + wurmId + " in party " + partyId + ".");
                    }
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load shade data: " + e.getMessage(), e);
        }
    }

    public static void sendList(Communicator communicator) {
        communicator.sendSafeServerMessage("Listing shade parties:");
        if(shadeParties.size() == 0) {
            communicator.sendSafeServerMessage("No shade parties exists.");
        } else {
            for(Map.Entry<Long, Shade.ShadeParty> entry : shadeParties.entrySet()) {
                Shade.ShadeParty sp = entry.getValue();
                communicator.sendSafeServerMessage("Shade party: id=" + sp.id + ", num=" + sp.num + ", x=" + sp.tileX + ", y=" + sp.tileY);
            }
        }
    }

    public static void poll() {
        for(Map.Entry<Long, ShadeParty> entry : shadeParties.entrySet()) {
            ShadeParty sp = entry.getValue();

            int kingdom = Zones.getKingdom(sp.tileX, sp.tileY);
            if(kingdom == 1 || kingdom == 2 || kingdom == 4) {
                Shade.disbandShadeParty(sp, false);
                continue;
            }

            int n = sp.num;
            for(int i = 0; i < sp.num; ++i) {
                if(sp.shades[i] < 0) --n;
                else {
                    Creature shade = null;
                    try {
                        shade = Creatures.getInstance().getCreature(sp.shades[i]);
                    } catch(NoSuchCreatureException e) { }
                    if(shade == null || shade.isDead()) {
                        sp.shades[i] = -1;
                        --n;
                    }
                }
            }
            if(n == 0) {
                Shade.disbandShadeParty(sp, true);
                Shade.createShadeParty(sp.num, sp.tileX, sp.tileY, true);
            }
        }
    }

    public static ShadeParty createShadeParty(int num, int tilex, int tiley, boolean surface) {
        if(num <= 0 || tilex < 0 || tilex >= Zones.worldTileSizeX || tiley < 0 || tiley >= Zones.worldTileSizeY)
            return null;
        ShadeParty shadeParty = null;
        long[] shades = new long[num];
        try {
            for(int i = 0; i < num; ++i) {
                float posx = tilex * 4 + 4.0f * Server.rand.nextFloat();
                float posy = tiley * 4 + 4.0f * Server.rand.nextFloat();
                int rot = Server.rand.nextInt(360);
                byte sex = (byte) (Server.rand.nextInt(2) == 0? 1 : 0);
                Creature shade = Creature.doNew(SHADE_CID, false, posx, posy, rot, surface? 0 : -1, "", sex, (byte) 3, (byte) 0, false);
                createItems(shade, i);
                shade.wearItems();
                shades[i] = shade.getWurmId();
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Error spawning shades: " + e.getMessage(), e);
            return null;
        }
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("INSERT INTO SCOUTPARTIES (ID,NUMBER,TILEX,TILEY,CREATED) VALUES(NULL,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            PreparedStatement ps2 = con.prepareStatement("INSERT INTO SCOUTS (WURMID,PARTYID,CREATED) VALUES(?,?,?)")) {
            int partyId;
            ps.setInt(1, num);
            ps.setInt(2, tilex);
            ps.setInt(3, tiley);
            ps.setLong(4, System.currentTimeMillis());
            int rows = ps.executeUpdate();
            if(rows == 0) throw new SQLException("Creating shade party failed, no rows affected.");
            try(ResultSet rs = ps.getGeneratedKeys()) {
                if(rs.next()) partyId = rs.getInt(1);
                else throw new SQLException("Creating shade party failed, no ID obtained.");
            }
            shadeParty = new ShadeParty(partyId, num, tilex, tiley, shades);
            for(int i = 0; i < shades.length; ++i) {
                ps2.setLong(1, shades[i]);
                ps2.setLong(2, partyId);
                ps2.setLong(3, System.currentTimeMillis());
                ps2.addBatch();
            }
            ps2.executeBatch();
            logger.info("Created a shade party of " + num + " members at " + tilex + "," + tiley + ".");
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to create shade party: " + e.getMessage(), e);
        }
        shadeParties.put(shadeParty.id, shadeParty);
        return shadeParty;
    }

    public static Item createItem(Creature shade, ShadeItem[] items) throws Exception {
        float pct = Server.rand.nextFloat() * 100.0f;
        for(int i = 0; i < items.length; ++i) {
            ShadeItem si = items[i];
            if(pct <= si.chance) {
                float ql = si.minQl + Server.rand.nextFloat() * (si.maxQl - si.minQl);
                byte rare = Server.rand.nextInt(si.rarityChance) == 0? MiscConstants.RARE : MiscConstants.COMMON;
                Item item = ItemFactory.createItem(si.templateId, ql, rare, (byte) 0, null);
                return item;
            }
            pct -= si.chance;
        }
        return null;
    }

    public static void createItems(Creature shade, int num) {
        try {
            Item inventory = shade.getInventory();
            ShadeItem[] weapons = num == 0? weaponsRank1 : (num <= 3? weaponsRank2 : null);
            if(weapons != null) {
                Item weapon = createItem(shade, weapons);
                if(weapon != null) inventory.insertItem(weapon);
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Failed to create shade items: " + e.getMessage(), e);
        }
    }

    public static void disbandShadeParty(Communicator communicator, long partyId) {
        Shade.ShadeParty sp = shadeParties.get(partyId);
        if(sp == null) {
            communicator.sendSafeServerMessage("No shade party exists with the id " + partyId + ".");
        } else {
            Shade.disbandShadeParty(sp, true);
            communicator.sendSafeServerMessage("Disbanded a shade party of " + sp.num + " members, at " + sp.tileX + "," + sp.tileY + ".");
        }
    }

    public static void disbandShadeParty(ShadeParty sp, boolean destroyShades) {
        for(int i = 0; i < sp.num; ++i) {
            if(sp.shades[i] >= 0) {
                Creature shade = null;
                try {
                    shade = Creatures.getInstance().getCreature(sp.shades[i]);
                } catch(NoSuchCreatureException e) { }
                if(destroyShades && shade != null && shade.isAlive()) {
                    shade.destroy();
                }
            }
        }
        logger.info("Disbanded a shade party of " + sp.num + " members at " + sp.tileX + "," + sp.tileY + ".");
        try(Connection con = ModSupportDb.getModSupportDb()) {
            try(PreparedStatement ps = con.prepareStatement("DELETE FROM SCOUTPARTIES WHERE ID=?");
                PreparedStatement ps2 = con.prepareStatement("DELETE FROM SCOUTS WHERE PARTYID=?")) {
                ps.setLong(1, sp.id);
                ps.execute();
                ps2.setLong(1, sp.id);
                ps2.execute();
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to disband shade party: " + e.getMessage(), e);
        }
        shadeParties.remove(sp.id);
    }
}
