package net.spirangle.awakening.creatures;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillList;
import net.spirangle.awakening.Config;


@SuppressWarnings("unused")
public class Offspring {
    public static int getChildTemplateId(Creature creature, int childTemplateId) {
        return childTemplateId;
    }

    public static int getAdultMaleTemplateId(Creature creature, int adultTemplateId) {
        if(adultTemplateId == CreatureTemplateIds.HELL_HORSE_CID)
            return getAdultHellHorseTemplate(creature);
        return adultTemplateId;
    }

    public static int getAdultFemaleTemplateId(Creature creature, int adultTemplateId) {
        if(adultTemplateId == CreatureTemplateIds.HELL_HORSE_CID)
            return getAdultHellHorseTemplate(creature);
        return adultTemplateId;
    }

    private static int getAdultHellHorseTemplate(Creature creature) {
        if(creature.isDominated() && creature.isBred()) {
            Creature dominator = creature.getDominator();
            if(dominator != null) {
                Skill taming = dominator.getSkills().getSkillOrLearn(SkillList.TAMEANIMAL);
                Skill ah = dominator.getSkills().getSkillOrLearn(SkillList.BREEDING);
                final double bonus = Math.max(0.0, taming.skillCheck(0.0, 0.0, false, 1.0f));
                final double power = ah.skillCheck(Config.nightSteedDifficulty, bonus, false, 1.0f);
                if(power > 0.0) return CreatureTemplateCreatorAwakening.NIGHT_STEED_CID;
            }
        }
        return CreatureTemplateIds.HELL_HORSE_CID;
    }

    public static String generateGenericName(Creature mother) {
        return null;
    }

    public static String generateFemaleName(Creature mother) {
        return null;
    }

    public static String generateMaleName(Creature mother) {
        return null;
    }
}
