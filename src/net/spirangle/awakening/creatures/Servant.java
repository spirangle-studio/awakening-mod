package net.spirangle.awakening.creatures;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.intra.MoneyTransfer;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.questions.InstructTaskQuestion;
import com.wurmonline.server.structures.Structure;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.VillageRole;
import com.wurmonline.server.villages.Villages;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.tasks.Task;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Servant {

    private static final Logger logger = Logger.getLogger(Servant.class.getName());

    public static int PAID_BY_KING = 0x0010;
    public static int BOUND_TO_DEED = 0x0020;

    public static final List<Servant> servants = new ArrayList<>();

    private static boolean isLoaded = false;
    private static Field npcServantField;

    public Creature npc;
    public Item contract;
    public Item lantern;
    public int status;
    public long face;
    public long salary;
    public List<Task> tasks;
    private long chatTime;
    private long chatDelay;
    private float posx;
    private float posy;
    private float posz;
    private float rotation;
    private long money;

    public static void init() {
        try {
            npcServantField = Npc.class.getDeclaredField("servant");
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Class Npc has not been properly modified.", e);
        }

        List<Servant> position = new ArrayList<>();

        try(Connection con = ModSupportDb.getModSupportDb()) {
            try(PreparedStatement ps = con.prepareStatement("SELECT CONTRACTID,NPCID,STATUS,FACE,POSX,POSY,POSZ,ROTATION,MONEY FROM SERVANTS WHERE NPCID>=0");
                PreparedStatement ps2 = con.prepareStatement("SELECT TASKID,TYPE,QUALITYLEVEL,DATA FROM TASKS WHERE NPCID=?");
                ResultSet rs = ps.executeQuery()) {
                long contractId, npcId/*,taskId*/, face, money;
                int status/*,taskType*/;
                float posx, posy, posz, rotation/*,taskQL*/;
                Item contract;
                Creature npc;
                Servant servant;
                while(rs.next()) {
                    contractId = rs.getLong(1);
                    npcId = rs.getLong(2);
                    status = rs.getInt(3);
                    face = rs.getLong(4);
                    posx = rs.getFloat(5);
                    posy = rs.getFloat(6);
                    posz = rs.getFloat(7);
                    rotation = rs.getFloat(8);
                    money = rs.getLong(9);
                    logger.info("Loading servant id " + npcId + " with contract " + contractId + ".");
                    try {
                        contract = Items.getItem(contractId);
                    } catch(NoSuchItemException e) {
                        logger.log(Level.SEVERE, "No contract exists with the id: " + contractId, e);
                        continue;
                    }
                    try {
                        npc = Creatures.getInstance().getCreature(npcId);
                    } catch(NoSuchCreatureException e) {
                        logger.log(Level.SEVERE, "No servant exists with the id: " + npcId, e);
                        continue;
                    }
                    if(npc != null && (npc instanceof Npc)) {
                        servant = new Servant(npc, contract, status, face, money);
                        try {
                            npcServantField.set(npc, servant);
                            logger.info(npc.getName() + " is indeed a servant.");
                        } catch(IllegalAccessException e) {
                            logger.log(Level.SEVERE, "Class Npc has not been properly modified.", e);
                        }
                        if(posx < 0.0f || posy < 0.0f) position.add(servant);
                        else servant.setPosition(posx, posy, posz, rotation);

                        servant.loadTasks(ps2);
                        servants.add(servant);
                    } else {
                        logger.log(Level.WARNING, "Creature with id " + npcId + " is not an NPC.");
                    }
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load servant data: " + e.getMessage(), e);
        }

        if(position.size() > 0)
            for(Servant r : position)
                r.savePosition();

        isLoaded = true;
    }

    public static boolean isLoaded() {
        return isLoaded;
    }

    public static Creature summonServant(Creature performer, Item contract, String name, int id,
                                         float posx, float posy, float rot, byte gender, int status) {

        int x = performer.getTileX();
        int y = performer.getTileY();
        VolaTile tile = Zones.getOrCreateTile(x, y, performer.isOnSurface());

        if(performer.getPower() >= MiscConstants.POWER_DEMIGOD) {
            status |= Servant.PAID_BY_KING;
            status &= ~Servant.BOUND_TO_DEED;
        } else {
            if(tile != null) {
                Structure struct = tile.getStructure();
                if(struct != null && !struct.mayPlaceMerchants(performer)) {
                    performer.getCommunicator().sendNormalServerMessage("You may not summon a servant inside this building. Permission to place merchant is required. Ask owner to configure permissions.");
                    return null;
                }
            }

            Village deed = Villages.getVillage(x, y, performer.isOnSurface());
            if(deed != null) {
                VillageRole role = deed.getRoleFor(performer);
                if(role != null && !role.mayPlaceMerchants()) {
                    performer.getCommunicator().sendNormalServerMessage("You may not summon a servant on this deed. Permission to place merchant is required. Ask mayor to configure permissions.");
                    return null;
                }
            } else {
                performer.getCommunicator().sendNormalServerMessage("You may only summon a servant on a deed where you have permissions to place merchants.");
                return null;
            }

            status &= ~Servant.PAID_BY_KING;
            status |= Servant.BOUND_TO_DEED;
        }

        Creature npc = null;
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(id);
            int layer = performer.getLayer();
            if(gender != 0 && gender != 1) gender = (byte) (Server.rand.nextBoolean()? 1 : 0);
            byte kingdom = performer.getKingdomId();
            int age = (int) (Server.rand.nextFloat() * Math.min(48, template.getMaxAge()));
            int floorLevel = performer.getFloorLevel();
            npc = Creature.doNew(id, true, posx, posy, rot, layer, name, gender, kingdom, (byte) 0, false, (byte) age, floorLevel);
            //			Shop shop = Economy.getEconomy().createShop(npc.getWurmId(),contract.getOwnerId());
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Failed to spawn servant.", e);
            return null;
        }
        long face = Server.rand.nextLong();
        long money = 0L;

        Servant servant = new Servant(npc, contract, status, face, money);
        try {
            npcServantField.set(npc, servant);
        } catch(IllegalAccessException e) {
            logger.log(Level.SEVERE, "Failed to create servant.", e);
            npc.destroy();
            return null;
        }

        if(!servant.updateDb()) {
            npc.destroy();
            return null;
        }

        if(tile != null) tile.setNewFace(npc);

        contract.setData(npc.getWurmId());
        servants.add(servant);
        return npc;
    }

    public static void dismissServant(Creature performer, Creature npc, boolean dropItems) {
        Servant servant = Servant.getServant(npc);
        if(servant == null) return;

        if(dropItems) {
            servant.dropItems(true);
            servant.collectMoney();
        }

        try(Connection con = ModSupportDb.getModSupportDb()) {
            try(PreparedStatement ps = con.prepareStatement("DELETE FROM TASKS WHERE NPCID=?")) {
                ps.setLong(1, npc.getWurmId());
                ps.execute();
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "Failed to remove tasks from servant id " + npc.getWurmId() + ".", e);
                return;
            }

            try(PreparedStatement ps = con.prepareStatement("UPDATE SERVANTS SET NPCID=-1,STATUS=0,FACE=0,POSX=-1.0,POSY=-1.0,POSZ=0.0,ROTATION=0.0,MONEY=0 WHERE CONTRACTID=?")) {
                ps.setLong(1, servant.contract.getWurmId());
                ps.execute();
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to dismiss servant from db.", e);
            return;
        }

        servant.contract.setData(-1L);
        npc.destroy();
        servants.remove(servant);
    }

    @SuppressWarnings("unused")
    public static void wearItemsFix(Creature creature, Item bodyPart, Item element) {
        logger.info(element.getName() + " can't be worn on " + bodyPart.getName() + ".");
        Set<Item> items = creature.getBody().getContainersAndWornItems(bodyPart);
        try {
            for(Item i : items)
                if(i.getParent() == bodyPart) {
                    logger.info("Dropping " + i.getName() + " worn on the " + i.getParent().getName() + " to the ground.");
                    creature.dropItem(i);
                }
            if(!bodyPart.insertItem(element)) {
                logger.info("Could not wear " + element.getName() + " dropping it to the ground.");
                //				inventory.insertItem(element);
                creature.dropItem(element);
            }
        } catch(Exception e) { }
    }

    @SuppressWarnings("unused")
    public static boolean giveToServant(Creature performer, Item source, Creature target, short action) {
        if(!(target instanceof Npc)) {
            logger.log(Level.WARNING, performer.getName() + " tries giving something to " + target.getName() + ", who isn't a servant.");
            return true;
        }
        logger.info(performer.getName() + " gives " + target.getName() + " the item " + source.getName() + ".");

        Servant servant = getServant(target);
        if(servant != null && servant.contract != null) {
            if(servant.handleGiveTo(performer, source))
                return false;

            if(source.getTemplateId() == ItemTemplateCreatorAwakening.servantTask) {
                InstructTaskQuestion question = new InstructTaskQuestion(performer, source, servant);
                question.sendQuestion();
                return false;
            }
        }
        return true;
    }

    public static Servant getServant(long npcId) {
        if(npcId > 0) {
            try {
                Creature npc = Creatures.getInstance().getCreature(npcId);
                return Servant.getServant(npc);
            } catch(NoSuchCreatureException e) {
                logger.log(Level.SEVERE, "No servant exists with the id: " + npcId, e);
            }
        }
        return null;
    }

    public static Servant getServant(Creature npc) {
        Servant servant = null;
        if(npc != null && (npc instanceof Npc)) {
            try {
                servant = (Servant) npcServantField.get(npc);
            } catch(IllegalAccessException e) {
                logger.log(Level.SEVERE, "Failed to get servant.", e);
            }
        }
        return servant;
    }

    public static String getName(byte gender) {
        return "servant";
    }

    public Servant(Creature npc, Item contract, int status, long face, long money) {
        this.npc = npc;
        this.contract = contract;
        this.lantern = null;
        this.status = status;
        this.face = face;
        this.salary = -1L;
        this.tasks = new ArrayList<>();
        this.chatTime = System.currentTimeMillis();
        this.chatDelay = 60000L;
        this.posx = npc.getPosX();
        this.posy = npc.getPosY();
        this.posz = npc.getPositionZ();
        this.rotation = npc.getStatus().getRotation();
        this.money = money;

        for(Item i : npc.getInventory().getItems())
            if(i.getTemplateId() == ItemList.lantern) lantern = i;
    }

    public boolean setPosition(float posx, float posy, float posz, float rotation) {
        if(posx < 0.0f || posy < 0.0f) {
            savePosition();
            return false;
        }
        if(posx == this.posx && posy == this.posy && posz == this.posz && rotation == this.rotation) return false;
        npc.setPositionX(posx);
        npc.setPositionY(posy);
        npc.setPositionZ(posz);
        npc.setRotation(rotation);
        final int diffx = (int) (posx * 10.0f) - (int) (this.posx * 10.0f);
        final int diffy = (int) (posy * 10.0f) - (int) (this.posy * 10.0f);
        final int diffz = (int) (posz * 10.0f) - (int) (this.posz * 10.0f);
        final int tilex = ((int) (posx) >> 2) - ((int) (this.posx) >> 2);
        final int tiley = ((int) (posy) >> 2) - ((int) (this.posy) >> 2);
        npc.moved(diffx, diffy, diffz, tilex, tiley);
        this.posx = posx;
        this.posy = posy;
        this.posz = posz;
        this.rotation = rotation;
        return true;
    }

    public boolean setPosition() {
        float posx = npc.getPosX();
        float posy = npc.getPosY();
        float posz = npc.getPositionZ();
        float rotation = npc.getStatus().getRotation();
        if(posx == this.posx && posy == this.posy && posz == this.posz && rotation == this.rotation) return false;
        npc.setPositionX(this.posx);
        npc.setPositionY(this.posy);
        npc.setPositionZ(this.posz);
        npc.setRotation(this.rotation);
        final int diffx = (int) (this.posx * 10.0f) - (int) (posx * 10.0f);
        final int diffy = (int) (this.posy * 10.0f) - (int) (posy * 10.0f);
        final int diffz = (int) (this.posz * 10.0f) - (int) (posz * 10.0f);
        final int tilex = ((int) (this.posx) >> 2) - ((int) (posx) >> 2);
        final int tiley = ((int) (this.posy) >> 2) - ((int) (posy) >> 2);
        npc.moved(diffx, diffy, diffz, tilex, tiley);
        return true;
    }

    public void savePosition() {
        this.posx = npc.getPosX();
        this.posy = npc.getPosY();
        this.posz = npc.getPositionZ();
        this.rotation = npc.getStatus().getRotation();
        updateDb();
    }

    public boolean updateDb() {
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("UPDATE SERVANTS SET NPCID=?,STATUS=?,FACE=?,POSX=?,POSY=?,POSZ=?,ROTATION=?,MONEY=? WHERE CONTRACTID=?")) {
            ps.setLong(1, npc.getWurmId());
            ps.setInt(2, status);
            ps.setLong(3, face);
            ps.setFloat(4, posx);
            ps.setFloat(5, posy);
            ps.setFloat(6, posz);
            ps.setFloat(7, rotation);
            ps.setLong(8, money);
            ps.setLong(9, contract.getWurmId());
            ps.execute();
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to update the servant with the database.", e);
            return false;
        }
        return true;
    }

    public boolean isEmployer(Creature player) {
        return contract.getOwnerId() == player.getWurmId();
    }

    public Player getEmployer() {
        Player employer = null;
        try {
            final Player p = Players.getInstance().getPlayer(contract.getOwnerId());
            employer = p;
        } catch(NoSuchPlayerException e) {
            logger.log(Level.SEVERE, "Could not find employer.", e);
        }
        return employer;
    }

    public boolean acceptItem(Creature performer, Item item) {
        if(tasks.size() > 0)
            for(Task t : tasks)
                if(t.acceptItem(performer, item)) return true;

        if(!isEmployer(performer)) return false;

        int templateId = item.getTemplateId();
        if(templateId == ItemTemplateCreatorAwakening.servantTask) {
            float ql = item.getQualityLevel();
            float weight = 0.0f;
            if(tasks.size() > 0) {
                for(Task t : tasks) {
                    ql += t.ql;
                    weight += t.getWeight();
                }
                ql = ql / (tasks.size() + 1);
            }
            float max = 5.0f;
            if(ql >= 95.0) max = 10.0f;
            else if(ql >= 90.0) max = 9.0f;
            else if(ql >= 70.0) max = 8.0f;
            else if(ql >= 50.0) max = 7.0f;
            else if(ql >= 30.0) max = 6.0f;
            if(weight > max) {
                performer.getCommunicator().sendNormalServerMessage(npc.getName() + " already has too many tasks.");
                return false;
            }
            return true;
        }

        if(templateId == ItemList.lantern) {
            if(lantern == null) {
                lantern = item;
                return true;
            }
            performer.getCommunicator().sendNormalServerMessage(npc.getName() + " already carries a lantern.");
            return false;
        }

        int weight = item.getFullWeight();
        if((!npc.canCarry(weight) && weight > 0) || npc.getInventory().getItems().size() >= 100) {
            performer.getCommunicator().sendNormalServerMessage(npc.getName() + " already carry too many items.");
            return false;
        }
        if(!item.isArmour()) {
            performer.getCommunicator().sendNormalServerMessage(npc.getName() + " kindly but firmly refuses to accept your gift.");
            return false;
        }
        return true;
    }

    public void updateInventory() {
        boolean hasLantern = false;
        Set<Item> inv = npc.getInventory().getItems();
        for(Item i : inv) {
            if(i.getTemplateId() == ItemList.lantern && !hasLantern) hasLantern = true;
            else if(i.isArmour()) {
                logger.info("Dropping " + i.getName() + " from inventory.");
                npc.dropItem(i);
            }
        }
    }

    public int dropItems(boolean undress) {
        int n = 0;
        Set<Item> inv = npc.getInventory().getItems();
        logger.info("Inventory contains " + inv.size() + " items.");
        for(Item i : inv) {
            if(i.getTemplateId() == ItemList.lantern) {
                i.setAuxData((byte) 0);
                i.setTemperature((short) 200);
            }
            npc.dropItem(i);
            ++n;
        }
        if(undress) {
            n += undress();
        }
        return n;
    }

    public int undress() {
        int n = 0;
        Item[] items = npc.getBody().getContainersAndWornItems();
        logger.info("Body contains " + items.length + " items.");
        for(Item i : items) {
            npc.dropItem(i);
            ++n;
        }
        return n;
    }

    public long getMoney() {
        return this.money;
    }

    public boolean takeMoney(long money) {
        if(money > 0L) {
            this.money += money;
            return updateDb();
        }
        return true;
    }

    public void collectMoney() {
        if(money > 0L) {
            Player employer = getEmployer();
            if(employer != null) {
                try {
                    employer.addMoney(money);
                    employer.getCommunicator().sendNormalServerMessage("You collect an amount of " + new Change(money).getChangeString() +
                                                                       " from " + npc.getName() + ".");
                    money = 0L;
                    updateDb();
                } catch(IOException e) { }
            }
        }
    }

    public void poll() {
        if(lantern != null) {
            if(!lantern.isOnFire()) {
                lantern.setAuxData((byte) 126);
                lantern.setTemperature((short) 10000);
            }
        }
        if(!tasks.isEmpty())
            for(Task t : tasks) t.poll();
    }

    @SuppressWarnings("unused")
    public boolean pollChat() {
        if(tasks.isEmpty()) return true;
        if(System.currentTimeMillis() - chatTime > chatDelay && !npc.isDead()) {
            if(tasks.size() == 1) {
                Task t = tasks.get(0);
                t.pollChat();
            } else {
                for(Task t : tasks) {
                    if(t.pollChat()) {
                        tasks.remove(t);
                        tasks.add(t);
                        break;
                    }
                }
            }
            chatTime = System.currentTimeMillis();
            chatDelay = 150000L + (long) Server.rand.nextInt(300000);
        }
        return true;
    }

    public void pollDaily() {
        setPosition();

        if((status & Servant.PAID_BY_KING) == 0) {
            long salary = getSalary();
            if(salary > 0L) {
                try {
                    Player employer = Players.getInstance().getPlayer(contract.getOwnerId());
                    if(employer == null || employer.getMoney() < salary) {
                        Server.getInstance().broadCastAction(npc.getNameWithGenus() + " did not get " + npc.getHisHerItsString() +
                                                             " salary today, and decides to find a better employer.", npc, 5);
                        logger.info("Servant quits job because of unpaid salary.");
                        Servant.dismissServant(null, npc, false);
                        return;
                    }
                    try {
                        employer.chargeMoney(salary);
                        employer.save();
                        logger.info("Servant[" + npc.getName() + "]: Pay salary of " + salary + ".");
                    } catch(IOException e) { }
                } catch(NoSuchPlayerException nsp) {
                    PlayerInfo employerInfo = PlayerInfoFactory.getPlayerInfoWithWurmId(contract.getOwnerId());
                    if(employerInfo != null) {
                        try {
                            employerInfo.load();
                            if(employerInfo.wurmId <= 0L || employerInfo.money < salary) {
                                logger.info("Servant quits job because of unpaid salary.");
                                Servant.dismissServant(null, npc, false);
                                return;
                            }
                            employerInfo.setMoney(employerInfo.money - salary);
                            employerInfo.save();
                            logger.info("Servant[" + npc.getName() + "]: Pay salary of " + salary + ".");
                        } catch(IOException e) { }
                    }
                    if(Servers.localServer.id != employerInfo.currentServer) {
                        new MoneyTransfer(employerInfo.getName(), employerInfo.wurmId, employerInfo.money, -salary, "Servant salary", (byte) 5, "", false);
                    } else {
                        new MoneyTransfer(employerInfo.getName(), employerInfo.wurmId, employerInfo.money, -salary, "Servant salary", (byte) 5, "");
                    }
                }
            }
        }

        if(tasks.size() > 0)
            for(Task t : tasks) t.pollDaily();
    }

    @SuppressWarnings("unused")
    public void talkLocal(String message) {
        VolaTile tile = npc.getCurrentTile();
        if(tile != null) {
            Message m = new Message(npc, (byte) 0, ":Local", "<" + npc.getName() + "> " + message);
            tile.broadCastMessage(m);
        }
    }

    public boolean handleGiveTo(Creature performer, Item target) {
        if(tasks.size() > 0)
            for(Task t : tasks)
                if(t.handleGiveTo(performer, target)) return true;
        return false;
    }

    public void reloadTasks() {
        tasks.clear();
        loadTasks();
    }

    public void loadTasks() {
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("SELECT TASKID,TYPE,QUALITYLEVEL,DATA FROM TASKS WHERE NPCID=?")) {
            loadTasks(ps);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load servant tasks: " + e.getMessage(), e);
        }
    }

    public void loadTasks(PreparedStatement ps) {
        try {
            ps.setLong(1, npc.getWurmId());
            try(ResultSet rs = ps.executeQuery()) {
                long taskId;
                int taskType;
                float taskQL;
                Task task;
                String data;
                while(rs.next()) {
                    taskId = rs.getLong(1);
                    taskType = rs.getInt(2);
                    taskQL = rs.getFloat(3);
                    data = rs.getString(4);
                    logger.info("Loading task: id=" + taskId + ", type=" + taskType + ", QL=" + taskQL + ", data=" + data);
                    task = Task.createTask(taskId, taskType, taskQL, this, data);
                    if(task != null) addTask(task, false);
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed read the servant tasks: " + e.getMessage(), e);
        }
    }

    public boolean addTask(Task task, boolean update) {
        salary = -1L;
        if(task.isGroupedTask()) {
            if(tasks.size() > 0)
                for(Task t : tasks)
                    if(t.type == task.type) {
                        t.addTask(task);
                        return !update || t.updateDb();
                    }
        }
        if(update && !task.updateDb()) return false;
        tasks.add(task);
        return true;
    }

    public Task getTask(long taskId) {
        if(tasks.size() > 0)
            for(Task t : tasks)
                if(t.id == taskId) return t;
        return null;
    }

    public Task getTaskByType(int type) {
        if(tasks.size() > 0)
            for(Task t : tasks)
                if(t.type == type) return t;
        return null;
    }

    public boolean dropTask(Task task) {
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("DELETE FROM TASKS WHERE TASKID=?")) {
            ps.setLong(1, task.id);
            ps.execute();
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to delete task from db.", e);
            return false;
        }
        salary = -1L;
        tasks.remove(task);
        task.delete();
        return true;
    }

    public long getSalary() {
        if(salary < 0L) {
            salary = 0L;
            for(Task t : tasks)
                salary += t.getSalary();
        }
        return salary;
    }
}
