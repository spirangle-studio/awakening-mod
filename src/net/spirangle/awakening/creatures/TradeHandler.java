package net.spirangle.awakening.creatures;

import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.Trade;
import com.wurmonline.server.items.TradingWindow;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class TradeHandler {

    private static final Logger logger = Logger.getLogger(TradeHandler.class.getName());

    private static final int maxPersonalItems = com.wurmonline.server.creatures.TradeHandler.getMaxNumPersonalItems();

    public static boolean addItemsToTrade(Creature trader, Trade trade, Shop shop, Map<Integer, Set<Item>> itemMap, boolean ownerTrade) {
        if(trade == null || shop == null || !shop.isPersonal()) return false;
        Set<Item> ite = trader.getInventory().getItems();
        Item[] itarr = ite.toArray(new Item[ite.size()]);
        TradingWindow myOffers = trade.getTradingWindow(1L);
        int templateId = -10;
        myOffers.startReceivingItems();
        for(int x = 0; x < itarr.length; ++x) {
            templateId = itarr[x].getTemplateId();
            Set<Item> its = itemMap.get(templateId);
            if(its == null) its = new HashSet<Item>();
            its.add(itarr[x]);
            if(ownerTrade) myOffers.addItem(itarr[x]);
            else if(!itarr[x].isCoin()) myOffers.addItem(itarr[x]);
            itemMap.put(templateId, its);
        }
        myOffers.stopReceivingItems();

        logger.info("Add items to trade.");
        return true;
    }

    public static boolean suckInterestingItems(Creature trader, Trade trade, Shop shop, boolean ownerTrade) {
        if(trade == null || shop == null || !shop.isPersonal()) return false;
        TradingWindow currWin = trade.getTradingWindow(2L);
        TradingWindow targetWin = trade.getTradingWindow(4L);
        Item[] offItems = currWin.getItems();
        Item[] setItems = targetWin.getItems();
        if(ownerTrade) {
            TradingWindow myOffers = trade.getTradingWindow(1L);
            Item[] currItems = myOffers.getItems();
            int size = 0;
            for(int c = 0; c < currItems.length; ++c) {
                if(!currItems[c].isCoin()) {
                    ++size;
                }
            }
            size += setItems.length;
            if(size > TradeHandler.maxPersonalItems) {
                trade.creatureOne.getCommunicator().sendNormalServerMessage(trader.getName() + " says, 'I cannot add more items to my stock right now.'");
            } else {
                TradingWindow hisReq = trade.getTradingWindow(3L);
                Item[] reqItems = hisReq.getItems();
                for(int c2 = 0; c2 < reqItems.length; ++c2) {
                    if(!reqItems[c2].isCoin()) {
                        ++size;
                    }
                }
                if(size > TradeHandler.maxPersonalItems) {
                    trade.creatureOne.getCommunicator().sendNormalServerMessage(trader.getName() + " says, 'I cannot add more items to my stock right now.'");
                } else {
                    targetWin.startReceivingItems();
                    for(int x3 = 0; x3 < offItems.length; ++x3) {
                        if(offItems[x3].getTemplateId() != 272 && offItems[x3].getTemplateId() != 781 && !offItems[x3].isArtifact() && !offItems[x3].isRoyal() && ((!offItems[x3].isVillageDeed() && !offItems[x3].isHomesteadDeed()) || !offItems[x3].hasData())) {
                            if(size > TradeHandler.maxPersonalItems) {
                                if(offItems[x3].isCoin()) {
                                    currWin.removeItem(offItems[x3]);
                                    targetWin.addItem(offItems[x3]);
                                }
                            } else if((offItems[x3].isLockable() && offItems[x3].isLocked()) || (offItems[x3].isHollow() && !offItems[x3].isEmpty(true))) {
                                if(offItems[x3].isLockable() && offItems[x3].isLocked()) {
                                    trade.creatureOne.getCommunicator().sendSafeServerMessage(trader.getName() + " says, 'I don't accept locked items any more. Sorry for the inconvenience.'");
                                } else {
                                    trade.creatureOne.getCommunicator().sendSafeServerMessage(trader.getName() + " says, 'Please empty the " + offItems[x3].getName() + " first.'");
                                }
                            } else {
                                currWin.removeItem(offItems[x3]);
                                targetWin.addItem(offItems[x3]);
                                ++size;
                            }
                        }
                    }
                    targetWin.stopReceivingItems();
                }
            }
        } else {
            targetWin.startReceivingItems();
            for(int x = 0; x < offItems.length; ++x) {
                if(offItems[x].isCoin()) {
                    Item parent2 = offItems[x];
                    try {
                        parent2 = offItems[x].getParent();
                    } catch(NoSuchItemException e) { }
                    if(offItems[x] == parent2 || parent2.isViewableBy(trader)) {
                        currWin.removeItem(offItems[x]);
                        targetWin.addItem(offItems[x]);
                    }
                }
            }
            targetWin.stopReceivingItems();
        }
        logger.info("Suck interesting items in trade.");
        return true;
    }
}
