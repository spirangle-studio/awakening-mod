package net.spirangle.awakening.creatures;

import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import net.spirangle.awakening.Config;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.*;


public class VillageGuard implements ModCreature {

    private static final Logger logger = Logger.getLogger(VillageGuard.class.getName());

    private static final int[] templateIds = {
            VILLAGE_GUARD_THE_CID,
            VILLAGE_GUARD_TIR_CID,
            VILLAGE_GUARD_CEL_CID,
            VILLAGE_GUARD_SHO_CID,
            VILLAGE_GUARD_THE_CID
    };

    private static final String[] templateCorpseNames = {
            "islestowerguard.",
            "jenn-kellontowerguard.",
            "mol-rehantowerguard.",
            "hordeofthesummonedtowerguard.",
            "islestowerguard."
    };

    public static int getTemplateId(byte kingdom) {
        logger.info("VillageGuard(" + kingdom + ", " + templateIds[kingdom] + ")");
        return templateIds[kingdom];
    }

    private final byte kingdom;
    private final int templateId;

    public VillageGuard(byte kingdom) {
        this.kingdom = kingdom;
        this.templateId = templateIds[this.kingdom];
    }

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_SWIMMING,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_HUMAN,
                CreatureTypes.C_TYPE_OPENDOORS
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(this.templateId);

        builder.name(Config.kingdomNames[this.kingdom] + " village guard");
        builder.description("A skilled mercenary, hired to protect this village. Most likely a former veteran tower guard with many years of experience.");
        builder.modelName("model.creature.humanoid.human.guard.tower");

        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_HUMAN);
        builder.vision((short) 45);
        builder.sex((byte) 0);
        builder.dimension((short) 180, (short) 20, (short) 35);
        builder.deathSounds(SoundNames.DEATH_MALE_SND, SoundNames.DEATH_FEMALE_SND);
        builder.hitSounds(SoundNames.HIT_MALE_SND, SoundNames.HIT_FEMALE_SND);
        builder.naturalArmour(0.3f);
        builder.damages(5.0f, 7.0f, 5.0f, 0.0f, 0.0f);
        builder.speed(1.5f);
        builder.moveRate(100);
        builder.itemsButchered(new int[0]);
        builder.maxHuntDist(100);
        builder.aggressive(100);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_HUMAN);
        builder.alignment(this.kingdom == 3? -70.0f : 70.0f);
        builder.combatDamageType(Wound.TYPE_SLASH);
        builder.maxGroupAttackSize(6);
        builder.baseCombatRating(20.0f);
        builder.hasHands(true);

        builder.skill(SkillList.BODY_STRENGTH, 30.0f);
        builder.skill(SkillList.BODY_CONTROL, 30.0f);
        builder.skill(SkillList.BODY_STAMINA, 35.0f);
        builder.skill(SkillList.MIND_LOGICAL, 17.0f);
        builder.skill(SkillList.MIND_SPEED, 27.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 24.0f);
        builder.skill(SkillList.SOUL_DEPTH, 24.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 80.0f);
        return builder;
    }

    public static void init(byte kingdom) {
        int templateId = templateIds[kingdom];
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(templateId);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), templateCorpseNames[kingdom]);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "spiritGuard"), true);
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "Village Guard: " + e.getMessage(), e);
        }
    }
}
