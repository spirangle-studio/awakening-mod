package net.spirangle.awakening.creatures;

import com.wurmonline.server.LoginHandler;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Players;
import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.villages.Citizen;
import com.wurmonline.server.villages.NoSuchVillageException;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.wurmonline.server.creatures.CreatureTemplateIds.*;
import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.*;


public class CreatureAI {

    private static final Logger logger = Logger.getLogger(CreatureAI.class.getName());

    private static final Pattern vinvitePattern = Pattern.compile("^\\/(?:vinvite|villageinvite)\\s+([a-zA-Z0-9]+)(?:\\s+(\\d+))?$");

    private static final Map<Long, Map<Integer, Long>> villageInvitations = new HashMap<>();

    @SuppressWarnings("unused")
    public static byte getAttitude(Creature creature, Creature target) {
        if(creature.opponent != target && target.isPlayer() && !creature.isDominated()) {
            if(!isKingdomEnemy(target.getKingdomId(), creature)) return 0;
            if(creature.isKingdomGuard() || creature.isSpiritGuard()) {
                if(target.isPlayer() && hasVillageInvitation(creature, (Player) target)) return 0;
            }
        }
        return 1;
    }

    @SuppressWarnings("unused")
    public static boolean isVillageEnemy(Village village, Creature creature) {
        return isKingdomEnemy(village.kingdom, creature) && !hasVillageInvitation(village, creature);
    }

    public static boolean isKingdomEnemy(byte kingdomId, Creature creature) {
        if(kingdomId == 3) {
            int t = creature.getTemplateId();
            return t != SHADE_CID && t != SHADE_HOUND_CID;
        } else if(kingdomId == 4) {
            int t = creature.getTemplateId();
            return !creature.isDragon() || t == DRAGON_RED_CID || t == DRAGON_BLACK_CID || t == DRAKE_RED_CID || t == DRAKE_BLACK_CID;
        }
        return true;
    }

    @SuppressWarnings("unused")
    public static boolean handleVillageInviteMessage(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        try {
            Matcher m = vinvitePattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(LoginHandler.raiseFirstLetter(name));
                    if(player == null)
                        communicator.sendAlertServerMessage("There is no player online by the name of " + name + ".");
                    else if(performer.getKingdomId() != player.getKingdomId()) {
                        Village village = performer.getCitizenVillage();
                        if(village != null) {
                            Citizen citizen = village.getCitizen(performer.getWurmId());
                            if(citizen != null && citizen.getRole().mayInviteCitizens()) {
                                long time = 60L;
                                if(m.group(2) != null) time = Math.min(720L, Long.parseLong(m.group(2)));
                                addVillageInvitation(player, village, time);
                                communicator.sendSafeServerMessage("You invite " + player.getName() + " as a friend of your village for " + time + " minutes.");
                                player.getCommunicator().sendSafeServerMessage("You have been invited by " + performer.getName() + " as a friend of the village " + village.getName() + ", for " + time + " minutes.");
                                return false;
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Village invite command error: " + e.getMessage(), e);
        }
        return true;
    }

    public static void addVillageInvitation(Player player, Village village, long time) {
        long expiration = System.currentTimeMillis() + time * 60000L;
        Map vinvs = villageInvitations.get(player.getWurmId());
        if(vinvs == null) {
            vinvs = new HashMap<>();
            villageInvitations.put(player.getWurmId(), vinvs);
        }
        vinvs.put(village.getId(), expiration);
    }

    private static boolean hasVillageInvitation(Creature guard, Player player) {
        byte gkid = guard.getKingdomId();
        byte pkid = player.getKingdomId();
        if(gkid != pkid) {
            Map<Integer, Long> vinvs = villageInvitations.get(player.getWurmId());
            if(vinvs != null) {
                long time = System.currentTimeMillis();
                Iterator<Map.Entry<Integer, Long>> it = vinvs.entrySet().iterator();
                while(it.hasNext()) {
                    Map.Entry<Integer, Long> entry = it.next();
                    int villageId = entry.getKey();
                    long expiration = entry.getValue();
                    if(expiration < time) {
                        try {
                            Village village = Villages.getVillage(villageId);
                            player.getCommunicator().sendAlertServerMessage("Your invitation to " + village.getName() + " has expired.");
                        } catch(NoSuchVillageException e) { }
                        it.remove();
                    }
                    if(guard.isSpiritGuard()) {
                        Village village = guard.getCitizenVillage();
                        if(village.getId() == villageId) return true;
                    } else {
                        try {
                            Village village = Villages.getVillage(villageId);
                            if(gkid == village.kingdom) return true;
                        } catch(NoSuchVillageException e) { }
                    }
                }
                if(vinvs.isEmpty()) villageInvitations.remove(player.getWurmId());
            }
        }
        return false;
    }

    public static boolean hasVillageInvitation(Village village, Creature creature) {
        if(!creature.isPlayer()) return false;
        Player player = (Player) creature;
        Map<Integer, Long> vinvs = villageInvitations.get(player.getWurmId());
        if(vinvs != null) {
            long time = System.currentTimeMillis();
            Long expiration = vinvs.get(village.getId());
            if(expiration != null) {
                if(expiration >= time) return true;

                player.getCommunicator().sendAlertServerMessage("Your invitation to " + village.getName() + " has expired.");
                vinvs.remove(village.getId());
                if(vinvs.isEmpty()) villageInvitations.remove(player.getWurmId());
            }
        }
        return false;
    }

    @SuppressWarnings("unused")
    public static CreatureAI getCreatureAI(final Creature c) {
        if(c.isDragon()) {
            return new CreatureAI(c);
        }
        return null;
    }

    @SuppressWarnings("unused")
    public static boolean pollNPCChat(final Creature c, final CreatureAI ai) {
        if(c.isDragon()) {
            if(ai == null) return false;
            ai.pollNPCChat();
        }
        return true;
    }

    @SuppressWarnings("unused")
    public static boolean canAutoDismissMerchant(final Creature c, final Shop shop) {
        final PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithWurmId(shop.getOwnerId());
        if(pi != null) {
            try {
                if(!pi.loaded) pi.load();
                if(pi.getPower() >= MiscConstants.POWER_HERO) return false;
            } catch(IOException e) { }
        }
        return true;
    }

    private static final String[] dragonSpeaks = {
            "Little human, I can smell you!",
            "I am astounded, that you have had the courage to walk into my lair.",
            "Where are you? I can hear your foot steps.",
            "Answer my riddle correctly and I will let you pass unharmed, come to think of it, I'd rather taste your flesh.",
            "Do you hope to steal from my treasure? I won't permit it, I have greater use for it than you do.",
            };

    private static final String[] dragonSpeaksTarget = {
            "I will sow the seeds of destruction once more, to teach others a lesson about how I am an impressive force to be reckoned with.",
            "Have you not heard of how unlucky it is to invoke a dragon's wrath?",
            "My armor is like tenfold shields, my teeth are swords, my claws spears, the shock of my tail a thunderbolt, my wings a hurricane, and my breath death!",
            "You are a fool.",
            "I will eat you little human!",
            };

    private static final String[] dragonQueenSpeaks = {
            "The curse some of you humans call 'Maglivyn' has also tainted the race of dragons.",
            "Once we were proud creatures, far above mere bickering and selfish interests, we only killed humans who trespassed our domain.",
            "We never spoke to humans, there was no reason for it, we had nothing to say to you.",
            "I am the last of my kind who still retains a mind unaffected by the curse. I am too old to be affected, years far beyond your counting.",
            "Now dragons have degenerated to lesser creatures, lesser of both mind and spirit, and strength.",
            "Dragons are creatures of magic, we are part of magic, and the world of magic has been tainted for a long time.",
            "Some of my children are breeding offspring in a worrying rate, these dragonets are spreading like wild fire.",
            };

    protected Creature creature;
    protected int chatPoller;
    protected int chatCounter;
    protected int chatDistance;
    protected String[] chatMessages;
    protected Creature chatTarget;

    public CreatureAI(Creature creature) {
        this.creature = creature;
        this.chatPoller = 0;
        this.chatCounter = 0;
        this.chatDistance = 0;
        this.chatMessages = null;
        this.chatTarget = null;
    }

    private void pollNPCChat() {
        if(chatPoller <= 0) {
            CreatureTemplate template = creature.getTemplate();
            if(creature.isDragon()) {
                String[] s = null;
                if(creature.getStatus().damage > 0) return;
                Creature t = creature.getTarget();
                if(t != null && t.isPlayer()) {
                    s = dragonSpeaksTarget;
                    chatPoller = 15 + Server.rand.nextInt(40);
                    chatDistance = 10;
                } else {
                    if(template.id == DRAGON_QUEEN_CID) {
                        s = dragonQueenSpeaks;
                        chatPoller = 30 + Server.rand.nextInt(30);
                        chatDistance = 5;
                    } else {
                        s = dragonSpeaks;
                        chatPoller = 60 + Server.rand.nextInt(120);
                        chatDistance = 20;
                    }
                }
                if(s == null) return;
                if(chatMessages != s || chatCounter >= s.length) chatCounter = 0;
                Server.getInstance().broadCastMessage(creature.getName() + " says: " + s[chatCounter], creature.getTileX(), creature.getTileY(), creature.isOnSurface(), chatDistance);
                ++chatCounter;
                chatMessages = s;
                chatTarget = t;
            }
        } else {
            --chatPoller;
        }
    }
}
