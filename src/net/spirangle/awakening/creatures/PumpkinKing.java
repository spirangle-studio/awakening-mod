package net.spirangle.awakening.creatures;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Server;
import com.wurmonline.server.WurmCalendar;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import net.spirangle.awakening.AwakeningMod;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.PUMPKIN_KING_CID;


public class PumpkinKing implements ModCreature {

    private static final Logger logger = Logger.getLogger(PumpkinKing.class.getName());

    public static final int HALLOWEEN_GIFT_ID = 0x1001;
    public static final int HALLOWEEN_GIFT_MAX = 5;

    public static final String[] pumpkinKingSpeaks = {
            /* 0*/"I don't believe it's %1$s! The most evil of heros! I'm unworthy to stand in your presence. The least I can do is give you this small gift.",
            /* 1*/"Oh, it's the wicked %1$s! Everyone's been talking about your great evil deeds! I'm proud to finally meet you! Let me give you this gift!",
            /* 2*/"Seems that you have been exceptionally naughty this year, %1$s. I award you this small gift! Hehehee *snarl* *cough*",
            /* 3*/"Did you frighten small children, did you spread fear and terror in the world! Indeed, you did all of it and more, %1$s! It makes me proud to award you this token of wicked appreciation.",
            /* 4*/"You have been a very naughty person this year, %1$s, so I will give you this gift!",
            /* 5*/"If there's something I hate more than, well, most of things, it's wasting gifts on unworthy people. But you, my dear %1$s, have done well this year, you've been quite naughty indeed.",
            /* 6*/"Some people are born for Halloween, and some are just counting the days until Christmas. I'm sure you belong to the former category, my dear %1$s. Take this, and you're welcome.",
            /* 7*/"My ledger says you've done a few naughty things, you've done alright this year, %1$s. I'm sure you can do better next year. Here, have this.",
            /* 8*/"The dead rise again, bats fly, terror strikes and screams echo, for tonight it’s Halloween. I'm here to treat you with a trick. Hehehe *cough*",
            /* 9*/"If human beings had genuine courage, they'd wear their costumes every day of the year, not just on Halloween. I'm sure you have courage %1$s, so take this gift.",
            /*10*/"I have a gift for you %1$s. You want it? Well then, come on over here and get it!",
            /*11*/"May you have a hair-raising experience, chills and thrills on Halloween! Don't let the night bite you. Mwahaha! Here, take this and have some fun.",
            /*12*/"That’s right. I am the Pumpkin King! And no, I'm no bloody farmer. Take this gift %1$s, and bugger off, I have wicked things to do.",
            /*13*/"Merry Halloween! And what is your name? That's all right %1$s. I have a present for you, anyway. There ya go! Heeheehee!",
            /*14*/"My name is Jack Goblington, and I am the Pumpking King. I'm here to spread terror, and gifts. For you %1$s, I have a gift, but next year I'll gift you with terror instead.",
            /*15*/"Did you know that in some languages \"gift\" actually means \"poison\"? Now, here %1$s, take this gift and have a gobbly good Halloween! Hehehe *snarl*",
            /*16*/"Did you do something really wicked this year %1$s? No you didn't! There's no fooling me... I'm the master of trick or treat. Take this and be gone with you.",
            /*17*/"You are certainly not the wickedest %2$s alive, not even close. But let's say there's room for improvement. Take this gift, %1$s, and go start improving.",
            /*18*/"Who is this? Oh, I see, it's %1$s, and you've not done even one wicked thing this year, I'm ashamed! Here, take this and next year I hope you do less good.",
            /*19*/"Oh, you've been very good this year, haven't you %1$s? Too good for my taste. Perhaps some trick or treating would make you somewhat less annoyingly good.",
            /*20*/"I'm impressed you dare showing yourself in my presence, %1$s. Here, have this gift and go trick or treat. Now piss off! *cough*",
            };

    public static final String[] pumpkinKingSpeaksDenyGift = {
            "You ain't fooling me buster, you're not getting any more gifts, and that's final!",
            "Beat it! You've asked me enough already, I've got better things to do.",
            "That was your final gift this year, next year I won't give you anything if you keep this up.",
            "I'm being exploited here! I'll tell Santa about you!",
            "You spoiled brat, there are limits to my generosity, you know!",
            "Pft, I should take those gifts back and give you a good spanking instead.",
            "I'll teach you one or two things about asking too often.",
            "You're being annoying, bugger off and bother someone else.",
            "I've got a special recipe with some human ingredients, and you look delicious. Come closer little human...",
            "I'm getting aggravated here, you don't want to awaken my wrath.",
            };

    private static PumpkinKing instance = null;

    public static PumpkinKing getInstance() {
        if(instance == null) instance = new PumpkinKing();
        return instance;
    }

    public static int getTemplateId() { return PUMPKIN_KING_CID; }

    public static boolean isHalloween() {
        int year = Year.now().getValue();
        if(AwakeningMod.debug) return true;
        return WurmCalendar.nowIsBetween(0, 0, 29, Calendar.OCTOBER, year, 23, 59, 4, Calendar.NOVEMBER, year);
    }

    private Creature creature;
    private int aggroCounter;
    private long aggroTimer;

    private PumpkinKing() {
        creature = null;
        aggroCounter = 0;
        aggroTimer = System.currentTimeMillis();
    }

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_CLIMBER
        };
        int[] itemsButchered = {
                ItemList.tooth,
                ItemList.skull,
                ItemList.boneCollar
        };

        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(PUMPKIN_KING_CID);

        builder.name("Pumpkin King");
        builder.description("The Pumpkin King treats visitors with gifts, for tricking or treating neighbours, friends as well as foes.");
        builder.modelName("model.creature.humanoid.goblin.leader");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_HUMAN);
        builder.vision((short) 20);
        builder.sex((byte) 0);
        builder.dimension((short) 150, (short) 30, (short) 20);
        builder.sizeModifier(120, 120, 120);
        builder.deathSounds(SoundNames.DEATH_TROLL_SND, SoundNames.DEATH_TROLL_SND);
        builder.hitSounds(SoundNames.HIT_TROLL_SND, SoundNames.HIT_TROLL_SND);
        builder.naturalArmour(0.14f);
        builder.damages(18.0f, 25.0f, 0.0f, 0.0f, 0.0f);
        builder.speed(0.75f);
        builder.moveRate(1200);
        builder.maxGroupAttackSize(3);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(40);
        builder.aggressive(99);
        builder.alignment(-70.0f);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_HUMANOID);

        builder.skill(SkillList.BODY_STRENGTH, 80.0f);
        builder.skill(SkillList.BODY_CONTROL, 90.0f);
        builder.skill(SkillList.BODY_STAMINA, 90.0f);
        builder.skill(SkillList.MIND_LOGICAL, 56.0f);
        builder.skill(SkillList.MIND_SPEED, 57.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 70.0f);
        builder.skill(SkillList.SOUL_DEPTH, 30.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 90.0f);
        builder.skill(SkillList.SWORD_SHORT, 90.0f);
        builder.skill(SkillList.SHIELD_MEDIUM_METAL, 90.0f);
        builder.skill(SkillList.GROUP_FIGHTING, 80.0f);
        builder.baseCombatRating(100.0f);

        return builder;
    }

    public boolean isSpawned() {
        if(creature != null && creature.isDead()) creature = null;
        return creature != null;
    }

    @SuppressWarnings("unused")
    public boolean spawn(float posX, float posY, float rot) {
        if(creature == null) {
            try(Connection con = ModSupportDb.getModSupportDb()) {
                creature = Spawner.getInstance().spawnCreature(con, PUMPKIN_KING_CID, posX, posY, rot, 0, null, MiscConstants.SEX_MALE, (byte) -1, Spawner.SPAWN_PUMPKIN_KING);
                return true;
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "Failed to spawn Pumpkin King.", e);
            }
        }
        return false;
    }

    public boolean load(Connection con) {
        try {
            creature = Spawner.getInstance().getSpawnedCreature(con, Spawner.SPAWN_PUMPKIN_KING, true);
            return (creature != null);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load Pumpkin King.", e);
        }
        return false;
    }

    public boolean destroy() {
        int n = Spawner.getInstance().destroySpawnedCreatures(Spawner.SPAWN_PUMPKIN_KING, true);
        creature = null;
        return n > 0;
    }

    public static int getGiftId() {
        int i = Server.rand.nextInt(10000);
        if(i < 8000) return ItemList.potionIllusion;
        else if(i < 8500) return ItemList.pumpkinHalloween;
        else if(i < 8600) return ItemList.opal;
        else if(i < 8700) return ItemList.emerald;
        else if(i < 8800) return ItemList.ruby;
        else if(i < 8900) return ItemList.sapphire;
        else if(i < 9000) return ItemList.diamond;
        else if(i < 9050) return ItemList.opalBlack;
        else if(i < 9100) return ItemList.emeraldStar;
        else if(i < 9150) return ItemList.rubyStar;
        else if(i < 9175) return ItemList.sapphireStar;
        else if(i < 9200) return ItemList.diamondStar;
        else if(i < 9400) return ItemList.coinSilver;
        else if(i < 9440) return ItemList.adamantineBar;
        else if(i < 9480) return ItemList.glimmerSteelBar;
        else if(i < 9500) return ItemList.seryllBar;
        else if(i < 9900) return ItemList.flaskGlass;
        else if(i < 9910) return ItemList.potionFletching;
        else if(i < 9920) return ItemList.potionCarpentry;
        else if(i < 9930) return ItemList.potionWoodcutting;
        else if(i < 9940) return ItemList.potionMasonry;
        else if(i < 9950) return ItemList.potionShipbuilding;
        else if(i < 9960) return ItemList.potionLeatherworking;
        else if(i < 9970) return ItemList.potionBlacksmithing;
        else if(i < 9980) return ItemList.potionArmourSmithing;
        else if(i < 9990) return ItemList.potionWeaponSmithing;
        else if(i < 9999) return ItemList.potionAffinity;
        else return ItemList.goblinLeaderHat;
    }

    public boolean giveHalloweenGift(Creature performer, Creature target) {
        if(!isHalloween()) {
            PumpkinKing.getInstance().speakNotHalloween(performer);
            return false;
        }
        try(Connection con = ModSupportDb.getModSupportDb()) {
            long steamId = performer.isPlayer()? ((Player) performer).getSteamId().getSteamID64() : 0L;
            int n = 0;
            try(PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) FROM GIFTS WHERE STEAMID=? AND GIFTID=? GROUP BY STEAMID")) {
                ps.setLong(1, steamId);
                ps.setInt(2, HALLOWEEN_GIFT_ID);
                try(ResultSet rs = ps.executeQuery()) {
                    if(rs.next()) n = rs.getInt(1);
                }
            } catch(SQLException e) {
                logger.log(Level.WARNING, "SQL error: " + e.getMessage(), e);
            }
            if(n >= HALLOWEEN_GIFT_MAX) {
                speakDenyGift(performer, target);
                return false;
            }
            int templateId = getGiftId();
            float alc = 0.0f, mod = 99.0f, ql = 10.0f;
            int weight = 0;
            boolean rare = false;

            if(performer.isPlayer()) alc = ((Player) performer).getAlcohol();

            switch(templateId) {
                case ItemList.potionIllusion:
                    mod = 198.0f;
                    break;
                case ItemList.coinSilver:
                    rare = true;
                    break;
                case ItemList.adamantineBar:
                case ItemList.glimmerSteelBar:
                case ItemList.seryllBar:
                    weight = 150 + Math.round(Server.rand.nextFloat() * Server.rand.nextFloat() * 850.0f);
                    rare = true;
                    break;
                case ItemList.flaskGlass:
                    rare = true;
                    break;
                default:
                    mod = Math.min(Server.rand.nextFloat() * 4.0f, 1.0f) * 99.0f;
            }

            ql = Math.max(10.0f, Math.min(99.0f, mod * Server.rand.nextFloat() - performer.getAlignment() / 5.0f + alc / 5.0f));
            Item gift = ItemFactory.createItem(templateId, ql, null);
            gift.setCreator(target.getName());
            performer.getInventory().insertItem(gift, true);
            if(rare) {
                int rarity = Server.rand.nextInt(1000000);
                if(rarity == 0) gift.setRarity(MiscConstants.FANTASTIC);
                else if(rarity < 100) gift.setRarity(MiscConstants.SUPREME);
                else if(rarity < 10000) gift.setRarity(MiscConstants.RARE);
            }
            if(weight > 0) gift.setWeight(weight, true);
            try(PreparedStatement ps = con.prepareStatement("INSERT INTO GIFTS (WURMID,STEAMID,GIFTID,SOURCEID,ITEMTEMPLATEID,ITEMID,CREATED) VALUES(?,?,?,?,?,?,?)")) {
                ps.setLong(1, performer.getWurmId());
                ps.setLong(2, steamId);
                ps.setInt(3, HALLOWEEN_GIFT_ID);
                ps.setLong(4, target.getWurmId());
                ps.setInt(5, templateId);
                ps.setLong(6, gift.getWurmId());
                ps.setLong(7, System.currentTimeMillis());
                ps.execute();
            } catch(SQLException e) {
                logger.log(Level.WARNING, "SQL error: " + e.getMessage(), e);
            }
            speakGiveGift(performer, target, gift);
            return true;
        } catch(Exception e) {
            logger.log(Level.WARNING, performer.getName() + " no gift received: " + e.getMessage(), e);
        }
        return false;
    }

    public void speakGiveGift(Creature performer, Creature target, Item gift) {
        if(gift == null) return;
        float alignment = performer.getAlignment();
        int n = Math.round(alignment / 10.0f) + 13 - Server.rand.nextInt(7);
        if(alignment <= -33.3f) n = Math.max(Math.min(n, 9), 0);
        else if(alignment >= 33.3f) n = Math.max(Math.min(n, 20), 11);
        else n = Math.max(Math.min(n, 14), 6);
        String s = StringUtils.format(pumpkinKingSpeaks[n], performer.getName(), performer.getSex() == MiscConstants.SEX_MALE? "man" : "woman");
        logger.info("HalloweenGiftAction.speakGiveGift(alignment: " + alignment + ", n: " + n + ", s: " + s + ")");
        Communicator communicator = performer.getCommunicator();
        communicator.sendNormalServerMessage(s);
        if(gift.getTemplateId() == ItemList.flaskGlass)
            communicator.sendNormalServerMessage("Oh, did I drink that one too? *burp* *hickup*");
    }

    public void speakDenyGift(Creature performer, Creature target) {
        long time = System.currentTimeMillis();
        if(time - aggroTimer > 5000L) aggroCounter = 0;
        if(aggroCounter >= 5 && Server.rand.nextInt(Math.max(20 - aggroCounter, 3)) == 0) {
            target.setTarget(performer.getWurmId(), true);
            aggroCounter = 0;
        } else {
            String s = pumpkinKingSpeaksDenyGift[Server.rand.nextInt(pumpkinKingSpeaksDenyGift.length)];
            performer.getCommunicator().sendNormalServerMessage(s);
            ++aggroCounter;
        }
        aggroTimer = time;
    }

    public void speakNotHalloween(Creature performer) {
        performer.getCommunicator().sendNormalServerMessage("It is not halloween so there are no gifts for you.");
    }
}
