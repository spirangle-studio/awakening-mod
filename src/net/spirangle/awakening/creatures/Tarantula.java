package net.spirangle.awakening.creatures;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.EncounterType;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.EncounterBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.TARANTULA_CID;


public class Tarantula implements ModCreature {

    private static final Logger logger = Logger.getLogger(Tarantula.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_AGG_HUMAN,
                CreatureTypes.C_TYPE_HUNTING,
                CreatureTypes.C_TYPE_ANIMAL,
                CreatureTypes.C_TYPE_CARNIVORE,
                CreatureTypes.C_TYPE_NON_NEWBIE
        };
        int[] itemsButchered = {
                ItemList.gland,
                ItemList.eye,
                ItemList.eye,
                ItemList.eye,
                ItemList.eye
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(TARANTULA_CID);

        builder.name("Tarantula");
        builder.description("This tarantula species is about the size of a wolf, and has a venomous bite.");
        builder.modelName("model.creature.multiped.spider.huge");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_SPIDER);
        builder.vision((short) 5);
        builder.sex((byte) 0);
        builder.dimension((short) 75, (short) 50, (short) 75);
        builder.sizeModifier(32, 32, 32);
        builder.deathSounds(SoundNames.DEATH_SPIDER_SND, SoundNames.DEATH_SPIDER_SND);
        builder.hitSounds(SoundNames.HIT_SPIDER_SND, SoundNames.HIT_SPIDER_SND);
        builder.naturalArmour(0.7f);
        builder.damages(3.0f, 0.0f, 10.0f, 0.0f, 0.0f);
        builder.speed(1.4f);
        builder.moveRate(700);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(10);
        builder.aggressive(74);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_INSECT);
        builder.handDamString("sting");
        builder.alignment(-50.0f);
        builder.maxAge(100);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_PLATE);
        builder.baseCombatRating(15.0f);
        builder.combatDamageType(Wound.TYPE_POISON);
        builder.maxGroupAttackSize(6);
        builder.denName("tarantula lair");
        builder.denMaterial(ItemMaterials.MATERIAL_STONE);
        builder.maxPercentOfCreatures(0.01f);

        builder.skill(SkillList.BODY_STRENGTH, 15.0f);
        builder.skill(SkillList.BODY_CONTROL, 45.0f);
        builder.skill(SkillList.BODY_STAMINA, 35.0f);
        builder.skill(SkillList.MIND_LOGICAL, 8.0f);
        builder.skill(SkillList.MIND_SPEED, 10.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 40.0f);
        builder.skill(SkillList.SOUL_DEPTH, 2.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 40.0f);
        return builder;
    }

    @Override
    public void addEncounters() {
        new EncounterBuilder(Tiles.Tile.TILE_TREE.id, EncounterType.ELEVATION_GROUND).addCreatures(TARANTULA_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_SAND.id, EncounterType.ELEVATION_GROUND).addCreatures(TARANTULA_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_CAVE.id, EncounterType.ELEVATION_GROUND).addCreatures(TARANTULA_CID, 1).build(1);
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(TARANTULA_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "hugespider.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "Tarantula: " + e.getMessage(), e);
        }
    }
}
