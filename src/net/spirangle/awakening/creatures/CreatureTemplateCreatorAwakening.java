package net.spirangle.awakening.creatures;

import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CreatureTemplateCreatorAwakening {

    private static final Logger logger = Logger.getLogger(CreatureTemplateCreatorAwakening.class.getName());

    public static final int PUMPKIN_KING_CID = 0x4801;
    public static final int SHADE_HOUND_CID = 0x5001;
    public static final int RED_DEER_CID = 0x5002;
    public static final int SHADE_CID = 0x5003;
    public static final int NIGHT_STEED_CID = 0x5005;

    public static final int DRAGON_QUEEN_CID = 0x5010;
    public static final int DRAGONET_BLACK_CID = 0x5011;
    public static final int DRAGONET_RED_CID = 0x5012;
    public static final int DRAGONET_GREEN_CID = 0x5013;

    public static final int TARANTULA_CID = 0x5014;

    public static final int VILLAGE_GUARD_TIR_CID = 0x5021;
    public static final int VILLAGE_GUARD_CEL_CID = 0x5022;
    public static final int VILLAGE_GUARD_SHO_CID = 0x5023;
    public static final int VILLAGE_GUARD_THE_CID = 0x5024;

    public static void init() {
        try {
            CreatureTemplate guardSpirit1 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_GOOD_LENIENT);
            ReflectionUtil.setPrivateField(guardSpirit1, ReflectionUtil.getField(guardSpirit1.getClass(), "spiritGuard"), false);

            CreatureTemplate guardSpirit2 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_EVIL_LENIENT);
            ReflectionUtil.setPrivateField(guardSpirit2, ReflectionUtil.getField(guardSpirit2.getClass(), "spiritGuard"), false);

            CreatureTemplate guardSpirit3 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_GOOD_ABLE);
            ReflectionUtil.setPrivateField(guardSpirit3, ReflectionUtil.getField(guardSpirit3.getClass(), "spiritGuard"), false);

            CreatureTemplate guardSpirit4 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_EVIL_ABLE);
            ReflectionUtil.setPrivateField(guardSpirit4, ReflectionUtil.getField(guardSpirit4.getClass(), "spiritGuard"), false);

            CreatureTemplate guardSpirit5 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_GOOD_DANGEROUS);
            ReflectionUtil.setPrivateField(guardSpirit5, ReflectionUtil.getField(guardSpirit5.getClass(), "spiritGuard"), false);

            CreatureTemplate guardSpirit6 = CreatureTemplateFactory.getInstance().getTemplate(CreatureTemplateIds.GUARD_SPIRIT_EVIL_DANGEROUS);
            ReflectionUtil.setPrivateField(guardSpirit6, ReflectionUtil.getField(guardSpirit6.getClass(), "spiritGuard"), false);

        } catch(NoSuchCreatureTemplateException | NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Edit template private field error: " + e.getMessage(), e);
        }
    }
}
