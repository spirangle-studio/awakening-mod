package net.spirangle.awakening.creatures;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.bodys.BodyTemplate;
import com.wurmonline.server.bodys.Wound;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.NoSuchCreatureTemplateException;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.zones.EncounterType;
import com.wurmonline.shared.constants.CreatureTypes;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.constants.SoundNames;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.CreatureTemplateBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.EncounterBuilder;
import org.gotti.wurmunlimited.modsupport.creatures.ModCreature;

import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.RED_DEER_CID;


public class RedDeer implements ModCreature {

    private static final Logger logger = Logger.getLogger(RedDeer.class.getName());

    @Override
    public CreatureTemplateBuilder createCreateTemplateBuilder() {
        final int[] types = {
                CreatureTypes.C_TYPE_ANIMAL,
                CreatureTypes.C_TYPE_MOVE_LOCAL,
                CreatureTypes.C_TYPE_GRAZER,
                CreatureTypes.C_TYPE_DOMESTIC,
                CreatureTypes.C_TYPE_LEADABLE,
                CreatureTypes.C_TYPE_HERBIVORE,
                CreatureTypes.C_TYPE_DOMINATABLE,
                CreatureTypes.C_TYPE_PREY,
                CreatureTypes.C_TYPE_FLEEING
        };
        int[] itemsButchered = {
                ItemList.tail,
                ItemList.hoof,
                ItemList.animalHide,
                ItemList.tallow,
                ItemList.bladder,
                ItemList.eye,
                ItemList.eye,
                ItemList.gland
        };
        CreatureTemplateBuilder builder = new CreatureTemplateBuilder(RED_DEER_CID);
        builder.name("Red deer");
        builder.description("The proud red deer, often depicted on royal insignia.");
        builder.modelName("model.creature.quadraped.deer");
        builder.types(types);
        builder.bodyType(BodyTemplate.TYPE_HORSE);
        builder.vision((short) 7);
        builder.sex((byte) 0);
        builder.dimension((short) 70, (short) 50, (short) 50);
        builder.sizeModifier(100, 100, 100);
        builder.deathSounds(SoundNames.DEATH_DEER_SND, SoundNames.DEATH_DEER_SND);
        builder.hitSounds(SoundNames.HIT_DEER_SND, SoundNames.HIT_DEER_SND);
        builder.naturalArmour(0.3f);
        builder.damages(5.0f, 5.0f, 10.0f, 4.0f, 0.0f);
        builder.speed(1.65f);
        builder.moveRate(100);
        builder.itemsButchered(itemsButchered);
        builder.maxHuntDist(5);
        builder.aggressive(10);
        builder.meatMaterial(ItemMaterials.MATERIAL_MEAT_GAME);
        builder.handDamString("kick");
        builder.maxAge(200);
        builder.alignment(100.0f);
        builder.armourType(ArmourTemplate.ARMOUR_TYPE_CLOTH);
        builder.combatDamageType(Wound.TYPE_CRUSH);
        builder.maxGroupAttackSize(3);
        builder.baseCombatRating(6.0f);
        builder.bonusCombatRating(14);
        builder.maxPercentOfCreatures(0.005f);

        builder.skill(SkillList.BODY_STRENGTH, 30.0f);
        builder.skill(SkillList.BODY_CONTROL, 23.0f);
        builder.skill(SkillList.BODY_STAMINA, 40.0f);
        builder.skill(SkillList.MIND_LOGICAL, 7.0f);
        builder.skill(SkillList.MIND_SPEED, 7.0f);
        builder.skill(SkillList.SOUL_STRENGTH, 52.0f);
        builder.skill(SkillList.SOUL_DEPTH, 8.0f);
        builder.skill(SkillList.WEAPONLESS_FIGHTING, 30.0f);
        return builder;
    }

    @Override
    public void addEncounters() {
        new EncounterBuilder(Tiles.Tile.TILE_TREE.id, EncounterType.ELEVATION_GROUND).addCreatures(RED_DEER_CID, 1).build(1);
        new EncounterBuilder(Tiles.Tile.TILE_TUNDRA.id, EncounterType.ELEVATION_GROUND).addCreatures(RED_DEER_CID, 1).build(1);
    }

    public static void init() {
        try {
            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(RED_DEER_CID);
            ReflectionUtil.setPrivateField(template, ReflectionUtil.getField(CreatureTemplate.class, "corpsename"), "deer.");
        } catch(NoSuchCreatureTemplateException | IllegalAccessException | NoSuchFieldException e) {
            logger.log(Level.SEVERE, "Red Deer: " + e.getMessage(), e);
        }
    }
}
