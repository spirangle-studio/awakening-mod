package net.spirangle.awakening;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.mesh.Tiles.Tile;
import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.Crops;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.economy.Economy;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.items.*;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.questions.*;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillSystem;
import com.wurmonline.server.utils.DbUtilities;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.util.StringUtilities;
import net.spirangle.awakening.actions.ActionStack;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.creatures.Shade;
import net.spirangle.awakening.creatures.Spawner;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.players.LoginHandler;
import net.spirangle.awakening.players.*;
import net.spirangle.awakening.tasks.KingdomAppointmentsTask;
import net.spirangle.awakening.tasks.MerchantTask;
import net.spirangle.awakening.tasks.VendorTask;
import net.spirangle.awakening.time.Scheduler;
import net.spirangle.awakening.time.Seasons;
import net.spirangle.awakening.util.StringUtils;
import net.spirangle.awakening.zones.Infestations;
import net.spirangle.awakening.zones.Treasure;
import net.spirangle.awakening.zones.Treasures;
import org.gotti.wurmunlimited.modloader.interfaces.MessagePolicy;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.wurmonline.server.Command.*;
import static com.wurmonline.server.ServerTweaksHandler.Tweak.*;


public class CommandHandler {

    private static final Logger logger = Logger.getLogger(CommandHandler.class.getName());

    private static final Pattern configPattern = Pattern.compile("^#config\\s+([a-zA-Z0-9]+)(?:\\s+(\\d+))*");
    private static final Pattern restorePattern = Pattern.compile("^#restore\\s+(rock|biomes|cave|mycelium)\\s+(?:" +
                                                                  "(load)\\s+(.+)|" +
                                                                  "(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+))");
    private static final Pattern altsPattern = Pattern.compile("^#alts\\s+([a-zA-Z0-9]+)");
    private static final Pattern guiltPattern = Pattern.compile("^#guilt\\s+([a-zA-Z0-9]+)\\s+(give|remove)(?:\\s+(\\d+)\\s+(.*))?");
    private static final Pattern monitorPattern = Pattern.compile("^#monitor\\s+([a-zA-Z0-9]+)\\s+(\\d+)");
    private static final Pattern pvpPattern = Pattern.compile("^#pvp\\s+([a-zA-Z0-9]+)\\s+(remove)");
    private static final Pattern karmaPattern = Pattern.compile("^#karma\\s+([a-zA-Z0-9]+)\\s+([\\-0-9]+)");
    private static final Pattern rotatePattern = Pattern.compile("^#rotate\\s+(\\d+)\\s+([0-9\\.]+)");
    private static final Pattern inventoryPattern = Pattern.compile("^#inventory\\s+([a-zA-Z0-9]+)\\s+(clear|give)(?:\\s+(\\d+)\\s+(\\d+))?");
    private static final Pattern finePattern = Pattern.compile("^#fine\\s+(\\w+)\\s+([\\d]+)\\s+(.*)");
    private static final Pattern transferPattern = Pattern.compile("^#transfer\\s+(\\w+)\\s+([\\w\\.\\/-]+)\\s+(\\w+)");
    private static final Pattern schedulePattern = Pattern.compile("^#schedule\\s+(?:" +
                                                                   "(stop|list)(?:\\s+([a-zA-Z0-9_]+))?|" +
                                                                   "broadcast\\s+([a-zA-Z0-9]+)\\s+(\\d+)\\/(\\d+)\\s+(#[0-9a-fA-F]{6}|\\d+)\\s+(.+)|" +
                                                                   "winterbeasts\\s+([a-zA-Z0-9]+)\\s+(\\d+)\\/(\\d+)\\s+(\\d+)(?:\\-(\\d+))?" +
                                                                   ")");
    private static final Pattern spawnPattern = Pattern.compile("^#spawn\\s+(?:" +
                                                                "winterbeasts\\s+(\\d+)(?:[\\:,\\- ](\\d+))?" +
                                                                ")");
    private static final Pattern tilePattern = Pattern.compile("^#tile\\s+(\\w+)");
    private static final Pattern setResourcePattern = Pattern.compile("^#setresource\\s+(\\w+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)(?:\\s+(\\d+))?");
    private static final Pattern servantPattern = Pattern.compile("^#servant\\s+(\\d+)\\s+(?:" +
                                                                  "(position|poll|drop|undress|tasks)|" +
                                                                  "face\\s+([0-7]),([0-7]),([0-7]),([0-7]),([0-7]),([0-7]),([0-7]),([0-7]),([0-7]),([0-7])|" +
                                                                  "xmasfood\\s+(\\d+)" +
                                                                  ")");

    private static final Pattern shadesPattern = Pattern.compile("^#shades\\s+(?:" +
                                                                 "(list|poll)$|" +
                                                                 "(\\d+)$|" +
                                                                 "(\\d+)\\s+(disband)$" +
                                                                 ")");

    private static final Pattern getFoodPattern = Pattern.compile("^#getFood\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)$");

    private static final Pattern infestationPattern = Pattern.compile("^#infestation\\s+(create|remove)(?:\\s+([0-5]|[0-4]\\.\\d+)\\s+(\\d+))?");

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private static CommandHandler instance = null;

    public static CommandHandler getInstance() {
        if(instance == null) instance = new CommandHandler();
        return instance;
    }

    private Field serverLag = null;

    private CommandHandler() {
    }

    private static boolean hasPowerForCommand(Player performer, int power, String command) {
        return hasPowerForCommand(performer, power, false, command);
    }

    private static boolean hasPowerForCommand(Player performer, int power, boolean cm, String command) {
        if(performer.getPower() >= power || (cm && performer.mayMute())) return true;
        logger.log(Level.WARNING, "Player " + performer.getName() + " tried to use #" + command + " command.");
        return false;
    }

    public MessagePolicy handleCommand(Communicator communicator, String message, String title) {
        int i1 = message.indexOf(' ');
        int i2 = message.indexOf('-');
        int i = i1 == -1 && i2 == -1? -1 : (i1 == -1? i2 : (i2 == -1? i1 : (i1 < i2? i1 : i2)));
        Command command = getCommand(i == -1? message.trim() : message.substring(0, i));
        if(command != null && command.isCustom()) return command.perform(communicator, message);
        return MessagePolicy.PASS;
    }

    public Command getCommand(String command) {
        if(commandsMap == null) createCommandsMap();
        return commandsMap.get(command);
    }

    public static MessagePolicy handleCommandConfig(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "config")) return MessagePolicy.PASS;
        try {
            Matcher m = configPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String cmd = m.group(1);
                    if(cmd.equals("reload")) reloadConfig(performer);
                    else if(cmd.equals("data")) {
                        try {
                            long data = Long.parseLong(m.group(2));
                            int d1 = (int) (data >> 32);
                            int d2 = (int) data;
                            communicator.sendSafeServerMessage("Values for " + data + ": data1=" + d1 + ", data2=" + d2);
                        } catch(Exception e) { }
                    } else if(cmd.equals("bml")) {
                        int w = 550;
                        int h = 550;
                        if(m.group(2) != null) w = Integer.parseInt(m.group(2));
                        if(m.group(3) != null) h = Integer.parseInt(m.group(3));
                        BMLEditorQuestion question = new BMLEditorQuestion(performer, "", w, h, false);
                        question.sendQuestion();
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Config command usage:");
                communicator.sendSafeServerMessage("#config reload  -- reload the awakening.properties file");
                communicator.sendSafeServerMessage("#config data <data>  -- print values for data1 and data2");
                communicator.sendSafeServerMessage("#config bml  -- open a BML editor");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Config command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandRestore(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "restore")) return MessagePolicy.PASS;
        try {
            Matcher m = restorePattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String type = m.group(1);
                    if(m.group(2) != null && m.group(3) != null) {
                        String cmd = m.group(2);
                        String path = m.group(3);
                        if(path != null && net.spirangle.awakening.zones.Tiles.loadMesh("map.mesh." + type, path) != null) {
                            communicator.sendSafeServerMessage("Loaded mesh for restoring map. [" + path + "]");
                        }
                    } else if(m.group(4) != null && m.group(5) != null && m.group(6) != null && m.group(7) != null) {
                        communicator.sendSafeServerMessage("Restoring map to original state...");
                        int n = 0;
                        int sx = Integer.parseInt(m.group(4));
                        int sy = Integer.parseInt(m.group(5));
                        int ex = Integer.parseInt(m.group(6));
                        int ey = Integer.parseInt(m.group(7));
                        if("biomes".equals(type)) {
                            n = net.spirangle.awakening.zones.Tiles.restoreBiomes(sx, sy, ex, ey);
                        } else if("mycelium".equals(type)) {
                            n = net.spirangle.awakening.zones.Tiles.restoreMycelium(sx, sy, ex, ey);
                        }
                        if(n < 0) {
                            communicator.sendAlertServerMessage("Original map data has not been loaded, or cached out.");
                        } else {
                            communicator.sendSafeServerMessage("Updated " + n + " tiles with original data.");
                        }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Restore command usage:");
                communicator.sendSafeServerMessage("#restore <rock|biomes|cave> load <path>  -- load the mesh data for the original map");
                communicator.sendSafeServerMessage("#restore <rock|biomes|cave|mycelium> <startx> <starty> <endx> <endy>  -- restore map for the given area using loaded mesh data");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Restore command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandAlts(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "alts")) return MessagePolicy.PASS;
        try {
            Matcher m = altsPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String name = m.group(1);
                    PlayerInfo playerInfo = PlayerInfoFactory.getPlayerInfoWithName(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(playerInfo == null) communicator.sendNormalServerMessage("No such player.");
                    else {
                        try {
                            int n = 0;
                            playerInfo.load();
                            Connection db = null;
                            PreparedStatement ps = null;
                            ResultSet rs = null;
                            List<String> steamIds = new ArrayList<>();
                            Set<PlayerInfo> alts = new HashSet<>();
                            try {
                                db = DbConnector.getPlayerDbCon();
                                try {
                                    ps = db.prepareStatement("SELECT DISTINCT STEAM_ID FROM STEAM_IDS WHERE PLAYER_ID=?");
                                    ps.setLong(1, playerInfo.wurmId);
                                    rs = ps.executeQuery();
                                    while(rs.next()) {
                                        steamIds.add(rs.getString("STEAM_ID"));
                                    }
                                } catch(SQLException e) {
                                } finally {
                                    DbUtilities.closeDatabaseObjects(ps, rs);
                                }
                                communicator.sendSafeServerMessage("Listing possible alt characters for " + name +
                                                                   " [IP: " + playerInfo.getIpaddress() + ", SteamID: " + String.join(",", steamIds) + "]:");
                                try {
                                    StringBuilder sql = new StringBuilder();
                                    sql.append("SELECT DISTINCT PLAYER_ID,STEAM_ID FROM STEAM_IDS WHERE PLAYER_ID!=? AND STEAM_ID");
                                    if(steamIds.size() == 1) sql.append("=").append(steamIds.get(0));
                                    else {
                                        sql.append(" IN(").append(steamIds.get(0));
                                        for(int i = 1; i < steamIds.size(); ++i)
                                            sql.append(",").append(steamIds.get(i));
                                        sql.append(")");
                                    }
                                    ps = db.prepareStatement(sql.toString());
                                    ps.setLong(1, playerInfo.wurmId);
                                    rs = ps.executeQuery();
                                    while(rs.next()) {
                                        long wurmId = rs.getLong("PLAYER_ID");
                                        long steamId = rs.getLong("STEAM_ID");
                                        PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithWurmId(wurmId);
                                        if(pi == null || alts.contains(pi)) continue;
                                        if(sendAltPlayerInfo(communicator, pi, steamId)) {
                                            alts.add(pi);
                                            ++n;
                                        }
                                    }
                                } catch(SQLException e) {
                                } finally {
                                    DbUtilities.closeDatabaseObjects(ps, rs);
                                }
                            } catch(SQLException e) {
                            } finally {
                                DbConnector.returnConnection(db);
                            }
                            PlayerInfo[] playerInfos = PlayerInfoFactory.getPlayerInfos();
                            for(PlayerInfo pi : playerInfos) {
                                if(alts.contains(pi)) continue;
                                try {
                                    pi.load();
                                } catch(IOException e) { }
                                if(!pi.getIpaddress().equals(playerInfo.getIpaddress())) continue;
                                if(sendAltPlayerInfo(communicator, pi, -10L)) {
                                    alts.add(pi);
                                    ++n;
                                }
                            }
                            if(n == 0) communicator.sendSafeServerMessage("No other characters found.");
                        } catch(IOException ioe) { }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Alts command usage:");
                communicator.sendSafeServerMessage("#alts <player>  -- list player alts, or possible alts.");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Alts command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandGuilt(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "guilt")) return MessagePolicy.PASS;
        try {
            Matcher m = guiltPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) communicator.sendNormalServerMessage("No such player.");
                    else if(player.getSteamId() == performer.getSteamId() && player.getPower() < MiscConstants.POWER_DEMIGOD) {
                        communicator.sendNormalServerMessage("You cannot give guilt to, or remove from, your own characters.");
                    } else {
                        String cmd = m.group(2);
                        if(cmd.equals("give") && m.group(3) != null && m.group(4) != null) {
                            int time = Integer.parseInt(m.group(3));
                            String reason = m.group(4);
                            player.getCommunicator().sendAlertServerMessage(performer.getName() + " has given you a heavy conscience: " + reason);
                            ClientConnection.giveHeavyConscience(player, time, false, reason);
                            communicator.sendSafeServerMessage("You have given " + name + " a heavy conscience.");
                            ChatChannels.getInstance().onReportsMessage(performer.getName() + " has given " + name + " a heavy conscience for " + time + " minutes.");
                        } else if(cmd.equals("remove")) {
                            player.getCommunicator().sendSafeServerMessage(performer.getName() + " has removed your heavy conscience.");
                            ClientConnection.removeHeavyConscience(player, false);
                            communicator.sendSafeServerMessage("You lifted a heavy conscience from the mind of " + name + ".");
                        }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Guilt command usage:");
                communicator.sendSafeServerMessage("#guilt <name> remove  -- remove heavy conscience from a player");
                communicator.sendSafeServerMessage("#guilt <name> give <time> <reason>  -- give heavy conscience to a player; time=minutes");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Guilt command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandMonitor(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "monitor")) return MessagePolicy.PASS;
        try {
            Matcher m = monitorPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) communicator.sendNormalServerMessage("No such player.");
                    else if(player.getSteamId() == performer.getSteamId() && player.getPower() < MiscConstants.POWER_DEMIGOD) {
                        communicator.sendNormalServerMessage("You cannot monitor your own characters.");
                    } else {
                        long time = Long.parseLong(m.group(2));
                        ActionStack.Pattern pattern = ActionStack.getInstance().getPattern(player);
                        if(pattern == null) {
                            communicator.sendNormalServerMessage("You cannot monitor " + player.getName() + ", there is no action pattern registered yet.");
                        } else {
                            communicator.sendAlertServerMessage("You monitor " + player.getName() + " for " + time + " minutes.");
                            ChatChannels.getInstance().onReportsMessage(performer.getName() + " monitors " + player.getName() + " for " + time + " minutes.");
                            pattern.setLogActionsTime(performer, player, time);
                        }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Monitor command usage:");
                communicator.sendSafeServerMessage("#monitor <name> <time>  -- monitor player's actions, writes actions with timestamps to log-file; time=minutes");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Monitor command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandPvP(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "pvp")) return MessagePolicy.PASS;
        try {
            Matcher m = pvpPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) communicator.sendNormalServerMessage("No such player.");
                    else {
                        String cmd = m.group(2);
                        if(cmd.equals("remove")) {
                            PlayerData pd = PlayersData.getInstance().get(player);
                            if(!pd.isPvP())
                                communicator.sendSafeServerMessage(name + " does not have the PvP setting.");
                            else {
                                pd.setPvP(false);
                                communicator.sendSafeServerMessage("You removed PvP setting from " + name + ".");
                                player.getCommunicator().sendSafeServerMessage("PvP was unchecked in your settings.");
                            }
                        }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("PvP command usage:");
                communicator.sendSafeServerMessage("#pvp <name> <remove>  -- remove PvP setting from a player");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "PvP command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandKarma(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "karma")) return MessagePolicy.PASS;
        try {
            Matcher m = karmaPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) communicator.sendNormalServerMessage("No such player.");
                    else {
                        int karma = Integer.parseInt(m.group(2));
                        if(karma != 0) {
                            int k = player.getKarma();
                            k += karma;
                            if(k < 0) k = 0;
                            player.setKarma(k);
                            communicator.sendSafeServerMessage("You have " + (karma > 0? "given to" : "taken from") + " " + name + " an amount of " + (karma > 0? karma : -karma) + " karma.");
                        }
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Karma command usage:");
                communicator.sendSafeServerMessage("#karma <name> <amount>  -- give an amount of karma to player (use negative number to take instead)");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Karma command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandRotate(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "rotate")) return MessagePolicy.PASS;
        try {
            Matcher m = rotatePattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    long wurmId = Long.parseLong(m.group(1));
                    Creature creature = Creatures.getInstance().getCreature(wurmId);
                    float rot = Float.parseFloat(m.group(2));
                    creature.setRotation(rot);
                    creature.moved(0, 0, 0, 0, 0);
                    communicator.sendSafeServerMessage(StringUtils.format("Set rotatation of %d to %.2f degrees.", wurmId, rot));
                }
            } else {
                communicator.sendSafeServerMessage("Rotate command usage:");
                communicator.sendSafeServerMessage("#rotate <wurmId> <degrees>  -- set rotation of a creature");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Rotate command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandInventory(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "inventory")) return MessagePolicy.PASS;
        try {
            Matcher m = inventoryPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Creature creature = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(creature == null) {
                        long wurmId = Long.parseLong(name);
                        creature = Creatures.getInstance().getCreature(wurmId);
                    }
                    String cmd = m.group(2);
                    if(cmd.equals("clear")) {
                        Set<Item> items = creature.getInventory().getItems();
                        Item[] itarr = items.toArray(new Item[items.size()]);
                        int i;
                        for(i = 0; i < itarr.length; ++i)
                            Items.destroyItem(itarr[i].getWurmId());
                        communicator.sendSafeServerMessage("Cleared inventory of " + i + " items from " + name + ".");
                        return MessagePolicy.DISCARD;
                    } else if(cmd.equals("give") && m.group(3) != null && m.group(4) != null) {
                        int itemId = Integer.parseInt(m.group(3));
                        float ql = (float) Integer.parseInt(m.group(4));
                        Item item = ItemFactory.createItem(itemId, ql, performer.getName());
                        creature.getInventory().insertItem(item);
                        communicator.sendSafeServerMessage("Created " + item.getName() + " and gave to " + creature.getName() + ".");
                        return MessagePolicy.DISCARD;
                    }
                }
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Inventory command error: " + e.getMessage(), e);
            return MessagePolicy.DISCARD;
        }
        communicator.sendSafeServerMessage("Inventory command usage:");
        communicator.sendSafeServerMessage("#inventory <wurmId> clear  -- destroy all items in the creature's inventory");
        communicator.sendSafeServerMessage("#inventory <wurmId> give <itemId> <qualityLevel>  -- create item and place in creature's inventory");
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandFine(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "fine")) return MessagePolicy.PASS;
        try {
            Matcher m = finePattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) {
                        communicator.sendAlertServerMessage("No such player.");
                        return MessagePolicy.DISCARD;
                    }
                    long fine = Long.parseLong(m.group(2));
                    if(fine <= 0L) return MessagePolicy.DISCARD;
                    String reason = m.group(3);
                    if(reason == null) {
                        communicator.sendAlertServerMessage("Give the player a reason for the fine.");
                        return MessagePolicy.DISCARD;
                    }
                    long money = player.getMoney();
                    long debt = 0L;
                    if(fine > money) {
                        debt = fine - money;
                        fine = money;
                    }
                    player.chargeMoney(fine);
                    final Shop kingsMoney = Economy.getEconomy().getKingsShop();
                    kingsMoney.setMoney(kingsMoney.getMoney() + fine);
                    String change = new Change(fine + debt).getChangeString();
                    communicator.sendSafeServerMessage("You make " + player.getName() + " pay a fine of " + change + ".");
                    player.getCommunicator().sendAlertServerMessage("You were fined " + change + " by " + performer.getName() + " with the reason: " + reason);
                    ChatChannels.getInstance().onReportsMessage(performer.getName() + " has fined " + player.getName() + " " + change + " with the reason: " + reason);
                    if(debt > 0L) {
                        change = new Change(debt).getChangeString();
                        player.getCommunicator().sendAlertServerMessage("Since you couldn't pay your fine, you were given a " +
                                                                        "heavy conscience to pay off your debt of " + change + ".");
                        int time = (int) (debt / 50L);
                        ClientConnection.giveHeavyConscience(player, time, false, reason);
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Fine usage:");
                communicator.sendSafeServerMessage("#fine <player> <amount> <reason>  -- make a player pay a fine in iron as a penalty");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Fine command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandTransfer(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "transfer")) return MessagePolicy.PASS;
        try {
            Matcher m = transferPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null && m.group(3) != null) {
                    String name = m.group(1);
                    Player player = Players.getInstance().getPlayerOrNull(com.wurmonline.server.LoginHandler.raiseFirstLetter(name));
                    if(player == null) {
                        communicator.sendAlertServerMessage("No such player.");
                        return MessagePolicy.DISCARD;
                    }
                    try(Connection con = AwakeningDb.getConnection(m.group(2))) {
                        if(con == null) {
                            communicator.sendAlertServerMessage("No such database file.");
                            return MessagePolicy.DISCARD;
                        }
                        String transferPlayer = m.group(3);
                        long transferId = 0L, money = 0L;
                        int karma = 0;
                        try(PreparedStatement ps = con.prepareStatement("SELECT WURMID,IPADDRESS,MONEY,KARMA FROM PLAYERS WHERE NAME=?")) {
                            ps.setString(1, transferPlayer);
                            try(ResultSet rs = ps.executeQuery()) {
                                if(rs.next()) {
                                    transferId = rs.getLong(1);
                                    money = rs.getLong(3);
                                    karma = rs.getInt(4);
                                }
                            }
                        }
                        if(transferId != 0L) {
                            int n = 0;
                            player.addMoney(money);
                            player.getCommunicator().sendSafeServerMessage("You gain " + new Change(money).getChangeString() + ".");
                            player.setKarma(player.getKarma() + karma);
                            player.getCommunicator().sendSafeServerMessage("You gain " + karma + " karma.");
                            try(PreparedStatement ps = con.prepareStatement("SELECT NUMBER,VALUE FROM SKILLS WHERE OWNER=?")) {
                                ps.setLong(1, transferId);
                                try(ResultSet rs = ps.executeQuery()) {
                                    while(rs.next()) {
                                        int number = rs.getInt(1);
                                        double value = rs.getDouble(2);
                                        Skill skill = player.getSkills().getSkillOrLearn(number);
                                        if(skill.getKnowledge() < value) {
                                            skill.setKnowledge(value, false);
                                            ++n;
                                        }
                                    }
                                    if(n > 0) {
                                        player.getCommunicator().sendSafeServerMessage("You transfer " + n + " skills.");
                                    }
                                }
                            }
                            communicator.sendAlertServerMessage("You transfer " + player.getName() + ", who gained: " + new Change(money).getChangeString() +
                                                                ", " + karma + " karma, and changed " + n + " skills.");
                        }
                    } catch(SQLException e) {
                        logger.log(Level.SEVERE, "Failed to transfer player data: " + e.getMessage(), e);
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Transfer player usage:");
                communicator.sendSafeServerMessage("#transfer <player> <dbFile> <dbPlayer>  -- transfer a player's money, karma and skills from an external db");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Transfer command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandSQL(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "sql")) return MessagePolicy.PASS;
        if(message.startsWith("#sql ")) {
            String sql = message.substring(5).trim();
            logger.info("Execute SQL: " + sql);
            communicator.sendNormalServerMessage("Execute SQL: " + sql);
            try(Connection con = ModSupportDb.getModSupportDb();
                Statement st = con.createStatement()) {
                st.execute(sql);
                communicator.sendNormalServerMessage("Done");
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "SQL error.", e);
                communicator.sendNormalServerMessage("SQL error (see server log).");
            }
            return MessagePolicy.DISCARD;
        } else if(message.startsWith("#sql-")) {
            int i = message.indexOf(' ');
            if(i >= 0) {
                String db = message.substring(5, i).trim();
                String sql = message.substring(i + 1).trim();
                logger.info("Execute SQL: " + sql);
                communicator.sendNormalServerMessage("Execute SQL: " + sql);
                try(Connection con = AwakeningDb.getConnection("sqlite/" + db);
                    Statement st = con.createStatement()) {
                    st.execute(sql);
                    communicator.sendNormalServerMessage("Done");
                } catch(SQLException e) {
                    logger.log(Level.SEVERE, "SQL error.", e);
                    communicator.sendNormalServerMessage("SQL error (see server log).");
                }
                return MessagePolicy.DISCARD;
            }
        }
        communicator.sendAlertServerMessage("Usage: #sql[-<database>] <sql>");
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandSchedule(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        boolean gm = performer.getPower() >= MiscConstants.POWER_DEMIGOD;
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, true, "schedule")) return MessagePolicy.PASS;
        try {
            Matcher m = schedulePattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String command = m.group(1);
                    String name = m.group(2);

                    if(command.equals("list")) {
                        Scheduler.getInstance().sendList(communicator);
                    } else if(command.equals("stop")) {
                        Scheduler.getInstance().stop(communicator, name);
                    }

                } else if(m.group(3) != null) {
                    String name = m.group(3);
                    int start = Integer.parseInt(m.group(4), 10);
                    int delay = Integer.parseInt(m.group(5), 10);
                    String s = m.group(6);
                    int color = 0xffffff;
                    if(s.charAt(0) == '#') color = Integer.parseInt(s.substring(1), 16);
                    else color = Integer.parseInt(s, 10);
                    String text = m.group(7);
                    Scheduler.getInstance().startBroadCast(communicator, name, start, delay, color, text);
                } else if(m.group(8) != null && gm) {
                    String name = m.group(8);
                    int start = Integer.parseInt(m.group(9), 10);
                    int delay = Integer.parseInt(m.group(10), 10);
                    int min = Integer.parseInt(m.group(11), 10);
                    int max = min;
                    if(m.group(12) != null) {
                        max = Integer.parseInt(m.group(12), 10);
                        if(max < min) {
                            int n = min;
                            min = max;
                            max = n;
                        }
                    }
                    Scheduler.getInstance().startWinterBeasts(communicator, name, start, delay, min, max);
                }
                //				communicator.sendSafeServerMessage("1:"+m.group(1)+", 2:"+m.group(2)+", 3:"+m.group(3)+", 4:"+m.group(4)+
                //					", 5:"+m.group(5)+", 6:"+m.group(6)+", 7:"+m.group(7)+", 8:"+m.group(8)+", 9:"+m.group(9)+", 10:"+m.group(10)+
                //					", 11:"+m.group(11)+", 12:"+m.group(12));
            } else {
                communicator.sendSafeServerMessage("Schedule command usage:");
                communicator.sendSafeServerMessage("#schedule list  -- print a list of all active tasks");
                communicator.sendSafeServerMessage("#schedule stop [command or id]  -- stop all tasks, or by command or id");
                communicator.sendSafeServerMessage("#schedule broadcast <id> <start/delay> <color> <message>  -- broadcast message, start=minute of the hour, delay=minutes");
                if(gm)
                    communicator.sendSafeServerMessage("#schedule winterbeasts <id> <start/delay> <min[-max]>  -- call winterbeasts command");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Schedule command error.", e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandSpawn(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "spawn")) return MessagePolicy.PASS;
        try {
            Matcher m = spawnPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    int min = Integer.parseInt(m.group(1), 10);
                    int max = min;
                    if(m.group(2) != null) max = Integer.parseInt(m.group(2), 10);
                    if(min < 1 || min > 10) {
                        communicator.sendAlertServerMessage("Minimum number value out of range");
                    } else if(max < min || max < 1 || max > 10) {
                        communicator.sendAlertServerMessage("Maximum number value out of range");
                    } else {
                        logger.info("#spawn winterbeasts at " + performer.getTileX() + "," + performer.getTileY() +
                                    " by " + performer.getName() + ", min=" + min + " max=" + max);
                        communicator.sendNormalServerMessage("Creating beasts of winter...");
                        int[] n = Spawner.getInstance().spawnWinterBeasts(min, max);
                        if(n != null)
                            communicator.sendNormalServerMessage("Spawned " + n[1] + " creatures in " + n[0] + " villages.");
                        else communicator.sendNormalServerMessage("Something went wrong (see server log).");
                        communicator.sendNormalServerMessage("Done");
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Spawn command usage:");
                communicator.sendSafeServerMessage("#spawn winterbeasts <min>[:max]  -- spawn bears or wolves in villages");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Winter beasts command error.", e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandTile(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "tile")) return MessagePolicy.PASS;
        try {
            Matcher m = tilePattern.matcher(message);
            if(m.find()) {
                String command = m.group(1).toLowerCase();
                int x = performer.getTileX();
                int y = performer.getTileY();
                int currTileId = Server.surfaceMesh.getTile(x, y);
                byte type = Tiles.decodeType(currTileId);
                byte data = Tiles.decodeData(currTileId);
                if("checkfarmgrowth".equals(command)) {
                    VolaTile tile = Zones.getOrCreateTile(x, y, true);
                    boolean farmed = Crops.decodeFieldState(data);
                    boolean growth = net.spirangle.awakening.zones.Tiles.checkForFarmGrowth(tile, farmed);
                    communicator.sendNormalServerMessage("Test checkForFarmGrowth(x=" + x + ", y=" + y + ", farmed=" + farmed + ") = " + growth);
                } else if("checktreesprout".equals(command)) {
                    boolean sprout = net.spirangle.awakening.zones.Tiles.checkForTreeSprout(x, y, type, data);
                    communicator.sendNormalServerMessage("Test checkForTreeSprout(x=" + x + ", y=" + y + ") = " + sprout);
                } else if("treasure".equals(command)) {
                    int rock = Tiles.decodeHeight(Server.rockMesh.getTile(x, y));
                    int height = Tiles.decodeHeight(currTileId);
                    int depth = 5;
                    if(height - rock <= depth) {
                        communicator.sendNormalServerMessage("The dirt isn't deep enough for burying a treasure here.");
                    } else {
                        Item map = ItemFactory.createItem(ItemTemplateCreatorAwakening.treasureMap, 20.0f, MiscConstants.COMMON, performer.getName());
                        if(map != null) {
                            byte a = (byte) Server.rand.nextInt(8);
                            map.setAuxData(a);
                            Treasure treasure = Treasures.getInstance().createTreasure(map, x, y, height - depth, a);
                            if(treasure != null) {
                                map.setData(treasure.getChestId());
                                performer.getInventory().insertItem(map, true);
                                communicator.sendNormalServerMessage("You hide a treasure chest where you are standing, and create a treasure map.");
                                communicator.sendNormalServerMessage("Bury treasure at: x=" + x + ", y=" + y + ", depth=" + depth);
                            }
                        }
                    }
                } else {
                    communicator.sendNormalServerMessage("Unknown tile command: " + command);
                }
            } else {
                communicator.sendAlertServerMessage("Usage: #tile <checkFarmGrowth|checkTreeSprout|treasure>");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Tile command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandSetResource(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "setresource")) return MessagePolicy.PASS;
        try {
            Matcher m = setResourcePattern.matcher(message);
            if(m.find()) {
                String resource = m.group(1).toLowerCase();
                int type;
                switch(resource) {
                    case "copper":
                        type = Tile.TILE_CAVE_WALL_ORE_COPPER.id;
                        break;
                    case "gold":
                        type = Tile.TILE_CAVE_WALL_ORE_GOLD.id;
                        break;
                    case "sandstone":
                        type = Tile.TILE_CAVE_WALL_SANDSTONE.id;
                        break;
                    case "silver":
                        type = Tile.TILE_CAVE_WALL_ORE_SILVER.id;
                        break;
                    case "slate":
                        type = Tile.TILE_CAVE_WALL_SLATE.id;
                        break;
                    default:
                        communicator.sendNormalServerMessage("This is not a supported resource.");
                        return MessagePolicy.DISCARD;
                }

                int l = Integer.parseInt(m.group(2));
                int t = Integer.parseInt(m.group(3));
                int r = Integer.parseInt(m.group(4));
                int b = Integer.parseInt(m.group(5));
                int min = Integer.parseInt(m.group(6));
                int max = min;
                if(m.group(7) != null)
                    max = Integer.parseInt(m.group(7));
                if(min < 1 || min > 0xffff) {
                    communicator.sendAlertServerMessage("Minimum number value out of range");
                } else if(max < min || max < 1 || max > 0xffff) {
                    communicator.sendAlertServerMessage("Maximum number value out of range");
                } else {

                    logger.info("#setresource by " + performer.getName() + ", resource=" + resource + ", min=" + min + ", max=" + max);
                    communicator.sendNormalServerMessage("Setting resource  [" + (max == min? "" + min : min + "<=>" + max) + "]...");

                    int x, y, n = 0;
                    int w = Zones.worldTileSizeX;
                    int h = Zones.worldTileSizeY;
                    if(l > r) {
                        x = r;
                        r = l;
                        l = x;
                    }
                    if(t > b) {
                        y = b;
                        b = t;
                        t = y;
                    }
                    if(l < 0) l = 0;
                    if(t < 0) t = 0;
                    if(r >= w) r = w - 1;
                    if(b >= h) b = h - 1;
                    for(x = l; x <= r; ++x)
                        for(y = t; y <= b; ++y) {
                            if(Tiles.decodeType(Server.caveMesh.getTile(x, y)) == type) {
                                //			            	res = Server.getCaveResource(x,y);
                                Server.setCaveResource(x, y, min + (max > min? Server.rand.nextInt(max - min) : 0));
                                ++n;
                            }
                        }

                    communicator.sendNormalServerMessage("Updated resource value for " + n + " " + resource + " tiles.");
                }
            } else {
                communicator.sendAlertServerMessage("Usage: #setresource <resource> <startx> <starty> <endx> <endy> <minimum> [maximum]");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Set resource command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandFlowersFix(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_IMPLEMENTOR, "flowersfix")) return MessagePolicy.PASS;

        logger.info("#flowersfix by " + performer.getName());
        communicator.sendNormalServerMessage("Fixing flowers...");

        int n = net.spirangle.awakening.zones.Tiles.fixFlowers(0, 0, Zones.worldTileSizeX - 1, Zones.worldTileSizeY - 1);
        if(n >= 0) {
            communicator.sendNormalServerMessage("Updated flowers for " + n + " tiles.");
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandServant(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "servant")) return MessagePolicy.PASS;
        try {
            Matcher m = servantPattern.matcher(message);
            if(m.find()) {
                long npcId = Long.parseLong(m.group(1));
                Servant servant = Servant.getServant(npcId);
                if(servant != null) {
                    Creature npc = servant.npc;
                    if(m.group(2) != null) {
                        String command = m.group(2);
                        switch(command) {
                            case "position":
                                servant.savePosition();
                                communicator.sendSafeServerMessage("Save fixed position for servant " + npc.getName() + ".");
                                break;
                            case "poll":
                                servant.pollDaily();
                                communicator.sendSafeServerMessage("Called daily poll for servant " + npc.getName() + ".");
                                break;
                            case "drop": {
                                int n = servant.dropItems(false);
                                communicator.sendSafeServerMessage("Forcing servant " + npc.getName() + " to drop " + n + " items from inventory.");
                                break;
                            }
                            case "undress": {
                                int n = servant.undress();
                                communicator.sendSafeServerMessage("Forcing servant " + npc.getName() + " to undress, dropped " + n + " items.");
                                break;
                            }
                            case "tasks": {
                                EditServantTasksQuestion question = new EditServantTasksQuestion(performer, servant);
                                question.sendQuestion();
                                break;
                            }
                        }
                    } else if(m.group(3) != null) {
                        long headType = Long.parseLong(m.group(3));
                        long eyeType = Long.parseLong(m.group(4));
                        long noseType = Long.parseLong(m.group(5));
                        long chinType = Long.parseLong(m.group(6));
                        long complexionType = Long.parseLong(m.group(7));
                        long skinColor = Long.parseLong(m.group(8));
                        long eyeColor = Long.parseLong(m.group(9));
                        long hairType = Long.parseLong(m.group(10));
                        long facialHairOrEyeBrow = Long.parseLong(m.group(11));
                        long hairColor = Long.parseLong(m.group(12));
                        long face = headType + (eyeType << 3L) + (complexionType << 6L) + (chinType << 9L) + (hairType << 12L) + (noseType << 15L) +
                                    (facialHairOrEyeBrow << 18L) + (eyeColor << 21L) + (hairColor << 24L) + (skinColor << 27L);
                        servant.face = face;
                        servant.updateDb();
                        int x = npc.getTileX();
                        int y = npc.getTileY();
                        VolaTile tile = Zones.getOrCreateTile(x, y, npc.isOnSurface());
                        if(tile != null) tile.setNewFace(npc);
                        communicator.sendSafeServerMessage("Changed face to " + face + " for servant " + npc.getName() + ".");
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Servant command usage:");
                communicator.sendSafeServerMessage("#servant <wurmId> drop  -- make servant drop items in inventory, but not undress");
                communicator.sendSafeServerMessage("#servant <wurmId> face <head>,<eye>,<nose>,<chin>,<compl>,<skin>,<ecol>,<hair>,<fhair>,<hcol>  -- set face of servant");
                communicator.sendSafeServerMessage("#servant <wurmId> poll  -- force the daily poll");
                communicator.sendSafeServerMessage("#servant <wurmId> position  -- set current position and rotation to fixed");
                communicator.sendSafeServerMessage("#servant <wurmId> undress  -- undress servant, and drop clothes to the ground");
                communicator.sendSafeServerMessage("#servant <wurmId> tasks  -- list all tasks, the tasks are listed as raw JSON data");
                communicator.sendSafeServerMessage("#servant <wurmId> xmasfood <nr>  -- create and give christmas food");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Servant command error.", e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandShades(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "shades")) return MessagePolicy.PASS;
        try {
            Matcher m = shadesPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String cmd = m.group(1);
                    if(cmd.equals("list")) {
                        Shade.sendList(communicator);
                    } else if(cmd.equals("poll")) {
                        Shade.poll();
                    }
                } else if(m.group(2) != null) {
                    int num = Integer.parseInt(m.group(2));
                    Shade.createShadeParty(num, performer.getTileX(), performer.getTileY(), true);
                    communicator.sendSafeServerMessage("Created a shade party of " + num + " members at " + performer.getTileX() + "," + performer.getTileY() + ".");
                } else if(m.group(3) != null) {
                    long partyId = Long.parseLong(m.group(3));
                    Shade.disbandShadeParty(communicator, partyId);
                }
            } else {
                communicator.sendSafeServerMessage("Shades command usage:");
                communicator.sendSafeServerMessage("#shades list -- list all shade parties with coordinates and ids");
                communicator.sendSafeServerMessage("#shades <num>  -- create a shade party which will spawn at the point where you are standing");
                communicator.sendSafeServerMessage("#shades <id> disband  -- disband a shade party");
                communicator.sendSafeServerMessage("#shades poll -- run the daily poll for all shade parties");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Shade command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandGetFood(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "getFood")) return MessagePolicy.PASS;
        try {
            Matcher m = getFoodPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null && m.group(2) != null && m.group(3) != null && m.group(4) != null) {
                    int templateId = Integer.parseInt(m.group(1));
                    int recipeId = Integer.parseInt(m.group(2));
                    byte stages = (byte) Integer.parseInt(m.group(3));
                    byte ingredients = (byte) Integer.parseInt(m.group(4));
                    ItemTemplateFactory itf = ItemTemplateFactory.getInstance();
                    ItemTemplate template = itf.getTemplate(templateId);
                    byte material = template.getMaterial();
                    Recipe recipe = Recipes.getRecipeById((short) recipeId);
                    Item item = ItemFactory.createItem(template.getTemplateId(), 100.0f, material, (byte) 0, null);
                    if(recipe != null) {
                        item.setName(recipe.getName());
                        short calories = template.getCalories();
                        short carbs = template.getCarbs();
                        short fats = template.getFats();
                        short proteins = template.getProteins();
                        byte bonus = (byte) Server.rand.nextInt(SkillSystem.getNumberOfSkillTemplates());
                        ItemMealData.save(item.getWurmId(), recipe.getRecipeId(), calories, carbs, fats, proteins, bonus, stages, ingredients);
                    }
                    item.setWeight(1000, true);

                    performer.getInventory().insertItem(item, true);
                    performer.getCommunicator().sendSafeServerMessage("You get some " + item.getName() + ".");
                }
            } else {
                communicator.sendSafeServerMessage("Get Food command usage:");
                communicator.sendSafeServerMessage("#getFood <templateId> <recipeId> <stages> <ingredients> -- create food, e.g. #getFood 1177 1270 10 10");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "GetFood command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandInfestation(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "infestation")) return MessagePolicy.PASS;
        try {
            Matcher m = infestationPattern.matcher(message);
            if(m.find()) {
                if(m.group(1) != null) {
                    String cmd = m.group(1);
                    if(cmd.equals("create") && m.group(2) != null && m.group(3) != null) {
                        int tileX = performer.getTileX();
                        int tileY = performer.getTileY();
                        float power = Float.parseFloat(m.group(2));
                        int type = Integer.parseInt(m.group(3));
                        Infestations.getInstance().createInfestation(tileX, tileY, power, type);
                    }
                }
            } else {
                communicator.sendSafeServerMessage("Infestation command usage:");
                communicator.sendSafeServerMessage("#infestation create <power> <type> -- create an infestation on the tile you're standing");
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Infestation command error: " + e.getMessage(), e);
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandAppointments(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "appointments")) return MessagePolicy.PASS;
        KingdomAppointmentsTask.listApplications(communicator);
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandSettings(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(!hasPowerForCommand(performer, MiscConstants.POWER_DEMIGOD, "settings")) return MessagePolicy.PASS;
        if(message.length() > 9) {
            String name = com.wurmonline.server.LoginHandler.raiseFirstLetter(message.substring(9).trim());
            PlayerInfo playerInfo = PlayerInfoFactory.getPlayerInfoWithName(name);
            if(playerInfo == null) {
                communicator.sendAlertServerMessage("No such player named \"" + name + "\".");
                return MessagePolicy.DISCARD;
            }
            try {
                playerInfo.load();
            } catch(IOException ioe) { }
            SettingsQuestion sq = new SettingsQuestion(performer, playerInfo);
            sq.sendQuestion();
        }
        return MessagePolicy.DISCARD;
    }

    public static boolean reloadConfig(Player performer) {
        Path path = Paths.get("mods/awakening.properties");
        if(!Files.exists(path)) {
            performer.getCommunicator().sendAlertServerMessage("The config file seems to be missing.");
            return true;
        }
        InputStream stream = null;
        try {
            performer.getCommunicator().sendAlertServerMessage("Opening the config file.");
            stream = Files.newInputStream(path);
            Properties properties = new Properties();
            performer.getCommunicator().sendAlertServerMessage("Reading from the config file.");
            properties.load(stream);
            logger.info("Reloading configuration.");
            performer.getCommunicator().sendAlertServerMessage("Loading all options.");
            Config.getInstance().configure(properties);
            logger.info("Configuration reloaded.");
            performer.getCommunicator().sendAlertServerMessage("The config file has been reloaded.");
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Error while reloading properties file.", e);
            performer.getCommunicator().sendAlertServerMessage("Error reloading the config file, check the server log.");
        } finally {
            try {
                if(stream != null) stream.close();
            } catch(Exception e) {
                logger.log(Level.SEVERE, "Properties file not closed, possible file lock.", e);
                performer.getCommunicator().sendAlertServerMessage("Error closing the config file, possible file lock.");
            }
        }
        return true;
    }

    public static MessagePolicy handleCommandCombat(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        CombatData data = PlayersData.getInstance().getCombatData(performer);
        String s = "Since last login you've ";
        boolean bury = true;
        if(performer.getDeity() != null) {
            Deity deity = performer.getDeity();
            bury = !deity.isAllowsButchering();
        }
        if(data.getSumKills() + data.getSumKarma() > 0) {
            if(data.getSumKills() > 0) {
                s += "killed " + data.getSumKills() + " creatures and gained " + StringUtils.bigdecimalFormat.format(data.getFightSkillGain()) + " fighting skill";
                if(data.getMaxFightSkillGainTemplateId() >= 0) {
                    try {
                        CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(data.getMaxFightSkillGainTemplateId());
                        s += ", most was " + StringUtils.bigdecimalFormat.format(data.getMaxFightSkillGain()) + " from " + StringUtilities.addGenus(template.getName(), false);
                    } catch(Exception e) { }
                }
            }
            if(data.getSumKarma() > 0) {
                s += data.getSumKills() > 0? ". You've " : "";
                s += "gained a total of " + data.getSumKarma() + " karma";
                if(data.getMaxKarma() > 0) {
                    s += ", of which the highest karma was " + data.getMaxKarma();
                    if(data.getMaxKarmaTemplateId() >= 0) {
                        try {
                            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(data.getMaxKarmaTemplateId());
                            s += " from " + (bury? "burying" : "killing") + " " + StringUtilities.addGenus(template.getName(), false);
                        } catch(Exception e) { }
                    }
                }
            }
            s += ".";
        } else {
            s += "killed no creatures and gained no karma" + (bury? " from burying corpses" : " by draining their souls") + ".";
        }
        communicator.sendSafeServerMessage(s);
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandCharacter(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(performer.isPlayer()) {
            String name = message.length() > 10? message.substring(10).trim() : null;
            CharacterQuestion question = new CharacterQuestion(performer, null, name);
            question.sendQuestion();
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandLag(Communicator communicator, String message) {
        CommandHandler ch = getInstance();
        if(ch.serverLag == null) {
            try {
                ch.serverLag = Server.class.getDeclaredField("secondsLag");
                ch.serverLag.setAccessible(true);
            } catch(NoSuchFieldException e) {
                logger.log(Level.SEVERE, "Could not retrieve towers field in class Kingdoms: " + e.getMessage(), e);
            }
        }
        if(ch.serverLag != null) {
            Player performer = communicator.getPlayer();
            try {
                int lag = (int) ch.serverLag.get(null);
                performer.getCommunicator().sendSafeServerMessage("Total lag in seconds since server start: " + lag);
            } catch(IllegalAccessException e) { }
        }
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandBugMessage(Communicator communicator, String message) {
        return ChatChannels.getInstance().onBugMessage(communicator, message.substring(5));
    }

    public static MessagePolicy handleCommandSuggestionMessage(Communicator communicator, String message) {
        return ChatChannels.getInstance().onSuggestionMessage(communicator, message.substring(12));
    }

    public static MessagePolicy handleCommandBank(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        long money = performer.getMoney();
        performer.getCommunicator().sendSafeServerMessage("Your bank account currently contains: " + new Change(money).getChangeString());
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandPvPList(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        LoginHandler.sendPvPList(performer);
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandMarket(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        if(message.length() > 7) {
            String name = message.substring(7).trim();
            ItemTemplate template = ItemTemplateFactory.getInstance().getTemplate(name);
            if(template != null) {
                int merchantWare = MerchantTask.getItemCategory(template.getTemplateId());
                int vendorWare = VendorTask.getItemCategory(template.getTemplateId());
                if(merchantWare != MerchantTask.CATEGORY_NONE || vendorWare != VendorTask.CATEGORY_NONE) {
                    MarketQuestion question = new MarketQuestion(performer, template);
                    question.sendQuestion();
                    return MessagePolicy.DISCARD;
                }
            }
        }
        performer.getCommunicator().sendAlertServerMessage("No such ware is traded by either merchants or vendors.");
        return MessagePolicy.DISCARD;
    }

    public static MessagePolicy handleCommandVendors(Communicator communicator, String message) {
        Player performer = communicator.getPlayer();
        Kingdom kingdom;
        if(message.length() > 8) {
            String name = message.substring(8).trim();
            kingdom = Kingdoms.getKingdomWithName(name);
            if(kingdom == null) {
                performer.getCommunicator().sendAlertServerMessage("No kingdom by the name of \"" + name + "\" exists in this world.");
                return MessagePolicy.DISCARD;
            }
        } else {
            kingdom = Kingdoms.getKingdomOrNull(performer.getKingdomId());
        }
        if(kingdom != null) {
            EconomyQuestion eq = new EconomyQuestion(performer, kingdom, EconomyQuestion.SUBJECT_VENDORS);
            eq.sendQuestion();
        }
        return MessagePolicy.DISCARD;
    }

    private static boolean sendAltPlayerInfo(Communicator communicator, PlayerInfo pi, long steamId) {
        try {
            pi.load();
            String ip = pi.getIpaddress();
            String text = pi.getName() + " [IP: " + (ip != null? ip.substring(1) : "-") + ", Steam ID: " +
                          (steamId != -10L? steamId : (pi.getSteamId() != null? pi.getSteamId().getSteamID64() : "missing")) + "]";
            Player player = Players.getInstance().getPlayerOrNull(pi.wurmId);
            if(player != null) text += " - online";
            else text += " - last seen: " + dateTimeFormat.format(new Date(pi.lastLogout));
            communicator.sendSafeServerMessage(text);
            return true;
        } catch(IOException ioe) { }
        return false;
    }

    private static Map<String, Command> commandsMap = null;

    private static void createCommandsMap() {
        commandsMap = Arrays.stream(commands).sorted(Command::compareTo)
                            .collect(Collectors.toMap(Command::getCommand, c -> c, (c1, c2) -> c1, LinkedHashMap::new));
    }

    @SuppressWarnings("unused")
    public static void sendHelp(Communicator com) {
        com.sendHelpMessage("Current available commands:");
        if(commandsMap == null) createCommandsMap();
        for(final Command command : commandsMap.values()) {
            if(command.getCommand().charAt(0) == '/' && command.canSeeCommand(com.player)) {
                command.sendHelpMessage(com);
            }
        }
        com.sendHelpMessage("/help or /? - this message");
    }

    @SuppressWarnings("unused")
    public static void sendGmHelp(Communicator com) {
        com.sendHelpMessage("Current available commands:");
        if(commandsMap == null) createCommandsMap();
        for(final Command command : commandsMap.values()) {
            if(command.getCommand().charAt(0) == '#' && command.canSeeCommand(com.player)) {
                command.sendHelpMessage(com);
            }
        }
        com.sendHelpMessage("#help - this message");
    }

    private static final Command[] commands = {
            new Command("/addfriend", "<friendsname> <category>", "add someone to your friends list remotely."),
            new Command("/afk", "[<message>]", "toggles Away-From-Keyboard mode, with optional message."),
            new Command("/alliance", "<message>", "alliance chat."),
            new Command("/almanac", "shows the harvestables using the reports in almanac(s) in your inventory."),
            new Command("/attackers", "shows who you have been fighting the last five minutes."),
            new Command("/ca", "toggles messages to the community assistant window"),
            new Command("/caringfor", "shows list of creatures you are caring for."),
            new Command("/challenge", "sends you a popup where you can answer challenges to your sovereignty.", HELP_IS_KING),
            new Command("/champs", "shows the Champion Eternal Records."),
            new Command("/clear", "clears the current tab."),
            new Command("/clear", "<tabName>", "clears the specified tab on event side (e.g. /clear combat)."),
            new Command("/converts", "shows the number of times you can change kingdom"),
            new Command("/fatigue", "displays how much time you have left to perform fatiguing tasks."),
            new Command("/fl", "shows your current combat focus level"),
            new Command("/fsleep", "freezes or thaws the consumption of sleep bonus. May be toggled every 5 minutes. This toggle is reset every server restart or you change server."),
            new Command("/ignore", "<player>", "makes you unable to hear that player. It also adds to mute vote if used by many people at the same time."),
            new Command("/ignore", "shows ignore list"),
            new Command("/invitations", "allows you to receive an invitation from another player to join their kingdom or religion."),
            new Command("/kingdoms", "displays kingdom influence on this server."),
            new Command("/lives", "shows the number of respawns you have left", HELP_IS_CHAMPION),
            new Command("/lotime", "shows how long until you leave the game if you lose link"),
            new Command("/me", "<emote>", "replaces '/me ' with your name and sends the rest to players in the vicinity."),
            new Command("/mission", "displays the last instructions received"),
            new Command("/openchat", "<channel>", "channel is one of [k]ingdom, [g]lobal kingdom or [t]rade."),
            new Command("/password", "<oldpassword> <newpassword>", "changes your password."),
            new Command("/playtime", "shows information about the time you have played."),
            new Command("/poll", "In Game poll."),
            new Command("/reputation", "shows your current reputation. Reputation is affected by attacking other players and stealing."),
            new Command("/random", "<number>", "broadcasts a random number up to max <number> a few tiles."),
            new Command("/rank", "shows your current battle rank."),
            new Command("/ranks", "shows top battle ranks."),
            new Command("/refer", "A premium account player may give away free silver coins or playing time once."),
            new Command("/release", "corpse", "normally people from your kingdom may not loot your corpse. If you issue this command they may."),
            new Command("/remove", "<person>", "removes person from your friends list"),
            new Command("/respawn", "sends a dialogue offering you to respawn when you are dead."),
            new Command("/revoke", "<villagename>", "removes you as a citizen from the village."),
            new Command("/shout", "<message>", "kingdom chat"),
            new Command("/signin", "[<message>]", "signs you in.", HELP_CAN_SIGN_IN),
            new Command("/signout", "[<message>]", "signs you out.", HELP_CAN_SIGN_IN),
            new Command("/sleep", "shows how long you have left of sleep bonus."),
            new Command("/snipe", "<person>", "(premium only) will mute the player if enough people issue this command at roughly the same time. You have only one snipe per time period."),
            new Command("/stopcaring", "frees all the animal husbandry slots for caring."),
            new Command("/stuck", "helps you getting out from trees your are stuck in."),
            new Command("/suicide", "kills you. You will lose some skill!"),
            new Command("/support", "<message>", "opens up a support ticket window so you can add extra details before sending."),
            new Command("/team", "<message>", "team chat"),
            new Command("/tell", "<person> <message>", "tells someone something ingame."),
            new Command("/tinvite", "<person>", "invites a player to your team."),
            new Command("/title", "displays the title you are currently using."),
            new Command("/titles", "gives the option to select an active title among your available titles."),
            new Command("/toggleccfp", "toggle visibility of the ccfp bar."),
            new Command("/tutorial", "start the in-game tutorial."),
            new Command("/tweet", "sends your tweet to the village twitter if enabled."),
            new Command("/uptime", "shows the time since the last reboot"),
            new Command("/village", "<message>", "village chat"),
            new Command("/vinvite", "<name>", "Sends a village invite to the named player.", new String[]{
                    "OR <name> [time] - Invite a player from an enemy kingdom for time in minutes, default 60 - guards won't attack."
            }, 0),
            new Command("/villageinvite", "See: /vinvite"),
            new Command("/vteleport", "Allows you to use your one free village teleport."),
            new Command("/vote", "<citizen>", "vote for a citizen to become mayor"),
            new Command("/warnings", "shows information about your official moderation warnings."),
            new Command("/weather", "Gives information about wind direction and speed"),
            new Command("/who", "shows logged on people"),
            new Command("/recruit", "<playername>", "Adds a player to your village recruit list."),
            new Command("/unrecruit", "<playername>", "Removes a player from your village recruit list."),
            new Command("/listrecruits", "Show your village recruitment list."),
            new Command("/join", "player <playername>", "Attempts to join the village of the player, must be on the village recruitment list."),
            new Command("/join", "village <villagename>", "Attempts to join the village, must be on the village recruitment list."),
            new Command("/mykingdoms", "Displays the kingdoms you are currently affiliated with on Chaos and Epic"),

            new Command("#chat", "<int color>", "colors your chat so that players understand that it is formal. The color is optional and you'll get orange otherwise, also second parmeter can be r.g.b values.", HELP_POWER_1 | HELP_MAY_MUTE),
            new Command("#invis", "toggles invisibility", HELP_POWER_1),
            new Command("#mute", "<playername> <hours> <reason>", "the player cannot communicate except with tell.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#unmute", "<playername>", "pardons a mute.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#mutewarn", "<playername> (reason)", "sends a warning that a player may be muted. The reason is optional.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#showmuters", "displays a list of the people who can mute apart from the gms.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#showmuted", "displays a list of the people who are muted.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#showcas", "displays a list of the ca.", HELP_POWER_2 | HELP_MAY_MUTE),
            new Command("#showdevtalkers", "displays a list of the people who can see the GM Tab.", HELP_POWER_2 | HELP_MAY_HEAR_DEV_TALK),
            new Command("#alerts", "lets you change periodic messages from the server.", HELP_POWER_2),
            new Command("#announce", "announces a blue system wide message.", HELP_POWER_2),
            new Command("#ban", "<playername> <days> <reason>", "bans the player and the ipaddress. You must provide the number of days and a reason.", HELP_POWER_2),
            new Command("#banhere|#baniphere|#pardonhere|pardoniphere", "<playername>", "bancontrol for the current server only", HELP_POWER_2),
            new Command("#banip", "<ipaddress> <days> <reason>", "bans the ipaddress and kicks anyone from it. You must provide the number of days and a reason.", HELP_POWER_2),
            new Command("#bansteamid", "<steamid/player> <days> <reason>", "bans the steamId", HELP_POWER_2),
            new Command("#kips", "displays kingdom IP addresses and time since last logout.", HELP_POWER_2),
            new Command("#broadcast", "broadcasts a system wide message.", HELP_POWER_2),
            new Command("#calcCreatures", "Calculates number of creatures on surface, in caves, are visible, and offline, Use with care - lag prone.", HELP_POWER_2),
            new Command("#changeemail", "<playername> <newemail>", "changes the email of a single player character.", HELP_POWER_2),
            new Command("#changemodel", "<model>", "change character model (gmdark or gmnormal)", HELP_POWER_2),
            new Command("#changepassword", "<playername> <newpassword>", "changes the password of a player.", HELP_POWER_2),
            new Command("#checkCreatures", "error checks the positions of creatures. Will return dislocated guards for instance. May provide a name like 'templar' to check only those. Use with care - lag prone and may cause instant spawns.", HELP_POWER_2),
            new Command("#checkclients", "[<name> [true]]", "sends a message to all clients that they should relaunch if they run an old client version.", new String[]{ null,
                                                                                                                                                                         "if <name> is specified then just send a message to the specified player to get a list of loaded classes.",
                                                                                                                                                                         "if [true] is specified then the list of loaded classes is NOT filted upon its return."
            }, HELP_POWER_2),
            new Command("#findboat", "<name>", "lets you find a boat with part of the name in it. May be processor heavy so if you notice lag, use with care!", HELP_POWER_2),
            new Command("#getAchievementData", "<playername> <achievement id>", null, HELP_POWER_2),
            new Command("#getip", "<playername>", "displays the players ip address and any other accounts from the same address.", HELP_POWER_2),
            new Command("#getips", "displays the current players with ip addresses.", HELP_POWER_2),
            new Command("#getwarnings", "<playername>", "displays info about the player's warnings.", HELP_POWER_2),
            new Command("#gm", "send a GM message to login server.", HELP_POWER_2),
            new Command("#gmlight", "togles personal light on/off when you are invisible.", HELP_POWER_2),
            new Command("#hideme", "[GM]", "hides (GM) name in MGMT and GM Tab list, if GM is specified then just the GM tab is done.", HELP_POWER_2),
            new Command("#kick", "<playername>", "kicks the player", HELP_POWER_2),
            new Command("#loadItem", "<long id>", "loads item with id, (removing from the owner).", HELP_POWER_2),
            new Command("#locateavatars", "locates all avatars on the server and tells you where they are. It could cause lag, so use sparingly.", HELP_POWER_2),
            new Command("#locatehorse", "<string>", "return the location of horses whose name contains the supplied argument string.", HELP_POWER_2),
            new Command("#offline", "shows offline creatures with location.", HELP_POWER_2),
            new Command("#onfire", "toggles player fire.", HELP_POWER_2),
            new Command("#online", "<playername>", "returns the current status and server of the given player", HELP_POWER_2),
            new Command("#pardon", "<playername>", "pardons the player and the ipaddress", HELP_POWER_2),
            new Command("#pardonip", "<ipaddress>", "pardons the ipaddress", HELP_POWER_2),
            new Command("#pardonsteamid", "<steamid>", "pardons the steamid", HELP_POWER_2),
            new Command("#plimit", "<new number>", "the number when the server no longer accepts free players. It will always let premiums in though.", HELP_POWER_2),
            new Command("#reload", "<creatureId or playername>", "reload a player or creature when bugged.", HELP_POWER_2),
            new Command("#rename", "<oldname> <newname> <password>", "renames the player. The player must be LOGGED OFF.", HELP_POWER_2),
            new Command("#resetwarnings", "<playername>", "resets the players warnings to 0.", HELP_POWER_2),
            new Command("#respawn", "<playername>", "respawns a dead player at the start.", HELP_POWER_2),
            new Command("#setmuter", "<name>", "gives or removes the ability to a normal player to mute other players.", HELP_POWER_2),
            new Command("#setreputation", "<playername> <new reputation>", "sets the reputation of a player.", HELP_POWER_2),
            new Command("#setserver", "<playername> <serverid>", "tells this server that the player is on the server with the number specified.", HELP_POWER_2),
            new Command("#showbans", "displays current bans", HELP_POWER_2),
            new Command("#showheros", "[power]", "displays a list of the people with power (defaults to Hero).", HELP_POWER_2),
            new Command("#showme", "shows (GM) name in MGMT Tab list.", HELP_POWER_IS_2),
            new Command("#soundspam", "spams area around you with random sounds for testing.", HELP_POWER_2),
            new Command("#timemod", "<hours>", "modifies your current time with the number of hours. Can be negative.", HELP_POWER_2),
            new Command("#toggleEpic", "toggles epic portals", HELP_POWER_2),
            new Command("#toggleglobal", "toggles global chat.", HELP_POWER_2),
            new Command("#warn", "<playername>", "the player receives an official warning.", HELP_POWER_2),
            new Command("#watch", "<playername> <description>", "creates a 'watch' ticket.", HELP_POWER_2),
            new Command("#who", "[J|H|M]", "sends a list of players online from Jenn-kellon, HOTS, or Mol-Rehan respectively", HELP_POWER_2),
            new Command("#worth", "<name>", "helps debug royal level kills on pvp servers.", HELP_POWER_2),
            new Command("#addmoney", "<name months days silvers detail>", "adds prem or silver to a players account. Detail needs to be any unique string like paypal id or 'reimburseXox22332'. Example: #addmoney rolf 0 2 2 add2days2silverrolf", HELP_POWER_3),
            new Command("#addtitle", "<name> [<title id>]", "adds the default title Clairvoyant to bug reporters. title id is optional.", HELP_POWER_3),
            new Command("#allowall", "opens the server for new connections. (leaves maintenance mode).", HELP_POWER_3),
            new Command("#artist", "<name> <sound> <graphics>", "toggle artist privileges. example:  #artist mrb false true.", HELP_POWER_3),
            new Command("#creaturepos", "toggles creature position logging.", HELP_POWER_3),
            new Command("#devtalk", "<name>", "toggles the ability to a normal player to hear the gm chat.", HELP_POWER_3),
            new Command("#dumpxml", "generates a new epic xml on the login server.", HELP_POWER_3),
            new Command("#invuln", "toggles invulnerability mode.", HELP_POWER_3),
            new Command("#itempos", "<id>", "checks the position of an item.", HELP_POWER_3),
            new Command("#maxcreatures", "<newvalue>", "sets the number of max creature to a new value.", HELP_POWER_3),
            new Command("#newmission", "<deityname>", "generates a new epic mission for the provided deity.", HELP_POWER_3),
            new Command("#overrideshop", "<name> <true|false>", "if set to true the player may use the shop even though he has had previous payment reversals.", HELP_POWER_3),
            new Command("#readlog", "Reads and displays the player's log from the server filesystem. TODO. ", HELP_POWER_3),
            new Command("#redeem", "functionality to retrieve items from banned players.", HELP_POWER_3),
            new Command("#registermail", "registers player email in list.", HELP_POWER_3),
            new Command("#removetitle", "<name> [<title id>]", "removes the default title Community Assistant. title id is optional.", HELP_POWER_3),
            new Command("#resetplayer", "<name>", "resets the players skills and faith to max 20. Also removes champion/realdeath.", HELP_POWER_3),
            new Command("#sdown", "displays a message that the server is shutting down and rejects new connections. Does not shut down (enters maintenance mode).", HELP_POWER_3),
            new Command("#startx", "<number>", "sets the tile X where new players (Jenn-Kellon, or for home servers all players) start to the number given.", HELP_POWER_3),
            new Command("#starty", "<number>", "sets the tile Y where new players (Jenn-Kellon, or for home servers all players) start to the number given.", HELP_POWER_3),
            new Command("#togglemission", "<missionname>", "enables or disables the mission with the name supplied.", HELP_POWER_3),
            new Command("#togglemounts", "enables or disables riding, driving and horse spawning.", HELP_POWER_3),
            new Command("#toggleqa", "<name>", "toggles the QA status on or off for the account.", HELP_POWER_3),
            new Command("#toggleflag", "<name> <flagid>", "toggles the flag on or off for the account.", HELP_POWER_3),
            new Command("#isqa", "<name>", "Checks if account has QA status.", HELP_POWER_3),
            new Command("#getStoredCreatures", "List the WurmID and names of stored creatures on this server.", HELP_POWER_3),
            new Command("#addfakemo", "value <owner> <parent>", "adds fake money to your inventory with the optionally provided owner and parent item.", HELP_POWER_3 | HELP_TEST_SERVER),
            new Command("#addmoney", "name months days silvers detail", "adds to a players account. Detail is the paypal transaction id.", HELP_POWER_3 | HELP_NOT_TEST_SERVER),
            new Command("#flattenRock", "<N>, <E>, <S>, <W>, [Extra Distance Below]", null, new String[]{
                    "Custom sized flatten zone. Flattens to rock instead of dirt."
            }, HELP_POWER_4),
            new Command("#flattenDirt", "<N>, <E>, <S>, <W>, [Extra Distance Below], [Min Dirt Distance to Rock]", null, new String[]{
                    "Pretty self-explanatory."
            }, HELP_POWER_4),
            new Command("#setpower", "<player> <power>", "sets the power of a player. 0=Normal, 1=Hero, 2=Demigod, 3=High-god, 4=Arch, 5=Dev.", HELP_POWER_4),
            new Command("#listdens", "shows a list of all unique dens.", HELP_POWER_4),
            new Command("#removeden", "<templateID>", "removes a unique den with a certain template ID", HELP_POWER_4),
            new Command("#reloadItems", "<wurmId>", "checks for all subitems of the given itemId and forces them back into that item.", HELP_POWER_4),
            new Command("#showPersonalGoals", "<playerName>", "Shows the current (and old) personal goals of the player", HELP_POWER_4),
            new Command("#addreimb", "name months days silvers setbok", "adds to a players reimbursement pool.", HELP_POWER_5),
            new Command("#addregalia", "name", "adds kingdom regalia.", HELP_POWER_5),
            new Command("#checkItems", "error checks the positions of items. Use with care - lag prone.", HELP_POWER_5),
            new Command("#checkZones", "Error checks the surface and cave zones. TODO. ", HELP_POWER_5),
            new Command("#createportals", "create Freedom Portals. TODO. ", HELP_POWER_5),
            new Command("#enableboats", "create boat entriess. TODO. ", HELP_POWER_5),
            new Command("#findfish", "lists the close special fish spots.", HELP_POWER_5),
            new Command("#findoldfish", "lists the old close rare fish spots.", HELP_POWER_5 | HELP_TEST_SERVER),
            new Command("#lagstatus", "gives the count in real seconds from startup versus the number of seconds ticked.", HELP_POWER_5),
            new Command("#locate", "<string>", "return the location of creature with name containing the supplied argument string.", HELP_POWER_5),
            new Command("#locateitem", "<templateid>", "return the location of the end game item with that id.", HELP_POWER_5),
            new Command("#noxmas", "remove Christmas light effect from player. ", HELP_POWER_5),
            new Command("#playerstatuses", "shows how long until a player should leave the world, or if they should be logged off already.", HELP_POWER_5),
            new Command("#removeknownrecipes", "[player] [recipeId]", null, new String[]{
                    "removes all known recieps from specified player (if just player specified)",
                    "OR the specified recipe from all players (if just recipe specified)",
                    "OR the specified recipe from the specified player (if both specified)."
            }, HELP_POWER_5),
            new Command("#removeNamedRecipe", "[player or recipeId] [\"remove\"]", null, new String[]{
                    "show recipe/player from named list",
                    "OR remove entry from named list (if 2nd param is \"remove\")."
            }, HELP_POWER_5),
            new Command("#uniques", "shows all unique creatures. Caution prone to lag. TODO", HELP_POWER_5),
            new Command("#vespeed", "<speed>", "sets vehicle speed (0-255).", HELP_POWER_5),
            new Command("#date", "<date>", "show the date in real date.", HELP_POWER_5),
            new Command("#wurmdate", "<wurmdate>", "show the date in wurm calendar format.", HELP_POWER_5),
            new Command("#xmaslight", "create Christmas light effect at current location.", HELP_POWER_5),
            new Command("#dumpcavewater", "creates a png image of the cave water types (waterCaveTypes.png)", HELP_POWER_5),
            new Command("#dumpcreatures", "creates a png image of the map with creatures on it.", HELP_POWER_5),
            new Command("#dumpmarkers", "creates a png image of the map for markers.", HELP_POWER_5),
            new Command("#dumproutes", "creates a png image of the map for routes.", HELP_POWER_5),
            new Command("#dumpsurfacewater", "creates a png image of the surface water types (waterSurfaceTypes.png)", HELP_POWER_5),
            new Command("#give", "<id> [ql] [amount]", "gives you an item with the specified parameters. Use id 176 for ebony wand.", HELP_POWER_5),
            new Command("#generateDeadVillage", "[number]", "generates a number of dead villages for archaeology purposes. A number more than 5 will not return info about the villages, just if it was successful or not.", HELP_POWER_5),
            new Command("#toggleca", "<name>", "toggles the community assistant flag for the named player.", HELP_MAY_APPOINT_CA),

            new Command(FIELD_GROWTH),
            new Command(SKILL_GAIN_RATE),
            new Command(CHARACTERISTICS_START),
            new Command(MIND_LOGIC_START),
            new Command(BC_START),
            new Command(FIGHT_START),
            new Command(OVERALL_START),
            new Command(PLAYER_CR),
            new Command(ACTION_SPEED),
            new Command(HOTA),
            new Command(MAX_CREATURES),
            new Command(AGG_PERCENT),
            new Command(UPKEEP),
            new Command(FREE_DEEDS),
            new Command(TRADER_MAX_MONEY),
            new Command(TRADER_INITIAL_MONEY),
            new Command(MINING_HITS),
            new Command(BREEDING_TIME),
            new Command(TREE_GROWTH),
            new Command(MONEY_POOL),

            new Command("/combat", "print combat stats such as number of kills etc.", HELP_CUSTOM, CommandHandler::handleCommandCombat),
            new Command("/character", "[player]", "open character window", HELP_CUSTOM, CommandHandler::handleCommandCharacter),
            new Command("/time", "shows current game time.", HELP_CUSTOM, Seasons::handleCommandTime),
            new Command("/lag", "show lag in seconds since server start", HELP_CUSTOM, CommandHandler::handleCommandLag),
            new Command("/bank", "show balance in bank account", HELP_CUSTOM, CommandHandler::handleCommandBank),
            new Command("/market", "[ware]", "list which merchants or vendors will sell the ware, including category and kingdom", HELP_CUSTOM, CommandHandler::handleCommandMarket),
            new Command("/vendors", "[kingdom]", "open vendors window", HELP_CUSTOM, CommandHandler::handleCommandVendors),
            new Command("/pvp", "list active PvP players, i.e. players with PvP checked in settings", HELP_CUSTOM, CommandHandler::handleCommandPvPList),
            new Command("/bug", "<message>", "send a bug report to the #bugs channel on Discord", HELP_CUSTOM, CommandHandler::handleCommandBugMessage),
            new Command("/suggestion", "<message>", "send a suggestion message to the #suggestions channel on Discord", HELP_CUSTOM, CommandHandler::handleCommandSuggestionMessage),

            new Command("#config", "reload", "reload the awakening.properties file", new String[]{
                    "OR data <data> - print values for data1 and data2",
                    "OR bml - open a BML editor"
            }, HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandConfig),
            new Command("#restore", "rock|biomes|cave load <path>", "load the mesh data for the original map", new String[]{
                    "OR rock|biomes|cave|mycelium <startx> <starty> <endx> <endy> - restore map for the given area using loaded mesh data"
            }, HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandRestore),
            new Command("#alts", "<player>", "list player alts, or possible alts.", HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandAlts),
            new Command("#guilt", "<player> remove", "remove heavy conscience from a player", new String[]{
                    "OR <name> give <time> <reason> - give heavy conscience to a player; time=minutes"
            }, HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandGuilt),
            new Command("#monitor", "<player> <time>", "monitor player's actions, writes actions with timestamps to log-file; time=minutes", HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandMonitor),
            new Command("#pvp", "<player> <remove>", "remove PvP setting from a player", HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandPvP),
            new Command("#karma", "<player> <amount>", "give an amount of karma to player (use negative number to take instead)", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandKarma),
            new Command("#rotate", "<wurmId> <degrees>", "set rotation of a creature", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandRotate),
            new Command("#inventory", "<wurmId> clear", "destroy all items in the creature's inventory", new String[]{
                    "OR <wurmId> give <itemId> <qualityLevel> - create item and place in creature's inventory"
            }, HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandInventory),
            new Command("#fine", "<player> <amount> <reason>", "make a player pay a fine in iron as a penalty", HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandFine),
            new Command("#transfer", "<player> <dbFile> <dbPlayer>", "transfer a player's money, karma and skills from an external db", HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandTransfer),
            new Command("#sql", "[or #sql-<dbfile>] <sql>", "execute a SQL query (dbfile should be filename excluding .db)", HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandSQL),
            new Command("#schedule", "list", "print a list of all active tasks", new String[]{
                    "OR stop [command or id] - stop all tasks, or by command or id",
                    "OR broadcast <id> <start/delay> <color> <message> - broadcast message, start=minute of the hour, delay=minutes"
            }, HELP_POWER_2 | HELP_MAY_MUTE | HELP_CUSTOM, CommandHandler::handleCommandSchedule),
            new Command("#tile", "checkFarmGrowth", "check for farm growth", new String[]{
                    "OR checkTreeSprout - check for tree sprouts",
                    "OR treasure - bury random treasure at the tile of the player"
            }, HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandTile),
            new Command("#setresource", "<resource> <startx> <starty> <endx> <endy> <minimum> [maximum]", "set resource amount on tiles", HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandSetResource),
            new Command("#flowersfix", "fix bugged flower tiles", HELP_POWER_5 | HELP_CUSTOM, CommandHandler::handleCommandFlowersFix),
            new Command("#servant", "<wurmId> <command>>", "enter #servant for full list of commands", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandServant),
            new Command("#shades", "<command(s)>", "enter #shades for full list of commands", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandShades),
            new Command("#getFood", "<templateId> <recipeId> <stages> <ingredients>", "create food, e.g. #getFood 1177 1270 10 10", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandGetFood),
            new Command("#infestation", "create <power> <type>", "create an infestation on the tile you're standing", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandInfestation),
            new Command("#appointments", "list appointment applications", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandAppointments),
            new Command("#settings", "<name>", "open player settings dialog", HELP_POWER_2 | HELP_CUSTOM, CommandHandler::handleCommandSettings),
            };
}
