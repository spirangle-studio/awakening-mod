package net.spirangle.awakening;

import com.wurmonline.server.Server;
import com.wurmonline.server.support.JSONException;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.support.JSONTokener;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RemoteHandler {

    private static final Logger logger = Logger.getLogger(RemoteHandler.class.getName());

    private static RemoteHandler instance = null;

    public static RemoteHandler getInstance() {
        if(instance == null) instance = new RemoteHandler();
        return instance;
    }

    private ServerSocket remoteSocket;

    private RemoteHandler() {
    }

    public void start() {
        new Thread(() -> {
            logger.info("Listening to port: " + Config.remotePort);
            try {
                remoteSocket = new ServerSocket(Config.remotePort);
                while(true) {
                    Socket socket = remoteSocket.accept();
                    InetAddress adress = socket.getInetAddress();
                    BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
                    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                    StringBuilder sb = new StringBuilder();
                    byte[] buffer = new byte[1024];
                    int read;
                    while((read = bis.read(buffer)) != -1) {
                        sb.append(new String(buffer, 0, read, StandardCharsets.UTF_8));
                        if(read < buffer.length) break;
                    }
                    String json = sb.toString();
                    logger.info("Received socket input [" + adress.getHostAddress() + "]: " + json);
                    String response;
                    try {
                        JSONTokener jt = new JSONTokener(json);
                        JSONObject jo = new JSONObject(jt);
                        response = handleCommand(jo);
                    } catch(JSONException e) {
                        logger.log(Level.WARNING, "Failed to parse json: " + json, e);
                        response = "error: json";
                    }
                    dos.writeBytes(response);
                    dos.flush();
                    dos.close();
                }
            } catch(IOException e) {
                logger.log(Level.WARNING, "Fail: " + e, e);
            }
        }).start();
    }

    private String handleCommand(JSONObject jo) {
        String command = jo.optString("command");
        if("ping".equals(command)) {
            return "OK";
        } else if("shutdown".equals(command)) {
            int time = Integer.parseInt(jo.optString("time", "600"));
            String reason = jo.optString("reason", "Server maintainance");
            Server.getInstance().startShutdown(time, reason);
            return "OK";
        }
        return "error: unknown";
    }
}
