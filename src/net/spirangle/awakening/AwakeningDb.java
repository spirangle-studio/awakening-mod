package net.spirangle.awakening;

import com.wurmonline.server.Constants;
import net.spirangle.awakening.creatures.PumpkinKing;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AwakeningDb {

    private static final Logger logger = Logger.getLogger(AwakeningDb.class.getName());

    private static final String SQLITE_DB_DRIVER = "org.sqlite.JDBC";

    public static final String CREATE_SCHEDULE = "CREATE TABLE SCHEDULE (" +
                                                 " ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                                 " TYPE INTEGER NOT NULL DEFAULT 0," +
                                                 " NAME TEXT NOT NULL DEFAULT ''," +
                                                 " START INTEGER NOT NULL DEFAULT 0," +
                                                 " DELAY INTEGER NOT NULL DEFAULT 1," +
                                                 " MESSAGE TEXT NOT NULL DEFAULT ''," +
                                                 " COLOR INTEGER NOT NULL DEFAULT 0," +
                                                 " MIN INTEGER NOT NULL DEFAULT 0," +
                                                 " MAX INTEGER NOT NULL DEFAULT 0," +
                                                 " CREATED INTEGER NOT NULL DEFAULT 0" +
                                                 ")";

    public static final String CREATE_SKILLS = "CREATE TABLE SKILLS (" +
                                               " ID INTEGER NOT NULL PRIMARY KEY," +
                                               " OWNER INTEGER NOT NULL DEFAULT 0," +
                                               " NUMBER INTEGER NOT NULL DEFAULT 0," +
                                               " VALUE REAL NOT NULL DEFAULT 0," +
                                               " MINVALUE REAL NOT NULL DEFAULT 0," +
                                               " LASTUSED INTEGER NOT NULL DEFAULT 0," +
                                               " DAY1 REAL NOT NULL DEFAULT 0," +
                                               " DAY2 REAL NOT NULL DEFAULT 0," +
                                               " DAY3 REAL NOT NULL DEFAULT 0," +
                                               " DAY4 REAL NOT NULL DEFAULT 0," +
                                               " DAY5 REAL NOT NULL DEFAULT 0," +
                                               " DAY6 REAL NOT NULL DEFAULT 0," +
                                               " DAY7 REAL NOT NULL DEFAULT 0," +
                                               " WEEK2 REAL NOT NULL DEFAULT 0" +
                                               ")";
    public static final String[] CREATE_SKILLS_INDEX = {
            "CREATE INDEX SKILLS_ownerId ON SKILLS(OWNER)"
    };

    public static final String CREATE_PLAYERSDATA = "CREATE TABLE PLAYERSDATA (" +
                                                    " WURMID INTEGER NOT NULL PRIMARY KEY," +
                                                    " NAME TEXT NOT NULL DEFAULT ''," +
                                                    " KINGDOM INTEGER NOT NULL DEFAULT 0," +
                                                    " FLAGS INTEGER NOT NULL DEFAULT 0," +
                                                    " DATA TEXT NOT NULL DEFAULT ''," +
                                                    " CREATED INTEGER NOT NULL DEFAULT 0," +
                                                    " CHANGED INTEGER NOT NULL DEFAULT 0" +
                                                    ")";

    public static final String CREATE_CREATURES = "CREATE TABLE CREATURES (" +
                                                  " WURMID INTEGER NOT NULL PRIMARY KEY," +
                                                  " SPAWNID INTEGER NOT NULL DEFAULT 0," +
                                                  " POSX REAL NOT NULL DEFAULT 0," +
                                                  " POSY REAL NOT NULL DEFAULT 0," +
                                                  " ROTATION REAL NOT NULL DEFAULT 0," +
                                                  " CREATED INTEGER NOT NULL DEFAULT 0" +
                                                  ")";
    public static final String[] CREATE_CREATURES_INDEX = {
            "CREATE INDEX CREATURES_spawnId ON CREATURES(SPAWNID)"
    };

    public static final String CREATE_GIFTS = "CREATE TABLE GIFTS (" +
                                              " ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                              " WURMID INTEGER NOT NULL," +                     // WurmId of player getting the gift
                                              " STEAMID INTEGER NOT NULL DEFAULT 0," +          // SteamId of player getting the gift
                                              " GIFTID INTEGER NOT NULL DEFAULT 0," +           // Gift concept id (halloween, christmas etc.)
                                              " SOURCEID INTEGER NOT NULL DEFAULT 0," +         // WurmId of creature giving the gift
                                              " ITEMTEMPLATEID INTEGER NOT NULL DEFAULT 0," +   // ItemTemplateId of given item
                                              " ITEMID INTEGER NOT NULL DEFAULT 0," +           // WurmId of given item
                                              " CREATED INTEGER NOT NULL DEFAULT 0" +
                                              ")";
    public static final String[] CREATE_GIFTS_INDEX = {
            "CREATE INDEX GIFTS_wurmId ON GIFTS(WURMID,GIFTID)",
            "CREATE INDEX GIFTS_steamId ON GIFTS(STEAMID,GIFTID)"
    };

    public static final String CREATE_SERVANTS = "CREATE TABLE SERVANTS (" +
                                                 " CONTRACTID INTEGER NOT NULL PRIMARY KEY," +
                                                 " NPCID INTEGER NOT NULL," +
                                                 " STATUS INTEGER NOT NULL DEFAULT 0," +
                                                 " FACE INTEGER NOT NULL DEFAULT 0," +
                                                 " POSX REAL NOT NULL DEFAULT 0.0," +
                                                 " POSY REAL NOT NULL DEFAULT 0.0," +
                                                 " POSZ REAL NOT NULL DEFAULT 0.0," +
                                                 " ROTATION REAL NOT NULL DEFAULT 0.0," +
                                                 " MONEY INTEGER NOT NULL DEFAULT 0" +
                                                 ")";

    public static final String CREATE_TASKS = "CREATE TABLE TASKS (" +
                                              " TASKID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                              " NPCID INTEGER NOT NULL," +
                                              " TYPE INTEGER NOT NULL DEFAULT 0," +
                                              " QUALITYLEVEL REAL NOT NULL DEFAULT 0.0," +
                                              " DATA TEXT NOT NULL DEFAULT ''" +
                                              ")";
    public static final String[] CREATE_TASKS_INDEX = {
            "CREATE INDEX TASKS_npcId ON TASKS(NPCID)"
    };

    public static final String CREATE_MARKET = "CREATE TABLE MARKET (" +
                                               " ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                               " TEMPLATEID INTEGER NOT NULL DEFAULT 0," +
                                               " CATEGORY INTEGER NOT NULL DEFAULT 0," +
                                               " SELLER INTEGER NOT NULL DEFAULT 0," +
                                               " MERCHANT INTEGER NOT NULL DEFAULT 0," +
                                               " SELLDATE INTEGER NOT NULL DEFAULT 0," +
                                               " QUALITYLEVEL REAL NOT NULL DEFAULT 0.0," +
                                               " AMOUNT REAL NOT NULL DEFAULT 0.0," +
                                               " WEIGHT INTEGER NOT NULL DEFAULT 0," +
                                               " POSX REAL NOT NULL DEFAULT 0.0," +
                                               " POSY REAL NOT NULL DEFAULT 0.0," +
                                               " MATERIAL INTEGER NOT NULL DEFAULT 0," +
                                               " AUX INTEGER NOT NULL DEFAULT 0," +
                                               " PRICE INTEGER NOT NULL DEFAULT 0" +
                                               ")";
    public static final String[] CREATE_MARKET_INDEX = {
            "CREATE INDEX MARKET_templId ON MARKET(TEMPLATEID)",
            "CREATE INDEX MARKET_category ON MARKET(CATEGORY)"
    };

    public static final String CREATE_TRANSACTIONS = "CREATE TABLE TRANSACTIONS (" +
                                                     " ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                                     " WURMID INTEGER NOT NULL DEFAULT 0," +
                                                     " TEMPLATEID INTEGER NOT NULL DEFAULT 0," +
                                                     " TYPE INTEGER NOT NULL DEFAULT 0," +
                                                     " CATEGORY INTEGER NOT NULL DEFAULT 0," +
                                                     " RESPONDER INTEGER NOT NULL DEFAULT 0," +
                                                     " DATE INTEGER NOT NULL DEFAULT 0," +
                                                     " QUALITYLEVEL REAL NOT NULL DEFAULT 0.0," +
                                                     " AMOUNT REAL NOT NULL DEFAULT 0.0," +
                                                     " WEIGHT INTEGER NOT NULL DEFAULT 0," +
                                                     " POSX REAL NOT NULL DEFAULT 0.0," +
                                                     " POSY REAL NOT NULL DEFAULT 0.0," +
                                                     " MATERIAL INTEGER NOT NULL DEFAULT 0," +
                                                     " AUX INTEGER NOT NULL DEFAULT 0," +
                                                     " PRICE INTEGER NOT NULL DEFAULT 0," +
                                                     " TAX INTEGER NOT NULL DEFAULT 0" +
                                                     ")";
    public static final String[] CREATE_TRANSACTIONS_INDEX = {
            "CREATE INDEX TRANSACTIONS_wurmId ON TRANSACTIONS(WURMID)",
            "CREATE INDEX TRANSACTIONS_templId ON TRANSACTIONS(TEMPLATEID)",
            "CREATE INDEX TRANSACTIONS_type ON TRANSACTIONS(TYPE)"
    };

    public static final String CREATE_QUESTSPERFORMED = "CREATE TABLE QUESTSPERFORMED (" +
                                                        " ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                                        " PERFORMER INTEGER NOT NULL DEFAULT 0," +        // WurmId of player performing the quest
                                                        " STEAMID INTEGER NOT NULL DEFAULT 0," +          // SteamId of player performing the quest
                                                        " QUEST INTEGER NOT NULL DEFAULT 0," +            // Id of the quest being performed
                                                        " STATE REAL NOT NULL DEFAULT 0.0," +             // State or progress of the quest
                                                        " DATA INTEGER NOT NULL DEFAULT 0," +             // Misc data
                                                        " STARTTIME INTEGER NOT NULL DEFAULT 0," +        // Time when the quest was started
                                                        " ENDTIME INTEGER NOT NULL DEFAULT 0" +           // Time when the quest was ended
                                                        ")";
    public static final String[] CREATE_QUESTSPERFORMED_INDEX = {
            "CREATE INDEX QUESTSPERFORMED_performer ON QUESTSPERFORMED(PERFORMER)"
    };

    public static final String CREATE_SCOUTPARTIES = "CREATE TABLE SCOUTPARTIES (" +
                                                     " ID INTEGER PRIMARY KEY AUTOINCREMENT," +        // Id of scout party
                                                     " NUMBER INTEGER NOT NULL DEFAULT 0," +           // Number of scouts in the party
                                                     " TILEX INTEGER NOT NULL DEFAULT 0," +            // X coordinate of party
                                                     " TILEY INTEGER NOT NULL DEFAULT 0," +            // Y coordinate of party
                                                     " CREATED INTEGER NOT NULL DEFAULT 0" +           // Time when the scout party was created
                                                     ")";

    public static final String CREATE_SCOUTS = "CREATE TABLE SCOUTS (" +
                                               " WURMID INTEGER NOT NULL PRIMARY KEY," +         // WurmId of scout creature
                                               " PARTYID INTEGER NOT NULL DEFAULT 0," +          // Id of scout party
                                               " CREATED INTEGER NOT NULL DEFAULT 0" +           // Time when the scout was created
                                               ")";
    public static final String[] CREATE_SCOUTS_INDEX = {
            "CREATE INDEX SCOUTS_party ON SCOUTS(PARTYID)"
    };

    public static final String CREATE_INFESTATIONS = "CREATE TABLE INFESTATIONS (" +
                                                     " WURMID INTEGER NOT NULL PRIMARY KEY," +         // Id of infestation item, i.e. curse crystal or stone
                                                     " TILEX INTEGER NOT NULL DEFAULT 0," +            // X coordinate of center of infestation
                                                     " TILEY INTEGER NOT NULL DEFAULT 0," +            // Y coordinate of center of infestation
                                                     " POWER REAL NOT NULL DEFAULT 0.0," +             // Power of infestation
                                                     " TYPE INTEGER NOT NULL DEFAULT 0," +             // Type of infestation
                                                     " STATE INTEGER NOT NULL DEFAULT 0," +            // State of infestation
                                                     " STARTTIME INTEGER NOT NULL DEFAULT 0," +        // Time when the infestation was started
                                                     " ENDTIME INTEGER NOT NULL DEFAULT 0," +          // Time when the infestation was ended
                                                     " CREATED INTEGER NOT NULL DEFAULT 0" +           // Time when the infestation was created
                                                     ")";

    public static final String CREATE_OFFICE_APPLICATIONS = "CREATE TABLE OFFICE_APPLICATIONS (" +
                                                            " ID INTEGER NOT NULL PRIMARY KEY," +         // Application id
                                                            " WURMID INTEGER NOT NULL DEFAULT 0," +       // WurmId of player
                                                            " STEAMID INTEGER NOT NULL DEFAULT 0," +      // SteamId of player
                                                            " KINGDOM INTEGER NOT NULL DEFAULT 0," +      // The kingdom the appointment is for
                                                            " OFFICE INTEGER NOT NULL DEFAULT 0," +       // The office applied for
                                                            " APPOINTMENT INTEGER NOT NULL DEFAULT 0," +  // The office the player was appointed to
                                                            " CREATED INTEGER NOT NULL DEFAULT 0" +       // Time when the application was sent
                                                            ")";

    public static void init() {
        try(Connection con = ModSupportDb.getModSupportDb();
            Statement st = con.createStatement()) {
            createTable(con, st, "SCHEDULE", CREATE_SCHEDULE, null);
            createTable(con, st, "SKILLS", CREATE_SKILLS, CREATE_SKILLS_INDEX);
            createTable(con, st, "PLAYERSDATA", CREATE_PLAYERSDATA, null);
            createTable(con, st, "CREATURES", CREATE_CREATURES, CREATE_CREATURES_INDEX);
            createTable(con, st, "GIFTS", CREATE_GIFTS, CREATE_GIFTS_INDEX);
            createTable(con, st, "SERVANTS", CREATE_SERVANTS, null);
            createTable(con, st, "TASKS", CREATE_TASKS, CREATE_TASKS_INDEX);
            createTable(con, st, "MARKET", CREATE_MARKET, CREATE_MARKET_INDEX);
            createTable(con, st, "TRANSACTIONS", CREATE_TRANSACTIONS, CREATE_TRANSACTIONS_INDEX);
            createTable(con, st, "QUESTSPERFORMED", CREATE_QUESTSPERFORMED, CREATE_QUESTSPERFORMED_INDEX);
            createTable(con, st, "SCOUTPARTIES", CREATE_SCOUTPARTIES, null);
            createTable(con, st, "SCOUTS", CREATE_SCOUTS, CREATE_SCOUTS_INDEX);
            createTable(con, st, "INFESTATIONS", CREATE_INFESTATIONS, null);
            createTable(con, st, "OFFICE_APPLICATIONS", CREATE_OFFICE_APPLICATIONS, null);

            PumpkinKing.getInstance().load(con);

        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to create database table.", e);
        }
    }

    public static void createTable(Connection con, Statement st, String tableName, String table, String[] indexes) throws SQLException {
        if(!ModSupportDb.hasTable(con, tableName)) {
            st.execute(table);
            logger.info("Created database table '" + tableName + "'.");
            if(indexes != null) {
                for(int i = 0; i < indexes.length; ++i)
                    st.execute(indexes[i]);
                logger.info("Created indexes for '" + tableName + "'.");
            }
        }
    }

    public static Connection getConnection(String db) {
        String dbConnection = getDbConnectionString(Constants.dbHost, db);
        try {
            Class.forName(SQLITE_DB_DRIVER);
            return DriverManager.getConnection(dbConnection);
        } catch(SQLException | ClassNotFoundException e) {
            logger.log(Level.WARNING, "Failed to initialize db connection: " + e.getMessage(), e);
        }
        return null;
    }

    public static String getDbConnectionString(String host, String db) {
        return "jdbc:sqlite:" + host + "/" + db + ".db";
    }
}
