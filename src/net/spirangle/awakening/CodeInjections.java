package net.spirangle.awakening;

import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.deities.Religion;
import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;
import javassist.expr.MethodCall;
import javassist.expr.NewExpr;
import net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening;
import net.spirangle.awakening.util.Syringe;

import java.util.logging.Logger;

import static net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening.NIGHT_STEED_CID;


public class CodeInjections {

    private static final Logger logger = Logger.getLogger(CodeInjections.class.getName());

    public static class StarterGearModifier extends ExprEditor {
        final Syringe s;
        final String kingdom;
        final String id;

        public StarterGearModifier(Syringe s, String kingdom, String id) {
            this.s = s;
            this.kingdom = kingdom;
            this.id = id;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("createSomeItems")) {
                mc.replace("net.spirangle.awakening.players.StarterGear.createSomeItems(player," + kingdom + ");");
                logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", custom starter gear [" + mc.getLineNumber() + "].");
            }
        }
    }


    public static class IsBulkModifier extends ExprEditor {
        final Syringe s;
        final String method;
        final String target;
        final int n;
        int i = 0;

        public IsBulkModifier(Syringe s, String method, String target, int n) {
            this.s = s;
            this.method = method;
            this.target = target;
            this.n = n;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals(method)) {
                if(n < 0 || i == n) {
                    mc.replace("$_ = (net.spirangle.awakening.items.ItemTemplateCreatorAwakening.isBulkContainer(" + target + ") || $proceed($$));");
                    logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + " (" + method + "), additional bulk containers [" + mc.getLineNumber() + "].");
                }
                ++i;
            }
        }
    }


    public static class IsPriestSuppressor extends ExprEditor {
        final Syringe s;

        public IsPriestSuppressor(Syringe s) {
            this.s = s;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getClassName().equals("com.wurmonline.server.creatures.Creature") && mc.getMethodName().equals("isPriest")) {
                mc.replace("$_ = false;");
                logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", is priest suppressor [" + mc.getLineNumber() + "].");
            }
        }
    }


    public static class IsSpiritGuardSuppressor extends ExprEditor {
        final Syringe s;
        final int index;
        int i = 0;

        public IsSpiritGuardSuppressor(Syringe s, int index) {
            this.s = s;
            this.index = index;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("isSpiritGuard")) {
                if(i == index || index == -1) {
                    mc.replace("$_ = false;");
                    logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", is spirit guard suppressor [" + mc.getLineNumber() + "].");
                }
                ++i;
            }
        }
    }


    public static class IsKingdomGuardOrSpiritGuard extends ExprEditor {
        final Syringe s;
        final int index;
        int i = 0;

        public IsKingdomGuardOrSpiritGuard(Syringe s, int index) {
            this.s = s;
            this.index = index;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("isKingdomGuard")) {
                if(i == index || index == -1) {
                    mc.replace("$_ = ($proceed($$) || $0.isSpiritGuard());");
                    logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", is kingdom guard or spirit guard [" + mc.getLineNumber() + "].");
                }
                ++i;
            }
        }
    }


    public static class IsFriendlyKingdomSuppressor extends ExprEditor {
        final Syringe s;
        final int index;
        int i = 0;

        public IsFriendlyKingdomSuppressor(Syringe s, int index) {
            this.s = s;
            this.index = index;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("isFriendlyKingdom")) {
                if(i == index) {
                    mc.replace("$_ = true;");
                    logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", is friendly kingdom suppressor [" + mc.getLineNumber() + "].");
                }
                ++i;
            }
        }
    }


    public static class IsNoEatOrDrinkFix extends ExprEditor {
        final Syringe s;

        public IsNoEatOrDrinkFix(Syringe s) {
            this.s = s;
        }

        @Override
        public void edit(final MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("isNoEatOrDrink")) {
                mc.replace("$_ = $proceed($$) || net.spirangle.awakening.misc.PermissionsFix.isNoEatOrDrink($0);");
                logger.info(s.getCtClass().getName() + ": Installed modify " + s.getCtMethod().getName() + ", extra eat or drink check to action [" + mc.getLineNumber() + "].");
            }
        }
    }


    public static class AlignmentModifier extends ExprEditor {
        final Syringe s;
        final String method;
        final int index;
        final String action;
        String code;
        int i = 0;

        public AlignmentModifier(Syringe s, int deityId, float mod, String action) {
            this(s, null, null, new int[]{ deityId }, new float[]{ mod }, null, 0, action);
        }

        public AlignmentModifier(Syringe s, int deityId, float mod, String method, String action) {
            this(s, null, null, new int[]{ deityId }, new float[]{ mod }, method, 0, action);
        }

        public AlignmentModifier(Syringe s, String statement, int deityId, float mod, String action) {
            this(s, statement, null, new int[]{ deityId }, new float[]{ mod }, null, 0, action);
        }

        public AlignmentModifier(Syringe s, String statement, int[] deityIds, float[] mods, int index, String action) {
            this(s, statement, null, deityIds, mods, null, index, action);
        }

        @SuppressWarnings("unused")
        public AlignmentModifier(Syringe s, String statement, int[] deityIds, float[] mods, String method, String action) {
            this(s, statement, null, deityIds, mods, method, 0, action);
        }

        public AlignmentModifier(Syringe s, String statement, String performer, int[] deityIds, float[] mods, String method, int index, String action) {
            this.s = s;
            statement = statement == null? "" : statement + " && ";
            if(performer == null) performer = "performer";
            if(method == null) method = "getDeity";
            this.method = method;
            this.index = index;
            this.action = action;
            this.code = "if(" + statement + performer + ".getDeity()!=null) {\n";
            for(int i = 0; i < deityIds.length; ++i) {
                this.code += "   ";
                if(i > 0) this.code += "else ";
                this.code += "if(" + performer + ".getDeity().number==" + deityIds[i] + ") " + performer + ".maybeModifyAlignment(" + mods[i] + "f);\n";
            }
            this.code += "}";
        }

        @Override
        public void edit(MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals(method)) {
                if(i == index) {
                    s.getCtMethod().insertAt(mc.getLineNumber(), code);
                    logger.info("Installed modify alignment for " + action + ".");
                }
                ++i;
            }
        }
    }

    public static class BuryAlignmentModifier extends ExprEditor {
        final Syringe s;
        final String action;

        public BuryAlignmentModifier(Syringe s, String action) {
            this.s = s;
            this.action = action;
        }

        @Override
        public void edit(MethodCall mc) throws CannotCompileException {
            if(mc.getMethodName().equals("maybeModifyAlignment")) {
                mc.replace("if(performer.getDeity()!=null && performer.getDeity().isHateGod())\n" +
                           "   performer.maybeModifyAlignment(-1.0f);" +
                           "else $_ = $proceed($$);");
                logger.info("Installed modify alignment for " + action + ".");
            }
        }
    }

    public static void preInit() {

        /* Server: */
        final Syringe server = Syringe.getClass("com.wurmonline.server.Server");
        server.insertAfter("run", "net.spirangle.awakening.time.Scheduler.handleServerLag(com.wurmonline.server.Server.secondsLag);", null);

        /* LoginHandler: */
        final Syringe lh = Syringe.getClass("com.wurmonline.server.LoginHandler");
        lh.instrument("reallyHandle", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("addLoginHandler")) {
                    mc.replace("$proceed($$);$_ = true;");
                    logger.info("LoginHandler: Accept login from different IPs.");
                }
            }
        });
        lh.insertBefore("login",
                        "{\n" +
                        "   String text = net.spirangle.awakening.players.LoginHandler.playerLoginTest($1,$2,$6);\n" +
                        "   if(text!=null) {\n" +
                        "      try {\n" +
                        "         com.wurmonline.server.Server.getInstance().steamHandler.EndAuthSession(steamIDAsString);\n" +
                        "         sendLoginAnswer(false,text,0.0f,0.0f,0.0f,0.0f,0,\"model.player.broken\",(byte)0,0);\n" +
                        "      } catch(java.io.IOException ioe) {\n" +
                        "         com.wurmonline.server.LoginHandler.logger.log(java.util.logging.Level.WARNING,this.conn.getIp()+\", problem sending login denied message: \"+text,ioe);\n" +
                        "      }\n" +
                        "      return;\n" +
                        "   }\n" +
                        "}", null);
        lh.setBody("getInitialSpawnPoint", "return net.spirangle.awakening.players.LoginHandler.getInitialSpawnPoint($1);", null);
        lh.instrument("createPlayer", "(Lcom/wurmonline/server/players/Player;I)I", new StarterGearModifier(lh, "-1", "1"));
        lh.instrument("createAndReturnPlayer", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBJBZZZJ)[B", new StarterGearModifier(lh, "kingdom", "2"));
        lh.instrument("createPlayer", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBJBZZJ)J", new StarterGearModifier(lh, "kingdom", "3"));
        lh.setBody("sendWho", "net.spirangle.awakening.players.LoginHandler.sendWho($1,$2);", null);
        lh.instrument("loadPlayer", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomId")) {
                    mc.replace("$_ = net.spirangle.awakening.players.LoginHandler.getClientKingdomId($0);");
                    logger.info("LoginHandler: Change kingdom Id to deity number for adepts.");
                }
            }
        });

        /* Players: */
        final Syringe players = Syringe.getClass("com.wurmonline.server.Players");
        players.insertBefore("sendAltarsToPlayer", "net.spirangle.awakening.players.LoginHandler.sendAltarsToPlayer($1);", null);
        players.setBody("sendPAWindow", "net.spirangle.awakening.players.ChatChannels.sendPAWindow($1);", null);
        players.setBody("getKingdomHelpChannelName", "return \"Help\";", null);

        /* Player: */
        final Syringe player = Syringe.getClass("com.wurmonline.server.players.Player");
        player.addField("public int petOrderState;", "0");
        player.addMethod("public String getHoverText(com.wurmonline.server.creatures.Creature watcher) { return net.spirangle.awakening.players.PlayerData.getHoverText(this,super.getHoverText(watcher)); }");
        player.setBody("seesPlayerAssistantWindow", "return $0.saveFile.seesPlayerAssistantWindow();", null);
        player.insertBefore("pollPayment", "net.spirangle.awakening.ClientConnection.handleClientModPoll($0);", null);
        player.instrument("setLegal", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomTemplateId")) {
                    mc.replace("$_ = 3;");
                    logger.info("setLegal: Permit players of all kingdoms turn unlawful, even mayors.");
                }
            }
        });
        player.instrument("setReputation", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isThisAChaosServer")) {
                    mc.replace("$_ = true;");
                    logger.info("setReputation: Prevent players from joining Shodroq (HOTS) when reputation goes below -200.");
                } else if(mc.getMethodName().equals("sendAlertServerMessage")) {
                    if(i == 3) {
                        mc.replace(";");
                        logger.info("setReputation: Prevent message about joining HOTS on reputation below -180.");
                    }
                    ++i;
                }
            }
        });

        /* Creature: */
        final Syringe creature = Syringe.getClass("com.wurmonline.server.creatures.Creature");
        creature.addField("public net.spirangle.awakening.creatures.CreatureAI customAI;", "null");
        creature.addMethod("public boolean isServant() { return false; }");
        creature.addMethod("public boolean isShadeHound() { return template.id==" + CreatureTemplateCreatorAwakening.SHADE_HOUND_CID + "; }");
        creature.addMethod("public boolean isDragonet() { return template.id>=" + CreatureTemplateCreatorAwakening.DRAGONET_BLACK_CID + " && template.id<=" + CreatureTemplateCreatorAwakening.DRAGONET_GREEN_CID + "; }");
        creature.addMethod("public boolean isCourtMagus() { return com.wurmonline.server.kingdom.King.isOfficial(1501,this.getWurmId(),this.getKingdomId());  }");
        creature.addMethod("public int getPetOrderState() { return 0; }");
        creature.addMethod("public void setPetOrderState(int state) {}");
        creature.setBody("getNameWithGenus",
                         "{\n" +
                         "   if(this.isUnique() || this.isPlayer()) return this.getName();\n" +
                         "   if(this.name.toLowerCase().compareTo(this.template.getName().toLowerCase()) != 0) return this.getName();\n" +
                         "   return com.wurmonline.shared.util.StringUtilities.addGenus(this.getName(),false);\n" +
                         "}", null);
        creature.instrument("doNew", "(IZFFFILjava/lang/String;BBBZBI)Lcom/wurmonline/server/creatures/Creature;", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("broadCastSafe")) {
                    creature.getCtMethod().insertAt(mc.getLineNumber() + 1,
                                                    "net.spirangle.awakening.players.ChatChannels.getInstance().sendRumour(toReturn);");
                    logger.info("doNew: Send rumor of unique spawned on Discord.");
                }
            }
        });
        creature.instrument("wearItems", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("insertItem")) {
                    mc.replace("if(!($_=$proceed($$))) net.spirangle.awakening.creatures.Servant.wearItemsFix(this,$0,$1);");
                    logger.info("Creature: Installed wear items fix.");
                }
            }
        });
        creature.instrument("checkPregnancy", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getChildTemplateId")) {
                    if(i == 0) {
                        mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.getChildTemplateId(this,$proceed($$));");
                        logger.info("Creature: Installed custom offspring function.");
                    }
                    ++i;
                } else if(mc.getMethodName().equals("isHorse")) {
                    mc.replace("$_ = !this.isUnicorn();");
                    logger.info("Creature: Installed change for all bred animals to get offspring names.");
                } else if(mc.getMethodName().equals("generateGenericName")) {
                    mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.generateGenericName(this);\n" +
                               "if($_==null) $_ = $proceed($$);");
                    logger.info("Creature: Installed generate generic name.");
                } else if(mc.getMethodName().equals("generateFemaleName")) {
                    mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.generateFemaleName(this);\n" +
                               "if($_==null) $_ = $proceed($$);");
                    logger.info("Creature: Installed generate female name.");
                } else if(mc.getMethodName().equals("generateGenericName")) {
                    mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.generateMaleName(this);\n" +
                               "if($_==null) $_ = $proceed($$);");
                    logger.info("Creature: Installed generate male name.");
                } else if(mc.getMethodName().equals("isAggHuman")) {
                    mc.replace("$_ = $proceed($$) || this.template.getTemplateId()==" + NIGHT_STEED_CID + ";");
                    logger.info("Creature: Make Night steed foals have same kingdom as mother.");
                }
            }
        });
        creature.insertBefore("clearOrders",
                              "com.wurmonline.server.creatures.Creature dominator = this.getDominator();\n" +
                              "if(dominator!=null && dominator.isPlayer()) dominator.setPetOrderState(0);", null);
        creature.instrument("modifyFightSkill", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getFightingSkill")) {
                    if(i == 6) {
                        mc.replace("net.spirangle.awakening.players.Combat.getInstance().modifyFightSkill($0,this,pskill,skillGained);\n" +
                                   "$_ = $proceed($$);");
                        logger.info("Creature: Inserted handling of fighting skill gain at modifyFightSkill.");
                    }
                    ++i;
                }
            }
        });
        creature.insertBefore("die", "(ZLjava/lang/String;Z)V",
                              "net.spirangle.awakening.players.Combat.getInstance().die($0,$0.attackers);", "handle kills");
        creature.instrument("pollStamina", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getDeity")) {
                    if(i == 1) {
                        mc.replace("$_ = $proceed($$);\n" +
                                   "if($_.number==28 || $_.number==29) $_ = com.wurmonline.server.deities.Deities.getDeity(4);");
                        logger.info("Creature: Make followers of some gods have reduced hunger on mycelium.");
                    }
                    ++i;
                }
            }
        });
        creature.insertAfter("sendAddSpellEffect",
                             "if($1.type>=101) com.wurmonline.server.spells.SpellGeneratorAwakening.addEffect($0,$1);", "add custom effects");
        creature.insertAfter("removeSpellEffect",
                             "if($1.type>=101) com.wurmonline.server.spells.SpellGeneratorAwakening.removeEffect($0,$1);", "remove custom effects");
        creature.instrument("die", "(ZLjava/lang/String;Z)V", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("setAuxData")) {
                    if(i == 1) {
                        mc.replace("$_ = $proceed($$);\n" +
                                   "$0.setColor(com.wurmonline.server.items.WurmColor.createColor(this.getColorRed(),this.getColorGreen(),this.getColorBlue()));");
                        logger.info("Creature: Make corpse keep creature color.");
                    }
                    ++i;
                }
            }
        });
        creature.insertBefore("pollNPCChat", "if(!net.spirangle.awakening.creatures.CreatureAI.pollNPCChat($0,$0.customAI)) {\n" +
                                             "   $0.customAI = net.spirangle.awakening.creatures.CreatureAI.getCreatureAI($0);\n" +
                                             "}", "added handling creature custom AI");
        creature.instrument("attackTarget", new IsSpiritGuardSuppressor(creature, 0));
        creature.instrument("moveAlongPath", new IsSpiritGuardSuppressor(creature, 0));
        creature.instrument("doNew", "(IZFFFILjava/lang/String;BBBZBI)Lcom/wurmonline/server/creatures/Creature;", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.instrument("findRandomCaveEntrance", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.instrument("checkBreedingPossibility", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.instrument("setDisease", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.instrument("doLavaDamage", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.instrument("canUseWithEquipment", new IsKingdomGuardOrSpiritGuard(creature, -1));
        creature.insertAfter("loadSkills", "if(this.isPlayer()) com.wurmonline.server.skills.ModSkills.loadSkills($0,$0.skills);", null);
        creature.insertBefore("createBasicItems", "net.spirangle.awakening.creatures.Spawner.createBasicItems($1);", null);

        /* Player: */
        player.addMethod("public int getPetOrderState() { return this.petOrderState; }");
        player.addMethod("public void setPetOrderState(int state) { this.petOrderState = state;this.getStatus().sendStateString(); }");
        player.instrument("addTitle", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("sendNormalServerMessage")) {
                    mc.replace("$_ = $proceed($$);\n" +
                               "net.spirangle.awakening.players.Titles.addTitle(this,title);");
                    logger.info("Player: Add custom handler for when players receive titles.");
                }
            }
        });
        player.instrument("pollFavor", new ExprEditor() {
            @Override
            public void edit(FieldAccess fa) throws CannotCompileException {
                if(fa.getFieldName().equals("favorGainSecondsLeft")) {
                    fa.replace("$_ = (this.favorGainSecondsLeft>0 || this.isCourtMagus()? 1 : 0);");
                    logger.info("pollFavor: Add double favor gains for the Court Magus.");
                }
            }
        });

        /* Creature: */
        creature.instrument("getAttitude", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getAttitude")) {
                    if(i == 0) {
                        mc.replace("$_ = net.spirangle.awakening.creatures.Pets.getPetAttitude($1,$0,$1.getPetOrderState());");
                        logger.info("Creature: Insert changed model for pet towards target attitude.");
                    } else if(i == 1 || i == 2) {
                        mc.replace("$_ = net.spirangle.awakening.creatures.Pets.getTargetAttitude($1,$0,$1.getPetOrderState());");
                        logger.info("Creature: Insert changed model for target towards pet attitude.");
                    }
                    ++i;
                }
            }
        });
        creature.insertBefore("getAttitude",
                              "if(net.spirangle.awakening.creatures.CreatureAI.getAttitude(this,aTarget)==0) { return 0; }", "special creature attitude for kingdom citizens");
        creature.setBody("isDominatable",
                         "return ($0.getLeader()==null || $0.getLeader()==$1) && !$0.isRidden() && $0.hitchedTo==null && " +
                         "($0.template.isDominatable() || ($1.isPlayer() && (($0.isShadeHound() && $1.getKingdomId()==3) || " +
                         "($0.isDragonet() && $1.getKingdomId()==4))));", "special tameable creature for kingdom citizens");
        creature.insertBefore("canAutoDismissMerchant",
                              "if(!net.spirangle.awakening.creatures.CreatureAI.canAutoDismissMerchant($0,$1)) return false;", "handle block dismissing of merchants");

        /* Npc: */
        final Syringe npc = Syringe.getClass("com.wurmonline.server.creatures.Npc");
        npc.addField("public net.spirangle.awakening.creatures.Servant servant;", "null");
        npc.addMethod("public boolean isServant() { return this.servant!=null; }");
        npc.addMethod("public String getNameWithGenus() { return this.getName(); }");
        npc.insertBefore("pollNPCChat", "if(!net.spirangle.awakening.creatures.Servant.isLoaded() || (this.isServant() && this.servant.pollChat())) { return; }", null);
        npc.insertBefore("doSomething",
                         "if(!net.spirangle.awakening.creatures.Servant.isLoaded()) return;\n" +
                         "if(this.isServant()) {\n" +
                         "  this.servant.poll();\n" +
                         "  this.setPassiveCounter(120);\n" +
                         "  return;\n" +
                         "}", null);
        npc.insertBefore("getMoveTarget", "if(!net.spirangle.awakening.creatures.Servant.isLoaded() || this.isServant()) { return null; }", null);
        npc.insertBefore("getFace", "if(this.isServant()) { return this.servant.face; }", null);

        /* CreatureAI: */
        final Syringe cai = Syringe.getClass("com.wurmonline.server.creatures.ai.CreatureAI");
        cai.instrument("creatureMovementTick", new IsSpiritGuardSuppressor(cai, 1));

        /* CreatureBehaviour: */
        final Syringe cb = Syringe.getClass("com.wurmonline.server.behaviours.CreatureBehaviour");
        cb.instrument("getBehavioursFor", "(Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/creatures/Creature;)Ljava/util/List;",
                      new IsFriendlyKingdomSuppressor(cb, 0));
        cb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/creatures/Creature;SF)Z",
                      new IsFriendlyKingdomSuppressor(cb, 0));
        cb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/creatures/Creature;SF)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("attackTarget")) {
                    mc.replace("if(performer.isPlayer()) performer.setPetOrderState(1);\n" +
                               "$_ = $proceed($$);");
                    logger.info("CreatureBehaviour: Set decisions.state = 1, to automatically attack enemies.");
                }
            }
        });
        cb.insertBefore("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/creatures/Creature;SF)Z",
                        "if(action==com.wurmonline.server.behaviours.Actions.GIVE" +
                        " && target!=null && target.isServant()" +
                        " && !net.spirangle.awakening.creatures.Servant.giveToServant(performer,source,target,action)) return true;", "give to servant action");
        cb.instrument("handle_EXAMINE", new ExprEditor() {
            int i = 0, j = -1;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("sendNormalServerMessage")) {
                    if(i == 3) {
                        mc.replace("net.spirangle.awakening.kingdom.Kingdoms.sendKingdomName(performer,false);");
                        logger.info("handle_EXAMINE: Change kingdom names of players to server kingdoms.");
                    } else if(i == 9) {
                        mc.replace("net.spirangle.awakening.kingdom.Kingdoms.sendKingdomBloodName(performer);");
                        logger.info("handle_EXAMINE: Change kingdom blood names of players to server kingdoms.");
                    } else if(j == 0) {
                        mc.replace("$_ = $proceed((target.getName()+\": \")+buf.toString());");
                        logger.info("handle_EXAMINE: Show animal name with traits.");
                    }
                    ++i;
                    if(j >= 0) ++j;
                } else if(mc.getMethodName().equals("hasTraits")) {
                    j = 0;
                }
            }
        });
        cb.setBody("handle_ASK_GIFT", "net.spirangle.awakening.time.Christmas.handle_ASK_GIFT($1,$2);", null);
        cb.insertBefore("handle_TARGET_and_TARGET_HOSTILE", "if(com.wurmonline.server.spells.Invisibility.handle_TARGET_and_TARGET_HOSTILE($1,$2)) return true;", null);

        /* CreatureStatus: */
        final Syringe cs = Syringe.getClass("com.wurmonline.server.creatures.CreatureStatus");
        cs.setBody("getTypeString", "return net.spirangle.awakening.creatures.Traits.getTypeString($0.modtype);", null);
        cs.setBody("getModtypeForString", "return net.spirangle.awakening.creatures.Traits.getModtypeForString($1);", null);
        cs.setBody("hasCustomColor", "return net.spirangle.awakening.creatures.Traits.hasCustomColor($0.modtype);", null);
        cs.setBody("getColorRed", "return net.spirangle.awakening.creatures.Traits.getColorRed($0.modtype);", null);
        cs.setBody("getColorGreen", "return net.spirangle.awakening.creatures.Traits.getColorGreen($0.modtype);", null);
        cs.setBody("getColorBlue", "return net.spirangle.awakening.creatures.Traits.getColorBlue($0.modtype);", null);
        cs.insertAfter("getBattleRatingTypeModifier", "return net.spirangle.awakening.creatures.Traits.getBattleRatingTypeModifier($0.modtype,$_);", null);
        cs.insertAfter("getDamageTypeModifier", "return net.spirangle.awakening.creatures.Traits.getDamageTypeModifier($0.modtype,$_);", null);
        cs.insertAfter("getParryTypeModifier", "return net.spirangle.awakening.creatures.Traits.getParryTypeModifier($0.modtype,$_);", null);
        cs.insertAfter("getDodgeTypeModifier", "return net.spirangle.awakening.creatures.Traits.getDodgeTypeModifier($0.modtype,$_);", null);
        cs.insertAfter("getAggTypeModifier", "return net.spirangle.awakening.creatures.Traits.getAggTypeModifier($0.modtype,$_);", null);
        cs.instrument("pollAge", new ExprEditor() {
            int i = 0, j = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getAdultMaleTemplateId")) {
                    if(i == 1) {
                        mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.getAdultMaleTemplateId(this.statusHolder,$proceed($$));");
                        logger.info("Creature: Installed custom function for getting adult male template.");
                    }
                    ++i;
                } else if(mc.getMethodName().equals("getAdultFemaleTemplateId")) {
                    if(j == 2) {
                        mc.replace("$_ = net.spirangle.awakening.creatures.Offspring.getAdultFemaleTemplateId(this.statusHolder,$proceed($$));");
                        logger.info("Creature: Installed custom function for getting adult female template.");
                    }
                    ++j;
                } else if(mc.getMethodName().equals("isHorse")) {
                    mc.replace("$_ = true;");
                    logger.info("CreatureStatus: Make all animals with offspring names keep them when turning adult.");
                }
            }
        });
        cs.instrument("sendStateString", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("listIterator")) {
                    mc.replace("net.spirangle.awakening.creatures.Pets.getStateString(this.statusHolder,$0,this.statusHolder.getPetOrderState());\n" +
                               "$_ = $proceed($$);");
                    logger.info("CreatureStatus: Show pet order state in player status.");
                }
            }
        });
        cs.instrument("getSizeMod", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getTemplateId")) {
                    mc.replace("$_ = 0;");
                    logger.info("CreatureStatus: Make named bison retain normal size.");
                }
            }
        });
        cs.instrument("getSizeMod", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getHitched")) {
                    cs.getCtMethod().insertAt(mc.getLineNumber(), "floatToRet *= net.spirangle.awakening.creatures.Traits.getSizeMod(this.statusHolder);");
                    logger.info("CreatureStatus: Modify size with certain traits for some creatures.");
                }
            }
        });

        /* ActionStack: */
        final Syringe actst = Syringe.getClass("com.wurmonline.server.behaviours.ActionStack");
        actst.insertBefore("addAction", "net.spirangle.awakening.actions.ActionStack.addAction(action);", null);

        /* CorpseBehaviour: */
        final Syringe corpse = Syringe.getClass("com.wurmonline.server.behaviours.CorpseBehaviour");
        corpse.instrument("bury", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("destroyItem")) {
                    if(i == 1) {
                        corpse.getCtMethod().addLocalVariable("corpseItems", CtClass.intType);
                        mc.replace("corpseItems = net.spirangle.awakening.players.Combat.hasCorpseItems(performer,corpse,template,action);\n" +
                                   "$_ = $proceed($$);");
                        logger.info("CorpseBehaviour: Install check for items in corpse when burying.");
                    }
                    ++i;
                } else if(mc.getMethodName().equals("checkCoinAward")) {
                    mc.replace("$_ = $proceed($$);" +/*(int)((float)($1)*"+rareCoinChanceMultiplier+")*/
                               "net.spirangle.awakening.players.Combat.getInstance().bury(performer,corpse,corpseItems,action);\n");
                    logger.info("CorpseBehaviour: Install additional bury code, i.e. karma gains etc.");
                }
            }
        });
        corpse.instrument("bury", new BuryAlignmentModifier(corpse, "bury"));
        corpse.instrument("createGrave", new BuryAlignmentModifier(corpse, "createGrave"));

        /* WurmCalendar: */
        final Syringe wc = Syringe.getClass("com.wurmonline.server.WurmCalendar");
        if(AwakeningMod.debug) {
            wc.setBody("isChristmas", "return nowIsBetween(14,50, 13, 11, java.time.Year.now().getValue(), 12, 0, 31, 11, java.time.Year.now().getValue());", null);
        } else {
            wc.setBody("isChristmas", "return nowIsBetween(14,50, 13, 11, java.time.Year.now().getValue(), 12, 0, 22, 11, java.time.Year.now().getValue());", null);
        }

        /* VirtualZone: */
        final Syringe vz = Syringe.getClass("com.wurmonline.server.zones.VirtualZone");
        vz.instrument("addCreature", "(JZJFFF)Z", new ExprEditor() {
            int i = 0, j = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isNpc")) {
                    if(i == 1) {
                        mc.replace("$_ = false;");
                        logger.info("VirtualZone: Removed NPCs from local chat.");
                    }
                    ++i;
                } else if(mc.getMethodName().equals("isSpiritGuard")) {
                    mc.replace("$_ = true;");
                    logger.info("VirtualZone: Make ghost creatures visible.");
                } else if(mc.getMethodName().equals("getKingdomId")) {
                    if(j == 4) {
                        mc.replace("$_ = net.spirangle.awakening.players.LoginHandler.getClientKingdomId($0);");
                        logger.info("VirtualZone: Change kingdom Id to deity number for adepts.");
                    }
                    ++j;
                }
            }
        });

        /* Villages: */
        final Syringe villages = Syringe.getClass("com.wurmonline.server.villages.Villages");
        villages.setBody("isFocusZoneBlocking", "{ return \"\"; }", "permit deeding inside focus zones, PvE etc.");
        villages.instrument("canFoundVillage", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(NewExpr nc) throws CannotCompileException {
                if(nc.getClassName().equals("java.awt.Rectangle")) {
                    if(i == 1) {
                        nc.replace("$_ = $proceed($1+75,$2+75,$3-150,$4-150);");
                        logger.info("Villages: Make minimum distance between deeds 25 tiles instead of 100 when founding settlement.");
                    }
                    ++i;
                }
            }
        });

        /* KingdomStatusQuestion: */
        final Syringe ksq = Syringe.getClass("com.wurmonline.server.questions.KingdomStatusQuestion");
        ksq.setBody("mayAlly", "return true;", null);

        /* CropTilePoller: */
        final Syringe ctp = Syringe.getClass("com.wurmonline.server.zones.CropTilePoller");
        ctp.insertBefore("initializeFields",
                         "com.wurmonline.server.zones.CropTilePoller.lastPolledTiles = net.spirangle.awakening.zones.Tiles.getLastPolledTiles();", "make farm ticks at 00:00 GMT");
        ctp.instrument("checkForFarmGrowth", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("setTile")) {
                    if(i == 2) {
                        ctp.getCtMethod().insertAt(mc.getLineNumber(), "if(!net.spirangle.awakening.zones.Tiles.checkForFarmGrowth(tempvtile1,farmed)) return;");
                        logger.info("CropTilePoller: Install block for untended field growth on deed.");
                    }
                    ++i;
                }
            }
        });

        /* EndGameItems: */
        final Syringe egi = Syringe.getClass("com.wurmonline.server.endgames.EndGameItems");
        egi.setBody("createAltars", "{}", "removed loading of huge altars");

        /* Deities: */
        final Syringe deities = Syringe.getClass("com.wurmonline.server.deities.Deities");
        deities.setBody("isNameOkay", "{ return true; }", "fixed accepting deity names for players");
        deities.insertBefore("addDeity", "if(com.wurmonline.server.deities.Religion.addDeity($1)) return;", "Fixed custom deity settings");

        /* Deity: */
        final Syringe deity = Syringe.getClass("com.wurmonline.server.deities.Deity");
        deity.addField("public long attributes;", "0L");
        deity.addMethod("public boolean isTypeSky() { return (attributes&" + Religion.DEITY_TYPE_SKY + ")!=0; }");
        deity.addMethod("public boolean isTypeSun() { return (attributes&" + Religion.DEITY_TYPE_SUN + ")!=0; }");
        deity.addMethod("public boolean isTypeStar() { return (attributes&" + Religion.DEITY_TYPE_STAR + ")!=0; }");
        deity.addMethod("public boolean isTypeMoon() { return (attributes&" + Religion.DEITY_TYPE_MOON + ")!=0; }");
        deity.addMethod("public boolean isTypeWind() { return (attributes&" + Religion.DEITY_TYPE_WIND + ")!=0; }");
        deity.addMethod("public boolean isTypeThunder() { return (attributes&" + Religion.DEITY_TYPE_THUNDER + ")!=0; }");
        deity.addMethod("public boolean isTypeRain() { return (attributes&" + Religion.DEITY_TYPE_RAIN + ")!=0; }");
        deity.addMethod("public boolean isTypeEarth() { return (attributes&" + Religion.DEITY_TYPE_EARTH + ")!=0; }");
        deity.addMethod("public boolean isTypeNature() { return (attributes&" + Religion.DEITY_TYPE_NATURE + ")!=0; }");
        deity.addMethod("public boolean isTypeWater() { return (attributes&" + Religion.DEITY_TYPE_WATER + ")!=0; }");
        deity.addMethod("public boolean isTypeMetal() { return (attributes&" + Religion.DEITY_TYPE_METAL + ")!=0; }");
        deity.addMethod("public boolean isTypeMountain() { return (attributes&" + Religion.DEITY_TYPE_MOUNTAIN + ")!=0; }");
        deity.addMethod("public boolean isTypeFertility() { return (attributes&" + Religion.DEITY_TYPE_FERTILITY + ")!=0; }");
        deity.addMethod("public boolean isTypeForest() { return (attributes&" + Religion.DEITY_TYPE_FOREST + ")!=0; }");
        deity.addMethod("public boolean isTypeAnimals() { return (attributes&" + Religion.DEITY_TYPE_ANIMALS + ")!=0; }");
        deity.addMethod("public boolean isTypeArtisan() { return (attributes&" + Religion.DEITY_TYPE_ARTISAN + ")!=0; }");
        deity.addMethod("public boolean isTypeHunting() { return (attributes&" + Religion.DEITY_TYPE_HUNTING + ")!=0; }");
        deity.addMethod("public boolean isTypeCreator() { return (attributes&" + Religion.DEITY_TYPE_CREATOR + ")!=0; }");
        deity.addMethod("public boolean isTypeDestroyer() { return (attributes&" + Religion.DEITY_TYPE_DESTROYER + ")!=0; }");
        deity.addMethod("public boolean isTypeHealing() { return (attributes&" + Religion.DEITY_TYPE_HEALING + ")!=0; }");
        deity.addMethod("public boolean isTypeWisdom() { return (attributes&" + Religion.DEITY_TYPE_WISDOM + ")!=0; }");
        deity.addMethod("public boolean isTypeFate() { return (attributes&" + Religion.DEITY_TYPE_FATE + ")!=0; }");
        deity.addMethod("public boolean isTypeMagic() { return (attributes&" + Religion.DEITY_TYPE_MAGIC + ")!=0; }");
        deity.addMethod("public boolean isTypeJustice() { return (attributes&" + Religion.DEITY_TYPE_JUSTICE + ")!=0; }");
        deity.addMethod("public boolean isTypeArts() { return (attributes&" + Religion.DEITY_TYPE_ARTS + ")!=0; }");
        deity.addMethod("public boolean isTypeTravel() { return (attributes&" + Religion.DEITY_TYPE_TRAVEL + ")!=0; }");
        deity.addMethod("public boolean isTypeSleep() { return (attributes&" + Religion.DEITY_TYPE_SLEEP + ")!=0; }");
        deity.addMethod("public boolean isTypeWealth() { return (attributes&" + Religion.DEITY_TYPE_WEALTH + ")!=0; }");
        deity.addMethod("public boolean isTypeLove() { return (attributes&" + Religion.DEITY_TYPE_LOVE + ")!=0; }");
        deity.addMethod("public boolean isTypeHonor() { return (attributes&" + Religion.DEITY_TYPE_HONOR + ")!=0; }");
        deity.addMethod("public boolean isTypeWar() { return (attributes&" + Religion.DEITY_TYPE_WAR + ")!=0; }");
        deity.addMethod("public boolean isTypeFire() { return (attributes&" + Religion.DEITY_TYPE_FIRE + ")!=0; }");
        deity.addMethod("public boolean isTypeTrickster() { return (attributes&" + Religion.DEITY_TYPE_TRICKSTER + ")!=0; }");
        deity.addMethod("public boolean isTypeDeath() { return (attributes&" + Religion.DEITY_TYPE_DEATH + ")!=0; }");
        deity.addMethod("public boolean isTypeChaos() { return (attributes&" + Religion.DEITY_TYPE_CHAOS + ")!=0; }");
        deity.addMethod("public boolean isTypeHate() { return (attributes&" + Religion.DEITY_TYPE_HATE + ")!=0; }");
        deity.addMethod("public boolean isAllowingStealing() { return (attributes&" + (Religion.DEITY_TYPE_FATE | Religion.DEITY_TYPE_JUSTICE | Religion.DEITY_TYPE_LOVE | Religion.DEITY_TYPE_HONOR) + ")==0; }");
        deity.setBody("isActionFaithful", "(I)Z", "return com.wurmonline.server.deities.Religion.isActionFaithful($0,$1);", "Replaced isActionFaithful with custom model");

        /* DbDeity: */
        final Syringe dbdeity = Syringe.getClass("com.wurmonline.server.deities.DbDeity");
        dbdeity.setBody("setFavor", "com.wurmonline.server.deities.Religion.setDeityFavor($0,$1);", "Custom handling of setting deity favor.");

        /* Item: */
        final Syringe item = Syringe.getClass("com.wurmonline.server.items.Item");
        item.insertBefore("isTurnable", "(Lcom/wurmonline/server/creatures/Creature;)Z", "if(!this.isTurnable() && this.canDisableTurning()) { return false; }", null);
        item.insertBefore("isMoveable", "if(this.isNoMove() && this.canDisableMoveable()) { return false; }", null);
        item.setBody("isHolyItem", "()Z", "{ return this.template.holyItem; }", "custom custom holy items");
        item.instrument("moveToItem", new IsBulkModifier(item, "isCrate", "target", 4));
        item.insertBefore("getSizeMod",
                          "if(this.getTemplateId()==272) return (float)Math.pow((double)this.getVolume()/(double)this.template.getVolume(), 0.3333333333333333);",
                          "Make corpse get same size as creature");
        item.insertAfter("getSizeMod",
                         "$_ *= net.spirangle.awakening.items.ItemTemplateCreatorAwakening.sizeMod(this);",
                         "Handle custom size of items");
        item.instrument("attackEnemies", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getClassName().equals("com.wurmonline.server.villages.Village") && mc.getMethodName().equals("isEnemy")) {
                    mc.replace("$_ = $proceed($$) && net.spirangle.awakening.creatures.CreatureAI.isVillageEnemy($0,$1);");
                    logger.info("Make turrets and archery towers not attack invited players.");
                }
            }
        });

        /* SimpleCreationEntry: */
        final Syringe sce = Syringe.getClass("com.wurmonline.server.items.SimpleCreationEntry");
        sce.instrument("run", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isHolyItem")) {
                    mc.replace("$_ = com.wurmonline.server.deities.Religion.isHolyItem_journalFix(performer,source,targetId,$0);");
                    logger.info("Fix so journal entry for \"Create an Electrum Statuette\" can be finished.");
                }
            }
        });

        /* ItemBehaviour: */
        final Syringe ib = Syringe.getClass("com.wurmonline.server.behaviours.ItemBehaviour");
        ib.instrument("moveBulkItemAsAction", new IsBulkModifier(ib, "isFood", "target", 1));
        ib.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;SF)Z", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getClassName().equals("com.wurmonline.server.creatures.Creature") && mc.getMethodName().equals("getVehicle")) {
                    if(i == 1) {
                        ib.getCtMethod().insertAt(mc.getLineNumber(),
                                                  "if(!net.spirangle.awakening.misc.PermissionsFix.canLoadCargo(target)) {" +
                                                  "   comm.sendNormalServerMessage(\"This \"+target.getName()+\" can't be loaded as cargo.\");" +
                                                  "   return true;" +
                                                  "}");
                        logger.info("ItemBehaviour: Added extra load cargo check to action.");
                    }
                    ++i;
                }
            }
        });
        ib.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;SF)Z", new IsNoEatOrDrinkFix(ib));
        ib.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;SF)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getClassName().equals("com.wurmonline.server.WurmCalendar") && mc.getMethodName().equals("isChristmas")) {
                    ib.getCtMethod().insertAt(mc.getLineNumber() + 1, "{ return net.spirangle.awakening.time.Christmas.handleChristmasTreeGift(performer,target); }");
                    logger.info("ItemBehaviour: Installed handle christmas tree gift.");
                }
            }
        });
        ib.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/items/Item;SF)Z", new IsNoEatOrDrinkFix(ib));
        ib.insertAfter("examine", "com.wurmonline.server.structures.StructureService.examineItem($2,$3);", null);
        ib.insertBefore("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;SF)Z",
                        "if(!net.spirangle.awakening.items.Items.openContainer($2,$3,$4)) return true;", null);
        ib.insertBefore("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/items/Item;SF)Z",
                        "if(!net.spirangle.awakening.items.Items.openContainer($2,$4,$5)) return true;", null);

        /* MethodsCreatures: */
        final Syringe mthc = Syringe.getClass("com.wurmonline.server.behaviours.MethodsCreatures");
        mthc.instrument("milk", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getNameWithoutPrefixes")) {
                    mc.replace("$_ = $0.getTemplate().getName();");
                    logger.info("MethodsCreatures: Changed naming of milk to use template name of creature.");
                } else if(mc.getMethodName().equals("fillContainer")) {
                    mc.replace("net.spirangle.awakening.creatures.Traits.milk($3,$4,target);\n" +
                               "$_ = $proceed($$);");
                    logger.info("MethodsCreatures: Installed adjust milk by traits function.");
                }
            }
        });
        mthc.instrument("shear", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("insertItem")) {
                    mc.replace("net.spirangle.awakening.creatures.Traits.shear($1,performer,target);\n" +
                               "$_ = $proceed($$);");
                    logger.info("MethodsCreatures: Installed adjust wool by traits function.");
                }
            }
        });
        mthc.instrument("tameEffect", new AlignmentModifier(mthc, Religion.DEITY_LAPHANER, -0.5f, "broadCastAction", "tame"));
        mthc.instrument("track", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("skillCheck")) {
                    mc.replace("$_ = $proceed($$);\n" +
                               "net.spirangle.awakening.zones.Infestations.track(performer,$1,$_);");
                    logger.info("MethodsCreatures: Add finding infestations to tracking.");
                }
            }
        });
        mthc.instrument("brand", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getCitizenVillage")) {
                    if(i == 0) mc.replace("$_ = ($0.getCurrentVillage()!=null? $proceed($$) : null);");
                    logger.info("MethodsCreatures: Fix brand null pointer bug.");
                    ++i;
                }
            }
        });

        /* MethodsItems: */
        final Syringe mi = Syringe.getClass("com.wurmonline.server.behaviours.MethodsItems");
        mi.instrument("improveItem", new IsPriestSuppressor(mi));
        mi.instrument("polishItem", new IsPriestSuppressor(mi));
        mi.instrument("temper", new IsPriestSuppressor(mi));
        mi.instrument("setTheftEffects", "(Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/behaviours/Action;IIZ)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isLibila")) {
                    mc.replace("$_ = $0.isAllowingStealing();");
                    logger.info("MethodsItems: Replaced isLibila in setTheftEffects with isAllowingStealing.");
                } else if(mc.getMethodName().equals("maybeModifyAlignment")) {
                    mc.replace("$_ = $proceed(performer.getDeity().isHateGod()? 5.0f : -5.0f);");
                    logger.info("MethodsItems: Make hate gods without isAllowingStealing get positive alignment punishment for stealing.");
                }
            }
        });
        mi.instrument("createSalve", new AlignmentModifier(mi, Religion.DEITY_SEDAES, 1.0f, "createItem", "mix"));
        mi.instrument("setTheftEffects", "(Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/behaviours/Action;IIZ)Z",
                      new AlignmentModifier(mi, Religion.DEITY_DRAKKAR, -1.0f, "theft"));
        mi.insertBefore("cannotPlant", "if(!net.spirangle.awakening.zones.Tiles.canPlantMarker($1,$2)) return true;", null);
        mi.insertAfter("mayDropDirt", "if(!net.spirangle.awakening.zones.Tiles.mayDropDirt($1)) return false;", null);
        mi.insertBefore("gatherRiftResource", "if(!net.spirangle.awakening.zones.Infestations.canGatherRiftResource($1,$2,$3,$4,$5)) return true;", null);
        mi.instrument("gatherRiftResource", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("createItem")) {
                    if(i == 0 || i == 2) {
                        mi.getCtMethod().insertAt(mc.getLineNumber() + 1, "net.spirangle.awakening.zones.Infestations.gatherRiftResource($1,$2,$3,$4,$5,power);");
                        logger.info("Gather infestation resource.");
                    }
                    ++i;
                }
            }
        });
        mi.insertAfter("getImpDesc", "$_ = net.spirangle.awakening.items.Items.getImpDesc($1,$2,$_);", "Handle getImpDesc message");

        /* MethodsStructure: */
        final Syringe ms = Syringe.getClass("com.wurmonline.server.behaviours.MethodsStructure");
        ms.instrument("continueFence", new AlignmentModifier(ms, Religion.DEITY_ASMAN, 0.5f, "continue fence"));

        /* ActionEntry: */
        final Syringe ae = Syringe.getClass("com.wurmonline.server.behaviours.ActionEntry");
        ae.addField("public long attributes;", "0L");

        /* Action: */
        final Syringe action = Syringe.getClass("com.wurmonline.server.behaviours.Action");
        action.instrument("poll", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isPriest")) {
                    if(i == 1) {
                        mc.replace("$_ = false;");
                        logger.info("Action: Block isPriest in action polls.");
                    }
                    ++i;
                }
            }
        });
        action.instrument("poll", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("maybeUpdateSecond")) {
                    mc.replace("$_ = $proceed($$);\n" +
                               "if(!this.done && this.performer.isPriest() && this.performer.getDeity()!=null) {\n" +
                               "   final short nr = this.getNumber();\n" +
                               "   if(nr>=0 && nr<com.wurmonline.server.behaviours.Actions.actionEntrys.length) {\n" +
                               "      final com.wurmonline.server.deities.Deity deity = this.performer.getDeity();\n" +
                               "      final com.wurmonline.server.behaviours.ActionEntry entry = com.wurmonline.server.behaviours.Actions.actionEntrys[nr];\n" +
                               "      this.done = entry!=null && com.wurmonline.server.deities.Religion.doAdeptCheck(this,entry,this.performer,deity,item,deity.attributes,entry.attributes);\n" +
                               "   }\n" +
                               "}");
                    logger.info("Action: Add restricted actions check for adepts.");
                }
            }
        });
        action.instrument("checkLegalMode", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isLibila")) {
                    mc.replace("$_ = true;");
                    logger.info("Make stealing permitted by all deities.");
                }
            }
        });

        /* Zones: */
        final Syringe zones = Syringe.getClass("com.wurmonline.server.zones.Zones");
        zones.setBody("loadChristmas", "if(!isHasLoadedChristmas()) { net.spirangle.awakening.time.Christmas.loadChristmas(); }", null);

        /* TilePoller: */
        final Syringe tp = Syringe.getClass("com.wurmonline.server.zones.TilePoller");
        tp.instrument("checkForGrassSpread", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomTemplateFor")) {
                    mc.replace("$_ = 3;");
                    logger.info("TilePoller: Make all land block mycelium turning to normal.");
                }
            }
        });
        tp.instrument("checkForMycelGrowth", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isThisAPvpServer")) {
                    mc.replace("$_ = false;");
                    logger.info("TilePoller: Block mycelium from spreading.");
                }
            }
        });
        tp.instrument("checkForTreeSprout", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomTemplateFor")) {
                    mc.replace("$_ = 3;");
                    logger.info("TilePoller: Make trees spreading to mycelium tiles become infected outside of Shodroq.");
                }
            }
        });
        tp.insertBefore("checkForTreeSprout", "if(!net.spirangle.awakening.zones.Tiles.checkForTreeSprout($1,$2,$3,$4)) return true;", null);

        /* VolaTile: */
        final Syringe vt = Syringe.getClass("com.wurmonline.server.zones.VolaTile");
        vt.instrument("pollOneCreatureOnThisTile", new IsKingdomGuardOrSpiritGuard(vt, -1));

        /* FaithZone: */
        final Syringe fz = Syringe.getClass("com.wurmonline.server.zones.FaithZone");
        fz.setBody("pollMycelium", "{\n" +
                                   "   java.awt.Point point = net.spirangle.awakening.zones.Tiles.getMyceliumGrowthPoint(this.altar);\n" +
                                   "   if(point!=null) spawnMycelium(point.x,point.y);\n" +
                                   "}", null);

        /* QuestionParser: */
        final Syringe qp = Syringe.getClass("com.wurmonline.server.questions.QuestionParser");
        qp.setBody("doesKingdomTemplateAcceptDeity", "{ return true; }");
        qp.instrument("parseSetDeityQuestion", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomId")) {
                    mc.replace("$_ = 0;");
                    logger.info("QuestionParser: No special handling for deities and kingdoms.");
                } else if(mc.getMethodName().equals("setKingdomId")) {
                    mc.replace("$_ = true;");
                    logger.info("QuestionParser: Permit all deities for all kingdoms.");
                } else if(mc.getMethodName().equals("setAlignment")) {
                    mc.replace("$_ = $proceed($0.getDeity().getAlignment()>=0? Math.max(50.0f,$0.getAlignment()) : Math.min(-50.0f,$0.getAlignment()));");
                    logger.info("QuestionParser: Make players get +/-50 alignment for all deities depending on WL/BL.");
                }
            }
        });

        /* VillageFoundationQuestion: */
        final Syringe vfq = Syringe.getClass("com.wurmonline.server.questions.VillageFoundationQuestion");
        vfq.instrument("parseVillageFoundationQuestion5", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("createVillage")) {
                    mc.replace("$_ = $proceed($$);\n" +
                               "net.spirangle.awakening.players.ChatChannels.getInstance().sendNewVillage($_,getResponder());");
                    logger.info("VillageFoundationQuestion: Send message when new village has been founded.");
                } else if(mc.getMethodName().equals("addHistory")) {
                    if(i == 1) {
                        //						qp.getCtMethod().insertAt(mc.getLineNumber()+1,"net.spirangle.awakening.players.Titles.updateNobilityRank($0);");
                        mc.replace("$_ = $proceed($$);\n" +
                                   "net.spirangle.awakening.players.Titles.updateNobilityRank($0);");
                        logger.info("VillageFoundationQuestion: Give nobility titles for resizing deed.");
                    }
                    ++i;
                }
            }
        });

        /* CaveWallBehaviour: */
        final Syringe cwb = Syringe.getClass("com.wurmonline.server.behaviours.CaveWallBehaviour");
        cwb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;IIZIIISF)Z",
                       new AlignmentModifier(cwb, null, new int[]{ Religion.DEITY_KIANE, Religion.DEITY_QALESHIN }, new float[]{ 0.5f, 0.5f }, 0, "mining"));

        /* WallBehaviour: */
        final Syringe wb = Syringe.getClass("com.wurmonline.server.behaviours.WallBehaviour");
        wb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/structures/Wall;SF)Z",
                      new AlignmentModifier(wb, Religion.DEITY_ASMAN, 1.0f, "building wall"));
        wb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/structures/Wall;SF)Z", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getColor")) {
                    if(i == 8) {
                        wb.getCtMethod().insertAt(mc.getLineNumber(), "com.wurmonline.server.structures.StructureService.examineWall($2,$3);");
                        logger.info("WallBehaviour: Extended examine for walls.");
                    }
                    ++i;
                }
            }
        });

        /* FenceBehaviour: */
        final Syringe fb = Syringe.getClass("com.wurmonline.server.behaviours.FenceBehaviour");
        fb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;ZLcom/wurmonline/server/structures/Fence;SF)Z", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("sendNormalServerMessage")) {
                    if(i == 1) {
                        fb.getCtMethod().insertAt(mc.getLineNumber() + 1, "com.wurmonline.server.structures.StructureService.examineFence($2,$4);");
                        logger.info("FenceBehaviour: Extended examine for fences.");
                    }
                    ++i;
                }
            }
        });

        /* Terraforming: */
        final Syringe terr = Syringe.getClass("com.wurmonline.server.behaviours.Terraforming");
        terr.instrument("plantSprout", new AlignmentModifier(terr, Religion.DEITY_PAYREN, 1.0f, "plant sprout"));
        terr.instrument("plantFlowerbed", new AlignmentModifier(terr, Religion.DEITY_PAYREN, 1.0f, "plant flowerbed"));
        terr.instrument("plantHedge", new AlignmentModifier(terr, Religion.DEITY_PAYREN, 1.0f, "plant hedge"));
        terr.instrument("plantFlower", new AlignmentModifier(terr, Religion.DEITY_PAYREN, 1.0f, "plant flower"));
        terr.instrument("handleChopAction", new AlignmentModifier(terr, "treeAge>=5", Religion.DEITY_SHONIN, 0.5f, "cutting tree"));
        terr.instrument("dig", "(Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;IIIFZLcom/wurmonline/mesh/MeshIO;Z)Z",
                        new AlignmentModifier(terr, Religion.DEITY_EILANDA, 0.5f, "isDirtHeightLower", "dig"));

        /* CombatHandler: */
        final Syringe ch = Syringe.getClass("com.wurmonline.server.creatures.CombatHandler");
        ch.instrument("setKillEffects", new AlignmentModifier(ch, null, new int[]{ Religion.DEITY_SEDAES, Religion.DEITY_SHONIN, Religion.DEITY_MIRIDON },
                                                              new float[]{ 0.5f, 0.5f, -0.5f }, 2, "killing"));

        /* ServerProjectile: */
        final Syringe sp = Syringe.getClass("com.wurmonline.server.combat.ServerProjectile");
        sp.instrument("isOkToAttack", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isLibila")) {
                    mc.replace("$_ = $0.isHateGod();");
                    logger.info("ServerProjectile: Replaced isLibila in isOkToAttack with isHateGod.");
                }
            }
        });

        /* SpellGenerator: */
        final Syringe sg = Syringe.getClass("com.wurmonline.server.spells.SpellGenerator");
        sg.setBody("createSpells", "{ com.wurmonline.server.spells.SpellGeneratorAwakening.createSpells(); }", null);

        /* Communicator: */
        final Syringe comm = Syringe.getClass("com.wurmonline.server.creatures.Communicator");
        comm.setBody("sendHelp", "net.spirangle.awakening.CommandHandler.sendHelp(this);", null);
        comm.setBody("sendDeityHelp", "net.spirangle.awakening.CommandHandler.sendGmHelp(this);", null);
        comm.setBody("sendCaveStrip", "net.spirangle.awakening.misc.HiddenOreFix.sendCaveStrip(this,$$);", null);
        comm.instrument("sendAddToInventory", new IsBulkModifier(comm, "isCrate", "parent", 1));
        comm.instrument("sendUpdateInventoryItem", "(Lcom/wurmonline/server/items/Item;JI)V", new IsBulkModifier(comm, "isCrate", "parent", 1));
        comm.setBody("sendAddPa", "net.spirangle.awakening.players.ChatChannels.sendAddPa(this,$1,$2);", null);
        comm.setBody("sendRemovePa", "net.spirangle.awakening.players.ChatChannels.sendRemovePa(this,$1);", null);
        comm.setBody("drunkGarble", "return $1;", null);
        comm.instrument("sendUpdateKingdomId", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomId")) {
                    mc.replace("$_ = net.spirangle.awakening.players.LoginHandler.getClientKingdomId($0);");
                    logger.info("Communicator: Change kingdom Id to deity number for adepts.");
                }
            }
        });
        comm.instrument("reallyHandle", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("get")) {
                    if(i == 0) {
                        mc.replace("$_ = $proceed($$);if($_==-101) {net.spirangle.awakening.ClientConnection.handlePacket(player,byteBuffer);return;}");
                        logger.info("Communicator: Handle custom client communication.");
                    }
                    ++i;
                }
            }
        });
        comm.instrument("creatureWearableRestrictions", new IsKingdomGuardOrSpiritGuard(comm, -1));
        comm.insertBefore("handleVillageInviteMessage", "if(!net.spirangle.awakening.creatures.CreatureAI.handleVillageInviteMessage($0,$1)) return;", null);

        /* Structure: */
        final Syringe str = Syringe.getClass("com.wurmonline.server.structures.Structure");
        str.instrument("isActionAllowed", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("mayModify")) {
                    mc.replace("$_ = (action==" + Actions.CONTINUE_BUILDING + " || $proceed($$));");
                    logger.info("Structure: Permit continue building inside a house without Manage Building set.");
                }
            }
        });
        str.setBody("canAllowEveryone", "return true;", null);

        /* Door: */
        final Syringe door = Syringe.getClass("com.wurmonline.server.structures.Door");
        door.setBody("canAllowEveryone", "return true;", null);

        /* SelectSpawnQuestion: */
        final Syringe ssq = Syringe.getClass("com.wurmonline.server.questions.SelectSpawnQuestion");
        ssq.instrument("answer", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("setKingdomId")) {
                    mc.replace("$1 = net.spirangle.awakening.players.LoginHandler.playerSpawnKingdom($0,$1);\n" +
                               "$_ = $proceed($$);");
                    logger.info("SelectSpawnQuestion: Set default kingdom to random when none is selected.");
                }
            }
        });

        /* Skill: */
        final Syringe skills = Syringe.getClass("com.wurmonline.server.skills.Skills");
        skills.insertBefore("learn", "(IFZ)Lcom/wurmonline/server/skills/Skill;",
                            "if($1>20000 && $1<30000) return com.wurmonline.server.skills.ModSkills.learn($0,$1,$2,$3);", null);

        /* SkillSystem: */
        final Syringe skillsys = Syringe.getClass("com.wurmonline.server.skills.SkillSystem");
        skillsys.setBody("getNumberOfSkillTemplates", "{ return templateList.size() - com.wurmonline.server.skills.ModSkills.modSkills.length; }", null);

        /* Bless: */
        final Syringe bless = Syringe.getClass("com.wurmonline.server.spells.Bless");
        bless.instrument("precondition", "(Lcom/wurmonline/server/skills/Skill;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/creatures/Creature;)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isLibila")) {
                    mc.replace("$_ = $0.isHateGod();");
                    logger.info("Bless: Make all adepts of black light deities corrupt animals when blessing.");
                }
            }
        });
        bless.setBody("blessCreature", "{ com.wurmonline.server.spells.ModBless.blessCreature($1,$2); }", null);

        /* Kingdoms: */
        final Syringe kingdoms = Syringe.getClass("com.wurmonline.server.kingdom.Kingdoms");
        kingdoms.instrument("addTower", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("addTowerKingdom")) {
                    mc.replace("net.spirangle.awakening.kingdom.GuardTowers.addGuardTower($1);\n" +
                               "$_ = $proceed($$);");
                    logger.info("addTower: Handle creating a guard tower.");
                }
            }
        });

        /* Village: */
        final Syringe village = Syringe.getClass("com.wurmonline.server.villages.Village");
        village.insertBefore("addTarget", "if(!net.spirangle.awakening.creatures.CreatureAI.isVillageEnemy($0,$1)) return;", null);
        village.instrument("isActionAllowed", "(SLcom/wurmonline/server/creatures/Creature;ZII)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isThisAPvpServer")) {
                    mc.replace("$_ = creature.isOnPvPServer();");
                    logger.info("isActionAllowed: Use village permissions inside PvE zones.");
                }
            }
        });
        village.instrument("mayAttack", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isThisAPvpServer")) {
                    mc.replace("$_ = attacker.isOnPvPServer();");
                    logger.info("mayAttack: Use village permissions inside PvE zones.");
                }
            }
        });

        /* GuardPlan: */
        final Syringe gp = Syringe.getClass("com.wurmonline.server.villages.GuardPlan");
        gp.instrument("pollGuards", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("doNew")) {
                    mc.replace("$_ = com.wurmonline.server.creatures.Creature.doNew(net.spirangle.awakening.creatures.VillageGuard.getTemplateId($8),$2,$3,$4,$5,$6,$7,$8);");
                    logger.info("pollGuards: Replace spirit templars with custom village guards.");
                }
            }
        });

        /* Message: */
        final Syringe message = Syringe.getClass("com.wurmonline.server.Message");
        message.setBody("isHelpChannel", "return $1.equals(\"Help\");", null);

        /* MailSendQuestion: */
        final Syringe msq = Syringe.getClass("com.wurmonline.server.questions.MailSendQuestion");
        msq.instrument("answer", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("getKingdomForPlayer") || mc.getMethodName().equals("getKingdomId")) {
                    mc.replace("$_ = (byte)1;");
                    logger.info("answer: Remove block against sending mail to other kingdoms.");
                }
            }
        });

        /* SelectSpellQuestion: */
        final Syringe sspq = Syringe.getClass("com.wurmonline.server.questions.SelectSpellQuestion");
        sspq.instrument("sendQuestion", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isCustomDeity")) {
                    mc.replace("$_ = false;");
                    logger.info("sendQuestion: Make all deities non-custom.");
                }
            }
        });

        /* ModCommHandler: */
        final Syringe mch = Syringe.getClass("org.gotti.wurmunlimited.modcomm.ModCommHandler");
        mch.insertBefore("handlePacketChannels", "net.spirangle.awakening.ClientConnection.handleClientModHandshake($1);", null);

        /* MethodsHighways: */
        final Syringe mhw = Syringe.getClass("com.wurmonline.server.highways.MethodsHighways");
        mhw.insertBefore("canPlantMarker", "if(!net.spirangle.awakening.zones.Tiles.canPlantMarker($1,$3)) return false;", null);

        /* PathFinder: */
        final Syringe pf = Syringe.getClass("com.wurmonline.server.creatures.ai.PathFinder");
        pf.instrument("startAstar", new IsKingdomGuardOrSpiritGuard(pf, 1));
        /* StaticPathFinder: */
        final Syringe spf = Syringe.getClass("com.wurmonline.server.creatures.ai.StaticPathFinder");
        spf.instrument("startAstar", new IsKingdomGuardOrSpiritGuard(spf, 1));
        /* StaticPathFinderAgg: */
        final Syringe spfa = Syringe.getClass("com.wurmonline.server.creatures.ai.StaticPathFinderAgg");
        spfa.instrument("startAstar", new IsKingdomGuardOrSpiritGuard(spfa, 1));
        /* StaticPathFinderNPC: */
        final Syringe spfn = Syringe.getClass("com.wurmonline.server.creatures.ai.StaticPathFinderNPC");
        spfn.instrument("startAstar", new IsKingdomGuardOrSpiritGuard(spfn, 1));

        /* FenceGate: */
        final Syringe fg = Syringe.getClass("com.wurmonline.server.structures.FenceGate");
        fg.instrument("canBeOpenedBy", new IsKingdomGuardOrSpiritGuard(fg, -1));

        /* Cults: */
        final Syringe cults = Syringe.getClass("com.wurmonline.server.players.Cults");
        cults.instrument("meditate", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("skillCheck")) {
                    mc.replace("$_ = $proceed($1,$2,$3,$4,$5,false,2.0);");
                    logger.info("meditate: Change meditation to use the old skill system.");
                }
            }
        });

        /* BodyPartBehaviour: */
        final Syringe bpb = Syringe.getClass("com.wurmonline.server.behaviours.BodyPartBehaviour");
        bpb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;SF)Z", new ExprEditor() {
            int i = 0;

            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("sendNormalServerMessage")) {
                    if(i == 2) {
                        mc.replace("net.spirangle.awakening.kingdom.Kingdoms.sendKingdomName(performer,true);");
                        logger.info("action: Change kingdom names of players to server kingdoms.");
                    }
                    ++i;
                }
            }
        });

        /* SwapDeityQuestion: */
        final Syringe sdq = Syringe.getClass("com.wurmonline.server.questions.SwapDeityQuestion");
        sdq.setBody("answer", "com.wurmonline.server.questions.SwapDeityAnswer.answer($0,$1);", null);

        /* Locates: */
        final Syringe loc = Syringe.getClass("com.wurmonline.server.behaviours.Locates");
        loc.insertAfter("locateSpring", "com.wurmonline.server.skills.Divination.locateSpring($1,$2,$3);", null);
        loc.insertAfter("useLocateItem", "com.wurmonline.server.skills.Divination.useLocateItem($1,$2,$3);", null);

        /* PlanterBehaviour: */
        final Syringe pb = Syringe.getClass("com.wurmonline.server.behaviours.PlanterBehaviour");
        pb.instrument("getBehavioursFor", "(Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/items/Item;)Ljava/util/List;", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isRaw") || mc.getMethodName().equals("isPStateNone")) {
                    mc.replace("$_ = $proceed($$) || source.getTemplate().getTemplateId()==440;");
                    logger.info("getBehavioursFor: Make woad potable.");
                }
            }
        });
        pb.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/items/Item;SF)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("isSpice") || mc.getMethodName().equals("isFresh")) {
                    mc.replace("$_ = $proceed($$) || source.getTemplate().getTemplateId()==440;");
                    logger.info("getBehavioursFor: Make woad potable.");
                }
            }
        });

        /* Seat: */
        final Syringe seat = Syringe.getClass("com.wurmonline.server.behaviours.Seat");
        seat.insertBefore("occupy", "net.spirangle.awakening.items.Vehicle.occupySeat($0,$1,$2);", null);

        /* Floor: */
        final Syringe floor = Syringe.getClass("com.wurmonline.server.structures.Floor");
        floor.setBody("loadAllFloors", "com.wurmonline.server.structures.StructureService.loadAllFloors();", null);

        /* FloorBehaviour: */
        final Syringe fb2 = Syringe.getClass("com.wurmonline.server.behaviours.FloorBehaviour");
        fb2.insertAfter("examine", "com.wurmonline.server.structures.StructureService.examineFloor($1,$2);", null);

        /* BridgePart: */
        final Syringe bp = Syringe.getClass("com.wurmonline.server.structures.BridgePart");
        bp.setBody("loadAllBridgeParts", "com.wurmonline.server.structures.StructureService.loadAllBridgeParts();", null);

        /* BridgePartBehaviour: */
        final Syringe bpb2 = Syringe.getClass("com.wurmonline.server.behaviours.BridgePartBehaviour");
        bpb2.insertAfter("examine", "com.wurmonline.server.structures.StructureService.examineBridgePart($1,$2);", null);

        /* AchievementGenerator: */
        final Syringe ag = Syringe.getClass("com.wurmonline.server.players.AchievementGenerator");
        ag.insertAfter("generateAchievements", "net.spirangle.awakening.players.Achievements.getInstance().init();", null);

        /* UnfinishedItemBehaviour: */
        final Syringe uib = Syringe.getClass("com.wurmonline.server.behaviours.UnfinishedItemBehaviour");
        uib.instrument("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/items/Item;Lcom/wurmonline/server/items/Item;SF)Z", new ExprEditor() {
            @Override
            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("cont")) {
                    mc.replace("$_ = net.spirangle.awakening.items.Items.handleFinishedAdvancedCreationEntry($0,$proceed($$));");
                    logger.info("UnfinishedItemBehaviour: Handle when creating item is finished with advanced creation entry.");
                }
            }
        });

        /* MeshIO: */
        final Syringe meshio = Syringe.getClass("com.wurmonline.mesh.MeshIO");
        meshio.setBody("calcDistantTerrain", "{" +
                                             "this.distantTerrainTypes = new byte[this.size*this.size/256];" +
                                             "net.spirangle.awakening.zones.DistantRendering.calcDistantTerrainForest(this);" +
                                             "}", null);
    }
}
