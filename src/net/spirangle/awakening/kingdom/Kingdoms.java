package net.spirangle.awakening.kingdom;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.shared.util.StringUtilities;

import static net.spirangle.awakening.AwakeningConstants.*;


@SuppressWarnings("unused")
public class Kingdoms {
    public static void sendKingdomName(final Creature creature, final boolean self) {
        if(!creature.isPlayer()) return;
        byte k = creature.getKingdomId();
        String kingdom;
        if(k == 1) kingdom = "from Tirvalon.";
        else if(k == 2) kingdom = "of Cellimdaric heritage.";
        else if(k == 3) kingdom = "a Shodroqi.";
        else if(k == 4) kingdom = "a Thekandrian.";
        else kingdom = "not allied to anyone.";
        String message = self? "You are " : creature.getNameWithGenus() + " is ";
        creature.getCommunicator().sendNormalServerMessage(message + kingdom);
    }

    public static void sendKingdomBloodName(final Creature creature) {
        if(!creature.isPlayer()) return;
        byte b = creature.getBlood();
        String blood;
        if(b == TIRVALON) blood = "have blood from Tirvalon.";
        else if(b == CELLIMDAR) blood = "have Cellimdaric blood.";
        else if(b == SHODROQ) blood = "be of a Shodroqi bloodline.";
        else if(b == THEKANDRE) blood = "have the blood of a Thekandrian.";
        else blood = "no discernable bloodline.";
        String message = StringUtilities.raiseFirstLetter(creature.getHeSheItString()) + " seems to ";
        creature.getCommunicator().sendNormalServerMessage(message + blood);
    }
}
