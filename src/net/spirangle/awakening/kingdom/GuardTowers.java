package net.spirangle.awakening.kingdom;

import com.wurmonline.server.Message;
import com.wurmonline.server.NoSuchPlayerException;
import com.wurmonline.server.Players;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.GuardTower;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import com.wurmonline.server.zones.FocusZone;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.players.EconomyData;
import net.spirangle.awakening.players.PlayersData;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class GuardTowers {

    private static final Logger logger = Logger.getLogger(GuardTowers.class.getName());

    @SuppressWarnings("unused")
    private static Map<Item, GuardTower> towers;

    @SuppressWarnings("unchecked")
    public static void init() {
        try {
            Field field = Kingdoms.class.getDeclaredField("towers");
            field.setAccessible(true);
            towers = (Map<Item, GuardTower>) field.get(null);
        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Could not retrieve towers field in class Kingdoms: " + e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused")
    public static void addGuardTower(Item tower) {
        long creator = tower.getLastOwnerId();
        logger.info("addGuardTower: " + creator);
        try {
            Player player = Players.getInstance().getPlayer(creator);
            logger.info("addGuardTower: " + player.getName());
            ChatChannels.getInstance().sendNewGuardTower(player, tower);

            int x = tower.getTileX();
            int y = tower.getTileY();
            int z = 100;
            List<FocusZone> zones = Arrays.asList(FocusZone.getAllZones()).stream()
                                          .filter(fz -> fz.isNonPvP() && x + z >= fz.getStartX() && y + z >= fz.getStartY() && x - z <= fz.getEndX() && y - z <= fz.getEndY())
                                          .collect(Collectors.toList());
            if(!zones.isEmpty()) {
                byte k = tower.getKingdom();
                boolean convert = true;
                Village c = null;
                for(Village v : Villages.getVillages())
                    if(v.isPermanent && zones.stream().anyMatch(fz -> fz.covers(v.getTokenX(), v.getTokenY()))) {
                        if(v.kingdom == k) {
                            convert = false;
                            break;
                        }
                        if(c == null || (Math.abs(x - v.getTokenX()) < Math.abs(x - c.getTokenX()) && Math.abs(y - v.getTokenY()) < Math.abs(y - c.getTokenY()))) {
                            c = v;
                        }
                    }
                if(convert && c != null) {
                    tower.setAuxData(c.kingdom);
                    Kingdom kingdom = Kingdoms.getKingdom(c.kingdom);
                    player.getCommunicator().sendAlertServerMessage("The guard tower was raised within the borders of " + kingdom.getName() + " and is now a part of this kingdom.");
                    return;
                }
            }

            int salary = Config.kingdomGuardTowerSalary[player.getKingdomId()];
            if(salary >= 0) {
                EconomyData economyData = PlayersData.getInstance().getBankAccount(player);
                if(economyData.paySalary(player, salary)) {
                    String m = new Change(salary).getChangeString();
                    String k = Kingdoms.getNameFor(player.getKingdomId());
                    logger.info("addGuardTower: " + k + " - " + m);
                    player.getCommunicator().sendSafeServerMessage("For the effort of raising this guard tower, the kingdom of " + k + " pays you a salary of " + m + ".", Message.KINGDOM);
                }
            }
        } catch(NoSuchPlayerException e) { }
    }
}
