package net.spirangle.awakening.misc;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.Constants;
import com.wurmonline.server.Features;
import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.zones.Water;
import com.wurmonline.server.zones.Zones;

import java.nio.ByteBuffer;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class HiddenOreFix {

    private static final Logger logger = Logger.getLogger(HiddenOreFix.class.getName());

    private static final int emptyRock = Tiles.encode((short) -100, Tiles.Tile.TILE_CAVE_WALL.id, (byte) 0);

    public static void sendCaveStrip(Communicator communicator, short xStart, short yStart, int width, int height) {
        Player player = communicator.getPlayer();
        if(player != null && player.hasLink()) {
            try {
                ByteBuffer bb = communicator.getConnection().getBuffer();
                bb.put((byte) 102);
                bb.put((byte) (Features.Feature.CAVEWATER.isEnabled()? 1 : 0));
                bb.put((byte) (player.isSendExtraBytes()? 1 : 0));
                bb.putShort(xStart);
                bb.putShort(yStart);
                bb.putShort((short) width);
                bb.putShort((short) height);
                boolean onSurface = player.isOnSurface();
                for(int x = 0; x < width; ++x) {
                    for(int y = 0; y < height; ++y) {
                        int xx = xStart + x;
                        int yy = yStart + y;
                        if(xx < 0 || xx >= Zones.worldTileSizeX || yy < 0 || yy >= Zones.worldTileSizeY) {
                            bb.putInt(emptyRock);
                            xx = 0;
                            yy = 0;
                        } else if(!onSurface) {
                            if(!(Tiles.decodeType(Server.caveMesh.getTile(xx, yy)) == Tiles.Tile.TILE_CAVE_EXIT.id) &&
                               isSurroundedByCaveWalls(xx, yy)) {
                                bb.putInt(getDummyWall(xx, yy));
                            } else {
                                bb.putInt(Server.caveMesh.data[xx | yy << Constants.meshSize]);
                            }
                        } else if(Tiles.isSolidCave(Tiles.decodeType(Server.caveMesh.data[xx | yy << Constants.meshSize]))) {
                            bb.putInt(getDummyWall(xx, yy));
                        } else {
                            bb.putInt(Server.caveMesh.data[xx | yy << Constants.meshSize]);
                        }
                        if(Features.Feature.CAVEWATER.isEnabled()) {
                            bb.putShort((short) Water.getCaveWater(xx, yy));
                        }
                        if(!communicator.player.isSendExtraBytes()) continue;
                        bb.put(Server.getClientCaveFlags(xx, yy));
                    }
                }
                communicator.getConnection().flush();
            } catch(Exception ex) {
                player.setLink(false);
            }
        }
    }

    private static boolean isSurroundedByCaveWalls(int tilex, int tiley) {
        return isCaveWall(tilex + 1, tiley) && isCaveWall(tilex - 1, tiley) &&
               isCaveWall(tilex, tiley + 1) && isCaveWall(tilex, tiley - 1);
    }

    private static int getDummyWall(int tilex, int tiley) {
        return Tiles.encode(
                Tiles.decodeHeight(Server.caveMesh.data[tilex | tiley << Constants.meshSize]),
                Tiles.Tile.TILE_CAVE_WALL.id,
                Tiles.decodeData(Server.caveMesh.data[tilex | tiley << Constants.meshSize])
        );
    }

    private static boolean isCaveWall(int xx, int yy) {
        if(xx < 0 || xx >= Zones.worldTileSizeX || yy < 0 || yy >= Zones.worldTileSizeY) return true;
        return Tiles.isSolidCave(Tiles.decodeType(Server.caveMesh.data[xx | yy << Constants.meshSize]));
    }
}
