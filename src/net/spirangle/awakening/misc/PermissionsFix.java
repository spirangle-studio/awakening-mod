package net.spirangle.awakening.misc;

import com.wurmonline.server.items.Item;

import java.util.logging.Logger;


@SuppressWarnings("unused")
public class PermissionsFix {

    private static final Logger logger = Logger.getLogger(PermissionsFix.class.getName());

    public static boolean canLoadCargo(Item target) {
        return !target.isNoMove();
    }

    public static boolean isNoEatOrDrink(Item target) {
        return target.isIndestructible();
    }
}
