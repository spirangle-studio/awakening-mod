package net.spirangle.awakening.misc;

import com.wurmonline.server.villages.Village;
import javassist.*;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;
import javassist.expr.MethodCall;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PvpServerFix {

    private static final Logger logger = Logger.getLogger(PvpServerFix.class.getName());

    public static void preInit() {
        try {

            ClassPool classPool = HookManager.getInstance().getClassPool();

            CtClass ctBrand = classPool.get("com.wurmonline.server.creatures.Brand");
            removePvpCheck(ctBrand, ctBrand.getDeclaredMethod("addInitialPermissions"));

            CtClass ctCreatures = classPool.get("com.wurmonline.server.creatures.Creatures");
            removePvpCheck(ctCreatures, ctCreatures.getDeclaredMethod("getManagedAnimalsFor"));
            removePvpCheck(ctCreatures, ctCreatures.getDeclaredMethod("getManagedWagonersFor"));
            removePvpCheck(ctCreatures, ctCreatures.getDeclaredMethod("getMayUseWagonersFor"));

            CtClass ctCreature = classPool.get("com.wurmonline.server.creatures.Creature");
            removePvpCheck(ctCreature, ctCreature.getDeclaredMethod("loadTemplate"));
            removePvpCheck(ctCreature, ctCreature.getDeclaredMethod("checkBreedCounter"));
            removePvpCheck(ctCreature, ctCreature.getDeclaredMethod("die"), new int[]{ 1 });
            removePvpCheck(ctCreature, ctCreature.getDeclaredMethod("getKingdomId"));
            removePvpCheck(ctCreature, ctCreature.getDeclaredMethod("resetBreedCounter"));

            CtClass ctCreatureStatus = classPool.get("com.wurmonline.server.creatures.CreatureStatus");
            removePvpField(ctCreatureStatus, ctCreatureStatus.getDeclaredMethod("pollAge"));

            CtClass ctCommunicator = classPool.get("com.wurmonline.server.creatures.Communicator");
            removePvpCheck(ctCommunicator, ctCommunicator.getDeclaredMethod("reallyHandle_CMD_MOVE_INVENTORY"));
            removePvpCheck(ctCommunicator, ctCommunicator.getDeclaredMethod("equipCreatureCheck"));

            CtClass ctVillageCitizenManageQuestion = classPool.get("com.wurmonline.server.questions.VillageCitizenManageQuestion");
            removePvpCheck(ctVillageCitizenManageQuestion, ctVillageCitizenManageQuestion.getDeclaredMethod("sendQuestion"));

            CtClass ctVillageFoundationQuestion = classPool.get("com.wurmonline.server.questions.VillageFoundationQuestion");
            removePvpCheck(ctVillageFoundationQuestion, ctVillageFoundationQuestion.getDeclaredMethod("addCitizenMultiplier"));
            removePvpCheck(ctVillageFoundationQuestion, ctVillageFoundationQuestion.getDeclaredMethod("sendQuestion6"));

            CtClass ctManageObjectList = classPool.get("com.wurmonline.server.questions.ManageObjectList");
            removePvpCheck(ctManageObjectList, ctManageObjectList.getDeclaredMethod("answer"));
            removePvpCheck(ctManageObjectList, ctManageObjectList.getDeclaredMethod("sendQuestion"));

            CtClass ctItem = classPool.get("com.wurmonline.server.items.Item");
            removePvpCheck(ctItem, ctItem.getDeclaredMethod("moveToItem"));
            removePvpField(ctItem, ctItem.getDeclaredMethod("moveToItem"));
            removePvpField(ctItem, ctItem.getDeclaredMethod("sendWear"));

            CtClass ctMethodsCreatures = classPool.get("com.wurmonline.server.behaviours.MethodsCreatures");
            removePvpCheck(ctMethodsCreatures, ctMethodsCreatures.getDeclaredMethod("brand"));

            CtClass ctCreatureBehaviour = classPool.get("com.wurmonline.server.behaviours.CreatureBehaviour");
            removePvpCheck(ctCreatureBehaviour, ctCreatureBehaviour.getDeclaredMethod("addVehicleOptions"), new int[]{ 2 });
            removePvpCheck(ctCreatureBehaviour, ctCreatureBehaviour.getMethod("action", "(Lcom/wurmonline/server/behaviours/Action;Lcom/wurmonline/server/creatures/Creature;Lcom/wurmonline/server/creatures/Creature;SF)Z"));

            CtClass ctManageMenu = classPool.get("com.wurmonline.server.behaviours.ManageMenu");
            removePvpCheck(ctManageMenu, ctManageMenu.getDeclaredMethod("getBehavioursFor"));
            removePvpCheck(ctManageMenu, ctManageMenu.getDeclaredMethod("action"));

            CtClass ctMethodsItems = classPool.get("com.wurmonline.server.behaviours.MethodsItems");
            removePvpCheck(ctMethodsItems, ctMethodsItems.getDeclaredMethod("getTransmutationMod"));

            CtClass ctTileRockBehaviour = classPool.get("com.wurmonline.server.behaviours.TileRockBehaviour");
            removePvpField(ctTileRockBehaviour, ctTileRockBehaviour.getDeclaredMethod("cannotMineSlope"));

            CtClass ctCaveTileBehaviour = classPool.get("com.wurmonline.server.behaviours.CaveTileBehaviour");
            removePvpField(ctCaveTileBehaviour, ctCaveTileBehaviour.getDeclaredMethod("raiseRockLevel"));

        } catch(NotFoundException e) {
            logger.log(Level.SEVERE, "Error editing functions:", e);
            throw new HookException(e);
        }
    }

    public static void init() {
        try {
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);

            Field OPTIMUMCRETRATIO = Village.class.getDeclaredField("OPTIMUMCRETRATIO");
            OPTIMUMCRETRATIO.setAccessible(true);
            modifiersField.setInt(OPTIMUMCRETRATIO, OPTIMUMCRETRATIO.getModifiers() & ~Modifier.FINAL);
            OPTIMUMCRETRATIO.set(null, 15.0f);
        } catch(Exception e) {
            logger.log(Level.SEVERE, "PvpServerFix: Failed to update OPTIMUMCRETRATIO: " + e.getMessage(), e);
        }
    }

    private static void removePvpCheck(CtClass cl, CtMethod m) {
        if(cl == null || m == null) return;
        try {
            m.instrument(new ExprEditor() {
                public void edit(MethodCall mc) throws CannotCompileException {
                    if(mc.getMethodName().equals("isThisAPvpServer")) {
                        mc.replace("$_ = false;");
                        logger.info("Rmoved PvP block from " + cl.getName() + "." + m.getName() + "() [" + mc.getLineNumber() + "]: isThisAPvpServer");
                    }
                }
            });
        } catch(CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new HookException(e);
        }
    }

    private static void removePvpCheck(CtClass cl, CtMethod m, int[] index) {
        if(cl == null || m == null || index == null) return;
        try {
            m.instrument(new ExprEditor() {
                int i = 0;

                public void edit(MethodCall mc) throws CannotCompileException {
                    if(mc.getMethodName().equals("isThisAPvpServer")) {
                        for(int n = index.length - 1; n >= 0; --n)
                            if(index[n] == i) {
                                mc.replace("$_ = false;");
                                logger.info("Rmoved PvP block from " + cl.getName() + "." + m.getName() + "() [" + mc.getLineNumber() + "]: isThisAPvpServer");
                            }
                        ++i;
                    }
                }
            });
        } catch(CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new HookException(e);
        }
    }

    private static void removePvpField(CtClass cl, CtMethod m) {
        if(cl == null || m == null) return;
        try {
            m.instrument(new ExprEditor() {
                public void edit(FieldAccess fa) throws CannotCompileException {
                    if(fa.getFieldName().equals("PVPSERVER")) {
                        fa.replace("$_ = false;");
                        logger.info("Rmoved PvP block from " + cl.getName() + "." + m.getName() + "() [" + fa.getLineNumber() + "]: PVPSERVER");
                    }
                }
            });
        } catch(CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new HookException(e);
        }
    }

    @SuppressWarnings("unused")
    private static void removePvpField(CtClass cl, CtMethod m, int[] index) {
        if(cl == null || m == null || index == null) return;
        try {
            m.instrument(new ExprEditor() {
                int i = 0;

                public void edit(FieldAccess fa) throws CannotCompileException {
                    if(fa.getFieldName().equals("PVPSERVER")) {
                        for(int n = index.length - 1; n >= 0; --n)
                            if(index[n] == i) {
                                fa.replace("$_ = false;");
                                logger.info("Rmoved PvP block from " + cl.getName() + "." + m.getName() + "() [" + fa.getLineNumber() + "]: PVPSERVER");
                            }
                        ++i;
                    }
                }
            });
        } catch(CannotCompileException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new HookException(e);
        }
    }
}
