package net.spirangle.awakening.misc;

import com.wurmonline.server.items.ItemList;
import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.Opcode;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import net.spirangle.awakening.util.BytecodeTools;
import org.gotti.wurmunlimited.modloader.classhooks.CodeReplacer;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class FireplaceFix {

    private static final Logger logger = Logger.getLogger(FireplaceFix.class.getName());

    public static void preInit() {
        try {
            CtClass itemCt = HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item");
            CtMethod coolOutSideItemCt = itemCt.getDeclaredMethod("coolOutSideItem");
            BytecodeTools find = new BytecodeTools(itemCt.getClassFile().getConstPool());
            find.addIload(4);
            find.addInteger(ItemList.forge);
            find.codeBranching(Opcode.IF_ICMPEQ, 19);
            find.addIload(4);
            find.addInteger(ItemList.kiln);
            find.codeBranching(Opcode.IF_ICMPEQ, 11);
            find.addIload(4);
            find.addInteger(ItemList.smelter);
            BytecodeTools replace = new BytecodeTools(itemCt.getClassFile().getConstPool());
            replace.addIload(4);
            replace.addInteger(ItemList.forge);
            replace.codeBranching(Opcode.IF_ICMPEQ, 27);
            replace.addIload(4);
            replace.addInteger(ItemList.kiln);
            replace.codeBranching(Opcode.IF_ICMPEQ, 19);
            replace.addIload(4);
            replace.addInteger(ItemList.smelter);
            replace.codeBranching(Opcode.IF_ICMPEQ, 11);
            replace.addIload(4);
            replace.addInteger(ItemList.openFireplace);
            CodeReplacer codeReplacer = new CodeReplacer(coolOutSideItemCt.getMethodInfo().getCodeAttribute());

            codeReplacer.replaceCode(find.get(), replace.get());
            coolOutSideItemCt.getMethodInfo().rebuildStackMapIf6(HookManager.getInstance().getClassPool(), itemCt.getClassFile());

            coolOutSideItemCt.instrument(new ExprEditor() {
                @Override
                public void edit(MethodCall methodCall) throws CannotCompileException {
                    if(Objects.equals("isFireplace", methodCall.getMethodName())) {
                        methodCall.replace("$_ = false;");
                    }
                }
            });
            logger.info("Make fireplace behave like a forge.");
        } catch(NotFoundException | CannotCompileException | BadBytecode e) {
            logger.log(Level.SEVERE, "FireplaceFix: " + e.getMessage(), e);
        }
    }
}
