package net.spirangle.awakening.zones;

import com.wurmonline.math.TilePos;
import com.wurmonline.mesh.Tiles;
import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.MethodsItems;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplateCreator;
import com.wurmonline.server.economy.Economy;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wurmonline.server.MiscConstants.*;
import static net.spirangle.awakening.AwakeningConstants.moonMetals;

public class Treasures {

    private static final Logger logger = Logger.getLogger(Treasures.class.getName());

    private static Treasures instance = null;

    public static Treasures getInstance() {
        if(instance == null) instance = new Treasures();
        return instance;
    }

    private final Queue<TilePos> treasureLocations = new LinkedList<>();
    private int mapFragments = 0;
    private final Map<Long, Treasure> treasures = new HashMap<>();
    private final Map<Long, Treasure> creatures = new HashMap<>();
    private ReentrantReadWriteLock rwLock;
    private Set<Item> hiddenItems;

    private Treasures() {
    }

    public void init() {
        ReentrantReadWriteLock rwLock = null;
        Set<Item> hiddenItems = null;
        try {
            Field rwLockField = Items.class.getDeclaredField("HIDDEN_ITEMS_RW_LOCK");
            rwLockField.setAccessible(true);
            rwLock = (ReentrantReadWriteLock) rwLockField.get(null);
            Field hiddenItemsField = Items.class.getDeclaredField("hiddenItems");
            hiddenItemsField.setAccessible(true);
            //noinspection unchecked
            hiddenItems = (Set<Item>) hiddenItemsField.get(null);
        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        this.rwLock = rwLock;
        this.hiddenItems = hiddenItems;
    }

    public void poll() {
        if(treasureLocations.size() < 10) {
            TilePos tilePos = getRandomTile();
            if(tilePos != null)
                treasureLocations.add(tilePos);
        }
    }

    public Item createRandomMapFragment() {
        float ql = 1.0f + 79.0f * Server.rand.nextFloat();
        byte rarity = MiscConstants.COMMON;
        byte aux = (byte) (ql / 10);
        int data = 1 << Server.rand.nextInt(8);
        try {
            Item mapFragment = ItemFactory.createItem(ItemTemplateCreatorAwakening.mapFragment, ql, (byte) 0, rarity, null);
            mapFragment.setAuxData(aux);
            mapFragment.setData1(data);
            return mapFragment;
        } catch(NoSuchTemplateException | FailedException e) { }
        return null;
    }

    public Treasure createRandomTreasure(Item map) {
        TilePos center = treasureLocations.poll();
        if(center == null) {
            center = getRandomTile();
        }
        if(center != null) {
            int tile = Server.surfaceMesh.getTile(center.x, center.y);
            int height = Tiles.decodeHeight(tile);
            int depth = 5;
            return createTreasure(map, center.x, center.y, height - depth, map.getAuxData());
        }
        return null;
    }

    private TilePos getRandomTile() {
        for(int i = 0; i < 30; ++i) {
            int x = Server.rand.nextInt(Zones.worldTileSizeX);
            int y = Server.rand.nextInt(Zones.worldTileSizeY);
            if(canCreateAtTile(x, y, 5)) {
                TilePos pos = new TilePos();
                pos.set(x, y);
                return pos;
            }
        }
        return null;
    }

    private boolean canCreateAtTile(int x, int y, int depth) {
        int tile = Server.surfaceMesh.getTile(x, y);
        int height = Tiles.decodeHeight(tile);
        if(height < 1 || height > 3000) return false;
        int rock = Tiles.decodeHeight(Server.rockMesh.getTile(x, y));
        if(height - rock <= depth) return false;
        if(getTreasure(x, y, 10) != null) return false;
        Village deed = Villages.getVillagePlus(x, y, true, 10);
        if(deed != null) return false;
        short[] steepness = Creature.getTileSteepness(x, y, true);
        if(steepness[0] > 20 && steepness[1] > 20) return false;
        VolaTile vt = Zones.getTileOrNull(x, y, true);
        return vt == null || vt.getStructure() == null;
    }

    public Treasure createTreasure(Item map, int tileX, int tileY, int height, byte type) {
        try {
            byte rarity = RARE;
            if(type > 4) rarity = SUPREME;
            Item chest = ItemFactory.createItem(ItemList.treasureChest, 30, rarity, null);
            chest.setAuxData(type);

            fillTreasureChest(chest, type);

            rwLock.writeLock().lock();
            try {
                hiddenItems.add(chest);
            } finally {
                rwLock.writeLock().unlock();
            }
            chest.setHidden(true);

            chest.putInVoid();
            chest.setPosX((tileX << 2) + 2);
            chest.setPosY((tileY << 2) + 2);
            chest.setPosZ(height / 10.0f);

            VolaTile vt = Zones.getOrCreateTile(tileX, tileY, true);
            int zoneId = vt.getZone().getId();
            chest.setZoneId(zoneId, true);

            TilePos center = new TilePos();
            center.set(tileX, tileY);
            Treasure treasure = new Treasure(chest.getWurmId(), map.getWurmId(), center, type, 0);
            treasures.put(chest.getWurmId(), treasure);
            treasures.put(map.getWurmId(), treasure);
            return treasure;
        } catch(FailedException | NoSuchTemplateException e) {
            logger.log(Level.WARNING, "Failed creating treasure chest: " + e.getMessage(), e);
            return null;
        }
    }

    public Treasure getTreasure(Item item) {
        Treasure treasure = getTreasure(item.getWurmId());
        if(treasure == null) treasure = getTreasure(item.getTileX(), item.getTileY());
        return treasure;
    }

    public Treasure getTreasure(long wurmId) {
        return treasures.get(wurmId);
    }

    public Treasure getTreasure(int tileX, int tileY) {
        return getTreasure(tileX, tileY, 0);
    }

    public Treasure getTreasure(int tileX, int tileY, int distance) {
        int x, y, d;
        for(Treasure treasure : treasures.values()) {
            x = Math.abs(tileX - treasure.center.x);
            y = Math.abs(tileY - treasure.center.y);
            d = distance;
            if((x == 0 && y <= d) || (y == 0 && x <= d) || (int) Math.sqrt(x * x + y * y) <= d)
                return treasure;
        }
        return null;
    }

    public Treasure getTreasure(Creature creature) {
        return creatures.get(creature.getWurmId());
    }

    void removeTreasure(Treasure treasure) {
        treasures.remove(treasure.chestId);
        treasures.remove(treasure.mapId);
    }

    public void addMapFragment(Item item) {
        if(item.getTemplate().getTemplateId() == ItemTemplateCreatorAwakening.mapFragment) {
            ++mapFragments;
            int data = item.getData1();
            int weight = item.getWeightGrams(false);
            if(weight == 10 && (data == 3 || data == 5 || data == 6 || data == 7)) {
                if(data == 3) data = 0x10;
                else if(data == 5) data = 0x20;
                else if(data == 6) data = 0x40;
                else if(data == 7) data = 0x80;
                item.setData1(data);
            }
        }
    }

    public void removeMapFragment(Item item) {
        if(item.getTemplate().getTemplateId() == ItemTemplateCreatorAwakening.mapFragment) {
            --mapFragments;
            Items.destroyItem(item.getWurmId());
        }
    }

    public Item getMapFragment(Creature creature) {
        return null;
    }

    public void addTreasureMap(Item item) {
        if(item.getTemplate().getTemplateId() == ItemTemplateCreatorAwakening.treasureMap) {
            long chestId = item.getData();
            Treasure treasure = getTreasure(chestId);
            if(treasure == null && chestId != -10L) {
                try {
                    Item chest = Items.getItem(chestId);
                    TilePos center = new TilePos();
                    center.set(chest.getTileX(), chest.getTileY());
                    treasure = new Treasure(chest.getWurmId(), item.getWurmId(), center, item.getAuxData(), 0);
                    treasures.put(chest.getWurmId(), treasure);
                    treasures.put(item.getWurmId(), treasure);
                } catch(NoSuchItemException e) { }
            }
        }
    }

    void addCreature(Treasure treasure, Creature creature) {
        addCreature(treasure, creature.getWurmId());
    }

    void addCreature(Treasure treasure, long wurmId) {
        creatures.put(wurmId, treasure);
    }

    void removeCreature(Creature creature) {
        removeCreature(creature.getWurmId());
    }

    void removeCreature(long wurmId) {
        creatures.remove(wurmId);
    }

    public boolean creatureKilled(Creature creature, Map<Long, Long> attackers) {
        if(creature == null) return false;
        Treasure treasure = getTreasure(creature);
        if(treasure == null) return false;
        treasure.creatureKilled(creature, attackers);
        removeCreature(creature);
        return true;
    }

    private static final int[] templateTypes1 = {
            ItemTemplateCreatorAwakening.servantTask,
            ItemList.ironBar,
            ItemList.copperBar,
            ItemList.tinBar,
            ItemList.zincBar,
            ItemList.leadBar,
            ItemList.silverBar,
            ItemList.goldBar,
            ItemList.steelBar,
            ItemList.brassBar,
            ItemList.bronzeBar,
            ItemList.leather,
            ItemList.cotton,
            ItemList.clay,
            ItemList.charcoal,
            ItemList.wemp,
            ItemList.whetStone
    };

    private static final int[] templateTypes2 = {
            ItemTemplateCreatorAwakening.diamondLens,
            ItemList.emerald,
            ItemList.emeraldStar,
            ItemList.ruby,
            ItemList.rubyStar,
            ItemList.opal,
            ItemList.opalBlack,
            ItemList.diamond,
            ItemList.diamondStar,
            ItemList.sapphire,
            ItemList.sapphireStar
    };

    private static final int[] templateTypes3 = {
            ItemTemplateCreatorAwakening.mapFragment,
            ItemList.potion,
            ItemList.trapSticks,
            ItemList.trapPole,
            ItemList.trapCorrosion,
            ItemList.trapAxe,
            ItemList.trapKnife,
            ItemList.trapNet,
            ItemList.trapScythe,
            ItemList.trapMan,
            ItemList.trapBow,
            ItemList.trapRope,
            ItemList.lockpick
    };

    private static final int[] templateTypes4 = {
            ItemList.potionTransmutation,
            ItemList.potionIllusion,
            ItemList.potionAffinity,
            ItemList.potionWeaponSmithing,
            ItemList.potionRopemaking,
            ItemList.potionWaterwalking,
            ItemList.potionMining,
            ItemList.potionTailoring,
            ItemList.potionArmourSmithing,
            ItemList.potionFletching,
            ItemList.potionBlacksmithing,
            ItemList.potionLeatherworking,
            ItemList.potionShipbuilding,
            ItemList.potionStonecutting,
            ItemList.potionMasonry,
            ItemList.potionWoodcutting,
            ItemList.potionCarpentry,
            ItemList.potionAcidDamage,
            ItemList.potionFireDamage,
            ItemList.potionFrostDamage,
            ItemList.potionImbueShatterprot,
            ItemList.potionButchery,
            ItemList.champagne,
            ItemList.fireworks
    };

    private static final int[] templateTypes5 = {
            ItemTemplateCreatorAwakening.diplomaticPassport,
            ItemTemplateCreatorAwakening.servantContract,
            ItemList.tuningFork,
            ItemList.resurrectionStone,
            ItemList.wandSculpting,
            ItemList.farwalkerAmulet,
            ItemList.shakerOrb,
            ItemList.sleepPowder
    };

    private static final int[] templateTypes6 = {
            ItemList.teleportationTwig,
            ItemList.teleportationStone,
            ItemList.rodTransmutation,
            ItemList.handMirror,
            ItemList.boneCollar,
            ItemList.eggLarge,
            ItemList.drakeHide
    };

    private static final int[] templateTypes7 = {
            ItemList.bloodAngels,
            ItemList.smokeSol,
            ItemList.slimeUttacha,
            ItemList.tomeMagicRed,
            ItemList.scrollBinding,
            ItemList.cherryWhite,
            ItemList.cherryRed,
            ItemList.cherryGreen,
            ItemList.giantWalnut,
            ItemList.tomeEruption,
            ItemList.wandOfTheSeas,
            ItemList.libramNight,
            ItemList.tomeMagicGreen,
            ItemList.tomeMagicBlack,
            ItemList.tomeMagicBlue,
            ItemList.tomeMagicWhite
    };

    private static final int[] minimumCoins = { 50, 100, 130, 150, 200, 250, 300, 350 };
    private static final int[] maximumCoins = { 150, 200, 300, 400, 500, 600, 700, 800 };

    private static final int[] minimumMoonMetals = { 100, 200, 300, 500, 750, 1000, 1250, 1500 };
    private static final int[] maximumMoonMetals = { 300, 400, 500, 800, 1100, 1300, 2000, 3000 };

    public void fillTreasureChest(Item chest, int type) {
        long money = (minimumCoins[type] + Server.rand.nextInt(maximumCoins[type] - minimumCoins[type])) * 100L;
        Item[] coins = Economy.getEconomy().getCoinsFor(money);
        for(Item coin : coins)
            chest.insertItem(coin);

        int remainingWeight = (minimumMoonMetals[type] + Server.rand.nextInt(maximumMoonMetals[type] - minimumMoonMetals[type]));
        float startQl = 30.0f;
        float qlValRange = 70.0f;
        while(remainingWeight > 0) {
            try {
                int weight = Math.min(remainingWeight, 50 + Server.rand.nextInt(250));
                int templId = moonMetals[Server.rand.nextInt(moonMetals.length)];
                float ql = startQl + Server.rand.nextFloat() * qlValRange;
                Item moonMetal = ItemFactory.createItem(templId, ql, getRarity(false), null);
                moonMetal.setWeight(weight, true);
                chest.insertItem(moonMetal);
                remainingWeight -= weight;
            } catch(NoSuchTemplateException | FailedException e) { }
        }

        List<Item> items = new ArrayList<>();
        int r = Server.rand.nextInt(100);
        if(type == 0) {
            createTreasureItems(items, templateTypes1, 60.0f, 40.0f, 10);
            if(r < 20) createTreasureItems(items, templateTypes3, 20.0f, 40.0f, 1);
            else createTreasureItems(items, templateTypes2, 30.0f, 40.0f, 2);
        } else if(type == 1) {
            createTreasureItems(items, templateTypes1, 60.0f, 40.0f, 8);
            if(r == 0) createTreasureItems(items, templateTypes4, 20.0f, 40.0f, 1);
            else if(r < 20) createTreasureItems(items, templateTypes3, 20.0f, 40.0f, 2);
            else createTreasureItems(items, templateTypes2, 30.0f, 40.0f, 3);
        } else if(type == 2) {
            createTreasureItems(items, templateTypes1, 70.0f, 30.0f, 6);
            createTreasureItems(items, templateTypes2, 50.0f, 30.0f, 3);
            if(r == 0) createTreasureItems(items, templateTypes5, 20.0f, 40.0f, 1);
            else if(r < 10) createTreasureItems(items, templateTypes4, 40.0f, 20.0f, 2);
            else createTreasureItems(items, templateTypes3, 40.0f, 20.0f, 3);
        } else if(type == 3) {
            createTreasureItems(items, templateTypes1, 80.0f, 20.0f, 4);
            createTreasureItems(items, templateTypes2, 60.0f, 30.0f, 3);
            if(r == 0) createTreasureItems(items, templateTypes5, 40.0f, 30.0f, 1);
            else if(r < 20) createTreasureItems(items, templateTypes4, 50.0f, 30.0f, 3);
            else createTreasureItems(items, templateTypes3, 50.0f, 40.0f, 4);
        } else if(type == 4) {
            createTreasureItems(items, templateTypes1, 90.0f, 10.0f, 2);
            createTreasureItems(items, templateTypes2, 70.0f, 20.0f, 3);
            if(r == 0) createTreasureItems(items, templateTypes6, 10.0f, 40.0f, 1);
            else if(r < 10) createTreasureItems(items, templateTypes5, 60.0f, 30.0f, 1);
            else if(r < 30) createTreasureItems(items, templateTypes4, 60.0f, 30.0f, 2);
            else createTreasureItems(items, templateTypes3, 60.0f, 40.0f, 5);
        } else if(type == 5) {
            createTreasureItems(items, templateTypes2, 70.0f, 30.0f, 3);
            if(r < 5) createTreasureItems(items, templateTypes6, 30.0f, 40.0f, 1);
            else if(r < 30) createTreasureItems(items, templateTypes5, 60.0f, 40.0f, 1);
            else createTreasureItems(items, templateTypes4, 60.0f, 40.0f, 2);
        } else if(type == 6) {
            createTreasureItems(items, templateTypes2, 80.0f, 20.0f, 3);
            if(r < 20) createTreasureItems(items, templateTypes6, 30.0f, 50.0f, 1);
            else if(r < 60) createTreasureItems(items, templateTypes5, 70.0f, 30.0f, 2);
            else createTreasureItems(items, templateTypes4, 70.0f, 30.0f, 3);
        } else if(type == 7) {
            createTreasureItems(items, templateTypes2, 90.0f, 10.0f, 3);
            if(r < 20) createTreasureItems(items, templateTypes7, 50.0f, 50.0f, 1);
            else if(r < 60) createTreasureItems(items, templateTypes6, 80.0f, 20.0f, 2);
            else createTreasureItems(items, templateTypes5, 80.0f, 20.0f, 3);
        }
        for(Item item : items) {
            chest.insertItem(item, true);
        }
    }

    private void createTreasureItems(List<Item> items, int[] templateIds, float startQl, float qlValRange, int maxNums) {
        for(int i = 0; i < maxNums; ++i) {
            int templateId = templateIds[Server.rand.nextInt(templateIds.length)];
            createTreasureItem(items, templateId, startQl, qlValRange);
        }
    }

    private void createTreasureItem(List<Item> items, int templateId, float startQl, float qlValRange) {
        if(templateId == ItemTemplateCreatorAwakening.mapFragment) {
            Item mapFragment = Treasures.getInstance().createRandomMapFragment();
            if(mapFragment != null) items.add(mapFragment);
        } else if(templateId == ItemList.potionTransmutation) {
            Item potion = Treasures.getInstance().createRandomMapFragment();
            if(potion != null) {
                byte aux = (byte) (1 + Server.rand.nextInt(8));
                potion.setAuxData(aux);
                potion.setWeight(1440, false);
                potion.setDescription(MethodsItems.getTransmutationLiquidDescription(aux));
                items.add(potion);
            }
        } else {
            byte rarity = getRarity(templateId == ItemList.boneCollar || templateId == ItemList.champagne);
            float ql = startQl + Server.rand.nextFloat() * qlValRange;
            try {
                Item item = ItemFactory.createItem(templateId, ql, rarity, "");
                if(templateId == ItemList.eggLarge)
                    item.setData1(CreatureTemplateCreator.getRandomDragonOrDrakeId());
                else if(templateId == ItemList.drakeHide)
                    item.setData1(CreatureTemplateCreator.getRandomDrakeId());
                items.add(item);
            } catch(FailedException | NoSuchTemplateException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    private static final String[] mapTierDescriptions = {
            " You recognize the same kind of scribblings found on some recipes dropped by goblins.",
            " There are some symbols of skulls and bones.",
            " You notice some symbols associated with curses.",
            " You notice some symbols associated with sinister curses.",
            " The markings are similar to those on recipes dropped by trolls.",
            " This map appears to have been drawn by a warlock, having powerful magical symbols.",
            " It has a small drawing that looks like a body with two heads.",
            " You recognize some words written in the language of ancient Thekandre."
    };

    private static final String[] combinedFragmentsDescrptions = {
            "",
            " It's a small fragment, you will need to combine with a few more.",
            " It's about half of a complete map.",
            " Only a small fragment is missing for a complete map."
    };

    public String getFragmentDescription(Item item) {
        byte tier = item.getAuxData();
        int data = item.getData1();
        int n = 0;
        for(int i = 0; i < 8; ++i)
            if((data & (1 << i)) != 0) ++n;
        return mapTierDescriptions[tier] + combinedFragmentsDescrptions[Math.min(n, 3)];
    }

    public String getMapDescription(Item item) {
        byte tier = item.getAuxData();
        return mapTierDescriptions[tier];
    }

    private byte getRarity(boolean isRare) {
        int r = Server.rand.nextInt(isRare? 500 : 10000);
        return r < 500? (r < 50? (r == 0? FANTASTIC : SUPREME) : RARE) : COMMON;
    }
}
