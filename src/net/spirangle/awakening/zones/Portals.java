package net.spirangle.awakening.zones;

import com.wurmonline.server.Items;
import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class Portals {

    private static final Logger logger = Logger.getLogger(Portals.class.getName());

    private static Portals instance = null;

    public static Portals getInstance() {
        if(instance == null) instance = new Portals();
        return instance;
    }

    public final Map<Byte, Long> kingdomPortals;
    public final Set<Long> portals;

    private Portals() {
        kingdomPortals = new HashMap<>();
        portals = new HashSet<>();
    }

    public void addPortal(Item item) {
        if(item.getAuxData() > 0 && item.hasData())
            kingdomPortals.put(item.getAuxData(), item.getWurmId());
        portals.add(item.getWurmId());
    }

    public boolean usePortal(Creature performer, Item source, Item target) {
        if(target.isEpicPortal() || target.isServerPortal()) {
            Player player = (Player) performer;
            byte aux = target.getAuxData();
            byte returnKingdom = player.lastKingdom;
            if(returnKingdom == 0) returnKingdom = player.getKingdomTemplateId();
            long portalId = -1L;
            if(aux == 100 && returnKingdom > 0) {
                Long id = kingdomPortals.get(returnKingdom);
                if(id != null) portalId = id;
            }
            if(portalId == -1L && target.hasData()) portalId = target.getData();
            logger.info("Using portal " + portalId + "...");
            if(portalId > 0L) {
                try {
                    Item portalTo = Items.getItem(portalId);
                    if(aux > 0 && aux < 100) {
                        player.lastKingdom = aux;
                    }
                    doTeleport(player, portalTo);
                    return true;
                } catch(NoSuchItemException e) { }
            }
            player.getCommunicator().sendNormalServerMessage("Nothing happens. The portal seems to have lost it's power, or perhaps you're missing something?");
        }
        return false;
    }

    public void doTeleport(Player player, Item portalTo) {
        player.setTeleportPoints(portalTo.getPosX(), portalTo.getPosY(), portalTo.isOnSurface()? 0 : -1, portalTo.getFloorLevel());
        player.startTeleporting();
        player.getCommunicator().sendNormalServerMessage("You feel a slight tingle in your spine.");
        player.getCommunicator().sendTeleport(false);
        player.teleport(true);
        player.stopTeleporting();
    }
}
