package net.spirangle.awakening.zones;

import com.wurmonline.math.TilePos;
import com.wurmonline.mesh.Tiles;
import com.wurmonline.mesh.Tiles.Tile;
import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.Terraforming;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.*;
import com.wurmonline.server.zones.*;
import net.spirangle.awakening.Config;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.zones.Infestation.*;

public class Infestations {

    private static final Logger logger = Logger.getLogger(Infestations.class.getName());

    private static final int[] infestationTypes = {
            ItemList.riftStone1,
            ItemList.riftStone2,
            ItemList.riftStone3,
            ItemList.riftStone4,
            ItemList.riftCrystal1,
            ItemList.riftCrystal2,
            ItemList.riftCrystal3,
            ItemList.riftCrystal4,
            ItemList.riftPlant1,
            ItemList.riftPlant2,
            ItemList.riftPlant3,
            ItemList.riftPlant4,
            };

    @SuppressWarnings("unused")
    public static boolean canGatherRiftResource(final Creature performer, final Item sourceTool, final Item targetItem, final float counter, final Action act) {
        Infestation infestation = getInstance().getInfestation(targetItem);
        if(infestation != null && !infestation.isCompleted()) {
            performer.getCommunicator().sendNormalServerMessage("There is a powerful curse infestation in this place. You need to banish the curse first.");
            return false;
        }
        return true;
    }

    @SuppressWarnings("unused")
    public static void gatherRiftResource(final Creature performer, final Item sourceTool, final Item targetItem, final float counter, final Action act, final double power) {
        int templId = targetItem.getTemplateId();
        if(templId >= ItemList.riftStone1 && templId <= ItemList.riftStone4) templId = ItemList.adamantineBar;
        else if(templId >= ItemList.riftCrystal1 && templId <= ItemList.riftCrystal4)
            templId = ItemList.glimmerSteelBar;
        else templId = ItemList.seryllBar;
        try {
            float maxQL = targetItem.getCurrentQualityLevel();
            float resPower = GeneralUtilities.calcRareQuality(power, act.getRarity(), sourceTool.getRarity());
            int weight = Config.infestationMinMetalWeight + Server.rand.nextInt(Config.infestationMaxMetalWeight - Config.infestationMinMetalWeight);
            Item moonMetal = ItemFactory.createItem(templId, Math.min(maxQL, resPower), act.getRarity(), performer.getName());
            moonMetal.setWeight(weight, true);
            performer.getInventory().insertItem(moonMetal);
            performer.getCommunicator().sendNormalServerMessage("You get " + moonMetal.getNameWithGenus() + ".", (byte) 2);
            Server.getInstance().broadCastAction(performer.getName() + " finds " + moonMetal.getNameWithGenus() + ".", performer, 5);
        } catch(NoSuchTemplateException | FailedException e) { }
    }

    @SuppressWarnings("unused")
    public static void track(final Creature performer, final double knowledge, final double power) {
        int range = (int) ((knowledge + power) * 0.5);
        if(range > 0) {
            int x = performer.getTileX();
            int y = performer.getTileY();
            Infestation infestation = getInstance().getInfestation(x, y, range);
            if(infestation != null) {
                int distance = infestation.getDistance(x, y);
                StringBuilder message = new StringBuilder();
                message.append("You notice ");
                if(distance <= 5) message.append("dark auras surrounding plants and animals, even rocks.");
                else if(distance <= 10) message.append("unusually aggressive wildlife.");
                else if(distance <= 20) message.append("strangely twisted branches on trees and bushes.");
                else if(distance <= 30) message.append("a faint odor of something rotten.");
                else if(distance <= 50)
                    message.append("traces of tracks of unusual size and strange movement patterns.");
                else if(distance <= 100) message.append("at a distance, bird song with strange haunted sounds.");
                performer.getCommunicator().sendAlertServerMessage(message.toString());
            }
        }
    }

    private static Infestations instance = null;

    public static Infestations getInstance() {
        if(instance == null) instance = new Infestations();
        return instance;
    }


    private final Map<Long, Infestation> infestations = new HashMap<>();
    private final Map<Long, Infestation> creatures = new HashMap<>();

    private Infestations() {

    }

    public void init() {
        try(Connection con = ModSupportDb.getModSupportDb()) {
            try(PreparedStatement ps = con.prepareStatement("SELECT WURMID,TILEX,TILEY,POWER,TYPE,STATE,STARTTIME,ENDTIME,CREATED " +
                                                            "FROM INFESTATIONS WHERE (STATE&" + STATE_COMPLETED + ")=0")) {
                try(ResultSet rs = ps.executeQuery()) {
                    while(rs.next()) {
                        long wurmId = rs.getLong(1);
                        int tileX = rs.getInt(2);
                        int tileY = rs.getInt(3);
                        TilePos center = new TilePos();
                        center.set(tileX, tileY);
                        float power = rs.getFloat(4);
                        byte type = rs.getByte(5);
                        int state = rs.getInt(6) & (~STATE_NEW_OR_CHANGED);
                        long startTime = rs.getLong(7);
                        long endTime = rs.getLong(8);
                        long created = rs.getLong(9);
                        Infestation infestation = new Infestation(wurmId, center, power, type, state, startTime, endTime, created);
                        infestations.put(wurmId, infestation);
                        logger.info("Loaded infestation " + wurmId + " at " + tileX + "," + tileY + ".");
                    }
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load infestations data: " + e.getMessage(), e);
        }
    }

    public void poll() {
        infestations.values().forEach(Infestation::poll);
        if(infestations.size() < Config.infestations && Server.rand.nextInt(3) == 0)
            createRandomInfestation();
    }

    public Infestation createRandomInfestation() {
        TilePos center = getRandomTile();
        if(center != null) {
            float power = getInfestationPower();
            int type = getInfestationType(center.x, center.y);
            return createInfestation(center.x, center.y, power, type);
        }
        return null;
    }

    private TilePos getRandomTile() {
        List<Infestation> list = new ArrayList(infestations.values());
        if(!list.isEmpty()) {
            for(int i = 0; i < 50; ++i) {
                Infestation infestation = list.get(Server.rand.nextInt(list.size()));
                double d = 35.0 + Server.rand.nextDouble() * 65.0;
                double a = Server.rand.nextDouble() * Math.PI * 2.0;
                int x = infestation.center.x + (int) Math.round(d * Math.cos(a));
                int y = infestation.center.y + (int) Math.round(d * Math.sin(a));
                if(canCreateAtTile(x, y)) {
                    TilePos pos = new TilePos();
                    pos.set(x, y);
                    return pos;
                }
            }
        }
        for(int i = 0; i < 30; ++i) {
            int x = Server.rand.nextInt(Zones.worldTileSizeX);
            int y = Server.rand.nextInt(Zones.worldTileSizeY);
            if(canCreateAtTile(x, y)) {
                TilePos pos = new TilePos();
                pos.set(x, y);
                return pos;
            }
        }
        return null;
    }

    private boolean canCreateAtTile(int x, int y) {
        if(FocusZone.isNonPvPZoneAt(x, y)) return false;
        int tile = Server.surfaceMesh.getTile(x, y);
        int height = Tiles.decodeHeight(tile);
        if(height < 20 || height > 2000) return false;
        if(getInfestation(x, y, 30) != null) return false;
        try {
            FaithZone fz = Zones.getFaithZone(x, y, true);
            if(fz != null && fz.getCurrentRuler() != null) return false;
        } catch(NoSuchZoneException e) { }
        short[] steepness = Creature.getTileSteepness(x, y, true);
        if(steepness[0] > 20 && steepness[1] > 20) return false;
        final VolaTile vt = Zones.getTileOrNull(x, y, true);
        if(vt != null) {
            return vt.getStructure() == null;
        }
        return true;
    }

    private float getInfestationPower() {
        return Config.infestationMinPower + Server.rand.nextFloat() * (Config.infestationMaxPower - Config.infestationMinPower);
    }

    private int getInfestationType(int x, int y) {
        int data = Server.surfaceMesh.getTile(x, y);
        byte tileType = Tiles.decodeType(data);
        Tile tile = Tiles.getTile(tileType);
        int type = 0;
        if(tile.id == Tile.TILE_ROCK.id || tile.id == Tile.TILE_SAND.id || tile.id == Tile.TILE_STEPPE.id || tile.id == Tile.TILE_CLAY.id ||
           tile.isTundra() || tile.isRoad() || tile.isMycelium()) type = 4;
        else if(tile.id == Tile.TILE_MARSH.id || tile.isTree() || tile.isBush()) type = 8;
        type += Server.rand.nextInt(4);
        return type;
    }

    public Infestation createInfestation(int tileX, int tileY, float power, int type) {
        try {
            float ql = 10.0f + Server.rand.nextFloat() * 90.0f;
            int resource = (int) Math.ceil(power * Config.infestationResourceModifier);
            Item curseItem = ItemFactory.createItem(infestationTypes[type], ql, (tileX << 2) + 2.0f, (tileY << 2) + 2.0f, 0.0f, true, MiscConstants.COMMON, 0L, null);
            ItemTemplate template = ItemTemplateFactory.getInstance().getTemplate(type < 8? ItemList.rock : ItemList.log);
            curseItem.setWeight(template.getWeightGrams() * resource, true);
            TilePos center = new TilePos();
            center.set(tileX, tileY);
            Infestation infestation = new Infestation(curseItem.getWurmId(), center, power, (byte) type, STATE_NEW);
            infestations.put(infestation.wurmId, infestation);
            logger.info("Created infestation (x=" + tileX + ", y=" + tileY + ", power=" + power + ", type=" + type + ").");
            if(type >= 8) {
                int data = Server.surfaceMesh.getTile(tileX, tileY);
                byte tileType = Tiles.decodeType(data);
                byte tileData = Tiles.decodeData(data);
                short height = Tiles.decodeHeight(data);
                byte tileId = Tile.TILE_GRASS.id;
                Tile tile = Tiles.getTile(tileType);
                if(Terraforming.allCornersAtRockLevel(tileX, tileY, Server.surfaceMesh)) {
                    tileId = Tiles.Tile.TILE_ROCK.id;
                    tileData = 0;
                } else if(tile.isMycelium()) {
                    tileId = Tiles.Tile.TILE_MYCELIUM.id;
                } else if(tile.isEnchanted()) {
                    tileId = Tiles.Tile.TILE_ENCHANTED_GRASS.id;
                    tileData = 0;
                } else if(tileType == Tiles.Tile.TILE_BUSH_LINGONBERRY.id) {
                    tileId = Tiles.Tile.TILE_TUNDRA.id;
                }
                Server.setSurfaceTile(tileX, tileY, height, tileId, tileData);
                try {
                    Zone z = Zones.getZone(tileX, tileY, true);
                    z.changeTile(tileX, tileY);
                } catch(NoSuchZoneException e) { }
                Players.getInstance().sendChangedTile(tileX, tileY, true, true);
            }
            Server.getInstance().broadCastMessage("You have a momentary feeling that something is terribly wrong, as if falling into darkness.", tileX, tileY, true, 20);
            return infestation;
        } catch(NoSuchTemplateException | FailedException e) {
            logger.log(Level.WARNING, "Failed creating infestation (x=" + tileX + ", y=" + tileY + ", power=" + power + ", type=" + type + ").", e);
            return null;
        }
    }

    @SuppressWarnings("unused")
    public void saveAllInfestations() {
        if(infestations.entrySet().stream().noneMatch(entry -> (entry.getValue().state & STATE_NEW_OR_CHANGED) != 0))
            return;
        try(Connection con = ModSupportDb.getModSupportDb()) {
            while(!saveInfestations(con)) ;
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save players data: " + e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused")
    public boolean saveInfestations() {
        if(infestations.entrySet().stream().noneMatch(entry -> (entry.getValue().state & STATE_NEW_OR_CHANGED) != 0))
            return true;
        try(Connection con = ModSupportDb.getModSupportDb()) {
            return saveInfestations(con);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save players data: " + e.getMessage(), e);
        }
        return true;
    }

    public boolean saveInfestations(Connection con) {
        if(infestations.entrySet().stream().noneMatch(entry -> (entry.getValue().state & STATE_NEW_OR_CHANGED) != 0))
            return true;
        boolean done = true;
        try(PreparedStatement ps1 = con.prepareStatement("INSERT INTO INFESTATIONS (WURMID,TILEX,TILEY,POWER,TYPE,STATE,STARTTIME,ENDTIME,CREATED) VALUES(?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps2 = con.prepareStatement("UPDATE INFESTATIONS SET TILEX=?,TILEY=?,POWER=?,TYPE=?,STATE=?,STARTTIME=?,ENDTIME=? WHERE WURMID=?")) {
            int i = 0, j = 0;
            Infestation infestation;
            List<Infestation> list = new ArrayList<>(20);
            for(Map.Entry<Long, Infestation> entry : infestations.entrySet()) {
                infestation = entry.getValue();
                if((infestation.state & STATE_NEW) != 0) {
                    if(i >= 10) {
                        done = false;
                        continue;
                    }
                    ps1.setLong(1, infestation.wurmId);
                    ps1.setInt(2, infestation.center.x);
                    ps1.setInt(3, infestation.center.y);
                    ps1.setFloat(4, infestation.power);
                    ps1.setByte(5, infestation.type);
                    ps1.setInt(6, infestation.state);
                    ps1.setLong(7, infestation.startTime);
                    ps1.setLong(8, infestation.endTime);
                    ps1.setLong(9, infestation.created);
                    ps1.addBatch();
                    ++i;
                    list.add(infestation);
                } else if((infestation.state & STATE_CHANGED) != 0) {
                    if(j >= 10) {
                        done = false;
                        continue;
                    }
                    ps2.setInt(1, infestation.center.x);
                    ps2.setInt(2, infestation.center.y);
                    ps2.setFloat(3, infestation.power);
                    ps2.setByte(4, infestation.type);
                    ps2.setInt(5, infestation.state);
                    ps2.setLong(6, infestation.startTime);
                    ps2.setLong(7, infestation.endTime);
                    ps2.setLong(8, infestation.wurmId);
                    ps2.addBatch();
                    ++j;
                    list.add(infestation);
                }
            }
            if(i > 0 || j > 0) {
                if(i > 0) ps1.executeBatch();
                if(j > 0) ps2.executeBatch();
                logger.info("Saved data for " + (i + j) + " infestations.");
                list.forEach(inf -> {
                    inf.state &= ~STATE_NEW_OR_CHANGED;
                    if(inf.isCompleted()) infestations.remove(inf.wurmId);
                });
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save players data: " + e.getMessage(), e);
        }
        return done;
    }

    public Infestation getInfestation(Item item) {
        Infestation infestation = getInfestation(item.getWurmId());
        if(infestation == null) infestation = getInfestation(item.getTileX(), item.getTileY());
        return infestation;
    }

    public Infestation getInfestation(long wurmId) {
        return infestations.get(wurmId);
    }

    public Infestation getInfestation(int tileX, int tileY) {
        return getInfestation(tileX, tileY, 0);
    }

    public Infestation getInfestation(int tileX, int tileY, int distance) {
        int x, y, d;
        for(Infestation inf : infestations.values()) {
            x = Math.abs(tileX - inf.center.x);
            y = Math.abs(tileY - inf.center.y);
            d = distance <= 0? inf.size : distance;
            if((x == 0 && y <= d) || (y == 0 && x <= d) || (int) Math.sqrt(x * x + y * y) <= d)
                return inf;
        }
        return null;
    }

    public Infestation getInfestation(Creature creature) {
        return creatures.get(creature.getWurmId());
    }

    void addCreature(Infestation infestation, Creature creature) {
        addCreature(infestation, creature.getWurmId());
    }

    void addCreature(Infestation infestation, long wurmId) {
        creatures.put(wurmId, infestation);
    }

    void removeCreature(Creature creature) {
        removeCreature(creature.getWurmId());
    }

    void removeCreature(long wurmId) {
        creatures.remove(wurmId);
    }

    public boolean creatureKilled(Creature creature, Map<Long, Long> attackers) {
        if(creature == null) return false;
        Infestation infestation = getInfestation(creature);
        if(infestation == null) return false;
        infestation.creatureKilled(creature, attackers);
        removeCreature(creature);
        return true;
    }
}
