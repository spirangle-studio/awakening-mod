package net.spirangle.awakening.zones;

import com.wurmonline.math.TilePos;
import com.wurmonline.server.Items;
import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.shared.constants.CreatureTypes;
import net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening;
import net.spirangle.awakening.players.ChatChannels;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Treasure {

    private static final Logger logger = Logger.getLogger(Treasure.class.getName());

    private static class TreasureCreature {
        int templateId;
        float value;

        TreasureCreature(int templateId, float value) {
            this.templateId = templateId;
            this.value = value;
        }
    }

    private static final TreasureCreature[] creatureTypes1 = {
            new TreasureCreature(CreatureTemplateIds.GOBLIN_CID, 0.14f),
            };

    private static final TreasureCreature[] creatureTypes2 = {
            new TreasureCreature(CreatureTemplateIds.SKELETON_CID, 0.18f),
            new TreasureCreature(CreatureTemplateIds.ZOMBIE_CID, 0.25f),
            };

    private static final TreasureCreature[] creatureTypes3 = {
            new TreasureCreature(CreatureTemplateIds.GUARD_SPIRIT_EVIL_LENIENT, 0.4f),
            };

    private static final TreasureCreature[] creatureTypes4 = {
            new TreasureCreature(CreatureTemplateIds.GUARD_SPIRIT_EVIL_ABLE, 0.6f),
            };

    private static final TreasureCreature[] creatureTypes5 = {
            new TreasureCreature(CreatureTemplateIds.TROLL_CID, 0.8f),
            };

    private static final TreasureCreature[] creatureTypes6 = {
            new TreasureCreature(CreatureTemplateIds.DEMON_SOL_CID, 1.5f),
            };

    private static final TreasureCreature[] creatureTypes7 = {
            new TreasureCreature(CreatureTemplateIds.SON_OF_NOGUMP_CID, 2.5f),
            };

    private static final TreasureCreature[] creatureTypes8 = {
            new TreasureCreature(CreatureTemplateCreatorAwakening.DRAGONET_BLACK_CID, 3.5f),
            new TreasureCreature(CreatureTemplateCreatorAwakening.DRAGONET_RED_CID, 3.5f),
            };

    private static final TreasureCreature[][] creatureTypes = {
            creatureTypes1,
            creatureTypes2,
            creatureTypes3,
            creatureTypes4,
            creatureTypes5,
            creatureTypes6,
            creatureTypes7,
            creatureTypes8,
            };

    long chestId;
    long mapId;
    TilePos center;
    byte type;
    float power;
    int state;
    float completed;
    long startTime;
    long endTime;
    private final Set<Long> creatures = new HashSet<>();

    public Treasure(long chestId, long mapId, TilePos center, byte type, int state) {
        this(chestId, mapId, center, type, state, 0L, 0L);
    }

    Treasure(long chestId, long mapId, TilePos center, byte type, int state, long startTime, long endTime) {
        this.chestId = chestId;
        this.mapId = mapId;
        this.center = center;
        this.type = type;
        this.power = ((float) (type + 3) / 10.0f) * (3.5f + Server.rand.nextFloat() * 1.5f);
        this.state = state;
        this.completed = 0.0f;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getChestId() {
        return chestId;
    }

    public long getMapId() {
        return mapId;
    }

    public int getTileX() {
        return center.x;
    }

    public int getTileY() {
        return center.y;
    }

    @SuppressWarnings("unused")
    public TilePos getCenterPos() {
        return center;
    }

    public int getDistance(int x, int y) {
        return Tiles.getDistance(center.x, center.y, x, y);
    }

    private static final String[] spawnMessages = {
            "A tribe of goblins are guarding this treasure, with fanatic fervor they attack you to defend it.",
            "Many have died for this treasure, and even in death they rise to continue defending it.",
            "The spirits of the dead who who fought over this treasure, are summoned to defend it once again.",
            "The spirits of the dead who who fought over this treasure, are summoned to defend it once again.",
            "With angry shouts and accusations a tribe of trolls appear from behind rocks and trees to defend their treasure.",
            "By opening the chest you've triggered a curse once cast by a warlock to protect the treasure, and demons are summoned.",
            "Someone found the location where a Son of Nogump had hid his treasure, and drew a map. This is that treasure, and this is that Nogump.",
            "Dragons are known to hoard treasures, and so does their relatives the dragonets. How it came to pass that the map to this treasure was made, you have no idea, but the guardians are still around."
    };

    private static final String[] opentreasureMessages = {
            "A small treasure was discovered by %1$s, who defeated a tribe of goblins guarding it.",
            "Having searched wide and far, %1$s found a treasure, which was guarded by the undead.",
            "A treasure was unearthed by %1$s, it was cursed and spirits of the dead had to be vanquished.",
            "A treasure was unearthed by %1$s, it was cursed and spirits of the dead had to be vanquished.",
            "A troll hoard was found by %1$s, but first %2$s had to defeat the trolls guarding it.",
            "An ancient warlock's treasure was uncovered by %1$s, unfortunately it triggered a curse summoning demons to defend it.",
            "Happy as %2$s were, %1$s having finally found a sought after treasure - it was nevertheless defended by two-headed monsters.",
            "Dragonets are guarding their treasures to the death, which %1$s found out having discovered a dragonet's hoard."
    };

    public boolean openTreasure(Creature performer) {
        if(spawnCreatures()) {
            String message = spawnMessages[type];
            Server.getInstance().broadCastMessage(message, center.x, center.y, true, 5);
            return false;
        }
        if(!creatures.isEmpty()) {
            performer.getCommunicator().sendNormalServerMessage("You will have to kill all defenders of the treasure, before it may be opened.");
            return false;
        }
        Treasures.getInstance().removeTreasure(this);
        Items.destroyItem(mapId);
        String message = String.format(opentreasureMessages[type], performer.getName(), performer.getHeSheItString());
        Server.getInstance().broadCastSafe(message, false);
        ChatChannels.getInstance().sendHistoryMessage(message);
        return true;
    }

    private boolean spawnCreatures() {
        if(power <= 0.0f || !creatures.isEmpty()) return false;
        int t = type;
        float p = power;
        TreasureCreature[] types = creatureTypes[t];
        double d = 2.0;
        double a = Server.rand.nextDouble() * Math.PI * 2.0;
        TreasureCreature type = null;
        CreatureTemplate template = null;
        int n = 0, z = 0;
        for(int i = 0; p > 0.0f; ++i) {
            try {
                if(n <= 0) {
                    type = types[Server.rand.nextInt(types.length)];
                    template = CreatureTemplateFactory.getInstance().getTemplate(type.templateId);
                    n = 3 + Server.rand.nextInt(3);
                }
                int x = (int) Math.round(d * Math.cos(a));
                int y = (int) Math.round(d * Math.sin(a));
                float posX = ((center.x + x) << 2) + 2.0f;
                float posY = ((center.y + y) << 2) + 2.0f;
                float rot = Server.rand.nextFloat() * 360.0f;
                byte gender = template.keepSex? template.getSex() : ((byte) Server.rand.nextInt(2));
                boolean reborn = false;
                byte age = -1;
                switch(template.getTemplateId()) {
                    case CreatureTemplateIds.SKELETON_CID:
                    case CreatureTemplateIds.ZOMBIE_CID:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_LENIENT:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_LENIENT:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_ABLE:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_ABLE:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_DANGEROUS:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_DANGEROUS:
                        age = 0;
                        reborn = true;
                        break;
                    default:
                        if(age < 0) age = 0;
                }
                byte mod = CreatureTypes.C_MOD_NONE;
                if(i == 0 && (t == 0 || t == 4 || t == 7)) {
                    switch(Server.rand.nextInt(5)) {
                        case 0:
                            mod = CreatureTypes.C_MOD_FIERCE;
                            break;
                        case 1:
                            mod = CreatureTypes.C_MOD_ANGRY;
                            break;
                        case 2:
                            mod = CreatureTypes.C_MOD_RAGING;
                            break;
                        case 3:
                            mod = CreatureTypes.C_MOD_ALERT;
                            break;
                        case 4:
                            mod = CreatureTypes.C_MOD_HARDENED;
                            break;
                    }
                }
                Creature creature = Creature.doNew(type.templateId, true, posX, posY, rot, 0, template.getName(), gender, (byte) 0, mod, reborn, age);
                creatures.add(creature.getWurmId());
                Treasures.getInstance().addCreature(this, creature);
                logger.info("Spawned " + creature.getName() + " at " + (center.x + x) + ", " + (center.y + y) + ".");
                d += 0.25;
                a += (0.4 + Server.rand.nextDouble() * 0.2) * Math.PI;
                p -= type.value;
                --n;
            } catch(Exception e) {
                logger.log(Level.SEVERE, "Failed spawning cursed creatures: " + e.getMessage(), e);
                if(++z >= 10) break;
            }
        }
        power = p;
        return true;
    }

    public boolean creatureKilled(Creature creature, Map<Long, Long> attackers) {
        if(creature == null) return false;
        creatures.remove(creature.getWurmId());
        return !creature.isPlayer() && attackers != null && !attackers.isEmpty();
    }
}
