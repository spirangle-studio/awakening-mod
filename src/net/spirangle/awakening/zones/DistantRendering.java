package net.spirangle.awakening.zones;

import com.wurmonline.mesh.MeshIO;
import com.wurmonline.mesh.Tiles;
import com.wurmonline.mesh.Tiles.Tile;

import static com.wurmonline.mesh.Tiles.Tile.*;

@SuppressWarnings("unused")
public class DistantRendering {

    public static void calcDistantTerrainForest(MeshIO meshIO) {
        int n = meshIO.getSize() / 16;
        int[] counts = new int[256];
        for(int xT = 0; xT < n; ++xT) {
            for(int yT = 0; yT < n; ++yT) {
                int vegetation = 0;
                int killerTrees = 0;
                int topCount = 0;
                int mostCommon = 0;
                Tile mostCommonTile = null;
                for(int i = 0; i < 256; ++i) counts[i] = 0;
                for(int x = 0; x < 16; ++x) {
                    for(int y = 0; y < 16; ++y) {
                        int type = Tiles.decodeType(meshIO.getTile(xT * 16 + x, yT * 16 + y)) & 0xFF;
                        Tile tile = Tiles.getTile(type);
                        if(tile != null) {
                            if(tile.isTree() || tile.isBush()) ++vegetation;
                            if(isKillerTree(tile)) ++killerTrees;
                        }
                        if(++counts[type] > topCount) {
                            topCount = counts[type];
                            mostCommon = type;
                            mostCommonTile = tile;
                        }
                    }
                }
                if(mostCommonTile != null && mostCommonTile.isGrass() && (vegetation >= 50 || killerTrees >= 9)) {
                    mostCommon = 255;
                    //Find most common vegetation
                    for(int i = 0; i < 256; ++i) {
                        Tile tile = Tiles.getTile(i);
                        if(tile != null && (tile.isTree() || tile.isBush())) {
                            if(counts[i] > counts[mostCommon]) mostCommon = i;
                        }
                    }
                }
                meshIO.getDistantTerrainTypes()[xT + yT * n] = (byte) mostCommon;
            }
        }
    }

    private static boolean isKillerTree(Tile tile) {
        return tile == TILE_ENCHANTED_TREE_WILLOW || tile == TILE_MYCELIUM_TREE_WILLOW || tile == TILE_TREE_WILLOW
               || tile == TILE_ENCHANTED_TREE_OAK || tile == TILE_MYCELIUM_TREE_OAK || tile == TILE_TREE_OAK;
    }
}
