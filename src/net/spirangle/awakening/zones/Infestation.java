package net.spirangle.awakening.zones;

import com.wurmonline.math.TilePos;
import com.wurmonline.server.*;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.players.Player;
import com.wurmonline.shared.util.StringUtilities;
import net.spirangle.awakening.creatures.CreatureTemplateCreatorAwakening;
import net.spirangle.awakening.creatures.Traits;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.players.CombatData;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;

import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static net.spirangle.awakening.players.Achievements.ACH_INFESTATION1;

public class Infestation {

    static final int STATE_NEW = 0x0001;
    static final int STATE_CHANGED = 0x0002;
    static final int STATE_NEW_OR_CHANGED = STATE_NEW | STATE_CHANGED;
    static final int STATE_ACTIVE = 0x0004;
    static final int STATE_COMPLETED = 0x0008;
    static final int STATE_DEFEATED = 0x0010;
    static final int STATE_DECAYED = 0x0020;

    public static final String[] powerLevels = {
            "weak",
            "strong",
            "terrifying",
            "demonic",
            "infernal",
            "hellish",
            };

    private static final Logger logger = Logger.getLogger(Infestation.class.getName());

    private static class Participant {
        long wurmId;
        long activityTime;
        int kills;
        int score;

        Participant(long wurmId) {
            this.wurmId = wurmId;
            this.activityTime = 0L;
            this.kills = 0;
            this.score = 0;
        }

        int getScore() {
            return score;
        }
    }

    private static class CursedCreature {
        int templateId;
        float value;

        CursedCreature(int templateId, float value) {
            this.templateId = templateId;
            this.value = value;
        }
    }

    private static final CursedCreature[] cursedCreatureTypes1 = {
            new CursedCreature(CreatureTemplateIds.CAVE_BUG_CID, 0.14f),
            new CursedCreature(CreatureTemplateIds.WOLF_BLACK_CID, 0.14f),
            new CursedCreature(CreatureTemplateIds.LION_MOUNTAIN_CID, 0.14f),
            new CursedCreature(CreatureTemplateIds.SKELETON_CID, 0.18f),
            new CursedCreature(CreatureTemplateIds.BEAR_BLACK_CID, 0.22f),
            new CursedCreature(CreatureTemplateIds.BEAR_BROWN_CID, 0.25f),
            };

    private static final CursedCreature[] cursedCreatureTypes2 = {
            new CursedCreature(CreatureTemplateIds.ZOMBIE_CID, 0.25f),
            new CursedCreature(CreatureTemplateIds.SPIDER_CID, 0.3f),
            new CursedCreature(CreatureTemplateIds.LAVA_SPIDER_CID, 0.35f),
            new CursedCreature(CreatureTemplateIds.SCORPION_CID, 0.4f),
            };

    private static final CursedCreature[] cursedCreatureTypes3 = {
            new CursedCreature(CreatureTemplateIds.CROCODILE_CID, 0.4f),
            new CursedCreature(CreatureTemplateIds.ANACONDA_CID, 0.45f),
            new CursedCreature(CreatureTemplateIds.HELL_HOUND_CID, 0.5f),
            new CursedCreature(CreatureTemplateIds.GUARD_SPIRIT_EVIL_LENIENT, 0.4f),
            };

    private static final CursedCreature[] cursedCreatureTypes4 = {
            new CursedCreature(CreatureTemplateIds.LAVA_CREATURE_CID, 0.5f),
            new CursedCreature(CreatureTemplateIds.TROLL_CID, 0.8f),
            new CursedCreature(CreatureTemplateIds.HELL_SCORPION_CID, 0.8f),
            new CursedCreature(CreatureTemplateIds.GUARD_SPIRIT_EVIL_ABLE, 0.6f),
            };

    private static final CursedCreature[] cursedCreatureTypes5 = {
            new CursedCreature(CreatureTemplateIds.DEATHCRAWLER_MINION_CID, 0.8f),
            new CursedCreature(CreatureTemplateIds.SPAWN_UTTACHA_CID, 1.0f),
            new CursedCreature(CreatureTemplateIds.DEMON_SOL_CID, 1.2f),
            new CursedCreature(CreatureTemplateIds.GUARD_SPIRIT_EVIL_DANGEROUS, 1.6f),
            };

    private static final CursedCreature[] cursedCreatureTypes6 = {
            new CursedCreature(CreatureTemplateIds.SON_OF_NOGUMP_CID, 2.5f),
            new CursedCreature(CreatureTemplateIds.EAGLESPIRIT_CID, 2.8f),
            new CursedCreature(CreatureTemplateIds.DRAKESPIRIT_CID, 3.0f),
            new CursedCreature(CreatureTemplateCreatorAwakening.DRAGONET_BLACK_CID, 3.5f),
            new CursedCreature(CreatureTemplateCreatorAwakening.DRAGONET_RED_CID, 3.5f),
            };

    private static final CursedCreature[][] cursedCreatureTypes = {
            cursedCreatureTypes1,
            cursedCreatureTypes2,
            cursedCreatureTypes3,
            cursedCreatureTypes4,
            cursedCreatureTypes5,
            cursedCreatureTypes6,
            };

    long wurmId;
    TilePos center;
    float power;
    byte type;
    int state;
    int size;
    int wave;
    float completed;
    long startTime;
    long endTime;
    long created;
    private final Set<Long> creatures = new HashSet<>();
    private final Map<Long, Participant> participants = new HashMap<>();

    public Infestation(long wurmId, TilePos center, float power, byte type, int state) {
        this(wurmId, center, power, type, state, 0L, 0L, System.currentTimeMillis());
    }

    Infestation(long wurmId, TilePos center, float power, byte type, int state, long startTime, long endTime, long created) {
        this.wurmId = wurmId;
        this.center = center;
        this.power = power;
        this.type = type;
        this.state = state;
        this.size = (int) (7.0f + power * 1.2f);
        this.wave = 0;
        this.completed = 0.0f;
        this.startTime = startTime;
        this.endTime = endTime;
        this.created = created;
    }

    public int getTileX() {
        return center.x;
    }

    public int getTileY() {
        return center.y;
    }

    @SuppressWarnings("unused")
    public TilePos getCenterPos() {
        return center;
    }

    public int getDistance(int x, int y) {
        return Tiles.getDistance(center.x, center.y, x, y);
    }

    public float getPower() {
        return power;
    }

    @SuppressWarnings("unused")
    public boolean setPower(final float power) {
        if(this.power == power) return false;
        this.power = power;
        this.state |= STATE_CHANGED;
        return true;
    }

    public byte getType() {
        return this.type;
    }

    @SuppressWarnings("unused")
    public boolean setType(final byte type) {
        if(this.type == type) return false;
        this.type = type;
        this.state |= STATE_CHANGED;
        return true;
    }

    @SuppressWarnings("unused")
    public int getState() {
        return this.state;
    }

    @SuppressWarnings("unused")
    public boolean setState(final int state) {
        if((this.state | STATE_NEW_OR_CHANGED) == (state | STATE_NEW_OR_CHANGED)) return false;
        this.state = state | STATE_CHANGED;
        return true;
    }

    public int getSize() {
        return size;
    }

    @SuppressWarnings("unused")
    public boolean setSize(final int size) {
        if(this.size == size) return false;
        this.size = size;
        this.state |= STATE_CHANGED;
        return true;
    }

    @SuppressWarnings("unused")
    public int getWave() {
        return wave;
    }

    @SuppressWarnings("unused")
    public boolean setWave(final int wave) {
        if(this.wave == wave) return false;
        this.wave = wave;
        this.state |= STATE_CHANGED;
        return true;
    }

    @SuppressWarnings("unused")
    public float getPercentWaveCompletion() {
        return completed;
    }

    @SuppressWarnings("unused")
    public boolean setPercentWaveCompletion(final float completed) {
        if(this.completed == completed) return false;
        this.completed = completed;
        this.state |= STATE_CHANGED;
        return true;
    }

    public int getNumber() {
        return 0;
    }

    @SuppressWarnings("unused")
    public boolean setNumber(final int number) {
//        this.state |= STATE_CHANGED;
        return false;
    }

    public boolean isActive() {
        return (this.state & STATE_ACTIVE) != 0;
    }

    public boolean setActive(final boolean active) {
        if(isActive() == active) return false;
        this.state = (active? this.state | STATE_ACTIVE : this.state & (~STATE_ACTIVE)) | STATE_CHANGED;
        return true;
    }

    public boolean isCompleted() {
        return (this.state & STATE_COMPLETED) != 0;
    }

    public boolean setCompleted(final boolean completed) {
        if(isCompleted() == completed) return false;
        this.state = (completed? this.state | STATE_COMPLETED : this.state & (~STATE_COMPLETED)) | STATE_CHANGED;
        this.endTime = System.currentTimeMillis();
        return true;
    }

    public boolean isDefeated() {
        return (this.state & STATE_DEFEATED) != 0;
    }

    public boolean setDefeated(final boolean defeated) {
        if(isDefeated() == defeated) return false;
        this.state = (defeated? this.state | STATE_DEFEATED : this.state & (~STATE_DEFEATED)) | STATE_CHANGED;
        return true;
    }

    public boolean isDecayed() {
        return (this.state & STATE_DECAYED) != 0;
    }

    public boolean setDecayed(final boolean decayed) {
        if(isDecayed() == decayed) return false;
        this.state = (decayed? this.state | STATE_DECAYED : this.state & (~STATE_DECAYED)) | STATE_CHANGED;
        return true;
    }

    public void poll() {
        if(!Items.exists(wurmId)) {
            setCompleted(true);
            setDecayed(true);
        }
    }

    public boolean canActivateWave() {
        if(!creatures.isEmpty()) {
            for(Iterator<Long> iterator = creatures.iterator(); iterator.hasNext(); ) {
                long wurmId = iterator.next();
                boolean remove = false;
                try {
                    Creature creature = Creatures.getInstance().getCreature(wurmId);
                    if(creature.isDead()) remove = true;
                } catch(NoSuchCreatureException e) {
                    remove = true;
                }
                if(remove) {
                    iterator.remove();
                    Infestations.getInstance().removeCreature(wurmId);
                }
            }
        }
        return creatures.isEmpty() && !isCompleted();
    }

    public void activateWave(Creature performer, double power) {
        if(!canActivateWave()) return;
        if(wave >= 5) {
            setActive(false);
            setCompleted(true);
            setDefeated(true);
            List<Participant> sorted = participants.values().stream()
                                                   .sorted(Comparator.comparingInt(Participant::getScore).reversed())
                                                   .collect(Collectors.toList());
            StringBuilder sb = new StringBuilder();
            int n = 0, p = 0, g = 0;
            int level = Math.min(4, (int) this.power);
            for(Participant participant : sorted) {
                if(participant.score == 0) continue;
                if(WurmId.getType(participant.wurmId) == 0) {
                    PlayerData pd = PlayersData.getInstance().get(participant.wurmId);
                    CombatData cd = pd.getCombatData();
                    cd.addCompletedInfestation(participant.kills, participant.score);
                    try {
                        Player player = Players.getInstance().getPlayer(participant.wurmId);
                        player.getCommunicator().sendNormalServerMessage("Your participation in banishing the infestation was " +
                                                                         participant.kills + " kill" + (participant.kills == 1? "" : "s") +
                                                                         " and a score of " + participant.score + ".");
                        player.achievement(ACH_INFESTATION1 + level);
                    } catch(NoSuchPlayerException e) { }
                    if(n < 5) {
                        if(n++ > 0) sb.append(n == 5 || n == sorted.size()? " and " : ", ");
                        sb.append(pd.name);
                    }
                    ++p;
                } else {
                    ++g;
                }
            }
            String powerLevel = powerLevels[level];
            StringBuilder message = new StringBuilder();
            message.append(StringUtilities.isVowel(powerLevel.charAt(0))? "An " : "A ").append(powerLevel)
                   .append(" infestation has been banished from the world of Awakening! ");
            if(n == 0 && g > 0) message.append("Brave guards killed off all cursed monsters.");
            else if(n == 1)
                message.append("The hero ").append(sb).append(" managed this feat single-handedly.");
            else if(n == sorted.size())
                message.append("The heroes ").append(sb).append(" did this feat on their own.");
            else message.append("The most prominent heroes were ").append(sb).append(".");
            long seconds = (endTime - startTime) / 1000L;
            message.append(" It was completed in ");
            int i = 2;
            if(seconds >= 86400L && i > 0) {
                message.append(seconds / 86400L).append(" day");
                if(seconds / 86400L > 1L) message.append('s');
                seconds %= 86400L;
                if(--i >= 1 && seconds > 0L) message.append(" and ");
            }
            if(seconds >= 3600L && i > 0) {
                message.append(seconds / 3600L).append(" hour");
                if(seconds / 3600L > 1L) message.append('s');
                seconds %= 3600L;
                if(--i >= 1 && seconds > 0L) message.append(" and ");
            }
            if(seconds >= 60L && i > 0) {
                message.append(seconds / 60L).append(" minute");
                if(seconds / 60L > 1L) message.append('s');
                seconds %= 60L;
                if(--i >= 1 && seconds > 0L) message.append(" and ");
            }
            if(seconds >= 0L && i > 0) {
                message.append(seconds).append(" second");
                if(seconds > 1L) message.append('s');
            }
            message.append(".");
            Server.getInstance().broadCastSafe(message.toString(), false);
            ChatChannels.getInstance().sendHistoryMessage(message.toString());
            return;
        }
        if(wave == 0) {
            startTime = System.currentTimeMillis();
            setActive(true);
        }
        wave++;
        spawnCreatures();
    }

    private void spawnCreatures() {
        float p = power * 0.5f + (float) wave * power * 0.2f;
        int t = (int) p;
        if(t > 5) t = 5;
        CursedCreature[] types = cursedCreatureTypes[t];
        double d = 2.0;
        double a = Server.rand.nextDouble() * Math.PI * 2.0;
        CursedCreature type = null;
        CreatureTemplate template = null;
        int n = 0, z = 0;
        while(p > 0.0f) {
            try {
                if(n <= 0) {
                    type = types[Server.rand.nextInt(types.length)];
                    template = CreatureTemplateFactory.getInstance().getTemplate(type.templateId);
                    n = 3 + Server.rand.nextInt(3);
                }
                int x = (int) Math.round(d * Math.cos(a));
                int y = (int) Math.round(d * Math.sin(a));
                float posX = ((center.x + x) << 2) + 2.0f;
                float posY = ((center.y + y) << 2) + 2.0f;
                float rot = Server.rand.nextFloat() * 360.0f;
                byte gender = template.keepSex? template.getSex() : ((byte) Server.rand.nextInt(2));
                boolean reborn = false;
                byte age = -1;
                switch(template.getTemplateId()) {
                    case CreatureTemplateIds.SKELETON_CID:
                    case CreatureTemplateIds.ZOMBIE_CID:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_LENIENT:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_LENIENT:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_ABLE:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_ABLE:
                    case CreatureTemplateIds.GUARD_SPIRIT_GOOD_DANGEROUS:
                    case CreatureTemplateIds.GUARD_SPIRIT_EVIL_DANGEROUS:
                        age = 0;
                        reborn = true;
                        break;
                    case CreatureTemplateIds.RAT_LARGE_CID:
                        age = (byte) (1 + Server.rand.nextInt(20));
                        break;
                    default:
                        if(age < 0) age = 0;
                }
                Creature creature = Creature.doNew(type.templateId, true, posX, posY, rot, 0, template.getName(), gender, (byte) 0, Traits.C_MOD_CURSED, reborn, age);
                creatures.add(creature.getWurmId());
                Infestations.getInstance().addCreature(this, creature);
                logger.info("Spawned " + creature.getName() + " at " + (center.x + x) + ", " + (center.y + y) + ".");
                d += 0.25;
                a += (0.4 + Server.rand.nextDouble() * 0.2) * Math.PI;
                p -= type.value;
                --n;
            } catch(Exception e) {
                logger.log(Level.SEVERE, "Failed spawning cursed creatures: " + e.getMessage(), e);
                if(++z >= 10) break;
            }
        }
    }

    public boolean creatureKilled(Creature creature, Map<Long, Long> attackers) {
        if(creature == null) return false;
        creatures.remove(creature.getWurmId());
        if(creature.isPlayer() || attackers == null || attackers.isEmpty()) return false;
        int score = (int) ((((creature.getBaseCombatRating() + creature.getBonusCombatRating()) * 5.0f) +
                            (Math.max(creature.getFightingSkill().getKnowledge(), creature.getWeaponLessFightingSkill().getKnowledge()) * 0.5)) * 0.1f);
        long now = System.currentTimeMillis();
        for(final Entry<Long, Long> entry : attackers.entrySet()) {
            long attackerId = entry.getKey();
            long attackTime = entry.getValue();
            if(now - attackTime >= 600000L || WurmId.getType(attackerId) != 0) continue;
            if(WurmId.getType(attackerId) == 0) {
                try {
                    Player player = Players.getInstance().getPlayer(attackerId);
                    addParticipantKill(player, 1, score);
                } catch(NoSuchPlayerException e) { }
            } else {
                try {
                    Creature c = Creatures.getInstance().getCreature(attackerId);
                    if(c.getDominator() != null && c.getDominator().isPlayer()) {
                        addParticipantKill(c.getDominator(), 0, score / 5);
                    } else if(c.isSpiritGuard() || c.isKingdomGuard()) {
                        addParticipantKill(c, 1, score);
                    }
                } catch(NoSuchCreatureException ex5) { }
            }
        }
        return true;
    }

    private void addParticipantKill(Creature creature, int kill, int score) {
        long now = System.currentTimeMillis();
        Participant participant = participants.get(creature.getWurmId());
        if(participant == null) {
            participant = new Participant(creature.getWurmId());
            participants.put(creature.getWurmId(), participant);
        }
        if(now - participant.activityTime >= 3600000L) {
            participant.kills = 0;
            participant.score = 0;
        }
        participant.activityTime = now;
        participant.kills += kill;
        participant.score += score;
    }
}
