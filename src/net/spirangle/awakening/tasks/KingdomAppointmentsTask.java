package net.spirangle.awakening.tasks;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.*;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static net.spirangle.awakening.AwakeningConstants.THEKANDRE;
import static net.spirangle.awakening.AwakeningConstants.TIRVALON;


public class KingdomAppointmentsTask extends Task {

    private static final Logger logger = Logger.getLogger(KingdomAppointmentsTask.class.getName());

    private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Appoint kingdom officials";
            params.tooltip = "GM Task: Allows players to apply for kingdom appointments";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        if(taskPaper == null || responder == null) return true;
        if(responder.getPower() >= MiscConstants.POWER_DEMIGOD && servant.npc.getKingdomId() > 0)
            return Kingdoms.getKingdomOrNull(servant.npc.getKingdomId()) != null;
        return false;
    }

    public static class Application {
        public long id;
        public long wurmId;
        public final long steamId;
        public byte kingdom;
        public int officeId;
        public int appointment;
        public long created;

        public Application(long id, long wurmId, long steamId, byte kingdom, int officeId, int appointment, long created) {
            this.id = id;
            this.wurmId = wurmId;
            this.steamId = steamId;
            this.kingdom = kingdom;
            this.officeId = officeId;
            this.appointment = appointment;
            this.created = created;
        }
    }

    public static final long WURM_TIME_OF_YEAR_APPOINTMENTS = 14299200L; // Starfall of Snake, 4th week, day of Tears at 12:00
    public static final long WURM_TIME_ONE_YEAR = 29030400L;

    private static final Map<Long, Application> applications = new HashMap<>();
    private static final Set<Application> unappointedOfficials = new HashSet<>();

    public static long getTimeSinceAppointments() {
        long now = System.currentTimeMillis();
        long wurmTime = WurmCalendar.getCurrentTime();
        long timeOfYear = wurmTime % WURM_TIME_ONE_YEAR;
        return timeOfYear >= WURM_TIME_OF_YEAR_APPOINTMENTS? timeOfYear - WURM_TIME_OF_YEAR_APPOINTMENTS :
               timeOfYear + (WURM_TIME_ONE_YEAR - WURM_TIME_OF_YEAR_APPOINTMENTS);
    }

    public static void loadApplications() {
        applications.clear();
        long timeSinceAppointments = getTimeSinceAppointments() * 8L;
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("SELECT ID,WURMID,STEAMID,KINGDOM,OFFICE,APPOINTMENT,CREATED FROM OFFICE_APPLICATIONS WHERE CREATED>=?")) {
            ps.setLong(1, timeSinceAppointments);
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    long id = rs.getLong(1);
                    long wurmId = rs.getLong(2);
                    long steamId = rs.getLong(3);
                    byte kingdom = rs.getByte(4);
                    int officeId = rs.getInt(5);
                    int appointment = rs.getInt(6);
                    long created = rs.getLong(7);
                    applications.put(steamId, new Application(id, wurmId, steamId, kingdom, officeId, appointment, created));
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load schedule tasks.", e);
        }
    }

    public static void listApplications(Communicator communicator) {
        if(applications.isEmpty() && unappointedOfficials.isEmpty()) {
            communicator.sendSafeServerMessage("There are no applications.");
            return;
        }
        if(!applications.isEmpty()) {
            communicator.sendSafeServerMessage("Listing appointment applications:");
            applications.values().forEach(application -> {
                listApplication(communicator, application);
            });
        }
        if(!unappointedOfficials.isEmpty()) {
            communicator.sendSafeServerMessage("Listing unappointed officials:");
            unappointedOfficials.forEach(application -> {
                listApplication(communicator, application);
            });
        }
    }

    public static void listApplication(Communicator communicator, Application application) {
        PlayerInfo playerInfo = getPlayerInfo(application);
        if(playerInfo != null) {
            King king = King.getKing(playerInfo.getChaosKingdom());
            Appointments appointments = Appointments.getAppointments(king.era);
            Appointment a = appointments.getAppointment(application.officeId);
            String text = playerInfo.getName() + " (" + king.kingdomName.substring(0, 3) + ")" +
                          " as " + a.getNameForGender(playerInfo.getSex()) +
                          " - " + dateTimeFormat.format(new Date(application.created));
            communicator.sendSafeServerMessage(text);
        }
    }

    public static void pollYearElection() {
        logger.info("KingdomAppointmentsTask: Poll year election...");
        unappointedOfficials.clear();
        for(byte i = TIRVALON; i <= THEKANDRE; ++i) {
            King king = King.getKing(i);
            if(king == null) continue;
            Appointments appointments = Appointments.getAppointments(king.era);
            if(appointments != null) {
                Stack<Application>[] officeApplications = new Stack[appointments.officials.length];
                for(int x = 0; x < appointments.officials.length; ++x) officeApplications[x] = new Stack<>();
                applications.values().stream()
                            .filter(a -> a.kingdom == king.kingdom)
                            .sorted((a1, a2) -> (int) (a1.created - a2.created))
                            .forEach(a -> officeApplications[a.officeId - 1500].add(a));
                Set<Integer> elected = new HashSet<>();
                Stack<Application> unelected = new Stack<>();
                for(int office = 0; office < appointments.officials.length; ++office) {
                    Stack<Application> queue = officeApplications[office];
                    if(!queue.isEmpty()) electOfficial(king, appointments, office, queue, elected, unelected);
                }
                List<Application> unelectedSorted = unelected.stream().sorted((a1, a2) -> (int) (a1.created - a2.created)).collect(Collectors.toList());
                unelected = new Stack<>();
                unelected.addAll(unelectedSorted);
                for(int office = 0; office < appointments.officials.length; ++office) {
                    if(unelected.isEmpty()) break;
                    if(elected.contains(office)) continue;
                    electOfficial(king, appointments, office, unelected, null, null);
                }
            }
        }
        applications.clear();
    }

    public static void pollUnappointedOfficials() {
        if(unappointedOfficials.isEmpty()) return;
        Iterator<Application> iterator = unappointedOfficials.iterator();
        while(iterator.hasNext()) {
            Application application = iterator.next();
            Player player = Players.getInstance().getPlayerOrNull(application.wurmId);
            if(player == null) continue;
            King king = King.getKing(player.getKingdomId());
            Appointments appointments = Appointments.getAppointments(king.era);
            if(addAppointment(player, king, appointments, application.officeId - 1500, application))
                iterator.remove();
        }
    }

    private static void electOfficial(King king, Appointments appointments, int office, Stack<Application> queue, Set<Integer> elected, Stack<Application> unelected) {
        Application application = queue.pop();
        while(!queue.isEmpty() && !Server.rand.nextBoolean()) {
            if(unelected != null) unelected.add(application);
            application = queue.pop();
        }
        if(unelected != null && !queue.isEmpty()) unelected.addAll(queue);
        Player player = Players.getInstance().getPlayerOrNull(application.wurmId);
        application.officeId = 1500 + office;
        if(!addAppointment(player, king, appointments, office, application))
            unappointedOfficials.add(application);
        if(elected != null) elected.add(office);
    }

    private static boolean addAppointment(Player player, King king, Appointments appointments, int office, Application application) {
        if(player == null) return false;
        Appointment a = appointments.getAppointment(1500 + office);
        Communicator c = player.getCommunicator();
        for(int o = 0; o < appointments.officials.length; ++o) {
            if(appointments.officials[o] == player.getWurmId()) {
                if(o != office) vacateOffice(player, appointments, 1500 + o);
                else return true;
            }
        }
        if(appointments.officials[office] > 0L)
            vacateOffice(appointments.officials[office], appointments, 1500 + office);
        appointments.setOfficial(a.getId(), player.getWurmId());
        player.achievement(323);
        king.addAppointment(a);
        String appointmentName = a.getNameForGender(player.getSex());
        String kingdomName = Kingdoms.getNameFor(player.getKingdomId());
        c.sendNormalServerMessage("You have graciously been awarded the " + appointmentName +
                                  " of " + kingdomName + " by " + king.getRulerTitle() + "!", (byte) 2);
        String message = "receives the post " + appointmentName + " of " + kingdomName + " from this year's election.";
        ChatChannels.getInstance().sendHistoryMessage(player.getName() + " " + message);
        HistoryManager.addHistory(player.getName(), message);
        return true;
    }

    private static void vacateOffice(long wurmId, Appointments appointments, int officeId) {
        Player player = Players.getInstance().getPlayerOrNull(wurmId);
        vacateOffice(player, appointments, officeId);
    }

    private static void vacateOffice(Player player, Appointments appointments, int officeId) {
        if(player != null) {
            Appointment a = appointments.getAppointment(officeId);
            String appointmentName = a.getNameForGender(player.getSex());
            String message = "You are hereby notified that you have been removed from the office of " + appointmentName + ".";
            player.getCommunicator().sendNormalServerMessage(message, (byte) 2);
        }
        appointments.setOfficial(officeId, 0L);
    }

    private static PlayerInfo getPlayerInfo(Application application) {
        if(application != null) {
            PlayerInfo playerInfo = PlayerInfoFactory.getPlayerInfoWithWurmId(application.wurmId);
            if(playerInfo != null) {
                if(playerInfo.loaded) return playerInfo;
                try {
                    playerInfo.load();
                    return playerInfo;
                } catch(IOException e) { }
            }
        }
        return null;
    }

    private Kingdom kingdom = null;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_KINGDOM_APPOINTMENTS;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Appoint officials for " + kingdom.getName();
    }

    @Override
    public String getSubject(Creature responder) {
        return kingdom.getName() + " official applications";
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        Kingdom kingdom = Kingdoms.getKingdomOrNull(npc.getKingdomId());
        if(kingdom == null) return;
        int w = 350;
        int h = 250;
        String bml = "border{\n" +
                     " varray{rescale='true';\n" +
                     "  text{type='bold';text=\"Handle " + kingdom.getName() + " appointments:\"}\n" +
                     " };\n" +
                     " null;\n" +
                     " scroll{horizontal='false';vertical='true';\n" +
                     "  varray{rescale='true';\n" +
                     "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                     "   text{text=''}\n" +
                     "   text{text=\"This task will instruct " + name + " to handle applications and appointments of kingdom officials, for " + kingdom.getName() + ".\"}\n" +
                     "  }\n" +
                     " };\n" +
                     " null;\n" +
                     " right{\n" +
                     "  harray{\n" +
                     "   button{id='start';size='80,20';text='Back'}\n" +
                     "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                     "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                     "   button{id='submit';size='80,20';text='Instruct'}\n" +
                     "  }\n" +
                     " }\n" +
                     "}\n";
        responder.getCommunicator().sendBml(w, h, false, true, bml, 200, 200, 200, "Servant: Appointments");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature npc = servant.npc;
        kingdom = Kingdoms.getKingdomOrNull(npc.getKingdomId());
        return kingdom != null;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        if(responder.getKingdomId() != kingdom.getId()) {
            denyNonCitizen(responder);
            return true;
        }
        ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, 0, null);
        stq.sendQuestion();
        return true;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        if(!responder.isPlayer()) return;
        Player player = (Player) responder;
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        Application application = applications.get(player.getSteamId().getSteamID64());
        PlayerInfo playerInfo = getPlayerInfo(application);
        int w = 350;
        int h = 420;
        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Handle ")
           .append(kingdom.getName())
           .append(" appointments:\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='")
           .append(question.getId())
           .append("'};\n")
           .append("   text{text=''}\n")
           .append("   text{text=\"").append(name).append(" accepts applications for kingdom offices ")
           .append("for the kingdom of ").append(kingdom.getName()).append(". Applicants are elected by the city ")
           .append("council on the day of Tears at noon, in the fourth week of the starfall of the Snake, and will ")
           .append("take office at that time.\"}\n")
           .append("   text{text=''}\n");
        if(application != null) {
            Appointment a = Appointment.getAppointment(application.officeId, kingdom.getId());
            if(application.wurmId != player.getWurmId()) {
                String piname = StringUtils.bmlString(playerInfo.getName());
                bml.append("   text{type='italic';text='I have the application of ").append(piname)
                   .append(", who you should be acquainted with. I will only accept a limited number of ")
                   .append("applications, would you prefer to replace ").append(piname).append("?'}\n");
            } else {
                bml.append("   text{type='italic';text='I have your application here, for the office of ")
                   .append(a.getNameForGender(player.getSex())).append(", would you like to change your preferred office?'}\n");
            }
            bml.append("   text{text=''}\n");
        }
        final Appointments appointments = King.getCurrentAppointments(kingdom.getId());
        int n = 0;
        for(int i = 0; i < appointments.officials.length; ++i) {
            int o = 1500 + i;
            Appointment a = Appointment.getAppointment(o, kingdom.getId());
            if(a == null) continue;
            if(n == 0)
                bml.append("   text{type='italic';text='For which of these offices would you like to apply?'}\n");
            bml.append("   radio{group='office';id='").append(o).append("';text='")
               .append(a.getNameForGender(player.getSex())).append("'}\n");
            ++n;
        }
        if(n > 0) {
            bml.append("   text{text=''}\n")
               .append("   text{type='italic';text=\"Note that if another individual is elected for your chosen office ")
               .append("you may still become elected for another office. In either case, as elected you must serve ")
               .append("for one year until the next election. You may apply for a post each year.\"}\n")
               .append("   text{text=''}\n");
        } else {
            bml.append("   text{type='italic';text=\"Unfortunately our kingdom doesn't have any offices at the moment.\"}\n");
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='start';size='80,20';text='Back'}\n")
           .append("   text{size='").append((w - 6 - 240)).append(",1';text=''}\n")
           .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
           .append("   button{id='submit';size='80,20';text='Apply'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        //      logger.info("BML: "+bml);
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        if(!responder.isPlayer()) return;
        Player player = (Player) responder;
        if(responder.getKingdomId() != kingdom.getId()) {
            denyNonCitizen(responder);
            return;
        }
        if(!player.acceptsInvitations()) {
            player.getCommunicator().sendAlertServerMessage("You need to type /invitations first.");
            return;
        }
        String office = properties.getProperty("office");
        if(office == null) return;
        int officeId = Integer.parseInt(office);
        Appointment appointment = Appointment.getAppointment(officeId, kingdom.getId());
        long steamId = player.getSteamId().getSteamID64();
        Application a = applications.get(steamId);
        try(Connection con = ModSupportDb.getModSupportDb()) {
            if(a != null) {
                try(PreparedStatement ps = con.prepareStatement("UPDATE OFFICE_APPLICATIONS SET WURMID=?,KINGDOM=?,OFFICE=? WHERE ID=?")) {
                    a.wurmId = player.getWurmId();
                    a.kingdom = kingdom.getId();
                    a.officeId = officeId;
                    ps.setLong(1, a.wurmId);
                    ps.setInt(2, a.kingdom);
                    ps.setInt(3, a.officeId);
                    ps.setLong(4, a.id);
                    ps.execute();
                }
            } else {
                try(PreparedStatement ps = con.prepareStatement("INSERT INTO OFFICE_APPLICATIONS (ID,WURMID,STEAMID,KINGDOM,OFFICE,APPOINTMENT,CREATED) VALUES(NULL,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, player.getWurmId());
                    ps.setLong(2, steamId);
                    ps.setByte(3, kingdom.getId());
                    ps.setInt(4, officeId);
                    ps.setLong(5, 0);
                    ps.setLong(6, System.currentTimeMillis());
                    long aid = 0;
                    int rows = ps.executeUpdate();
                    if(rows == 0) throw new SQLException("Creating office application failed, no rows affected.");
                    try(ResultSet rs = ps.getGeneratedKeys()) {
                        if(rs.next()) aid = rs.getInt(1);
                        else throw new SQLException("Creating office application failed, no ID obtained.");
                    }
                    a = new Application(aid, player.getWurmId(), steamId, kingdom.getId(), officeId, 0, System.currentTimeMillis());
                    applications.put(steamId, a);
                }
            }
            player.getCommunicator().sendSafeServerMessage(servant.npc.getName() + " accepts your application for the post of " +
                                                           appointment.getNameForGender(player.getSex()) + ".");
        } catch(SQLException e) {
            player.getCommunicator().sendAlertServerMessage(servant.npc.getName() + " rejected your application for the post of " +
                                                            appointment.getNameForGender(player.getSex()) + ".");
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        byte kingdomId = (byte) jo.optInt("kingdomId");
        kingdom = Kingdoms.getKingdomOrNull(kingdomId);
        return kingdom != null;
    }

    @Override
    public String toJSONString() {
        return "{kingdomId:" + kingdom.getId() + "}";
    }

    private void denyNonCitizen(Creature responder) {
        responder.getCommunicator().sendAlertServerMessage(servant.npc.getName() + " says: You are not a citizen of the kingdom " +
                                                           kingdom.getName() + " and so you are not permitted to apply for office " +
                                                           "in our great nation.");
    }
}
