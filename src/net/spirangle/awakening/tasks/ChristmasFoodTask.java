package net.spirangle.awakening.tasks;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.*;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.skills.SkillSystem;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class ChristmasFoodTask extends Task {

    private static final Logger logger = Logger.getLogger(ChristmasFoodTask.class.getName());

    public static final int CHRISTMAS_FOOD_GIFT_ID = 0x1011;

    private static final int[] foodGroups = { 0, 6, 12, 16, 20 };


    private static class ChristmasFood {
        public String name;
        public int templateId;
        public int recipeId;
        public int realTemplateId;
        public int foodXState;
        public int containerTemplateId;
        public float containerQualityLevel;
        public String containerName;

        public ChristmasFood(String nm, int tid, int rid, int rtid, int xs, int ctid, float cql, String cnm) {
            name = nm;
            templateId = tid;
            recipeId = rid;
            realTemplateId = rtid;
            foodXState = xs;
            containerTemplateId = ctid;
            containerQualityLevel = cql;
            containerName = cnm;
        }
    }


    private static final ChristmasFood[] foodTypes = {
            new ChristmasFood("gingerbread cookie", ItemList.cookie, 1139, ItemList.ginger, -1, -1, 0.0f, null),
            new ChristmasFood("christmas cake", ItemList.cake, 796, ItemList.anyFruit, -1, -1, 0.0f, null),
            new ChristmasFood("candied fruit", ItemList.sweet, 1124, ItemList.anyFruit, -1, -1, 0.0f, null),
            new ChristmasFood("chocolate", ItemList.chocolate, 1041, -1, -1, -1, 0.0f, null),
            new ChristmasFood("fire roasted chestnut", ItemList.chestnut, 1060, -1, -1, -1, 0.0f, null),
            new ChristmasFood("marshmallow", ItemList.sweet, 1126, ItemList.sugar, -1, -1, 0.0f, null),
            new ChristmasFood("mint humbugs", ItemList.sweet, 1129, ItemList.mint, -1, -1, 0.0f, null),
            new ChristmasFood("nougat", ItemList.sweet, 1127, ItemList.anyFruit, -1, -1, 0.0f, null),
            new ChristmasFood("plain toffee", ItemList.sweet, 1137, ItemList.anyMilk, -1, -1, 0.0f, null),
            new ChristmasFood("raspberry trifle", ItemList.pudding, 657, -1, -1, -1, 0.0f, null),
            new ChristmasFood("spiced apple", ItemList.appleGreen, 1162, -1, 7, -1, 0.0f, null),
            new ChristmasFood("wurm mince pie", ItemList.pie, 1270, -1, -1, -1, 0.0f, null),
            new ChristmasFood("egg nog", ItemList.whisky, 649, ItemList.eggSmall, -1, ItemList.beerSteinPottery, 1.0f, "tankard"),
            new ChristmasFood("hot chocolate", ItemList.teaGreen, 1122, ItemList.cocoa, -1, ItemList.beerSteinPottery, 1.0f, "tankard"),
            new ChristmasFood("white wine", ItemList.wineWhite, 1417, -1, -1, ItemList.skullMug, 1.0f, "goblet"),
            new ChristmasFood("red wine", ItemList.wineRed, 1418, -1, -1, ItemList.skullMug, 1.0f, "goblet"),
            new ChristmasFood("cherry brandy", ItemList.brandy, 1420, ItemList.cherries, -1, ItemList.skullMug, 1.0f, "goblet"),
            new ChristmasFood("strawberry gin", ItemList.gin, 1421, ItemList.strawberries, -1, ItemList.skullMug, 1.0f, "goblet"),
            new ChristmasFood("moonshine", ItemList.moonshine, 1426, -1, -1, ItemList.beerSteinPottery, 1.0f, "tankard"),
            new ChristmasFood("pilsner", ItemList.beer, 1430, ItemList.belladonna, -1, ItemList.beerSteinPottery, 1.0f, "tankard")
    };

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Christmas food and drink";
            params.tooltip = "GM Task: Give christmas food and drink to visitors";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return taskPaper == null || responder == null || responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public static boolean createXmasFood(Communicator communicator, int nr) {
        logger.info("Create Christmas Food: " + nr);
        if(nr >= 0 && nr < foodTypes.length) {
            Player performer = communicator.getPlayer();
            communicator.sendNormalServerMessage("Create Christmas Food: " + foodTypes[nr].name);
            giveFoodTo(performer, nr);
            return true;
        }
        return false;
    }

    public static Item giveFoodTo(Creature responder, int n) {
        ItemTemplateFactory itf = ItemTemplateFactory.getInstance();
        try {
            ChristmasFood cf = foodTypes[n];
            ItemTemplate template = itf.getTemplate(cf.templateId);
            byte material = template.getMaterial();
            Recipe recipe = Recipes.getRecipeById((short) cf.recipeId);
            Item item = ItemFactory.createItem(template.getTemplateId(), 100.0f, material, (byte) 0, null);
            if(cf.name != null) item.setName(cf.name);

            byte stages = 20;
            byte ingredients = 20;
            if(item.isLiquid()) {
                stages = 10;
                ingredients = 10;
            }

            if(recipe != null) {
                short calories = template.getCalories();
                short carbs = template.getCarbs();
                short fats = template.getFats();
                short proteins = template.getProteins();
                byte bonus = (byte) Server.rand.nextInt(SkillSystem.getNumberOfSkillTemplates());
                ItemMealData.save(item.getWurmId(), recipe.getRecipeId(), calories, carbs, fats, proteins, bonus, stages, ingredients);
            }

            if(cf.realTemplateId != -1) {
                ItemTemplate rit = itf.getTemplate(cf.realTemplateId);
                if(rit != null) item.setRealTemplate(rit.getTemplateId());
            }

            if(cf.foodXState != -1) {
                item.setAuxData((byte) cf.foodXState);
            }

            item.setWeight(100, true);

            if(cf.containerTemplateId != -1) {
                Item container = ItemFactory.createItem(cf.containerTemplateId, cf.containerQualityLevel, null);
                if(cf.containerName != null) container.setName(cf.containerName);
                container.insertItem(item, true);
                item = container;
            }


            responder.getInventory().insertItem(item, true);
            responder.getCommunicator().sendSafeServerMessage("Please accept some " + cf.name + ", and do enjoy the holidays!");

            return item;

        } catch(NoSuchTemplateException | FailedException e) {
            logger.log(Level.SEVERE, "Failed creating Christmas food.", e);
        }
        return null;
    }

    public int group = 1;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_CHRISTMAS_FOOD;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Give Christmas " + (isGivingFood()? "food" : "drink");
    }

    @Override
    public String getSubject(Creature responder) {
        return "Christmas " + (isGivingFood()? "food" : "drink");
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        String bml = "text{text=\"This task will instruct " + name + " to give food or drink to people asking for it.\r\n\r\nWhich group of food or drink should " + name + " give?\"}" +
                     "radio{ group='group'; id='1';text='gingerbread cookie, christmas cake, candied fruit, chocolate, fire roasted chestnut, marshmallow'}" +
                     "radio{ group='group'; id='2';text='mint humbugs, nougat, plain toffee, raspberry trifle, spiced apple, wurm mince pie.'}" +
                     "radio{ group='group'; id='3';text='egg nog, hot chocolate, white wine, red wine'}" +
                     "radio{ group='group'; id='4';text='cherry brandy, strawberry gin, moonshine, pilsner'}";
        responder.getCommunicator().sendBml(450, 350, true, true, bml, 200, 200, 200, "Servant: Cristmas food and drink");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        String g = properties.getProperty("group");
        if(g == null) return false;
        group = Integer.parseInt(g);
        if(group < 1) group = 1;
        else if(group > 4) group = 4;
        return true;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) FROM GIFTS WHERE STEAMID=? AND GIFTID=? AND SOURCEID=? AND CREATED>=? GROUP BY STEAMID")) {
            ps.setLong(1, responder.isPlayer()? ((Player) responder).getSteamId().getSteamID64() : 0L);
            ps.setInt(2, CHRISTMAS_FOOD_GIFT_ID);
            ps.setLong(3, servant.npc.getWurmId());
            ps.setLong(4, System.currentTimeMillis() - 1800000L);
            try(ResultSet rs = ps.executeQuery()) {
                if(rs.next()) {
                    int n = rs.getInt(1);
                    if(n > 0) {
                        responder.getCommunicator().sendNormalServerMessage("You've already been given some " +
                                                                            (isGivingFood()? "food" : "drink") + ", please come back later.");
                        return true;
                    }
                }
                int n1 = foodGroups[group - 1];
                int n2 = foodGroups[group];
                int n = n1 + Server.rand.nextInt(n2 - n1);
                Item food = giveFoodTo(responder, n);
                if(food != null) {
                    try(PreparedStatement ps2 = con.prepareStatement("INSERT INTO GIFTS (WURMID,STEAMID,GIFTID,SOURCEID,ITEMTEMPLATEID,ITEMID,CREATED) VALUES(?,?,?,?,?,?,?)")) {
                        ps2.setLong(1, responder.getWurmId());
                        ps2.setLong(2, responder.isPlayer()? ((Player) responder).getSteamId().getSteamID64() : 0L);
                        ps2.setInt(3, CHRISTMAS_FOOD_GIFT_ID);
                        ps2.setLong(4, servant.npc.getWurmId());
                        ps2.setInt(5, food.getTemplateId());
                        ps2.setLong(6, food.getWurmId());
                        ps2.setLong(7, System.currentTimeMillis());
                        ps2.execute();
                    }
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load servant data.", e);
        }
        return true;
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        group = jo.optInt("group");
        logger.info("ChristmasFoodTask.readJSON(group: " + group + ")");
        return true;
    }

    @Override
    public String toJSONString() {
        return "{group:" + group + "}";
    }

    public boolean isGivingFood() { return group <= 2; }

    public boolean isGivingDrink() { return group >= 3; }
}
