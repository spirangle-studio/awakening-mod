package net.spirangle.awakening.tasks;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONException;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.support.JSONString;
import com.wurmonline.server.support.JSONTokener;
import net.spirangle.awakening.creatures.Servant;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class Task implements JSONString {

    private static final Logger logger = Logger.getLogger(Task.class.getName());

    public static class AboutTaskParams {
        public final Creature responder;
        public final boolean getTitle;
        public final boolean getDescription;
        public String title;
        public String header;
        public String description;
        public int popupWidth;
        public int popupHeight;

        public AboutTaskParams(Creature responder, boolean getTitle, boolean getDescription) {
            this.responder = responder;
            this.getTitle = getTitle;
            this.getDescription = getDescription;
            this.title = null;
            this.header = null;
            this.description = null;
            this.popupWidth = -1;
            this.popupHeight = -1;
        }
    }

    public static class InstructTaskParams {
        public final Creature responder;
        public final Item taskPaper;
        public final Servant servant;
        public String title;
        public String tooltip;

        public InstructTaskParams(Creature responder, Item taskPaper, Servant servant) {
            this.responder = responder;
            this.taskPaper = taskPaper;
            this.servant = servant;
            this.title = null;
            this.tooltip = null;
        }
    }

    public static class RegisteredTask {
        final public Class<? extends Task> taskClass;
        final public Constructor<? extends Task> constructor;
        final public Method getAboutTaskTitle;
        final public Method getInstructTaskTitle;
        final public Method createTaskTest;

        private RegisteredTask(final Class<? extends Task> taskClass) throws NoSuchMethodException, IllegalArgumentException {
            this.taskClass = taskClass;
            this.constructor = taskClass.getConstructor();
            this.getAboutTaskTitle = taskClass.getMethod("getAboutTaskTitle", AboutTaskParams.class);
            this.getInstructTaskTitle = taskClass.getMethod("getInstructTaskTitle", InstructTaskParams.class);
            this.createTaskTest = taskClass.getMethod("createTaskTest", Creature.class, Item.class, Servant.class);
        }
    }

    private static Map<Integer, RegisteredTask> registeredTasks;

    /**
     * Static method for configuring all tasks.
     */
    @SuppressWarnings("unused")
    public static void configure(Properties properties) {
        MerchantTask.configure(properties);
    }

    /**
     * Invoke this static method to register a new task for servants. Custom tasks should have an ID above 10000.
     *
     * @param id
     * @param taskClass
     */
    public static void registerTask(int id, Class<? extends Task> taskClass) {
        if(registeredTasks == null) registeredTasks = new LinkedHashMap<>();
        else if(registeredTasks.containsKey(id)) {
            logger.log(Level.SEVERE, "The task ID: " + id + " has already been registered. This task will not be used: " + taskClass.getName());
            return;
        }
        try {
            RegisteredTask registeredTask = new RegisteredTask(taskClass);
            registeredTasks.put(id, registeredTask);
        } catch(NoSuchMethodException | IllegalArgumentException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static RegisteredTask getRegisteredTask(int id) {
        return registeredTasks.get(id);
    }

    public static Set<Map.Entry<Integer, RegisteredTask>> getRegisteredTasks() {
        return registeredTasks.entrySet();
    }

    /**
     * Called by Servant on loading during start up
     */
    public static Task createTask(long id, int type, float ql, Servant servant, String data) {
        return Task.createTask(null, id, type, ql, null, servant, data);
    }

    /**
     * Called by InstructTaskQuestion on giving task instruction to servant
     */
    public static Task createTask(Creature responder, int type, Item taskPaper, Servant servant) {
        return Task.createTask(responder, -1L, type, taskPaper.getQualityLevel(), taskPaper, servant, null);
    }

    /**
     * Factory method for creating a Task object.
     */
    private static Task createTask(Creature responder, long id, int type, float ql, Item taskPaper, Servant servant, String data) {
        if(registeredTasks == null || registeredTasks.isEmpty()) return null;
        Task task = null;
        RegisteredTask registeredTask = registeredTasks.get(type);
        if(registeredTask != null && registeredTask.constructor != null && registeredTask.createTaskTest != null) {
            try {
                boolean result = (boolean) registeredTask.createTaskTest.invoke(null, responder, taskPaper, servant);
                if(result) task = registeredTask.constructor.newInstance();
            } catch(InstantiationException | IllegalAccessException | IllegalArgumentException |
                    InvocationTargetException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        if(task != null) {
            task.init(id, ql, taskPaper, servant);
            if(data != null) {
                if(!task.parseJSON(data)) return null;
            }
        }
        return task;
    }

    public long id = -1L;
    public int type = -1;
    public float ql = 0.0f;
    public Item taskPaper = null;
    public Servant servant = null;

    protected void init(long id, float ql, Item taskPaper, Servant servant) {
        this.id = id;
        this.type = getTaskType();
        this.ql = ql;
        this.taskPaper = taskPaper;
        this.servant = servant;
    }

    public int getTaskType() {
        return -1;
    }

    public boolean isGroupedTask() { return false; }

    public void addTask(Task task) { }

    /**
     * Called by TalkToServantQuestion for title string.
     */
    public String getTitle(Creature responder) { return null; }

    /**
     * Called by TalkToServantQuestion for subject string.
     */
    public String getSubject(Creature responder) {
        return null;
    }

    /**
     * Store task data in db.
     */
    public boolean updateDb() {
        return updateDb(this.toJSONString());
    }

    /**
     * Store task data in db.
     */
    public boolean updateDb(String data) {
        try(Connection con = ModSupportDb.getModSupportDb()) {
            int affectedRows = 0;
            if(id <= 0L) {
                String[] returnId = { "TASKID" };
                try(PreparedStatement ps = con.prepareStatement("INSERT INTO TASKS (NPCID,TYPE,QUALITYLEVEL,DATA) VALUES(?,?,?,?)", returnId)) {
                    ps.setLong(1, servant.npc.getWurmId());
                    ps.setInt(2, type);
                    ps.setFloat(3, ql);
                    ps.setString(4, data);
                    affectedRows = ps.executeUpdate();
                    if(affectedRows > 0) {
                        try(ResultSet rs = ps.getGeneratedKeys()) {
                            if(rs.next()) id = rs.getLong(1);
                        }
                    }
                }
            } else {
                try(PreparedStatement ps = con.prepareStatement("UPDATE TASKS SET NPCID=?,TYPE=?,QUALITYLEVEL=?,DATA=? WHERE TASKID=?")) {
                    ps.setLong(1, servant.npc.getWurmId());
                    ps.setInt(2, type);
                    ps.setFloat(3, ql);
                    ps.setString(4, data);
                    ps.setLong(5, id);
                    affectedRows = ps.executeUpdate();
                }
            }
            if(affectedRows == 0) {
                logger.log(Level.SEVERE, "Failed to save servant task data.");
                return false;
            }
            return true;
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to update servant task data: " + e.getMessage(), e);
        }
        return false;
    }

    /**
     * Called when deleting the task.
     */
    public void delete() { }

    /**
     * Called by the Servant on each poll call on the Npc.
     */
    public void poll() { }

    /**
     * Called by the Servant on each pollChat call on the Npc. Return true if used.
     */
    public boolean pollChat() { return false; }

    /**
     * Called by the Servant every wurm day at 6:00, i.e. every 3 real hours.
     */
    public void pollDaily() { }

    /**
     * The amount of salary this task will cost the employer on a daily basis. The salary is paid in advance at 6:00.
     */
    public long getSalary() { return 0L; }

    /**
     * The weight of the task on the servant, used for deciding the number of tasks.
     */
    public float getWeight() { return 1.0f; }

    /**
     * Called by InstructTaskQuestion for creating and sending a BML string for the task-specific question window.
     */
    public abstract void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant);

    /**
     * Called by InstructTaskQuestion when receiving the answer of the sendCreateTaskQuestion.
     */
    public abstract boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties);

    /**
     * Called by TalkToServantQuestion when receiving the answer for the talk to question and task's subject is selected.
     */
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) { return true; }

    /**
     * Called by Servant when an item is given to the servant. Should return true if task is overriding handling of the gift.
     */
    public boolean handleGiveTo(Creature performer, Item source) { return false; }

    /**
     * Called by Servant when an item is given to the servant. Should return true if task is accepting the gift to the inventory.
     */
    public boolean acceptItem(Creature performer, Item item) { return false; }

    /**
     * Called by ServantTaskQuestion for creating and sending the BML string for the question window used by the task-specific question.
     */
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) { }

    /**
     * Called by ServantTaskQuestion when receiving the answer of the sendTaskQuestion.
     */
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) { }

    protected boolean parseJSON(String data) {
        try {
            JSONTokener jt = new JSONTokener(data);
            JSONObject jo = new JSONObject(jt);
            return readJSON(jo);
        } catch(JSONException e) {
            logger.log(Level.WARNING, "Failed to load task " + id + ": " + e.getMessage(), e);
        }
        return false;
    }

    protected boolean readJSON(JSONObject jo) {
        return true;
    }

    @Override
    public String toJSONString() {
        return "{}";
    }
}
