package net.spirangle.awakening.tasks;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.SimplePopup;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.tutorial.Mission;
import com.wurmonline.server.tutorial.MissionPerformed;
import com.wurmonline.server.tutorial.MissionPerformer;
import com.wurmonline.server.tutorial.Missions;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MissionTask extends Task {

    private static final Logger logger = Logger.getLogger(MissionTask.class.getName());

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Trigger mission start";
            params.tooltip = "Trigger the start of a mission";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return taskPaper == null || responder == null || responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public String subject;
    public String instruction;
    public int mission;
    public int minRestartTime;
    public boolean restartIncomplete;

    private List<Mission> missionsAvail = null;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_MISSION;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Start mission: " + (subject.length() > 100? subject.substring(0, 97) + "..." : subject);
    }

    @Override
    public String getSubject(Creature responder) {
        return subject;
    }

    @Override
    public void delete() {
        try(Connection con = ModSupportDb.getModSupportDb()) {
            deleteQuestPerformed(con, -AwakeningConstants.TASK_MISSION);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to delete quest performed data.", e);
        }
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 350;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Instruct mission task:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"This task will instruct " + name + " to start a mission.\"}\n" +
                   "   text{text=''}\n" +
                   "   table{cols='2';" +
                   "    label{text='Subject '};input{id='subject';text='';maxchars='100';size='120,20'};\n" +
                   "    label{text='Instruction '};input{id='instruction';maxchars='400';maxlines='5';size='120,100';text=''};\n" +
                   "    label{text=\"Mission to start \"};dropdown{id='mission';size='120,20';options=\"");

        Mission[] missions = Missions.getAllMissions();
        int n = 0;
        missionsAvail = new ArrayList<>();
        for(Mission m : missions)
            if(m.getCreatorType() != 2) {
                missionsAvail.add(m);
                if(n > 0) bml.append(",");
                bml.append(m.getName());
                ++n;
            }

        bml.append("\"};\n" +
                   "    label{text='Minimum restart time '};input{id='minRestartTime';size='120,20';maxchars='5';text='0'};\n" +
                   "   }\n" +
                   "   checkbox{id='restartIncomplete';text=\"Permit restart of incomplete missions\"}\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Instruct'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Start Mission");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        subject = properties.getProperty("subject");
        if(subject.length() <= 0) return false;
        instruction = properties.getProperty("instruction");
        int i = Integer.parseInt(properties.getProperty("mission"));
        try {
            Mission m = missionsAvail.get(i);
            mission = m.getId();
        } catch(IndexOutOfBoundsException ioue) {
            responder.getCommunicator().sendAlertServerMessage("Failed to parse value for mission.");
        }
        minRestartTime = Integer.parseInt(properties.getProperty("minRestartTime"));
        restartIncomplete = "true".equals(properties.getProperty("restartIncomplete"));
        return true;
    }

    @SuppressWarnings("unused")
    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        Mission m = Missions.getMissionWithId(mission);
        if(m == null || m.isInactive()) {
            responder.getCommunicator().sendNormalServerMessage("There is no such active mission [" + mission + "], please talk to a GM about this message.");
            return true;
        }

        MissionPerformer mp = MissionPerformed.getMissionPerformer(responder.getWurmId());
        MissionPerformed mpf = null;
        if(mp != null) {
            mpf = mp.getMission(mission);
            if(mpf != null) {
                if(!mpf.isCompleted() && !mpf.isFailed() && !restartIncomplete) {
                    responder.getCommunicator().sendNormalServerMessage("You need to complete the mission before you can start it again.");
                    return true;
                }
                try(Connection con = ModSupportDb.getModSupportDb();
                    PreparedStatement ps = con.prepareStatement("SELECT ID,PERFORMER,STATE,DATA,STARTTIME,ENDTIME FROM QUESTSPERFORMED WHERE STEAMID=? AND QUEST=?")) {
                    long t = (long) minRestartTime * 60000L, i = -1, p = -1, d = 0, s = 0, e = 0, n = 0, l = 0;
                    float st = 0.0f;
                    ps.setLong(1, responder.isPlayer()? ((Player) responder).getSteamId().getSteamID64() : 0L);
                    ps.setInt(2, -AwakeningConstants.TASK_MISSION);
                    try(ResultSet rs = ps.executeQuery()) {
                        while(rs.next()) {
                            p = rs.getLong(2);
                            n = rs.getLong(5);
                            if(n > l) l = n;
                            if(p == responder.getWurmId()) {
                                i = rs.getLong(1);
                                st = rs.getFloat(3);
                                d = rs.getLong(4);
                                s = n;
                                e = rs.getLong(6);
                            }
                        }
                    }
                    if(minRestartTime > 0L && l > System.currentTimeMillis() - t) {
                        responder.getCommunicator().sendNormalServerMessage("Come back later, you still have to wait some time before restarting this mission.");
                        return true;
                    }
                    if(mpf.isFailed() && !m.hasSecondChance()) {
                        responder.getCommunicator().sendNormalServerMessage("You've failed this mission, and don't get a second chance.");
                    } else if(mpf.isCompleted() && !m.mayBeRestarted()) {
                        responder.getCommunicator().sendNormalServerMessage("You've completed this mission, and it may not be restarted.");
                    } else {
                        if(i > 0) {
                            updateQuestPerformed(con, i, 1.0f, mission, System.currentTimeMillis(), -1);
                        } else {
                            insertQuestPerformed(con, responder, -AwakeningConstants.TASK_MISSION, 1.0f, mission, System.currentTimeMillis(), -1);
                        }
                        mpf.setState(1.0f, responder.getWurmId());
                        startMission(responder, m);
                    }
                    return true;
                } catch(SQLException e) {
                    logger.log(Level.SEVERE, "Failed to load quest performed data.", e);
                }
                return true;
            }
        }
        if(mpf == null) {
            mp = MissionPerformed.startNewMission(mission, responder.getWurmId(), 1.0f);
            mpf = mp.getMission(mission);
        }
        try(Connection con = ModSupportDb.getModSupportDb()) {
            insertQuestPerformed(con, responder, -AwakeningConstants.TASK_MISSION, 1.0f, mission, System.currentTimeMillis(), -1);
            startMission(responder, m);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to insert quest performed data.", e);
        }
        return true;
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        subject = jo.optString("subject");
        instruction = jo.optString("instruction");
        mission = jo.optInt("mission");
        minRestartTime = jo.optInt("minRestartTime");
        restartIncomplete = jo.optBoolean("restartIncomplete");
        return true;
    }

    @Override
    public String toJSONString() {
        return "{subject:" + JSONObject.quote(subject) +
               ",instruction:" + JSONObject.quote(instruction) +
               ",mission:" + mission +
               ",minRestartTime:" + minRestartTime +
               ",restartIncomplete:" + (restartIncomplete? "true" : "false") + "}";
    }

    private void insertQuestPerformed(Connection con, Creature responder, long quest, float state, long data, long startTime, long endTime) {
        try(PreparedStatement ps = con.prepareStatement("INSERT INTO QUESTSPERFORMED (PERFORMER,STEAMID,QUEST,STATE,DATA,STARTTIME,ENDTIME) VALUES(?,?,?,?,?,?,?)")) {
            ps.setLong(1, responder.getWurmId());
            ps.setLong(2, responder.isPlayer()? ((Player) responder).getSteamId().getSteamID64() : 0L);
            ps.setFloat(3, quest);
            ps.setFloat(4, state);
            ps.setLong(5, data);
            ps.setLong(6, startTime);
            ps.setLong(7, endTime);
            ps.executeUpdate();
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to insert quest performed data.", e);
        }
    }

    private void updateQuestPerformed(Connection con, long id, float state, long data, long startTime, long endTime) {
        try(PreparedStatement ps = con.prepareStatement("UPDATE QUESTSPERFORMED SET STATE=?,DATA=?,STARTTIME=?,ENDTIME=? WHERE ID=?")) {
            ps.setFloat(1, state);
            ps.setLong(2, data);
            ps.setLong(3, startTime);
            ps.setLong(4, endTime);
            ps.setLong(5, id);
            ps.executeUpdate();
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to update quest performed data.", e);
        }
    }

    private void deleteQuestPerformed(Connection con, long quest) {
        try(PreparedStatement ps = con.prepareStatement("DELETE FROM QUESTSPERFORMED WHERE PERFORMER=? AND QUEST=?")) {
            ps.setLong(1, quest);
            ps.executeUpdate();
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to delete quest performed data.", e);
        }
    }

    private void startMission(Creature performer, Mission m) {
        String i = instruction;
        if(i == null || i.length() == 0) i = m.getInstruction();
        if(i != null && i.length() > 0) {
            SimplePopup pop = new SimplePopup(performer, "Mission start", i);
            pop.sendQuestion();
        }
    }
}
