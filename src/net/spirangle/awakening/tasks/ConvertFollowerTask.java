package net.spirangle.awakening.tasks;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.QuestionParser;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConvertFollowerTask extends Task {

    private static final Logger logger = Logger.getLogger(ConvertFollowerTask.class.getName());

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Convert to follower of a deity";
            params.tooltip = "GM Task: Allows players to convert to a deity";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return taskPaper == null || responder == null || responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public Deity deity = null;
    private int[] deityIds = null;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_CONVERT_FOLLOWER;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Convert to follower of " + deity.name;
    }

    @Override
    public String getSubject(Creature responder) {
        return "The faith of " + deity.name;
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 250;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Convert followers of a deity:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"This task will instruct " + name + " to convert players into followers of a deity.\"}\n" +
                   "   text{text=''}\n" +
                   "   harray{\n" +
                   "    label{text=\"Which deity? \"}\n" +
                   "    dropdown{id='deity';default='0';options=\"");
        Deity[] deities = Deities.getDeities();
        deityIds = new int[deities.length];
        for(int i = 0; i < deities.length; ++i) {
            if(i > 0) bml.append(',');
            bml.append(deities[i].name);
            deityIds[i] = deities[i].number;
        }
        bml.append("\"}\n" +
                   "   }\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Instruct'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Convert Follower");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        if(deityIds == null) return false;
        String d = properties.getProperty("deity");
        logger.info("Deity: " + d);
        if(d == null) return false;
        int deityId = Integer.parseInt(d);
        logger.info("Deity: " + deityId);
        deity = Deities.getDeity(deityIds[deityId]);
        deityIds = null;
        logger.info("Deity: " + (deity == null? "null" : deity.name));
        return deity != null;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, 0, null);
        stq.sendQuestion();
        return true;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        String deityName = StringUtils.bmlString(deity.name);
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 420;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"The faith of " + deityName + ":\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"" + name + " starts to preach about " + deityName + ":\"}\n" +
                   "   text{type='italic';color='127,255,255';text=\"");
        for(int i = 0; i < deity.altarConvertText1.length; ++i)
            bml.append((i == 0? "" : "\n\n") + deity.altarConvertText1[i]);
        bml.append("\"}\n");
        boolean conv = responder.getDeity() != deity;
        if(conv) {
            bml.append("   text{text=''}\n" +
                       "   text{text=\"Will you convert to " + deityName + "?\"}\n" +
                       "   radio{group='convert';id='yes';text='Yes'}\n" +
                       "   radio{group='convert';id='no';selected='true';text='No'}\n");
            if(responder.getDeity() != null) {
                String deityName2 = StringUtils.bmlString(responder.getDeity().name);
                bml.append("   text{text=''}\n" +
                           "   text{type='bold';color='255,0,0';text=\"If you answer yes, your faith and all your abilities granted by " + deityName2 + " will be lost!\"}\n");
            }
        }
        bml.append("  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - (conv? 240 : 160)) + ",1';text=''}\n");
        if(conv) {
            bml.append("   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='80,20';text='Convert'}\n");
        } else {
            bml.append("   button{id='cancel';size='80,20';text='Close'}\n");
        }
        bml.append("  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        String conv = properties.getProperty("convert");
        if(conv != null) {
            if("no".equals(conv)) {
                responder.getCommunicator().sendNormalServerMessage("You decide not to convert.");
            } else if("yes".equals(conv)) {
                if(QuestionParser.canConvertToDeity(responder, deity)) {
                    try {
                        responder.setChangedDeity();
                        responder.setDeity(deity);
                        responder.setFaith(1.0f);
                        responder.setFavor(1.0f);
                        if(deity.isHateGod()) {
                            responder.setAlignment(Math.min(-1.0f, responder.getAlignment()));
                        } else {
                            responder.setAlignment(Math.max(1.0f, responder.getAlignment()));
                        }
                        responder.getCommunicator().sendNormalServerMessage("You have now converted to " + deity.name + "!");
                    } catch(IOException e) {
                        responder.getCommunicator().sendNormalServerMessage("You failed to convert to " + deity.name + " due to a server error! Please report this to the GMs.");
                        logger.log(Level.SEVERE, responder.getName() + ": " + e.getMessage(), e);
                    }
                }
            }
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        int deityId = jo.optInt("deityId");
        deity = Deities.getDeity(deityId);
        return deity != null;
    }

    @Override
    public String toJSONString() {
        return "{deityId:" + deity.number + "}";
    }
}
