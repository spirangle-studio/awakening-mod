package net.spirangle.awakening.tasks;

import com.wurmonline.server.Items;
import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wurmonline.server.MiscConstants.SEX_MALE;


public class WaiterTask extends Task {

    private static final Logger logger = Logger.getLogger(WaiterTask.class.getName());

    private class Larder {
        public Item larder;
        public boolean tip;

        public Larder(Item larder, boolean tip) {
            this.larder = larder;
            this.tip = tip;
        }
    }

    public static String getAboutTaskTitle(AboutTaskParams params) {
        if(params.getTitle) params.title = "Waiter - sells food from a dedicated larder";
        if(params.getDescription) {
            params.header = "About Task: Waiter";
            params.description = "The waiter task makes the servant sell food from a dedicated larder. When " +
                                 "instructing the task, the larder needs to be standing on the same tile as the " +
                                 "waiter, but may later be moved up to five tiles. If the larder is moved more than " +
                                 "five tiles or destroyed, the task will automatically be dropped. Optionally, the " +
                                 "waiter will accept tip from customers.\n\n" +
                                 "The waiter task costs 10 iron daily.\n\n" +
                                 "Any food placed in the larder will then be sold by the waiter, using the set price " +
                                 "of the food items. Therefore make sure to set the price of all food items before " +
                                 "placing them in the larder. It is also recommended that the larder be locked, the " +
                                 "waiter can access contents of locked larders. Only food placed in the food boxes is " +
                                 "sold, not snowballs in the icebox. If the food takes any damage, the waiter will " +
                                 "ignore it and won't sell it, so only freshly cooked food will be sold.\n\n" +
                                 "To buy food, talk to the waiter and ask for the menu. When buying food, only one " +
                                 "item per food type is listed on the menu, and a maximum of 20 items are listed.\n\n" +
                                 "Waiters will only server food to customers for as long as they have something to " +
                                 "serve on in their inventory, this includes plates, pottery bowls and jars, and " +
                                 "buckets. The employer may give items to serve on, or any player, and the waiter " +
                                 "will accept taking the items until the inventory has been filled. If a player " +
                                 "gives an item to serve on, there's no guarantee the same item will be given back " +
                                 "when serving food next time, but continue buying food and it will eventually be " +
                                 "returned.";
        }
        return params.title;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            Creature npc = params.servant.npc;
            params.title = (npc.getSex() == SEX_MALE? "Waiter" : "Waitress") + ": Sell food";
            params.tooltip = "Sell food from a larder box";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        if(taskPaper == null || responder == null) return true;
        Creature npc = servant.npc;
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        if(items.length >= 1) {
            for(Item item : items)
                if(item.getTemplateId() == ItemList.larder) return true;
        }
        return false;
    }

    public List<Larder> larders = new ArrayList<>();

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_WAITER;
    }

    @Override
    public boolean isGroupedTask() { return true; }

    @Override
    public void addTask(Task task) {
        if(task instanceof WaiterTask) {
            List<Larder> l = ((WaiterTask) task).larders;
            for(Larder larder : l) larders.add(larder);
        }
    }

    @Override
    public String getTitle(Creature responder) {
        String waiter = getWaiterName();
        return waiter + ": Sell food from a larder";
    }

    @Override
    public String getSubject(Creature responder) {
        String waiter = getWaiterName();
        Creature npc = servant.npc;
        float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
        if(alc == 100.0f) return "*burp* Wwrdfth.. ergh.. fudh.. *hickup*";
        if(alc == 95.0f) return "*burp* Wait.. wait.. what?.. Fewd!! *hickup*";
        if(alc >= 90.0f)
            return "*burp* " + (npc.getSex() == SEX_MALE? "Waitersh" : "Waitressh") + ", .. *hickup*.. food!! *hickup*";
        if(alc >= 60.0f) return waiter + ", food! *hickup*!";
        if(alc >= 30.0f) return waiter + ", food! Quickly, I'm hungry!";
        if(alc >= 20.0f) return waiter + ", bring the menu!";
        if(alc >= 10.0f) return waiter + ", bring the menu, please.";
        return waiter + ", I would like to look in the menu, please.";
    }

    @Override
    public long getSalary() { return 10L; }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 350;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Instruct mission task:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"This task will instruct " + name + " to sell food from a larder.\n\n" +
                   "From which larder would you like " + name + " to sell food?\"}\n");
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        int n = 0;
        for(Item item : items)
            if(item.getTemplateId() == ItemList.larder) {
                String itemName = StringUtils.bmlString(item.getName());
                bml.append("   radio{group='larder';id='" + item.getWurmId() + "';text=\"" + itemName + "\"}\n");
                if(++n >= 10) break;
            }
        bml.append("   text{text=''}\n" +
                   "   text{text=\"You may move the larder a maximum of five tiles. If the larder is moved more than " +
                   "five tiles or destroyed, the task will automatically be dropped.\n\n" +
                   "A menu is generated from the food items in the larder, and the set price is used for each item. " +
                   "A maximum of 20 dishes are listed, and only freshly cooked food is served. The food is ignored if it has any damage.\"}\n" +
                   "   text{text=''}\n" +
                   "   checkbox{id='tip';selected='true';text=\"Accept tip from customers\"}\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Instruct'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
//        logger.info("BML: "+bml);
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, getWaiterName());
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        String l = properties.getProperty("larder");
        if(l == null) return false;
        long larderId = Long.parseLong(l);
        try {
            Item larder = Items.getItem(larderId);
            if(isInReach(larder)) {
                boolean tip = "true".equals(properties.getProperty("tip", "true"));
                for(Larder larder2 : larders)
                    if(larder2.larder == larder) {
                        if(larder2.tip != tip) {
                            larder2.tip = tip;
                            updateDb();
                            responder.getCommunicator().sendNormalServerMessage(servant.npc.getName() + " updates the food menu.");
                        }
                        return false;
                    }
                Larder larder3 = new Larder(larder, tip);
                larders.add(larder3);
                return true;
            }
        } catch(NoSuchItemException e) {
            logger.log(Level.WARNING, "No larder exists with the id: " + larderId, e);
        }
        return false;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        Item container = getFoodContainer(0);
        if(container == null) {
            Creature npc = servant.npc;
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: I'm sorry, but I've got nothing to serve your food on.");
        } else {
            ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, 0, null);
            stq.sendQuestion();
        }
        return true;
    }

    @Override
    public boolean acceptItem(Creature performer, Item item) {
        if(isFoodContainer(item)) {
            if(!item.isEmpty(false)) {
                Creature npc = servant.npc;
                performer.getCommunicator().sendNormalServerMessage(npc.getName() + " will only accept empty food containers.");
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        StringBuilder bml = new StringBuilder();
        HashMap<String, Item> foods = new HashMap<String, Item>();
        Item food;
        String name, price;
        boolean update = false;
        boolean buy = true;
        int w = 350;
        int h = 450;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Buy a drink from the bartender:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n");
        foods_list:
        for(Larder larder : larders) {
            if(!isInReach(larder.larder)) {
                removeLarder(larder, false);
                update = true;
                continue;
            }
            for(Item box : larder.larder.getItems()) {
                if(box.getTemplateId() == ItemList.icebox) continue;
                for(Item f : box.getItems()) {
                    if(f.getDamage() > 0.0f) continue;
                    name = f.getName();
                    if(!foods.containsKey(name)) {
                        foods.put(name, f);
                        if(foods.size() >= 20) break foods_list;
                    }
                }
            }
        }
        if(update) updateDb();
        if(foods.size() == 0) {
            bml.append("text{text=\"We are fresh out of dishes for today, please come back when our cook has made more food.\"}");
            buy = false;
        } else {
            bml.append("text{type='italic';text=\"This is today's menu:\"}");
            for(Map.Entry<String, Item> entry : foods.entrySet()) {
                name = StringUtils.bmlString(entry.getKey());
                food = entry.getValue();
                price = new Change(food.getPrice()).getChangeString();
                bml.append("radio{group='food';id=\"" + name + "\";text=\"" + StringUtils.capitalize(name) + ", " + price + "\"}");
            }
            float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
            bml.append("radio{group='food';id='0';text=\"Do nothing\";selected='true'}" +
                       "text{text=''}");
            if(alc < 20.0f) bml.append("checkbox{id='tip';text=\"Tip the " + getWaiterName() + "\"}");
            else bml.append("text{text=\"<tip> " +
                            (alc == 100.0f? "Tph.. tcke *hickup*" :
                             (alc >= 95.0f? "Tip.. take *hickup*" :
                              (alc >= 90.0f? "I.. lve yu.. *hickup*" :
                               (alc >= 60.0f? "You are my bestest friend! *hickup*" :
                                (alc >= 30.0f? "Here you go! Well deserved!" :
                                 "And something extra for you!"))))) + "\"}");
        }
        bml.append("  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n");
        if(buy) {
            bml.append("   text{size='" + (w - 6 - 160) + ",1';text=''}\n" +
                       "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='80,20';text='Buy'}\n");
        } else {
            bml.append("   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                       "   button{id='cancel';size='80,20';text='Cancel'}\n");
        }
        bml.append("  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, true, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        String name = properties.getProperty("food");
        boolean tip = "true".equals(properties.getProperty("tip", "true"));
        if(name == null || "0".equals(name)) return;
        serveFood(responder, name, tip);
    }

    private void serveFood(Creature responder, String name, boolean tip) {
        Creature npc = servant.npc;
        Larder larder = null;
        Item food = null;
        find_food:
        for(Larder l : larders) {
            if(!isInReach(l.larder)) continue;
            for(Item box : l.larder.getItems()) {
                if(box.getTemplateId() == ItemList.icebox) continue;
                for(Item item : box.getItems()) {
                    if(item.getDamage() > 0.0f) continue;
                    if(name.equals(item.getName())) {
                        larder = l;
                        food = item;
                        break find_food;
                    }
                }
            }
        }
        if(food == null) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: Please excuse me, the last of our delicious " + name + " was just served to another customer.");
            return;
        }
        long money = responder.getMoney();
        int price = food.getPrice();
        if(price > money) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: It seems you can't afford the " + name + ", perhaps something else instead?");
            return;
        }
        Item container = getFoodContainer(food.getWeightGrams());
        if(container == null) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: I need something larger to serve this food on.");
            return;
        }
        if(tip) {
            if(!larder.tip) {
                responder.getCommunicator().sendNormalServerMessage(npc.getName() + " does not accept tip for this food.");
            } else {
                float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
                if(alc == 100.0f) price += price * 5;
                else if(alc >= 95.0f) price += price * 2;
                else if(alc >= 90.0f) price += price;
                else if(alc >= 60.0f) price += price / 2;
                else if(alc >= 30.0f) price += price / 4;
                else if(alc >= 20.0f) price += price / 10;
                else price += price / 20;
            }
        }

        if(price > money) price = (int) money;
        try {
            responder.chargeMoney(price);
            servant.takeMoney(price);
        } catch(IOException e) { }
        responder.getCommunicator().sendNormalServerMessage("You pay " + npc.getName() + " " + new Change(price).getChangeString() +
                                                            ", and you receive some " + food.getName() + ".");
        responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: Please return the " + container.getName() + " when you are done eating.");
        container.insertItem(food, true);
        responder.getInventory().insertItem(container, true);
    }

    private void removeLarder(Larder larder, boolean update) {
        if(larder != null) {
            larders.remove(larder);
            if(larders.size() == 0) servant.dropTask(this);
            else if(update) updateDb();
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        JSONArray ja = jo.optJSONArray("larders");
        logger.info("WaiterTask.readJSON(larders: " + ja.length() + ")");
        for(int i = 0; i < ja.length(); ++i) {
            JSONObject jo2 = ja.getJSONObject(i);
            long larderId = jo2.optLong("larder");
            logger.info("WaiterTask.readJSON(larderId: " + larderId + ")");
            try {
                Item larder = Items.getItem(larderId);
                if(isInReach(larder)) {
                    boolean tip = jo2.optBoolean("tip");
                    larders.add(new Larder(larder, tip));
                }
            } catch(NoSuchItemException e) {
                logger.log(Level.WARNING, "No larder exists with the id: " + larderId, e);
            }
        }
        return larders.size() >= 1;
    }

    @Override
    public String toJSONString() {
        StringBuilder json = new StringBuilder();
        json.append("{larders:[");
        int n = 0;
        for(Larder larder : larders) {
            if(n > 0) json.append(',');
            json.append("{larder:").append(larder.larder.getWurmId());
            json.append(",tip:").append(larder.tip? "true" : "false");
            json.append('}');
            ++n;
        }
        json.append("]}");
        return json.toString();
    }

    private boolean isFoodContainer(Item item) {
        switch(item.getTemplateId()) {
            case ItemList.bowlPottery:
            case ItemList.jarPottery:
            case ItemList.plate:
            case ItemList.bucketSmall:
                return true;
        }
        return false;
    }

    private Item getFoodContainer(int amount) {
        Creature npc = servant.npc;
        for(Item i : npc.getInventory().getItems()) {
            logger.info("WaiterTask.getFoodContainer(" + i.getName() + ": getFreeVolume[" + i.getFreeVolume() + "], amount[" + amount + "])");
            if(isFoodContainer(i) && i.isEmpty(false) && (amount <= 0 || amount <= i.getFreeVolume()))
                return i;
        }
        return null;
    }

    private String getWaiterName() {
        return servant.npc.getSex() == SEX_MALE? "Waiter" : "Waitress";
    }

    private boolean isInReach(Item larder) {
        Creature npc = servant.npc;
        if(larder != null && npc != null && larder.isOnSurface() == npc.isOnSurface()) {
            return Creature.getRange(npc, larder.getPosX(), larder.getPosY()) <= 20.0;
        }
        return false;
    }
}
