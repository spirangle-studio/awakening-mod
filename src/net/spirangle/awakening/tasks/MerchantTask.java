package net.spirangle.awakening.tasks;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.economy.Economy;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.items.*;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.MarketQuestion.MarketInfo;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.constants.ItemMaterials;
import com.wurmonline.shared.util.MaterialUtilities;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.players.EconomyData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.DAY_AS_MILLIS;


public class MerchantTask extends Task {

    private static final Logger logger = Logger.getLogger(MerchantTask.class.getName());

    public static final byte CATEGORY_NONE = 0;
    public static final byte CATEGORY_ALL = 1;
    public static final byte CATEGORY_DIRT = 11;
    public static final byte CATEGORY_SAND = 12;
    public static final byte CATEGORY_LOGS = 13;
    public static final byte CATEGORY_ROCKS = 14;
    public static final byte CATEGORY_CLAY = 15;
    public static final byte CATEGORY_COAL = 16;
    public static final byte CATEGORY_ORES = 17;
    public static final byte CATEGORY_LUMPS = 18;
    public static final byte CATEGORY_CEREALS = 21;
    public static final byte CATEGORY_VEGETABLES = 22;
    public static final byte CATEGORY_FRUIT_BERRIES = 23;
    public static final byte CATEGORY_HERBS_SPICES = 24;
    public static final byte CATEGORY_NUTS = 25;
    public static final byte CATEGORY_MUSHROOMS = 26;
    public static final byte CATEGORY_SEEDS = 27;
    public static final byte CATEGORY_SPROUTS = 28;
    public static final byte CATEGORY_SEEDLINGS = 29;
    public static final byte CATEGORY_FLOWERS = 30;
    public static final byte CATEGORY_COTTON_WOOL = 31;
    public static final byte CATEGORY_WEMP = 32;
    public static final byte CATEGORY_GRASS_THATCH = 33;
    public static final byte CATEGORY_MEAT = 51;
    public static final byte CATEGORY_FISH = 52;
    public static final byte CATEGORY_EGGS = 53;
    public static final byte CATEGORY_FURS = 54;
    public static final byte CATEGORY_HIDES_LEATHER = 55;
    public static final byte CATEGORY_MILK_DAIRY = 56;
    public static final byte CATEGORY_HONEY = 61;
    public static final byte CATEGORY_OIL = 62;
    public static final byte CATEGORY_PAPER = 71;
    public static final byte CATEGORY_ROPES = 72;
    public static final byte CATEGORY_TAILORING = 101;
    public static final byte CATEGORY_ALCHEMY = 102;
    public static final byte CATEGORY_BUTCHERING = 103;
    public static final byte CATEGORY_COOKING = 104;
    public static final byte CATEGORY_BEVERAGES = 105;
    public static final byte CATEGORY_MILLING = 106;
    public static final byte CATEGORY_CARPENTRY = 107;
    public static final byte CATEGORY_MASONRY = 108;
    public static final byte CATEGORY_SMITHING = 109;

    private static final int MERCHANT_ID_TALK_TO = -1;
    private static final int MERCHANT_ID_CANCEL = 0;
    private static final int MERCHANT_ID_START = 1;
    private static final int MERCHANT_ID_SELL = 2;
    private static final int MERCHANT_ID_TRADES = 3;
    private static final int MERCHANT_ID_DETAILS = 4;
    private static final int MERCHANT_ID_WARES = 5;
    private static final int MERCHANT_ID_BUY = 6;


    private static class Category {
        byte id;
        String name;
        String trade;

        Category(byte id, String name) {
            this(id, name, name);
        }

        Category(byte id, String name, String trade) {
            this.id = id;
            this.name = name;
            this.trade = trade;
        }
    }


    public static class Ware {
        int templateId;
        byte category;
        public String name;
        public float price;
        public double supplyDemandFactor;
        public long decayFactor;
        public boolean buy;
        public boolean sell;
        boolean hasDecayed;

        Ware(int templateId, byte category, String name) {
            this.templateId = templateId;
            this.category = category;
            this.name = name;
            this.price = 1.0f;
            this.supplyDemandFactor = 20.0;
            this.decayFactor = 360;
            this.buy = true;
            this.sell = true;
            this.hasDecayed = false;
        }
    }


    private static class Trade {
        Item container;
        Category category;
        float minQl;
        float profit;

        Trade(Item container, Category category, float minQl, float profit) {
            this.container = container;
            this.category = category;
            this.minQl = minQl;
            this.profit = profit;
        }
    }


    private static class Deal {
        public final Item ware;
        public final Trade trade;
        public final float ql;
        public final int weight;
        public final long price;

        Deal(Item ware, Trade trade, float ql, int weight, long price) {
            this.ware = ware;
            this.trade = trade;
            this.ql = ql;
            this.weight = weight;
            this.price = price;
        }
    }


    private static class Customer {
        Category category;
        Trade trade;
        Ware[] wares;
        Ware ware;
        int material;
        int amount;
        float minQl;
        ItemTemplate template;
        Map<Long, Deal> deal;

        Customer() {
            this.category = null;
            this.trade = null;
            this.wares = null;
            this.ware = null;
            this.material = ItemMaterials.MATERIAL_UNDEFINED;
            this.amount = 0;
            this.minQl = 0.0f;
            this.template = null;
            this.deal = null;
        }
    }


    private static final Category[] categories = {
            new Category(CATEGORY_ALL, "general bulk wares", "all trades"),
            new Category(CATEGORY_ALCHEMY, "alchemy bulk wares", "alchemy"),
            new Category(CATEGORY_BEVERAGES, "beverages"),
            new Category(CATEGORY_BUTCHERING, "butchering products", "butchering"),
            new Category(CATEGORY_CARPENTRY, "carpentry bulk wares", "carpentry"),
            new Category(CATEGORY_CEREALS, "cereals"),
            new Category(CATEGORY_CLAY, "clay"),
            new Category(CATEGORY_COAL, "coal and peat", "coal"),
            new Category(CATEGORY_COOKING, "cooking bulk wares", "cooking"),
            new Category(CATEGORY_COTTON_WOOL, "cotton and wool"),
            new Category(CATEGORY_DIRT, "dirt"),
            new Category(CATEGORY_EGGS, "eggs"),
            new Category(CATEGORY_FISH, "fish"),
            new Category(CATEGORY_FLOWERS, "flowers"),
            new Category(CATEGORY_FRUIT_BERRIES, "fruits and berries"),
            new Category(CATEGORY_FURS, "furs"),
            new Category(CATEGORY_GRASS_THATCH, "grass and thatch"),
            new Category(CATEGORY_HERBS_SPICES, "herbs and spices"),
            new Category(CATEGORY_HIDES_LEATHER, "hides and leather"),
            new Category(CATEGORY_HONEY, "honey"),
            new Category(CATEGORY_LOGS, "logs"),
            new Category(CATEGORY_LUMPS, "lumps"),
            new Category(CATEGORY_MASONRY, "masonry bulk wares", "masonry"),
            new Category(CATEGORY_MEAT, "meat"),
            new Category(CATEGORY_MILK_DAIRY, "milk and dairy products", "milk and dairy"),
            new Category(CATEGORY_MILLING, "milling products", "milling"),
            new Category(CATEGORY_MUSHROOMS, "mushrooms"),
            new Category(CATEGORY_NUTS, "nuts"),
            new Category(CATEGORY_OIL, "oils"),
            new Category(CATEGORY_ORES, "ores"),
            new Category(CATEGORY_PAPER, "paper"),
            new Category(CATEGORY_ROCKS, "rock shards", "rocks"),
            new Category(CATEGORY_ROPES, "ropes"),
            new Category(CATEGORY_SAND, "sand"),
            new Category(CATEGORY_SEEDLINGS, "seedlings"),
            new Category(CATEGORY_SEEDS, "seeds"),
            new Category(CATEGORY_SMITHING, "smithing bulk wares", "smithing"),
            new Category(CATEGORY_SPROUTS, "sprouts"),
            new Category(CATEGORY_TAILORING, "tailoring bulk wares", "tailoring"),
            new Category(CATEGORY_VEGETABLES, "vegetables"),
            new Category(CATEGORY_WEMP, "wemp"),
            };

    private static final String categoriesOptions;

    static {
        StringBuilder sb = new StringBuilder();
        for(Category c : categories) {
            if(sb.length() > 0) sb.append(',');
            sb.append(c.trade);
        }
        categoriesOptions = sb.toString();
    }

    private static final String materialsOptions = "any," +
                                                   "adamantine,alloy,animal,applewood," +
                                                   "barley,bear,beef,birchwood,blueberrywood,bone,brass,bronze," +
                                                   "camelliawood,canine,cedarwood,charcoal,cherrywood,chestnut,clay,copper,cotton,crystal," +
                                                   "dairy,diamond,dragon," +
                                                   "electrum," +
                                                   "fat,feline,fire,firwood,flesh,fowl," +
                                                   "game,glass,glimmersteel,gold,grapewood," +
                                                   "hazelnutwood,honey,horse,human,humanoid," +
                                                   "insect,iron,ivy," +
                                                   "lamb,lavenderwood,lead,leather,lemonwood,lindenwood,lingonberrywood," +
                                                   "magic,maplewood,marble,meat,metal,moonmetal," +
                                                   "oakenwood,oat,oil,oleanderwood,olivewood,orangewood," +
                                                   "paper,peat,pinewood,pork,pottery," +
                                                   "raspberrywood,reed,rosewood,rye," +
                                                   "salt,sandstone,seafood,seryll,silver,slate,snake,steel,stone,straw," +
                                                   "tar,thorn,tin,tough," +
                                                   "vegetarian," +
                                                   "walnut,water,wemp,wheat,willow,wool," +
                                                   "zinc";

    private static final Category[] categoriesIndex;
    public static final Ware[] wares;
    public static final HashMap<Integer, Ware> waresIndex;

    private static final float[] minQls = { 0.0f/*any*/, 1.0f, 5.0f, 10.0f, 20.0f, 30.0f, 40.0f, 50.0f, 60.0f, 70.0f, 80.0f, 90.0f, 95.0f, 96.0f, 97.0f, 98.0f, 99.0f, 100.0f };
    private static final float[] profits = { 0.5f, 0.75f, 0.8f, 0.9f, 1.0f, 1.05f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.75f, 2.0f, 2.5f, 3.0f, 4.0f, 5.0f, 7.5f, 10f };
    private static final int[] amounts = { 0/*any*/, 1, 2, 5, 10, 20, 30, 50, 100, 200, 300, 500, 1000, 2000, 5000, 10000 };
    private static final int[] materials = { ItemMaterials.MATERIAL_UNDEFINED,
                                             ItemMaterials.MATERIAL_ADAMANTINE, ItemMaterials.MATERIAL_METALFRAG_ALLOY, ItemMaterials.MATERIAL_ANIMAL, ItemMaterials.MATERIAL_WOOD_APPLE,
                                             ItemMaterials.MATERIAL_BARLEY, ItemMaterials.MATERIAL_MEAT_BEAR, ItemMaterials.MATERIAL_MEAT_BEEF, ItemMaterials.MATERIAL_WOOD_BIRCH,
                                             ItemMaterials.MATERIAL_WOOD_BLUEBERRY, ItemMaterials.MATERIAL_BONE, ItemMaterials.MATERIAL_BRASS, ItemMaterials.MATERIAL_BRONZE,
                                             ItemMaterials.MATERIAL_WOOD_CAMELLIA, ItemMaterials.MATERIAL_MEAT_CANINE, ItemMaterials.MATERIAL_WOOD_CEDAR, ItemMaterials.MATERIAL_COAL,
                                             ItemMaterials.MATERIAL_WOOD_CHERRY, ItemMaterials.MATERIAL_WOOD_CHESTNUT, ItemMaterials.MATERIAL_CLAY, ItemMaterials.MATERIAL_COPPER,
                                             ItemMaterials.MATERIAL_COTTON, ItemMaterials.MATERIAL_CRYSTAL,
                                             ItemMaterials.MATERIAL_DAIRY, ItemMaterials.MATERIAL_DIAMOND, ItemMaterials.MATERIAL_MEAT_DRAGON,
                                             ItemMaterials.MATERIAL_ELECTRUM,
                                             ItemMaterials.MATERIAL_FAT, ItemMaterials.MATERIAL_MEAT_CAT, ItemMaterials.MATERIAL_FIRE, ItemMaterials.MATERIAL_WOOD_FIR, ItemMaterials.MATERIAL_FLESH,
                                             ItemMaterials.MATERIAL_MEAT_FOWL,
                                             ItemMaterials.MATERIAL_MEAT_GAME, ItemMaterials.MATERIAL_GLASS, ItemMaterials.MATERIAL_GLIMMERSTEEL, ItemMaterials.MATERIAL_GOLD, ItemMaterials.MATERIAL_WOOD_GRAPE,
                                             ItemMaterials.MATERIAL_WOOD_HAZELNUT, ItemMaterials.MATERIAL_HONEY, ItemMaterials.MATERIAL_MEAT_HORSE, ItemMaterials.MATERIAL_MEAT_HUMAN,
                                             ItemMaterials.MATERIAL_MEAT_HUMANOID,
                                             ItemMaterials.MATERIAL_MEAT_INSECT, ItemMaterials.MATERIAL_IRON, ItemMaterials.MATERIAL_WOOD_IVY,
                                             ItemMaterials.MATERIAL_MEAT_LAMB, ItemMaterials.MATERIAL_WOOD_LAVENDER, ItemMaterials.MATERIAL_LEAD, ItemMaterials.MATERIAL_LEATHER,
                                             ItemMaterials.MATERIAL_WOOD_LEMON, ItemMaterials.MATERIAL_WOOD_LINDEN, ItemMaterials.MATERIAL_WOOD_LINGONBERRY,
                                             ItemMaterials.MATERIAL_MAGIC, ItemMaterials.MATERIAL_WOOD_MAPLE, ItemMaterials.MATERIAL_MARBLE, ItemMaterials.MATERIAL_MEAT,
                                             ItemMaterials.MATERIAL_METALFRAG_BASE, ItemMaterials.MATERIAL_METALFRAG_MOON,
                                             ItemMaterials.MATERIAL_WOOD_OAK, ItemMaterials.MATERIAL_OAT, ItemMaterials.MATERIAL_OIL, ItemMaterials.MATERIAL_WOOD_OLEANDER,
                                             ItemMaterials.MATERIAL_WOOD_OLIVE, ItemMaterials.MATERIAL_WOOD_ORANGE,
                                             ItemMaterials.MATERIAL_PAPER, ItemMaterials.MATERIAL_PEAT, ItemMaterials.MATERIAL_WOOD_PINE, ItemMaterials.MATERIAL_MEAT_PORK, ItemMaterials.MATERIAL_POTTERY,
                                             ItemMaterials.MATERIAL_WOOD_RASPBERRY, ItemMaterials.MATERIAL_REED, ItemMaterials.MATERIAL_WOOD_ROSE, ItemMaterials.MATERIAL_RYE,
                                             ItemMaterials.MATERIAL_SALT, ItemMaterials.MATERIAL_SANDSTONE, ItemMaterials.MATERIAL_MEAT_SEAFOOD, ItemMaterials.MATERIAL_SERYLL,
                                             ItemMaterials.MATERIAL_SILVER, ItemMaterials.MATERIAL_SLATE, ItemMaterials.MATERIAL_MEAT_SNAKE, ItemMaterials.MATERIAL_STEEL,
                                             ItemMaterials.MATERIAL_STONE, ItemMaterials.MATERIAL_STRAW,
                                             ItemMaterials.MATERIAL_TAR, ItemMaterials.MATERIAL_WOOD_THORN, ItemMaterials.MATERIAL_TIN, ItemMaterials.MATERIAL_MEAT_TOUGH,
                                             ItemMaterials.MATERIAL_VEGETARIAN,
                                             ItemMaterials.MATERIAL_WOOD_WALNUT, ItemMaterials.MATERIAL_WATER, ItemMaterials.MATERIAL_WEMP, ItemMaterials.MATERIAL_WHEAT,
                                             ItemMaterials.MATERIAL_WOOD_WILLOW, ItemMaterials.MATERIAL_WOOL,
                                             ItemMaterials.MATERIAL_ZINC };


    public static int getItemCategory(int id) {
        Ware ware = waresIndex.get(id);
        if(ware == null) return CATEGORY_NONE;
        return ware.category;
    }

    public static String getAboutTaskTitle(AboutTaskParams params) {
        if(params.getTitle) params.title = "Merchant - buys and sells bulk wares";
        if(params.getDescription) {
            params.header = "About Task: Merchant";
            params.description = "The merchant task makes the servant buy and sell wares from a container such as a " +
                                 "crate, a food storage bin, or a barrel. When instructing the task, the container " +
                                 "needs to be standing on the same tile as the merchant, but may later be moved up to " +
                                 "five tiles. If the container is moved more than five tiles or destroyed, the task " +
                                 "will automatically be dropped. Note that only liquids are traded in barrels. You can " +
                                 "give the merchant task more than once, for each container you wish to use.\n\n" +
                                 "The merchants employed by players use default settings, and buy at a slightly lower " +
                                 "price, and sell at a slightly higher price, than the merchants in the cities. " +
                                 "Instead they remove the need to travel to the cities to trade wares.\n\n" +
                                 "The merchant task costs 10 iron daily.\n\n" +
                                 "When selling wares, place the items in the dedicated container, and talk to the " +
                                 "merchant. You will be given a price which you can either accept or decline. When " +
                                 "buying wares, first select which trade and continue, then select what type of item " +
                                 "and you may also optionally narrow the search for a specific material, amount or " +
                                 "quality; then a list of wares comes up and if you select to buy the bought wares will " +
                                 "be placed in the dedicated container.\n\n" +
                                 "All sold wares are stored in a global market database. Wares in the market database " +
                                 "remains locked for a month of Wurm time (3 days and 18h), and after that time becomes " +
                                 "available for buying. Bought wares are removed from the market database. All wares " +
                                 "have decay, which means after some time the wares are removed from the market " +
                                 "database and cannot be bought, such wares are destroyed. The amount of time for a " +
                                 "ware to decay depends on the type of item, it varies between a couple of months up " +
                                 "to a year, real time.\n\n" +
                                 "Prices are calculated using a complicated formula, which takes into account quality " +
                                 "of the wares, but also availability and many other factors. As a type of ware starts " +
                                 "to saturate the market, the merchants will buy the ware at a lower and lower price, " +
                                 "and eventually they refuse buying any more. Once the wares are either sold or decays, " +
                                 "the prices will go up again.";
        }
        return params.title;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "Merchant: Buy and sell wares";
            params.tooltip = "Buy and sell wares from a dedicated container";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        if(taskPaper == null || responder == null) return true;
        Creature npc = servant.npc;
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        if(items.length >= 1)
            for(Item item : items)
                if(isContainer(item)) return true;
        //		responder.getCommunicator().sendNormalServerMessage("There is no sealed wine barrel near "+npc.getName()+".");
        return false;
    }

    public static void pollMarketDecay() {
        int n = Server.rand.nextInt(wares.length);
        Ware ware = wares[n];
        if(ware.hasDecayed || ware.decayFactor <= 0L) {
            int i;
            for(i = n + 1; true; ++i) {
                if(i >= wares.length) i = 0;
                if(i == n) {
                    for(i = 0; i < wares.length; ++i)
                        wares[i].hasDecayed = false;
                    break;
                } else if(!wares[i].hasDecayed && wares[i].decayFactor > 0L) {
                    ware = wares[i];
                    break;
                }
            }
        }
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("DELETE FROM MARKET WHERE TEMPLATEID=? AND (SELLDATE<=? OR QUALITYLEVEL<=0.1)");
            PreparedStatement ps2 = con.prepareStatement("UPDATE MARKET SET PRICE=round(PRICE*?),QUALITYLEVEL=(QUALITYLEVEL*?) WHERE TEMPLATEID=? AND SELLDATE<=?")) {
            ps.setInt(1, ware.templateId);
            ps.setLong(2, System.currentTimeMillis() - DAY_AS_MILLIS * ware.decayFactor);
            ps.executeUpdate();
            if(ware.decayFactor > 0) {
                double decay = 0.02d + (Server.rand.nextDouble() * 0.08d);
                ps2.setDouble(1, 1.0d - decay * 0.5d);
                ps2.setDouble(2, 1.0d - decay * 1.5d);
                ps2.setInt(3, ware.templateId);
                ps2.setLong(4, System.currentTimeMillis() - DAY_AS_MILLIS * (ware.decayFactor - 60));
                ps2.executeUpdate();
            } else {
            }
            ware.hasDecayed = true;
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to delete market data: " + e.getMessage(), e);
        }
    }

    public static List<MarketInfo> getMarketInfo(Player performer, ItemTemplate template) {
        Ware ware = waresIndex.get(template.getTemplateId());
        List<MarketInfo> result = new ArrayList<>();
        if(ware == null) return result;
        for(Servant s : Servant.servants) {
            MerchantTask merchant = (MerchantTask) s.getTaskByType(AwakeningConstants.TASK_MERCHANT);
            if(merchant != null) {
                for(Trade trade : merchant.trades) {
                    if(trade.category.id == ware.category || trade.category.id == CATEGORY_ALL) {
                        String village = s.npc.currentVillage != null && !s.npc.currentVillage.isPermanent? s.npc.currentVillage.getName() : null;
                        String kingdom = Kingdoms.getChatNameFor(s.npc.currentKingdom);
                        String location = village == null? kingdom : village + " (" + kingdom.substring(0, 3) + ")";
                        String container = trade.container != null? trade.container.getTemplate().getName() : "";
                        result.add(new MarketInfo(s.npc.getName(), trade.category.trade, location, container));
                        break;
                    }
                }
            }
        }
        return result;
    }

    private static boolean isContainer(Item item) {
        return isContainer(item.getTemplate().getTemplateId());
    }

    private static boolean isContainer(int templateId) {
        return templateId == ItemList.crateSmall ||
               templateId == ItemList.crateLarge ||
               templateId == ItemList.bulkContainer ||
               templateId == ItemList.hopper ||
               templateId == ItemList.barrelSmall ||
               templateId == ItemList.barrelLarge;
    }

    private static boolean isLiquidContainer(Item item) {
        return isLiquidContainer(item.getTemplate().getTemplateId());
    }

    private static boolean isLiquidContainer(int templateId) {
        return templateId == ItemList.barrelSmall ||
               templateId == ItemList.barrelLarge;
    }

    private static boolean isFoodContainer(Item item) {
        return isFoodContainer(item.getTemplate().getTemplateId());
    }

    private static boolean isFoodContainer(int templateId) {
        return templateId == ItemList.crateSmall ||
               templateId == ItemList.crateLarge ||
               templateId == ItemList.hopper;
    }

    private static int compareContainers(Item item1, Item item2) {
        if(item1.isCrate()) {
            if(item2.isCrate()) return item1.getRemainingCrateSpace() > item2.getRemainingCrateSpace()? 1 : -1;
            return -1;
        }
        if(item2.isCrate()) return 1;
        int templateId1 = item1.getTemplate().getTemplateId();
        int templateId2 = item2.getTemplate().getTemplateId();
        if(templateId1 == ItemList.barrelLarge && templateId2 == ItemList.barrelSmall) return 1;
        if(templateId1 == ItemList.barrelSmall && templateId2 == ItemList.barrelLarge) return -1;
        return 0;
    }

    private static String testLiquidContainer(Item container, ItemTemplate template) {
        if(!isLiquidContainer(container)) return "The " + container.getName() + " cannot hold liquids.";
        Set<Item> items = container.getItems();
        if(!items.isEmpty()) {
            if(items.size() > 1) return "There are too many items in the " + container.getName() + ".";
            Item item = items.stream().findFirst().orElse(null);
            return "The " + container.getName() + " already contains " + item.getName() + ", empty it first.";
        }
        return null;
    }

    private final List<Trade> trades = new ArrayList<>();
    private final boolean[] tradesCategories = new boolean[categoriesIndex.length];
    private Category[] buyCategories = null;

    @Override
    protected void init(long id, float ql, Item taskPaper, Servant servant) {
        super.init(id, ql, taskPaper, servant);
        updateTrades();
    }

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_MERCHANT;
    }

    @Override
    public boolean isGroupedTask() {
        return true;
    }

    @Override
    public void addTask(Task task) {
        if(task instanceof MerchantTask) {
            List<Trade> t = ((MerchantTask) task).trades;
            trades.addAll(t);
            updateTrades();
        }
    }

    @Override
    public String getTitle(Creature responder) {
        return "Merchant: Buy and sell wares";
    }

    @Override
    public String getSubject(Creature responder) {
        return "Merchant, I would like to make a trade.";
    }

    @Override
    public long getSalary() { return 10L; }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 350;
        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Instruct servant in merchant task:\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='").append(question.getId()).append("'};\n")
           .append("   text{text=''}\n")
           .append("   text{text=\"This task will instruct ").append(name).append(" to buy and sell wares from a container.\n\n")
           .append("Which container would you like to store merchandise in during trades?\"}\n");
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        int n = 0;
        for(Item item : items) {
            if(isContainer(item) && item.isViewableBy(responder)) {
                String itemName = StringUtils.bmlString(item.getName());
                bml.append("   radio{group='container';id='").append(item.getWurmId()).append("';text=\"").append(itemName).append("\"}\n");
                if(++n >= 10) break;
            }
        }
        bml.append("   text{text=''}\n")
           .append("   text{text=\"You may move the container a maximum of five tiles. If the container is moved more than ")
           .append("five tiles or destroyed, the task will automatically be dropped.\"}\n");
        if(responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
            bml.append("   text{text=''}")
               .append("   text{text=\"What trade will the merchant specialize in?\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='category';default='0';options=\"").append(categoriesOptions).append("\"}\n")
               .append("   }\n")
               .append("   text{text=''}\n")
               .append("   text{text=\"Minimum QL of wares trader accepts trading in?\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='minql';default='0';options=\"1,5,10,20,30,40,50,60,70,80,90,95,96,97,98,99,100\"}\n")
               .append("   }\n")
               .append("   text{text=''}\n")
               .append("   text{text=\"Profit modifier?\";hover=\"0.5=trader makes a 50% loss, 1=trader makes no profit, 2=trader doubles price, etc.\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='profit';default='13';options=\"0.5,0.75,0.8,0.9,1,1.05,1.1,1.2,1.3,1.4,1.5,1.75,2,2.5,3,4,5,7.5,10\"}\n")
               .append("   }\n");
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='start';size='80,20';text='Back'}\n")
           .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
           .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
           .append("   button{id='submit';size='80,20';text='Instruct'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Merchant");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        String c = properties.getProperty("container");
        if(c == null) return false;
        long containerId = Long.parseLong(c);
        try {
            Item container = Items.getItem(containerId);
            if(isInReach(container)) {
                int categoryId = categories[Integer.parseInt(properties.getProperty("category", "0"))].id;
                Category category = categoriesIndex[categoryId];
                float minQl = minQls[Integer.parseInt(properties.getProperty("minql", "0")) + 1];
                float profit = profits[Integer.parseInt(properties.getProperty("profit", "14"))];
                for(Trade trade : trades)
                    if(trade.container == container && trade.category == category) {
                        if(trade.minQl != minQl || trade.profit != profit) {
                            trade.minQl = minQl;
                            trade.profit = profit;
                            updateDb();
                            updateTrades();
                            responder.getCommunicator().sendNormalServerMessage(servant.npc.getName() + " updates trades.");
                        }
                        return false;
                    }
                Trade trade = new Trade(container, category, minQl, profit);
                trades.add(trade);
                updateTrades();
                return true;
            }
        } catch(NoSuchItemException e) {
            logger.log(Level.WARNING, "No container exists with the id: " + containerId, e);
        }
        return false;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, MERCHANT_ID_START, new Customer());
        stq.sendQuestion();
        return true;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        Customer customer = (Customer) data;
        String title = servant.npc.getName();
        StringBuilder bml = new StringBuilder();
        StringBuilder passthrough = new StringBuilder();
        int buttons = MERCHANT_ID_CANCEL;
        int back = MERCHANT_ID_TALK_TO;
        int w = 350;
        int h = 350;
        Map<Item, Trade> wares = null;

        passthrough.append("passthrough{id='id';text='").append(question.getId()).append("'};");

        bml.append("border{\n");

        if(id == MERCHANT_ID_START) {
            boolean update = false;
            long p;
            wares = new HashMap<>();
            for(Trade trade : trades) {
                if(!isInReach(trade.container)) {
                    removeTrade(trade, false);
                    update = true;
                } else {
                    for(Item item : trade.container.getItems()) {
                        int t = item.getTemplateId();
                        if(item.isBulkItem()) t = item.getRealTemplateId();
                        p = getSellPrice(trade, item);
                        if(p <= 0L) continue;
                        int ic = getItemCategory(t);
                        if(ic == CATEGORY_NONE || ic < 0 || ic >= categoriesIndex.length) continue;
                        Category c = categoriesIndex[ic];
                        if(c == trade.category || (c != null && trade.category.id == CATEGORY_ALL)) {
                            Trade trade2 = wares.get(item);
                            if(trade2 != null && trade2.profit <= trade.profit) continue;
                            wares.put(item, trade);
                        }
                    }
                }
                if(wares.size() >= 50) break;
            }
            if(update && !trades.isEmpty()) updateDb();
            if(!wares.isEmpty()) id = MERCHANT_ID_SELL;
        }
        if(id != MERCHANT_ID_SELL && id != MERCHANT_ID_WARES) {
            bml.append(" varray{rescale='true';\n")
               .append("  text{type='bold';text=\"Talking to merchant:\"}\n")
               .append(" };\n")
               .append(" null;\n")
               .append(" scroll{horizontal='false';vertical='true';\n")
               .append("  varray{rescale='true';\n");
            if(id == MERCHANT_ID_START) {
                int i, n;
                if(trades.size() == 0) {
                    bml.append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"Hello! I don't currently trade in anything.\"}\n");
                } else {
                    boolean gm = responder.getPower() >= MiscConstants.POWER_DEMIGOD;
                    bml.append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"Hello! I specialize in the following trades:\"}\n")
                       .append("   text{text=''}\n")
                       .append("   table{cols='").append(gm? 3 : 2).append("';\n")
                       .append("    label{type='bold';text='Trade'};\n")
                       .append("    label{type='bold';text='Container'};\n");
                    if(gm) bml.append("    label{type='bold';text='GM Only'};\n");
                    n = 0;
                    for(Trade trade : trades) {
                        bml.append("    label{text=\"").append(trade.category.name).append("\"};\n")
                           .append("    label{text=\"").append(StringUtils.bmlString(trade.container.getName())).append("\"};\n");
                        if(gm)
                            bml.append("    label{text=\"[minQl=").append(trade.minQl).append(", profit=").append(trade.profit).append("]\"};\n");
                        ++n;
                    }
                    bml.append("   }\n")
                       .append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"Would you like to buy wares? What type of wares are you looking for?\"}\n")
                       .append("   harray{\n")
                       .append("    dropdown{id='buy';options=\"");
                    for(i = 0, n = 0; i < buyCategories.length; ++i) {
                        if(n > 0) bml.append(',');
                        bml.append(buyCategories[i].name);
                        ++n;
                    }
                    bml.append("\"}\n")
                       .append("   }\n")
                       .append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"If you intend to sell, place your wares in the dedicated container; ")
                       .append("then come talk to me again and we will make a bargain. Place liquids in barrels, food wares in ")
                       .append("a food storage bin, and other bulk wares in a crate or bulk storage bin.\"}\n");
                    buttons = MERCHANT_ID_TRADES;
                    w = 450;
                }
            } else if(id == MERCHANT_ID_DETAILS) {
                if(customer.wares == null || customer.wares.length == 0) {
                    bml.append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"I'm sorry, but I don't have any wares of that kind here.\"}\n");
                    back = MERCHANT_ID_START;

                } else {
                    int colw = 120;
                    bml.append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"Can you be a bit more specific, what you are looking for:\"}\n")
                       .append("   text{text=''}\n")
                       .append("   harray{text{size='").append(colw).append(",19';text=\"Trade: \"};label{text=\"").append(customer.category.name).append("\"}}\n")
                       .append("   harray{text{size='").append(colw).append(",19';text=\"Type of item: \"};dropdown{id='item';size='250,17';options=\"");
                    ItemTemplateFactory itf = ItemTemplateFactory.getInstance();
                    for(int i = 0, n = 0; i < customer.wares.length; ++i) {
                        if(n > 0) bml.append(',');
                        int templId = customer.wares[i].templateId;
                        ItemTemplate it = itf.getTemplateOrNull(templId);
                        if(it == null) bml.append("something unknown");
                        else bml.append(StringUtils.bmlString(it.getPlural()));
                        ++n;
                    }
                    bml.append("\"}}\n")
                       .append("   harray{text{size='").append(colw).append(",19';text=\"Material: \"};dropdown{id='material';size='250,17';options=\"").append(materialsOptions).append("\"}}\n")
                       .append("   harray{text{size='").append(colw).append(",19';text=\"Maximum amount: \"};dropdown{id='amount';size='250,17';options=\"any,1,2,5,10,20,30,50,100,200,300,500,1000,2000,5000,10000\"}}\n")
                       .append("   harray{text{size='").append(colw).append(",19';text=\"Minimum quality: \"};dropdown{id='ql';size='250,17';options=\"any,1.00,5.00,10.00,20.00,30.00,40.00,50.00,60.00,70.00,80.00,90.00,95.00,96.00,97.00,98.00,99.00,100.00\"}}\n");
                    buttons = MERCHANT_ID_DETAILS;
                    w = 450;
                }
            }
            bml.append("  }\n")
               .append(" };\n");
        } else {
            title = "Market";
            w = 450;
            h = 450;
            bml.append(" null;\n")
               .append(" null;\n")
               .append(" varray{rescale='true';\n");
            if(id == MERCHANT_ID_SELL) {
                if(wares == null) return;
                customer.deal = new HashMap<>();
                int n = 0;
                int tooMuchMessage = 0;
                int solidsInBarrelMessage = 0;
                int belowStandardsMessage = 0;
                int overflowingMarketMessage = 0;
                int tooExpensiveMessage = 0;
                bml.append("  tree{id=\"t1\";cols=\"4\";showheader=\"true\";height=\"392\"")
                   .append("col{text=\"Sell\";width=\"45\"};col{text=\"QL\";width=\"45\"};col{text=\"Weight\";width=\"50\"};col{text=\"Price\";width=\"64\"};\n");
                for(Entry<Item, Trade> entry : wares.entrySet()) {
                    Item item = entry.getKey();
                    Trade trade = entry.getValue();
                    ItemTemplate template = item.isBulkItem()? item.getRealTemplate() : item.getTemplate();
                    Ware ware = waresIndex.get(template.getTemplateId());
                    if(ware == null) continue;
                    float ql = item.getQualityLevel();
                    long price = 0L;
                    double amount;
                    boolean calcCorrectBulkWeight = item.isCrate();
                    if(item.isBulkItem()) {
                        final Item parent = item.getParentOrNull();
                        if(parent != null && parent.isCrate()) calcCorrectBulkWeight = true;
                        amount = item.getBulkNumsFloat(true);
                    } else {
                        amount = (double) item.getFullWeight() * 0.001d;
                    }
                    int weight = item.getFullWeight(calcCorrectBulkWeight);
                    byte material = item.getMaterial();
                    StringBuilder sb = new StringBuilder();
                    sb.append(net.spirangle.awakening.items.Items.getName(template, amount >= 2.0f, material));
                    if(amount >= 2.0f) sb.append(" (").append((int) amount).append("x)");
                    String name = StringUtils.bmlString(sb.toString());
                    String hover = name;
                    boolean tooExpensive = false;
                    if(amount > Config.merchantMaxAmount) {
                        hover += ": too much";
                        ++tooMuchMessage;
                    } else if(isLiquidContainer(trade.container) && !item.isLiquid()) {
                        hover += ": solids in barrel";
                        ++solidsInBarrelMessage;
                    } else {
                        int diff = (int) (trade.minQl - item.getQualityLevel());
                        if(diff > 0) {
                            hover += ": ql below standards";
                            ++belowStandardsMessage;
                        } else {
                            price = getSellPrice(trade, item);
                            long p = adjustPriceSupplyDemand(ware, amount, price);
                            if(p < (price * 3L) / 10L) {
                                hover += ": market overflowing";
                                ++overflowingMarketMessage;
                            } else {
                                price = p;
                                hover += ": " + new Change(price).getChangeShortString();
                                if(price > Config.merchantMaxPrice) {
                                    tooExpensive = true;
                                    ++tooExpensiveMessage;
                                } else {
                                    customer.deal.put(item.getWurmId(), new Deal(item, trade, ql, weight, price));
                                }
                            }
                        }
                    }
                    bml.append("   row{id=\"e").append(n).append("\";hover=\"").append(hover).append("\";name=\"").append(name).append("\";rarity=\"0\";children=\"0\";")
                       .append("col{").append(price > 0L && !tooExpensive? "checkbox=\"true\";id=\"deal" + item.getWurmId() + "\"" : "text=\"\"").append("};")
                       .append("col{text=\"").append(StringUtils.decimalFormat.format(ql)).append("\"};")
                       .append("col{text=\"").append(StringUtils.decimalFormat.format((double) weight * 0.001d)).append("\"};")
                       .append("col{text=\"").append(StringUtils.bigdecimalFormat.format((double) price * 0.0001d)).append("\"}}\n");
                    ++n;
                    if(n >= 50) break;
                }
                bml.append("  }\n");
                if(tooMuchMessage > 0)
                    responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: I'm unable to handle too large quantities, wares would spoil before finding customers.");
                if(solidsInBarrelMessage > 0)
                    responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: Solid bulk wares should not be placed in a barrel, only liquids.");
                if(belowStandardsMessage > 0)
                    responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: I don't trade in wares that are below the standards that my customers' demands. I'm sorry, but " + (n > 0? "some of " : "") + "your wares does not live up to my expectations.");
                if(overflowingMarketMessage > 0)
                    responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: The market is already overflowing of some of your wares, there is not enough demand for it.");
                if(tooExpensiveMessage > 0)
                    responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: I don't have that kind of money, that you are trying to sell.");
                buttons = MERCHANT_ID_SELL;
            } else if(id == MERCHANT_ID_WARES) {
                if(customer.trade == null || customer.template == null) return;
                int n = 0, i = 0;
                if(customer.ware.sell) {
                    try(Connection con = ModSupportDb.getModSupportDb();
                        PreparedStatement ps = con.prepareStatement("SELECT ID,QUALITYLEVEL,AMOUNT,WEIGHT,MATERIAL,AUX,PRICE FROM MARKET " +
                                                                    "WHERE TEMPLATEID=? AND SELLDATE<=? " +
                                                                    (customer.material != ItemMaterials.MATERIAL_UNDEFINED? "AND MATERIAL=? " : "") +
                                                                    (customer.amount > 0? "AND AMOUNT<=? " : "") +
                                                                    (customer.minQl > 0.0f? "AND QUALITYLEVEL>=? " : "") +
                                                                    "ORDER BY AMOUNT DESC,PRICE ASC LIMIT 50")) {
                        ps.setInt(++i, customer.ware.templateId);
                        ps.setLong(++i, System.currentTimeMillis() - 302400000L);
                        if(customer.material != ItemMaterials.MATERIAL_UNDEFINED) ps.setInt(++i, customer.material);
                        if(customer.amount > 0) ps.setDouble(++i, customer.amount);
                        if(customer.minQl > 0.0f) ps.setFloat(++i, customer.minQl);
                        try(ResultSet rs = ps.executeQuery()) {
                            for(; rs.next(); ++n) {
                                if(n == 0) {
                                    bml.append("  tree{id=\"t1\";cols=\"4\";showheader=\"true\";height=\"392\"")
                                       .append("col{text=\"Buy\";width=\"45\"};col{text=\"QL\";width=\"45\"};col{text=\"Weight\";width=\"50\"};col{text=\"Price\";width=\"64\"};\n");
                                }
                                long marketId = rs.getLong(1);
                                float ql = rs.getFloat(2);
                                float amount = rs.getFloat(3);
                                int weight = rs.getInt(4);
                                byte material = rs.getByte(5);
                                long price = getBuyPrice(customer.trade, customer.ware.templateId, amount, ql, rs.getLong(7));
                                StringBuilder sb = new StringBuilder();
                                sb.append(net.spirangle.awakening.items.Items.getName(customer.template, amount >= 2.0f, material));
                                if(amount >= 2.0f) sb.append(" (").append((int) amount).append("x)");
                                String name = StringUtils.bmlString(sb.toString());
                                String qlText = StringUtils.decimalFormat.format(ql);
                                String weightText = StringUtils.decimalFormat.format((double) weight * 0.001d);
                                String hover = name + " - QL: " + qlText + ", Weight: " + weightText + ", Price: " + new Change(price).getChangeShortString();
                                bml.append("   row{id=\"e").append(n).append("\";hover=\"").append(hover).append("\";name=\"").append(name).append("\";rarity=\"0\";children=\"0\";")
                                   .append("col{checkbox=\"true\";id=\"buy").append(marketId).append("\"};")
                                   .append("col{text=\"").append(qlText).append("\"};")
                                   .append("col{text=\"").append(weightText).append("\"};")
                                   .append("col{text=\"").append(StringUtils.bigdecimalFormat.format((double) price * 0.0001d)).append("\"}}\n");
                            }
                        }
                    } catch(SQLException e) {
                        logger.log(Level.SEVERE, "Failed to load market data: " + e.getMessage(), e);
                    }
                }
                if(n == 0) {
                    String ware = StringUtils.bmlString(net.spirangle.awakening.items.Items.getName(customer.template, true, (byte) customer.material));
                    bml.append("  text{text=''}\n")
                       .append("  text{type='italic';text=\"I'm sorry, it seems I've run out of ").append(ware).append(" for the moment. Please come back later, or see if there is something else I can offer instead.\"}\n");
                    back = MERCHANT_ID_DETAILS;
                } else {
                    bml.append("  }\n");
                    buttons = MERCHANT_ID_BUY;
                    w = 450;
                }
            }
            bml.append(" };\n");
        }
        bml.append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n");
        if(buttons == MERCHANT_ID_CANCEL) {
            passthrough.append("passthrough{id='backTo';text='").append(back).append("'};");
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 160).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n");
        } else if(buttons == MERCHANT_ID_SELL) {
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='sell';size='80,20';text='Sell'}\n");
        } else if(buttons == MERCHANT_ID_TRADES) {
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='submit';size='80,20';text='Continue'}\n");
        } else if(buttons == MERCHANT_ID_DETAILS) {
            passthrough.append("passthrough{id='backTo';text='").append(MERCHANT_ID_START).append("'};");
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='submit';size='80,20';text='Continue'}\n");
        } else if(buttons == MERCHANT_ID_BUY) {
            passthrough.append("passthrough{id='backTo';text='").append(MERCHANT_ID_DETAILS).append("'};");
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='submit';size='80,20';text='Buy'}\n");
        }
        bml.append("   ").append(passthrough).append("\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, title);
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        Customer customer = (Customer) data;

        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("back"))) {
            id = Integer.parseInt(properties.getProperty("backTo", String.valueOf(MERCHANT_ID_TALK_TO)));
            if(id == MERCHANT_ID_TALK_TO) {
                TalkToServantQuestion ttq = new TalkToServantQuestion(responder, question.getSourceItem(), servant);
                ttq.sendQuestion();
            } else {
                ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, id, customer);
                stq.sendQuestion();
            }
        } else if("true".equals(properties.getProperty("sell"))) {
            final List<Long> dealIds = new ArrayList<>(20);
            properties.forEach((k, v) -> {
                String key = (String) k;
                if(key.startsWith("deal") && "true".equals(v)) dealIds.add(Long.parseLong(key.substring(4)));
            });
            sellToMarket(responder, customer, dealIds);
        } else if(id == MERCHANT_ID_START) {
            String buy = properties.getProperty("buy");
            if(buy == null) return;
            int n = Integer.parseInt(buy);
            if(buyCategories == null || n < 0 || n >= buyCategories.length) return;
            Category c = buyCategories[n];
            if(c == null) return;
            List<Ware> w = new ArrayList<>();
            for(int i = 0; i < wares.length; ++i)
                if(wares[i].category == c.id) {
                    w.add(wares[i]);
                }
            customer.category = c;
            customer.wares = w.toArray(new Ware[w.size()]);
            ServantTaskQuestion stq = new ServantTaskQuestion(responder, null, this, MERCHANT_ID_DETAILS, customer);
            stq.sendQuestion();
        } else if(id == MERCHANT_ID_DETAILS) {
            customer.ware = customer.wares[Integer.parseInt(properties.getProperty("item"))];
            customer.material = materials[Integer.parseInt(properties.getProperty("material", String.valueOf(ItemMaterials.MATERIAL_UNDEFINED)))];
            customer.amount = amounts[Integer.parseInt(properties.getProperty("amount", "0"))];
            customer.minQl = minQls[Integer.parseInt(properties.getProperty("ql", "0"))];
            customer.template = ItemTemplateFactory.getInstance().getTemplateOrNull(customer.ware.templateId);
            if(customer.template == null) return;
            boolean l1 = customer.template.isLiquid();
            boolean f1 = customer.template.isFood();
            for(Trade trade : trades) {
                if(customer.ware.category == trade.category.id || trade.category.id == CATEGORY_ALL) {
                    boolean l2 = isLiquidContainer(trade.container);
                    boolean f2 = isFoodContainer(trade.container);
                    if((l1 && l2) || (!l1 && f1 && f2) || (!l1 && !l2 && !f1)) {
                        if(customer.trade == null || trade.profit < customer.trade.profit ||
                           (trade.profit == customer.trade.profit && compareContainers(trade.container, customer.trade.container) > 0))
                            customer.trade = trade;
                    }
                }
            }
            ServantTaskQuestion stq = new ServantTaskQuestion(responder, null, this, MERCHANT_ID_WARES, customer);
            stq.sendQuestion();
        } else if(id == MERCHANT_ID_WARES) {
            final List<Long> marketIds = new ArrayList<>(20);
            properties.forEach((k, v) -> {
                String key = (String) k;
                if(key.startsWith("buy") && "true".equals(v)) marketIds.add(Long.parseLong(key.substring(3)));
            });
            buyFromMarket(responder, customer, marketIds);
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        JSONArray ja = jo.optJSONArray("trades");
        for(int i = 0; i < ja.length(); ++i) {
            JSONObject jo2 = ja.getJSONObject(i);
            long containerId = jo2.optLong("container");
            try {
                Item container = Items.getItem(containerId);
                if(isInReach(container)) {
                    int categoryId = jo2.optInt("category");
                    Category category = categoriesIndex[categoryId];
                    float minQl = (float) jo2.optDouble("minQl");
                    float profit = (float) jo2.optDouble("profit");
                    trades.add(new Trade(container, category, minQl, profit));
                }
            } catch(NoSuchItemException e) {
                logger.log(Level.WARNING, "No container exists with the id: " + containerId, e);
            }
        }
        updateTrades();
        return trades.size() > 0;
    }

    @Override
    public String toJSONString() {
        StringBuilder json = new StringBuilder();
        json.append("{trades:[");
        int n = 0;
        for(Trade trade : trades) {
            if(n > 0) json.append(',');
            json.append("{container:").append(trade.container.getWurmId());
            json.append(",category:").append(trade.category.id);
            json.append(",minQl:").append(trade.minQl);
            json.append(",profit:").append(trade.profit);
            json.append('}');
            ++n;
        }
        json.append("]}");
        return json.toString();
    }

    private void removeTrade(Trade trade, boolean update) {
        if(trade != null) {
            trades.remove(trade);
            if(trades.isEmpty()) servant.dropTask(this);
            else if(update) updateDb();
            updateTrades();
        }
    }

    private void updateTrades() {
        this.buyCategories = null;
        if(trades.size() > 0) {
            for(int i = 0; i < categoriesIndex.length; ++i) tradesCategories[i] = false;
            for(Trade trade : trades) {
                tradesCategories[trade.category.id] = true;
            }
            List<Category> cats = new ArrayList<>();
            for(int i = 1; i < categories.length; ++i)
                if(tradesCategories[CATEGORY_ALL] || tradesCategories[categories[i].id])
                    cats.add(categories[i]);
            this.buyCategories = cats.toArray(new Category[cats.size()]);
        }
    }

    public void sellToMarket(Creature responder, Customer customer, List<Long> dealIds) {
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ins = con.prepareStatement("INSERT INTO MARKET (ID,TEMPLATEID,CATEGORY,SELLER,MERCHANT,SELLDATE,QUALITYLEVEL,AMOUNT,WEIGHT,POSX,POSY,MATERIAL,AUX,PRICE) VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?)")) {
            dealIds.forEach(dealId -> {
                try {
                    Deal deal = customer.deal.get(dealId);
                    sellToMarket(con, ins, responder, customer, deal);
                } catch(SQLException e) {
                    logger.log(Level.SEVERE, "Failed to save market data: " + e.getMessage(), e);
                }
            });
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save market data: " + e.getMessage(), e);
        }
    }

    private boolean sellToMarket(Connection con, PreparedStatement ins, Creature responder, Customer customer, Deal deal) throws SQLException {
        if(deal == null) return false;
        Player player = (Player) responder;
        Creature npc = servant.npc;
        Item ware = deal.ware;
        boolean calcCorrectBulkWeight = ware.isCrate();
        ItemTemplate templ = ware.getTemplate();
        float amount = 0.0f;
        if(ware.isBulkItem()) {
            templ = ware.getRealTemplate();
            final Item parent = ware.getParentOrNull();
            if(parent != null && parent.isCrate()) calcCorrectBulkWeight = true;
            amount = ware.getBulkNumsFloat(true);
        } else {
            amount = (float) ware.getFullWeight() * 0.001f;
        }
        float ql = ware.getQualityLevel();
        int weight = ware.getFullWeight(calcCorrectBulkWeight);
        int material = ware.getMaterial();
        int aux = ware.getAuxData();
        if(ql != deal.ql || weight != deal.weight) {
            responder.getCommunicator().sendAlertServerMessage(npc.getName() + " says: I can see that you've tried to cheat me on the " + ware.getName() + ", and so withdraw from this deal.");
            logger.info(player.getName() + " cheating: ql[" + StringUtils.bigdecimalFormat.format(ql) + " / " + StringUtils.bigdecimalFormat.format(deal.ql) +
                        "], weight[" + StringUtils.bigdecimalFormat.format(weight) + " / " + StringUtils.bigdecimalFormat.format(deal.weight) + "]");
            return false;
        }
        ins.setInt(1, templ.getTemplateId());
        ins.setInt(2, deal.trade.category.id);
        ins.setLong(3, responder.getWurmId());
        ins.setLong(4, npc.getWurmId());
        ins.setLong(5, System.currentTimeMillis());
        ins.setFloat(6, ql);
        ins.setFloat(7, amount);
        ins.setInt(8, weight);
        ins.setFloat(9, npc.getPosX());
        ins.setFloat(10, npc.getPosY());
        ins.setInt(11, material);
        ins.setInt(12, aux);
        ins.setLong(13, deal.price);
        ins.execute();

        String price = new Change(deal.price).getChangeString();
        responder.getCommunicator().sendNormalServerMessage("You sell " + ware.getName() + (ware.isBulkItem()? " (" + ((int) amount) + "x)" : "") + " for the amount of " + price + ".");
        Items.destroyItem(ware.getWurmId());

        long before = player.getMoney();
        EconomyData account = PlayersData.getInstance().getBankAccount(player);
        account.addMoney(player, deal.price, false);
        account.addTransaction(con, player, templ.getTemplateId(), 1, deal.trade.category.id, npc, ql, amount, weight, material, aux, deal.price, false);
        if(deal.trade.container != null && deal.trade.container.isEmpty(true))
            deal.trade.container.updateModelNameOnGroundItem();
        return true;
    }

    public void buyFromMarket(Creature responder, Customer customer, List<Long> marketIds) {
        if(!marketIds.isEmpty() && customer.trade != null && customer.template != null && customer.ware != null) {
            try(Connection con = ModSupportDb.getModSupportDb();
                PreparedStatement sel = con.prepareStatement("SELECT QUALITYLEVEL,AMOUNT,WEIGHT,MATERIAL,AUX,PRICE FROM MARKET WHERE ID=?");
                PreparedStatement del = con.prepareStatement("DELETE FROM MARKET WHERE ID=?")) {
                marketIds.forEach(marketId -> {
                    try {
                        buyFromMarket(con, sel, del, responder, customer, marketId);
                    } catch(SQLException e) {
                        logger.log(Level.SEVERE, "Failed to load market data: " + e.getMessage(), e);
                    }
                });
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "Failed to load market data: " + e.getMessage(), e);
            }
        }
    }

    private boolean buyFromMarket(Connection con, PreparedStatement sel, PreparedStatement del, Creature responder, Customer customer, long marketId) throws SQLException {
        if(marketId == 0L || customer.trade == null || customer.template == null || customer.ware == null) return false;
        Creature npc = servant.npc;
        sel.setLong(1, marketId);
        del.setLong(1, marketId);
        try(ResultSet rs = sel.executeQuery()) {
            if(rs.next()) {
                float ql = rs.getFloat(1);
                double amount = rs.getDouble(2);
                int weight = rs.getInt(3);
                byte material = rs.getByte(4);
                byte aux = rs.getByte(5);
                long price = getBuyPrice(customer.trade, customer.ware.templateId, amount, ql, rs.getLong(6));
                long money = responder.getMoney();
                if(price > money) {
                    responder.getCommunicator().sendNormalServerMessage("You can't afford " + new Change(price).getChangeString() + ".");
                    return false;
                }
                Item container = customer.trade.container;
//                logger.info(customer.trade.category.name+" ("+customer.trade.container.getName()+") profit: "+customer.trade.profit);
                if(customer.template.isLiquid() || (container.isCrate() && container.getRemainingCrateSpace() < (int) amount)) {
                    String message;
                    if(customer.template.isLiquid()) {
                        message = testLiquidContainer(container, customer.template);
                    } else {
                        String name = amount == 1? customer.template.getName() : StringUtils.decimalFormat.format(amount) + "x " + customer.template.getPlural();
                        message = "The " + container.getName() + " doesn't have enough space for containing the " + name + ".";
                    }
                    if(message != null) {
                        responder.getCommunicator().sendNormalServerMessage(message);
                        return false;
                    }
                }
                Item item;
                try {
                    if(customer.template.isLiquid()) {
                        item = ItemFactory.createItem(customer.template.getTemplateId(), ql, material, MaterialUtilities.COMMON, null);
                    } else {
                        item = ItemFactory.createItem(ItemList.bulkItem, ql, material, MaterialUtilities.COMMON, null);
                        item.setRealTemplate(customer.template.getTemplateId());
                        weight = (int) (amount * (double) customer.template.getVolume());
                    }
                    item.setAuxData(aux);
                    item.setWeight(weight, true);
                    container.insertItem(item, true);
                    container.updateModelNameOnGroundItem();
                    del.executeUpdate();

                    String change = new Change(price).getChangeString();
                    long tax = (long) ((float) price * 0.05f);
                    responder.getCommunicator().sendNormalServerMessage("You buy " + customer.template.getName() + (customer.template.isBulk()? " (" + ((int) amount) + "x)" : "") +
                                                                        " for the amount of " + change + ".");
                    long before = responder.getMoney();
                    responder.chargeMoney(price);
                    payTax(null, tax);
                    Player player = (Player) responder;
                    EconomyData account = PlayersData.getInstance().getBankAccount(player);
                    account.addTransaction(con, player, customer.template.getTemplateId(), 1, customer.trade.category.id, npc, ql, (float) amount, weight, material, aux, -price, false);
                } catch(FailedException | NoSuchTemplateException e) {
                    logger.log(Level.SEVERE, "Failed to create wares: " + e.getMessage(), e);
                } catch(IOException e2) {
                    logger.log(Level.SEVERE, "Failed to charge money: " + e2.getMessage(), e2);
                }
            }
        }
        return true;
    }

    private boolean isInReach(Item container) {
        Creature npc = servant.npc;
        if(container != null && npc != null && container.isOnSurface() == npc.isOnSurface()) {
            return Creature.getRange(npc, container.getPosX(), container.getPosY()) <= 20.0;
        }
        return false;
    }

    private long getSellPrice(Trade trade, Item wares) {
        int templId = wares.getTemplateId();
        if(wares.isBulkItem()) templId = wares.getRealTemplateId();
        Ware ware = waresIndex.get(templId);
        if(ware == null || !ware.buy) return -1L;
        double x = 0.9d + (((double) ((int) servant.contract.getWurmId() % 1001) / 1000.0d) * 0.2d);
        double y = 0.9d + (((double) (((int) servant.contract.getWurmId() ^ templId) % 1001) / 1000.0d) * 0.2d);
        double price = ware.price;
        double amount;
        if(wares.isBulkItem()) amount = wares.getBulkNumsFloat(true);
        else amount = wares.getFullWeight() * 0.001d;
        double ql = wares.getQualityLevel();
        double qlMod;
        double material = Config.materialValues[wares.getMaterial()];
        if((wares.isMetal() && wares.isCombine() && templId != ItemList.armourChains) || wares.isMinable())
            material = 1.0d;
        if(ql <= 10.0d) qlMod = Config.basePrice1 + ql * Config.basePriceMod1;
        else if(ql <= 20.0d) qlMod = Config.basePrice10 + (ql - 10.0d) * Config.basePriceMod10;
        else if(ql <= 50.0d) qlMod = Config.basePrice20 + (ql - 20.0d) * Config.basePriceMod20;
        else qlMod = Config.basePrice50 + (ql - 50.0d) * Config.basePriceMod50;
        double profit = 1.0d / (1.0d + (trade.profit - 1.0d) * x);
        return (long) Math.ceil(price * amount * qlMod * profit * y * material);
    }

    private long getBuyPrice(Trade trade, int templateId, double amount, float ql, long price) {
        float x = 0.9f + (((float) ((int) servant.contract.getWurmId() % 1001) / 1000.0f) * 0.2f);
        float y = 0.9f + (((float) (((int) servant.contract.getWurmId() ^ templateId) % 1001) / 1000.0f) * 0.2f);
        float n = (float) price, m = n;
        float profit = 1.0f + (trade.profit - 1.0f) * x;
        n = n * 1.1f;
        m = m * (0.8f + (0.3f * profit)) * y;
        return (long) (m < n? n : m);
    }

    private long adjustPriceSupplyDemand(Ware ware, double amount, long price) {
        if(ware == null) return -1L;
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("SELECT SUM(AMOUNT) FROM MARKET WHERE TEMPLATEID=?")) {
            ps.setInt(1, ware.templateId);
            try(ResultSet rs = ps.executeQuery()) {
                if(rs.next()) {
                    double sum = rs.getDouble(1) + amount;
                    double factor = Math.sqrt(sum);
                    factor = 1.0 - 0.1 * (factor / ware.supplyDemandFactor);
                    return (long) ((double) price * factor);
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load market data: " + e.getMessage(), e);
        }
        return price;
    }

    private void payTax(Creature responder, long tax) {
        final Shop kingsMoney = Economy.getEconomy().getKingsShop();
        kingsMoney.setMoney(kingsMoney.getMoney() + tax);
        if(responder != null) {
            responder.getCommunicator().sendNormalServerMessage("You also pay a tax of " + new Change(tax).getChangeString() + ".");
        }
    }

    static {
        categoriesIndex = new Category[128];
        for(int i = 0; i < categories.length; ++i)
            categoriesIndex[categories[i].id] = categories[i];

        wares = new Ware[]{
                new Ware(ItemList.acorn, CATEGORY_ALCHEMY, "acorn"),
                new Ware(ItemList.adamantineBar, CATEGORY_LUMPS, "adamantineBar"),
                new Ware(ItemList.adamantineOre, CATEGORY_ORES, "adamantineOre"),
                new Ware(ItemList.animalHide, CATEGORY_HIDES_LEATHER, "animalHide"),
                new Ware(ItemList.appleGreen, CATEGORY_FRUIT_BERRIES, "appleGreen"),
                new Ware(ItemList.armourChains, CATEGORY_SMITHING, "armourChains"),
                new Ware(ItemList.arrowBallista, CATEGORY_CARPENTRY, "arrowBallista"),
                new Ware(ItemList.arrowHeadBallista, CATEGORY_SMITHING, "arrowHeadBallista"),
                new Ware(ItemList.arrowHeadHunter, CATEGORY_SMITHING, "arrowHeadHunter"),
                new Ware(ItemList.arrowHeadWar, CATEGORY_SMITHING, "arrowHeadWar"),
                new Ware(ItemList.arrowHunting, CATEGORY_CARPENTRY, "arrowHunting"),
                new Ware(ItemList.arrowShaft, CATEGORY_CARPENTRY, "arrowShaft"),
                new Ware(ItemList.arrowWar, CATEGORY_CARPENTRY, "arrowWar"),
                new Ware(ItemList.ash, CATEGORY_ALCHEMY, "ash"),
                new Ware(ItemList.barley, CATEGORY_CEREALS, "barley"),
                new Ware(ItemList.basil, CATEGORY_HERBS_SPICES, "basil"),
                new Ware(ItemList.batteringRamHead, CATEGORY_SMITHING, "batteringRamHead"),
                new Ware(ItemList.beeswax, CATEGORY_ALCHEMY, "beeswax"),
                new Ware(ItemList.belayingPin, CATEGORY_CARPENTRY, "belayingPin"),
                new Ware(ItemList.belladonna, CATEGORY_HERBS_SPICES, "belladonna"),
                new Ware(ItemList.bladder, CATEGORY_BUTCHERING, "bladder"),
                new Ware(ItemList.blueberry, CATEGORY_FRUIT_BERRIES, "blueberry"),
                new Ware(ItemList.bowString, CATEGORY_ROPES, "bowString"),
                new Ware(ItemList.branch, CATEGORY_LOGS, "branch"),
                new Ware(ItemList.brassBand, CATEGORY_SMITHING, "brassBand"),
                new Ware(ItemList.brassBar, CATEGORY_LUMPS, "brassBar"),
                new Ware(ItemList.brickClay, CATEGORY_CLAY, "brickClay"),
                new Ware(ItemList.brickPottery, CATEGORY_MASONRY, "brickPottery"),
                new Ware(ItemList.bronzeBar, CATEGORY_LUMPS, "bronzeBar"),
                new Ware(ItemList.cabbage, CATEGORY_VEGETABLES, "cabbage"),
                new Ware(ItemList.cabbageSeeds, CATEGORY_SEEDS, "cabbageSeeds"),
                new Ware(ItemList.carrot, CATEGORY_VEGETABLES, "carrot"),
                new Ware(ItemList.carrotSeeds, CATEGORY_SEEDS, "carrotSeeds"),
                new Ware(ItemList.catseye, CATEGORY_MASONRY, "catseye"),
                new Ware(ItemList.catseyeBlind, CATEGORY_MASONRY, "catseyeBlind"),
                new Ware(ItemList.charcoal, CATEGORY_COAL, "charcoal"),
                new Ware(ItemList.cheeseBuffalo, CATEGORY_MILK_DAIRY, "cheeseBuffalo"),
                new Ware(ItemList.cheeseCow, CATEGORY_MILK_DAIRY, "cheeseCow"),
                new Ware(ItemList.cheeseFeta, CATEGORY_MILK_DAIRY, "cheeseFeta"),
                new Ware(ItemList.cheeseGoat, CATEGORY_MILK_DAIRY, "cheeseGoat"),
                new Ware(ItemList.cherries, CATEGORY_FRUIT_BERRIES, "cherries"),
                new Ware(ItemList.chestnut, CATEGORY_NUTS, "chestnut"),
                new Ware(ItemList.clay, CATEGORY_CLAY, "clay"),
                new Ware(ItemList.clothString, CATEGORY_TAILORING, "clothString"),
                new Ware(ItemList.clothYard, CATEGORY_TAILORING, "clothYard"),
                new Ware(ItemList.clothYardWool, CATEGORY_TAILORING, "clothYardWool"),
                new Ware(ItemList.cochineal, CATEGORY_ALCHEMY, "cochineal"),
                new Ware(ItemList.cocoaBean, CATEGORY_HERBS_SPICES, "cocoaBean"),
                new Ware(ItemList.coconut, CATEGORY_NUTS, "coconut"),
                new Ware(ItemTemplateCreatorAwakening.coffeeBean, CATEGORY_HERBS_SPICES, "coffeeBean"),
                new Ware(ItemTemplateCreatorAwakening.coffeeGround, CATEGORY_MILLING, "coffeeGround"),
                new Ware(ItemList.colossusPart, CATEGORY_MASONRY, "colossusPart"),
                new Ware(ItemList.concrete, CATEGORY_MASONRY, "concrete"),
                new Ware(ItemList.cookingOil, CATEGORY_OIL, "cookingOil"),
                new Ware(ItemList.copperBar, CATEGORY_LUMPS, "copperBar"),
                new Ware(ItemList.copperOre, CATEGORY_ORES, "copperOre"),
                new Ware(ItemList.corn, CATEGORY_CEREALS, "corn"),
                new Ware(ItemList.cornflour, CATEGORY_MILLING, "cornflour"),
                new Ware(ItemList.cotton, CATEGORY_COTTON_WOOL, "cotton"),
                new Ware(ItemList.cottonBale, CATEGORY_COTTON_WOOL, "cottonBale"),
                new Ware(ItemList.cottonSeed, CATEGORY_SEEDS, "cottonSeed"),
                new Ware(ItemList.crabMeat, CATEGORY_FISH, "crabMeat"),
                new Ware(ItemList.cream, CATEGORY_MILK_DAIRY, "cream"),
                new Ware(ItemList.crudeShaft, CATEGORY_CARPENTRY, "crudeShaft"),
                new Ware(ItemList.cucumber, CATEGORY_VEGETABLES, "cucumber"),
                new Ware(ItemList.cucumberSeeds, CATEGORY_SEEDS, "cucumberSeeds"),
                new Ware(ItemList.cumin, CATEGORY_HERBS_SPICES, "cumin"),
                new Ware(ItemList.deadBass, CATEGORY_FISH, "deadBass"),
                new Ware(ItemList.deadCarp, CATEGORY_FISH, "deadCarp"),
                new Ware(ItemList.deadCatFish, CATEGORY_FISH, "deadCatFish"),
                new Ware(ItemList.deadClam, CATEGORY_FISH, "deadClam"),
                new Ware(ItemList.deadDorado, CATEGORY_FISH, "deadDorado"),
                new Ware(ItemList.deadHerring, CATEGORY_FISH, "deadHerring"),
                new Ware(ItemList.deadLoach, CATEGORY_FISH, "deadLoach"),
                new Ware(ItemList.deadMarlin, CATEGORY_FISH, "deadMarlin"),
                new Ware(ItemList.deadMinnow, CATEGORY_FISH, "deadMinnow"),
                new Ware(ItemList.deadOctopus, CATEGORY_FISH, "deadOctopus"),
                new Ware(ItemList.deadPerch, CATEGORY_FISH, "deadPerch"),
                new Ware(ItemList.deadPike, CATEGORY_FISH, "deadPike"),
                new Ware(ItemList.deadRoach, CATEGORY_FISH, "deadRoach"),
                new Ware(ItemList.deadSailFish, CATEGORY_FISH, "deadSailFish"),
                new Ware(ItemList.deadSalmon, CATEGORY_FISH, "deadSalmon"),
                new Ware(ItemList.deadSardine, CATEGORY_FISH, "deadSardine"),
                new Ware(ItemList.deadSharkBlue, CATEGORY_FISH, "deadSharkBlue"),
                new Ware(ItemList.deadSharkWhite, CATEGORY_FISH, "deadSharkWhite"),
                new Ware(ItemList.deadSnook, CATEGORY_FISH, "deadSnook"),
                new Ware(ItemList.deadTarpon, CATEGORY_FISH, "deadTarpon"),
                new Ware(ItemList.deadTrout, CATEGORY_FISH, "deadTrout"),
                new Ware(ItemList.deadTuna, CATEGORY_FISH, "deadTuna"),
                new Ware(ItemList.deadWurmfish, CATEGORY_FISH, "deadWurmfish"),
                new Ware(ItemList.deckBoard, CATEGORY_CARPENTRY, "deckBoard"),
                new Ware(ItemList.dirtPile, CATEGORY_DIRT, "dirtPile"),
                new Ware(ItemList.dredgeLip, CATEGORY_SMITHING, "dredgeLip"),
                new Ware(ItemList.dyeBlack, CATEGORY_ALCHEMY, "dyeBlack"),
                new Ware(ItemList.dyeBlue, CATEGORY_ALCHEMY, "dyeBlue"),
                new Ware(ItemList.dyeGreen, CATEGORY_ALCHEMY, "dyeGreen"),
                new Ware(ItemList.dyeRed, CATEGORY_ALCHEMY, "dyeRed"),
                new Ware(ItemList.dyeWhite, CATEGORY_ALCHEMY, "dyeWhite"),
                new Ware(ItemList.eggSmall, CATEGORY_EGGS, "eggSmall"),
                new Ware(ItemList.electrumBar, CATEGORY_LUMPS, "electrumBar"),
                new Ware(ItemList.eye, CATEGORY_BUTCHERING, "eye"),
                new Ware(ItemList.fenceBars, CATEGORY_SMITHING, "fenceBars"),
                new Ware(ItemList.fennel, CATEGORY_HERBS_SPICES, "fennel"),
                new Ware(ItemList.fennelPlant, CATEGORY_HERBS_SPICES, "fennelPlant"),
                new Ware(ItemList.fennelSeeds, CATEGORY_SEEDS, "fennelSeeds"),
                new Ware(ItemList.filetFish, CATEGORY_FISH, "filetFish"),
                new Ware(ItemList.filetMeat, CATEGORY_MEAT, "filetMeat"),
                new Ware(ItemList.flint, CATEGORY_ROCKS, "flint"),
                new Ware(ItemList.floatFeather, CATEGORY_BUTCHERING, "floatFeather"),
                new Ware(ItemList.floorBoards, CATEGORY_CARPENTRY, "floorBoards"),
                new Ware(ItemList.flour, CATEGORY_MILLING, "flour"),
                new Ware(ItemList.flower1, CATEGORY_FLOWERS, "flower1"),
                new Ware(ItemList.flower2, CATEGORY_FLOWERS, "flower2"),
                new Ware(ItemList.flower3, CATEGORY_FLOWERS, "flower3"),
                new Ware(ItemList.flower4, CATEGORY_FLOWERS, "flower4"),
                new Ware(ItemList.flower5, CATEGORY_FLOWERS, "flower5"),
                new Ware(ItemList.flower6, CATEGORY_FLOWERS, "flower6"),
                new Ware(ItemList.flower7, CATEGORY_FLOWERS, "flower7"),
                new Ware(ItemList.flowerLavender, CATEGORY_FLOWERS, "flowerLavender"),
                new Ware(ItemList.flowerRose, CATEGORY_FLOWERS, "flowerRose"),
                new Ware(ItemList.fruitJuice, CATEGORY_BEVERAGES, "fruitJuice"),
                new Ware(ItemList.fur, CATEGORY_FURS, "fur"),
                new Ware(ItemList.garlic, CATEGORY_HERBS_SPICES, "garlic"),
                new Ware(ItemList.ginger, CATEGORY_HERBS_SPICES, "ginger"),
                new Ware(ItemList.gland, CATEGORY_BUTCHERING, "gland"),
                new Ware(ItemList.glimmerSteelBar, CATEGORY_LUMPS, "glimmerSteelBar"),
                new Ware(ItemList.glimmerSteelOre, CATEGORY_ORES, "glimmerSteelOre"),
                new Ware(ItemList.goldBar, CATEGORY_LUMPS, "goldBar"),
                new Ware(ItemList.goldOre, CATEGORY_ORES, "goldOre"),
                new Ware(ItemList.grapesBlue, CATEGORY_FRUIT_BERRIES, "grapesBlue"),
                new Ware(ItemList.grapeSeedling, CATEGORY_SEEDLINGS, "grapeSeedling"),
                new Ware(ItemList.grapesGreen, CATEGORY_FRUIT_BERRIES, "grapesGreen"),
                new Ware(ItemList.heart, CATEGORY_BUTCHERING, "heart"),
                new Ware(ItemList.honey, CATEGORY_HONEY, "honey"),
                new Ware(ItemList.hoof, CATEGORY_BUTCHERING, "hoof"),
                new Ware(ItemList.hops, CATEGORY_HERBS_SPICES, "hops"),
                new Ware(ItemList.hopsSeedling, CATEGORY_SEEDLINGS, "hopsSeedling"),
                new Ware(ItemList.horn, CATEGORY_BUTCHERING, "horn"),
                new Ware(ItemList.hornLong, CATEGORY_BUTCHERING, "hornLong"),
                new Ware(ItemList.hornTwisted, CATEGORY_BUTCHERING, "hornTwisted"),
                new Ware(ItemList.hullPlank, CATEGORY_CARPENTRY, "hullPlank"),
                new Ware(ItemList.ink, CATEGORY_ALCHEMY, "ink"),
                new Ware(ItemList.ironBand, CATEGORY_SMITHING, "ironBand"),
                new Ware(ItemList.ironBar, CATEGORY_LUMPS, "ironBar"),
                new Ware(ItemList.ironOre, CATEGORY_ORES, "ironOre"),
                new Ware(ItemList.ivySeedling, CATEGORY_SEEDLINGS, "ivySeedling"),
                new Ware(ItemList.joists, CATEGORY_CARPENTRY, "joists"),
                new Ware(ItemList.keelPart, CATEGORY_CARPENTRY, "keelPart"),
                new Ware(ItemList.kelp, CATEGORY_HERBS_SPICES, "kelp"),
                new Ware(ItemList.kindling, CATEGORY_LOGS, "kindling"),
                new Ware(ItemList.label, CATEGORY_PAPER, "label"),
                new Ware(ItemList.largeChainLinkIron, CATEGORY_SMITHING, "largeChainLinkIron"),
                new Ware(ItemList.leadBar, CATEGORY_LUMPS, "leadBar"),
                new Ware(ItemList.leadOre, CATEGORY_ORES, "leadOre"),
                new Ware(ItemList.leather, CATEGORY_HIDES_LEATHER, "leather"),
                new Ware(ItemList.leatherPieces, CATEGORY_HIDES_LEATHER, "leatherPieces"),
                new Ware(ItemList.leatherStrip, CATEGORY_HIDES_LEATHER, "leatherStrip"),
                new Ware(ItemList.leavesCamellia, CATEGORY_HERBS_SPICES, "leavesCamellia"),
                new Ware(ItemList.leavesOleander, CATEGORY_HERBS_SPICES, "leavesOleander"),
                new Ware(ItemList.lemon, CATEGORY_FRUIT_BERRIES, "lemon"),
                new Ware(ItemList.lettuce, CATEGORY_VEGETABLES, "lettuce"),
                new Ware(ItemList.lettuceSeeds, CATEGORY_SEEDS, "lettuceSeeds"),
                new Ware(ItemList.lingonberry, CATEGORY_FRUIT_BERRIES, "lingonberry"),
                new Ware(ItemList.log, CATEGORY_LOGS, "log"),
                new Ware(ItemList.lovage, CATEGORY_HERBS_SPICES, "lovage"),
                new Ware(ItemList.lowQlIron, CATEGORY_ORES, "lowQlIron"),
                new Ware(ItemList.lye, CATEGORY_ALCHEMY, "lye"),
                new Ware(ItemList.marbleBrick, CATEGORY_MASONRY, "marbleBrick"),
                new Ware(ItemList.marbleShard, CATEGORY_ROCKS, "marbleShard"),
                new Ware(ItemList.marbleSlab, CATEGORY_MASONRY, "marbleSlab"),
                new Ware(ItemList.meat, CATEGORY_MEAT, "meat"),
                new Ware(ItemList.metalHooks, CATEGORY_SMITHING, "metalHooks"),
                new Ware(ItemList.metalRivet, CATEGORY_SMITHING, "metalRivet"),
                new Ware(ItemList.metalWires, CATEGORY_SMITHING, "metalWires"),
                new Ware(ItemList.milk_bison, CATEGORY_MILK_DAIRY, "milk_bison"),
                new Ware(ItemList.milk_cow, CATEGORY_MILK_DAIRY, "milk_cow"),
                new Ware(ItemList.milk_sheep, CATEGORY_MILK_DAIRY, "milk_sheep"),
                new Ware(ItemList.mineDoorPlanks, CATEGORY_CARPENTRY, "mineDoorPlanks"),
                new Ware(ItemList.mint, CATEGORY_HERBS_SPICES, "mint"),
                new Ware(ItemList.mixedGrass, CATEGORY_GRASS_THATCH, "mixedGrass"),
                new Ware(ItemList.mortar, CATEGORY_MASONRY, "mortar"),
                new Ware(ItemList.moss, CATEGORY_GRASS_THATCH, "moss"),
                new Ware(ItemList.mushroomBlack, CATEGORY_MUSHROOMS, "mushroomBlack"),
                new Ware(ItemList.mushroomBlue, CATEGORY_MUSHROOMS, "mushroomBlue"),
                new Ware(ItemList.mushroomBrown, CATEGORY_MUSHROOMS, "mushroomBrown"),
                new Ware(ItemList.mushroomGreen, CATEGORY_MUSHROOMS, "mushroomGreen"),
                new Ware(ItemList.mushroomRed, CATEGORY_MUSHROOMS, "mushroomRed"),
                new Ware(ItemList.mushroomYellow, CATEGORY_MUSHROOMS, "mushroomYellow"),
                new Ware(ItemList.nailsIronLarge, CATEGORY_SMITHING, "nailsIronLarge"),
                new Ware(ItemList.nailsIronSmall, CATEGORY_SMITHING, "nailsIronSmall"),
                new Ware(ItemList.nettles, CATEGORY_HERBS_SPICES, "nettles"),
                new Ware(ItemList.nori, CATEGORY_HERBS_SPICES, "nori"),
                new Ware(ItemList.nutHazel, CATEGORY_NUTS, "nutHazel"),
                new Ware(ItemList.nutmeg, CATEGORY_HERBS_SPICES, "nutmeg"),
                new Ware(ItemList.oar, CATEGORY_CARPENTRY, "oar"),
                new Ware(ItemList.oat, CATEGORY_CEREALS, "oat"),
                new Ware(ItemList.olive, CATEGORY_FRUIT_BERRIES, "olive"),
                new Ware(ItemList.oliveOil, CATEGORY_OIL, "oliveOil"),
                new Ware(ItemList.onion, CATEGORY_VEGETABLES, "onion"),
                new Ware(ItemList.orange, CATEGORY_FRUIT_BERRIES, "orange"),
                new Ware(ItemList.oregano, CATEGORY_HERBS_SPICES, "oregano"),
                new Ware(ItemList.paperSheet, CATEGORY_PAPER, "paperSheet"),
                new Ware(ItemList.paprika, CATEGORY_HERBS_SPICES, "paprika"),
                new Ware(ItemList.paprikaSeeds, CATEGORY_SEEDS, "paprikaSeeds"),
                new Ware(ItemList.papyrusSheet, CATEGORY_PAPER, "papyrusSheet"),
                new Ware(ItemList.parsley, CATEGORY_HERBS_SPICES, "parsley"),
                new Ware(ItemList.paw, CATEGORY_BUTCHERING, "paw"),
                new Ware(ItemList.pea, CATEGORY_VEGETABLES, "pea"),
                new Ware(ItemTemplateCreatorAwakening.peanut, CATEGORY_NUTS, "peanut"),
                new Ware(ItemList.peaPod, CATEGORY_VEGETABLES, "peaPod"),
                new Ware(ItemList.peat, CATEGORY_COAL, "peat"),
                new Ware(ItemList.pegWood, CATEGORY_CARPENTRY, "pegWood"),
                new Ware(ItemList.pigFood, CATEGORY_COOKING, "pigFood"),
                new Ware(ItemList.pineapple, CATEGORY_FRUIT_BERRIES, "pineapple"),
                new Ware(ItemList.pineNuts, CATEGORY_NUTS, "pineNuts"),
                new Ware(ItemList.plank, CATEGORY_CARPENTRY, "plank"),
                new Ware(ItemList.potato, CATEGORY_VEGETABLES, "potato"),
                new Ware(ItemList.pumpkin, CATEGORY_VEGETABLES, "pumpkin"),
                new Ware(ItemList.pumpkinSeed, CATEGORY_SEEDS, "pumpkinSeed"),
                new Ware(ItemList.raspberries, CATEGORY_FRUIT_BERRIES, "raspberries"),
                new Ware(ItemList.reed, CATEGORY_GRASS_THATCH, "reed"),
                new Ware(ItemList.reedFibre, CATEGORY_GRASS_THATCH, "reedFibre"),
                new Ware(ItemList.reedSeed, CATEGORY_SEEDS, "reedSeed"),
                new Ware(ItemList.rice, CATEGORY_CEREALS, "rice"),
                new Ware(ItemList.riftCrystal, CATEGORY_ROCKS, "riftCrystal"),
                new Ware(ItemList.riftStone, CATEGORY_ROCKS, "riftStone"),
                new Ware(ItemList.riftWood, CATEGORY_LOGS, "riftWood"),
                new Ware(ItemList.rock, CATEGORY_ROCKS, "rock"),
                new Ware(ItemList.rockSalt, CATEGORY_ROCKS, "rockSalt"),
                new Ware(ItemList.rope, CATEGORY_ROPES, "rope"),
                new Ware(ItemList.ropeAnchor, CATEGORY_ROPES, "ropeAnchor"),
                new Ware(ItemList.ropeAnchor, CATEGORY_SMITHING, "ropeAnchor"),
                new Ware(ItemList.ropeMooring, CATEGORY_ROPES, "ropeMooring"),
                new Ware(ItemList.ropeThick, CATEGORY_ROPES, "ropeThick"),
                new Ware(ItemList.ropeThin, CATEGORY_ROPES, "ropeThin"),
                new Ware(ItemList.ropeThin, CATEGORY_ROPES, "ropeThin"),
                new Ware(ItemList.rosemary, CATEGORY_HERBS_SPICES, "rosemary"),
                new Ware(ItemList.roseSeedling, CATEGORY_SEEDLINGS, "roseSeedling"),
                new Ware(ItemList.roundedBrick, CATEGORY_MASONRY, "roundedBrick"),
                new Ware(ItemList.rye, CATEGORY_CEREALS, "rye"),
                new Ware(ItemList.sage, CATEGORY_HERBS_SPICES, "sage"),
                new Ware(ItemList.salt, CATEGORY_COOKING, "salt"),
                new Ware(ItemList.sand, CATEGORY_SAND, "sand"),
                new Ware(ItemList.sandstone, CATEGORY_ROCKS, "sandstone"),
                new Ware(ItemList.sandstoneBrick, CATEGORY_MASONRY, "sandstoneBrick"),
                new Ware(ItemList.sandstoneSlab, CATEGORY_MASONRY, "sandstoneSlab"),
                new Ware(ItemList.sapMaple, CATEGORY_BEVERAGES, "sapMaple"),
                new Ware(ItemList.sassafras, CATEGORY_HERBS_SPICES, "sassafras"),
                new Ware(ItemList.scrapwood, CATEGORY_LOGS, "scrapwood"),
                new Ware(ItemList.seryllBar, CATEGORY_LUMPS, "seryllBar"),
                new Ware(ItemList.shaft, CATEGORY_CARPENTRY, "shaft"),
                new Ware(ItemList.sheet, CATEGORY_TAILORING, "sheet"),
                new Ware(ItemList.sheetCopper, CATEGORY_SMITHING, "sheetCopper"),
                new Ware(ItemList.sheetGold, CATEGORY_SMITHING, "sheetGold"),
                new Ware(ItemList.sheetIron, CATEGORY_SMITHING, "sheetIron"),
                new Ware(ItemList.sheetLead, CATEGORY_SMITHING, "sheetLead"),
                new Ware(ItemList.sheetSilver, CATEGORY_SMITHING, "sheetSilver"),
                new Ware(ItemList.sheetSteel, CATEGORY_SMITHING, "sheetSteel"),
                new Ware(ItemList.sheetTin, CATEGORY_SMITHING, "sheetTin"),
                new Ware(ItemList.shingleClay, CATEGORY_CLAY, "shingleClay"),
                new Ware(ItemList.shinglePottery, CATEGORY_MASONRY, "shinglePottery"),
                new Ware(ItemList.shingleSlate, CATEGORY_MASONRY, "shingleSlate"),
                new Ware(ItemList.shingleWood, CATEGORY_CARPENTRY, "shingleWood"),
                new Ware(ItemList.silverBar, CATEGORY_LUMPS, "silverBar"),
                new Ware(ItemList.silverOre, CATEGORY_ORES, "silverOre"),
                new Ware(ItemList.slateBrick, CATEGORY_MASONRY, "slateBrick"),
                new Ware(ItemList.slateShard, CATEGORY_ROCKS, "slateShard"),
                new Ware(ItemList.slateSlab, CATEGORY_MASONRY, "slateSlab"),
                new Ware(ItemList.sourceSalt, CATEGORY_ALCHEMY, "sourceSalt"),
                new Ware(ItemList.sprout, CATEGORY_SPROUTS, "sprout"),
                new Ware(ItemList.steelBar, CATEGORY_LUMPS, "steelBar"),
                new Ware(ItemList.stoneBrick, CATEGORY_MASONRY, "stoneBrick"),
                new Ware(ItemList.stoneSlab, CATEGORY_MASONRY, "stoneSlab"),
                new Ware(ItemList.strawberries, CATEGORY_FRUIT_BERRIES, "strawberries"),
                new Ware(ItemList.strawberrySeed, CATEGORY_SEEDS, "strawberrySeed"),
                new Ware(ItemList.sugar, CATEGORY_HERBS_SPICES, "sugar"),
                new Ware(ItemList.sugarBeet, CATEGORY_VEGETABLES, "sugarBeet"),
                new Ware(ItemList.sugarBeetSeeds, CATEGORY_SEEDS, "sugarBeetSeeds"),
                new Ware(ItemList.syrupMaple, CATEGORY_BEVERAGES, "syrupMaple"),
                new Ware(ItemList.tackleLarge, CATEGORY_CARPENTRY, "tackleLarge"),
                new Ware(ItemList.tackleSmall, CATEGORY_CARPENTRY, "tackleSmall"),
                new Ware(ItemList.tail, CATEGORY_BUTCHERING, "tail"),
                new Ware(ItemList.tallow, CATEGORY_BUTCHERING, "tallow"),
                new Ware(ItemList.tannin, CATEGORY_ALCHEMY, "tannin"),
                new Ware(ItemList.tar, CATEGORY_COAL, "tar"),
                new Ware(ItemList.tenon, CATEGORY_CARPENTRY, "tenon"),
                new Ware(ItemList.thatch, CATEGORY_GRASS_THATCH, "thatch"),
                new Ware(ItemList.thyme, CATEGORY_HERBS_SPICES, "thyme"),
                new Ware(ItemList.tinBar, CATEGORY_LUMPS, "tinBar"),
                new Ware(ItemList.tinOre, CATEGORY_ORES, "tinOre"),
                new Ware(ItemList.tomato, CATEGORY_VEGETABLES, "tomato"),
                new Ware(ItemList.tomatoSeeds, CATEGORY_SEEDS, "tomatoSeeds"),
                new Ware(ItemList.tooth, CATEGORY_BUTCHERING, "tooth"),
                new Ware(ItemList.torch, CATEGORY_CARPENTRY, "torch"),
                new Ware(ItemList.turmeric, CATEGORY_HERBS_SPICES, "turmeric"),
                new Ware(ItemList.turmericSeeds, CATEGORY_SEEDS, "turmericSeeds"),
                new Ware(ItemList.walnut, CATEGORY_NUTS, "walnut"),
                new Ware(ItemList.waxSealingKit, CATEGORY_PAPER, "waxSealingKit"),
                new Ware(ItemList.wemp, CATEGORY_WEMP, "wemp"),
                new Ware(ItemList.wempFibre, CATEGORY_WEMP, "wempFibre"),
                new Ware(ItemList.wempSeed, CATEGORY_SEEDS, "wempSeed"),
                new Ware(ItemList.wheat, CATEGORY_CEREALS, "wheat"),
                new Ware(ItemList.woad, CATEGORY_ALCHEMY, "woad"),
                new Ware(ItemList.woodBeam, CATEGORY_CARPENTRY, "woodBeam"),
                new Ware(ItemList.woodenHandleSword, CATEGORY_CARPENTRY, "woodenHandleSword"),
                new Ware(ItemList.woodpulp, CATEGORY_PAPER, "woodpulp"),
                new Ware(ItemList.wool, CATEGORY_COTTON_WOOL, "wool"),
                new Ware(ItemList.woolYarn, CATEGORY_TAILORING, "woolYarn"),
                new Ware(ItemList.zincBar, CATEGORY_LUMPS, "zincBar"),
                new Ware(ItemList.zincOre, CATEGORY_ORES, "zincOre"),
                };
        waresIndex = new HashMap<>();
        for(int i = 0; i < wares.length; ++i)
            waresIndex.put(wares[i].templateId, wares[i]);
    }
}
