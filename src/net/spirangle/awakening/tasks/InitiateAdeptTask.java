package net.spirangle.awakening.tasks;

import com.wurmonline.server.*;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.deities.Religion;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.players.Cultist;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.skills.NoSuchSkillException;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.server.skills.Skills;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.shared.constants.ItemMaterials;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.util.StringUtils;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class InitiateAdeptTask extends Task {

    private static final Logger logger = Logger.getLogger(InitiateAdeptTask.class.getName());

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Initiate follower to adept";
            params.tooltip = "GM Task: Allows players to become adepts of the specified deity";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return taskPaper == null || responder == null || responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public Deity deity = null;
    private int[] deityIds = null;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_INITIATE_ADEPT;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Initiate adepts of " + deity.name;
    }

    @Override
    public String getSubject(Creature responder) {
        if((responder.getDeity() == deity && responder.getFaith() >= 30.0f) || responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
            if(!responder.isPriest()) return "Becoming an adept of " + deity.name;
            else if(!hasAdeptItems(responder)) return "Receive the items of an adept of " + deity.name;
        }
        return null;
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 250;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Initiate adepts of a deity:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"This task will instruct " + name + " to initiate adepts from followers of a deity.\"}" +
                   "   text{text=''}\n" +
                   "   harray{\n" +
                   "    label{text=\"Which deity? \"}\n" +
                   "    dropdown{id='deity';default='0';options=\"");
        Deity[] deities = Deities.getDeities();
        deityIds = new int[deities.length];
        for(int i = 0; i < deities.length; ++i) {
            if(i > 0) bml.append(',');
            bml.append(deities[i].name);
            deityIds[i] = deities[i].number;
        }
        bml.append("\"}" +
                   "   }\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Instruct'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Initiate Adept");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        if(deityIds == null) return false;
        String d = properties.getProperty("deity");
        logger.info("Deity: " + d);
        if(d == null) return false;
        int deityId = Integer.parseInt(d);
        logger.info("Deity: " + deityId);
        deity = Deities.getDeity(deityIds[deityId]);
        deityIds = null;
        logger.info("Deity: " + (deity == null? "null" : deity.name));
        return deity != null;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        if(!responder.isPriest()) {
            ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, 0, null);
            stq.sendQuestion();
        } else if(!hasAdeptItems(responder) && isAcceptedByDeity(responder)) {
            removeAdeptItems(responder);
            giveAdeptItems(responder);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        String deityName = StringUtils.bmlString(deity.name);
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 350;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Initiate adept of " + deityName + ":\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{type='italic';text=\"You may choose to become an adept of " + deityName + ".\n\n" +
                   "If you answer yes, you will receive special powers from your deity, such as the ability to cast spells.\n\n");
        if(Servers.localServer.PVPSERVER) {
            bml.append("You must also walk this path if you strive to become a real champion of " + deityName + ". " +
                       "As a champion, you may only escape death a few times though. After that your life ends permanently.\n\n");
        }
        bml.append("If your faith ever fails you, you will lose your adepthood.\n\n" +
                   "Do you want to become an adept of " + deityName + "?\"}\n" +
                   "   radio{group='adept';id='yes';text='Yes'}\n" +
                   "   radio{group='adept';id='no';text='No';selected='true'}\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Initiate'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        String conv = properties.getProperty("adept");
        if(conv != null) {
            if("no".equals(conv)) {
                responder.getCommunicator().sendNormalServerMessage("You decide not to become an adept for now.");
            } else if("yes".equals(conv)) {
                boolean gm = responder.getPower() >= MiscConstants.POWER_HIGH_GOD;
                if(responder.getDeity() != deity) {
                    responder.getCommunicator().sendNormalServerMessage("You are not a follower of " + deity.name + ".");
                } else if(responder.isPriest()) {
                    responder.getCommunicator().sendNormalServerMessage("You are already an adept.");
                } else if(responder.getFaith() < 30.0f && !gm) {
                    responder.getCommunicator().sendNormalServerMessage("You need more faith to become an adept.");
                } else if(isAcceptedByDeity(responder) || gm) {
                    responder.setPriest(true);
                    logger.info(responder.getName() + " has just become an adept of " + deity.name);
                    responder.getCommunicator().sendNormalServerMessage("You have attained the grade of adept. Welcome to the " + (deity.isHateGod()? "sinister" : "sacred") + " Order of " + deity.name + "!");
                    Server.getInstance().broadCastAction(responder.getName() + " is now an adept of " + deity.name + "!", responder, 5);
                    ChatChannels.getInstance().sendNewAdept((Player) responder);
                    try {
                        responder.setFavor(responder.getFaith());
                    } catch(IOException e) {
                        logger.log(Level.WARNING, responder.getName() + ": " + e.getMessage(), e);
                    }
                    if(!hasAdeptItems(responder)) {
                        removeAdeptItems(responder);
                        giveAdeptItems(responder);
                    }
                }
            }
        }
    }

    private boolean hasAdeptItems(Creature responder) {
        int holyItemId = Religion.getHolyItem(deity);
        if(holyItemId == -1) return true;
        Item inventory = responder.getInventory();
        if(inventory.findItem(holyItemId, true) != null) return true;
        Item[] items = responder.getBody().getContainersAndWornItems();
        for(int i = 0; i < items.length; ++i) {
            if(items[i].getTemplateId() == holyItemId) return true;
            if(!items[i].isEmpty(false) && items[i].findItem(holyItemId, true) != null) return true;
        }
        return false;
    }

    private void removeAdeptItems(Creature responder) {
        removeAdeptItems(responder, responder.getInventory().getItemsAsArray());
        removeAdeptItems(responder, responder.getBody().getContainersAndWornItems());
    }

    private void removeAdeptItems(Creature responder, Item[] items) {
        for(int i = 0; i < items.length; ++i) {
            if(Religion.isHolyItem(items[i])) Items.destroyItem(items[i].getWurmId());
            else if(!items[i].isEmpty(false)) removeAdeptItems(responder, items[i].getItemsAsArray());
        }
    }

    private boolean isAcceptedByDeity(Creature responder) {
        if(responder.getPower() >= MiscConstants.POWER_IMPLEMENTOR) return true;
        Skills skills = responder.getSkills();
        Cultist cultist = responder.getCultist();
        int path = cultist == null? -1 : cultist.getPath();
        Skill characteristic = null, skill1 = null, skill2 = null, skill3 = null;
        double characteristicLevel = 0.0, skillLevel1 = 0.0, skillLevel2 = 0.0, skillLevel3 = 0.0;
        boolean hasRightAlignment = false/*, hasRightPath = false*/;
        try {
            switch(deity.number) {
                case Religion.DEITY_SEDAES:
                    characteristic = skills.getSkill(SkillList.MIND_SPEED);
                    characteristicLevel = 23.0;
                    hasRightAlignment = responder.getAlignment() >= 100.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_POWER;
                    skill1 = skills.getSkill(SkillList.GROUP_FIGHTING);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.GROUP_ARCHERY);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.GROUP_HEALING);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_PAYREN:
                    characteristic = skills.getSkill(SkillList.SOUL);
                    characteristicLevel = 23.0;
                    hasRightAlignment = responder.getAlignment() >= 50.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_INSANITY;
                    skill1 = skills.getSkill(SkillList.TAMEANIMAL);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.GROUP_NATURE);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.TRACKING);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_EILANDA:
                    characteristic = skills.getSkill(SkillList.SOUL_STRENGTH);
                    characteristicLevel = 21.0;
                    hasRightAlignment = responder.getAlignment() >= 75.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_POWER;
                    skill1 = skills.getSkill(SkillList.SPEAR_LONG);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.LEATHERWORKING);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.PRAYER);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_SHONIN:
                    characteristic = skills.getSkill(SkillList.BODY);
                    characteristicLevel = 25.0;
                    hasRightAlignment = responder.getAlignment() >= 100.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_POWER || path==Cults.PATH_INSANITY;
                    skill1 = skills.getSkill(SkillList.GROUP_FIGHTING);
                    skillLevel1 = 70.0;
                    skill2 = skills.getSkill(SkillList.GROUP_SWORDS);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.GROUP_POLEARMS);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_ASMAN:
                    characteristic = skills.getSkill(SkillList.MIND_LOGICAL);
                    characteristicLevel = 25.0;
                    hasRightAlignment = responder.getAlignment() >= 75.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_POWER;
                    skill1 = skills.getSkill(SkillList.MASONRY);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.SMITHING_METALLURGY);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.GROUP_ALCHEMY);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_KIANE:
                    characteristic = skills.getSkill(SkillList.MIND);
                    characteristicLevel = 24.0;
                    hasRightAlignment = responder.getAlignment() >= 50.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_INSANITY;
                    skill1 = skills.getSkill(SkillList.RESTORATION);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.CLOTHTAILORING);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.ALCHEMY_NATURAL);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_MIRIDON:
                    characteristic = skills.getSkill(SkillList.BODY_STRENGTH);
                    characteristicLevel = 25.0;
                    hasRightAlignment = responder.getAlignment() <= -75.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_POWER || path==Cults.PATH_HATE;
                    skill1 = skills.getSkill(SkillList.GROUP_FIGHTING);
                    skillLevel1 = 50.0;
                    skill2 = skills.getSkill(SkillList.FIGHT_AGGRESSIVESTYLE);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.TAUNTING);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_LAPHANER:
                    characteristic = skills.getSkill(SkillList.SOUL_DEPTH);
                    characteristicLevel = 23.0;
                    hasRightAlignment = responder.getAlignment() <= -50.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_HATE;
                    skill1 = skills.getSkill(SkillList.PUPPETEERING);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.SMITHING_GOLDSMITHING);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.TAMEANIMAL);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_DRAKKAR:
                    characteristic = skills.getSkill(SkillList.BODY_CONTROL);
                    characteristicLevel = 25.0;
                    hasRightAlignment = responder.getAlignment() <= -100.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_INSANITY || path==Cults.PATH_HATE;
                    skill1 = skills.getSkill(SkillList.BUTCHERING);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.ALCHEMY_NATURAL);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.GROUP_THIEVERY);
                    skillLevel3 = 20.0;
                    break;
                case Religion.DEITY_QALESHIN:
                    characteristic = skills.getSkill(SkillList.BODY_STAMINA);
                    characteristicLevel = 23.0;
                    hasRightAlignment = responder.getAlignment() >= 100.0f;
                    //               hasRightPath = path==Cults.PATH_KNOWLEDGE || path==Cults.PATH_LOVE || path==Cults.PATH_POWER;
                    skill1 = skills.getSkill(SkillList.ARCHAEOLOGY);
                    skillLevel1 = 30.0;
                    skill2 = skills.getSkill(SkillList.FIREMAKING);
                    skillLevel2 = 25.0;
                    skill3 = skills.getSkill(SkillList.MINING);
                    skillLevel3 = 20.0;
                    break;
            }
        } catch(NoSuchSkillException e) { }

        if(cultist == null || path == -1) {
            responder.getCommunicator().sendNormalServerMessage("Your have not yet chosen a meditation path, one that is sanctified by our Order.");
        } else if(characteristic != null && skill1 != null && skill2 != null && skill3 != null) {
            if(characteristic.getKnowledge() < characteristicLevel) {
                responder.getCommunicator().sendNormalServerMessage("You have not trained " + characteristic.getName() + " well enough.");
            } else if(!hasRightAlignment) {
                responder.getCommunicator().sendNormalServerMessage("You are not yet aligned with the nature of " + deity.name + ".");
                //         } else if(!hasRightPath) {
                //            responder.getCommunicator().sendNormalServerMessage("Your chosen path, "+Cults.getPathNameFor((byte)path)+", is not one of those sanctified by our Order.");
            } else if(cultist.getLevel() < 4) {
                responder.getCommunicator().sendNormalServerMessage("You have not yet attained the proper focus on your meditation path.");
            } else if(skill1.getKnowledge() < skillLevel1) {
                responder.getCommunicator().sendNormalServerMessage("You have not learned enough " + skill1.getName() + ".");
            } else if(skill2.getKnowledge() < skillLevel2) {
                responder.getCommunicator().sendNormalServerMessage("You have not studied enough " + skill2.getName() + ".");
            } else if(skill3.getKnowledge() < skillLevel3) {
                responder.getCommunicator().sendNormalServerMessage("You are not enough educated in " + skill3.getName() + ".");
            } else return true;
        }
        responder.getCommunicator().sendNormalServerMessage(deity.name + " does not accept you as an adept in your current state. Work hard, and return again for another trial.");
        return false;
    }

    private void giveAdeptItems(Creature responder) {
        Creature npc = servant.npc;
        Item inventory = responder.getInventory();
        try {
            Item holyItem = null;
            switch(deity.number) {
                case Religion.DEITY_SEDAES:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.sedaesSword, responder.getFaith(), ItemMaterials.MATERIAL_BRONZE, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_PAYREN:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.payrenStaff, responder.getFaith(), ItemMaterials.MATERIAL_WOOD_OAK, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_EILANDA:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.eilandaSpear, responder.getFaith(), ItemMaterials.MATERIAL_GLIMMERSTEEL, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_SHONIN:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.shoninSword, responder.getFaith(), ItemMaterials.MATERIAL_ELECTRUM, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_ASMAN:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.asmanStaff, responder.getFaith(), ItemMaterials.MATERIAL_GLIMMERSTEEL, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_KIANE:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.kianeStaff, responder.getFaith(), ItemMaterials.MATERIAL_WOOD_APPLE, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_MIRIDON:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.miridonAxe, responder.getFaith(), ItemMaterials.MATERIAL_SERYLL, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_LAPHANER:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.laphanerStaff, responder.getFaith(), ItemMaterials.MATERIAL_WOOD_CHERRY, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_DRAKKAR:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.drakkarKnife, responder.getFaith(), ItemMaterials.MATERIAL_SILVER, MiscConstants.RARE, npc.getName());
                    break;
                case Religion.DEITY_QALESHIN:
                    holyItem = ItemFactory.createItem(ItemTemplateCreatorAwakening.qaleshinStaff, responder.getFaith(), ItemMaterials.MATERIAL_WOOD_WALNUT, MiscConstants.RARE, npc.getName());
                    break;
                default:
                    return;
            }
            if(holyItem != null) {
                inventory.insertItem(holyItem, true);
                responder.getCommunicator().sendUpdateKingdomId();
                responder.getCommunicator().sendNormalServerMessage("You receive a " + holyItem.getName() + ".");
            }
        } catch(NoSuchTemplateException | FailedException e) {
            logger.log(Level.SEVERE, "Failed creating adept items.", e);
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        int deityId = jo.optInt("deityId");
        deity = Deities.getDeity(deityId);
        return deity != null;
    }

    @Override
    public String toJSONString() {
        return "{deityId:" + deity.number + "}";
    }
}
