package net.spirangle.awakening.tasks;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.behaviours.MethodsItems;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BartenderTask extends Task {

    private static final Logger logger = Logger.getLogger(BartenderTask.class.getName());

    private static final int[] drinkAmounts = { 10, 20, 50, 100, 200, 330, 500, 1000, 1500, 2000 };


    private class Drink {
        public Item barrel;
        public int amount;
        public int price;
        public boolean tip;

        public Drink(Item barrel, int amount, int price, boolean tip) {
            this.barrel = barrel;
            this.amount = amount;
            this.price = price;
            this.tip = tip;
        }
    }

    public static String getAboutTaskTitle(AboutTaskParams params) {
        if(params.getTitle) params.title = "Bartender - sells drinks from sealed barrels to customers";
        if(params.getDescription) {
            params.header = "About Task: Bartender";
            params.description = "The bartender task makes the servant sell drinks from sealed barrels and wine barrels. " +
                                 "The barrels can either be standing on the ground or be placed in a rack. When instructing " +
                                 "the task the barrel needs to be on the same tile as the servant, but may later be moved " +
                                 "at most 5 tiles. If the barrel is moved more than five tiles or destroyed, the task will " +
                                 "automatically be dropped.\n\n" +
                                 "When instructing the bartender task, you may select the amount to serve and the price " +
                                 "for 0.1kg of the drink. You may also make the bartender accept tip, in which case a " +
                                 "small extra cost is added to the price, which increases with the drunkenness of the " +
                                 "customer.\n\n" +
                                 "The liquid in the barrel is automatically subtracted with each sold drink and when a " +
                                 "barrel is empty, the task is dropped. You may however unseal and refill the barrel and " +
                                 "re-seal it, and the task will continue working.\n\nDepending on the QL of the servant " +
                                 "task, the bartender may sell from many barrels. To do so, simply give more bartender " +
                                 "tasks for each barrel to sell from. To sell different amounts at different prices you " +
                                 "can give the task and point to the same barrel but with different settings.\n\n" +
                                 "The bartender task costs 5 iron daily per active bartender task.\n\n" +
                                 "Bartenders will only server drinks to customers for as long as they have something to " +
                                 "serve in in their inventory, this includes pottery beer steins, bowls, flasks and " +
                                 "jars, and buckets, skull mugs, and water skins. The employer may give items to serve " +
                                 "in, or any player, and the bartender will accept taking the items until the inventory " +
                                 "has been filled. If a player gives an item to serve in, there's no guarantee the same " +
                                 "item will be given back when serving the next drink, but continue buying drinks and " +
                                 "it will eventually be returned.";
        }
        return params.title;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "Bartender: Sell drinks";
            params.tooltip = "Sell drinks from a sealed wine barrel";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        if(taskPaper == null || responder == null) return true;
        Creature npc = servant.npc;
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        if(items.length >= 1) {
            for(Item item : items) {
                if(item.getTemplateId() == ItemList.wineBarrelSmall && item.isSealedByPlayer()) return true;
                else if(item.getTemplateId() == ItemList.wineBarrelRack) {
                    for(Item barrel : item.getItems())
                        if(barrel.getTemplateId() == ItemList.wineBarrelSmall && barrel.isSealedByPlayer()) return true;
                }
            }
        }
        return false;
    }

    public List<Drink> drinks = new ArrayList<>();

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_BARTENDER;
    }

    @Override
    public boolean isGroupedTask() { return true; }

    @Override
    public void addTask(Task task) {
        if(task instanceof BartenderTask) {
            List<Drink> d = ((BartenderTask) task).drinks;
            for(Drink drink : d) drinks.add(drink);
        }
    }

    @Override
    public String getTitle(Creature responder) {
        return "Bartender: Sell drinks from sealed wine barrels";
    }

    @Override
    public String getSubject(Creature responder) {
        float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
        if(alc == 100.0f) return "*burp* Bnghdr.. ergh.. dnk.. *hickup*";
        if(alc == 95.0f) return "*burp* Blartblurght, mowe dwnkh!! *hickup*";
        if(alc >= 90.0f) return "*burp* Bartendewssh, more.. *hickup*.. drinkssh!! *hickup*";
        if(alc >= 60.0f) return "Bartender, more drinks! *hickup*!";
        if(alc >= 30.0f) return "Bartender, more drinks! Quickly, I'm thirsty!";
        if(alc >= 20.0f) return "Bartender, bring the drinks menu!";
        if(alc >= 10.0f) return "Bartender, another look in the drinks menu, please.";
        return "Bartender, I would like to look in the drinks menu, please.";
    }

    @Override
    public long getSalary() { return 5L + drinks.size(); }

    @Override
    public float getWeight() { return 0.2f * (float) drinks.size(); }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 450;
        int h = 350;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Sell drinks from barrels:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"This task will instruct " + name + " to sell drinks from a sealed wine barrel.\n\nFrom which barrel would you like " + name + " to sell drinks?\"}\n");
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        int n = 0;
        wine_barrels_list:
        for(Item item : items) {
            if(item.getTemplateId() == ItemList.wineBarrelRack) {
                for(Item barrel : item.getItems())
                    if(addSealedWineBarrel(responder, bml, barrel)) {
                        if(++n >= 10) break wine_barrels_list;
                    }
            } else if(addSealedWineBarrel(responder, bml, item)) {
                if(++n >= 10) break;
            }
        }
        bml.append("   text{text=''}\n" +
                   "   text{text=\"You may move the barrel a maximum of five tiles. If the barrel is moved more than five tiles or destroyed, the task will automatically be dropped.\"}\n" +
                   "   text{text=''}\n" +
                   "   text{text='Amount of drink to serve?'}\n" +
                   "   harray{\n" +
                   "    dropdown{id='amount';size='150,1';default='3';options='0.01kg,0.02kg,0.05kg,0.1kg,0.2kg,0.33kg,0.5kg,1.0kg,1.5kg,2.0kg'}\n" +
                   "   }\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"What will be the price for 0.1kg of this drink?\"}\n" +
                   "   table{cols='8';" +
                   "    label{text=' '};label{text='Gold'};" +
                   "    label{text=' '};label{text='Silver'};" +
                   "    label{text=' '};label{text='Copper'};" +
                   "    label{text=' '};label{text='Iron'};" +
                   "    label{text=' '};input{maxchars='2';id='gold';text='0'};" +
                   "    label{text=' '};input{maxchars='2';id='silver';text='0'};" +
                   "    label{text=' '};input{maxchars='2';id='copper';text='0'};" +
                   "    label{text=' '};input{maxchars='2';id='iron';text='0'}" +
                   "   }\n" +
                   "   text{text=''}\n" +
                   "   checkbox{id='tip';text=\"Accept tip from customers\"}\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Instruct'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Bartender");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        String b = properties.getProperty("barrel");
        if(b == null) return false;
        long barrelId = Long.parseLong(b);
        try {
            Item barrel = Items.getItem(barrelId);
            if(isInReach(barrel)) {
                int amount = drinkAmounts[Integer.parseInt(properties.getProperty("amount", "3"))];
                int price = Integer.parseInt(properties.getProperty("gold", "0")) * 1000000 +
                            Integer.parseInt(properties.getProperty("silver", "0")) * 10000 +
                            Integer.parseInt(properties.getProperty("copper", "0")) * 100 +
                            Integer.parseInt(properties.getProperty("iron", "0"));
                boolean tip = "true".equals(properties.getProperty("tip", "true"));
                for(Drink drink : drinks)
                    if(drink.barrel == barrel) {
                        if(drink.amount != amount || drink.price != price || drink.tip != tip) {
                            drink.amount = amount;
                            drink.price = price;
                            drink.tip = tip;
                            updateDb();
                            responder.getCommunicator().sendNormalServerMessage(servant.npc.getName() + " updates the drinks menu.");
                        }
                        return false;
                    }
                Drink drink = new Drink(barrel, amount, price, tip);
                drinks.add(drink);
                return true;
            }
        } catch(NoSuchItemException e) {
            logger.log(Level.WARNING, "No barrel exists with the id: " + barrelId, e);
        }
        return false;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        Item container = getDrinkContainer(0);
        if(container == null) {
            Creature npc = servant.npc;
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: I'm sorry, but I've got nothing to serve your drink in.");
        } else {
            ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, 0, null);
            stq.sendQuestion();
        }
        return true;
    }

    @Override
    public boolean acceptItem(Creature performer, Item item) {
        if(isDrinkContainer(item)) {
            if(!item.isEmpty(false)) {
                Creature npc = servant.npc;
                performer.getCommunicator().sendNormalServerMessage(npc.getName() + " will only accept empty drink containers.");
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 450;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Buy a drink from the bartender:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{type='italic';text=\"" + (drinks.size() == 1? "This is the drink" : "These are the drinks") + " I can offer:\"}\n");
        String name, price;
        boolean update = false;
        DecimalFormat df = new DecimalFormat("#.##");
        for(Drink drink : drinks) {
            name = null;
            for(Item item : drink.barrel.getItems())
                if(item.isLiquid()) {
                    name = item.getName();
                    break;
                }
            if(!isInReach(drink.barrel)) name = null;
            if(name == null) {
                removeDrink(drink, false);
                update = true;
            } else {
                price = new Change(drink.price).getChangeString();
                bml.append("   radio{group='drink';id='" + drink.barrel.getWurmId() + "';text=\"" + StringUtils.capitalize(name) +
                           ", " + price + " [" + df.format((double) drink.amount * 0.001) + "kg]\"}");
            }
        }
        if(drinks.size() == 0) return;
        if(update) updateDb();
        float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
        bml.append("   radio{group='drink';id='0';selected='true';text=\"Do nothing\"}\n" +
                   "   text{text=''}\n");
        if(alc < 20.0f) bml.append("   checkbox{id='tip';text=\"Tip the bartender\"}\n");
        else bml.append("   text{type='italic';text=\"<tip> " +
                        (alc == 100.0f? "Tph.. tcke *hickup*" :
                         (alc >= 95.0f? "Tip.. take *hickup*" :
                          (alc >= 90.0f? "I.. lve yu.. *hickup*" :
                           (alc >= 60.0f? "You are my bestest friend! *hickup*" :
                            (alc >= 30.0f? "Here you go! Well deserved!" :
                             "And something extra for you!"))))) + "\"}");
        bml.append("  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='start';size='80,20';text='Back'}\n" +
                   "   text{size='" + (w - 6 - 160) + ",1';text=''}\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='submit';size='80,20';text='Buy'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        //      logger.info("BML: "+bml);
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        long drinkId = Long.parseLong(properties.getProperty("drink"));
        boolean tip = "true".equals(properties.getProperty("tip", "true"));
        if(drinkId == 0L) return;
        for(Drink drink : drinks) {
            if(drink.barrel.getWurmId() == drinkId) {
                serveDrink(responder, drink, tip);
                break;
            }
        }
    }

    private void serveDrink(Creature responder, Drink drink, boolean tip) {
        Item liquid = null;
        for(Item item : drink.barrel.getItems())
            if(item.isLiquid()) {
                liquid = item;
                break;
            }
        Creature npc = servant.npc;
        if(liquid == null || liquid.getWeightGrams() < drink.amount) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: I'm sorry, there isn't enough left in this barrel.");
            removeDrink(drink, true);
            return;
        }
        long money = responder.getMoney();
        if(drink.price > money) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: It seems you can't afford this drink.");
            return;
        }
        Item container = getDrinkContainer(drink.amount);
        if(container == null) {
            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: I need something larger to serve this drink in.");
            return;
        }
        int price = drink.price;
        if(tip) {
            if(!drink.tip) {
                responder.getCommunicator().sendNormalServerMessage(npc.getName() + " does not accept tip for this drink.");
            } else {
                float alc = responder.isPlayer()? ((Player) responder).getAlcohol() : 0.0f;
                if(alc == 100.0f) price += price * 5;
                else if(alc >= 95.0f) price += price * 2;
                else if(alc >= 90.0f) price += price;
                else if(alc >= 60.0f) price += price / 2;
                else if(alc >= 30.0f) price += price / 4;
                else if(alc >= 20.0f) price += price / 10;
                else price += price / 20;
            }
        }

        if(price > money) price = (int) money;
        try {
            responder.chargeMoney(price);
            servant.takeMoney(price);
        } catch(IOException e) { }
        responder.getCommunicator().sendNormalServerMessage("You pay " + npc.getName() + " " + new Change(price).getChangeString() +
                                                            ", and you receive a drink of " + liquid.getName() + ".");
        responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: Please return the " + container.getName() + " when you are done drinking.");
        try {
            Item item = MethodsItems.splitLiquid(liquid, drink.amount, responder);
            container.insertItem(item, true);
            responder.getInventory().insertItem(container, true);
            if(liquid.getWeightGrams() <= 0) {
                Items.destroyItem(liquid.getWurmId());
                drink.barrel.setIsSealedByPlayer(false);
            }
            if(liquid.getWeightGrams() <= drink.amount) removeDrink(drink, true);
        } catch(FailedException | NoSuchTemplateException e) {
            logger.log(Level.WARNING, "Could not split liquid.", e);
        }
    }

    private void removeDrink(Drink drink, boolean update) {
        if(drink != null) {
            drinks.remove(drink);
            if(drinks.size() == 0) servant.dropTask(this);
            else if(update) updateDb();
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        JSONArray ja = jo.optJSONArray("drinks");
        logger.info("BartenderTask.readJSON(drinks: " + ja.length() + ")");
        for(int i = 0; i < ja.length(); ++i) {
            JSONObject jo2 = ja.getJSONObject(i);
            long barrelId = jo2.optLong("barrel");
            logger.info("BartenderTask.readJSON(barrelId: " + barrelId + ")");
            try {
                Item barrel = Items.getItem(barrelId);
                if(isInReach(barrel)) {
                    int amount = jo2.optInt("amount");
                    int price = jo2.optInt("price");
                    boolean tip = jo2.optBoolean("tip");
                    drinks.add(new Drink(barrel, amount, price, tip));
                }
            } catch(NoSuchItemException e) {
                logger.log(Level.WARNING, "No barrel exists with the id: " + barrelId, e);
            }
        }
        return drinks.size() >= 1;
    }

    @Override
    public String toJSONString() {
        StringBuilder json = new StringBuilder();
        json.append("{drinks:[");
        int n = 0;
        for(Drink drink : drinks) {
            if(n > 0) json.append(',');
            json.append("{barrel:").append(drink.barrel.getWurmId())
                .append(",amount:").append(drink.amount)
                .append(",price:").append(drink.price)
                .append(",tip:").append(drink.tip? "true" : "false")
                .append('}');
            ++n;
        }
        json.append("]}");
        return json.toString();
    }

    private boolean isDrinkContainer(Item item) {
        switch(item.getTemplateId()) {
            case ItemList.bowlPottery:
            case ItemList.jarPottery:
            case ItemList.beerSteinPottery:
            case ItemList.skullMug:
            case ItemList.bucketSmall:
            case ItemList.skinWater:
            case ItemList.flaskPottery:
                return true;
        }
        return false;
    }

    private Item getDrinkContainer(int amount) {
        Creature npc = servant.npc;
        Set<Item> inv = npc.getInventory().getItems();
        for(Item i : inv) {
            logger.info("BartenderTask.getDrinkContainer(" + i.getName() + ": getFreeVolume[" + i.getFreeVolume() + "], amount[" + amount + "])");
            if(isDrinkContainer(i) && i.isEmpty(false) && (amount <= 0 || amount <= i.getFreeVolume()))
                return i;
        }
        return null;
    }

    private boolean addSealedWineBarrel(Creature performer, StringBuilder bml, Item item) {
        if(item.getTemplateId() == ItemList.wineBarrelSmall && item.isSealedByPlayer() && item.isViewableBy(performer)) {
            bml.append("   radio{group='barrel';id='" + item.getWurmId() + "';text=\"" + StringUtils.bmlString(item.getName()) + "\"}\n");
            return true;
        }
        return false;
    }

    private boolean isInReach(Item barrel) {
        Creature npc = servant.npc;
        if(barrel != null && npc != null && barrel.isOnSurface() == npc.isOnSurface()) {
            return Creature.getRange(npc, barrel.getPosX(), barrel.getPosY()) <= 20.0;
        }
        return false;
    }
}
