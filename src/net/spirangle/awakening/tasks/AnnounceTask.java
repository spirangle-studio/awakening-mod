package net.spirangle.awakening.tasks;

import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;
import java.util.logging.Logger;


public class AnnounceTask extends Task {

    private static final Logger logger = Logger.getLogger(AnnounceTask.class.getName());

    private static final int MAXCHARS = 100;
    private static final int[] RANGES = { 1, 2, 5, 10, 15, 20, 30 };
    private static final String[] RANGE_VERBS = { "whispers", "mumbles", "says", "yells", "shouts", "screams", "shrieks" };

    public static String getAboutTaskTitle(AboutTaskParams params) {
        if(params.getTitle) params.title = "Announce - announces a message to people close by";
        if(params.getDescription) {
            params.header = "About Task: Announce";
            params.description = "The announce task makes the servant say a message on a schedule every now and then. " +
                                 "Timing is random, but more or less frequent. Messages are said to the event log. " +
                                 "Note that frequent announcements may become a bit disturbing. More than one announcement " +
                                 "task can be given to the same servant, but each will add to the salary.\n\n" +
                                 "The options for each announcement is of course the text message, and then how often " +
                                 "and how loud cn be selected.\n\n" +
                                 "The louder and more often the higher the cost of the task. An announcement said " +
                                 "rarely will cost 1 iron daily, while always shrieking it will cost 10 iron.";
        }
        return params.title;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        params.title = "Announce message";
        params.tooltip = "Announce a message to nearby listeners";
        return params.title;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return true;
    }

    public String message = "";
    public float chance = 0.0f;
    public int range = 2;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_ANNOUNCE;
    }

    @Override
    public String getTitle(Creature responder) {
        return "Announce: " + (message.length() > 100? message.substring(0, 97) + "..." : message);
    }

    @Override
    public long getSalary() { return (chance <= 0.05f? 1 : (chance <= 0.15f? 2 : (chance <= 0.25f? 3 : 6))) + (range <= 2? 0 : range - 2); }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        int w = 450;
        int h = 350;
        String bml = "border{\n" +
                     " varray{rescale='true';\n" +
                     "  text{type='bold';text=\"Announce a message to nearby listeners:\"}\n" +
                     " };\n" +
                     " null;\n" +
                     " scroll{horizontal='false';vertical='true';\n" +
                     "  varray{rescale='true';\n" +
                     "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                     "   text{text=''}\n" +
                     "   text{text=\"This task will instruct " + name + " to announce a message now and then to nearby listeners. " +
                     "The message must not be too long for " + name + " to remember.\n\nWhat is the message to be announced?\"}\n" +
                     "   harray{\n" +
                     "    input{id='message';maxchars='" + MAXCHARS + "';maxlines='2';size='400,40';text=''}\n" +
                     "   }\n" +
                     "   text{text=''}\n" +
                     "   text{text=\"How often should the message be announced?\"}\n" +
                     "   radio{group='chance';id='50';text=\"always\"}\n" +
                     "   radio{group='chance';id='25';text=\"often\"}\n" +
                     "   radio{group='chance';id='15';text=\"sometimes\"}\n" +
                     "   radio{group='chance';id='5';text=\"rarely\";selected='true'}\n" +
                     "   text{text=''}\n" +
                     "   harray{\n" +
                     "    label{text=\"How loud should the message be announced? \"}\n" +
                     "    dropdown{id='range';default='2';options=\"whisper,mumble,speak,yell,shout,scream,shriek\"}\n" +
                     "   }\n" +
                     "  }\n" +
                     " };\n" +
                     " null;\n" +
                     " right{\n" +
                     "  harray{\n" +
                     "   button{id='start';size='80,20';text='Back'}\n" +
                     "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                     "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                     "   button{id='submit';size='80,20';text='Instruct'}\n" +
                     "  }\n" +
                     " }\n" +
                     "}\n";
        responder.getCommunicator().sendBml(w, h, false, true, bml, 200, 200, 200, "Servant: Announce Message");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        message = properties.getProperty("message");
        chance = (float) Integer.parseInt(properties.getProperty("chance")) / 100.0f;
        range = Integer.parseInt(properties.getProperty("range"));
        return message.length() > 0 && message.length() <= MAXCHARS;
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        message = jo.optString("message");
        chance = (float) jo.optDouble("chance");
        range = jo.optInt("range", 2);
        logger.info("AnnounceTask.readJSON(message: " + message + ", chance: " + chance + ")");
        return true;
    }

    @Override
    public String toJSONString() {
        return "{message:" + JSONObject.quote(message) +
               ",chance:" + chance +
               ",range:" + range + "}";
    }

    @Override
    public boolean pollChat() {
        float r1 = Server.rand.nextFloat();
        float r2 = Server.rand.nextFloat();
        logger.info("AnnounceTask.pollChat(r1: " + r1 + ", r2: " + r2 + ", chance: " + (chance * r2) + " [" + (r1 <= chance * r2? "true" : "false") + "], message: " + message + ")");
        if(r1 <= chance * r2) {
            Server.getInstance().broadCastAction(servant.npc.getNameWithGenus() + " " + RANGE_VERBS[range] + ": " + message, servant.npc, RANGES[range]);
            //			servant.talkLocal(message);
            return true;
        }
        return false;
    }
}
