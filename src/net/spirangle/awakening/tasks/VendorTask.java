package net.spirangle.awakening.tasks;

import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.Vehicle;
import com.wurmonline.server.behaviours.Vehicles;
import com.wurmonline.server.creatures.*;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.ItemMealData;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.players.PlayerInfoFactory;
import com.wurmonline.server.questions.MarketQuestion.MarketInfo;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import com.wurmonline.shared.util.StringUtilities;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.players.ChatChannels;
import net.spirangle.awakening.players.EconomyData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class VendorTask extends Task {

    private static final Logger logger = Logger.getLogger(VendorTask.class.getName());

    public static final byte CATEGORY_NONE = 0;
    public static final byte CATEGORY_ALL = 1;
    public static final byte CATEGORY_WEAPON_WOOD = 11;
    public static final byte CATEGORY_WEAPON_METAL = 12;
    public static final byte CATEGORY_WEAPON_RANGED = 15;
    public static final byte CATEGORY_ARMOUR_LEATHER = 21;
    public static final byte CATEGORY_ARMOUR_CHAIN = 22;
    public static final byte CATEGORY_ARMOUR_PLATE = 23;
    public static final byte CATEGORY_SHIELD_WOOD = 25;
    public static final byte CATEGORY_SHIELD_METAL = 26;
    public static final byte CATEGORY_CLOTHES = 31;
    public static final byte CATEGORY_ANIMALS = 36;
    public static final byte CATEGORY_FOOD_DISH = 41;
    public static final byte CATEGORY_FOOD_LIQUID = 42;
    public static final byte CATEGORY_FOOD_BAKED = 43;
    public static final byte CATEGORY_FOOD_CANDY = 44;
    public static final byte CATEGORY_BEVERAGE_DRINK = 51;
    public static final byte CATEGORY_BEVERAGE_ALCOHOL = 52;
    public static final byte CATEGORY_HEALING = 58;
    public static final byte CATEGORY_TOOL_SMITHING = 61;
    public static final byte CATEGORY_TOOL_TAILORING = 62;
    public static final byte CATEGORY_TOOL_CARPENTRY = 63;
    public static final byte CATEGORY_TOOL_FARMING = 64;
    public static final byte CATEGORY_TOOL_COOKING = 65;
    public static final byte CATEGORY_TOOL_MASONRY = 66;
    public static final byte CATEGORY_TOOL_FISHING = 67;
    public static final byte CATEGORY_TOOL_MISC = 68;
    public static final byte CATEGORY_BAGS = 71;
    public static final byte CATEGORY_CONTAINER_WOOD = 72;
    public static final byte CATEGORY_CONTAINER_METAL = 73;
    public static final byte CATEGORY_CONTAINER_POTTERY = 74;
    public static final byte CATEGORY_FURNITURE = 81;
    public static final byte CATEGORY_DECORATIONS = 82;
    public static final byte CATEGORY_LAMPS = 85;
    public static final byte CATEGORY_GEMS = 91;
    public static final byte CATEGORY_JEWELRY = 92;
    public static final byte CATEGORY_TOYS = 93;
    public static final byte CATEGORY_LOCKS = 95;
    public static final byte CATEGORY_PAPER_REPORTS = 101;

    private static final int VENDOR_ID_TALK_TO = -1;
    private static final int VENDOR_ID_CANCEL = 0;
    private static final int VENDOR_ID_START = 1;
    private static final int VENDOR_ID_SELL = 2;
    private static final int VENDOR_ID_TRADES = 3;

    public static class Category {
        public final byte id;
        public final String name;
        public final String title;
        public final String trade;
        public float price;
        public long budget;

        Category(byte id, String name, String title) {
            this(id, name, title, title);
        }

        Category(byte id, String name, String title, String trade) {
            this.id = id;
            this.name = name;
            this.title = title;
            this.trade = trade;
            this.price = 0.0f;
            this.budget = 0L;
        }
    }


    public static class Ware {
        public final int templateId;
        public final byte category;
        public final String name;
        public final float price;

        Ware(int templateId, byte category, String name) {
            this.templateId = templateId;
            this.category = category;
            this.name = name;
            this.price = 1.0f;
        }

        Ware(int templateId, byte category, String name, float price) {
            this.templateId = templateId;
            this.category = category;
            this.name = name;
            this.price = price;
        }
    }


    public static class Trade {
        public final VendorTask vendor;
        public final Item container;
        public final Category category;
        public float minQl;
        public float profit;
        public long money;
        public double supplyDemandFactor;

        Trade(VendorTask vendor, Item container, Category category, float minQl, float profit, long money, double supplyDemandFactor) {
            this.vendor = vendor;
            this.container = container;
            this.category = category;
            this.minQl = minQl;
            this.profit = profit;
            this.money = money;
            this.supplyDemandFactor = supplyDemandFactor;
        }

        Trade(Trade t) {
            this.vendor = t.vendor;
            this.container = t.container;
            this.category = t.category;
            this.minQl = t.minQl;
            this.profit = t.profit;
            this.money = t.money;
            this.supplyDemandFactor = t.supplyDemandFactor;
        }
    }


    public static class Deal {
        public final Category category;
        public final Trade trade;
        public final long itemId;
        public final long creatureId;
        public final long price;

        Deal(Category category, Trade trade, Item item, long price) {
            this.category = category;
            this.trade = trade;
            this.itemId = item.getWurmId();
            this.creatureId = -10L;
            this.price = price;
        }

        Deal(Category category, Trade trade, Creature creature, long price) {
            this.category = category;
            this.trade = trade;
            this.itemId = -10L;
            this.creatureId = creature.getWurmId();
            this.price = price;
        }
    }

    public static final Category[] categories = {
            new Category(CATEGORY_ALL, null, "generalWares", "all trades"),
            new Category(CATEGORY_WEAPON_WOOD, "weaponWood", "wood weapons"),
            new Category(CATEGORY_WEAPON_METAL, "weaponMetal", "metal weapons"),
            new Category(CATEGORY_WEAPON_RANGED, "weaponRanged", "ranged weapons"),
            new Category(CATEGORY_ARMOUR_LEATHER, "armourLeather", "leather armour"),
            new Category(CATEGORY_ARMOUR_CHAIN, "armourChain", "chain armour"),
            new Category(CATEGORY_ARMOUR_PLATE, "armourPlate", "plate armour"),
            new Category(CATEGORY_SHIELD_WOOD, "shieldWood", "wood shields"),
            new Category(CATEGORY_SHIELD_METAL, "shieldMetal", "metal shields"),
            new Category(CATEGORY_CLOTHES, "clothes", "clothes"),
            new Category(CATEGORY_ANIMALS, "animals", "animals"),
            new Category(CATEGORY_FOOD_DISH, "foodDish", "dishes"),
            new Category(CATEGORY_FOOD_LIQUID, "foodLiquid", "soups and stews"),
            new Category(CATEGORY_FOOD_BAKED, "foodBaked", "bread and pies"),
            new Category(CATEGORY_FOOD_CANDY, "foodCandy", "desserts and confections"),
            new Category(CATEGORY_BEVERAGE_DRINK, "beverageDrink", "drinks"),
            new Category(CATEGORY_BEVERAGE_ALCOHOL, "beverageAlcohol", "alcohol"),
            new Category(CATEGORY_HEALING, "healing", "healing"),
            new Category(CATEGORY_TOOL_SMITHING, "toolSmithing", "smithing tools"),
            new Category(CATEGORY_TOOL_TAILORING, "toolTailoring", "tailoring tools"),
            new Category(CATEGORY_TOOL_CARPENTRY, "toolCarpentry", "carpentry tools"),
            new Category(CATEGORY_TOOL_FARMING, "toolFarming", "farming tools"),
            new Category(CATEGORY_TOOL_COOKING, "toolCooking", "cooking tools"),
            new Category(CATEGORY_TOOL_MASONRY, "toolMasonry", "masonry tools"),
            new Category(CATEGORY_TOOL_FISHING, "toolFishing", "fishing tools"),
            new Category(CATEGORY_TOOL_MISC, "toolMisc", "miscellaneous tools"),
            new Category(CATEGORY_BAGS, "bags", "bags"),
            new Category(CATEGORY_CONTAINER_WOOD, "containerWood", "wood containers"),
            new Category(CATEGORY_CONTAINER_METAL, "containerMetal", "metal containers"),
            new Category(CATEGORY_CONTAINER_POTTERY, "containerPottery", "pottery containers"),
            new Category(CATEGORY_FURNITURE, "furniture", "furniture"),
            new Category(CATEGORY_DECORATIONS, "decorations", "decorations"),
            new Category(CATEGORY_LAMPS, "lamps", "lamps"),
            new Category(CATEGORY_GEMS, "gems", "gems"),
            new Category(CATEGORY_JEWELRY, "jewelry", "jewelry"),
            new Category(CATEGORY_TOYS, "toys", "toys"),
            new Category(CATEGORY_LOCKS, "locks", "locks and keys"),
            new Category(CATEGORY_PAPER_REPORTS, "paperReports", "reports"),
            };

    private static final String categoriesOptions;

    static {
        StringBuilder sb = new StringBuilder();
        for(Category c : categories) {
            if(sb.length() > 0) sb.append(',');
            sb.append(c.trade);
        }
        categoriesOptions = sb.toString();
    }

    private static final Category[] categoriesIndex;
    private static final Ware[] wares;
    private static final HashMap<Integer, Ware> waresIndex;

    private static final float[] minQls = { 0.0f/*any*/, 1.0f, 5.0f, 10.0f, 20.0f, 30.0f, 40.0f, 50.0f, 60.0f, 70.0f, 80.0f, 90.0f, 95.0f, 96.0f, 97.0f, 98.0f, 99.0f, 100.0f };
    private static final float[] profits = { 0.5f, 0.75f, 0.8f, 0.9f, 1.0f, 1.05f, 1.1f, 1.2f, 1.3f, 1.4f, 1.5f, 1.75f, 2.0f, 2.5f, 3.0f, 4.0f, 5.0f, 7.5f, 10f };

    private static final double animalBabyPrice = 0.7d;
    private static final double[] animalAgePrices = {
            0.8d, // young
            0.9d, // adolescent
            1.0d, // mature
            1.2d, // aged
            0.7d, // old
            0.2d  // venerable
    };
    private static final double[] animalTraitPrices = {
            1.6d,  // 0 "It will fight fiercely."
            1.8d,  // 1 "It has fleeter movement than normal."
            1.5d,  // 2 "It is a tough bugger."
            1.8d,  // 3 "It has a strong body."
            1.9d,  // 4 "It has lightning movement."
            1.8d,  // 5 "It can carry more than average."
            2.0d,  // 6 "It has very strong leg muscles."
            1.2d,  // 7 "It has keen senses."
            0.2d,  // 8 "It has malformed hindlegs."
            0.2d,  // 9 "The legs are of different length."
            0.7d,  // 10 "It seems overly aggressive."
            0.8d,  // 11 "It looks very unmotivated."
            0.8d,  // 12 "It is unusually strong willed."
            0.2d,  // 13 "It has some illness."
            0.8d,  // 14 "It looks constantly hungry."
            1.0d,  // 15 "brown"
            1.0d,  // 16 "gold"
            1.0d,  // 17 "black"
            1.0d,  // 18 "white"
            0.4d,  // 19 "It looks feeble and unhealthy."
            1.4d,  // 20 "It looks unusually strong and healthy."
            1.5d,  // 21 "It has a certain spark in its eyes."
            1.0d,  // 22 "It has been corrupted."
            1.0d,  // 23 "piebald pinto"
            1.1d,  // 24 "blood bay"
            1.15d, // 25 "ebony black"
            1.0d,  // 26
            1.0d,  // 27 "It bears the mark of the rift."
            1.0d,  // 28 "It bears the mark of a traitor."
            1.0d,  // 29 "It has a mark of Valrei."
            1.1d,  // 30 "skewbald pinto"
            1.1d,  // 31 "gold buckskin"
            1.1d,  // 32 "black silver"
            1.1d,  // 33 "appaloosa"
            1.1d,  // 34 "chestnut"
            1.0d,  // 35
            1.0d,  // 36
            1.0d,  // 37
            1.0d,  // 38
            1.0d,  // 39
            1.0d,  // 40
            1.0d,  // 41
            1.0d,  // 42
            1.0d,  // 43
            1.0d,  // 44
            1.0d,  // 45
            1.0d,  // 46
            1.0d,  // 47
            1.0d,  // 48
            1.0d,  // 49
            1.0d,  // 50
            1.0d,  // 51
            1.0d,  // 52
            1.0d,  // 53
            1.0d,  // 54
            1.0d,  // 55
            1.0d,  // 56
            1.0d,  // 57
            1.0d,  // 58
            1.0d,  // 59
            1.0d,  // 60
            1.0d,  // 61
            1.0d,  // 62
            1.0d   // 63 "It has been bred in captivity."
    };

    public static int getItemCategory(Item item) {
        if(item == null || item.getRarity() >= MiscConstants.RARE || item.isUnfinished()) return CATEGORY_NONE;
        ItemTemplate template = item.getTemplate();
        int templId = template.getTemplateId();
        if((templId == ItemList.paperSheet || templId == ItemList.papyrusSheet) && item.getAuxData() == 0)
            return CATEGORY_NONE;

        final Ware ware = waresIndex.get(templId);
        if(ware != null) return ware.category;

        if(template.isBulk() || template.isNoSellBack || item.isNoDrop() || template.isIndestructible() || item.isRoyal())
            return CATEGORY_NONE;

        if(template.isFood()) {
            if(item.isLiquid()) return CATEGORY_FOOD_LIQUID;
            else if(template.canLarder()) {
                if(item.isBaked()) return CATEGORY_FOOD_BAKED;
                else if(item.isCandied() || item.isChocolateCoated()) return CATEGORY_FOOD_CANDY;
                else if(item.isDish()) return CATEGORY_FOOD_DISH;
            }
        } else if(item.isDrinkable()) {
            if(template.isAlcohol()) return CATEGORY_BEVERAGE_ALCOHOL;
            if(templId != ItemList.water && !item.isMilk()) return CATEGORY_BEVERAGE_DRINK;
        } else if(templId == ItemList.woundCover) {
            return CATEGORY_HEALING;
        } else if(template.isTool()) {
            if(template.isSmithingTool() || template.isMiningtool()) return CATEGORY_TOOL_SMITHING;
            else if(template.isCarpentryTool()) return CATEGORY_TOOL_CARPENTRY;
            else if(template.isFieldTool() || template.isDiggingtool() || template.isDredgingTool)
                return CATEGORY_TOOL_FARMING;
            else if(template.isCookingTool()) return CATEGORY_TOOL_COOKING;
            else if(template.isFishingReel() || templId == ItemList.netFishing) return CATEGORY_TOOL_FISHING;
            else return CATEGORY_TOOL_MISC;
        } else if(template.isWeapon()) {
            if(templId == ItemList.clubHuge) return CATEGORY_NONE;
            if(template.isWood()) return CATEGORY_WEAPON_WOOD;
            else if(item.isMetal()) return CATEGORY_WEAPON_METAL;
        } else if(item.isWeaponBow() || item.isBowUnstringed() || item.isQuiver()) {
            return CATEGORY_WEAPON_RANGED;
        } else if(template.isShield()) {
            if(template.isMetal()) return CATEGORY_SHIELD_METAL;
            return CATEGORY_SHIELD_WOOD;
        } else if(template.isArmour()) {
            if(template.isCloth()) return CATEGORY_CLOTHES;
            else if(template.isLeather()) {
                if(!item.isDragonArmour()) return CATEGORY_ARMOUR_LEATHER;
            } else if(template.isMetal()) {
                if(template.getName().indexOf("chain") >= 0) return CATEGORY_ARMOUR_CHAIN;
                else return CATEGORY_ARMOUR_PLATE;
            }
        } else if(template.isDecoration() && !template.isContainerLiquid()) {
            if(template.isWood()) return CATEGORY_FURNITURE;
            else if(templId != ItemList.corpse) return CATEGORY_DECORATIONS;
        } else if(item.isStreetLamp() || item.isCandleHolder() || item.isOilConsuming()) {
            return CATEGORY_LAMPS;
        } else if(item.isGem()) {
            return CATEGORY_GEMS;
        } else if(item.isEnchantableJewelry()) {
            return CATEGORY_JEWELRY;
        } else if(templId == ItemList.yoyo || template.isPuppet()) {
            return CATEGORY_TOYS;
        } else if(template.isHollow()) {
            if(template.isCloth()) return CATEGORY_BAGS;
            else if(template.isWood()) return CATEGORY_CONTAINER_WOOD;
            else if(template.isMetal()) return CATEGORY_CONTAINER_METAL;
            else if(template.isPottery()) return CATEGORY_CONTAINER_POTTERY;
        } else if(template.isLock() || template.isKey()) {
            return CATEGORY_LOCKS;
        }
        return CATEGORY_NONE;
    }

    public static int getItemCategory(int id) {
        Ware ware = waresIndex.get(id);
        if(ware == null) return CATEGORY_NONE;
        return ware.category;
    }

    public static int getCreatureCategory(Creature creature) {
        return CATEGORY_ANIMALS;
    }

    private static boolean isCraftedItem(Item item) {
        return item.creator != null && item.creator.length() > 0;
    }

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Vendor: Buy crafted and cooked wares";
            params.tooltip = "Buy items from a dedicated container";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        if(taskPaper == null || responder == null) return true;
        return responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public static void pollYearBudget() {
        logger.info("VendorTask: Poll year budget...");
        int[] scores = { 0, 0, 0, 0, 0 };
        for(Servant s : Servant.servants) {
            VendorTask vendor = (VendorTask) s.getTaskByType(AwakeningConstants.TASK_VENDOR);
            if(vendor != null) {
                boolean update = false;
                int kingdomId = s.npc.getKingdomId();
                for(Trade t : vendor.trades) {
                    if(t.money < t.category.budget) {
                        float result = (float) t.money / (float) t.category.budget;
                        double sdf = 0.0;
                        if(result >= 1.5) sdf += 0.15;
                        else if(result >= 1.0) sdf += 0.1;
                        else if(result >= 0.5) sdf += 0.05;
                        else if(result >= 0.3) sdf += 0.02;
                        else if(result >= 0.2) sdf += 0.01;
                        else if(result <= 0.02f) {
                            scores[kingdomId] += 5;
                            sdf -= 0.03;
                        } else if(result <= 0.1f) {
                            scores[kingdomId] += 2;
                            sdf -= 0.02;
                        } else if(result <= 0.2f) {
                            scores[kingdomId] += 1;
                            sdf -= 0.01;
                        }
                        t.money += t.category.budget;
                        t.supplyDemandFactor = 1.0d;//normalizeSupplyDemandFactor(t.supplyDemandFactor+sdf);
                        update = true;
                    }
                    long max = (t.category.budget * 150) / 100;
                    if(t.money > max) t.money = max;
                }
                if(update) vendor.updateDb();
            }
        }
        long lastLogoutAfter = System.currentTimeMillis() - (29030400L * 1000L);
        PlayerInfo[] playerInfos = PlayerInfoFactory.getPlayerInfos();
        for(int kingdomId = 1; kingdomId <= 4; ++kingdomId) {
            if(scores[kingdomId] > 0) {
                Kingdom kingdom = Kingdoms.getKingdomOrNull((byte) kingdomId);
                int sleepBonus = Math.min(18000, 60 * scores[kingdomId]);
                for(PlayerInfo pi : playerInfos) {
                    try {
                        pi.load();
                        if(pi.lastLogout >= lastLogoutAfter && pi.getChaosKingdom() == kingdomId) {
                            giveYearBudgetBonus(kingdom, pi.wurmId, sleepBonus);
                        }
                    } catch(IOException ioe) { }
                }
            }
        }
        String message = "This year's budget has been given to the vendors.";
        ChatChannels.broadCastMessage(message, 0x66CC66);
        ChatChannels.getInstance().sendHistoryMessage(message);

        EconomyData.pollNewYear();
    }

    private static double normalizeSupplyDemandFactor(double sdf) {
        return Math.max(0.75, Math.min(1.25, sdf));
    }

    private static void giveYearBudgetBonus(Kingdom kingdom, long wurmId, int sleepBonus) {
        try {
            final Player p = Players.getInstance().getPlayer(wurmId);
            p.getSaveFile().addToSleep(sleepBonus);
            StringBuilder message = new StringBuilder();
            message.append("All citizens of ").append(kingdom.getName()).append(" gain ");
            int h = sleepBonus / 3600;
            int m = (sleepBonus % 3600) / 60;
            if(h == 5) {
                message.append("full sleep bonus, having tirelessly worked hard to complete this year's budget.");
            } else {
                if(h > 0) {
                    message.append(StringUtilities.getWordForNumber(h)).append(h == 1? " hour" : " hours");
                    if(m > 0) message.append(" and ");
                }
                if(m > 0) message.append(m).append(m == 1? " minute" : " minutes");
                message.append(" of sleep bonus, having worked well with completing this year's budget.");
            }
            p.getCommunicator().sendSafeServerMessage(message.toString());
        } catch(NoSuchPlayerException e) {
            final PlayerInfo pi = PlayerInfoFactory.getPlayerInfoWithWurmId(wurmId);
            if(pi != null) {
                try {
                    pi.load();
                    if(pi.wurmId <= 0L) return;
                    pi.addToSleep(sleepBonus);
                } catch(IOException ioe) { }
            }
        }
    }

    public static void sendKingdomBudget(Player performer) {
        Kingdom kingdom = Kingdoms.getKingdomOrNull(performer.getKingdomId());
        if(kingdom == null || performer.getKingdomId() == 0) {
            performer.getCommunicator().sendNormalServerMessage("You are not a citizen of any kingdom.");
        }
        sendKingdomBudget(performer, kingdom);
    }

    public static void sendKingdomBudget(Player performer, Kingdom kingdom) {
        if(kingdom == null) {
            sendKingdomBudget(performer);
            return;
        }
        Communicator communicator = performer.getCommunicator();
        List<String> vendorBudgets = new ArrayList<>();
        for(Servant s : Servant.servants) {
            VendorTask vendor = (VendorTask) s.getTaskByType(AwakeningConstants.TASK_VENDOR);
            if(vendor != null) {
                int kingdomId = s.npc.getKingdomId();
                if(kingdomId != kingdom.getId()) continue;
                for(Trade t : vendor.trades) {
                    float result = (float) t.money / (float) t.category.budget;
                    if(result > 1.0f) result = 1.0f;
                    vendorBudgets.add(t.category.title + " " + Math.round(result * 100.0f) + "%");
                }
            }
        }
        vendorBudgets = vendorBudgets.stream().sorted().collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        sb.append(kingdom.getName()).append(" vendor budgets: ");
        int n = 0;
        for(String vb : vendorBudgets) {
            if(n > 0 && n < 10) sb.append(", ");
            else if(n == 10) {
                communicator.sendNormalServerMessage(sb.toString());
                sb = new StringBuilder();
                n = 0;
            }
            sb.append(vb);
            ++n;
        }
        if(n == 0) {
            sb.append(" kingdom has no vendors");
        }
        communicator.sendNormalServerMessage(sb.toString());
    }

    public static Map<String, Trade> getKingdomBudget(Player performer) {
        return getKingdomBudget(Kingdoms.getKingdomOrNull(performer.getKingdomId()));
    }

    public static Map<String, Trade> getKingdomBudget(Kingdom kingdom) {
        if(kingdom == null || kingdom.getId() == 0) return null;
        Map<String, Trade> vendorTrades = new HashMap<>();
        for(Servant s : Servant.servants) {
            VendorTask vendor = (VendorTask) s.getTaskByType(AwakeningConstants.TASK_VENDOR);
            if(vendor != null) {
                if(s.npc.getKingdomId() != kingdom.getId()) continue;
                for(Trade trade : vendor.trades) {
                    Trade t = vendorTrades.get(trade.category.title);
                    if(t == null) t = new Trade(trade);
                    else t.money += trade.money;
                    vendorTrades.put(t.category.title, t);
                }
            }
        }
        return vendorTrades.entrySet().stream().sorted(Entry.comparingByKey())
                           .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public static List<MarketInfo> getMarketInfo(Player performer, ItemTemplate template) {
        Ware ware = waresIndex.get(template.getTemplateId());
        List<MarketInfo> result = new ArrayList<>();
        if(ware == null) return result;
        for(Servant s : Servant.servants) {
            VendorTask vendor = (VendorTask) s.getTaskByType(AwakeningConstants.TASK_VENDOR);
            if(vendor != null) {
                for(Trade trade : vendor.trades) {
                    if(trade.category.id == ware.category || trade.category.id == CATEGORY_ALL) {
                        String location = Kingdoms.getChatNameFor(s.npc.currentKingdom);
                        String container = trade.container != null? trade.container.getTemplate().getName() : "";
                        result.add(new MarketInfo(s.npc.getName(), trade.category.trade, location, container));
                        break;
                    }
                }
            }
        }
        return result;
    }

    private final List<Trade> trades = new ArrayList<>();
    private final boolean[] tradesCategories = new boolean[categoriesIndex.length];

    @Override
    protected void init(long id, float ql, Item taskPaper, Servant servant) {
        super.init(id, ql, taskPaper, servant);
        updateTrades();
    }

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_VENDOR;
    }

    @Override
    public boolean isGroupedTask() { return true; }

    @Override
    public void addTask(Task task) {
        if(task instanceof VendorTask) {
            List<Trade> t = ((VendorTask) task).trades;
            for(Trade trade : t) trades.add(trade);
            updateTrades();
        }
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Buy crafted or cooked items";
    }

    @Override
    public String getSubject(Creature responder) {
        return "Vendor, I would like to sell goods.";
    }

    @Override
    public long getSalary() { return 0L; }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        int w = 350;
        int h = 550;
        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Instruct servant in vendor task:\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='").append(question.getId()).append("'};\n")
           .append("   text{text=''}\n")
           .append("   text{text=\"This task will instruct ").append(name).append(" to buy crafted and cooked wares.\n\n")
           .append("Optionally, merchandise will be bought from a nearby container, which?\"}\n");
        VolaTile tile = Zones.getOrCreateTile(npc.getTileX(), npc.getTileY(), npc.isOnSurface());
        Item[] items = tile.getItems();
        int n = 0;
        containers_list:
        for(Item item : items) {
            if(isContainer(item) && item.isViewableBy(responder)) {
                String itemName = StringUtils.bmlString(item.getName());
                bml.append("   radio{group='container';id='").append(item.getWurmId()).append("';text=\"").append(itemName).append("\"}\n");
                if(++n >= 10) break;
            }
        }
        bml.append("   radio{group='container';id='-10';selected='true';text=\"Use no container\"}\n")
           .append("   text{text=''}\n")
           .append("   text{text=\"You may move the container a maximum of five tiles. If the container is moved more than ")
           .append("five tiles or destroyed, the task will automatically be dropped.\"}\n");
        if(responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
            bml.append("   text{text=''}")
               .append("   text{text=\"What trade will the vendor specialize in?\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='category';default='0';options=\"").append(categoriesOptions).append("\"}\n")
               .append("   }\n")
               .append("   text{text=''}\n")
               .append("   text{text=\"Minimum QL of wares trader accepts trading in?\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='minql';default='0';options=\"1,5,10,20,30,40,50,60,70,80,90,95,96,97,98,99,100\"}\n")
               .append("   }\n")
               .append("   text{text=''}\n")
               .append("   text{text=\"Profit modifier?\";hover=\"0.5=trader makes a 50% loss, 1=trader makes no profit, 2=trader doubles price, etc.\"}\n")
               .append("   harray{\n")
               .append("    dropdown{id='profit';default='13';options=\"0.5,0.75,0.8,0.9,1,1.05,1.1,1.2,1.3,1.4,1.5,1.75,2,2.5,3,4,5,7.5,10\"}\n")
               .append("   }\n");
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='start';size='80,20';text='Back'}\n")
           .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
           .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
           .append("   button{id='submit';size='80,20';text='Instruct'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servant: Vendor");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        Creature responder = question.getResponder();
        String c = properties.getProperty("container");
        long containerId = c != null? Long.parseLong(c) : -10L;
        try {
            int categoryId = categories[Integer.parseInt(properties.getProperty("category", "0"))].id;
            if(categoryId == CATEGORY_ANIMALS) containerId = -10L;
            Item container = containerId != -10L? Items.getItem(containerId) : null;
            if(container == null || isInReach(container)) {
                Category category = categoriesIndex[categoryId];
                float minQl = minQls[Integer.parseInt(properties.getProperty("minql", "0")) + 1];
                float profit = profits[Integer.parseInt(properties.getProperty("profit", "14"))];
                for(Trade trade : trades)
                    if(trade.container == container && trade.category == category) {
                        if(trade.minQl != minQl || trade.profit != profit) {
                            trade.minQl = minQl;
                            trade.profit = profit;
                            if(trade.money > trade.category.budget) trade.money = trade.category.budget;
                            updateDb();
                            updateTrades();
                            responder.getCommunicator().sendNormalServerMessage(servant.npc.getName() + " updates trades.");
                        }
                        return false;
                    }
                Trade trade = new Trade(this, container, category, minQl, profit, category.budget, 1.0);
                trades.add(trade);
                updateTrades();
                return true;
            }
        } catch(NoSuchItemException e) {
            logger.log(Level.WARNING, "No container exists with the id: " + containerId, e);
        }
        return false;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        ServantTaskQuestion stq = new ServantTaskQuestion(responder, question.getSourceItem(), this, VENDOR_ID_START, new ArrayList<Deal>());
        stq.sendQuestion();
        return true;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        List<Deal> deals = (ArrayList<Deal>) data;
        StringBuilder bml = new StringBuilder();
        StringBuilder passthrough = new StringBuilder();
        int buttons = VENDOR_ID_CANCEL;
        int back = VENDOR_ID_TALK_TO;
        int w = 350;
        int h = 350;
        Item vehicle = null;
        boolean update = false;
        boolean tariff = responder.getStatus().kingdom != npc.getStatus().kingdom;

        passthrough.append("passthrough{id='id';text='").append(question.getId()).append("'};");

        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Talking to vendor:\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n");

        long dayOfYear = (WurmCalendar.getCurrentTime() % 2419200L) / 86400L;
        int hour = WurmCalendar.getHour();
        if(tariff && dayOfYear >= 28L && hour >= 12 && dayOfYear <= 28 * 9 && hour < 12) {
            bml.append("   text{text=''}\n")
               .append("   text{type='italic';text=\"I'm sorry, we don't accept foreign trade this part of the year. If you return ")
               .append("after first day of starfall of the Dancer, at noon - I may still have enough budget for trading with you.\"}\n");
        } else if(id == VENDOR_ID_START) {
            deals.clear();
            HashSet<Item> items = new HashSet<>();
            HashSet<Creature> creatures = new HashSet<>();
            for(Trade trade : trades) {
                if(trade.category.id == CATEGORY_ANIMALS) {
                    for(int y = responder.getTileY() - 3; y <= responder.getTileY() + 3; ++y)
                        for(int x = responder.getTileX() - 3; x <= responder.getTileX() + 3; ++x) {
                            VolaTile tile = Zones.getOrCreateTile(x, y, responder.isOnSurface());
                            if(tile != null) {
                                Creature[] tileCreatures = tile.getCreatures();
                                for(Creature creature : tileCreatures)
                                    if(creature.getLeader() != null && creature.getLeader().getWurmId() == responder.getWurmId() && creature.isBred())
                                        creatures.add(creature);
                            }
                        }
                    continue;
                }
                if(trade.container == null) continue;
                if(!isInReach(trade.container)) {
                    removeTrade(trade, false);
                    update = true;
                } else if(trade.container.getTemplateId() == ItemList.larder) {
                    for(Item box : trade.container.getItems()) {
                        if(box.getTemplateId() == ItemList.icebox) continue;
                        for(Item f : box.getItems()) {
                            if(f.getDamage() > 0.0f) continue;
                            items.add(f);
                        }
                    }
                } else {
                    for(Item item : trade.container.getItems())
                        items.add(item);
                }
            }
            if(items.size() == 0 && question.getSourceItem() != null)
                items.add(question.getSourceItem());
            if(items.size() == 0 && responder.isVehicleCommander() && responder.getVehicle() != -10L) {
                Vehicle v = Vehicles.getVehicleForId(responder.getVehicle());
                if(v != null && !v.isCreature()) {
                    try {
                        vehicle = Items.getItem(responder.getVehicle());
                        for(Item item : vehicle.getItems())
                            items.add(item);
                    } catch(NoSuchItemException e) { }
                }
            }

            for(Item item : items)
                addItemToDeal(deals, item);
            for(Creature creature : creatures)
                addCreatureToDeal(deals, creature);

            if(deals.size() > 0) {
                long price = 0L;
                int itemDeals = 0;
                int creatureDeals = 0;
                Iterator<Deal> iter = deals.iterator();
                while(iter.hasNext()) {
                    Deal deal = iter.next();
                    price += deal.price;
                    if(deals.size() <= 1) break;
                    if(deal.itemId != -10L) {
                        try {
                            Item item = Items.getItem(deal.itemId);
                            int diff = (int) (item.getQualityLevel() - deal.trade.minQl);
                            if(diff < 0) {
                                price -= deal.price;
                                iter.remove();
                            }
                        } catch(NoSuchItemException e) {
                        }
                    }
                    if(deal.itemId != -10L) ++itemDeals;
                    if(deal.creatureId != -10L) ++creatureDeals;
                }
                int diff = 0;
                bml.append("   text{text=''}\n")
                   .append("   text{type='italic';text=\"");
                if(deals.size() == 1) {
                    Deal deal = deals.get(0);
                    if(deal.itemId != -10L) {
                        try {
                            Item item = Items.getItem(deal.itemId);
                            String wareName = StringUtils.bmlString(item.getName());
                            bml.append("I'm looking at the ").append(wareName);
                            if(question.getSourceItem() == item) {
                                bml.append(" that you are holding.");
                            } else if(vehicle != null) {
                                String vehicleName = StringUtils.bmlString(vehicle.getName());
                                bml.append(" that you have in your ").append(vehicleName).append(".");

                            } else {
                                String containerName = StringUtils.bmlString(deal.trade.container.getName());
                                bml.append(" that you've placed in my ").append(containerName).append(" here.");
                            }
                            diff = (int) (deal.trade.minQl - item.getQualityLevel());
                        } catch(NoSuchItemException e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }
                    } else if(deal.creatureId != -10L) {
                        try {
                            Creature creature = Creatures.getInstance().getCreature(deal.creatureId);
                            String wareName = StringUtils.bmlString(creature.getName());
                            bml.append("I'm looking at ").append(wareName).append(", that you are leading to my store.");
                        } catch(NoSuchCreatureException e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }
                    }
                    if(diff > 0) {
                        bml.append(" I'm not interested to be honest. I had something with a bit higher quality in mind.");
                    } else if(price > deal.trade.money) {
                        bml.append(" I'm interested in what you are showing me, but I've already spent this year's ")
                           .append(deal.category.name).append(" budget, and could not afford anything more of that kind.");
                        price = 0L;
                    } else {
                        bml.append(" The price I have in mind is:");
                    }
                } else {
                    if(itemDeals >= 1 && creatureDeals == 0) {
                        bml.append("I'm looking at the wares that you bring to my shop. For all of the ")
                           .append(StringUtilities.getWordForNumber(itemDeals)).append(" items I'll give you a good price:");
                    } else if(itemDeals == 0 && creatureDeals >= 1) {
                        bml.append("I'm looking at the animals that you bring. For all of the ")
                           .append(StringUtilities.getWordForNumber(creatureDeals)).append(" creatures I'll give you a good price:");
                    } else if(itemDeals >= 1 && creatureDeals >= 1) {
                        bml.append("For all of the ").append(StringUtilities.getWordForNumber(itemDeals))
                           .append(" wares, and the ").append(StringUtilities.getWordForNumber(creatureDeals))
                           .append(" animals I'll give you a good price:");
                    }
                }
                bml.append("\"}\n");
                if(diff <= 0 && price > 0) {
                    bml.append("   text{text=''}\n")
                       .append("   text{type='bold';text=\"").append(new Change(price).getChangeString()).append("\"}\n")
                       .append("   text{text=''}\n")
                       .append("   text{type='italic';text=\"Do you accept this offer?\"}\n");
                    buttons = VENDOR_ID_SELL;
                }
                if(vehicle != null) {
                    String vehicleName = StringUtils.bmlString(vehicle.getName());
                    bml.append("   text{text=''}\n")
                       .append("   text{color='255,127,127';text=\"Note that you are selling wares from your ").append(vehicleName).append(".\"}\n");
                }
            } else if(trades.size() == 0) {
                bml.append("   text{text=''}\n")
                   .append("   text{type='italic';text=\"Hello! I don't currently trade in anything.\"}\n");
            } else {
                boolean gm = responder.getPower() >= MiscConstants.POWER_DEMIGOD;
                bml.append("   text{text=''}\n")
                   .append("   text{type='italic';text=\"Hello! I specialize in the following trades:\"}\n")
                   .append("   text{text=''}\n")
                   .append("   table{cols='").append(gm? 3 : 2).append("';\n")
                   .append("    label{type='bold';text='Trade'};\n")
                   .append("    label{type='bold';text='Container'};\n");
                if(gm) bml.append("    label{type='bold';text='GM Only'};\n");
                for(Trade trade : trades) {
                    bml.append("    label{text=\"").append(trade.category.title).append("\"};\n")
                       .append("    label{text=\"").append(trade.container != null? StringUtils.bmlString(trade.container.getName()) : "-").append("\"};\n");
                    if(gm)
                        bml.append("    label{text=\"[minQl=").append(trade.minQl).append(", profit=").append(trade.profit)
                           .append(", budget=").append(trade.money).append("/").append(trade.category.budget).append("]\"};\n");
                }
                bml.append("   }\n")
                   .append("   text{text=''}\n");
                boolean tradeItems = false;
                boolean tradeAnimals = false;
                for(Trade t : trades) {
                    if(t.category.id == CATEGORY_ANIMALS) tradeAnimals = true;
                    else tradeItems = true;
                    if(tradeItems && tradeAnimals) break;
                }
                if(tradeItems) {
                    bml.append("   text{type='italic';text=\"Place your wares in this container; ")
                       .append("then come talk to me again and we will make a bargain. You can also show one ware at a time, and I will have a look.\"}\n");
                }
                if(tradeAnimals) {
                    bml.append("   text{type='italic';text=\"I will have a look at your animals if you lead them to me, and give you a good price for them.\"}\n");
                }
                buttons = VENDOR_ID_TRADES;
                w = gm? 550 : 450;
            }
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n");
        if(buttons == VENDOR_ID_CANCEL) {
            passthrough.append("passthrough{id='backTo';text='").append(back).append("'};");
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 160).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n");
        } else if(buttons == VENDOR_ID_SELL) {
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 240).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='submit';size='80,20';text='Sell'}\n");
        } else {
            bml.append("   button{id='back';size='80,20';text='Back'}\n")
               .append("   text{size='").append(w - 6 - 160).append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Close'}\n");
        }
        bml.append("   ").append(passthrough).append("\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        if(update) updateDb();
//        logger.info("BML: "+bml.toString());
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        List<Deal> deals = (List<Deal>) data;
        boolean update = false;

        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("back"))) {
            id = Integer.parseInt(properties.getProperty("backTo", String.valueOf(VENDOR_ID_TALK_TO)));
            if(id == VENDOR_ID_TALK_TO) {
                TalkToServantQuestion ttq = new TalkToServantQuestion(responder, question.getSourceItem(), servant);
                ttq.sendQuestion();
            }
        } else if(id == VENDOR_ID_START) {
            Player player = (Player) responder;
            int itemDeals = 0, creatureDeals = 0;
            long price = 0L;
            boolean budgetMessage = true;
            int templateId = 0;
            int category = 0;
            double ql = 0.0f;
            float amount = 0.0f;
            int weight = 0;
            byte material = 0;
            byte aux = 0;
            boolean tariff = responder.getStatus().kingdom != npc.getStatus().kingdom;
            for(Deal deal : deals) {
                if(deal.itemId != -10L) {
                    try {
                        Item item = Items.getItem(deal.itemId);
                        if(deal.trade.money >= deal.price) {
                            if(itemDeals == 0 && creatureDeals == 0) {
                                templateId = item.getTemplateId();
                                category = deal.trade.category.id;
                                material = item.getMaterial();
                                aux = item.getAuxData();
                            } else {
                                if((templateId != 0 && item.getTemplateId() != templateId) || creatureDeals > 0)
                                    templateId = 0;
                                if(category != 0 && deal.trade.category.id != category) category = 0;
                                if(material != 0 && item.getMaterial() != material) material = 0;
                                if(aux != 0 && item.getAuxData() != aux) aux = 0;
                            }
                            ++itemDeals;
                            ++amount;
                            weight += item.getWeightGrams();
                            ql += item.getQualityLevel();
                            price += deal.price;
                            deal.trade.money -= deal.price;
                            update = true;
                            Items.destroyItem(item.getWurmId());
                        } else if(budgetMessage) {
                            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: Buying the " + StringUtilities.addGenus(item.getName()) +
                                                                                " would break my budget for " + deal.category.title + ".");
                            budgetMessage = false;
                        }
                    } catch(NoSuchItemException e) {
                    }
                } else if(deal.creatureId != -10L) {
                    try {
                        Creature creature = Creatures.getInstance().getCreature(deal.creatureId);
                        if(deal.trade.money >= deal.price) {
                            if(itemDeals == 0 && creatureDeals == 0) {
                                templateId = creature.getTemplateId();
                                category = deal.trade.category.id;
                            } else {
                                if((templateId != 0 && creature.getTemplateId() != templateId) || itemDeals > 0)
                                    templateId = 0;
                                if(category != 0 && deal.trade.category.id != category) category = 0;
                                material = 0;
                                aux = 0;
                            }
                            ++itemDeals;
                            ++amount;
                            price += deal.price;
                            deal.trade.money -= deal.price;
                            update = true;
                            creature.destroy();
                        } else if(budgetMessage) {
                            responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: Buying " + StringUtilities.addGenus(creature.getName()) +
                                                                                " would break my budget for " + deal.category.title + ".");
                            budgetMessage = false;
                        }
                    } catch(NoSuchCreatureException e) {
                    }
                }
            }
            if(itemDeals >= 1 || creatureDeals >= 1) {
                String coins = new Change(price).getChangeString();
                StringBuilder sb = new StringBuilder();
                sb.append("You sell ");
                if(itemDeals >= 1) sb.append(itemDeals).append(" items");
                if(itemDeals >= 1 && creatureDeals >= 1) sb.append(" and ");
                if(creatureDeals >= 1) sb.append(creatureDeals).append(" animals");
                sb.append(" for the amount of " + coins + ".");
                responder.getCommunicator().sendNormalServerMessage(sb.toString());
                long before = player.getMoney();
                EconomyData account = PlayersData.getInstance().getBankAccount(player);
                account.addMoney(player, price, tariff);
                ql /= itemDeals;
                try(Connection con = ModSupportDb.getModSupportDb()) {
                    if(itemDeals >= 1)
                        account.addTransaction(con, player, templateId, 2, category, npc, (float) ql, amount, weight, material, aux, price, tariff);
                    if(creatureDeals >= 1)
                        account.addTransaction(con, player, templateId, 3, category, npc, 0.0f, amount, 0, 0, 0, price, tariff);
                } catch(SQLException e) {
                    logger.log(Level.SEVERE, "Failed to save transaction: " + e.getMessage(), e);
                }
                logger.info(player.getName() + " sold " + itemDeals + "x item" + (itemDeals == 1? "" : "s") + " to vendor " + npc.getName() +
                            ", for the amount of " + coins + ". [" + before + " => " + player.getMoney() + "]");
            }
        }
        if(update) updateDb();
    }

    private void removeTrade(Trade trade, boolean update) {
        if(trade != null) {
            trades.remove(trade);
            if(trades.size() == 0) servant.dropTask(this);
            else if(update) updateDb();
            updateTrades();
        }
    }

    private void updateTrades() {
        if(trades.size() > 0) {
            for(int i = 0; i < categoriesIndex.length; ++i) tradesCategories[i] = false;
            for(Trade trade : trades) {
                tradesCategories[trade.category.id] = true;
            }
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        JSONArray ja = jo.optJSONArray("trades");
        for(int i = 0; i < ja.length(); ++i) {
            JSONObject jo2 = ja.getJSONObject(i);
            int categoryId = jo2.optInt("category");
            Category category = categoriesIndex[categoryId];
            long containerId = jo2.optLong("container", -10L);
            Item container = null;
            if(containerId != -10L) {
                try {
                    container = Items.getItem(containerId);
                } catch(NoSuchItemException e) {
                    logger.log(Level.WARNING, "No container exists with the id: " + containerId, e);
                    continue;
                }
                if(container != null && !isInReach(container)) {
                    logger.log(Level.WARNING, "Container not in reach, with the id: " + containerId);
                    continue;
                }
            }
            float minQl = (float) jo2.optDouble("minQl");
            float profit = (float) jo2.optDouble("profit");
            int money = jo2.optInt("money");
            double supplyDemandFactor = 1.0d;//normalizeSupplyDemandFactor(jo2.optDouble("sdf",1.0));
            trades.add(new Trade(this, container, category, minQl, profit, money, supplyDemandFactor));
        }
        updateTrades();
        return trades.size() > 0;
    }

    @Override
    public String toJSONString() {
        StringBuilder json = new StringBuilder();
        json.append("{trades:[");
        int n = 0;
        for(Trade trade : trades) {
            if(n > 0) json.append(',');
            json.append("{category:").append(trade.category.id);
            if(trade.container != null) {
                json.append(",container:").append(trade.container.getWurmId());
            }
            json.append(",minQl:").append(trade.minQl)
                .append(",profit:").append(trade.profit)
                .append(",money:").append(trade.money);
            if(trade.supplyDemandFactor != 1.0)
                json.append(",sdf:").append(trade.supplyDemandFactor);
            json.append('}');
            ++n;
        }
        json.append("]}");
        return json.toString();
    }

    private static boolean isContainer(Item item) {
        switch(item.getTemplateId()) {
            case ItemList.chestLarge:
            case ItemList.larder:
            case ItemList.woodenCoffer:
            case ItemList.stoneCoffin:
            case ItemList.barrelSmall:
            case ItemList.barrelLarge:
                return true;
        }
        return false;
    }

    private boolean isInReach(Item container) {
        Creature npc = servant.npc;
        if(container != null && npc != null && container.isOnSurface() == npc.isOnSurface()) {
            return Creature.getRange(npc, container.getPosX(), container.getPosY()) <= 20.0;
        }
        return false;
    }

    private boolean addItemToDeal(List<Deal> deals, Item item) {
        int ic = getItemCategory(item);
        if(ic != CATEGORY_NONE && ic >= 0 && ic < categoriesIndex.length) {
            Category category = categoriesIndex[ic];
            for(Trade trade : trades) {
                if(addItemToDeal(category, trade, deals, item)) return true;
            }
        }
        return false;
    }

    private boolean addItemToDeal(Category category, Trade trade, List<Deal> deals, Item item) {
        if(category == trade.category || (category != null && trade.category != null && trade.category.id == CATEGORY_ALL)) {
            long price = getSellPrice(category, trade, item);
            if(price > 0L) {
                deals.add(new Deal(category, trade, item, price));
                return true;
            }
        }
        return false;
    }

    private boolean addCreatureToDeal(List<Deal> deals, Creature creature) {
        int ic = getCreatureCategory(creature);
        if(ic != CATEGORY_NONE && ic >= 0 && ic < categoriesIndex.length) {
            Category category = categoriesIndex[ic];
            for(Trade trade : trades) {
                if(addCreatureToDeal(category, trade, deals, creature)) return true;
            }
        }
        return false;
    }

    private boolean addCreatureToDeal(Category category, Trade trade, List<Deal> deals, Creature creature) {
        if(category == trade.category || (category != null && trade.category != null && trade.category.id == CATEGORY_ALL)) {
            long price = getSellPrice(category, trade, creature);
            if(price > 0L) {
                deals.add(new Deal(category, trade, creature, price));
                return true;
            }
        }
        return false;
    }

    private long getSellPrice(Category category, Trade trade, Item item) {
        ItemTemplate template = item.getTemplate();
        int templId = template.getTemplateId();
        final Ware ware = waresIndex.get(templId);
        double x = 0.8d + (((double) (servant.contract.getWurmId() % 1001L) / 1000.0d) * 0.4d);
        double y = 0.8d + (((double) ((servant.contract.getWurmId() ^ (long) templId) % 1001L) / 1000.0d) * 0.4d);
        double price = (Config.vendorBasePrice * category.price * (ware != null? ware.price : 1.0f));
        double weight = (double) item.getWeightGrams() * 0.001d;
        double ql = item.getCurrentQualityLevel();
        double profit = 1.0d / (1.0d + (trade.profit - 1.0d) * x);
        double sdf = getSupplyDemandFactor(trade, price);
        if(template.usesFoodState() || template.isFood() || item.isDrinkable()) {
            final ItemMealData imd = ItemMealData.getItemMealData(item.getWurmId());
            if(imd != null) {
                price *= Config.vendorFoodPrice;
                if(!template.isLiquid() && weight > 1.0d) weight = Math.sqrt(weight);
                double qlMod = (1.0d + Math.sqrt(ql) * 9.9d) * 100.0d;
                double nutrition = Math.sqrt(item.getNutritionLevel());
                float stages = imd.getStages();
                float ingredients = imd.getIngredients();
                double complexity = Math.min(100.0f, (stages / 10.0f) * (ingredients / 30.0f) * 10.0f);
                return (long) Math.ceil(price * weight * qlMod * profit * sdf * y * nutrition * complexity);
            }
        } else if(templId == ItemList.woundCover) {
            double power = item.getAuxData();
            price = price * (power * 0.1d);
        } else if(templId == ItemList.papyrusSheet || templId == ItemList.paperSheet) {
            weight = 1.0;
        }
        if(!template.isLiquid()) weight = Math.sqrt(Math.sqrt(weight));
        double qlMod = ql * 0.1d;
        qlMod = (1.0d + (qlMod * qlMod) * 0.99d) * 100.0d;
        double difficulty = 0.9d + ((double) template.getDifficulty() * 0.005);
        double material = Config.materialValues[item.getMaterial()];
        return (long) Math.ceil(price * weight * qlMod * profit * sdf * y * difficulty * material);
    }

    private long getSellPrice(Category category, Trade trade, Creature creature) {
        if(!creature.isHorse() && !creature.isUnicorn()) return 0L;
        CreatureTemplate template = creature.getTemplate();
        int templId = template.getTemplateId();
        double x = 0.8d + (((double) (servant.contract.getWurmId() % 1001L) / 1000.0d) * 0.4d);
        double y = 0.8d + (((double) ((servant.contract.getWurmId() ^ (long) templId) % 1001L) / 1000.0d) * 0.4d);
        double price = (Config.vendorBasePrice * category.price * 100.0f);
        double profit = 1.0d / (1.0d + (trade.profit - 1.0d) * x);
        double sdf = getSupplyDemandFactor(trade, price);
        int age = creature.getStatus().age;
        if(age < 3) age = 0;
        else if(age < 8) age = 1;
        else if(age < 12) age = 2;
        else if(age < 30) age = 3;
        else if(age < 40) age = 4;
        else age = 5;
        double ageMod = animalAgePrices[age];
        if(template.isBabyCreature()) ageMod *= animalBabyPrice;
        double traitMod = 1.0d;
        for(int i = 0; i < 64; ++i) {
            if(creature.hasTrait(i)) {
                traitMod *= animalTraitPrices[i];
            }
        }
        return (long) Math.ceil(price * ageMod * traitMod * profit * sdf * y);
    }

    private double getSupplyDemandFactor(Trade trade, double price) {
        return (0.7 + (((double) trade.money - price) / (double) trade.category.budget) * 0.6) * trade.supplyDemandFactor;
    }

    static {
        categoriesIndex = new Category[128];
        for(int i = 0; i < categories.length; ++i)
            categoriesIndex[categories[i].id] = categories[i];

        wares = new Ware[]{
                new Ware(ItemList.backPack, CATEGORY_BAGS, "backPack"),
                new Ware(ItemList.satchel, CATEGORY_BAGS, "satchel"),
                new Ware(ItemList.axeSmall, CATEGORY_WEAPON_METAL, "axeSmall"),
                new Ware(ItemList.potion, CATEGORY_NONE, "potion"),
                new Ware(ItemList.hatchet, CATEGORY_TOOL_CARPENTRY, "hatchet"),
                new Ware(ItemList.knifeCarving, CATEGORY_TOOL_CARPENTRY, "knifeCarving"),
                new Ware(ItemList.pickAxe, CATEGORY_TOOL_MISC, "pickAxe"),
                new Ware(ItemList.swordLong, CATEGORY_WEAPON_METAL, "swordLong"),
                new Ware(ItemList.saw, CATEGORY_TOOL_CARPENTRY, "saw"),
                new Ware(ItemList.shovel, CATEGORY_TOOL_FARMING, "shovel"),
                new Ware(ItemList.rake, CATEGORY_TOOL_FARMING, "rake"),
                new Ware(ItemList.hammerMetal, CATEGORY_TOOL_SMITHING, "hammerMetal"),
                new Ware(ItemList.hammerWood, CATEGORY_TOOL_CARPENTRY, "hammerWood"),
                new Ware(ItemList.anvilSmall, CATEGORY_TOOL_SMITHING, "anvilSmall"),
                new Ware(ItemList.cheeseDrill, CATEGORY_TOOL_COOKING, "cheeseDrill"),
                new Ware(ItemList.fryingPan, CATEGORY_TOOL_COOKING, "fryingPan"),
                new Ware(ItemList.jarPottery, CATEGORY_CONTAINER_POTTERY, "jarPottery"),
                new Ware(ItemList.bowlPottery, CATEGORY_CONTAINER_POTTERY, "bowlPottery"),
                new Ware(ItemList.flaskPottery, CATEGORY_CONTAINER_POTTERY, "flaskPottery"),
                new Ware(ItemList.skinWater, CATEGORY_BAGS, "skinWater"),
                new Ware(ItemList.swordShort, CATEGORY_WEAPON_METAL, "swordShort"),
                new Ware(ItemList.swordTwoHander, CATEGORY_WEAPON_METAL, "swordTwoHander"),
                new Ware(ItemList.shieldSmallWood, CATEGORY_SHIELD_WOOD, "shieldSmallWood"),
                new Ware(ItemList.shieldSmallMetal, CATEGORY_SHIELD_METAL, "shieldSmallMetal"),
                new Ware(ItemList.shieldMediumWood, CATEGORY_SHIELD_WOOD, "shieldMediumWood"),
                new Ware(ItemList.shieldLargeWood, CATEGORY_SHIELD_WOOD, "shieldLargeWood"),
                new Ware(ItemList.shieldLargeMetal, CATEGORY_SHIELD_METAL, "shieldLargeMetal"),
                new Ware(ItemList.axeHuge, CATEGORY_WEAPON_METAL, "axeHuge"),
                new Ware(ItemList.axeMedium, CATEGORY_WEAPON_METAL, "axeMedium"),
                new Ware(ItemList.knifeButchering, CATEGORY_TOOL_COOKING, "knifeButchering"),
                new Ware(ItemList.fishingRodIronHook, CATEGORY_TOOL_FISHING, "fishingRodIronHook"),
                new Ware(ItemList.stoneChisel, CATEGORY_TOOL_MASONRY, "stoneChisel"),
                new Ware(ItemList.beltLeather, CATEGORY_CLOTHES, "beltLeather"),
                new Ware(ItemList.leatherGlove, CATEGORY_ARMOUR_LEATHER, "leatherGlove"),
                new Ware(ItemList.leatherJacket, CATEGORY_ARMOUR_LEATHER, "leatherJacket"),
                new Ware(ItemList.leatherBoot, CATEGORY_ARMOUR_LEATHER, "leatherBoot"),
                new Ware(ItemList.leatherSleeve, CATEGORY_ARMOUR_LEATHER, "leatherSleeve"),
                new Ware(ItemList.leatherCap, CATEGORY_ARMOUR_LEATHER, "leatherCap"),
                new Ware(ItemList.leatherHose, CATEGORY_ARMOUR_LEATHER, "leatherHose"),
                new Ware(ItemList.clothGlove, CATEGORY_CLOTHES, "clothGlove"),
                new Ware(ItemList.clothShirt, CATEGORY_CLOTHES, "clothShirt"),
                new Ware(ItemList.clothSleeve, CATEGORY_CLOTHES, "clothSleeve"),
                new Ware(ItemList.clothJacket, CATEGORY_CLOTHES, "clothJacket"),
                new Ware(ItemList.clothHose, CATEGORY_CLOTHES, "clothHose"),
                new Ware(ItemList.clothShoes, CATEGORY_CLOTHES, "clothShoes"),
                new Ware(ItemList.studdedLeatherSleeve, CATEGORY_ARMOUR_LEATHER, "studdedLeatherSleeve"),
                new Ware(ItemList.studdedLeatherBoot, CATEGORY_ARMOUR_LEATHER, "studdedLeatherBoot"),
                new Ware(ItemList.studdedLeatherCap, CATEGORY_ARMOUR_LEATHER, "studdedLeatherCap"),
                new Ware(ItemList.studdedLeatherHose, CATEGORY_ARMOUR_LEATHER, "studdedLeatherHose"),
                new Ware(ItemList.studdedLeatherGlove, CATEGORY_ARMOUR_LEATHER, "studdedLeatherGlove"),
                new Ware(ItemList.studdedLeatherJacket, CATEGORY_ARMOUR_LEATHER, "studdedLeatherJacket"),
                new Ware(ItemList.candle, CATEGORY_LAMPS, "candle"),
                new Ware(ItemList.lantern, CATEGORY_LAMPS, "lantern"),
                new Ware(ItemList.oilLamp, CATEGORY_LAMPS, "oilLamp"),
                new Ware(ItemList.spindle, CATEGORY_TOOL_TAILORING, "spindle"),
                new Ware(ItemList.flintSteel, CATEGORY_TOOL_MISC, "flintSteel"),
                new Ware(ItemList.fishingRodWoodenHook, CATEGORY_TOOL_FISHING, "fishingRodWoodenHook"),
                new Ware(ItemList.breadWheat, CATEGORY_FOOD_BAKED, "breadWheat"),
                new Ware(ItemList.doorLock, CATEGORY_LOCKS, "doorLock"),
                new Ware(ItemList.key, CATEGORY_LOCKS, "key"),
                new Ware(ItemList.pigFood, CATEGORY_FOOD_LIQUID, "pigFood"),
                new Ware(ItemList.stoneOven, CATEGORY_TOOL_COOKING, "stoneOven"),
                new Ware(ItemList.forge, CATEGORY_TOOL_SMITHING, "forge"),
                new Ware(ItemList.chestLarge, CATEGORY_CONTAINER_WOOD, "chestLarge"),
                new Ware(ItemList.anvilLarge, CATEGORY_TOOL_SMITHING, "anvilLarge"),
                new Ware(ItemList.cartSmall, CATEGORY_NONE, "cartSmall"),
                new Ware(ItemList.barrelSmall, CATEGORY_CONTAINER_WOOD, "barrelSmall"),
                new Ware(ItemList.barrelLarge, CATEGORY_CONTAINER_WOOD, "barrelLarge"),
                new Ware(ItemList.chestSmall, CATEGORY_CONTAINER_WOOD, "chestSmall"),
                new Ware(ItemList.padLockSmall, CATEGORY_LOCKS, "padLockSmall"),
                new Ware(ItemList.padLockLarge, CATEGORY_LOCKS, "padLockLarge"),
                new Ware(ItemList.grindstone, CATEGORY_TOOL_COOKING, "grindstone"),
                new Ware(ItemList.bread, CATEGORY_FOOD_BAKED, "bread"),
                new Ware(ItemList.signPointing, CATEGORY_DECORATIONS, "signPointing"),
                new Ware(ItemList.signLarge, CATEGORY_DECORATIONS, "signLarge"),
                new Ware(ItemList.signSmall, CATEGORY_DECORATIONS, "signSmall"),
                new Ware(ItemList.needleIron, CATEGORY_TOOL_TAILORING, "needleIron"),
                new Ware(ItemList.needleCopper, CATEGORY_TOOL_TAILORING, "needleCopper"),
                new Ware(ItemList.pliers, CATEGORY_TOOL_MASONRY, "pliers"),
                new Ware(ItemList.loom, CATEGORY_TOOL_TAILORING, "loom"),
                new Ware(ItemList.statuette, CATEGORY_DECORATIONS, "statuette"),
                new Ware(ItemList.candelabra, CATEGORY_LAMPS, "candelabra"),
                new Ware(ItemList.necklace, CATEGORY_JEWELRY, "necklace"),
                new Ware(ItemList.armring, CATEGORY_JEWELRY, "armring"),
                new Ware(ItemList.pendulum, CATEGORY_JEWELRY, "pendulum"),
                new Ware(ItemList.gateLock, CATEGORY_LOCKS, "gateLock"),
                new Ware(ItemList.spoon, CATEGORY_TOOL_COOKING, "spoon"),
                new Ware(ItemList.knifeFood, CATEGORY_TOOL_COOKING, "knifeFood"),
                new Ware(ItemList.fork, CATEGORY_TOOL_COOKING, "fork"),
                new Ware(ItemList.tableRound, CATEGORY_FURNITURE, "tableRound", 0.2f),
                new Ware(ItemList.stoolRound, CATEGORY_FURNITURE, "stoolRound"),
                new Ware(ItemList.tableSquareSmall, CATEGORY_FURNITURE, "tableSquareSmall", 0.2f),
                new Ware(ItemList.chair, CATEGORY_FURNITURE, "chair"),
                new Ware(ItemList.tableSquareLarge, CATEGORY_FURNITURE, "tableSquareLarge"),
                new Ware(ItemList.armChair, CATEGORY_FURNITURE, "armChair"),
                new Ware(ItemList.sickle, CATEGORY_TOOL_FARMING, "sickle"),
                new Ware(ItemList.scythe, CATEGORY_TOOL_FARMING, "scythe"),
                new Ware(ItemList.yoyo, CATEGORY_TOYS, "yoyo"),
                new Ware(ItemList.gloveSteel, CATEGORY_ARMOUR_CHAIN, "gloveSteel"),
                new Ware(ItemList.chainBoot, CATEGORY_ARMOUR_CHAIN, "chainBoot"),
                new Ware(ItemList.chainHose, CATEGORY_ARMOUR_CHAIN, "chainHose"),
                new Ware(ItemList.chainJacket, CATEGORY_ARMOUR_CHAIN, "chainJacket"),
                new Ware(ItemList.chainSleeve, CATEGORY_ARMOUR_CHAIN, "chainSleeve"),
                new Ware(ItemList.chainGlove, CATEGORY_ARMOUR_CHAIN, "chainGlove"),
                new Ware(ItemList.chainCoif, CATEGORY_ARMOUR_CHAIN, "chainCoif"),
                new Ware(ItemList.plateBoot, CATEGORY_ARMOUR_PLATE, "plateBoot"),
                new Ware(ItemList.plateHose, CATEGORY_ARMOUR_PLATE, "plateHose"),
                new Ware(ItemList.plateJacket, CATEGORY_ARMOUR_PLATE, "plateJacket"),
                new Ware(ItemList.plateSleeve, CATEGORY_ARMOUR_PLATE, "plateSleeve"),
                new Ware(ItemList.plateGauntlet, CATEGORY_ARMOUR_PLATE, "plateGauntlet"),
                new Ware(ItemList.helmetBasinet, CATEGORY_ARMOUR_PLATE, "helmetBasinet"),
                new Ware(ItemList.helmetGreat, CATEGORY_ARMOUR_PLATE, "helmetGreat"),
                new Ware(ItemList.helmetOpen, CATEGORY_ARMOUR_PLATE, "helmetOpen"),
                new Ware(ItemList.raftSmall, CATEGORY_NONE, "raftSmall"),
                new Ware(ItemList.maulLarge, CATEGORY_WEAPON_METAL, "maulLarge"),
                new Ware(ItemList.maulSmall, CATEGORY_WEAPON_METAL, "maulSmall"),
                new Ware(ItemList.maulMedium, CATEGORY_WEAPON_METAL, "maulMedium"),
                new Ware(ItemList.whetStone, CATEGORY_TOOL_SMITHING, "whetStone", 0.1f),
                new Ware(ItemList.ring, CATEGORY_JEWELRY, "ring"),
                new Ware(ItemList.pelt, CATEGORY_TOOL_CARPENTRY, "pelt"),
                new Ware(ItemList.ropeTool, CATEGORY_TOOL_MISC, "ropeTool"),
                new Ware(ItemList.practiceDoll, CATEGORY_WEAPON_WOOD, "practiceDoll"),
                new Ware(ItemList.altarWood, CATEGORY_NONE, "altarWood"),
                new Ware(ItemList.altarStone, CATEGORY_NONE, "altarStone"),
                new Ware(ItemList.altarGold, CATEGORY_NONE, "altarGold"),
                new Ware(ItemList.altarSilver, CATEGORY_NONE, "altarSilver"),
                new Ware(ItemList.metalBowl, CATEGORY_NONE, "metalBowl"),
                new Ware(ItemList.keyCopy, CATEGORY_LOCKS, "keyCopy"),
                new Ware(ItemList.keyMold, CATEGORY_LOCKS, "keyMold"),
                new Ware(ItemList.keyForm, CATEGORY_LOCKS, "keyForm"),
                new Ware(ItemList.tempmarker, CATEGORY_DECORATIONS, "tempmarker"),
                new Ware(ItemList.stew, CATEGORY_FOOD_LIQUID, "stew"),
                new Ware(ItemList.casserole, CATEGORY_FOOD_DISH, "casserole"),
                new Ware(ItemList.gulasch, CATEGORY_FOOD_LIQUID, "gulasch"),
                new Ware(ItemList.saucePan, CATEGORY_TOOL_COOKING, "saucePan"),
                new Ware(ItemList.cauldron, CATEGORY_TOOL_COOKING, "cauldron"),
                new Ware(ItemList.soup, CATEGORY_FOOD_LIQUID, "soup"),
                new Ware(ItemList.diamondCrazy, CATEGORY_GEMS, "diamondCrazy"),
                new Ware(ItemList.porridge, CATEGORY_FOOD_LIQUID, "porridge"),
                new Ware(ItemList.emerald, CATEGORY_GEMS, "emerald"),
                new Ware(ItemList.emeraldStar, CATEGORY_GEMS, "emeraldStar"),
                new Ware(ItemList.ruby, CATEGORY_GEMS, "ruby"),
                new Ware(ItemList.rubyStar, CATEGORY_GEMS, "rubyStar"),
                new Ware(ItemList.opal, CATEGORY_GEMS, "opal"),
                new Ware(ItemList.opalBlack, CATEGORY_GEMS, "opalBlack"),
                new Ware(ItemList.diamond, CATEGORY_GEMS, "diamond"),
                new Ware(ItemList.diamondStar, CATEGORY_GEMS, "diamondStar"),
                new Ware(ItemList.sapphire, CATEGORY_GEMS, "sapphire"),
                new Ware(ItemList.sapphireStar, CATEGORY_GEMS, "sapphireStar"),
                new Ware(ItemList.file, CATEGORY_TOOL_CARPENTRY, "file"),
                new Ware(ItemList.awl, CATEGORY_TOOL_TAILORING, "awl"),
                new Ware(ItemList.leatherKnife, CATEGORY_TOOL_TAILORING, "leatherKnife"),
                new Ware(ItemList.scissors, CATEGORY_TOOL_TAILORING, "scissors"),
                new Ware(ItemList.clayShaper, CATEGORY_TOOL_MISC, "clayShaper"),
                new Ware(ItemList.spatula, CATEGORY_TOOL_MISC, "spatula"),
                new Ware(ItemList.statueNymph, CATEGORY_DECORATIONS, "statueNymph", 0.2f),
                new Ware(ItemList.statueDemon, CATEGORY_DECORATIONS, "statueDemon", 0.2f),
                new Ware(ItemList.statueDog, CATEGORY_DECORATIONS, "statueDog", 0.2f),
                new Ware(ItemList.statueTroll, CATEGORY_DECORATIONS, "statueTroll", 0.2f),
                new Ware(ItemList.statueBoy, CATEGORY_DECORATIONS, "statueBoy", 0.2f),
                new Ware(ItemList.statueGirl, CATEGORY_DECORATIONS, "statueGirl", 0.2f),
                new Ware(ItemList.stoneBench, CATEGORY_FURNITURE, "stoneBench", 0.2f),
                new Ware(ItemList.stoneFountainDrink, CATEGORY_NONE, "stoneFountainDrink"),
                new Ware(ItemList.stoneCoffin, CATEGORY_NONE, "stoneCoffin"),
                new Ware(ItemList.stoneFountain, CATEGORY_NONE, "stoneFountain"),
                new Ware(ItemList.fruitpress, CATEGORY_TOOL_COOKING, "fruitpress"),
                new Ware(ItemList.fruitJuice, CATEGORY_BEVERAGE_DRINK, "fruitJuice"),
                new Ware(ItemList.wineRed, CATEGORY_BEVERAGE_ALCOHOL, "wineRed"),
                new Ware(ItemList.wineWhite, CATEGORY_BEVERAGE_ALCOHOL, "wineWhite"),
                new Ware(ItemList.bucketSmall, CATEGORY_CONTAINER_WOOD, "bucketSmall"),
                new Ware(ItemList.teaGreen, CATEGORY_BEVERAGE_DRINK, "teaGreen"),
                new Ware(ItemList.lemonade, CATEGORY_BEVERAGE_DRINK, "lemonade"),
                new Ware(ItemList.jam, CATEGORY_FOOD_LIQUID, "jam"),
                new Ware(ItemList.metalBrush, CATEGORY_TOOL_MISC, "metalBrush"),
                new Ware(ItemList.catapult, CATEGORY_NONE, "catapult"),
                new Ware(ItemList.bowShort, CATEGORY_WEAPON_RANGED, "bowShort"),
                new Ware(ItemList.bowMedium, CATEGORY_WEAPON_RANGED, "bowMedium"),
                new Ware(ItemList.bowLong, CATEGORY_WEAPON_RANGED, "bowLong"),
                new Ware(ItemList.bowComposite, CATEGORY_WEAPON_RANGED, "bowComposite"),
                new Ware(ItemList.archeryTarget, CATEGORY_WEAPON_RANGED, "archeryTarget", 0.2f),
                new Ware(ItemList.quiver, CATEGORY_WEAPON_RANGED, "quiver"),
                new Ware(ItemList.lockpick, CATEGORY_LOCKS, "lockpick"),
                new Ware(ItemList.dragonLeatherSleeve, CATEGORY_ARMOUR_LEATHER, "dragonLeatherSleeve"),
                new Ware(ItemList.dragonLeatherBoot, CATEGORY_ARMOUR_LEATHER, "dragonLeatherBoot"),
                new Ware(ItemList.dragonLeatherCap, CATEGORY_ARMOUR_LEATHER, "dragonLeatherCap"),
                new Ware(ItemList.dragonLeatherHose, CATEGORY_ARMOUR_LEATHER, "dragonLeatherHose"),
                new Ware(ItemList.dragonLeatherGlove, CATEGORY_ARMOUR_LEATHER, "dragonLeatherGlove"),
                new Ware(ItemList.dragonLeatherJacket, CATEGORY_ARMOUR_LEATHER, "dragonLeatherJacket"),
                new Ware(ItemList.dragonScaleBoot, CATEGORY_ARMOUR_LEATHER, "dragonScaleBoot"),
                new Ware(ItemList.dragonScaleHose, CATEGORY_ARMOUR_LEATHER, "dragonScaleHose"),
                new Ware(ItemList.dragonScaleJacket, CATEGORY_ARMOUR_LEATHER, "dragonScaleJacket"),
                new Ware(ItemList.dragonScaleSleeve, CATEGORY_ARMOUR_LEATHER, "dragonScaleSleeve"),
                new Ware(ItemList.dragonScaleGauntlet, CATEGORY_ARMOUR_LEATHER, "dragonScaleGauntlet"),
                new Ware(ItemList.compass, CATEGORY_TOOL_MISC, "compass"),
                new Ware(ItemList.woundCover, CATEGORY_HEALING, "woundCover"),
                new Ware(ItemList.bedStandard, CATEGORY_FURNITURE, "bedStandard"),
                new Ware(ItemList.flag, CATEGORY_DECORATIONS, "flag"),
                new Ware(ItemList.sandwich, CATEGORY_FOOD_BAKED, "sandwich"),
                new Ware(ItemList.spyglass, CATEGORY_NONE, "spyglass"),
                new Ware(ItemList.boatRowing, CATEGORY_NONE, "boatRowing"),
                new Ware(ItemList.boatSailing, CATEGORY_NONE, "boatSailing"),
                new Ware(ItemList.trowel, CATEGORY_TOOL_MASONRY, "trowel"),
                new Ware(ItemList.streetLamp, CATEGORY_LAMPS, "streetLamp"),
                new Ware(ItemList.statuetteFo, CATEGORY_DECORATIONS, "statuetteFo"),
                new Ware(ItemList.statuetteLibila, CATEGORY_DECORATIONS, "statuetteLibila"),
                new Ware(ItemList.statuetteMagranon, CATEGORY_DECORATIONS, "statuetteMagranon"),
                new Ware(ItemList.statuetteVynora, CATEGORY_DECORATIONS, "statuetteVynora"),
                new Ware(ItemList.mailboxWood, CATEGORY_NONE, "mailboxWood"),
                new Ware(ItemList.mailboxStone, CATEGORY_NONE, "mailboxStone"),
                new Ware(ItemList.mailboxWoodTwo, CATEGORY_NONE, "mailboxWoodTwo"),
                new Ware(ItemList.mailboxStoneTwo, CATEGORY_NONE, "mailboxStoneTwo"),
                new Ware(ItemList.whipOne, CATEGORY_WEAPON_RANGED, "whipOne"),
                new Ware(ItemList.toolbelt, CATEGORY_TOOL_TAILORING, "toolbelt"),
                new Ware(ItemList.colossus, CATEGORY_NONE, "colossus"),
                new Ware(ItemList.pumpkinHalloween, CATEGORY_DECORATIONS, "pumpkinHalloween"),
                new Ware(ItemList.cartLarge, CATEGORY_NONE, "cartLarge"),
                new Ware(ItemList.cog, CATEGORY_NONE, "cog"),
                new Ware(ItemList.corbita, CATEGORY_NONE, "corbita"),
                new Ware(ItemList.knarr, CATEGORY_NONE, "knarr"),
                new Ware(ItemList.caravel, CATEGORY_NONE, "caravel"),
                new Ware(ItemList.anchor, CATEGORY_NONE, "anchor"),
                new Ware(ItemList.boatLock, CATEGORY_LOCKS, "boatLock"),
                new Ware(ItemList.barrelHuge, CATEGORY_CONTAINER_WOOD, "barrelHuge"),
                new Ware(ItemList.banner, CATEGORY_DECORATIONS, "banner"),
                new Ware(ItemList.bannerKingdom, CATEGORY_DECORATIONS, "bannerKingdom"),
                new Ware(ItemList.flagKingdom, CATEGORY_DECORATIONS, "flagKingdom"),
                new Ware(ItemList.marketStall, CATEGORY_NONE, "marketStall"),
                new Ware(ItemList.dredge, CATEGORY_TOOL_MISC, "dredge"),
                new Ware(ItemList.mineDoorPlanks, CATEGORY_NONE, "mineDoorPlanks"),
                new Ware(ItemList.mineDoorStone, CATEGORY_NONE, "mineDoorStone"),
                new Ware(ItemList.mineDoorGold, CATEGORY_NONE, "mineDoorGold"),
                new Ware(ItemList.mineDoorSilver, CATEGORY_NONE, "mineDoorSilver"),
                new Ware(ItemList.mineDoorSteel, CATEGORY_NONE, "mineDoorSteel"),
                new Ware(ItemList.stoneWell, CATEGORY_NONE, "stoneWell", 0.2f),
                new Ware(ItemList.trapSticks, CATEGORY_WEAPON_WOOD, "trapSticks"),
                new Ware(ItemList.trapPole, CATEGORY_WEAPON_WOOD, "trapPole"),
                new Ware(ItemList.trapCorrosion, CATEGORY_WEAPON_WOOD, "trapCorrosion"),
                new Ware(ItemList.trapAxe, CATEGORY_WEAPON_WOOD, "trapAxe"),
                new Ware(ItemList.trapKnife, CATEGORY_WEAPON_WOOD, "trapKnife"),
                new Ware(ItemList.trapNet, CATEGORY_WEAPON_WOOD, "trapNet"),
                new Ware(ItemList.trapScythe, CATEGORY_WEAPON_WOOD, "trapScythe"),
                new Ware(ItemList.trapMan, CATEGORY_WEAPON_WOOD, "trapMan"),
                new Ware(ItemList.trapBow, CATEGORY_WEAPON_WOOD, "trapBow"),
                new Ware(ItemList.trapRope, CATEGORY_WEAPON_WOOD, "trapRope"),
                new Ware(ItemList.saddle, CATEGORY_TOOL_FARMING, "saddle"),
                new Ware(ItemList.saddleLarge, CATEGORY_TOOL_FARMING, "saddleLarge"),
                new Ware(ItemList.horseShoe, CATEGORY_TOOL_FARMING, "horseShoe"),
                new Ware(ItemList.bridle, CATEGORY_TOOL_FARMING, "bridle"),
                new Ware(ItemList.stoneFountain2, CATEGORY_NONE, "stoneFountain2"),
                new Ware(ItemList.meditationRugOne, CATEGORY_DECORATIONS, "meditationRugOne"),
                new Ware(ItemList.puppetFo, CATEGORY_TOYS, "puppetFo"),
                new Ware(ItemList.puppetMagranon, CATEGORY_TOYS, "puppetMagranon"),
                new Ware(ItemList.puppetVynora, CATEGORY_TOYS, "puppetVynora"),
                new Ware(ItemList.puppetLibila, CATEGORY_TOYS, "puppetLibila"),
                new Ware(ItemList.meditationRugTwo, CATEGORY_DECORATIONS, "meditationRugTwo"),
                new Ware(ItemList.meditationRugThree, CATEGORY_DECORATIONS, "meditationRugThree"),
                new Ware(ItemList.meditationRugFour, CATEGORY_DECORATIONS, "meditationRugFour"),
                new Ware(ItemList.groomingBrush, CATEGORY_TOOL_FARMING, "groomingBrush"),
                new Ware(ItemList.farmersSalve, CATEGORY_HEALING, "farmersSalve"),
                new Ware(ItemList.christmasTree, CATEGORY_DECORATIONS, "christmasTree"),
                new Ware(ItemList.flaskGlass, CATEGORY_NONE, "flaskGlass"),
                new Ware(ItemList.potionTransmutation, CATEGORY_NONE, "potionTransmutation"),
                new Ware(ItemList.snowman, CATEGORY_DECORATIONS, "snowman"),
                new Ware(ItemList.signShop, CATEGORY_DECORATIONS, "signShop"),
                new Ware(ItemList.streetLampTorch, CATEGORY_LAMPS, "streetLampTorch"),
                new Ware(ItemList.streetLampHanging, CATEGORY_LAMPS, "streetLampHanging"),
                new Ware(ItemList.streetLampImperial, CATEGORY_LAMPS, "streetLampImperial"),
                new Ware(ItemList.hopper, CATEGORY_CONTAINER_WOOD, "hopper"),
                new Ware(ItemList.bulkContainer, CATEGORY_CONTAINER_WOOD, "bulkContainer"),
                new Ware(ItemList.chestNoDecayLarge, CATEGORY_CONTAINER_METAL, "chestNoDecayLarge"),
                new Ware(ItemList.chestNoDecaySmall, CATEGORY_CONTAINER_METAL, "chestNoDecaySmall"),
                new Ware(ItemList.trashBin, CATEGORY_CONTAINER_WOOD, "trashBin"),
                new Ware(ItemList.buildMarker, CATEGORY_DECORATIONS, "buildMarker"),
                new Ware(ItemList.brandingIron, CATEGORY_TOOL_FARMING, "brandingIron"),
                new Ware(ItemList.leatherBarding, CATEGORY_ARMOUR_LEATHER, "leatherBarding"),
                new Ware(ItemList.chainBarding, CATEGORY_ARMOUR_CHAIN, "chainBarding"),
                new Ware(ItemList.clothBarding, CATEGORY_CLOTHES, "clothBarding"),
                new Ware(ItemList.spearLong, CATEGORY_WEAPON_WOOD, "spearLong"),
                new Ware(ItemList.halberd, CATEGORY_WEAPON_METAL, "halberd"),
                new Ware(ItemList.spearSteel, CATEGORY_WEAPON_METAL, "spearSteel"),
                new Ware(ItemList.staffSteel, CATEGORY_WEAPON_METAL, "staffSteel"),
                new Ware(ItemList.staffWood, CATEGORY_WEAPON_WOOD, "staffWood"),
                new Ware(ItemList.pillar, CATEGORY_DECORATIONS, "pillar", 0.2f),
                new Ware(ItemList.bellTower, CATEGORY_DECORATIONS, "bellTower", 0.1f),
                new Ware(ItemList.weaponsRack, CATEGORY_CONTAINER_WOOD, "weaponsRack", 0.2f),
                new Ware(ItemList.weaponsRackPolearms, CATEGORY_CONTAINER_WOOD, "weaponsRackPolearms", 0.2f),
                new Ware(ItemList.cake, CATEGORY_FOOD_CANDY, "cake"),
                new Ware(ItemList.cakeSlice, CATEGORY_FOOD_CANDY, "cakeSlice"),
                new Ware(ItemList.gardenGnome, CATEGORY_DECORATIONS, "gardenGnome"),
                new Ware(ItemList.medallionHota, CATEGORY_JEWELRY, "medallionHota"),
                new Ware(ItemList.statueHota, CATEGORY_DECORATIONS, "statueHota", 0.2f),
                new Ware(ItemList.papyrusPress, CATEGORY_TOOL_MISC, "papyrusPress"),
                new Ware(ItemList.papyrusSheet, CATEGORY_PAPER_REPORTS, "papyrusSheet", 4.0f),
                new Ware(ItemList.reedPen, CATEGORY_TOOL_MISC, "reedPen"),
                new Ware(ItemList.oilBarrel, CATEGORY_CONTAINER_WOOD, "oilBarrel"),
                new Ware(ItemList.weaponsRackBows, CATEGORY_CONTAINER_WOOD, "weaponsRackBows"),
                new Ware(ItemList.armourStand, CATEGORY_CONTAINER_WOOD, "armourStand"),
                new Ware(ItemList.wineBarrelSmall, CATEGORY_CONTAINER_WOOD, "wineBarrelSmall"),
                new Ware(ItemList.thatchingTool, CATEGORY_TOOL_FARMING, "thatchingTool"),
                new Ware(ItemList.clothHood, CATEGORY_CLOTHES, "clothHood"),
                new Ware(ItemList.handMirror, CATEGORY_NONE, "handMirror"),
                new Ware(ItemList.santaHat, CATEGORY_CLOTHES, "santaHat"),
                new Ware(ItemList.sacrificialKnife, CATEGORY_TOOL_MISC, "sacrificialKnife"),
                new Ware(ItemList.statueHorse, CATEGORY_DECORATIONS, "statueHorse", 0.1f),
                new Ware(ItemList.flowerpotPottery, CATEGORY_DECORATIONS, "flowerpotPottery"),
                new Ware(ItemList.flowerpotYellow, CATEGORY_DECORATIONS, "flowerpotYellow"),
                new Ware(ItemList.flowerpotBlue, CATEGORY_DECORATIONS, "flowerpotBlue"),
                new Ware(ItemList.flowerpotPurple, CATEGORY_DECORATIONS, "flowerpotPurple"),
                new Ware(ItemList.flowerpotWhite, CATEGORY_DECORATIONS, "flowerpotWhite"),
                new Ware(ItemList.flowerpotOrange, CATEGORY_DECORATIONS, "flowerpotOrange"),
                new Ware(ItemList.flowerpotGreenish, CATEGORY_DECORATIONS, "flowerpotGreenish"),
                new Ware(ItemList.flowerpotWhiteDotted, CATEGORY_DECORATIONS, "flowerpotWhiteDotted"),
                new Ware(ItemList.gravestone, CATEGORY_DECORATIONS, "gravestone", 0.2f),
                new Ware(ItemList.staffSapphire, CATEGORY_NONE, "staffSapphire"),
                new Ware(ItemList.staffRuby, CATEGORY_NONE, "staffRuby"),
                new Ware(ItemList.staffDiamond, CATEGORY_NONE, "staffDiamond"),
                new Ware(ItemList.staffOpal, CATEGORY_NONE, "staffOpal"),
                new Ware(ItemList.staffEmerald, CATEGORY_NONE, "staffEmerald"),
                new Ware(ItemList.tabard, CATEGORY_CLOTHES, "tabard"),
                new Ware(ItemList.potionIllusion, CATEGORY_NONE, "potionIllusion"),
                new Ware(ItemList.villageBoard, CATEGORY_DECORATIONS, "villageBoard"),
                new Ware(ItemList.potionAffinity, CATEGORY_NONE, "potionAffinity"),
                new Ware(ItemList.copperBrazier, CATEGORY_LAMPS, "copperBrazier"),
                new Ware(ItemList.marbleBrazierPillar, CATEGORY_LAMPS, "marbleBrazierPillar"),
                new Ware(ItemList.snowLantern, CATEGORY_LAMPS, "snowLantern"),
                new Ware(ItemList.blackBearRug, CATEGORY_FURNITURE, "blackBearRug"),
                new Ware(ItemList.brownBearRug, CATEGORY_FURNITURE, "brownBearRug"),
                new Ware(ItemList.mountainLionRug, CATEGORY_FURNITURE, "mountainLionRug"),
                new Ware(ItemList.blackWolfRug, CATEGORY_FURNITURE, "blackWolfRug"),
                new Ware(ItemList.wagon, CATEGORY_NONE, "wagon"),
                new Ware(ItemList.crateSmall, CATEGORY_CONTAINER_WOOD, "crateSmall"),
                new Ware(ItemList.crateLarge, CATEGORY_CONTAINER_WOOD, "crateLarge"),
                new Ware(ItemList.shipCarrier, CATEGORY_NONE, "shipCarrier"),
                new Ware(ItemList.ricePorridge, CATEGORY_FOOD_LIQUID, "ricePorridge"),
                new Ware(ItemList.risotto, CATEGORY_FOOD_DISH, "risotto"),
                new Ware(ItemList.wineRice, CATEGORY_BEVERAGE_ALCOHOL, "wineRice"),
                new Ware(ItemList.tent, CATEGORY_NONE, "tent"),
                new Ware(ItemList.tentExploration, CATEGORY_NONE, "tentExploration"),
                new Ware(ItemList.tentMilitary, CATEGORY_NONE, "tentMilitary"),
                new Ware(ItemList.pavilion, CATEGORY_NONE, "pavilion"),
                new Ware(ItemList.colossusOfVynora, CATEGORY_NONE, "colossusOfVynora"),
                new Ware(ItemList.colossusOfMagranon, CATEGORY_NONE, "colossusOfMagranon"),
                new Ware(ItemList.potionWeaponSmithing, CATEGORY_NONE, "potionWeaponSmithing"),
                new Ware(ItemList.potionRopemaking, CATEGORY_NONE, "potionRopemaking"),
                new Ware(ItemList.potionWaterwalking, CATEGORY_NONE, "potionWaterwalking"),
                new Ware(ItemList.potionMining, CATEGORY_NONE, "potionMining"),
                new Ware(ItemList.potionTailoring, CATEGORY_NONE, "potionTailoring"),
                new Ware(ItemList.potionArmourSmithing, CATEGORY_NONE, "potionArmourSmithing"),
                new Ware(ItemList.potionFletching, CATEGORY_NONE, "potionFletching"),
                new Ware(ItemList.potionBlacksmithing, CATEGORY_NONE, "potionBlacksmithing"),
                new Ware(ItemList.potionLeatherworking, CATEGORY_NONE, "potionLeatherworking"),
                new Ware(ItemList.potionShipbuilding, CATEGORY_NONE, "potionShipbuilding"),
                new Ware(ItemList.potionStonecutting, CATEGORY_NONE, "potionStonecutting"),
                new Ware(ItemList.potionMasonry, CATEGORY_NONE, "potionMasonry"),
                new Ware(ItemList.potionWoodcutting, CATEGORY_NONE, "potionWoodcutting"),
                new Ware(ItemList.potionCarpentry, CATEGORY_NONE, "potionCarpentry"),
                new Ware(ItemList.woodenBedsideTable, CATEGORY_FURNITURE, "woodenBedsideTable"),
                new Ware(ItemList.potionAcidDamage, CATEGORY_NONE, "potionAcidDamage"),
                new Ware(ItemList.potionFireDamage, CATEGORY_NONE, "potionFireDamage"),
                new Ware(ItemList.potionFrostDamage, CATEGORY_NONE, "potionFrostDamage"),
                new Ware(ItemList.openFireplace, CATEGORY_FURNITURE, "openFireplace"),
                new Ware(ItemList.canopyBed, CATEGORY_FURNITURE, "canopyBed"),
                new Ware(ItemList.woodenBench, CATEGORY_FURNITURE, "woodenBench", 0.5f),
                new Ware(ItemList.wardrobe, CATEGORY_FURNITURE, "wardrobe"),
                new Ware(ItemList.woodenCoffer, CATEGORY_FURNITURE, "woodenCoffer", 0.3f),
                new Ware(ItemList.royalThrone, CATEGORY_FURNITURE, "royalThrone"),
                new Ware(ItemList.washingBowl, CATEGORY_FURNITURE, "washingBowl"),
                new Ware(ItemList.tripodTableSmall, CATEGORY_FURNITURE, "tripodTableSmall"),
                new Ware(ItemList.turtleShield, CATEGORY_SHIELD_WOOD, "turtleShield"),
                new Ware(ItemList.rangePole, CATEGORY_TOOL_MASONRY, "rangePole"),
                new Ware(ItemList.dioptra, CATEGORY_TOOL_MASONRY, "dioptra"),
                new Ware(ItemList.colossusOfFo, CATEGORY_NONE, "colossusOfFo"),
                new Ware(ItemList.smallCarpet, CATEGORY_FURNITURE, "smallCarpet"),
                new Ware(ItemList.mediumCarpet, CATEGORY_FURNITURE, "mediumCarpet"),
                new Ware(ItemList.largeCarpet, CATEGORY_FURNITURE, "largeCarpet"),
                new Ware(ItemList.highBookshelf, CATEGORY_FURNITURE, "highBookshelf", 0.3f),
                new Ware(ItemList.lowBookshelf, CATEGORY_FURNITURE, "lowBookshelf", 0.3f),
                new Ware(ItemList.highChair1, CATEGORY_FURNITURE, "highChair1"),
                new Ware(ItemList.highChair2, CATEGORY_FURNITURE, "highChair2"),
                new Ware(ItemList.highChair3, CATEGORY_FURNITURE, "highChair3"),
                new Ware(ItemList.colossusOfLibila, CATEGORY_NONE, "colossusOfLibila"),
                new Ware(ItemList.spinningWheel, CATEGORY_TOOL_TAILORING, "spinningWheel"),
                new Ware(ItemList.loungeChair, CATEGORY_FURNITURE, "loungeChair"),
                new Ware(ItemList.royalLoungeChaise, CATEGORY_FURNITURE, "royalLoungeChaise"),
                new Ware(ItemList.cupboard, CATEGORY_FURNITURE, "cupboard", 0.5f),
                new Ware(ItemList.roundMarbleTable, CATEGORY_FURNITURE, "roundMarbleTable", 0.2f),
                new Ware(ItemList.rectMarbleTable, CATEGORY_FURNITURE, "rectMarbleTable", 0.2f),
                new Ware(ItemList.siegeShield, CATEGORY_SHIELD_WOOD, "siegeShield", 0.1f),
                new Ware(ItemList.pewpewdie, CATEGORY_NONE, "pewpewdie", 0.1f),
                new Ware(ItemList.ballista, CATEGORY_NONE, "ballista"),
                new Ware(ItemList.trebuchet, CATEGORY_NONE, "trebuchet"),
                new Ware(ItemList.barrier, CATEGORY_NONE, "barrier"),
                new Ware(ItemList.pewpewdieAcid, CATEGORY_NONE, "pewpewdieAcid", 0.1f),
                new Ware(ItemList.pewpewdieFire, CATEGORY_NONE, "pewpewdieFire", 0.1f),
                new Ware(ItemList.pewpewdieLightning, CATEGORY_NONE, "pewpewdieLightning", 0.1f),
                new Ware(ItemList.woolCap, CATEGORY_CLOTHES, "woolCap"),
                new Ware(ItemList.woolCapYellow, CATEGORY_CLOTHES, "woolCapYellow"),
                new Ware(ItemList.woolCapGreen, CATEGORY_CLOTHES, "woolCapGreen"),
                new Ware(ItemList.woolCapRed, CATEGORY_CLOTHES, "woolCapRed"),
                new Ware(ItemList.woolCapBlue, CATEGORY_CLOTHES, "woolCapBlue"),
                new Ware(ItemList.commonWoolHat, CATEGORY_CLOTHES, "commonWoolHat"),
                new Ware(ItemList.commonWoolHatDark, CATEGORY_CLOTHES, "commonWoolHatDark"),
                new Ware(ItemList.commonWoolHatBrown, CATEGORY_CLOTHES, "commonWoolHatBrown"),
                new Ware(ItemList.commonWoolHatGreen, CATEGORY_CLOTHES, "commonWoolHatGreen"),
                new Ware(ItemList.commonWoolHatRed, CATEGORY_CLOTHES, "commonWoolHatRed"),
                new Ware(ItemList.commonWoolHatBlue, CATEGORY_CLOTHES, "commonWoolHatBlue"),
                new Ware(ItemList.forestersWoolHat, CATEGORY_CLOTHES, "forestersWoolHat"),
                new Ware(ItemList.forestersWoolHatGreen, CATEGORY_CLOTHES, "forestersWoolHatGreen"),
                new Ware(ItemList.forestersWoolHatDark, CATEGORY_CLOTHES, "forestersWoolHatDark"),
                new Ware(ItemList.forestersWoolHatBlue, CATEGORY_CLOTHES, "forestersWoolHatBlue"),
                new Ware(ItemList.forestersWoolHatRed, CATEGORY_CLOTHES, "forestersWoolHatRed"),
                new Ware(ItemList.brownBearHelm, CATEGORY_ARMOUR_LEATHER, "brownBearHelm"),
                new Ware(ItemList.leatherHat0, CATEGORY_CLOTHES, "leatherHat0"),
                new Ware(ItemList.squireWoolCap, CATEGORY_CLOTHES, "squireWoolCap"),
                new Ware(ItemList.squireWoolCapGreen, CATEGORY_CLOTHES, "squireWoolCapGreen"),
                new Ware(ItemList.squireWoolCapBlue, CATEGORY_CLOTHES, "squireWoolCapBlue"),
                new Ware(ItemList.squireWoolCapBlack, CATEGORY_CLOTHES, "squireWoolCapBlack"),
                new Ware(ItemList.squireWoolCapRed, CATEGORY_CLOTHES, "squireWoolCapRed"),
                new Ware(ItemList.squireWoolCapYellow, CATEGORY_CLOTHES, "squireWoolCapYellow"),
                new Ware(ItemList.gardenGnomeGreen, CATEGORY_DECORATIONS, "gardenGnomeGreen"),
                new Ware(ItemList.pewpewdieIce, CATEGORY_NONE, "pewpewdieIce", 0.1f),
                new Ware(ItemList.yuleGoat, CATEGORY_DECORATIONS, "yuleGoat"),
                new Ware(ItemList.maskEnlightended, CATEGORY_CLOTHES, "maskEnlightended"),
                new Ware(ItemList.maskRavager, CATEGORY_CLOTHES, "maskRavager"),
                new Ware(ItemList.maskPale, CATEGORY_CLOTHES, "maskPale"),
                new Ware(ItemList.maskShadow, CATEGORY_CLOTHES, "maskShadow"),
                new Ware(ItemList.maskChallenge, CATEGORY_CLOTHES, "maskChallenge"),
                new Ware(ItemList.maskIsles, CATEGORY_CLOTHES, "maskIsles"),
                new Ware(ItemList.goldGreatHelmHorned, CATEGORY_ARMOUR_PLATE, "goldGreatHelmHorned"),
                new Ware(ItemList.openPlumedHelm, CATEGORY_ARMOUR_PLATE, "openPlumedHelm"),
                new Ware(ItemList.goldChallengeStatue, CATEGORY_DECORATIONS, "goldChallengeStatue", 0.2f),
                new Ware(ItemList.silverChallengeStatue, CATEGORY_DECORATIONS, "silverChallengeStatue", 0.2f),
                new Ware(ItemList.bronzeChallengeStatue, CATEGORY_DECORATIONS, "bronzeChallengeStatue", 0.2f),
                new Ware(ItemList.marbleChallengeStatue, CATEGORY_DECORATIONS, "marbleChallengeStatue", 0.2f),
                new Ware(ItemList.hotaNecklace, CATEGORY_JEWELRY, "hotaNecklace"),
                new Ware(ItemList.staffOfLand, CATEGORY_NONE, "staffOfLand"),
                new Ware(ItemList.tapestryStand, CATEGORY_DECORATIONS, "tapestryStand"),
                new Ware(ItemList.tapestryPattern1, CATEGORY_DECORATIONS, "tapestryPattern1"),
                new Ware(ItemList.tapestryPattern2, CATEGORY_DECORATIONS, "tapestryPattern2"),
                new Ware(ItemList.tapestryPattern3, CATEGORY_DECORATIONS, "tapestryPattern3"),
                new Ware(ItemList.tapestryMotifCavalry, CATEGORY_DECORATIONS, "tapestryMotifCavalry"),
                new Ware(ItemList.tapestryMotifFestivities, CATEGORY_DECORATIONS, "tapestryMotifFestivities"),
                new Ware(ItemList.tapestryMotifBattleKyara, CATEGORY_DECORATIONS, "tapestryMotifBattleKyara"),
                new Ware(ItemList.tapestryFaeldray, CATEGORY_DECORATIONS, "tapestryFaeldray"),
                new Ware(ItemList.valentines, CATEGORY_DECORATIONS, "valentines"),
                new Ware(ItemList.helmetCavalier, CATEGORY_ARMOUR_PLATE, "helmetCavalier"),
                new Ware(ItemList.tallKingdomBanner, CATEGORY_DECORATIONS, "tallKingdomBanner"),
                new Ware(ItemList.marblePlanter, CATEGORY_DECORATIONS, "marblePlanter"),
                new Ware(ItemList.marblePlanterYellow, CATEGORY_DECORATIONS, "marblePlanterYellow"),
                new Ware(ItemList.marblePlanterBlue, CATEGORY_DECORATIONS, "marblePlanterBlue"),
                new Ware(ItemList.marblePlanterPurple, CATEGORY_DECORATIONS, "marblePlanterPurple"),
                new Ware(ItemList.marblePlanterWhite, CATEGORY_DECORATIONS, "marblePlanterWhite"),
                new Ware(ItemList.marblePlanterOrange, CATEGORY_DECORATIONS, "marblePlanterOrange"),
                new Ware(ItemList.marblePlanterGreenish, CATEGORY_DECORATIONS, "marblePlanterGreenish"),
                new Ware(ItemList.marblePlanterWhiteDotted, CATEGORY_DECORATIONS, "marblePlanterWhiteDotted"),
                new Ware(ItemList.goblinLeaderHat, CATEGORY_ARMOUR_LEATHER, "goblinLeaderHat"),
                new Ware(ItemList.trollKingHat, CATEGORY_ARMOUR_LEATHER, "trollKingHat"),
                new Ware(ItemList.roseTrellis, CATEGORY_TOOL_FARMING, "roseTrellis"),
                new Ware(ItemList.amphoraSmallPottery, CATEGORY_CONTAINER_POTTERY, "amphoraSmallPottery"),
                new Ware(ItemList.amphoraLargePottery, CATEGORY_CONTAINER_POTTERY, "amphoraLargePottery"),
                new Ware(ItemList.kiln, CATEGORY_TOOL_MASONRY, "kiln"),
                new Ware(ItemList.conch, CATEGORY_DECORATIONS, "conch"),
                new Ware(ItemList.birdcage, CATEGORY_DECORATIONS, "birdcage"),
                new Ware(ItemList.smelter, CATEGORY_TOOL_SMITHING, "smelter"),
                new Ware(ItemList.halterRope, CATEGORY_TOOL_FARMING, "halterRope"),
                new Ware(ItemList.swordDisplay, CATEGORY_DECORATIONS, "swordDisplay", 0.5f),
                new Ware(ItemList.axeDisplay, CATEGORY_DECORATIONS, "axeDisplay"),
                new Ware(ItemList.yuleReindeer, CATEGORY_DECORATIONS, "yuleReindeer"),
                new Ware(ItemList.shoulderPads1, CATEGORY_CLOTHES, "shoulderPads1"),
                new Ware(ItemList.shoulderPads2, CATEGORY_CLOTHES, "shoulderPads2"),
                new Ware(ItemList.shoulderPads3, CATEGORY_CLOTHES, "shoulderPads3"),
                new Ware(ItemList.shoulderPads4, CATEGORY_CLOTHES, "shoulderPads4"),
                new Ware(ItemList.shoulderPads5, CATEGORY_CLOTHES, "shoulderPads5"),
                new Ware(ItemList.shoulderPads6, CATEGORY_CLOTHES, "shoulderPads6"),
                new Ware(ItemList.shoulderPads7, CATEGORY_CLOTHES, "shoulderPads7"),
                new Ware(ItemList.shoulderPads8, CATEGORY_CLOTHES, "shoulderPads8"),
                new Ware(ItemList.shoulderPads9, CATEGORY_CLOTHES, "shoulderPads9"),
                new Ware(ItemList.shoulderPads10, CATEGORY_CLOTHES, "shoulderPads10"),
                new Ware(ItemList.shoulderPads11, CATEGORY_CLOTHES, "shoulderPads11"),
                new Ware(ItemList.shoulderPads12, CATEGORY_CLOTHES, "shoulderPads12"),
                new Ware(ItemList.shoulderPads13, CATEGORY_CLOTHES, "shoulderPads13"),
                new Ware(ItemList.shoulderPads14, CATEGORY_CLOTHES, "shoulderPads14"),
                new Ware(ItemList.shoulderPads15, CATEGORY_CLOTHES, "shoulderPads15"),
                new Ware(ItemList.shoulderPads16, CATEGORY_CLOTHES, "shoulderPads16"),
                new Ware(ItemList.shoulderPads17, CATEGORY_CLOTHES, "shoulderPads17"),
                new Ware(ItemList.shoulderPads18, CATEGORY_CLOTHES, "shoulderPads18"),
                new Ware(ItemList.shirtcotton1, CATEGORY_CLOTHES, "shirtcotton1"),
                new Ware(ItemList.shirtcotton2, CATEGORY_CLOTHES, "shirtcotton2"),
                new Ware(ItemList.shirtcotton3, CATEGORY_CLOTHES, "shirtcotton3"),
                new Ware(ItemList.pantscotton1, CATEGORY_CLOTHES, "pantscotton1"),
                new Ware(ItemList.pantscotton2, CATEGORY_CLOTHES, "pantscotton2"),
                new Ware(ItemList.pantscotton3, CATEGORY_CLOTHES, "pantscotton3"),
                new Ware(ItemList.pantscotton3gren, CATEGORY_CLOTHES, "pantscotton3gren"),
                new Ware(ItemList.sleevescotton1, CATEGORY_CLOTHES, "sleevescotton1"),
                new Ware(ItemList.sleevescotton3, CATEGORY_CLOTHES, "sleevescotton3"),
                new Ware(ItemList.ringRift1, CATEGORY_JEWELRY, "ringRift1"),
                new Ware(ItemList.ringRift2, CATEGORY_JEWELRY, "ringRift2"),
                new Ware(ItemList.ringRift3, CATEGORY_JEWELRY, "ringRift3"),
                new Ware(ItemList.ringRift4, CATEGORY_JEWELRY, "ringRift4"),
                new Ware(ItemList.ringRift5, CATEGORY_JEWELRY, "ringRift5"),
                new Ware(ItemList.braceletRift1, CATEGORY_JEWELRY, "braceletRift1"),
                new Ware(ItemList.braceletRift2, CATEGORY_JEWELRY, "braceletRift2"),
                new Ware(ItemList.braceletRift3, CATEGORY_JEWELRY, "braceletRift3"),
                new Ware(ItemList.braceletRift4, CATEGORY_JEWELRY, "braceletRift4"),
                new Ware(ItemList.braceletRift5, CATEGORY_JEWELRY, "braceletRift5"),
                new Ware(ItemList.necklaceRift1, CATEGORY_JEWELRY, "necklaceRift1"),
                new Ware(ItemList.necklaceRift2, CATEGORY_JEWELRY, "necklaceRift2"),
                new Ware(ItemList.necklaceRift3, CATEGORY_JEWELRY, "necklaceRift3"),
                new Ware(ItemList.necklaceRift4, CATEGORY_JEWELRY, "necklaceRift4"),
                new Ware(ItemList.necklaceRift5, CATEGORY_JEWELRY, "necklaceRift5"),
                new Ware(ItemList.potionImbueShatterprot, CATEGORY_NONE, "potionImbueShatterprot"),
                new Ware(ItemList.shoulderPads19, CATEGORY_CLOTHES, "shoulderPads19"),
                new Ware(ItemList.shoulderPads20, CATEGORY_CLOTHES, "shoulderPads20"),
                new Ware(ItemList.shoulderPads21, CATEGORY_CLOTHES, "shoulderPads21"),
                new Ware(ItemList.shoulderPads22, CATEGORY_CLOTHES, "shoulderPads22"),
                new Ware(ItemList.maskOfTheReturner, CATEGORY_CLOTHES, "maskOfTheReturner"),
                new Ware(ItemList.anniversaryKnapsack, CATEGORY_BAGS, "anniversaryKnapsack"),
                new Ware(ItemList.champagne, CATEGORY_BEVERAGE_ALCOHOL, "champagne"),
                new Ware(ItemList.sleevescotton2, CATEGORY_CLOTHES, "sleevescotton2"),
                new Ware(ItemList.sleevescotton, CATEGORY_CLOTHES, "sleevescotton"),
                new Ware(ItemList.pantscotton, CATEGORY_CLOTHES, "pantscotton"),
                new Ware(ItemList.wineBarrelRack, CATEGORY_CONTAINER_WOOD, "wineBarrelRack"),
                new Ware(ItemList.smallBarrelRack, CATEGORY_CONTAINER_WOOD, "smallBarrelRack"),
                new Ware(ItemList.planterRack, CATEGORY_CONTAINER_WOOD, "planterRack"),
                new Ware(ItemList.amphoraRack, CATEGORY_CONTAINER_WOOD, "amphoraRack"),
                new Ware(ItemList.crowbar, CATEGORY_TOOL_MASONRY, "crowbar"),
                new Ware(ItemList.alchemyCupboard, CATEGORY_FURNITURE, "alchemyCupboard", 0.5f),
                new Ware(ItemList.alchFlask, CATEGORY_NONE, "alchFlask"),
                new Ware(ItemList.storageUnit, CATEGORY_CONTAINER_WOOD, "storageUnit"),
                new Ware(ItemList.batteringRam, CATEGORY_NONE, "batteringRam"),
                new Ware(ItemList.almanac, CATEGORY_PAPER_REPORTS, "almanac"),
                new Ware(ItemList.planterPottery, CATEGORY_TOOL_FARMING, "planterPottery"),
                new Ware(ItemList.planterPotteryFull, CATEGORY_TOOL_FARMING, "planterPotteryFull"),
                new Ware(ItemList.pieDishPottery, CATEGORY_TOOL_COOKING, "pieDishPottery"),
                new Ware(ItemList.cakeTin, CATEGORY_TOOL_COOKING, "cakeTin"),
                new Ware(ItemList.stoneware, CATEGORY_TOOL_COOKING, "stoneware"),
                new Ware(ItemList.roastingDishPottery, CATEGORY_TOOL_COOKING, "roastingDishPottery"),
                new Ware(ItemList.breadSlice, CATEGORY_FOOD_BAKED, "breadSlice"),
                new Ware(ItemList.measuringJugPottery, CATEGORY_TOOL_COOKING, "measuringJugPottery"),
                new Ware(ItemList.plate, CATEGORY_TOOL_COOKING, "plate"),
                new Ware(ItemList.hive, CATEGORY_TOOL_FARMING, "hive"),
                new Ware(ItemList.pie, CATEGORY_FOOD_BAKED, "pie"),
                new Ware(ItemList.still, CATEGORY_TOOL_COOKING, "still"),
                new Ware(ItemList.mead, CATEGORY_BEVERAGE_ALCOHOL, "mead"),
                new Ware(ItemList.cider, CATEGORY_BEVERAGE_ALCOHOL, "cider"),
                new Ware(ItemList.beer, CATEGORY_BEVERAGE_ALCOHOL, "beer"),
                new Ware(ItemList.whisky, CATEGORY_BEVERAGE_ALCOHOL, "whisky"),
                new Ware(ItemList.chocolate, CATEGORY_FOOD_CANDY, "chocolate"),
                new Ware(ItemList.omelette, CATEGORY_FOOD_DISH, "omelette"),
                new Ware(ItemList.curry, CATEGORY_FOOD_DISH, "curry"),
                new Ware(ItemList.salad, CATEGORY_FOOD_DISH, "salad"),
                new Ware(ItemList.sausage, CATEGORY_FOOD_DISH, "sausage"),
                new Ware(ItemList.biscuit, CATEGORY_FOOD_CANDY, "biscuit"),
                new Ware(ItemList.ratOnAStick, CATEGORY_FOOD_DISH, "ratOnAStick"),
                new Ware(ItemList.hogRoast, CATEGORY_FOOD_DISH, "hogRoast"),
                new Ware(ItemList.lambSpit, CATEGORY_FOOD_DISH, "lambSpit"),
                new Ware(ItemList.haggis, CATEGORY_FOOD_DISH, "haggis"),
                new Ware(ItemList.cheesecake, CATEGORY_FOOD_CANDY, "cheesecake"),
                new Ware(ItemList.kielbasa, CATEGORY_FOOD_DISH, "kielbasa"),
                new Ware(ItemList.stuffedMushroom, CATEGORY_FOOD_DISH, "stuffedMushroom"),
                new Ware(ItemList.crisps, CATEGORY_FOOD_DISH, "crisps"),
                new Ware(ItemList.scone, CATEGORY_FOOD_BAKED, "scone"),
                new Ware(ItemList.toast, CATEGORY_FOOD_BAKED, "toast"),
                new Ware(ItemList.vodka, CATEGORY_BEVERAGE_ALCOHOL, "vodka"),
                new Ware(ItemList.brandy, CATEGORY_BEVERAGE_ALCOHOL, "brandy"),
                new Ware(ItemList.moonshine, CATEGORY_BEVERAGE_ALCOHOL, "moonshine"),
                new Ware(ItemList.gin, CATEGORY_BEVERAGE_ALCOHOL, "gin"),
                new Ware(ItemList.mortarAndPestle, CATEGORY_TOOL_COOKING, "mortarAndPestle"),
                new Ware(ItemList.beeSmoker, CATEGORY_TOOL_FARMING, "beeSmoker"),
                new Ware(ItemList.meatballs, CATEGORY_FOOD_DISH, "meatballs"),
                new Ware(ItemList.beerSteinPottery, CATEGORY_CONTAINER_POTTERY, "beerSteinPottery"),
                new Ware(ItemList.waxSealingKit, CATEGORY_TOOL_COOKING, "waxSealingKit"),
                new Ware(ItemList.tart, CATEGORY_FOOD_CANDY, "tart"),
                new Ware(ItemList.pizza, CATEGORY_FOOD_BAKED, "pizza"),
                new Ware(ItemList.sushi, CATEGORY_FOOD_DISH, "sushi"),
                new Ware(ItemList.pudding, CATEGORY_FOOD_DISH, "pudding"),
                new Ware(ItemList.messageBoard, CATEGORY_DECORATIONS, "messageBoard"),
                new Ware(ItemList.paperSheet, CATEGORY_PAPER_REPORTS, "paperSheet", 3.0f),
                new Ware(ItemList.hopsTrellis, CATEGORY_TOOL_FARMING, "hopsTrellis"),
                new Ware(ItemList.larder, CATEGORY_CONTAINER_WOOD, "larder"),
                new Ware(ItemList.icecream, CATEGORY_FOOD_CANDY, "icecream"),
                new Ware(ItemList.sweet, CATEGORY_FOOD_CANDY, "sweet"),
                new Ware(ItemList.rum, CATEGORY_BEVERAGE_ALCOHOL, "rum"),
                new Ware(ItemList.muffin, CATEGORY_FOOD_CANDY, "muffin"),
                new Ware(ItemList.cookie, CATEGORY_FOOD_CANDY, "cookie"),
                new Ware(ItemList.runeMag, CATEGORY_NONE, "runeMag"),
                new Ware(ItemList.runeFo, CATEGORY_NONE, "runeFo"),
                new Ware(ItemList.runeVyn, CATEGORY_NONE, "runeVyn"),
                new Ware(ItemList.runeLib, CATEGORY_NONE, "runeLib"),
                new Ware(ItemList.runeJackal, CATEGORY_NONE, "runeJackal"),
                new Ware(ItemList.lunchbox, CATEGORY_CONTAINER_METAL, "lunchbox"),
                new Ware(ItemList.xmasLunchbox, CATEGORY_CONTAINER_METAL, "xmasLunchbox"),
                new Ware(ItemList.goldenMirror, CATEGORY_NONE, "goldenMirror"),
                new Ware(ItemList.midsummerMask, CATEGORY_CLOTHES, "midsummerMask"),
                new Ware(ItemList.creatureCrate, CATEGORY_NONE, "creatureCrate", 0.3f),
                new Ware(ItemList.crateRack, CATEGORY_CONTAINER_WOOD, "crateRack"),
                new Ware(ItemList.strawBed, CATEGORY_FURNITURE, "strawBed"),
                new Ware(ItemList.bsbRack, CATEGORY_CONTAINER_WOOD, "bsbRack"),
                new Ware(ItemList.bulkContainerUnit, CATEGORY_CONTAINER_WOOD, "bulkContainerUnit", 0.3f),
                new Ware(ItemList.tapestryEvening, CATEGORY_DECORATIONS, "tapestryEvening"),
                new Ware(ItemList.tapestryMclavin, CATEGORY_DECORATIONS, "tapestryMclavin"),
                new Ware(ItemList.tapestryEhizellbob, CATEGORY_DECORATIONS, "tapestryEhizellbob"),
                new Ware(ItemList.maskTrollHalloween, CATEGORY_CLOTHES, "maskTrollHalloween"),
                new Ware(ItemList.shoulderPumpkinHalloween, CATEGORY_CLOTHES, "shoulderPumpkinHalloween"),
                new Ware(ItemList.statueEagle, CATEGORY_DECORATIONS, "statueEagle", 0.2f),
                new Ware(ItemList.statueWorg, CATEGORY_DECORATIONS, "statueWorg", 0.2f),
                new Ware(ItemList.statueHellHorse, CATEGORY_DECORATIONS, "statueHellHorse", 0.2f),
                new Ware(ItemList.statueVynora, CATEGORY_DECORATIONS, "statueVynora", 0.2f),
                new Ware(ItemList.statueMagranon, CATEGORY_DECORATIONS, "statueMagranon", 0.2f),
                new Ware(ItemList.statueFo, CATEGORY_DECORATIONS, "statueFo", 0.2f),
                new Ware(ItemList.statueLibila, CATEGORY_DECORATIONS, "statueLibila", 0.2f),
                new Ware(ItemList.statueDrake, CATEGORY_DECORATIONS, "statueDrake", 0.2f),
                new Ware(ItemList.saddleBags, CATEGORY_BAGS, "saddleBags"),
                new Ware(ItemList.saddleBagsXmas, CATEGORY_BAGS, "saddleBagsXmas"),
                new Ware(ItemList.boxTackle, CATEGORY_TOOL_FISHING, "boxTackle"),
                new Ware(ItemList.netKeep, CATEGORY_TOOL_FISHING, "netKeep"),
                new Ware(ItemList.netFishing, CATEGORY_TOOL_FISHING, "netFishing"),
                new Ware(ItemList.fishingPole, CATEGORY_TOOL_FISHING, "fishingPole"),
                new Ware(ItemList.eyeletMetal, CATEGORY_TOOL_FISHING, "eyeletMetal", 0.1f),
                new Ware(ItemList.fishingRod, CATEGORY_TOOL_FISHING, "fishingRod"),
                new Ware(ItemList.fishingLineBasic, CATEGORY_TOOL_FISHING, "fishingLineBasic"),
                new Ware(ItemList.fishingLineLight, CATEGORY_TOOL_FISHING, "fishingLineLight"),
                new Ware(ItemList.fishingLineMedium, CATEGORY_TOOL_FISHING, "fishingLineMedium"),
                new Ware(ItemList.fishingLineHeavy, CATEGORY_TOOL_FISHING, "fishingLineHeavy"),
                new Ware(ItemList.fishingLineBraided, CATEGORY_TOOL_FISHING, "fishingLineBraided"),
                new Ware(ItemList.floatFeather, CATEGORY_TOOL_FISHING, "floatFeather", 0.1f),
                new Ware(ItemList.floatTwig, CATEGORY_TOOL_FISHING, "floatTwig", 0.1f),
                new Ware(ItemList.floatMoss, CATEGORY_TOOL_FISHING, "floatMoss", 0.1f),
                new Ware(ItemList.floatBark, CATEGORY_TOOL_FISHING, "floatBark", 0.1f),
                new Ware(ItemList.hookWood, CATEGORY_TOOL_FISHING, "hookWood", 0.1f),
                new Ware(ItemList.hookMetal, CATEGORY_TOOL_FISHING, "hookMetal"),
                new Ware(ItemList.hookBone, CATEGORY_TOOL_FISHING, "hookBone"),
                new Ware(ItemList.baitFly, CATEGORY_TOOL_FISHING, "baitFly", 0.1f),
                new Ware(ItemList.baitCheese, CATEGORY_TOOL_FISHING, "baitCheese", 0.1f),
                new Ware(ItemList.baitDough, CATEGORY_TOOL_FISHING, "baitDough", 0.1f),
                new Ware(ItemList.baitWurm, CATEGORY_TOOL_FISHING, "baitWurm", 0.1f),
                new Ware(ItemList.baitFish, CATEGORY_TOOL_FISHING, "baitFish", 0.1f),
                new Ware(ItemList.baitGrub, CATEGORY_TOOL_FISHING, "baitGrub", 0.1f),
                new Ware(ItemList.baitWheat, CATEGORY_TOOL_FISHING, "baitWheat", 0.1f),
                new Ware(ItemList.baitCorn, CATEGORY_TOOL_FISHING, "baitCorn", 0.1f),
                new Ware(ItemList.reelWood, CATEGORY_TOOL_FISHING, "reelWood", 0.1f),
                new Ware(ItemList.reelMetal, CATEGORY_TOOL_FISHING, "reelMetal"),
                new Ware(ItemList.reelProfessional, CATEGORY_TOOL_FISHING, "reelProfessional"),
                new Ware(ItemList.fishingReelLight, CATEGORY_TOOL_FISHING, "fishingReelLight"),
                new Ware(ItemList.fishingReelMedium, CATEGORY_TOOL_FISHING, "fishingReelMedium"),
                new Ware(ItemList.fishingReelDeepWater, CATEGORY_TOOL_FISHING, "fishingReelDeepWater"),
                new Ware(ItemList.fishingReelProfessional, CATEGORY_TOOL_FISHING, "fishingReelProfessional"),
                new Ware(ItemList.tackleReel, CATEGORY_TOOL_FISHING, "tackleReel"),
                new Ware(ItemList.tackleLine, CATEGORY_TOOL_FISHING, "tackleLine"),
                new Ware(ItemList.tackleFloat, CATEGORY_TOOL_FISHING, "tackleFloat"),
                new Ware(ItemList.tackleHook, CATEGORY_TOOL_FISHING, "tackleHook"),
                new Ware(ItemList.tackleBaitFly, CATEGORY_TOOL_FISHING, "tackleBaitFly", 0.1f),
                new Ware(ItemList.tackleBaitCheese, CATEGORY_TOOL_FISHING, "tackleBaitCheese", 0.1f),
                new Ware(ItemList.tackleBaitDough, CATEGORY_TOOL_FISHING, "tackleBaitDough", 0.1f),
                new Ware(ItemList.tackleBaitWurm, CATEGORY_TOOL_FISHING, "tackleBaitWurm", 0.1f),
                new Ware(ItemList.tackleBaitFishBits, CATEGORY_TOOL_FISHING, "tackleBaitFishBits", 0.1f),
                new Ware(ItemList.tackleBaitGrub, CATEGORY_TOOL_FISHING, "tackleBaitGrub", 0.1f),
                new Ware(ItemList.tackleBaitWheat, CATEGORY_TOOL_FISHING, "tackleBaitWheat", 0.1f),
                new Ware(ItemList.tackleBaitCorn, CATEGORY_TOOL_FISHING, "tackleBaitCorn", 0.1f),
                new Ware(ItemList.tackleBaitSardine, CATEGORY_TOOL_FISHING, "tackleBaitSardine", 0.1f),
                new Ware(ItemList.tackleBaitRoach, CATEGORY_TOOL_FISHING, "tackleBaitRoach", 0.1f),
                new Ware(ItemList.tackleBaitPerch, CATEGORY_TOOL_FISHING, "tackleBaitPerch", 0.1f),
                new Ware(ItemList.tackleBaitMinnow, CATEGORY_TOOL_FISHING, "tackleBaitMinnow", 0.1f),
                new Ware(ItemList.rackFishingRods, CATEGORY_CONTAINER_WOOD, "rackFishingRods"),
                new Ware(ItemList.fishTrophy, CATEGORY_DECORATIONS, "fishTrophy"),
                new Ware(ItemList.buoy, CATEGORY_TOOL_FISHING, "buoy", 0.5f),
                new Ware(ItemList.necklacePearl, CATEGORY_JEWELRY, "necklacePearl"),
                new Ware(ItemList.emptyLowBookshelf, CATEGORY_FURNITURE, "emptyLowBookshelf", 0.3f),
                new Ware(ItemList.emptyHighBookshelf, CATEGORY_FURNITURE, "emptyHighBookshelf", 0.3f),
                new Ware(ItemList.barDecoration, CATEGORY_FURNITURE, "barDecoration", 0.1f),
                new Ware(ItemList.archaeologyReport, CATEGORY_PAPER_REPORTS, "archaeologyReport"),
                new Ware(ItemList.archaeologyJournal, CATEGORY_PAPER_REPORTS, "archaeologyJournal"),
                new Ware(ItemList.statueGuard, CATEGORY_DECORATIONS, "statueGuard", 0.1f),
                new Ware(ItemList.statueKyklops, CATEGORY_DECORATIONS, "statueKyklops", 0.1f),
                new Ware(ItemList.statueRiftBeast, CATEGORY_DECORATIONS, "statueRiftBeast", 0.2f),
                new Ware(ItemList.statueMountainLion, CATEGORY_DECORATIONS, "statueMountainLion", 0.1f),
                new Ware(ItemList.book, CATEGORY_PAPER_REPORTS, "book"),
                new Ware(ItemList.creatureCarrier, CATEGORY_CONTAINER_WOOD, "creatureCarrier"),
                new Ware(ItemList.potionButchery, CATEGORY_NONE, "potionButchery"),
                new Ware(ItemList.goblinCrystalBall, CATEGORY_NONE, "goblinCrystalBall"),
                new Ware(ItemList.statueUnicorn, CATEGORY_DECORATIONS, "statueUnicorn", 0.2f),
                new Ware(ItemList.statueGoblin, CATEGORY_DECORATIONS, "statueGoblin", 0.2f),
                new Ware(ItemList.statueFiend, CATEGORY_DECORATIONS, "statueFiend", 0.2f),
                new Ware(ItemList.statuetteMiner, CATEGORY_DECORATIONS, "statuetteMiner"),
                new Ware(ItemList.statuetteSwordsman, CATEGORY_DECORATIONS, "statuetteSwordsman"),
                new Ware(ItemList.statuetteAxeman, CATEGORY_DECORATIONS, "statuetteAxeman"),
                new Ware(ItemList.statuetteDigger, CATEGORY_DECORATIONS, "statuetteDigger"),
                new Ware(ItemList.miniDeedToken, CATEGORY_DECORATIONS, "miniDeedToken"),
                new Ware(ItemList.dasBoot, CATEGORY_DECORATIONS, "dasBoot"),
                new Ware(ItemList.clothHoodWhite, CATEGORY_CLOTHES, "clothHoodWhite"),
                new Ware(ItemList.clothSleeveWhite, CATEGORY_CLOTHES, "clothSleeveWhite"),
                new Ware(ItemList.clothJacketWhite, CATEGORY_CLOTHES, "clothJacketWhite"),
                new Ware(ItemList.maskSkullHalloween, CATEGORY_CLOTHES, "maskSkullHalloween"),
                new Ware(ItemList.hatWitchHalloween, CATEGORY_CLOTHES, "hatWitchHalloween"),
                new Ware(ItemList.statueTich, CATEGORY_DECORATIONS, "statueTich", 0.1f),
                new Ware(ItemList.chickenCoop, CATEGORY_CONTAINER_WOOD, "chickenCoop", 0.1f),
                new Ware(ItemList.xmasSnowman, CATEGORY_DECORATIONS, "xmasSnowman"),
                };
        waresIndex = new HashMap<>();
        for(int i = 0; i < wares.length; ++i)
            waresIndex.put(wares[i].templateId, wares[i]);
    }
}
