package net.spirangle.awakening.tasks;

import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.ServantTaskQuestion;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONArray;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import net.spirangle.awakening.util.StringUtils;

import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class HolidayCoinTask extends Task {

    private static final Logger logger = Logger.getLogger(HolidayCoinTask.class.getName());

    private class HolidayCoinTradeItem {
        public int id;
        public String name;
        public int templateId;
        public int color;
        public float qualityLevel;
        public int weight;
        public int containerTemplateId;
        public float containerQualityLevel;

        public HolidayCoinTradeItem(int i, String nm, int tid, int col, float ql, int w, int ctid, float cql) {
            id = i;
            name = nm;
            templateId = tid;
            color = col;
            qualityLevel = ql;
            weight = w;
            containerTemplateId = ctid;
            containerQualityLevel = cql;
        }
    }


    private final HolidayCoinTradeItem[] tradeItemTypes = {
            new HolidayCoinTradeItem(0x0101, "dye, christmas blue", ItemList.dye, 0xBD8F2A, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0102, "dye, christmas red", ItemList.dye, 0x2624D4, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0103, "dye, christmas green", ItemList.dye, 0x0D8D3C, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0104, "dye, christmas gold", ItemList.dye, 0x05A8CA, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0105, "dye, christmas yellow", ItemList.dye, 0x66FFFF, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0106, "dye, christmas orange", ItemList.dye, 0x008CFF, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0107, "dye, christmas brown", ItemList.dye, 0x13458B, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0108, "dye, christmas pink", ItemList.dye, 0xB469FF, 99.0f, 2000, ItemList.jarPottery, 1.0f),
            new HolidayCoinTradeItem(0x0109, "dye, christmas black", ItemList.dye, 0x000000, 99.0f, 2000, ItemList.jarPottery, 1.0f)
    };

    public static String getAboutTaskTitle(AboutTaskParams params) {
        return null;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        if(createTaskTest(params.responder, params.taskPaper, params.servant)) {
            params.title = "(GM) Trade holiday coins";
            params.tooltip = "GM Task: Trade holiday coins for special items";
            return params.title;
        }
        return null;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return taskPaper == null || responder == null || responder.getPower() >= MiscConstants.POWER_DEMIGOD;
    }

    public HolidayCoinTradeItem[] tradeItems = null;

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_HOLIDAY_COIN;
    }

    @Override
    public String getTitle(Creature responder) {
        return "GM: Trade holiday coins";
    }

    @Override
    public String getSubject(Creature responder) {
        return "Trading holiday coins";
    }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        bml.append("text{text=\"This task will instruct " + name + " to trade holday coins for special items.\r\n\r\nWhich items should " + name + " trade?\"}");
        for(int i = 0; i < tradeItemTypes.length; ++i) {
            HolidayCoinTradeItem hcti = tradeItemTypes[i];
            String tradeItem = StringUtils.bmlString(StringUtils.capitalize(hcti.name));
            bml.append("checkbox{id='trade-item-" + hcti.id + "';text=\"" + tradeItem + "\"}");
        }
        responder.getCommunicator().sendBml(450, 350, true, true, bml.toString(), 200, 200, 200, "Servant: Trade Holiday Coin");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        ArrayList<HolidayCoinTradeItem> list = new ArrayList<HolidayCoinTradeItem>();
        for(int i = 0; i < tradeItemTypes.length; ++i) {
            HolidayCoinTradeItem hcti = tradeItemTypes[i];
            if("true".equals(properties.getProperty("trade-item-" + hcti.id))) {
                list.add(hcti);
            }
        }
        tradeItems = list.toArray(new HolidayCoinTradeItem[list.size()]);
        //		for(int i=0; i<tradeItems.length; ++i)
        //			AwakeningMod.info("HolidayCoinTask.handleCreateTaskAnswer(tradeItem: "+tradeItems[i].name+")");
        return true;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String message = "If you have a holiday coin, give it to " + npc.getName() + " for an option to pick a special item.";
        responder.getCommunicator().sendNormalServerMessage(message);
        return true;
    }

    @Override
    public boolean handleGiveTo(Creature performer, Item source) {
        if(source.getTemplateId() == ItemTemplateCreatorAwakening.holidayCoin) {
            ServantTaskQuestion stq = new ServantTaskQuestion(performer, source, this, 0, null);
            stq.sendQuestion();
            return true;
        }
        return false;
    }

    @Override
    public void sendTaskQuestion(ServantTaskQuestion question, int id, Object data) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        StringBuilder bml = new StringBuilder();
        bml.append("text{text=\"" + name + " will give you one of these items in return for your holiday coin:\"}");
        for(int i = 0; i < tradeItems.length; ++i) {
            HolidayCoinTradeItem hcti = tradeItems[i];
            String tradeItem = StringUtils.bmlString(StringUtils.capitalize(hcti.name));
            bml.append("radio{group='trade-item';id='" + hcti.id + "';text=\"" + tradeItem + "\"}");
        }
        bml.append("radio{group='trade-item';id='0';text=\"Do nothing\";selected='true'}");
        responder.getCommunicator().sendBml(350, 350, true, true, bml.toString(), 200, 200, 200, servant.npc.getName());
    }

    @Override
    public void handleTaskAnswer(ServantTaskQuestion question, int id, Object data, Properties properties) {
        Creature responder = question.getResponder();
        String ti = properties.getProperty("trade-item");
        HolidayCoinTradeItem hcti = null;
        if(ti != null) {
            int n = Integer.parseInt(ti);
            for(int i = 0; i < tradeItems.length; ++i)
                if(tradeItems[i].id == n) {
                    hcti = tradeItems[i];
                    break;
                }
        }
        if(hcti != null) {
            String creator = "Santa Claus";
            try {
                Item item = ItemFactory.createItem(hcti.templateId, hcti.qualityLevel, creator);
                item.setName(hcti.name);
                if(hcti.color != -1) item.setColor(hcti.color);
                if(hcti.weight != -1) item.setWeight(hcti.weight, true);
                if(hcti.containerTemplateId != -1) {
                    Item container = ItemFactory.createItem(hcti.containerTemplateId, hcti.containerQualityLevel, creator);
                    container.insertItem(item, true);
                    item = container;
                }
                responder.getInventory().insertItem(item, true);
                Items.destroyItem(question.getSourceItem().getWurmId());
            } catch(NoSuchTemplateException | FailedException e) {
                logger.log(Level.SEVERE, "Failed creating trade item.", e);
            }
        }
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        JSONArray ja = jo.optJSONArray("tradeItems");
        if(ja != null) {
            ArrayList<HolidayCoinTradeItem> list = new ArrayList<HolidayCoinTradeItem>();
            for(int i = 0, n = ja.length(); i < n; ++i) {
                int id = ja.optInt(i);
                for(int j = 0; j < tradeItemTypes.length; ++j) {
                    if(tradeItemTypes[j].id == id) {
                        list.add(tradeItemTypes[j]);
                        //						AwakeningMod.info("HolidayCoinTask.readJSON(tradeItem: "+tradeItemTypes[j].name+")");
                        break;
                    }
                }
            }
            tradeItems = list.toArray(new HolidayCoinTradeItem[list.size()]);
        }
        return true;
    }

    @Override
    public String toJSONString() {
        StringBuilder json = new StringBuilder();
        json.append("{tradeItems:[");
        for(int i = 0; i < tradeItems.length; ++i) {
            if(i > 0) json.append(',');
            json.append(tradeItems[i].id);
        }
        json.append("]}");
        return json.toString();
    }
}
