package net.spirangle.awakening.tasks;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.questions.Question;
import com.wurmonline.server.questions.TalkToServantQuestion;
import com.wurmonline.server.support.JSONObject;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;
import java.util.logging.Logger;


public class MessageTask extends Task {

    private static final Logger logger = Logger.getLogger(MessageTask.class.getName());

    public static String getAboutTaskTitle(AboutTaskParams params) {
        if(params.getTitle) params.title = "Message - tells visitors a private message";
        if(params.getDescription) {
            params.header = "About Task: Message";
            params.description = "The message task makes the servant tell visiting players some private information " +
                                 "in the event log. Servants can usually handle many messages.\n\n" +
                                 "Each message will cost 1 iron daily.";
        }
        return params.title;
    }

    public static String getInstructTaskTitle(InstructTaskParams params) {
        params.title = "Tell message";
        params.tooltip = "Tell a message to players who come to talk";
        return params.title;
    }

    public static boolean createTaskTest(Creature responder, Item taskPaper, Servant servant) {
        return true;
    }

    public String subject = "";
    public String message = "";

    @Override
    public int getTaskType() {
        return AwakeningConstants.TASK_MESSAGE;
    }

    @Override
    public String getTitle(Creature responder) {
        return "Message: " + (message.length() > 100? message.substring(0, 97) + "..." : message);
    }

    @Override
    public String getSubject(Creature responder) {
        return "Message: " + subject;
    }

    @Override
    public long getSalary() { return 1L; }

    @Override
    public float getWeight() { return 0.1f; }

    @Override
    public void sendCreateTaskQuestion(Question question, Item taskPaper, Servant servant) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        String name = StringUtils.bmlString(npc.getName());
        int w = 450;
        int h = 350;
        String bml = "border{\n" +
                     " varray{rescale='true';\n" +
                     "  text{type='bold';text=\"Tell a message:\"}\n" +
                     " };\n" +
                     " null;\n" +
                     " scroll{horizontal='false';vertical='true';\n" +
                     "  varray{rescale='true';\n" +
                     "   passthrough{id='id';text='" + question.getId() + "'};\n" +
                     "   text{text=''}\n" +
                     "   text{text=\"This task will instruct " + name + " to tell a message when asked for it. " +
                     "The message must not be too long for " + name + " to remember.\n\nWhat is the subject of the message?\"}\n" +
                     "   harray{\n" +
                     "    input{id='subject';maxchars='50';maxlines='1';size='400,15';text=''}\n" +
                     "   }\n" +
                     "   text{text=''}\n" +
                     "   text{text=\"What is the message?\"}\n" +
                     "   harray{\n" +
                     "    input{id='message';maxchars='100';maxlines='2';size='400,40';text=''}\n" +
                     "   }\n" +
                     "  }\n" +
                     " };\n" +
                     " null;\n" +
                     " right{\n" +
                     "  harray{\n" +
                     "   button{id='start';size='80,20';text='Back'}\n" +
                     "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                     "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                     "   button{id='submit';size='80,20';text='Instruct'}\n" +
                     "  }\n" +
                     " }\n" +
                     "}\n";
        responder.getCommunicator().sendBml(w, h, false, true, bml, 200, 200, 200, "Servant: Announce Message");
    }

    @Override
    public boolean handleCreateTaskAnswer(Question question, Item taskPaper, Servant servant, Properties properties) {
        subject = properties.getProperty("subject");
        message = properties.getProperty("message");
        return subject.length() > 0 && message.length() > 0;
    }

    @Override
    public boolean handleTalkToAnswer(TalkToServantQuestion question, Properties properties) {
        Creature responder = question.getResponder();
        Creature npc = servant.npc;
        responder.getCommunicator().sendNormalServerMessage(npc.getName() + " says: " + message);
        return true;
    }

    @Override
    protected boolean readJSON(JSONObject jo) {
        if(!super.readJSON(jo)) return false;
        subject = jo.optString("subject");
        message = jo.optString("message");
        logger.info("MessageTask.readJSON(subject: " + subject + ", message: " + message + ")");
        return true;
    }

    @Override
    public String toJSONString() {
        return "{subject:" + JSONObject.quote(subject) + ",message:" + JSONObject.quote(message) + "}";
    }
}
