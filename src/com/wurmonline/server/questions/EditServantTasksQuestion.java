package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.creatures.Creature;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.tasks.Task;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.EDIT_SERVANT_TASKS_QUESTION_TYPE;


public class EditServantTasksQuestion extends Question {

    private static final Logger logger = Logger.getLogger(EditServantTasksQuestion.class.getName());

    private final Servant servant;

    public EditServantTasksQuestion(Creature responder, Servant servant) {
        super(responder, "Servant", "", EDIT_SERVANT_TASKS_QUESTION_TYPE, responder.getWurmId());
        this.servant = servant;
    }

    @Override
    public void sendQuestion() {
        Creature responder = this.getResponder();
        if(responder.getPower() < MiscConstants.POWER_DEMIGOD) return;
        Creature npc = servant.npc;
        StringBuilder bml = new StringBuilder();
        int w = 550;
        int h = 550;
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Edit Servant tasks:\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + getId() + "'};\n" +
                   "   text{text=\"Listing the tasks performed by " + StringUtils.bmlString(npc.getName()) + ":\"}\n");
        try(Connection con = ModSupportDb.getModSupportDb();
            PreparedStatement ps = con.prepareStatement("SELECT TASKID,TYPE,QUALITYLEVEL,DATA FROM TASKS WHERE NPCID=?")) {
            ps.setLong(1, npc.getWurmId());
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    long taskId = rs.getLong(1);
                    int taskType = rs.getInt(2);
                    float taskQL = rs.getFloat(3);
                    String data = rs.getString(4);
                    bml.append("   text{text=\"Task (id=" + taskId + ", type=" + taskType + ", QL=" + taskQL + "):\"}\n");
                    bml.append("   harray{input{id='task-" + taskId + "';bgcolor='100,100,100';maxchars='2048';maxlines='-1';size='520,50';text=\"" + StringUtils.bmlString(data) + "\"}}\n");
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to load servant tasks: " + e.getMessage(), e);
        }
        bml.append("  }\n" +
                   " }\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                   "   button{id='reload';size='80,20';text='Reload'}\n" +
                   "   button{id='submit';size='80,20';text='Update'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {
        Creature responder = this.getResponder();
        if(responder.getPower() < MiscConstants.POWER_DEMIGOD) return;

        if("true".equals(properties.getProperty("cancel"))) return;

        Creature npc = servant.npc;
        boolean reload = false;
        if("true".equals(properties.getProperty("reload"))) reload = true;
        else {
            Enumeration<String> e = (Enumeration<String>) properties.propertyNames();
            int n = 0;
            while(e.hasMoreElements()) {
                String key = e.nextElement();
                if(key.startsWith("task-")) {
                    String data = properties.getProperty(key);
                    logger.info(key + ": " + data);
                    long taskId = Long.parseLong(key.substring(5));
                    Task task = servant.getTask(taskId);
                    if(task != null) {
                        task.updateDb(data);
                        responder.getCommunicator().sendSafeServerMessage("Updated the task " + taskId + " performed by servant " + npc.getName() + ".");
                        ++n;
                    }
                }
            }
            if(n > 0) reload = true;
        }
        if(reload) {
            servant.reloadTasks();
            responder.getCommunicator().sendSafeServerMessage("Reloaded all tasks performed by servant " + npc.getName() + ".");
        }
    }
}
