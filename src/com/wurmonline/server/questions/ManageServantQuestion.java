package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.creatures.Creatures;
import com.wurmonline.server.creatures.NoSuchCreatureException;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.villages.Village;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wurmonline.server.questions.TalkToServantQuestion.*;
import static net.spirangle.awakening.AwakeningConstants.MANAGE_SERVANT_QUESTION_TYPE;


public class ManageServantQuestion extends Question {

    private static final Logger logger = Logger.getLogger(ManageServantQuestion.class.getName());

    private final Item contract;
    private Creature npc;
    private final int subject;

    public ManageServantQuestion(Creature responder, Item contract) {
        this(responder, contract, -1);
    }

    public ManageServantQuestion(Creature responder, Item contract, int subject) {
        super(responder, "Servant", "", MANAGE_SERVANT_QUESTION_TYPE, contract.getWurmId());
        this.contract = contract;
        this.npc = null;
        this.subject = subject;
        if(subject < TALK_ABOUT_SERVANTS) {
            long data = contract.getData();
            if(data >= 0) {
                try {
                    this.npc = Creatures.getInstance().getCreature(data);
                } catch(NoSuchCreatureException e) { }
            }
        }
    }

    private void sendQuestion(int subject) {
        Creature responder = this.getResponder();
        ManageServantQuestion question = new ManageServantQuestion(responder, contract, subject);
        question.sendQuestion();
    }

    @Override
    public void sendQuestion() {
        Creature responder = this.getResponder();
        int w = 350;
        int h = 350;
        if(subject >= TALK_ABOUT_SERVANTS) {
            sendTalkAboutServantsQuestion(responder, getId(), subject);
        } else if(npc == null) {
            boolean citiz = false;
            Village v = responder.getCitizenVillage();
            if(v != null) citiz = true;
            StringBuilder bml = new StringBuilder();
            bml.append("border{\n" +
                       " varray{rescale='true';\n" +
                       "  text{type='bold';text='Manage Servant:'}\n" +
                       " };\n" +
                       " null;\n" +
                       " scroll{horizontal='false';vertical='true';\n" +
                       "  varray{rescale='true';\n" +
                       "   passthrough{id='id';text='" + id + "'};\n" +
                       "   text{text=\"A personal servant is in your employment, to perform tasks for you. To give " +
                       "a task you first have to write a task instruction on a paper, which should be given to the " +
                       "servant. How many tasks you can give is depending on the quality of the task instructions, " +
                       "and also which kind of tasks the servant will understand.\n\nWhen summoning a servant, you " +
                       "have to pay a salary which is taken automatically from your bank account. The salary depends on " +
                       "what tasks the servant is doing, each task has an individual cost. If there isn't enough money " +
                       "in your account when it's time to pay the salary, the servant will leave. The basic cost, i.e. " +
                       "with no tasks, is one iron per hour, and then each task adds extra cost.\"}\n" +
                       "   text{text=''}\n" +
                       "   text{type='bold';text=\"Hire servant:\"}\n" +
                       "   text{text=\"By using this contract a servant will appear. The servant will appear where you stand, facing the direction you are facing.\"}\n" +
                       "   text{text=''}\n");

            if(citiz && v != null) {
                bml.append("   text{type='italic';text=\"The servant will become part of " + StringUtils.bmlString(v.getName()) + ".\"}\n");
            } else {
                bml.append("   text{type='italic';text=\"The servant will not become part of any village.\"}\n");
            }

            byte sex = responder.getSex();
            w = 500;
            h = 450;
            bml.append("   text{text=''}\n" +
                       "   label{text='Gender:'}\n" +
                       "   radio{group='gender';id='male';text='Male'" + (sex == MiscConstants.SEX_MALE? ";selected='true'" : "") + "}\n" +
                       "   radio{group='gender';id='female';text='Female'" + (sex == MiscConstants.SEX_FEMALE? ";selected='true'" : "") + "}\n" +
                       "   text{text=''}\n" +
                       "   label{text=\"Name:\"};\n" +
                       "   harray{\n" +
                       "    input{id='name';size='300,20';maxchars='20'};\n" +
                       "   }\n" +
                       "  }\n" +
                       " };\n" +
                       " null;\n" +
                       " right{\n" +
                       "  harray{\n" +
                       "   button{id='about';size='120,20';text='About Servants'}\n" +
                       "   text{size='" + (w - 6 - 320) + ",1';text=''}\n" +
                       "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='120,20';text='Summon Servant'}\n" +
                       "  }\n" +
                       " }\n" +
                       "}\n");
            responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
        } else {
            Servant servant = Servant.getServant(npc);
            if(servant == null) return;
            StringBuilder bml = new StringBuilder();
            bml.append("border{\n" +
                       " varray{rescale='true';\n" +
                       "  text{type='bold';text='Manage Servant:'}\n" +
                       " };\n" +
                       " null;\n" +
                       " scroll{horizontal='false';vertical='true';\n" +
                       "  varray{rescale='true';\n" +
                       "   passthrough{id='id';text='" + getId() + "'};\n" +
                       "   text{text=\"The servant " + StringUtils.bmlString(npc.getName()) + " is in your employment, and currently has " + servant.tasks.size() + " tasks.\"}\n" +
                       "   text{text=''}\n");

            long salary = servant.getSalary();
            if(salary == 0L) {
                bml.append("   text{text=\"You are currently paying " + npc.getName() + " no salary.\"}\n");
            } else {
                String cost = new Change(salary).getChangeString();
                bml.append("   text{text=\"You are currently paying " + StringUtils.bmlString(npc.getName()) + " a salary of " + cost + " per day, which is taken from your bank account.\"}\n");
            }

            w = 450;
            h = 350;
            boolean tasks = responder.getPower() >= MiscConstants.POWER_DEMIGOD;
            bml.append("   text{text=''}\n" +
                       "   text{type='italic';text=\"Dismissing this servant will remove all tasks, but any given items are dropped on the ground for you to pick up.\"}\n" +
                       "   text{text=''}\n" +
                       "   text{text=\"Will you dismiss this servant?\"}\n" +
                       "   radio{group='dismiss';id='true';text='Yes'}" +
                       "   radio{group='dismiss';id='false';text='No';selected='true'}" +
                       "  }\n" +
                       " };\n" +
                       " null;\n" +
                       " right{\n" +
                       "  harray{\n" +
                       "   button{id='about';size='120,20';text='About Servants'}\n" +
                       (tasks? "   button{id='tasks';size='80,20';text='Edit Tasks'}\n" : "") +
                       "   text{size='" + (w - 6 - (tasks? 400 : 320)) + ",1';text=''}\n" +
                       "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='120,20';text='Dismiss Servant'}\n" +
                       "  }\n" +
                       " }\n" +
                       "}\n");
            responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
        }
    }

    @Override
    public void answer(Properties properties) {
        Creature responder = this.getResponder();

        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("start"))) {
            sendQuestion(TALK_ABOUT_NOTHING);
        } else if("true".equals(properties.getProperty("about-back"))) {
            String to = properties.getProperty("about-to");
            sendQuestion(to == null? TALK_ABOUT_SERVANTS : Integer.parseInt(to));
        } else if(properties.containsKey("about")) {
            String about = properties.getProperty("about");
            sendQuestion("true".equals(about)? TALK_ABOUT_SERVANTS : Integer.parseInt(about));
        } else if("true".equals(properties.getProperty("tasks"))) {
            Servant servant = Servant.getServant(npc);
            if(servant != null) {
                EditServantTasksQuestion question = new EditServantTasksQuestion(responder, servant);
                question.sendQuestion();
            }
        } else if(npc == null) {
            byte gender = "male".equals(properties.getProperty("gender"))? MiscConstants.SEX_MALE : MiscConstants.SEX_FEMALE;
            String name = properties.getProperty("name");

            boolean contractIsActivated = false;
            long servantId = -1L, face = 0L, money = 0L;
            float posx = -1.0f, posy = -1.0f, posz = 0.0f, rotation = 0.0f;
            int status = 0;
            try(Connection con = ModSupportDb.getModSupportDb()) {
                try(PreparedStatement ps = con.prepareStatement("SELECT NPCID,STATUS,FACE,POSX,POSY,POSZ,ROTATION,MONEY FROM SERVANTS WHERE CONTRACTID=?")) {
                    ps.setLong(1, contract.getWurmId());
                    try(ResultSet rs = ps.executeQuery()) {
                        if(rs.next()) {
                            servantId = rs.getLong(1);
                            status = rs.getInt(2);
                            face = rs.getLong(3);
                            posx = rs.getFloat(4);
                            posy = rs.getFloat(5);
                            posz = rs.getFloat(6);
                            rotation = rs.getFloat(7);
                            money = rs.getLong(8);
                            contractIsActivated = true;
                        }
                    }
                } catch(SQLException e) { }
                if(!contractIsActivated) {
                    try(PreparedStatement ps2 = con.prepareStatement("INSERT INTO SERVANTS (CONTRACTID,NPCID,STATUS,FACE,POSX,POSY,POSZ,ROTATION,MONEY) VALUES(?,?,?,?,?,?,?,?,?)")) {
                        ps2.setLong(1, contract.getWurmId());
                        ps2.setLong(2, servantId);
                        ps2.setInt(3, status);
                        ps2.setLong(4, face);
                        ps2.setFloat(5, posx);
                        ps2.setFloat(6, posy);
                        ps2.setFloat(7, posz);
                        ps2.setFloat(8, rotation);
                        ps2.setLong(9, money);
                        ps2.execute();
                        contract.setCreator(responder.getName());
                        responder.getCommunicator().sendNormalServerMessage("The servant contract has now been signed, and is ready for summoning a servant.");
                    }
                }
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "Failed to update servant contract with database.", e);
                return;
            }

            if(name.length() < 2) {
                name = Servant.getName(gender);
            }

            posx = responder.getPosX();
            posy = responder.getPosY();
            rotation = responder.getStatus().getRotation();
            responder.getCommunicator().sendNormalServerMessage("You summon " + name + ".");
            npc = Servant.summonServant(responder, contract, name, CreatureTemplateIds.NPC_HUMAN_CID, posx, posy, rotation, gender, status);
        } else {
            boolean dismiss = "true".equals(properties.getProperty("dismiss"));
            if(dismiss) {
                if(contract.isTraded()) {
                    responder.getCommunicator().sendNormalServerMessage("You cannot dismiss a servant while the contract is part of a trade.");
                } else {
                    Servant.dismissServant(responder, npc, true);
                    responder.getCommunicator().sendNormalServerMessage("You dismiss " + npc.getName() + ".");
                }
            }
        }
    }
}
