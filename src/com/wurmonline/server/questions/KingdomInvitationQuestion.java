package com.wurmonline.server.questions;

import com.wurmonline.server.Items;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.villages.NoSuchVillageException;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import net.spirangle.awakening.AwakeningConstants;
import net.spirangle.awakening.creatures.CreatureAI;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;

import static net.spirangle.awakening.AwakeningConstants.KINGDOM_INVITATION_QUESTION_TYPE;


public class KingdomInvitationQuestion extends Question {
    private final Item diplomaticPassport;

    public KingdomInvitationQuestion(Player responder, Item diplomaticPassport) {
        super(responder, "Kingdom Invitation", "", KINGDOM_INVITATION_QUESTION_TYPE, responder.getWurmId());
        this.diplomaticPassport = diplomaticPassport;
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        StringBuilder bml = new StringBuilder();
        int w = 450;
        int h = 350;
        String name = StringUtils.bmlString(responder.getName());
        PlayerData pd = PlayersData.getInstance().get(responder);
        if(pd.diplomaticPassportLastUse > System.currentTimeMillis() - AwakeningConstants.DAY_AS_MILLIS) {
            responder.getCommunicator().sendAlertServerMessage("You have already used a diplomatic passport recently, and need to wait a bit longer to activate another.");
            return;
        }
        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Kingdom invitation for ").append(name).append(":\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='").append(getId()).append("'};\n")
           .append("   text{text=''}\n")
           .append("   text{text='When activating your diplomatic passport, you will gain diplomatic immunity to one selected capital for the duration of eight Wurm hours (one real hour). No tower guards will attack you, however village guards will if you move too near an enemy village. For the duration of the invitation, you may enter the capital of the kingdom, but be careful, hostile players may still see you as an enemy and attack.'}\n")
           .append("   text{text=''}\n")
           .append("   text{text='Select capital:'}\n");
        for(Kingdom kingdom : Kingdoms.getAllKingdoms()) {
            if(kingdom.getId() != responder.getStatus().kingdom) {
                Village capital = Villages.getFirstPermanentVillageForKingdom(kingdom.getId());
                if(capital != null) {
                    bml.append("   radio{group='capital';id='" + capital.getId() + "';text='" + capital.getName() + "'}\n");
                }
            }
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
           .append("   button{id='submit';size='80,20';text='Activate'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {
        Player responder = (Player) getResponder();

        if("true".equals(properties.getProperty("cancel"))) return;

        byte capitalId = (byte) Integer.parseInt(properties.getProperty("capital"));
        try {
            Village capital = Villages.getVillage(capitalId);
            PlayerData pd = PlayersData.getInstance().get(responder);
            long time = 60L;
            CreatureAI.addVillageInvitation(responder, capital, time);
            responder.getCommunicator().sendSafeServerMessage("You now have diplomatic immunity to " + capital.getName() + " capital, for " + time + " minutes.");
            pd.diplomaticPassportLastUse = System.currentTimeMillis();
            Items.destroyItem(diplomaticPassport.getWurmId());
        } catch(NoSuchVillageException e) {
            responder.getCommunicator().sendAlertServerMessage("Please contact staff, the selected capital doesn't exist.");
        }
    }
}
