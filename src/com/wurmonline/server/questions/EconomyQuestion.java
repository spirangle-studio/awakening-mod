package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.economy.Economy;
import com.wurmonline.server.economy.Shop;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.items.ItemTemplateFactory;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.kingdom.Kingdom;
import com.wurmonline.server.kingdom.Kingdoms;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.players.EconomyData;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.tasks.VendorTask;
import net.spirangle.awakening.tasks.VendorTask.Trade;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.ECONOMY_QUESTION_TYPE;


public class EconomyQuestion extends Question {

    private static final Logger logger = Logger.getLogger(EconomyQuestion.class.getName());

    public static final int SUBJECT_ECONOMY = 0;
    public static final int SUBJECT_TRANSACTIONS = 1;
    public static final int SUBJECT_VENDORS = 2;

    private final Player target;
    private final int subject;
    private Kingdom kingdom;

    public EconomyQuestion(Player responder, int subject) {
        this(responder, null, null, subject);
    }

    public EconomyQuestion(Player responder, Player target, int subject) {
        this(responder, target, null, subject);
    }

    public EconomyQuestion(Player responder, Kingdom kingdom, int subject) {
        this(responder, null, kingdom, subject);
    }

    public EconomyQuestion(Player responder, Player target, Kingdom kingdom, int subject) {
        super(responder, "Economy", "", ECONOMY_QUESTION_TYPE, responder.getWurmId());
        this.target = target;
        this.kingdom = kingdom;
        this.subject = subject;
    }

    private void sendQuestion(int subject) {
        Player responder = (Player) this.getResponder();
        EconomyQuestion eq = new EconomyQuestion(responder, target, kingdom, subject);
        eq.sendQuestion();
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        Player player = target != null && responder.getPower() >= MiscConstants.POWER_DEMIGOD? target : responder;
        String title = this.title;
        StringBuilder bml = new StringBuilder();
        int w = 450;
        int h = 370;
        int colw = 150;
        boolean resizable = false;
        if(subject == SUBJECT_ECONOMY) {
            String change;
            long money = player.getMoney();
            PlayerData pd = PlayersData.getInstance().get(player);
            EconomyData ed = pd.getEconomyData();
            bml.append("border{\n")
               .append(" null;\n")
               .append(" null;\n")
               .append(" scroll{horizontal='false';vertical='true';\n")
               .append("  varray{rescale='true';\n")
               .append("   passthrough{id='id';text='").append(getId()).append("'};\n");

            final Shop kingsMoney = Economy.getEconomy().getKingsShop();
            change = new Change(kingsMoney.getMoney()).getChangeString();
            bml.append("   harray{text{size='").append(colw).append(",15';text=\"Kingdom coffers:\"};label{text=\"").append(change).append("\"}}\n")
               .append("   text{text=''}\n");

            change = new Change(money).getChangeString();
            bml.append("   text{type='bold';text=\"The economy of ").append(StringUtils.bmlString(player.getName())).append(":\"}\n")
               .append("   harray{text{size='").append(colw).append(",15';text=\"Bank balance:\"};label{text=\"").append(change).append("\"}}\n");

            if(ed.getTotalIncome() > 0L) {
                change = new Change(ed.getTotalIncome()).getChangeString();
                bml.append("   harray{text{size='").append(colw).append(",15';text=\"Income (total):\"};label{text=\"").append(change).append("\"}}\n");
                change = new Change(ed.getTotalTax()).getChangeString();
                bml.append("   harray{text{size='").append(colw).append(",15';text=\"Tax paid (total):\"};label{text=\"").append(change).append("\"}}\n");
                change = new Change(ed.getYearIncome()).getChangeString();
                bml.append("   harray{text{size='").append(colw).append(",15';text=\"Income (current year):\"};label{text=\"").append(change).append("\"}}\n");
                change = new Change(ed.getYearTax()).getChangeString();
                bml.append("   harray{text{size='").append(colw).append(",15';text=\"Tax paid (current year):\"};label{text=\"").append(change).append("\"}}\n");
            }

            bml.append("  }\n")
               .append(" };\n")
               .append(" null;\n")
               .append(" right{\n")
               .append("  harray{\n")
               .append("   button{id='transactions';size='100,20';text='Transactions'}\n")
               .append("   button{id='vendors';size='80,20';text='Vendors'}\n")
               .append("   button{id='submit';size='80,20';text='Ok'}\n")
               .append("  }\n")
               .append(" }\n")
               .append("}\n");

        } else if(subject == SUBJECT_TRANSACTIONS) {
            title = player.getNamePossessive() + " transactions";
            w = 550;
            bml.append("border{\n")
               .append(" null;\n")
               .append(" null;\n")
               .append(" varray{rescale='true';\n")
               .append("  passthrough{id='id';text='").append(getId()).append("'};\n");

            bml.append("  tree{id=\"t1\";cols=\"5\";showheader=\"true\";height=\"312\"")
               .append("col{text=\"QL\";width=\"45\"};col{text=\"Type\";width=\"45\"};col{text=\"Date\";width=\"75\"};col{text=\"\";width=\"25\"};col{text=\"Price\";width=\"64\"};\n");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try(Connection con = ModSupportDb.getModSupportDb();
                PreparedStatement ps = con.prepareStatement("SELECT ID,TEMPLATEID,TYPE,CATEGORY,DATE,QUALITYLEVEL,AMOUNT,MATERIAL,AUX,PRICE,TAX FROM TRANSACTIONS " +
                                                            "WHERE WURMID=? ORDER BY DATE DESC LIMIT 50")) {
                ps.setLong(1, player.getWurmId());
                try(ResultSet rs = ps.executeQuery()) {
                    for(int i = 0; rs.next(); ++i) {
                        try {
                            long id = rs.getLong(1);
                            ItemTemplate template = ItemTemplateFactory.getInstance().getTemplate(rs.getInt(2));
//                            int type = rs.getInt(3);
//                            int category = rs.getInt(4);
                            Date date = new Date(rs.getLong(5));
                            float ql = rs.getFloat(6);
                            float amount = rs.getFloat(7);
                            byte material = rs.getByte(8);
                            byte aux = rs.getByte(9);
                            long price = rs.getLong(10);
                            if(price == 0L) continue;
                            StringBuilder sb = new StringBuilder();
                            sb.append(net.spirangle.awakening.items.Items.getName(template, amount >= 2.0f, material));
                            if(amount >= 2.0f) sb.append(" (").append((int) amount).append("x)");
                            String name = sb.toString();
                            String type = price < 0L? "buy" : "sell";
                            String balance = price > 0L? "+" : "-";
                            if(price < 0L) price = -price;
                            bml.append("   row{id=\"e").append(i).append("\";hover=\"").append(name).append("\";name=\"").append(name).append("\";rarity=\"0\";children=\"0\";")
                               .append("col{text=\"").append(StringUtils.decimalFormat.format(ql)).append("\"};")
                               .append("col{text=\"").append(type).append("\"};")
                               .append("col{text=\"").append(sdf.format(date)).append("\"};")
                               .append("col{text=\"").append(balance).append("\"};")
                               .append("col{text=\"").append(StringUtils.bigdecimalFormat.format((double) price * 0.0001d)).append("\"}}\n");
                        } catch(NoSuchTemplateException e) { }
                    }
                }
            } catch(SQLException e) {
                logger.log(Level.SEVERE, "Failed to load market data: " + e.getMessage(), e);
            }

            bml.append("  }\n")
               .append(" };\n")
               .append(" null;\n")
               .append(" right{\n")
               .append("  harray{\n")
               .append("   button{id='submit';size='80,20';text='Ok'}\n")
               .append("  }\n")
               .append(" }\n")
               .append("}\n");

        } else if(subject == SUBJECT_VENDORS) {
            w = 450;
            h = 650;
            resizable = true;
            if(kingdom == null) kingdom = Kingdoms.getKingdomOrNull(player.getKingdomId());
            bml.append("border{\n")
               .append(" varray{rescale='true';\n")
               .append("  text{type='bold';text=\"").append(StringUtils.bmlString(kingdom.getName())).append(" vendor budgets:\"}\n")
               .append(" };\n")
               .append(" null;\n")
               .append(" scroll{horizontal='false';vertical='true';\n")
               .append("  varray{rescale='true';\n")
               .append("   passthrough{id='id';text='").append(getId()).append("'};\n")
               .append("   table{cols='4';\n")
               .append("    label{type='bold';text='Trade'};\n")
               .append("    label{type='bold';text='Budget'};\n")
               .append("    label{type='bold';text='Report'};\n")
               .append("    label{type='bold';text='Vendor'};\n");

            Map<String, Trade> vendorBudgets = VendorTask.getKingdomBudget(kingdom);
            if(vendorBudgets != null && !vendorBudgets.isEmpty()) {
                vendorBudgets.entrySet().forEach(e -> {
                    Trade trade = e.getValue();
                    String budget = new Change(trade.category.budget).getChangeString();
                    int report = (int) (((float) trade.money / (float) trade.category.budget) * 100.0f);
                    String vendor = trade.vendor.servant.npc.getName();
                    bml.append("    label{text=\"").append(trade.category.title).append("\"};\n")
                       .append("    label{text=\"").append(budget).append("\"};\n")
                       .append("    label{text=\"").append(report).append("%\"};\n")
                       .append("    label{text=\"").append(vendor).append("\"};\n");
                });
            }

            bml.append("   }\n")
               .append("  }\n")
               .append(" };\n")
               .append(" null;\n")
               .append(" right{\n")
               .append("  harray{\n")
               .append("   button{id='print';size='80,20';text='Print';hover='Print reports to Event log'}\n")
               .append("   button{id='vendors';size='80,20';text='Update'}\n")
               .append("   button{id='submit';size='80,20';text='Ok'}\n")
               .append("  }\n")
               .append(" }\n")
               .append("}\n");
        }
        responder.getCommunicator().sendBml(w, h, resizable, true, bml.toString(), 200, 200, 200, title);
    }

    @Override
    public void answer(Properties properties) {
        if("true".equals(properties.getProperty("transactions"))) {
            sendQuestion(SUBJECT_TRANSACTIONS);
        } else if("true".equals(properties.getProperty("vendors"))) {
            sendQuestion(SUBJECT_VENDORS);
        } else if("true".equals(properties.getProperty("print"))) {
            Player responder = (Player) this.getResponder();
            VendorTask.sendKingdomBudget(responder, kingdom);
        }
    }
}
