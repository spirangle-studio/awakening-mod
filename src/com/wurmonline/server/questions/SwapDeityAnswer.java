package com.wurmonline.server.questions;

import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.players.Player;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("unused")
public class SwapDeityAnswer {

    private static final Logger logger = Logger.getLogger(SwapDeityAnswer.class.getName());

    public static void answer(final SwapDeityQuestion question, final Properties answers) {
        question.setAnswer(answers);
        final String did = answers.getProperty("did");
        final int deityid = Integer.parseInt(did);
        final Player player = (Player) question.getResponder();
        if(deityid != question.playerDeity.getNumber()) {
            final Deity deity = Deities.getDeity(deityid);
            try {
                player.setDeity(deity);
                player.getCommunicator().sendNormalServerMessage("You are now a follower of " + deity.name + ".");
                player.setAlignment(deity.getAlignment() >= 0? 50.0f : -50.0f);
            } catch(IOException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }
}
