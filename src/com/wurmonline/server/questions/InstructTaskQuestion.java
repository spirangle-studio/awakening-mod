package com.wurmonline.server.questions;

import com.wurmonline.server.Items;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.tasks.Task;
import net.spirangle.awakening.tasks.Task.InstructTaskParams;
import net.spirangle.awakening.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import static com.wurmonline.server.questions.TalkToServantQuestion.*;
import static net.spirangle.awakening.AwakeningConstants.INSTRUCT_TASK_QUESTION_TYPE;


public class InstructTaskQuestion extends Question {
    private final Item taskPaper;
    private final Servant servant;
    private Task task;
    private final int subject;

    public InstructTaskQuestion(Creature responder, Item taskPaper, Servant servant) {
        this(responder, taskPaper, servant, null, TALK_ABOUT_NOTHING);
    }

    private InstructTaskQuestion(Creature responder, Item taskPaper, Servant servant, Task task, int subject) {
        super(responder, "Servant", "", INSTRUCT_TASK_QUESTION_TYPE, taskPaper.getWurmId());
        this.taskPaper = taskPaper;
        this.servant = servant;
        this.task = task;
        this.subject = subject;
    }

    private void sendQuestion(int subject) {
        Creature responder = this.getResponder();
        InstructTaskQuestion question = new InstructTaskQuestion(responder, taskPaper, servant, task, subject);
        question.sendQuestion();
    }

    @Override
    public void sendQuestion() {
        Creature responder = this.getResponder();
        if(subject >= TALK_ABOUT_SERVANTS) {
            sendTalkAboutServantsQuestion(responder, getId(), subject);
        } else if(task == null) {
            Creature npc = servant.npc;
            StringBuilder bml = new StringBuilder();
            int w = 450;
            int h = 350;
            InstructTaskParams params = new InstructTaskParams(responder, taskPaper, servant);

            bml.append("border{\n" +
                       " varray{rescale='true';\n" +
                       "  text{type='bold';text=\"Instructing task:\"}\n" +
                       " };\n" +
                       " null;\n" +
                       " scroll{horizontal='false';vertical='true';\n" +
                       "  varray{rescale='true';\n" +
                       "   passthrough{id='id';text='" + getId() + "'};\n" +
                       "   text{text=\"You have a written task instruction that can be given to a servant. The instruction is written in a general " +
                       "manner, and you will have to fill in the missing parts while talking to " + StringUtils.bmlString(npc.getName()) + ".\n\n" +
                       "Once the task has been given you won't get the paper back, and the instructions can't be changed.\n\nFirst you will have to " +
                       "decide what type of task it is:\"}\n");

            Task.getRegisteredTasks().forEach(entry -> {
                int id = entry.getKey();
                Task.RegisteredTask registeredTask = entry.getValue();
                try {
                    params.title = null;
                    params.tooltip = null;
                    String title = (String) registeredTask.getInstructTaskTitle.invoke(null, params);
                    if(title != null) {
                        bml.append("   radio{group='task';id='").append(id).append("';text=\"").append(title).append("\"");
                        if(params.tooltip != null) bml.append(";hover=\"").append(params.tooltip).append("\"");
                        bml.append("}\n");
                    }
                } catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { }
            });

            bml.append("   radio{group='task';id='0';selected='true';text=\"Do nothing\"}\n" +
                       "  }\n" +
                       " };\n" +
                       " null;\n" +
                       " right{\n" +
                       "  harray{\n" +
                       "   button{id='about';size='120,20';text='About Servants'}\n" +
                       "   text{size='" + (w - 6 - 280) + ",1';text=''}\n" +
                       "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='80,20';text='Continue'}\n" +
                       "  }\n" +
                       " }\n" +
                       "}\n");
            //         AwakeningMod.log("BML: "+bml.toString());
            responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
        } else {
            task.sendCreateTaskQuestion(this, taskPaper, servant);
        }
    }

    @Override
    public void answer(Properties properties) {
        Creature responder = this.getResponder();

        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("start"))) {
            task = null;
            sendQuestion(TALK_ABOUT_NOTHING);
        } else if("true".equals(properties.getProperty("about-back"))) {
            String to = properties.getProperty("about-to");
            sendQuestion(to == null? TALK_ABOUT_SERVANTS : Integer.parseInt(to));
        } else if(properties.containsKey("about")) {
            String about = properties.getProperty("about");
            sendQuestion("true".equals(about)? TALK_ABOUT_SERVANTS : Integer.parseInt(about));
        } else if(task == null) {
            int taskType = Integer.parseInt(properties.getProperty("task"));
            task = Task.createTask(responder, taskType, taskPaper, servant);
            if(task != null) sendQuestion(TALK_ABOUT_NOTHING);
        } else if(task.handleCreateTaskAnswer(this, taskPaper, servant, properties) && servant.addTask(task, true) && taskPaper != null) {
            Items.destroyItem(taskPaper.getWurmId());
            responder.getCommunicator().sendNormalServerMessage(servant.npc.getName() + " takes the paper, and commits the instruction to memory.");
        }
    }
}
