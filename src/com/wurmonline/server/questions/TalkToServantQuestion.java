package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Server;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.economy.Change;
import com.wurmonline.server.items.Item;
import net.spirangle.awakening.creatures.Servant;
import net.spirangle.awakening.tasks.Task;
import net.spirangle.awakening.util.StringUtils;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.TALK_TO_SERVANT_QUESTION_TYPE;


public class TalkToServantQuestion extends Question {

    private static final Logger logger = Logger.getLogger(TalkToServantQuestion.class.getName());

    public static final int TALK_ABOUT_NOTHING = 0;
    public static final int TALK_ABOUT_CURRENT_TASKS = 1;
    public static final int TALK_ABOUT_COLLECT_MONEY = 2;
    public static final int TALK_ABOUT_GOSSIP = 3;
    public static final int TALK_ABOUT_SERVANTS = 4;

    private final Item source;
    private final Servant servant;
    private int subject;
    private Task task;

    public TalkToServantQuestion(Creature responder, Item source, Servant servant) {
        this(responder, source, servant, TALK_ABOUT_NOTHING, null);
    }

    protected TalkToServantQuestion(Creature responder, Item source, Servant servant, int subject, Task task) {
        super(responder, servant.npc.getName(), "", TALK_TO_SERVANT_QUESTION_TYPE, servant.npc.getWurmId());
        this.source = source;
        this.servant = servant;
        this.subject = subject;
        this.task = task;
    }

    public Item getSourceItem() {
        return source;
    }

    private void sendQuestion(int subject, Task task) {
        Creature responder = this.getResponder();
        TalkToServantQuestion ttq = new TalkToServantQuestion(responder, source, servant, subject, task);
        ttq.sendQuestion();
    }

    @Override
    public void sendQuestion() {
        Creature responder = this.getResponder();
        int w = 350;
        int h = 350;
        if(subject >= TALK_ABOUT_SERVANTS) {
            sendTalkAboutServantsQuestion(responder, getId(), subject);
        } else if(task == null) {
            Creature npc = servant.npc;
            String name = StringUtils.bmlString(npc.getName());
            if(subject == TALK_ABOUT_NOTHING || subject == TALK_ABOUT_CURRENT_TASKS) {
                StringBuilder bml = new StringBuilder();
                bml.append("border{\n" +
                           " varray{rescale='true';\n" +
                           "  text{type='bold';text='Talking to " + name + ":'}\n" +
                           " };\n" +
                           " null;\n" +
                           " scroll{horizontal='false';vertical='true';\n" +
                           "  varray{rescale='true';\n" +
                           "   passthrough{id='id';text='" + getId() + "'};\n");
                if(subject == TALK_ABOUT_NOTHING) {
                    bml.append("   text{text=''}\n" +
                               "   text{text=\"You are talking to " + name + ", and " + npc.getHeSheItString() + " has some things to tell you. What would you like to talk about?\"}\n" +
                               "   text{text=''}\n");
                    if(servant.isEmployer(responder) || responder.getPower() >= MiscConstants.POWER_DEMIGOD)
                        bml.append("   radio{group='subject';id='" + TALK_ABOUT_CURRENT_TASKS + "';text=\"Current tasks\"}\n");
                    if(servant.isEmployer(responder) && servant.getMoney() > 0L)
                        bml.append("   radio{group='subject';id='" + TALK_ABOUT_COLLECT_MONEY + "';text=\"Collect money from sold wares\"}\n");
                    if(servant.tasks.size() >= 1) {
                        for(Task t : servant.tasks) {
                            String subject = t.getSubject(responder);
                            if(subject != null)
                                bml.append("   radio{group='subject';id='task-" + t.id + "';text=\"" + StringUtils.bmlString(subject) + "\"}\n");
                        }
                    }
                    bml.append("   radio{group='subject';id='" + TALK_ABOUT_GOSSIP + "';text=\"Village gossip\"}\n" +
                               "   radio{group='subject';id='" + TALK_ABOUT_NOTHING + "';text=\"Do nothing\";selected='true'}\n" +
                               "  }\n" +
                               " };\n" +
                               " null;\n" +
                               " right{\n" +
                               "  harray{\n" +
                               "   button{id='about';size='120,20';text='About Servants'}\n" +
                               "   text{size='" + (w - 6 - 280) + ",1';text=''}\n" +
                               "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                               "   button{id='submit';size='80,20';text='Continue'}\n" +
                               "  }\n" +
                               " }\n" +
                               "}\n");
                } else if(subject == TALK_ABOUT_CURRENT_TASKS) {
                    String salary = new Change(servant.getSalary()).getChangeString();
                    bml.append("   passthrough{id='subject';text='remove-tasks'};\n");
                    if((servant.status & Servant.PAID_BY_KING) == 0) {
                        bml.append("   text{text=''}\n" +
                                   "   text{type='italic';text=\"You are currently paying me a salary of " + salary + " per day.\"}\n");
                    }
                    bml.append("   text{text=''}\n" +
                               "   text{type='italic';text=\"Do you want me to stop doing any of the tasks I've been assigned?\"}\n" +
                               "   text{text=''}\n");

                    String subject;
                    for(Task t : servant.tasks) {
                        subject = t.getTitle(responder);
                        if(subject == null) subject = t.getClass().getName();
                        bml.append("   checkbox{id='remove-task-" + t.id + "';text=\"" + StringUtils.bmlString(subject) + "\"}\n");
                    }
                    bml.append("  }\n" +
                               " };\n" +
                               " null;\n" +
                               " right{\n" +
                               "  harray{\n" +
                               "   button{id='start';size='80,20';text='Back'}\n" +
                               "   text{size='" + (w - 6 - 240) + ",1';text=''}\n" +
                               "   button{id='cancel';size='80,20';text='Cancel'}\n" +
                               "   button{id='submit';size='80,20';text='Update'}\n" +
                               "  }\n" +
                               " }\n" +
                               "}\n");
                }
                responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
            }
        }
    }

    public static void sendTalkAboutServantsQuestion(Creature responder, int id, int subject) {
        StringBuilder bml = new StringBuilder();
        String header = null;
        String content = null;
        int w = 450;
        int h = 450;
        Task.RegisteredTask registeredTask = Task.getRegisteredTask(subject);
        if(subject != TALK_ABOUT_SERVANTS && registeredTask != null) {
            Task.AboutTaskParams params = new Task.AboutTaskParams(responder, false, true);
            try {
                registeredTask.getAboutTaskTitle.invoke(null, params);
                header = params.header;
                if(params.description != null) {
                    String sb = "   text{text=\"" + params.description + "\"}\n" +
                                "   text{text=''}\n";
                    content = sb;
                }
            } catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { }
        }
        if(header == null || content == null) {
            header = "About Servants";
            StringBuilder sb = new StringBuilder();
            sb.append("   text{text=\"What are servants?\n\nOn Awakening they are a custom NPC which is summoned with a servant ")
              .append("contract. Such contracts can be bought from a trader in one of the capital cities of each kingdom. Once summoned ")
              .append("a servant may be assigned a number of tasks, the number depends on the QL of the task instructions given. To create ")
              .append("a task instruction, you need paper and ink or dye, and a reed pen. Once you have a task instruction, activate it and ")
              .append("give to the servant, which opens a dialog with which task to perform and options for specific instructions.\n\n")
              .append("A summoned servant expects a salary on a daily basis (Wurm days, i.e. every 3 hours real time). The salary depends ")
              .append("on what tasks and the number of tasks, each task costs differently.\n\n")
              .append("New tasks are added continuously, and some are only available for GMs, but these can players currently assign to their servants:\"}\n");

            Task.AboutTaskParams params = new Task.AboutTaskParams(responder, true, false);
            int i = 0;
            for(Map.Entry<Integer, Task.RegisteredTask> entry : Task.getRegisteredTasks()) {
                int rtid = entry.getKey();
                Task.RegisteredTask rt = entry.getValue();
                try {
                    params.title = null;
                    String title = (String) rt.getAboutTaskTitle.invoke(null, params);
                    if(title != null) {
                        sb.append("   radio{group='about';id='").append(rtid).append("';");
                        if(i == 0) sb.append("selected='true';");
                        sb.append("text=\"").append(title).append("\"}\n");
                        ++i;
                    }
                } catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException e) { }
            }

            sb.append("   text{text=''}\n")
              .append("   text{text=\"Servants will accept clothes from the contract owner. The clothes are worn automatically. They will also accept a ")
              .append("lantern, which lights up the place. Some tasks will make servants accept other items as well.\n\n")
              .append("To dismiss a servant, use the contract. When dismissed, all tasks are lost. If the servant is dismissed, the clothes or any items ")
              .append("in inventory are dropped to the ground. Should the servant stop getting paid, he or she leaves and any items worn or carried are taken as payment instead.\"}")
              .append("   text{text=''}\n");
            content = sb.toString();
        }
        bml.append("border{\n")
           .append(" center{size='450,1';\n")
           .append("  varray{\n")
           .append("   text{text=''}\n")
           .append("   header{color='127,127,127';text=\"")
           .append(header)
           .append("\"}\n")
           .append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='")
           .append(id)
           .append("'};\n")
           .append(content)
           .append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n");
        if(subject == TALK_ABOUT_SERVANTS) {
            bml.append("   button{id='start';size='80,20';text='Back'}\n")
               .append("   text{size='")
               .append((w - 6 - 240))
               .append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
               .append("   button{id='submit';size='80,20';text='Continue'}\n");
        } else {
            bml.append("   passthrough{id='about-to';text='")
               .append(TALK_ABOUT_SERVANTS)
               .append("'}\n")
               .append("   button{id='about-back';size='80,20';text='Back'}\n")
               .append("   text{size='")
               .append((w - 6 - 160))
               .append(",1';text=''}\n")
               .append("   button{id='cancel';size='80,20';text='Cancel'}\n");
        }
        bml.append("  }\n")
           .append(" }\n")
           .append("}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, "Servants");
    }

    @Override
    public void answer(Properties properties) {
        Creature responder = this.getResponder();
        Creature npc = servant.npc;
        String name = npc.getName();

        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("start"))) {
            sendQuestion(TALK_ABOUT_NOTHING, null);
        } else if("true".equals(properties.getProperty("about-back"))) {
            String to = properties.getProperty("about-to");
            sendQuestion(to == null? TALK_ABOUT_SERVANTS : Integer.parseInt(to), null);
        } else if(properties.containsKey("about")) {
            String about = properties.getProperty("about");
            sendQuestion("true".equals(about)? TALK_ABOUT_SERVANTS : Integer.parseInt(about), null);
        } else if(properties.containsKey("subject")) {
            String sub = properties.getProperty("subject");
            if(sub.startsWith("task-")) {
                long id = Long.parseLong(sub.substring(5));
                for(Task t : servant.tasks) {
                    if(t.id == id) {
                        task = t;
                        task.handleTalkToAnswer(this, properties);
                        break;
                    }
                }
            } else if("remove-tasks".equals(sub)) {
                if(servant.isEmployer(responder) || responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
                    int n = 0;
                    StringBuilder in = new StringBuilder();
                    Task[] tarr = servant.tasks.toArray(new Task[servant.tasks.size()]);
                    for(Task t : tarr) {
                        if("true".equals(properties.getProperty("remove-task-" + t.id))) {
                            ++n;
                            if(in.length() > 0) in.append(',');
                            in.append(t.id);
                            servant.tasks.remove(t);
                            servant.salary = -1L;
                            t.delete();
                        }
                    }
                    try(Connection con = ModSupportDb.getModSupportDb();
                        Statement st = con.createStatement()) {
                        st.execute("DELETE FROM TASKS WHERE TASKID IN (" + in + ")");
                    } catch(SQLException e) {
                        logger.log(Level.SEVERE, "Failed to delete servant task data.", e);
                    }
                    responder.getCommunicator().sendNormalServerMessage(name + " stops doing " + n + " tasks.");
                    sendQuestion(TALK_ABOUT_CURRENT_TASKS, null);
                }
            } else {
                subject = Integer.parseInt(sub);
                task = null;
                switch(subject) {
                    case TALK_ABOUT_NOTHING:
                        return;
                    case TALK_ABOUT_CURRENT_TASKS:
                        if(!servant.isEmployer(responder) && responder.getPower() < MiscConstants.POWER_DEMIGOD) {
                            responder.getCommunicator().sendNormalServerMessage("You're not " + name + "'s employer.");
                        } else if(servant.tasks.size() == 0) {
                            responder.getCommunicator().sendNormalServerMessage(name + " just shrugs, " + npc.getHeSheItString() + " doesn't have any tasks at the moment.");
                        } else {
                            sendQuestion(subject, task);
                        }
                        break;
                    case TALK_ABOUT_COLLECT_MONEY:
                        if(servant.isEmployer(responder)) {
                            servant.collectMoney();
                        }
                        break;
                    case TALK_ABOUT_GOSSIP: {
                        int n = villageGossipGeneral.length;
                        String[] kingdomGossip = null;
                        switch(npc.getKingdomId()) {
                            case 1:
                                kingdomGossip = villageGossipTirvalon;
                                break;
                            case 2:
                                kingdomGossip = villageGossipCellimdar;
                                break;
                            case 3:
                                kingdomGossip = villageGossipShodroq;
                                break;
                            case 5:
                                kingdomGossip = villageGossipThekandre;
                                break;
                        }
                        if(kingdomGossip != null) n += kingdomGossip.length;
                        n = Server.rand.nextInt(n);
                        String gossip = n < villageGossipGeneral.length? villageGossipGeneral[n] : kingdomGossip[n - villageGossipGeneral.length];
                        Server.getInstance().broadCastAction(servant.npc.getNameWithGenus() + " gossips: " + gossip, servant.npc, 2);
                        break;
                    }
                }
            }
        }
    }

    public static final String[] villageGossipGeneral = {
            "I miss moments like this more than anything.",
            "Many local farmers have been saying their sheep and pigs have been stolen or worse, killed.",
            "Someone attempted to defile our temple. I bet it was those damned goblins.",
            "There is a cave to the North. All of the entrances are trapped. They must be protecting something. Who are they you ask? Go find out for yourself.",
            "Hey! I was having a private conversation with myself here. Why don't you mind your own business?",
            "I'm going to keep drinking until you start looking good.",
            "The food at the inn is absolutely delicious. It must be their oven.",
            "I hear there is an abandoned mine northeast of here. Rumor has it the miners hit a passage that they shouldn't have. They never returned. I bet they stumbled upon a Goblin City.",
            "Just you wait and see. Before you know it, there will be an Auction House so that trade amongst all of the empires can take place freely, and more importantly, safely.",
            "Beware the treachery from within. Be it the village, or your own party of adventurers.",
            "Dragon's breath! There is a shortage of fish in town.",
            "Just because you don't see a use for something or someone, doesn't make something or someone useless.",
            "You think I'm being paranoid but the truth is I'm worth a lot more to them dead than alive.",
            "What's in that bag and why are you hiding it here?",
            "Just give me my cut of the copper and I will be on my way. Just don't tell anyone you saw me here.",
            "For a Silver Piece, I won't tell anyone I saw you here. For two, I'll even point you in the general direction of safety.",
            "Don't worry. Sometimes the wrong things must be done for the right reasons.",
            "Rumor has it the undead are about. It's been a very long time since anyone has uttered even a word pertaining to the lifeless walkers.",
            "Look for the bookcase, in the last room, at the bottom of the dungeon. Top shelf. You will find it there.",
            "To become an Adept requires an incredible amount of dedication; more than I can describe. Are you sure you are up to the task?",
            };

    public static final String[] villageGossipTirvalon = {
            "The village idiot isn't as dumb as he pretends to be.",
            "Rumor has it a secluded tribe of hags live near the old merchant road to the East. Be careful if you find yourself on the road building project.",
            "You should never bury your dead. You should burn 'em and keep them in urns. If your body is not burned within six days time it rises. Make sure you got a source of fire handy, just in case.",
            "War is upon us, so be wary if wandering in the forest by yourself. Shades have been sighted in every direction.",
            "If a deer chooses to grace you with the presence, you are blessed my friend.",
            "Remember the teachings of the ancients, if harm is done to a deer, 5 years bad luck will follow you.",
            };

    public static final String[] villageGossipCellimdar = {
            "War is upon us, so be wary if wandering in the forest by yourself. Shades have been sighted in every direction.",
            };

    public static final String[] villageGossipShodroq = {
            "There is a person of remarkable esteem trapped in the tower dungeon.",
            "You should never bury your dead. You should burn 'em and keep them in urns. If your body is not burned within six days time it rises. Make sure you got a source of fire handy, just in case.",
            };

    public static final String[] villageGossipThekandre = {
            "There is a cave to the North. All of the entrances are trapped. They must be protecting something. Who are they you ask? Go find out for yourself.",
            "I understand everyone makes mistakes. But yours might have just cost Thekandre greatly.",
            };
}
