package com.wurmonline.server.questions;

import com.wurmonline.server.LoginHandler;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.Players;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.players.Character;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;
import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.CHARACTER_QUESTION_TYPE;


public class CharacterQuestion extends Question {

    private static final Logger logger = Logger.getLogger(CharacterQuestion.class.getName());

    private Player target;
    private final String name;

    public CharacterQuestion(Player responder) {
        this(responder, null);
    }

    public CharacterQuestion(Player responder, Player target) {
        this(responder, target, null);
    }

    public CharacterQuestion(Player responder, Player target, String name) {
        super(responder, "Character", "", CHARACTER_QUESTION_TYPE, responder.getWurmId());
        this.target = target;
        if(name != null) name = LoginHandler.raiseFirstLetter(name);
        this.name = name;
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        Player player = target == null || responder.getWurmId() == target.getWurmId()? responder : target;
        Character character;
        if(name != null && !name.equals(player.getName())) {
            target = Players.getInstance().getPlayerOrNull(name);
            if(target == null || target.getPower() > responder.getPower()) {
                responder.getCommunicator().sendNormalServerMessage("No player found by the name " + name + ".");
                return;
            }
            player = target;
            character = PlayersData.getInstance().getCharacter(target);
        } else {
            character = PlayersData.getInstance().getCharacter(player);
        }
        StringBuilder bml = new StringBuilder();
        int w = 450;
        int h = 450;
        String name = StringUtils.bmlString(player.getName());
        String spouseName = "";
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"The character of " + name + ":\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + getId() + "'};\n");

        if(character.getName() != null) {
            name = StringUtils.bmlString(character.getName());
        }
        if(target == null || responder.getWurmId() == target.getWurmId() || responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
            if(character.hasSpouse()) {
                PlayerData spousePd = PlayersData.getInstance().get(character.getSpouse());
                spouseName = StringUtils.bmlString(spousePd.name);
            }
            bml.append("   text{text=''}\n" +
                       "   harray{\n" +
                       "    text{type='bold';size='80,17';text=\"Name: \";hover=\"Your character's name, will show in the roleplay chat.\"};\n" +
                       "    input{id='name';size='250,15';maxchars='50';text=\"" + name + "\"};\n" +
                       "   }\n" +
                       "   harray{\n" +
                       "    text{type='bold';size='80,17';text=\"Spouse: \";hover=\"The spouse of your character. Enter the player name, who need to be online. A request will then be sent.\"};\n" +
                       "    input{id='spouse';size='250,15';maxchars='50';text=\"" + spouseName + "\"};\n" +
                       "   }\n" +
                       "   harray{\n" +
                       "    text{type='bold';size='80,17';text=\"Mother: \";hover=\"Currently not implemented.\"};\n" +
                       "    label{text='-'};\n" +
                       "   }\n" +
                       "   harray{\n" +
                       "    text{type='bold';size='80,17';text=\"Father: \";hover=\"Currently not implemented.\"};\n" +
                       "    label{text='-'}\n" +
                       "   }\n" +
                       "   text{text=''}\n" +
                       "   text{type='bold';text=\"Personality:\"}\n" +
                       "   input{id='personality';bgcolor='100,100,100';maxchars='1024';maxlines='5';text=\"" + StringUtils.bmlString(character.getPersonality()) + "\"}\n" +
                       "   text{text=''}\n" +
                       "   text{type='bold';text=\"Background history:\"}\n" +
                       "   input{id='background';bgcolor='100,100,100';maxchars='1024';maxlines='5';text=\"" + StringUtils.bmlString(character.getBackground()) + "\"}\n" +
                       "   text{text=''}\n" +
                       "   text{type='italic';text=\"Your character sheet is visible to other players when they right click your body, and select the character option. " +
                       "This feature can be turned off in your player settings.\"}\n");

        } else {
            w = 350;
            if(character.hasSpouse()) {
                PlayerData spousePd = PlayersData.getInstance().get(character.getSpouse());
                Character spouseCharacter = spousePd.getCharacter();
                if(spouseCharacter.getName() == null || spousePd.name.equals(spouseCharacter.getName())) {
                    spouseName = StringUtils.bmlString(spousePd.name);
                } else {
                    spouseName = StringUtils.bmlString(spouseCharacter.getName() + " (" + spousePd.name + ")");
                }
            }
            bml.append("   text{text=''}\n" +
                       "   table{cols='2';\n" +
                       "    label{type='bold';text='Name: '};\n" +
                       "    label{text=\"" + name + "\"};\n");
            if(spouseName.length() > 0) {
                bml.append("    label{type='bold';text='Spouse: '};\n" +
                           "    label{text=\"" + spouseName + "\"};\n");
            }
            bml.append("    label{type='bold';text='Mother: '};\n" +
                       "    label{text=\"-\"};\n" +
                       "    label{type='bold';text='Father: '};\n" +
                       "    label{text=\"-\"};\n" +
                       "   }\n" +
                       "   text{text=''}\n" +
                       "   text{type='bold';text='Personality:'};\n" +
                       "   text{text=\"" + StringUtils.bmlString(character.getPersonality()) + "\"}\n" +
                       "   text{text=''}\n" +
                       "   text{type='bold';text='Background history:'};\n" +
                       "   text{text=\"" + StringUtils.bmlString(character.getBackground()) + "\"}\n");
        }

        bml.append("  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n");
        if(target == null || responder.getWurmId() == target.getWurmId() || responder.getPower() >= MiscConstants.POWER_DEMIGOD) {
            bml.append("   button{id='cancel';size='80,20';text='Cancel'}\n" +
                       "   button{id='submit';size='80,20';text='Save'}\n");
        } else {
            bml.append("   button{id='submit';size='80,20';text='Close'}\n");
        }
        bml.append("  }\n" +
                   " }\n" +
                   "}\n");

        //      logger.info("BML: "+bml.toString());
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {
        Player responder = (Player) getResponder();
        if(target != null && responder.getWurmId() != target.getWurmId() && responder.getPower() < MiscConstants.POWER_DEMIGOD)
            return;

        if("true".equals(properties.getProperty("cancel"))) return;

        boolean changed = false;
        Player player = target != null? target : responder;
        Character character = PlayersData.getInstance().getCharacter(player);
        if(properties.containsKey("name")) {
            String name = properties.getProperty("name").trim();
            if(player.getName().equals(name)) name = null;
            logger.info("Set character name to: " + name);
            changed = character.setName(name) || changed;
        }
        if(properties.containsKey("spouse")) {
            String spouseName = properties.getProperty("spouse").trim();
            logger.info("Set character spouse to: " + spouseName);
            if(spouseName.length() > 0) {
                Player spouse = Players.getInstance().getPlayerOrNull(spouseName);
                if(spouse == null) {
                    responder.getCommunicator().sendNormalServerMessage("There is no player online named " + spouseName + ".");
                } else if(!character.isSpouse(spouse)) {
                    responder.getCommunicator().sendNormalServerMessage("A spouse request has been sent to " + spouse.getName() + ".");
                    SpouseQuestion question = new SpouseQuestion(spouse, player);
                    question.sendQuestion();
                }
            } else if(character.hasSpouse()) {
                changed = character.setSpouse(null) || changed;
            }
        }
        if(properties.containsKey("personality")) {
            String personality = properties.getProperty("personality").trim();
            logger.info("Set character personality to: " + personality);
            changed = character.setPersonality(personality) || changed;
        }
        if(properties.containsKey("background")) {
            String background = properties.getProperty("background").trim();
            logger.info("Set character background to: " + background);
            changed = character.setBackground(background) || changed;
        }
        if(changed) {
            responder.getCommunicator().sendNormalServerMessage((player == responder? "Your character" : "The character of " + target.getName()) + " has been updated.");
        }
    }
}
