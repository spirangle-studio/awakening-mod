package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.players.Character;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.Cache;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;

import static net.spirangle.awakening.AwakeningConstants.SPOUSE_QUESTION_TYPE;


public class SpouseQuestion extends Question {
    private final Player spouse;

    public SpouseQuestion(Player responder, Player spouse) {
        super(responder, "Spouse request", "", SPOUSE_QUESTION_TYPE, responder.getWurmId());
        this.spouse = spouse;
    }

    @Override
    public void sendQuestion() {
        if(this.spouse == null) return;
        Player responder = (Player) getResponder();
        Object reject = Cache.getInstance().get("spouse.reject." + spouse.getWurmId() + "-" + responder.getWurmId());
        if(reject != null) {
            spouse.getCommunicator().sendNormalServerMessage(responder.getName() + " rejected your spousal request.");
            return;
        }

        StringBuilder bml = new StringBuilder();
        int w = 300;
        int h = 150;
        String name = StringUtils.bmlString(responder.getName());
        String spouseName = StringUtils.bmlString(spouse.getName());
        bml.append("border{\n" +
                   " null;\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + getId() + "'};\n" +
                   "   text{text=''}\n" +
                   "   text{text=\"" + spouseName + " is asking to be your " + getSpouseType(spouse) + ", do you agree to this request?\"}\n" +
                   "   radio{group='accept';id='yes';text='Yes'}\n" +
                   "   radio{group='accept';id='no';text='No';selected='true'}\n" +
                   "  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='submit';size='80,20';text='Reply'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {
        Player responder = (Player) getResponder();
        if(spouse == null) return;
        if("yes".equals(properties.getProperty("accept"))) {
            Character character = PlayersData.getInstance().getCharacter(responder);
            Character spouseCharacter = PlayersData.getInstance().getCharacter(spouse);
            character.setSpouse(spouse);
            spouseCharacter.setSpouse(responder);
            String spouseType1 = getSpouseType(responder);
            String spouseType2 = getSpouseType(spouse);
            responder.getCommunicator().sendNormalServerMessage("You and " + spouse.getName() + " are hereby pronounced " + spouseType1 + " and " + spouseType2 + ".");
            spouse.getCommunicator().sendNormalServerMessage("You and " + responder.getName() + " are hereby pronounced " + spouseType2 + " and " + spouseType1 + ".");
        } else if("no".equals(properties.getProperty("accept"))) {
            Cache.getInstance().put("spouse.reject." + spouse.getWurmId() + "-" + responder.getWurmId(), Boolean.FALSE, 3600L);
            responder.getCommunicator().sendNormalServerMessage("You rejected a spousal request from " + spouse.getName() + ".");
            spouse.getCommunicator().sendNormalServerMessage(responder.getName() + " rejected your spousal request.");
        }
    }

    private String getSpouseType(Player player) {
        return player.getSex() == MiscConstants.SEX_MALE? "husband" : "wife";
    }
}
