package com.wurmonline.server.questions;

import com.wurmonline.server.creatures.CreatureTemplate;
import com.wurmonline.server.creatures.CreatureTemplateFactory;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.players.CombatData;
import net.spirangle.awakening.players.PlayerData;
import net.spirangle.awakening.players.PlayersData;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;

import static net.spirangle.awakening.AwakeningConstants.COMBAT_QUESTION_TYPE;


public class CombatQuestion extends Question {

    Player target;

    public CombatQuestion(Player responder) {
        this(responder, null);
    }

    public CombatQuestion(Player responder, Player target) {
        super(responder, "Combat", "", COMBAT_QUESTION_TYPE, responder.getWurmId());
        this.target = target;
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        Player player = target == null? responder : target;
        PlayerData pd = PlayersData.getInstance().get(player);
        CombatData cd = pd.getCombatData();
        StringBuilder bml = new StringBuilder();
        int n = 0;
        int w = 450;
        int h = 450;
        int colw = 150;
        String name = StringUtils.bmlString(player.getName());
        bml.append("border{\n" +
                   " varray{rescale='true';\n" +
                   "  text{type='bold';text=\"Combat statistics for " + name + ":\"}\n" +
                   " };\n" +
                   " null;\n" +
                   " scroll{horizontal='false';vertical='true';\n" +
                   "  varray{rescale='true';\n" +
                   "   passthrough{id='id';text='" + getId() + "'};\n" +
                   "   text{text=''}\n");

        if(pd.getWarPoints() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"War Points:\"};label{text=\"" + pd.getWarPoints() + "\"}}\n" +
                       "   text{text=''}\n");
            ++n;
        }
        if(cd.getDeaths() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Deaths:\"};label{text=\"" + cd.getDeaths() + " times\"}}\n");
            ++n;
        }
        if(cd.getKills() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed creatures:\"};label{text=\"" + cd.getKills() + "\"}}\n");
            ++n;
        }
        if(cd.getKillPlayers() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed players:\"};label{text=\"" + cd.getKillPlayers() + "\"}}\n");
            ++n;
        }
        if(cd.getKillGuards() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed tower guards:\"};label{text=\"" + cd.getKillGuards() + "\"}}\n");
            ++n;
        }
        if(cd.getKillDragons() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed dragons:\"};label{text=\"" + cd.getKillDragons() + "\"}}\n");
            ++n;
        }
        if(cd.getKillUniques() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed uniques:\"};label{text=\"" + cd.getKillUniques() + "\"}}\n");
            ++n;
        }
        if(cd.getKillUndead() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Killed undead:\"};label{text=\"" + cd.getKillUndead() + "\"}}\n");
            ++n;
        }
        if(cd.getBuries() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Buried corpses:\"};label{text=\"" + cd.getBuries() + "\"}}\n");
            ++n;
        }
        if(cd.getKarma() > 0) {
            bml.append("   harray{text{size='" + colw + ",15';text=\"Gained karma:\"};label{text=\"" + cd.getKarma() + "\"}}\n");
            ++n;
        }

        if(cd.getSumKills() + cd.getSumKarma() > 0) {
            bml.append("   text{text=''}\n" +
                       "   text{type='bold';text=\"Since last login:\"}\n");
            if(cd.getSumKills() > 0) {
                bml.append("   harray{text{size='" + colw + ",15';text=\"Killed creatures:\"};label{text=\"" + cd.getSumKills() + "\"}}\n" +
                           "   harray{text{size='" + colw + ",15';text=\"Gained fighting skill:\"};label{text=\"" + StringUtils.bigdecimalFormat.format(cd.getFightSkillGain()) + "\"}}\n");
                n += 2;
                if(cd.getMaxFightSkillGainTemplateId() >= 0) {
                    try {
                        CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(cd.getMaxFightSkillGainTemplateId());
                        bml.append("   harray{text{size='" + colw + ",15';text=\"Top creature:\"};label{text=\"" + StringUtils.bmlString(template.getName()) + "\"}}\n" +
                                   "   harray{text{size='" + colw + ",15';text=\"Top fighting skill:\"};label{text=\"" + StringUtils.bigdecimalFormat.format(cd.getMaxFightSkillGain()) + "\"}}\n");
                        n += 2;
                    } catch(Exception e) { }
                }
            }
            if(cd.getSumKarma() > 0) {
                bml.append("   harray{text{size='" + colw + ",15';text=\"Gained karma:\"};label{text=\"" + cd.getSumKarma() + "\"}}\n");
                ++n;
                if(cd.getMaxKarma() > 0) {
                    bml.append("   harray{text{size='" + colw + ",15';text=\"Top karma:\"};label{text=\"" + cd.getMaxKarma() + "\"}}\n");
                    ++n;
                    if(cd.getMaxKarmaTemplateId() >= 0) {
                        try {
                            CreatureTemplate template = CreatureTemplateFactory.getInstance().getTemplate(cd.getMaxKarmaTemplateId());
                            bml.append("   harray{text{size='" + colw + ",15';text=\"Top karma creature:\"};label{text=\"" + StringUtils.bmlString(template.getName()) + "\"}}\n");
                            ++n;
                        } catch(Exception e) { }
                    }
                }
            }
        }

        if(cd.getInfestations() > 0) {
            bml.append("   text{text=''}\n" +
                       "   text{type='bold';text=\"Infestations:\"}\n" +
                       "   harray{text{size='" + colw + ",15';text=\"Participation:\"};label{text=\"" + cd.getInfestations() + "\"}}\n" +
                       "   harray{text{size='" + colw + ",15';text=\"Total Kills:\"};label{text=\"" + cd.getInfestationKills() + "\"}}\n" +
                       "   harray{text{size='" + colw + ",15';text=\"Total Score:\"};label{text=\"" + cd.getInfestationScore() + "\"}}\n" +
                       "   harray{text{size='" + colw + ",15';text=\"Last Kills:\"};label{text=\"" + cd.getLastInfestationKills() + "\"}}\n" +
                       "   harray{text{size='" + colw + ",15';text=\"Last Score:\"};label{text=\"" + cd.getLastInfestationScore() + "\"}}\n");
            n += cd.getSumKills();
        }

        if(n == 0) {
            bml.append("   text{text=\"" + (player == responder? "You've" : name + " has") + " been in no combat yet, there are no statistics to show.\"}\n");
        }

        bml.append("  }\n" +
                   " };\n" +
                   " null;\n" +
                   " right{\n" +
                   "  harray{\n" +
                   "   button{id='submit';size='80,20';text='Close'}\n" +
                   "  }\n" +
                   " }\n" +
                   "}\n");
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {

    }
}
