package com.wurmonline.server.questions;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import net.spirangle.awakening.tasks.Task;

import java.util.Properties;

import static com.wurmonline.server.questions.TalkToServantQuestion.TALK_ABOUT_NOTHING;
import static net.spirangle.awakening.AwakeningConstants.SERVANT_TASK_QUESTION_TYPE;


public class ServantTaskQuestion extends Question {
    private final Item source;
    private final Task task;
    private final int taskId;
    private final Object data;

    public ServantTaskQuestion(Creature responder, Item source, Task task, int taskId, Object data) {
        super(responder, "Servant", "", SERVANT_TASK_QUESTION_TYPE, responder.getWurmId());
        this.source = source;
        this.task = task;
        this.taskId = taskId;
        this.data = data;
    }

    public Item getSourceItem() {
        return source;
    }

    @Override
    public void sendQuestion() {
        task.sendTaskQuestion(this, taskId, data);
    }

    @Override
    public void answer(Properties properties) {
        if("true".equals(properties.getProperty("cancel"))) return;

        if("true".equals(properties.getProperty("start"))) {
            Creature responder = this.getResponder();
            TalkToServantQuestion ttq = new TalkToServantQuestion(responder, source, task.servant, TALK_ABOUT_NOTHING, null);
            ttq.sendQuestion();
        } else {
            task.handleTaskAnswer(this, taskId, data, properties);
        }
    }
}
