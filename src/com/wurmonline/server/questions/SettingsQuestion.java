package com.wurmonline.server.questions;

import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.NoSuchPlayerException;
import com.wurmonline.server.Players;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.players.PlayerInfo;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import net.spirangle.awakening.players.Character;
import net.spirangle.awakening.players.*;
import net.spirangle.awakening.util.StringUtils;

import java.util.Properties;

import static net.spirangle.awakening.AwakeningConstants.SETTINGS_QUESTION_TYPE;


public class SettingsQuestion extends Question {
    private Player target;
    private final PlayerInfo playerInfo;

    public SettingsQuestion(Player responder) {
        this(responder, responder);
    }

    public SettingsQuestion(Player responder, Player target) {
        super(responder, "Settings", "", SETTINGS_QUESTION_TYPE, responder.getWurmId());
        this.target = target;
        this.playerInfo = target.getSaveFile();
    }

    public SettingsQuestion(Player responder, PlayerInfo playerInfo) {
        super(responder, "Settings", "", SETTINGS_QUESTION_TYPE, responder.getWurmId());
        if(playerInfo == null) {
            this.target = responder;
            this.playerInfo = responder.getSaveFile();
        } else {
            try {
                this.target = Players.getInstance().getPlayer(playerInfo.wurmId);
            } catch(NoSuchPlayerException e) {
                this.target = null;
            }
            this.playerInfo = playerInfo;
        }
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        PlayerInfo pi = playerInfo != null && responder.getPower() >= MiscConstants.POWER_DEMIGOD? playerInfo : responder.getSaveFile();
        StringBuilder bml = new StringBuilder();
        int w = 450;
        int h = 350;
        String name = StringUtils.bmlString(pi.getName());
        PlayerData pd = PlayersData.getInstance().get(pi.wurmId);
        LeaderBoard lb = pd.getLeaderBoard();
        Character ch = pd.getCharacter();
        CombatData cd = pd.getCombatData();
        bml.append("border{\n")
           .append(" varray{rescale='true';\n")
           .append("  text{type='bold';text=\"Player settings for ").append(name).append(":\"}\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='").append(getId()).append("'};\n")
           .append("   text{text=''}\n");
        if(pd.isPvP())
            bml.append("   text{type='bold';text=\"You have the PvP settings checked\";hover=\"This means you are playing with PvP rules all the time. Only a GM can remove this setting.\"}\n");
        else
            bml.append("   checkbox{id='pvp';text=\"Play with PvP settings (cannot uncheck once set)\";hover=\"This setting will say to other players that you play by PvP rules all the time.\"}\n");
        bml.append("   checkbox{id='lbhide';").append(lb.isHidden()? "selected='true'" : "").append("text=\"Hide my name on leaderboards\";hover=\"Individual settings on each leaderboard overrides this setting.\"}\n")
           .append("   checkbox{id='chhide';").append(ch.isHidden()? "selected='true'" : "").append("text=\"Hide my character sheet\";hover=\"Remove the option for other players, of looking at your character when right clicking your body.\"}\n")
           .append("   checkbox{id='cdhide';").append(cd.isHidden()? "selected='true'" : "").append("text=\"Hide my combat statistics\";hover=\"Remove the option for other players, of looking at your combat stats when right clicking your body.\"}\n")
           .append("   text{text=''}\n")
           .append("   checkbox{id='crossKingdom';").append(pi.isFlagSet(MiscConstants.FLAG_CROSS_KINGDOM)? "selected='true'" : "").append("text=\"Permit cross kingdom PMs\";hover=\"This can also be set in the profile-settings.\"}\n")
           .append("   checkbox{id='muteworld';").append(pd.isWorldChannelMuted()? "selected='true'" : "").append("text=\"Mute World channel\";hover=\"This will mute messages in the world channel.\"}\n")
           .append("   checkbox{id='muterp';").append(pd.isRoleplayChannelMuted()? "selected='true'" : "").append("text=\"Mute roleplay channel\";hover=\"This will mute messages in the roleplay channel.\"}\n")
           .append("   text{text=''}\n")
           .append("   checkbox{id='hideBeams';").append(pd.hideBeams()? "selected='true'" : "").append("text=\"Hide beams at the great towers\";hover=\"Changing this setting requires a client restart for the beams to show/hide.\"}\n");
        Village village = Villages.getVillageForCreature(pi.wurmId);
        if(village != null && village.isMayor(pi.wurmId)) {
            bml.append("   text{text=''}\n")
               .append("   text{type='bold';text='Being mayor of a village, you have extra settings:'}\n")
               .append("   checkbox{id='deedfarm';").append(pd.isDeedFarming()? "selected='true'" : "").append("text=\"Farm growth only when tended\";hover=\"Farm tiles on deed will only grow when they are tended.\"}\n")
               .append("   checkbox{id='mycelGrowth';").append(pd.isAltarMycelGrowth()? "selected='true'" : "").append("text=\"Mycelium growth around black light altars\";hover=\"Mycelium will spread around dominant altars of black light deities, on the deed.\"}\n")
               .append("   harray{\n")
               .append("    text{size='120,17';text='Deed visibility:'};\n")
               .append("    dropdown{id='deedvis';size='250,15';default='").append(pd.getDeedVisibility()).append("';options=\"show deed on map viewer,hide name,hide name and border,hide terraforming to perimiter\";hover=\"How to display your deed on the map viewer, on the web page.\"}\n")
               .append("   }");
        }
        if(responder.getPower() >= MiscConstants.POWER_DEMIGOD && pi.wurmId != responder.getWurmId()) {
            bml.append("   text{text=''}\n")
               .append("   text{text=\"GM Settings:\"}\n")
               .append("   checkbox{id='pa';").append(pi.isPlayerAssistant()? "selected='true'" : "").append("text=\"Player Assistant\"}\n")
               .append("   checkbox{id='kingdomChange';").append(pd.isKingdomChangeLoginDone()? "selected='true'" : "").append("text=\"Kingdom Change Login\"}\n");
        }
        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='cancel';size='80,20';text='Cancel'}\n")
           .append("   button{id='submit';size='80,20';text='Save'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");

        //      AwakeningMod.log("BML: "+bml.toString());
        responder.getCommunicator().sendBml(w, h, false, true, bml.toString(), 200, 200, 200, this.title);
    }

    @Override
    public void answer(Properties properties) {
        Player responder = (Player) getResponder();

        if("true".equals(properties.getProperty("cancel"))) return;

        PlayerInfo pi = playerInfo != null && responder.getPower() >= MiscConstants.POWER_DEMIGOD? playerInfo : responder.getSaveFile();
        boolean changed = false;
        PlayerData pd = PlayersData.getInstance().get(pi.wurmId);
        LeaderBoard lb = pd.getLeaderBoard();
        if("true".equals(properties.getProperty("pvp", "false"))) changed = pd.setPvP(true) || changed;
        changed = lb.setHidden("true".equals(properties.getProperty("lbhide", "false"))) || changed;
        Character ch = pd.getCharacter();
        changed = ch.setHidden("true".equals(properties.getProperty("chhide", "false"))) || changed;
        CombatData cd = pd.getCombatData();
        changed = cd.setHidden("true".equals(properties.getProperty("cdhide", "false"))) || changed;
        changed = setPlayerFlag(pi, MiscConstants.FLAG_CROSS_KINGDOM, "true".equals(properties.getProperty("crossKingdom", "false"))) || changed;
        changed = pd.setMuteWorldChannel("true".equals(properties.getProperty("muteworld", "false"))) || changed;
        changed = pd.setMuteRoleplayChannel("true".equals(properties.getProperty("muterp", "false"))) || changed;
        changed = pd.setHideBeams("true".equals(properties.getProperty("hideBeams", "false"))) || changed;
        changed = pd.setNoDeedFarming(!"true".equals(properties.getProperty("deedfarm", "true"))) || changed;
        changed = pd.setAltarMycelGrowth("true".equals(properties.getProperty("mycelGrowth", "false"))) || changed;
        if(properties.containsKey("deedvis")) {
            changed = pd.setDeedVisibility(Integer.parseInt(properties.getProperty("deedvis"))) || changed;
        }
        if(changed) {
            responder.getCommunicator().sendNormalServerMessage((pi.wurmId == responder.getWurmId()? "Your settings" : "The settings of " + pi.getName()) + " have been updated.");
        }
        if(responder.getPower() >= MiscConstants.POWER_DEMIGOD && pi.wurmId != responder.getWurmId()) {
            boolean pa = "true".equals(properties.getProperty("pa", "false"));
            if(pa != pi.isPlayerAssistant()) {
                pi.setIsPlayerAssistant(pa);
                if(target != null) {
                    if(pa) {
                        target.getCommunicator().sendSafeServerMessage("You are now a Community Assistant. New players may ask you questions.");
                        target.getCommunicator().sendSafeServerMessage("The suggested way to approach new players is not to approach them directly but instead let them ask questions.");
                    } else {
                        target.getCommunicator().sendSafeServerMessage("You are no longer a Community Assistant.");
                    }
                }
            }
            boolean kingdomChange = "true".equals(properties.getProperty("kingdomChange", "false"));
            pd.setKingdomChangeLogin(kingdomChange);
        }
    }

    private boolean setPlayerFlag(PlayerInfo pi, int flag, boolean set) {
        if(pi.isFlagSet(flag) == set) return false;
        pi.setFlag(flag, set);
        return true;
    }
}
