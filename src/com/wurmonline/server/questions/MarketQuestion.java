package com.wurmonline.server.questions;

import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.players.Player;
import net.spirangle.awakening.tasks.MerchantTask;
import net.spirangle.awakening.tasks.VendorTask;

import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import static net.spirangle.awakening.AwakeningConstants.MARKET_QUESTION_TYPE;


public class MarketQuestion extends Question {

    public static class MarketInfo {
        public String traderName;
        public String trade;
        public String location;
        public String container;

        public MarketInfo(String traderName, String trade, String location, String container) {
            this.traderName = traderName;
            this.trade = trade;
            this.location = location;
            this.container = container;
        }
    }

    private final ItemTemplate template;

    public MarketQuestion(Player responder, ItemTemplate template) {
        super(responder, "Market", "", MARKET_QUESTION_TYPE, responder.getWurmId());
        this.template = template;
    }

    @Override
    public void sendQuestion() {
        Player responder = (Player) getResponder();
        String title = this.title;
        StringBuilder bml = new StringBuilder();
        int w = 500;
        int h = 370;
        int colw = 150;
        boolean resizable = false;
        bml.append("border{\n")
           .append(" null;\n")
           .append(" null;\n")
           .append(" scroll{horizontal='false';vertical='true';\n")
           .append("  varray{rescale='true';\n")
           .append("   passthrough{id='id';text='").append(getId()).append("'};\n")
           .append("   text{text=''}\n");

        List<MarketInfo> merchants = MerchantTask.getMarketInfo(responder, template);
        if(!merchants.isEmpty()) {
            bml.append("   text{type='bold';text='").append(template.getName()).append(" is traded by these merchants:'}\n")
               .append("   text{text=''}\n");
            bml.append("   table{cols='4';\n")
               .append("    label{type='bold';text='Merchant'};\n")
               .append("    label{type='bold';text='Trade'};\n")
               .append("    label{type='bold';text='Location'};\n")
               .append("    label{type='bold';text='Container'};\n");
            merchants.stream().sorted(Comparator.comparing(m -> m.traderName)).forEach(m -> {
                bml.append("    label{text='").append(m.traderName).append("'};\n")
                   .append("    label{text='").append(m.trade).append("'};\n")
                   .append("    label{text=\"").append(m.location).append("\"};\n")
                   .append("    label{text=\"").append(m.container).append("\"};\n");

            });
            bml.append("   }\n");
            bml.append("   text{text=''}\n");
        }

        List<MarketInfo> vendors = VendorTask.getMarketInfo(responder, template);
        if(!vendors.isEmpty()) {
            bml.append("   text{type='bold';text='").append(template.getName()).append(" is traded by these vendors:'}\n")
               .append("   text{text=''}\n");
            bml.append("   table{cols='4';\n")
               .append("    label{type='bold';text='Vendor'};\n")
               .append("    label{type='bold';text='Trade'};\n")
               .append("    label{type='bold';text='Location'};\n")
               .append("    label{type='bold';text='Container'};\n");
            vendors.stream().sorted(Comparator.comparing(m -> m.traderName)).forEach(m -> {
                bml.append("    label{text='").append(m.traderName).append("'};\n")
                   .append("    label{text='").append(m.trade).append("'};\n")
                   .append("    label{text=\"").append(m.location).append("\"};\n")
                   .append("    label{text=\"").append(m.container).append("\"};\n");

            });
            bml.append("   }\n");
            bml.append("   text{text=''}\n");
        }

        if(merchants.isEmpty() && vendors.isEmpty()) {
            bml.append("   text{text='Neither merchants nor vendors does currently trade in ").append(template.getPlural()).append(".'}\n");
        }

        bml.append("  }\n")
           .append(" };\n")
           .append(" null;\n")
           .append(" right{\n")
           .append("  harray{\n")
           .append("   button{id='submit';size='80,20';text='Ok'}\n")
           .append("  }\n")
           .append(" }\n")
           .append("}\n");

        responder.getCommunicator().sendBml(w, h, resizable, true, bml.toString(), 200, 200, 200, title);
    }

    @Override
    public void answer(Properties properties) { }
}
