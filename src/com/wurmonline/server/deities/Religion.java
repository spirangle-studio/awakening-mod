package com.wurmonline.server.deities;

import com.wurmonline.server.DbConnector;
import com.wurmonline.server.Items;
import com.wurmonline.server.NoSuchItemException;
import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.behaviours.MethodsItems;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.items.Materials;
import com.wurmonline.server.utils.DbUtilities;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Religion {

    private static final Logger logger = Logger.getLogger(Religion.class.getName());

    public static final int DEITY_SEDAES = 21;
    public static final int DEITY_PAYREN = 22;
    public static final int DEITY_EILANDA = 23;
    public static final int DEITY_SHONIN = 24;
    public static final int DEITY_ASMAN = 25;
    public static final int DEITY_KIANE = 26;
    public static final int DEITY_MIRIDON = 27;
    public static final int DEITY_LAPHANER = 28;
    public static final int DEITY_DRAKKAR = 29;
    public static final int DEITY_QALESHIN = 30;

    public static final long DEITY_TYPE_SKY = 0x0000000000000001L;
    public static final long DEITY_TYPE_SUN = 0x0000000000000002L;
    public static final long DEITY_TYPE_STAR = 0x0000000000000004L;
    public static final long DEITY_TYPE_MOON = 0x0000000000000008L;
    public static final long DEITY_TYPE_WIND = 0x0000000000000010L;
    public static final long DEITY_TYPE_THUNDER = 0x0000000000000020L;
    public static final long DEITY_TYPE_RAIN = 0x0000000000000040L;
    public static final long DEITY_TYPE_EARTH = 0x0000000000000080L;
    public static final long DEITY_TYPE_NATURE = 0x0000000000000100L;
    public static final long DEITY_TYPE_WATER = 0x0000000000000200L;
    public static final long DEITY_TYPE_METAL = 0x0000000000000400L;
    public static final long DEITY_TYPE_MOUNTAIN = 0x0000000000000800L;
    public static final long DEITY_TYPE_FERTILITY = 0x0000000000001000L;
    public static final long DEITY_TYPE_FOREST = 0x0000000000002000L;
    public static final long DEITY_TYPE_ANIMALS = 0x0000000000004000L;
    public static final long DEITY_TYPE_ARTISAN = 0x0000000000010000L;
    public static final long DEITY_TYPE_HUNTING = 0x0000000000020000L;
    public static final long DEITY_TYPE_CREATOR = 0x0000000000040000L;
    public static final long DEITY_TYPE_DESTROYER = 0x0000000000080000L;
    public static final long DEITY_TYPE_HEALING = 0x0000000000100000L;
    public static final long DEITY_TYPE_WISDOM = 0x0000000000200000L;
    public static final long DEITY_TYPE_FATE = 0x0000000000400000L;
    public static final long DEITY_TYPE_MAGIC = 0x0000000000800000L;
    public static final long DEITY_TYPE_JUSTICE = 0x0000000001000000L;
    public static final long DEITY_TYPE_ARTS = 0x0000000002000000L;
    public static final long DEITY_TYPE_TRAVEL = 0x0000000004000000L;
    public static final long DEITY_TYPE_SLEEP = 0x0000000008000000L;
    public static final long DEITY_TYPE_WEALTH = 0x0000000010000000L;
    public static final long DEITY_TYPE_LOVE = 0x0000000020000000L;
    public static final long DEITY_TYPE_HONOR = 0x0000000040000000L;
    public static final long DEITY_TYPE_WAR = 0x0000000080000000L;
    public static final long DEITY_TYPE_FIRE = 0x0000000100000000L;
    public static final long DEITY_TYPE_TRICKSTER = 0x0000000200000000L;
    public static final long DEITY_TYPE_DEATH = 0x0000000400000000L;
    public static final long DEITY_TYPE_CHAOS = 0x0000000800000000L;
    public static final long DEITY_TYPE_HATE = 0x0000001000000000L;

    private static Field deityAttributesField;

    private static Map<Integer, Deity> deities = null;
    private static final Map<Integer, Long> skillAttributes = new HashMap<Integer, Long>();

    public static void init() {
        Field entryAttributesField;
        try {
            entryAttributesField = ActionEntry.class.getDeclaredField("attributes");
            entryAttributesField.setAccessible(true);
        } catch(NoSuchFieldException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return;
        }
        ActionEntry ae;
        long attributes;
        for(int i = 0, n = Actions.actionEntrys.length; i < n; ++i) {
            ae = Actions.actionEntrys[i];
            attributes = 0L;
            switch(ae.getNumber()) {
                //				case Actions.EXAMINE:                                            attributes = ;break;
                //				case Actions.EQUIPMENT:                                          attributes = ;break;
                //				case Actions.OPEN:                                               attributes = ;break;
                //				case Actions.CLOSE:                                              attributes = ;break;
                //				case Actions.WAVE:                                               attributes = ;break;
                //				case Actions.TAKE:                                               attributes = ;break;
                //				case Actions.DROP:                                               attributes = ;break;
                //				case Actions.CUT:                                                attributes = ;break;
                //				case Actions.DISENGAGE:                                          attributes = ;break;
                //				case Actions.ASSIST:                                             attributes = ;break;
                //				case Actions.RIDE:                                               attributes = ;break;
                //				case Actions.LIGHT:                                              attributes = ;break;
                //				case Actions.BUY:                                                attributes = ;break;
                //				case Actions.EMPTY:                                              attributes = ;break;
                //				case Actions.CALL:                                               attributes = ;break;
                //				case Actions.REMOVE:                                             attributes = ;break;
                //				case Actions.READ:                                               attributes = ;break;
                //				case Actions.KNOCK:                                              attributes = ;break;
                //				case Actions.TASTE:                                              attributes = ;break;
                //				case Actions.QUIT:                                               attributes = ;break;
                //				case Actions.TELL:                                               attributes = ;break;
                case Actions.QUAFF:
                    attributes = DEITY_TYPE_WATER;
                    break;
                //				case Actions.DISMOUNT:                                           attributes = ;break;
                //				case Actions.ABANDON:                                            attributes = ;break;
                //				case Actions.UNCOVER:                                            attributes = ;break;
                //				case Actions.COVER:                                              attributes = ;break;
                //				case Actions.UNSADDLE:                                           attributes = ;break;
                //				case Actions.LOCK:                                               attributes = ;break;
                //				case Actions.SADDLE:                                             attributes = ;break;
                case Actions.WAKE:
                    attributes = DEITY_TYPE_SLEEP;
                    break;
                //				case Actions.SELL:                                               attributes = ;break;
                //				case Actions.WEAR:                                               attributes = ;break;
                //				case Actions.WIZKILL:                                            attributes = ;break;
                //				case Actions.FINDPATH:                                           attributes = ;break;
                //				case Actions.CRIMINALMODE:                                       attributes = ;break;
                //				case Actions.FAITHMODE:                                          attributes = ;break;
                //				case Actions.DROPDIRT:                                           attributes = ;break;
                //				case Actions.CLIMB:                                              attributes = ;break;
                //				case Actions.STOPCLIMB:                                          attributes = ;break;
                //				case Actions.ORDERPET_MOVE:                                      attributes = ;break;
                //				case Actions.ORDERPET_CLEAR:                                     attributes = ;break;
                //				case Actions.ORDERPET_ATTACK:                                    attributes = ;break;
                case Actions.PET_TRANSFER:
                    attributes = DEITY_TYPE_ANIMALS;
                    break;
                //				case Actions.PET_STAYONLINE:                                     attributes = ;break;
                //				case Actions.PET_GOOFFLINE:                                      attributes = ;break;
                //				case Actions.TAME:                                               attributes = ;break;
                //				case Actions.GIVE:                                               attributes = ;break;
                //				case Actions.FOLLOW:                                             attributes = ;break;
                //				case Actions.GROUP:                                              attributes = ;break;
                //				case Actions.UNGROUP:                                            attributes = ;break;
                case Actions.POUR:
                    attributes = DEITY_TYPE_WATER;
                    break;
                //				case Actions.SIP:                                                attributes = ;break;
                case Actions.SNUFF:
                    attributes = DEITY_TYPE_FIRE;
                    break;
                case Actions.CRUSH:
                    attributes = DEITY_TYPE_FERTILITY;
                    break;
                //				case Actions.PICKSEED:                                           attributes = ;break;
                //				case Actions.BUILD_PLAN:                                         attributes = ;break;
                //				case Actions.BUILD_PLAN_REMOVE:                                  attributes = ;break;
                //				case Actions.BUILD_PLAN_FINALIZE:                                attributes = ;break;
                //				case Actions.RENAME:                                             attributes = ;break;
                //				case Actions.ADDPERSON:                                          attributes = ;break;
                //				case Actions.REMOVEPERSON:                                       attributes = ;break;
                //				case Actions.MANAGE:                                             attributes = ;break;
                //				case Actions.TRADE:                                              attributes = ;break;
                //				case Actions.GET_COORDINATES:                                    attributes = ;break;
                //				case Actions.FOUND_VILLAGE:                                      attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_CITIZENS:                            attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_GUARDS:                              attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_SETTINGS:                            attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_REPUTATIONS:                         attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_GATES:                               attributes = ;break;
                //				case Actions.VILLAGE_HISTORY:                                    attributes = ;break;
                //				case Actions.AREA_HISTORY:                                       attributes = ;break;
                //				case Actions.VILLAGE_INVITE:                                     attributes = ;break;
                //				case Actions.DRAG:                                               attributes = ;break;
                //				case Actions.STOPDRAG:                                           attributes = ;break;
                //				case Actions.RESIZE_VILLAGE:                                     attributes = ;break;
                //				case Actions.VILLAGE_INFO:                                       attributes = ;break;
                //				case Actions.REPLACE:                                            attributes = ;break;
                case Actions.NOSPOON:
                    attributes = DEITY_TYPE_TRICKSTER;
                    break;
                //				case Actions.MANAGE_VILLAGE_ALLIANCES:                           attributes = ;break;
                //				case Actions.ALLIANCE_INVITE:                                    attributes = ;break;
                //				case Actions.DESTROY_STRUCTURE:                                  attributes = ;break;
                case Actions.DESTROY_ITEM:
                    attributes = DEITY_TYPE_ARTISAN;
                    break;
                //				case Actions.SPAMMODE:                                           attributes = ;break;
                //				case Actions.MANAGE_TRADERS:                                     attributes = ;break;
                //				case Actions.SET_PRICE:                                          attributes = ;break;
                //				case Actions.GET_PRICE:                                          attributes = ;break;
                //				case Actions.SETDATA:                                            attributes = ;break;
                //				case Actions.SETKINGDOM:                                         attributes = ;break;
                //				case Actions.POLL:                                               attributes = ;break;
                //				case Actions.ASK_REFRESH:                                        attributes = ;break;
                //				case Actions.LEARNSKILL:                                         attributes = ;break;
                //				case Actions.COMBINE:                                            attributes = ;break;
                //				case Actions.TELEPORTSET:                                        attributes = ;break;
                //				case Actions.TELEPORT:                                           attributes = ;break;
                case Actions.CHOP:
                    attributes = DEITY_TYPE_FERTILITY;
                    break;
                //				case Actions.CHOP_UP:                                            attributes = ;break;
                //				case Actions.PROTECT:                                            attributes = ;break;
                //				case Actions.PUSH:                                               attributes = ;break;
                case Actions.STEAL:
                    attributes = DEITY_TYPE_JUSTICE;
                    break;
//                case Actions.LOCKPICK:attributes = ;break;
                //				case Actions.UNLOCK:                                             attributes = ;break;
                case Actions.TAUNT:
                    attributes = DEITY_TYPE_HONOR;
                    break;
                //				case Actions.LEARN:                                              attributes = ;break;
                //				case Actions.SHIELDBASH:                                         attributes = ;break;
                //				case Actions.LEAD:                                               attributes = ;break;
                //				case Actions.STOPLEADING:                                        attributes = ;break;
                //				case Actions.BACKSTAB:                                           attributes = ;break;
                //				case Actions.TRACK:                                              attributes = ;break;
                //				case Actions.RECITE:                                             attributes = ;break;
                //				case Actions.TEACH:                                              attributes = ;break;
                //				case Actions.REST:                                               attributes = ;break;
                //				case Actions.KICK:                                               attributes = ;break;
                //				case Actions.ATTACK:                                             attributes = ;break;
                //				case Actions.LISTEN:                                             attributes = ;break;
                //				case Actions.BUILD:                                              attributes = ;break;
                //				case Actions.BURN:                                               attributes = ;break;
                //				case Actions.USE:                                                attributes = ;break;
                //				case Actions.BURY:                                               attributes = ;break;
                //				case Actions.BUTCHER:                                            attributes = ;break;
                //				case Actions.MEND:                                               attributes = ;break;
                //				case Actions.CAST:                                               attributes = ;break;
                //				case Actions.SCALP:                                              attributes = ;break;
                //				case Actions.SHOOT:                                              attributes = ;break;
                //				case Actions.QUICK_SHOT:                                         attributes = ;break;
                //				case Actions.SHOOT_HEAD:                                         attributes = ;break;
                //				case Actions.SHOOT_FACE:                                         attributes = ;break;
                //				case Actions.SHOOT_TORSO:                                        attributes = ;break;
                //				case Actions.SHOOT_LEFTARM:                                      attributes = ;break;
                //				case Actions.SHOOT_RIGHTARM:                                     attributes = ;break;
                //				case Actions.SHOOT_LEGS:                                         attributes = ;break;
                //				case Actions.STRING:                                             attributes = ;break;
                //				case Actions.BOW_UNSTRING:                                       attributes = ;break;
                //				case Actions.TARGET_PRACTICE:                                    attributes = ;break;
                //				case Actions.HIDE:                                               attributes = ;break;
                //				case Actions.STEALTH:                                            attributes = ;break;
                //				case Actions.PICK:                                               attributes = ;break;
                //				case Actions.HANG:                                               attributes = ;break;
                case Actions.SIT:
                    attributes = DEITY_TYPE_WIND;
                    break;
                //				case Actions.SLEEP:                                              attributes = ;break;
                //				case Actions.PRAY:                                               attributes = ;break;
                //				case Actions.SACRIFICE:                                          attributes = ;break;
                case Actions.DESECRATE:
                    attributes = DEITY_TYPE_MAGIC;
                    break;
                //				case Actions.DIG:                                                attributes = ;break;
                //				case Actions.MINE:                                               attributes = ;break;
                //				case Actions.MINEUPWARDS:                                        attributes = ;break;
                //				case Actions.MINEDOWNWARDS:                                      attributes = ;break;
                //				case Actions.CREATE:                                             attributes = ;break;
                //				case Actions.STOP:                                               attributes = ;break;
                //				case Actions.FLATTEN:                                            attributes = ;break;
                //				case Actions.FARM:                                               attributes = ;break;
                case Actions.HARVEST:
                    attributes = DEITY_TYPE_DEATH;
                    break;
                case Actions.SOW:
                    attributes = DEITY_TYPE_DEATH;
                    break;
                //				case Actions.ROAD_PACK:                                          attributes = ;break;
                //				case Actions.ROAD_PAVE:                                          attributes = ;break;
                //				case Actions.PROSPECT:                                           attributes = ;break;
                case Actions.FIGHT_AGGRESSIVE:
                    attributes = DEITY_TYPE_LOVE;
                    break;
                //				case Actions.FIGHT_NORMAL:                                       attributes = ;break;
                //				case Actions.FIGHT_DEFEND:                                       attributes = ;break;
                //          case Actions.FISH:
                //				case Actions.SET_LOCK:                                           attributes = ;break;
                //				case Actions.REPAIR:                                             attributes = ;break;
                //				case Actions.BUILD_STONEWALL:                                    attributes = ;break;
                //				case Actions.BUILD_STONEWALL_HIGH:                               attributes = ;break;
                //				case Actions.BUILD_PALISADE:                                     attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE:                                 attributes = ;break;
                //				case Actions.BUILD_PALISADE_GATE:                                attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_GATE:                            attributes = ;break;
                //				case Actions.CONTINUE_BUILDING:                                  attributes = ;break;
                //				case Actions.CONTINUE_BUILDING_FENCE:                            attributes = ;break;
                //				case Actions.PLAN_FENCE_DESTROY:                                 attributes = ;break;
                //				case Actions.FENCE_DESTROY:                                      attributes = ;break;
                //				case Actions.FENCE_DISASSEMBLE:                                  attributes = ;break;
                //				case Actions.WALL_DESTROY:                                       attributes = ;break;
                //				case Actions.WALL_DISASSEMBLE:                                   attributes = ;break;
                //				case Actions.PLANT_SIGN:                                         attributes = ;break;
                //				case Actions.TURN_ITEM:                                          attributes = ;break;
                //				case Actions.TURN_ITEM_BACK:                                     attributes = ;break;
                //				case Actions.SUMMON:                                             attributes = ;break;
                //				case Actions.DESTROY:                                            attributes = ;break;
                //				case Actions.PULL:                                               attributes = ;break;
                //				case Actions.EAT:                                                attributes = ;break;
                //				case Actions.DRINK:                                              attributes = ;break;
                //				case Actions.SHUTDOWN:                                           attributes = ;break;
                //				case Actions.GETINFO:                                            attributes = ;break;
                case Actions.PLANT:
                    attributes = DEITY_TYPE_DESTROYER;
                    break;
                //				case Actions.PICKSPROUT:                                         attributes = ;break;
                //				case Actions.GROW:                                               attributes = ;break;
                //				case Actions.FILL:                                               attributes = ;break;
                case Actions.SPIN:
                    attributes = DEITY_TYPE_MOON;
                    break;
                case Actions.DESTROY_PAVE:
                    attributes = DEITY_TYPE_TRAVEL;
                    break;
                //				case Actions.IMPROVE:                                            attributes = ;break;
                //				case Actions.REPAIR_STRUCT:                                      attributes = ;break;
                //				case Actions.PAYMENT_MANAGEMENT:                                 attributes = ;break;
                //				case Actions.POWER_MANAGEMENT:                                   attributes = ;break;
                //				case Actions.FIRSTAID:                                           attributes = ;break;
                //				case Actions.SPECMOVE1:                                          attributes = ;break;
                //				case Actions.SPECMOVE2:                                          attributes = ;break;
                //				case Actions.SPECMOVE3:                                          attributes = ;break;
                //				case Actions.SPECMOVE4:                                          attributes = ;break;
                //				case Actions.SPECMOVE5:                                          attributes = ;break;
                //				case Actions.SPECMOVE6:                                          attributes = ;break;
                //				case Actions.SPECMOVE7:                                          attributes = ;break;
                //				case Actions.SPECMOVE8:                                          attributes = ;break;
                //				case Actions.SPECMOVE9:                                          attributes = ;break;
                //				case Actions.SPECMOVE10:                                         attributes = ;break;
                //				case Actions.SPECMOVE11:                                         attributes = ;break;
                //				case Actions.SPECMOVE12:                                         attributes = ;break;
                case Actions.DECLARE_WAR:
                    attributes = DEITY_TYPE_LOVE;
                    break;
                case Actions.OFFER_PEACE:
                    attributes = DEITY_TYPE_WAR;
                    break;
                //				case Actions.PRACTICE:                                           attributes = ;break;
                //				case Actions.FAITH_MANAGEMENT:                                   attributes = ;break;
                //				case Actions.ASK_CONVERT:                                        attributes = ;break;
                //				case Actions.ASK_GIFT:                                           attributes = ;break;
                //				case Actions.CONVERT:                                            attributes = ;break;
                //				case Actions.PREACH:                                             attributes = ;break;
                //				case Actions.READ_INSCRIPTION_1:                                 attributes = ;break;
                //				case Actions.READ_INSCRIPTION_2:                                 attributes = ;break;
                //				case Actions.READ_INSCRIPTION_3:                                 attributes = ;break;
                //				case Actions.SET_REALDEATH:                                      attributes = ;break;
                //				case Actions.DESTROY_ALTAR:                                      attributes = ;break;
                //				case Actions.MANAGE_BANK:                                        attributes = ;break;
                //				case Actions.FORAGE:                                             attributes = ;break;
                //				case Actions.BOTANIZE:                                           attributes = ;break;
                //				case Actions.FILLET:                                             attributes = ;break;
                //				case Actions.WITHDRAW_MONEY:                                     attributes = ;break;
                case Actions.TUNNEL:
                    attributes = DEITY_TYPE_SKY;
                    break;
                //				case Actions.WORK:                                               attributes = ;break;
                case Actions.REINFORCE:
                    attributes = DEITY_TYPE_MOUNTAIN;
                    break;
                //				case Actions.FEED:                                               attributes = ;break;
                //				case Actions.COLOR:                                              attributes = ;break;
                case Actions.COLOR_REMOVE:
                    attributes = DEITY_TYPE_ARTS;
                    break;
                case Actions.LOAD:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.UNLOAD:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.UNWIND:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.FIRE:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.WINCH:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.WINCH5:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                case Actions.WINCH10:
                    attributes = DEITY_TYPE_HEALING;
                    break;
                //				case Actions.TRANSFER_NORTH:                                     attributes = ;break;
                //				case Actions.TRANSFER_EAST:                                      attributes = ;break;
                //				case Actions.TRANSFER_SOUTH:                                     attributes = ;break;
                //				case Actions.TRANSFER_WEST:                                      attributes = ;break;
                //				case Actions.MANAGE_SERVERS:                                     attributes = ;break;
                //				case Actions.SPELL_BLESS:                                        attributes = ;break;
                //				case Actions.SPELL_CURELIGHT:                                    attributes = ;break;
                //				case Actions.SPELL_CUREMEDIUM:                                   attributes = ;break;
                //				case Actions.SPELL_CURESERIOUS:                                  attributes = ;break;
                //				case Actions.SPELL_HEAL:                                         attributes = ;break;
                //				case Actions.SPELL_REFRESH:                                      attributes = ;break;
                //				case Actions.SPELL_MEND:                                         attributes = ;break;
                //				case Actions.SPELL_SMITE:                                        attributes = ;break;
                //				case Actions.SPELL_SUNDERITEM:                                   attributes = ;break;
                //				case Actions.SPELL_DRAIN_STAMINA:                                attributes = ;break;
                //				case Actions.SPELL_DRAIN_HEALTH:                                 attributes = ;break;
                //				case Actions.SPELL_EARLY_HARVEST:                                attributes = ;break;
                //				case Actions.SPELL_DRAIN_SKILL:                                  attributes = ;break;
                //				case Actions.SPELL_BREAK_ALTAR:                                  attributes = ;break;
                //				case Actions.SPELL_ENCHANT_FO_HATER:                             attributes = ;break;
                //				case Actions.SPELL_ENCHANT_MAGRANON_HATER:                       attributes = ;break;
                //				case Actions.SPELL_ENCHANT_VYNORA_HATER:                         attributes = ;break;
                //				case Actions.SPELL_ENCHANT_LIBILA_HATER:                         attributes = ;break;
                //				case Actions.SPELL_ENCHANT_FO_PROT:                              attributes = ;break;
                //				case Actions.SPELL_ENCHANT_MAGRANON_PROT:                        attributes = ;break;
                //				case Actions.SPELL_ENCHANT_VYNORA_PROT:                          attributes = ;break;
                //				case Actions.SPELL_ENCHANT_LIBILA_PROT:                          attributes = ;break;
                //				case Actions.SPELL_ENCHANT_HUMAN_HATER:                          attributes = ;break;
                //				case Actions.SPELL_ENCHANT_REGENERATION_HATER:                   attributes = ;break;
                //				case Actions.SPELL_ENCHANT_ANIMAL_HATER:                         attributes = ;break;
                //				case Actions.SPELL_ENCHANT_DRAGON_HATER:                         attributes = ;break;
                //				case Actions.SPELL_LOCATE_ARTIFACT:                              attributes = ;break;
                //				case Actions.SPELL_VESSEL:                                       attributes = ;break;
                //				case Actions.SPELL_REBIRTH:                                      attributes = ;break;
                //				case Actions.SPELL_DOMINATE:                                     attributes = ;break;
                //				case Actions.SPELL_CHARM:                                        attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_SKILLGAIN:                           attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_WEAPONDAM:                           attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_PAINSHARE:                           attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_QUICKACTIONS:                        attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_GOODFOOD:                            attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_DAMAGE_BRITTLE:                      attributes = ;break;
                //				case Actions.SPELL_CREATUREBUFF_PROT_DAMAGE:                     attributes = ;break;
                //				case Actions.MIX:                                                attributes = ;break;
                //				case Actions.TREAT:                                              attributes = ;break;
                //				case Actions.MIX_INFO:                                           attributes = ;break;
                //				case Actions.BECOME_PRIEST:                                      attributes = ;break;
                //				case Actions.ATTACK_UPPER_LEFT_HARD:                             attributes = ;break;
                //				case Actions.ATTACK_UPPER_LEFT_NORMAL:                           attributes = ;break;
                //				case Actions.ATTACK_UPPER_LEFT_QUICK:                            attributes = ;break;
                //				case Actions.ATTACK_MID_LEFT_HARD:                               attributes = ;break;
                //				case Actions.ATTACK_MID_LEFT_NORMAL:                             attributes = ;break;
                //				case Actions.ATTACK_MID_LEFT_QUICK:                              attributes = ;break;
                //				case Actions.ATTACK_LOW_LEFT_HARD:                               attributes = ;break;
                //				case Actions.ATTACK_LOW_LEFT_NORMAL:                             attributes = ;break;
                //				case Actions.ATTACK_LOW_LEFT_QUICK:                              attributes = ;break;
                //				case Actions.ATTACK_LOW_HARD:                                    attributes = ;break;
                //				case Actions.ATTACK_LOW_NORMAL:                                  attributes = ;break;
                //				case Actions.ATTACK_LOW_QUICK:                                   attributes = ;break;
                //				case Actions.ATTACK_HIGH_HARD:                                   attributes = ;break;
                //				case Actions.ATTACK_HIGH_NORMAL:                                 attributes = ;break;
                //				case Actions.ATTACK_HIGH_QUICK:                                  attributes = ;break;
                //				case Actions.ATTACK_CENTER_HARD:                                 attributes = ;break;
                //				case Actions.ATTACK_CENTER_NORMAL:                               attributes = ;break;
                //				case Actions.ATTACK_CENTER_QUICK:                                attributes = ;break;
                //				case Actions.ATTACK_UPPER_RIGHT_HARD:                            attributes = ;break;
                //				case Actions.ATTACK_UPPER_RIGHT_NORMAL:                          attributes = ;break;
                //				case Actions.ATTACK_UPPER_RIGHT_QUICK:                           attributes = ;break;
                //				case Actions.ATTACK_MID_RIGHT_HARD:                              attributes = ;break;
                //				case Actions.ATTACK_MID_RIGHT_NORMAL:                            attributes = ;break;
                //				case Actions.ATTACK_MID_RIGHT_QUICK:                             attributes = ;break;
                //				case Actions.ATTACK_LOW_RIGHT_HARD:                              attributes = ;break;
                //				case Actions.ATTACK_LOW_RIGHT_NORMAL:                            attributes = ;break;
                //				case Actions.ATTACK_LOW_RIGHT_QUICK:                             attributes = ;break;
                //				case Actions.DEFEND_HIGH:                                        attributes = ;break;
                //				case Actions.DEFEND_LEFT:                                        attributes = ;break;
                //				case Actions.DEFEND_LOW:                                         attributes = ;break;
                //				case Actions.DEFEND_RIGHT:                                       attributes = ;break;
                //				case Actions.CULTIVATE:                                          attributes = ;break;
                //				case Actions.RENT_1_COPPER:                                      attributes = ;break;
                //				case Actions.RENT_10_COPPER:                                     attributes = ;break;
                //				case Actions.RENT_1_SILVER:                                      attributes = ;break;
                //				case Actions.RENT_10_SILVER:                                     attributes = ;break;
                //				case Actions.RESET_RENT:                                         attributes = ;break;
                //				case Actions.HIRE:                                               attributes = ;break;
                //				case Actions.ASKSLEEP:                                           attributes = ;break;
                //				case Actions.TARGET:                                             attributes = ;break;
                //				case Actions.FREEZE:                                             attributes = ;break;
                case Actions.SUCK:
                    attributes = DEITY_TYPE_ANIMALS;
                    break;
                //				case Actions.WATCH:                                              attributes = ;break;
                //				case Actions.HATCH:                                              attributes = ;break;
                //				case Actions.EMBARK_DRIVER:                                      attributes = ;break;
                //				case Actions.EMBARK_PASSENGER:                                   attributes = ;break;
                //				case Actions.DISEMBARK:                                          attributes = ;break;
                //				case Actions.CHECK_REIMBURSEMENTS:                               attributes = ;break;
                //				case Actions.ASK_CHANGETILE:                                     attributes = ;break;
                //				case Actions.MAIL_CHECK:                                         attributes = ;break;
                //				case Actions.MAIL_SEND:                                          attributes = ;break;
                //				case Actions.SPELL_ENCHANT_COURIER:                              attributes = ;break;
                //				case Actions.SPELL_ENCHANT_MESSENGER:                            attributes = ;break;
                //				case Actions.FIGHT_FOCUS:                                        attributes = ;break;
                //				case Actions.CLEARTARGET:                                        attributes = ;break;
                //				case Actions.THROW:                                              attributes = ;break;
                case Actions.DUEL:
                    attributes = DEITY_TYPE_WAR;
                    break;
                //				case Actions.SPAR:                                               attributes = ;break;
                //				case Actions.MILK:                                               attributes = ;break;
                //				case Actions.HEALFAST:                                           attributes = ;break;
                //				case Actions.HEAL_ABSORB:                                        attributes = ;break;
                //				case Actions.DISBAND_VILLAGE:                                    attributes = ;break;
                //				case Actions.STOP_DISBAND_VILLAGE:                               attributes = ;break;
                //				case Actions.DRAIN_COFFERS:                                      attributes = ;break;
                //				case Actions.ASK_TUTORIAL:                                       attributes = ;break;
                //				case Actions.SET_LOGGING:                                        attributes = ;break;
                //				case Actions.ASPIRE_KING:                                        attributes = ;break;
                //				case Actions.APPOINT:                                            attributes = ;break;
                //				case Actions.KINGDOM_STATUS:                                     attributes = ;break;
                //				case Actions.KINGDOM_HISTORY:                                    attributes = ;break;
                //				case Actions.ANNOUNCE:                                           attributes = ;break;
                case Actions.ABDICATE:
                    attributes = DEITY_TYPE_SUN;
                    break;
                //				case Actions.MANAGE_VEHICLE:                                     attributes = ;break;
                //				case Actions.MOOR:                                               attributes = ;break;
                //				case Actions.RAISEANCHOR:                                        attributes = ;break;
                //				case Actions.DREDGE:                                             attributes = ;break;
                //				case Actions.BUILD_MINEDOOR:                                     attributes = ;break;
                //				case Actions.MANAGE_MINEDOOR:                                    attributes = ;break;
                case Actions.RENT_10_IRON:
                    attributes = DEITY_TYPE_WEALTH;
                    break;
                //				case Actions.RENT_25_IRON:                                       attributes = ;break;
                //				case Actions.RENT_50_IRON:                                       attributes = ;break;
                //				case Actions.SETNP:                                              attributes = ;break;
                //				case Actions.SCULPT:                                             attributes = ;break;
                //				case Actions.RECHARGE:                                           attributes = ;break;
                //				case Actions.ECONOMIC_INFO:                                      attributes = ;break;
                //				case Actions.MANAGE_PERIMETER:                                   attributes = ;break;
                case Actions.PRUNE:
                    attributes = DEITY_TYPE_FOREST;
                    break;
                case Actions.TRAP:
                    attributes = DEITY_TYPE_HUNTING;
                    break;
                //				case Actions.DISARM:                                             attributes = ;break;
                //				case Actions.SPELL_SIXTH_SENSE:                                  attributes = ;break;
                //				case Actions.HITCH:                                              attributes = ;break;
                //				case Actions.UNHITCH:                                            attributes = ;break;
                //				case Actions.BREED:                                              attributes = ;break;
                case Actions.WISH:
                    attributes = DEITY_TYPE_WISDOM;
                    break;
                //				case Actions.PROT_TILE:                                          attributes = ;break;
                //				case Actions.DEPROT_TILE:                                        attributes = ;break;
                //				case Actions.COUNT_ITEMS:                                        attributes = ;break;
                //				case Actions.MEDITATE:                                           attributes = ;break;
                //				case Actions.LEAVE_PATH:                                         attributes = ;break;
                //				case Actions.PATH_LEADERS:                                       attributes = ;break;
                //				case Actions.LIGHT_PATH:                                         attributes = ;break;
                //				case Actions.ENCHANT:                                            attributes = ;break;
                //				case Actions.LOVE_EFFECT:                                        attributes = ;break;
                //				case Actions.DOUBLE_ATTACK_DAM:                                  attributes = ;break;
                //				case Actions.DOUBLE_STRUCT_DAM:                                  attributes = ;break;
                //				case Actions.FEAR_EFFECT:                                        attributes = ;break;
                //				case Actions.NO_ELEMENTAL_DAM:                                   attributes = ;break;
                //				case Actions.IGNORE_TRAPS:                                       attributes = ;break;
                //				case Actions.CLEAN_WOUND:                                        attributes = ;break;
                //				case Actions.FILLUP:                                             attributes = ;break;
                case Actions.PUPPETEER:
                    attributes = DEITY_TYPE_WISDOM;
                    break;
                //				case Actions.GROOM:                                              attributes = ;break;
                //				case Actions.MAGICLINK:                                          attributes = ;break;
                //				case Actions.SPELL_HOLYCROP:                                     attributes = ;break;
                //				case Actions.SPELL_RITUAL_SUN:                                   attributes = ;break;
                //				case Actions.SPELL_RITE_DEATH:                                   attributes = ;break;
                //				case Actions.SPELL_RITE_SPRING:                                  attributes = ;break;
                //				case Actions.SPELL_OAKSHELL:                                     attributes = ;break;
                //				case Actions.SPELL_WILLOWSPINE:                                  attributes = ;break;
                //				case Actions.SPELL_BEARPAW:                                      attributes = ;break;
                //				case Actions.SPELL_HUMIDDRIZZLE:                                 attributes = ;break;
                //				case Actions.SPELL_GENESIS:                                      attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_LIFETRANSFER:                        attributes = ;break;
                //				case Actions.SPELL_FORESTGIANT:                                  attributes = ;break;
                //				case Actions.SPELL_FROGLEAP:                                     attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_VENOM:                               attributes = ;break;
                //				case Actions.SPELL_TORNADO:                                      attributes = ;break;
                //				case Actions.SPELL_ICEPILLAR:                                    attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_MINDSTEALER:                         attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_NIMBLE:                              attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_FROSTBRAND:                          attributes = ;break;
                //				case Actions.SPELL_TENTACLESDEEP:                                attributes = ;break;
                //				case Actions.SPELL_LOCATE_PLAYER:                                attributes = ;break;
                //				case Actions.SPELL_FIREPILLAR:                                   attributes = ;break;
                //				case Actions.SPELL_LIGHTTOKEN:                                   attributes = ;break;
                //				case Actions.SPELL_GOATSHAPE:                                    attributes = ;break;
                //				case Actions.SPELL_FRANTICCHARGE:                                attributes = ;break;
                //				case Actions.SPELL_FIREHEART:                                    attributes = ;break;
                //				case Actions.SPELL_MASS_STAMINA:                                 attributes = ;break;
                //				case Actions.SPELL_PHANTASMS:                                    attributes = ;break;
                //				case Actions.SPELL_HELLSTRENGTH:                                 attributes = ;break;
                //				case Actions.SPELL_ROTTINGGUT:                                   attributes = ;break;
                //				case Actions.SPELL_WEAKNESS:                                     attributes = ;break;
                //				case Actions.SPELL_WORMBRAINS:                                   attributes = ;break;
                //				case Actions.SPELL_ZOMBIEINFESTATION:                            attributes = ;break;
                //				case Actions.SPELL_PAINRAIN:                                     attributes = ;break;
                //				case Actions.SPELL_FUNGUSTRAP:                                   attributes = ;break;
                //				case Actions.SPELL_OUTOFMIND:                                    attributes = ;break;
                //				case Actions.SPELL_LANDOFDEAD:                                   attributes = ;break;
                //				case Actions.SPELL_WILDGROWTH:                                   attributes = ;break;
                //				case Actions.SPELL_WARD:                                         attributes = ;break;
                //				case Actions.SPELL_LIGHTOFFO:                                    attributes = ;break;
                //				case Actions.SPELL_MOLESENSES:                                   attributes = ;break;
                //				case Actions.SPELL_STRONGWALL:                                   attributes = ;break;
                //				case Actions.SPELL_WRATH_OF_MAGRANON:                            attributes = ;break;
                //				case Actions.SPELL_EXCEL:                                        attributes = ;break;
                //				case Actions.SPELL_REVEAL_SETTLEMENT:                            attributes = ;break;
                //				case Actions.SPELL_REVEAL_CREATURES:                             attributes = ;break;
                //				case Actions.SPELL_WISDOM_VYNORA:                                attributes = ;break;
                //				case Actions.SPELL_FUNGUS:                                       attributes = ;break;
                //				case Actions.SPELL_TRUEHIT:                                      attributes = ;break;
                //				case Actions.SPELL_SCORN_LIBILA:                                 attributes = ;break;
                //				case Actions.SPELL_DISINTEGRATE:                                 attributes = ;break;
                //				case Actions.SPELL_DISPEL:                                       attributes = ;break;
                //				case Actions.SPELL_NOLOCATE:                                     attributes = ;break;
                //				case Actions.SELECT_SPELL:                                       attributes = ;break;
                //				case Actions.SPELL_DIRT:                                         attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_BLOODTHIRST:                         attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_SLOWDOWN:                            attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_BLESSINGDARK:                        attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_LOCATEFISH:                          attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_LOCATECHAMP:                         attributes = ;break;
                //				case Actions.SPELL_ITEMBUFF_LOCATEENEMY:                         attributes = ;break;
                //				case Actions.WRAP:                                               attributes = ;break;
                //				case Actions.TOGGLE_JOAT:                                        attributes = ;break;
                case Actions.TRANSMUTATE:
                    attributes = DEITY_TYPE_NATURE;
                    break;
                //				case Actions.ADD:                                                attributes = ;break;
                //				case Actions.REMOVE_TEN:                                         attributes = ;break;
                //				case Actions.REMOVE_FIFTY:                                       attributes = ;break;
                //				case Actions.REFUND:                                             attributes = ;break;
                //				case Actions.GM_INTERFACE:                                       attributes = ;break;
                //				case Actions.CHOP_OLD_TREE:                                      attributes = ;break;
                //				case Actions.TEAM_INVITE:                                        attributes = ;break;
                //				case Actions.TEAM_KICK:                                          attributes = ;break;
                //				case Actions.TEAM_MANAGE:                                        attributes = ;break;
                //				case Actions.MISSION_MANAGE:                                     attributes = ;break;
                //				case Actions.TALK:                                               attributes = ;break;
                //				case Actions.SHOW:                                               attributes = ;break;
                //				case Actions.STEP_ON:                                            attributes = ;break;
                //				case Actions.GETLOGEVENTS:                                       attributes = ;break;
                //				case Actions.BUILD_IRON_FENCE:                                   attributes = ;break;
                //				case Actions.BUILD_WOVEN_FENCE:                                  attributes = ;break;
                //				case Actions.BUILD_IRON_FENCE_GATE:                              attributes = ;break;
                //				case Actions.FOUND_KINGDOM:                                      attributes = ;break;
                //				case Actions.CONFIGURE_TWITTER:                                  attributes = ;break;
                //				case Actions.READ_INSCRIPTION_4:                                 attributes = ;break;
                //				case Actions.TEST_TERRAIN:                                       attributes = ;break;
                //				case Actions.BRAND:                                              attributes = ;break;
                //				case Actions.SPELL_SHARD_ICE:                                    attributes = ;break;
                //				case Actions.TESTCASE:                                           attributes = ;break;
                //				case Actions.VOTE:                                               attributes = ;break;
                //				case Actions.CHALLENGE:                                          attributes = ;break;
                //				case Actions.RECALL:                                             attributes = ;break;
                //				case Actions.FINAL_BREATH:                                       attributes = ;break;
                //				case Actions.ON_DEATH:                                           attributes = ;break;
                //				case Actions.ON_CUT_DOWN:                                        attributes = ;break;
                //				case Actions.SET_PROTECTED:                                      attributes = ;break;
                //				case Actions.VOICE_CHAT:                                         attributes = ;break;
                case Actions.LAST_GASP:
                    attributes = DEITY_TYPE_FATE;
                    break;
                //				case Actions.RITUAL_FAITH:                                       attributes = ;break;
                //				case Actions.RITUAL_RAIN:                                        attributes = ;break;
                //				case Actions.RITUAL_FOG:                                         attributes = ;break;
                //				case Actions.RITUAL_SUN:                                         attributes = ;break;
                //				case Actions.RITUAL_HEALING:                                     attributes = ;break;
                //				case Actions.RITUAL_DEATH:                                       attributes = ;break;
                //				case Actions.RITUAL_PLAGUE:                                      attributes = ;break;
                //				case Actions.CREATE_ZONE:                                        attributes = ;break;
                //				case Actions.CONQUER:                                            attributes = ;break;
                //				case Actions.INSCRIBE:                                           attributes = ;break;
                //				case Actions.INSCRIPTION_READ:                                   attributes = ;break;
                //				case Actions.BUILD_PLAN_ROOF:                                    attributes = ;break;
                //				case Actions.BUILD_PLAN_FLOOR_ABOVE:                             attributes = ;break;
                //				case Actions.BUILD_PLAN_FLOOR_BELOW:                             attributes = ;break;
                //				case Actions.MANAGE_ACHIEVEMENT:                                 attributes = ;break;
                //				case Actions.KARMA_QUESTION:                                     attributes = ;break;
                //				case Actions.MAGIC_WALL:                                         attributes = ;break;
                //				case Actions.SUMMON_GUARDS:                                      attributes = ;break;
                //				case Actions.BUILD_PLAN_FLOOR_ABOVE_WITH_DOOR:                   attributes = ;break;
                //				case Actions.BUILD_PLAN_FLOOR_ABOVE_WITH_OPENING:                attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_PARAPET:                         attributes = ;break;
                //				case Actions.BUILD_STONE_FENCE_PARAPET:                          attributes = ;break;
                //				case Actions.RAISE_GROUND:                                       attributes = ;break;
                case Actions.SMELT:
                    attributes = DEITY_TYPE_ARTISAN;
                    break;
                //				case Actions.BUILD_WOODEN_FENCE_CRUDE:                           attributes = ;break;
                //				case Actions.BUILD_STONE_IRON_FENCE_PARAPET:                     attributes = ;break;
                //				case Actions.CLIMB_UP:                                           attributes = ;break;
                //				case Actions.CLIMB_DOWN:                                         attributes = ;break;
                //				case Actions.FLOOR_DESTROY:                                      attributes = ;break;
                //				case Actions.ROOF_DESTROY:                                       attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_GARDESGARD_LOW:                  attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_GARDESGARD_HIGH:                 attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_CRUDE_GATE:                      attributes = ;break;
                //				case Actions.BUILD_WOODEN_FENCE_GARDESGARD_GATE:                 attributes = ;break;
                //				case Actions.BUILD_EXPAND_TILE:                                  attributes = ;break;
                //				case Actions.BUILD_REMOVE_TILE:                                  attributes = ;break;
                //				case Actions.LEVEL:                                              attributes = ;break;
                //				case Actions.FLATTEN_BORDER:                                     attributes = ;break;
                //				case Actions.GM_TOOL:                                            attributes = ;break;
                //				case Actions.FEATURE_MANAGEMENT:                                 attributes = ;break;
                //				case Actions.ANALYSE:                                            attributes = ;break;
                //				case Actions.THREATEN:                                           attributes = ;break;
                //				case Actions.GM_SET_TRAITS:                                      attributes = ;break;
                //				case Actions.GM_SET_ENCHANTS:                                    attributes = ;break;
                //				case Actions.MANAGE_VILLAGE_ROLES:                               attributes = ;break;
                //				case Actions.BUILD_STONE_FENCE:                                  attributes = ;break;
                //				case Actions.BUILD_STONE_CURB:                                   attributes = ;break;
                //				case Actions.BUILD_ROPE_FENCE_LOW:                               attributes = ;break;
                //				case Actions.BUILD_ROPE_FENCE_HIGH:                              attributes = ;break;
                //				case Actions.BUILD_HIGH_IRON_FENCE:                              attributes = ;break;
                //				case Actions.BUILD_HIGH_IRON_FENCE_GATE:                         attributes = ;break;
                //				case Actions.SPELL_KARMA_DISEASE:                                attributes = ;break;
                //				case Actions.SPELL_KARMA_RUSTMONSTER:                            attributes = ;break;
                //				case Actions.SPELL_KARMA_FIREBALL:                               attributes = ;break;
                //				case Actions.SPELL_KARMA_BOLT:                                   attributes = ;break;
                //				case Actions.SPELL_KARMA_MISSILE:                                attributes = ;break;
                //				case Actions.SPELL_KARMA_CONTINUUM:                              attributes = ;break;
                //				case Actions.SPELL_KARMA_STONESKIN:                              attributes = ;break;
                //				case Actions.SPELL_KARMA_SLOW:                                   attributes = ;break;
                //				case Actions.SPELL_KARMA_TRUESTRIKE:                             attributes = ;break;
                //				case Actions.SPELL_KARMA_ICEWALL:                                attributes = ;break;
                //				case Actions.SPELL_KARMA_FIREWALL:                               attributes = ;break;
                //				case Actions.SPELL_KARMA_STONEWALL:                              attributes = ;break;
                //				case Actions.SPELL_KARMA_SUMMON:                                 attributes = ;break;
                //				case Actions.SPELL_KARMA_FORECAST:                               attributes = ;break;
                //				case Actions.SPELL_KARMA_WEATHER:                                attributes = ;break;
                //				case Actions.SPELL_KARMA_MIRRORS:                                attributes = ;break;
                case Actions.PLANT_FLOWERBED:
                    attributes = DEITY_TYPE_METAL;
                    break;
                case Actions.PLANT_FLOWER:
                    attributes = DEITY_TYPE_METAL;
                    break;
                case Actions.WATER_PLANT:
                    attributes = DEITY_TYPE_RAIN;
                    break;
                //				case Actions.MANAGE_PLAYER_PROFILE:                              attributes = ;break;
                case Actions.ADD_INVENTORY_GROUP:
                    attributes = DEITY_TYPE_CHAOS;
                    break;
                //				case Actions.OPEN_INVENTORY_CONTAINER:                           attributes = ;break;
                //				case Actions.FORAGE_VEG:                                         attributes = ;break;
                //				case Actions.FORAGE_RESOURCE:                                    attributes = ;break;
                //				case Actions.FORAGE_BERRIES:                                     attributes = ;break;
                //				case Actions.BOTANIZE_SEEDS:                                     attributes = ;break;
                //				case Actions.BOTANIZE_HERBS:                                     attributes = ;break;
                //				case Actions.BOTANIZE_PLANTS:                                    attributes = ;break;
                //				case Actions.BOTANIZE_RESOURCE:                                  attributes = ;break;
                //				case Actions.ROAD_PAVE_CORNER:                                   attributes = ;break;
                //				case Actions.SET_INVISIBLE:                                      attributes = ;break;
                //				case Actions.SET_VISIBLE:                                        attributes = ;break;
                //				case Actions.SET_INVULNERABLE:                                   attributes = ;break;
                //				case Actions.SET_VULNERABLE:                                     attributes = ;break;
                //				case Actions.DECAY:                                              attributes = ;break;
                //				case Actions.EQUIP:                                              attributes = ;break;
                //				case Actions.EQUIP_LEFT:                                         attributes = ;break;
                //				case Actions.EQUIP_RIGHT:                                        attributes = ;break;
                //				case Actions.UNEQUIP:                                            attributes = ;break;
                //				case Actions.REMOVE_INVENTORY_GROUP:                             attributes = ;break;
                //				case Actions.TICKET_VIEW:                                        attributes = ;break;
                //				case Actions.TICKET_CANCEL:                                      attributes = ;break;
                //				case Actions.TICKET_RESPOND:                                     attributes = ;break;
                //				case Actions.TICKET_RESOLVED:                                    attributes = ;break;
                //				case Actions.TICKET_FORWARD_GM:                                  attributes = ;break;
                //				case Actions.TICKET_FORWARD_ARCH:                                attributes = ;break;
                //				case Actions.TICKET_FORWARD_DEV:                                 attributes = ;break;
                //				case Actions.TICKET_ONHOLD:                                      attributes = ;break;
                //				case Actions.TICKET_TAKE:                                        attributes = ;break;
                //				case Actions.TICKET_FORWARD_CM:                                  attributes = ;break;
                //				case Actions.TICKET_FEEDBACK:                                    attributes = ;break;
                //				case Actions.MANAGE_RECRUITMENT_AD:                              attributes = ;break;
                //				case Actions.TICKET_REOPEN:                                      attributes = ;break;
                case Actions.DISCARD:
                    attributes = DEITY_TYPE_CREATOR;
                    break;
                //				case Actions.LOOK_FOR_VILLAGE:                                   attributes = ;break;
                //				case Actions.DELETE_RECRUITMENT_AD:                              attributes = ;break;
                //				case Actions.EDIT_RECRUITMENT_AD:                                attributes = ;break;
                //				case Actions.PAINT_TERRAIN:                                      attributes = ;break;
                //				case Actions.LOAD_CARGO:                                         attributes = ;break;
                //				case Actions.UNLOAD_CARGO:                                       attributes = ;break;
                //				case Actions.ADD_TO_CREATION_WINDOW:                             attributes = ;break;
                //				case Actions.SWITCH_SKILL:                                       attributes = ;break;
                //				case Actions.INGAME_VOTING_SETUP:                                attributes = ;break;
                //				case Actions.COMMUNE:                                            attributes = ;break;
                //				case Actions.BUILD_MEDIUM_CHAIN_FENCE:                           attributes = ;break;
                //				case Actions.BUILD_WOOD_WALL:                                    attributes = ;break;
                //				case Actions.BUILD_WOOD_WINDOW:                                  attributes = ;break;
                //				case Actions.BUILD_WOOD_DOOR:                                    attributes = ;break;
                //				case Actions.BUILD_WOOD_DOUBLE_DOOR:                             attributes = ;break;
                //				case Actions.BUILD_WOOD_ARCHED_WALL:                             attributes = ;break;
                //				case Actions.BUILD_STONE_WALL:                                   attributes = ;break;
                //				case Actions.BUILD_STONE_WINDOW:                                 attributes = ;break;
                //				case Actions.BUILD_STONE_DOOR:                                   attributes = ;break;
                //				case Actions.BUILD_STONE_DOUBLE_DOOR:                            attributes = ;break;
                //				case Actions.BUILD_STONE_ARCHED_WALL:                            attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_WALL:                           attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_WINDOW:                         attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_DOOR:                           attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_DOUBLE_DOOR:                    attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_ARCHED_WALL:                    attributes = ;break;
                //				case Actions.ROAD_PAVE_ROUGH:                                    attributes = ;break;
                //				case Actions.ROAD_PAVE_ROUND:                                    attributes = ;break;
                //				case Actions.SPELL_KARMA_SUMMON_WORG:                            attributes = ;break;
                //				case Actions.SPELL_KARMA_SUMMON_WRAITH:                          attributes = ;break;
                //				case Actions.SPELL_KARMA_SUMMON_SKELETONS:                       attributes = ;break;
                //				case Actions.TRANSFER_RARITY:                                    attributes = ;break;
                //				case Actions.IMBUE:                                              attributes = ;break;
                //				case Actions.SPELL_KARMA_SPROUT_TREES:                           attributes = ;break;
                //				case Actions.GROUP_CA_HELP_SETUP:                                attributes = ;break;
                //				case Actions.HOLD:                                               attributes = ;break;
                //				case Actions.BUILD_PLAN_BRIDGE:                                  attributes = ;break;
                case Actions.DROP_AS_PILE:
                    attributes = DEITY_TYPE_EARTH;
                    break;
                //				case Actions.DRAIN:                                              attributes = ;break;
                //				case Actions.SURVEY:                                             attributes = ;break;
                //				case Actions.SPELL_TANGLEWEAVE:                                  attributes = ;break;
                case Actions.RUMMAGE:
                    attributes = DEITY_TYPE_MOUNTAIN;
                    break;
                //				case Actions.UNBRAND:                                            attributes = ;break;
                case Actions.TRIM:
                    attributes = DEITY_TYPE_NATURE;
                    break;
                //				case Actions.GATHER:                                             attributes = ;break;
                //				case Actions.SHEAR:                                              attributes = ;break;
                //				case Actions.MODIFY_WALL:                                        attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_WALL:                             attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_WINDOW:                           attributes = ;break;
                //				case Actions.BUILD_PLAIN_NARROW_STONE_WINDOW:                    attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_DOOR:                             attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_DOUBLE_DOOR:                      attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_ARCHED_WALL:                      attributes = ;break;
                //				case Actions.BUILD_FENCE_PORTCULLIS:                             attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_PORTCULLIS:                       attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_BARRED_WALL:                      attributes = ;break;
                //				case Actions.BUILD_STONE_PORTCULLIS:                             attributes = ;break;
                //				case Actions.BUILD_WOOD_PORTCULLIS:                              attributes = ;break;
                //				case Actions.BUILD_PLAN_STAIRCASE_WITH_OPENING_ABOVE:            attributes = ;break;
                case Actions.PLANT_CENTER:
                    attributes = DEITY_TYPE_FOREST;
                    break;
                //				case Actions.MANAGE_FRIENDS:                                     attributes = ;break;
                //				case Actions.INSIDEOUT:                                          attributes = ;break;
                //				case Actions.MANAGE_ANIMAL:                                      attributes = ;break;
                //				case Actions.MANAGE_BUILDING:                                    attributes = ;break;
                //				case Actions.MANAGE_CART:                                        attributes = ;break;
                //				case Actions.MANAGE_DOOR:                                        attributes = ;break;
                //				case Actions.MANAGE_GATE:                                        attributes = ;break;
                //				case Actions.MANAGE_SHIP:                                        attributes = ;break;
                //				case Actions.MANAGE_WAGON:                                       attributes = ;break;
                //				case Actions.MANAGE_UPKEEP:                                      attributes = ;break;
                //				case Actions.HAUL_UP:                                            attributes = ;break;
                //				case Actions.HAUL_DOWN:                                          attributes = ;break;
                //				case Actions.MANAGE_ALL_DOORS:                                   attributes = ;break;
                //				case Actions.TAG_ITEM:                                           attributes = ;break;
                //				case Actions.SUMMON_TAGGED_ITEM:                                 attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_BALCONY:                        attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_JETTY:                          attributes = ;break;
                //				case Actions.BUILD_DECORATED_STONE_ORIEL:                        attributes = ;break;
                //				case Actions.BUILD_WOODEN_CANOPY:                                attributes = ;break;
                //				case Actions.BUILD_WOODEN_WIDE_WINDOW:                           attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_ORIEL:                            attributes = ;break;
                case Actions.USE_PENDULUM:
                    attributes = DEITY_TYPE_THUNDER;
                    break;
                //				case Actions.ROTATE_WALL:                                        attributes = ;break;
                //				case Actions.MANAGE_ITEM_RESTRICTIONS:                           attributes = ;break;
                //				case Actions.PICKUP_PLANTED:                                     attributes = ;break;
                //				case Actions.SPELL_KARMA_INCINERATE:                             attributes = ;break;
                //				case Actions.MANAGE_SMALL_CART:                                  attributes = ;break;
                //				case Actions.MANAGE_ITEM:                                        attributes = ;break;
                //				case Actions.SHOW_VILLAGE_PLAN:                                  attributes = ;break;
                //				case Actions.SEARCH_PERMISSIONS:                                 attributes = ;break;
                //				case Actions.SHOW_HISTORY_FOR_OBJECT:                            attributes = ;break;
                //				case Actions.SHOW_HISTORY_FOR_DOOR:                              attributes = ;break;
                //				case Actions.SKIP_TUTORIAL:                                      attributes = ;break;
                //				case Actions.ROAD_PAVE_CORNER_ROUGH:                             attributes = ;break;
                //				case Actions.ROAD_PAVE_CORNER_ROUND:                             attributes = ;break;
                //				case Actions.PUSH_GENTLY:                                        attributes = ;break;
                //				case Actions.PULL_GENTLY:                                        attributes = ;break;
                //				case Actions.TOGGLE_CA:                                          attributes = ;break;
                //				case Actions.TOGGLE_CM:                                          attributes = ;break;
                //				case Actions.GET_LCM_INFO:                                       attributes = ;break;
                case Actions.SIT_ANY:
                    attributes = DEITY_TYPE_WIND;
                    break;
                case Actions.SIT_LEFT:
                    attributes = DEITY_TYPE_WIND;
                    break;
                case Actions.SIT_RIGHT:
                    attributes = DEITY_TYPE_WIND;
                    break;
                //				case Actions.BUILD_PLAN_WIDE_STAIRCASE:                          attributes = ;break;
                //				case Actions.BUILD_PLAN_RIGHT_STAIRCASE:                         attributes = ;break;
                //				case Actions.BUILD_PLAN_LEFT_STAIRCASE:                          attributes = ;break;
                case Actions.BURY_ALL:
                    attributes = DEITY_TYPE_HUNTING;
                    break;
                //				case Actions.STAND_UP:                                           attributes = ;break;
                //				case Actions.BUILD_PLAN_CLOCKWISE_STAIRCASE:                     attributes = ;break;
                //				case Actions.BUILD_PLAN_ANTICLOCKWISE_STAIRCASE:                 attributes = ;break;
                //				case Actions.BUILD_PLAN_CLOCKWISE_STAIRCASE_WITH_BANISTER:       attributes = ;break;
                //				case Actions.BUILD_PLAN_ANTICLOCKWISE_STAIRCASE_WITH_BANISTER:   attributes = ;break;
                //				case Actions.BUILD_PLAN_WIDE_STAIRCASE_RIGHT_BANISTER:           attributes = ;break;
                //				case Actions.BUILD_PLAN_WIDE_STAIRCASE_LEFT_BANISTER:            attributes = ;break;
                //				case Actions.BUILD_PLAN_WIDE_STAIRCASE_BOTH_BANISTER:            attributes = ;break;
                //				case Actions.TARGET_HOSTILE:                                     attributes = ;break;
                case Actions.PLOT_COURSE:
                    attributes = DEITY_TYPE_STAR;
                    break;
                //				case Actions.DEFAULT_TERRAFORMING:                               attributes = ;break;
                //				case Actions.KINGDOM_MEMBER_LIST:                                attributes = ;break;
                //				case Actions.BOTANIZE_SPICES:                                    attributes = ;break;
                //				case Actions.GM_SET_MEDPATH:                                     attributes = ;break;
                case Actions.CHANGE_PATH:
                    attributes = DEITY_TYPE_STAR;
                    break;
                //				case Actions.EQUIP_TIMED:                                        attributes = ;break;
                //				case Actions.EQUIP_TIMED_AUTO:                                   attributes = ;break;
                //				case Actions.SET_VOLUME_1:                                       attributes = ;break;
                //				case Actions.SET_VOLUME_2:                                       attributes = ;break;
                //				case Actions.SET_VOLUME_5:                                       attributes = ;break;
                //				case Actions.SET_VOLUME_10:                                      attributes = ;break;
                //				case Actions.SET_VOLUME_20:                                      attributes = ;break;
                //				case Actions.SET_VOLUME_50:                                      attributes = ;break;
                //				case Actions.SET_VOLUME_100:                                     attributes = ;break;
                //				case Actions.SET_VOLUME_200:                                     attributes = ;break;
                //				case Actions.SET_VOLUME_500:                                     attributes = ;break;
                //				case Actions.SET_VOLUME_1000:                                    attributes = ;break;
                //				case Actions.SET_VOLUME_2000:                                    attributes = ;break;
                //				case Actions.SET_VOLUME_5000:                                    attributes = ;break;
                //				case Actions.SET_VOLUME_10000:                                   attributes = ;break;
                //				case Actions.COOKBOOK:                                           attributes = ;break;
                //				case Actions.SEAL:                                               attributes = ;break;
                //				case Actions.UNSEAL:                                             attributes = ;break;
                //				case Actions.COLLECT:                                            attributes = ;break;
                //				case Actions.WRITE_RECIPE:                                       attributes = ;break;
                //				case Actions.READ_RECIPE:                                        attributes = ;break;
                //				case Actions.ADD_RECIPE:                                         attributes = ;break;
                //				case Actions.THROW_PLAYFUL:                                      attributes = ;break;
                //				case Actions.PLANT_LEFT:                                         attributes = ;break;
                //				case Actions.PLANT_RIGHT:                                        attributes = ;break;
                //				case Actions.VIEW_LINKS:                                         attributes = ;break;
                //				case Actions.SET_LINK_NORTH:                                     attributes = ;break;
                //				case Actions.SET_LINK_NORTH_EAST:                                attributes = ;break;
                //				case Actions.SET_LINK_EAST:                                      attributes = ;break;
                //				case Actions.SET_LINK_SOUTH_EAST:                                attributes = ;break;
                //				case Actions.SET_LINK_SOUTH:                                     attributes = ;break;
                //				case Actions.SET_LINK_SOUTH_WEST:                                attributes = ;break;
                //				case Actions.SET_LINK_WEST:                                      attributes = ;break;
                //				case Actions.SET_LINK_NORTH_WEST:                                attributes = ;break;
                case Actions.PRY:
                    attributes = DEITY_TYPE_TRAVEL;
                    break;
                //				case Actions.FIND_ROUTE:                                         attributes = ;break;
                //				case Actions.VIEW_PROTECTION:                                    attributes = ;break;
                //				case Actions.BUILD_WOOD_ARCHED_LEFT:                             attributes = ;break;
                //				case Actions.BUILD_WOOD_ARCHED_RIGHT:                            attributes = ;break;
                //				case Actions.BUILD_WOOD_ARCHED_T:                                attributes = ;break;
                //				case Actions.BUILD_STONE_ARCHED_LEFT:                            attributes = ;break;
                //				case Actions.BUILD_STONE_ARCHED_RIGHT:                           attributes = ;break;
                //				case Actions.BUILD_STONE_ARCHED_T:                               attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_ARCHED_LEFT:                    attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_ARCHED_RIGHT:                   attributes = ;break;
                //				case Actions.BUILD_TIMBER_FRAMED_ARCHED_T:                       attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_ARCHED_LEFT:                      attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_ARCHED_RIGHT:                     attributes = ;break;
                //				case Actions.BUILD_PLAIN_STONE_ARCHED_T:                         attributes = ;break;
                //				case Actions.BUILD_SLATE_WALL:                                   attributes = ;break;
                //				case Actions.BUILD_SLATE_WINDOW:                                 attributes = ;break;
                //				case Actions.BUILD_SLATE_NARROW_WINDOW:                          attributes = ;break;
                //				case Actions.BUILD_SLATE_DOOR:                                   attributes = ;break;
                //				case Actions.BUILD_SLATE_DOUBLE_DOOR:                            attributes = ;break;
                //				case Actions.BUILD_SLATE_ARCHED_WALL:                            attributes = ;break;
                //				case Actions.BUILD_SLATE_PORTCULLIS:                             attributes = ;break;
                //				case Actions.BUILD_SLATE_BARRED_WALL:                            attributes = ;break;
                //				case Actions.BUILD_SLATE_ORIEL:                                  attributes = ;break;
                //				case Actions.BUILD_SLATE_ARCHED_LEFT:                            attributes = ;break;
                //				case Actions.BUILD_SLATE_ARCHED_RIGHT:                           attributes = ;break;
                //				case Actions.BUILD_SLATE_ARCHED_T:                               attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_WALL:                           attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_WINDOW:                         attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_NARROW_WINDOW:                  attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_DOOR:                           attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_DOUBLE_DOOR:                    attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_ARCHED_WALL:                    attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_PORTCULLIS:                     attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_BARRED_WALL:                    attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_ORIEL:                          attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_ARCHED_LEFT:                    attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_ARCHED_RIGHT:                   attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_ARCHED_T:                       attributes = ;break;
                //				case Actions.BUILD_POTTERY_WALL:                                 attributes = ;break;
                //				case Actions.BUILD_POTTERY_WINDOW:                               attributes = ;break;
                //				case Actions.BUILD_POTTERY_NARROW_WINDOW:                        attributes = ;break;
                //				case Actions.BUILD_POTTERY_DOOR:                                 attributes = ;break;
                //				case Actions.BUILD_POTTERY_DOUBLE_DOOR:                          attributes = ;break;
                //				case Actions.BUILD_POTTERY_ARCHED_WALL:                          attributes = ;break;
                //				case Actions.BUILD_POTTERY_PORTCULLIS:                           attributes = ;break;
                //				case Actions.BUILD_POTTERY_BARRED_WALL:                          attributes = ;break;
                //				case Actions.BUILD_POTTERY_ORIEL:                                attributes = ;break;
                //				case Actions.BUILD_POTTERY_ARCHED_LEFT:                          attributes = ;break;
                //				case Actions.BUILD_POTTERY_ARCHED_RIGHT:                         attributes = ;break;
                //				case Actions.BUILD_POTTERY_ARCHED_T:                             attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_WALL:                               attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_WINDOW:                             attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_NARROW_WINDOW:                      attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_DOOR:                               attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_DOUBLE_DOOR:                        attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_ARCHED_WALL:                        attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_PORTCULLIS:                         attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_BARRED_WALL:                        attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_ORIEL:                              attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_ARCHED_LEFT:                        attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_ARCHED_RIGHT:                       attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_ARCHED_T:                           attributes = ;break;
                //				case Actions.BUILD_MARBLE_WALL:                                  attributes = ;break;
                //				case Actions.BUILD_MARBLE_WINDOW:                                attributes = ;break;
                //				case Actions.BUILD_MARBLE_NARROW_WINDOW:                         attributes = ;break;
                //				case Actions.BUILD_MARBLE_DOOR:                                  attributes = ;break;
                //				case Actions.BUILD_MARBLE_DOUBLE_DOOR:                           attributes = ;break;
                //				case Actions.BUILD_MARBLE_ARCHED_WALL:                           attributes = ;break;
                //				case Actions.BUILD_MARBLE_PORTCULLIS:                            attributes = ;break;
                //				case Actions.BUILD_MARBLE_BARRED_WALL:                           attributes = ;break;
                //				case Actions.BUILD_MARBLE_ORIEL:                                 attributes = ;break;
                //				case Actions.BUILD_MARBLE_ARCHED_LEFT:                           attributes = ;break;
                //				case Actions.BUILD_MARBLE_ARCHED_RIGHT:                          attributes = ;break;
                //				case Actions.BUILD_MARBLE_ARCHED_T:                              attributes = ;break;
                //				case Actions.BUILD_SLATE_FENCE:                                  attributes = ;break;
                //				case Actions.BUILD_SLATE_IRON_FENCE:                             attributes = ;break;
                //				case Actions.BUILD_SLATE_IRON_FENCE_GATE:                        attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_FENCE:                          attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_IRON_FENCE:                     attributes = ;break;
                //				case Actions.BUILD_ROUNDED_STONE_IRON_FENCE_GATE:                attributes = ;break;
                //				case Actions.BUILD_POTTERY_FENCE:                                attributes = ;break;
                //				case Actions.BUILD_POTTERY_IRON_FENCE:                           attributes = ;break;
                //				case Actions.BUILD_POTTERY_IRON_FENCE_GATE:                      attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_FENCE:                              attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_IRON_FENCE:                         attributes = ;break;
                //				case Actions.BUILD_SANDSTONE_IRON_FENCE_GATE:                    attributes = ;break;
                //				case Actions.BUILD_MARBLE_FENCE:                                 attributes = ;break;
                //				case Actions.BUILD_MARBLE_IRON_FENCE:                            attributes = ;break;
                //				case Actions.BUILD_MARBLE_IRON_FENCE_GATE:                       attributes = ;break;
                //				case Actions.TOGGLE_RENDER:                                      attributes = ;break;
                //				case Actions.TOGGLE_HALF_ARCH:                                   attributes = ;break;
                //				case Actions.INCREASE_ANGLE:                                     attributes = ;break;
                //				case Actions.REDUCE_ANGLE:                                       attributes = ;break;
                //				case Actions.RAM:                                                attributes = ;break;
                //				case Actions.STUDY:                                              attributes = ;break;
                //				case Actions.RECORD:                                             attributes = ;break;
                case Actions.ADD_FOLDER:
                    attributes = DEITY_TYPE_CHAOS;
                    break;
                //				case Actions.REMOVE_FOLDER:                                      attributes = ;break;
                //				case Actions.REINFORCE_STONE:                                    attributes = ;break;
                //				case Actions.REINFORCE_SLATE:                                    attributes = ;break;
                //				case Actions.REINFORCE_POTTERY:                                  attributes = ;break;
                //				case Actions.REINFORCE_ROUNDED_STONE:                            attributes = ;break;
                //				case Actions.REINFORCE_SANDSTONE:                                attributes = ;break;
                //				case Actions.REINFORCE_MARBLE:                                   attributes = ;break;
                //				case Actions.REINFORCE_WOOD:                                     attributes = ;break;
                //				case Actions.MANAGE_WAGONER:                                     attributes = ;break;
                //				case Actions.MOVE_CENTER:                                        attributes = ;break;
                //				case Actions.LEVEL_BORDER:                                       attributes = ;break;
                //				case Actions.BUILD_ALL_WALL_PLANS:                               attributes = ;break;
                //				case Actions.BUILD_SCAFFOLDING_OPENING:                          attributes = ;break;
                //				case Actions.BUILD_SCAFFOLDING_FLOOR:                            attributes = ;break;
                //				case Actions.BUILD_SCAFFOLDING_SUPPORT:                          attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_TALL_STONE_WALL:                  attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_PORTCULLIS:                       attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_HIGH_IRON_FENCE:                  attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_HIGH_IRON_FENCE_GATE:             attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_STONE_PARAPET:                    attributes = ;break;
                //				case Actions.BUILD_FENCE_SLATE_CHAIN_FENCE:                      attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_TALL_STONE_WALL:          attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_PORTCULLIS:               attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_HIGH_IRON_FENCE:          attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_HIGH_IRON_FENCE_GATE:     attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_STONE_PARAPET:            attributes = ;break;
                //				case Actions.BUILD_FENCE_ROUNDED_STONE_CHAIN_FENCE:              attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_TALL_STONE_WALL:              attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_PORTCULLIS:                   attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_HIGH_IRON_FENCE:              attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_HIGH_IRON_FENCE_GATE:         attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_STONE_PARAPET:                attributes = ;break;
                //				case Actions.BUILD_FENCE_SANDSTONE_CHAIN_FENCE:                  attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_TALL_STONE_WALL:               attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_PORTCULLIS:                    attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_HIGH_IRON_FENCE:               attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_HIGH_IRON_FENCE_GATE:          attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_STONE_PARAPET:                 attributes = ;break;
                //				case Actions.BUILD_FENCE_RENDERED_CHAIN_FENCE:                   attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_TALL_STONE_WALL:                attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_PORTCULLIS:                     attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_HIGH_IRON_FENCE:                attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_HIGH_IRON_FENCE_GATE:           attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_STONE_PARAPET:                  attributes = ;break;
                //				case Actions.BUILD_FENCE_POTTERY_CHAIN_FENCE:                    attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_TALL_STONE_WALL:                 attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_PORTCULLIS:                      attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_HIGH_IRON_FENCE:                 attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_HIGH_IRON_FENCE_GATE:            attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_STONE_PARAPET:                   attributes = ;break;
                //				case Actions.BUILD_FENCE_MARBLE_CHAIN_FENCE:                     attributes = ;break;
                //				case Actions.REMOVE_MINEDOOR:                                    attributes = ;break;
                //          case Actions.LOAD_CREATURE:
                //				case Actions.UNLOAD_CREATURE:                                    attributes = ;break;
                //				case Actions.GETSTOREDCREATUREINFO:                              attributes = ;break;
                //				case Actions.INVESTIGATE:                                        attributes = ;break;
                //				case Actions.IDENTIFY_ITEM:                                      attributes = ;break;
                //				case Actions.COMBINE_FRAGMENT:                                   attributes = ;break;
                //				case Actions.ON_DESTROY:                                         attributes = ;break;
                //				case Actions.TRANSFERBULKITEMS:                                  attributes = ;break;
                //				case Actions.SETUP_DELIVERY:                                     attributes = ;break;
                //				case Actions.MANAGE_DELIVERIES:                                  attributes = ;break;
                //				case Actions.CANCEL_DELIVERY:                                    attributes = ;break;
                //				case Actions.VIEW_DELIVERY:                                      attributes = ;break;
                //				case Actions.WAGONER_HISTORY:                                    attributes = ;break;
                //				case Actions.WAGONER_DISMISS:                                    attributes = ;break;
                //				case Actions.DIG_TO_PILE:                                        attributes = ;break;
                case Actions.REMOVE_ASH:
                    attributes = DEITY_TYPE_FIRE;
                    break;
                //				case Actions.COLOR2:                                             attributes = ;break;
                case Actions.COLOR2_REMOVE:
                    attributes = DEITY_TYPE_ARTS;
                    break;
                //				case Actions.PLACE_ITEM:                                         attributes = ;break;
                //				case Actions.PLACE_ITEM_LARGE:                                   attributes = ;break;
                //				case Actions.SWITCH_TILE_TYPE:                                   attributes = ;break;
            }
            if(attributes != 0L) {
                try {
                    entryAttributesField.set(ae, attributes);
                } catch(IllegalAccessException e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
        logger.info("Updated adept restrictions for action entries.");
    }

    @SuppressWarnings("unused")
    public static boolean addDeity(Deity deity) {
        logger.info("Init deity " + deity.getName());
        long attributes = 0L;
        if(deities == null) {
            try {
                Field field = Deities.class.getDeclaredField("deities");
                field.setAccessible(true);
                deities = (Map<Integer, Deity>) field.get(null);
            } catch(NoSuchFieldException | IllegalAccessException e) {
                logger.log(Level.SEVERE, "Religion.addDeity: " + e.getMessage(), e);
                deities = null;
                return false;
            }
        }
        if(deityAttributesField == null) {
            try {
                deityAttributesField = Deity.class.getDeclaredField("attributes");
                deityAttributesField.setAccessible(true);
            } catch(NoSuchFieldException e) {
                return false;
            }
        }
        if(deity.number == DEITY_SEDAES) {
            attributes = DEITY_TYPE_SUN | DEITY_TYPE_WATER | DEITY_TYPE_HEALING | DEITY_TYPE_WISDOM | DEITY_TYPE_FATE;
            deity.convertText1 = SEDAES_CONV;
            deity.altarConvertText1 = SEDAES_ALT;
        } else if(deity.number == DEITY_PAYREN) {
            attributes = DEITY_TYPE_NATURE | DEITY_TYPE_FOREST | DEITY_TYPE_ANIMALS | DEITY_TYPE_HUNTING | DEITY_TYPE_TRICKSTER;
            deity.convertText1 = PAYREN_CONV;
            deity.altarConvertText1 = PAYREN_ALT;
        } else if(deity.number == DEITY_EILANDA) {
            attributes = DEITY_TYPE_MOON | DEITY_TYPE_WIND | DEITY_TYPE_EARTH | DEITY_TYPE_TRAVEL | DEITY_TYPE_SLEEP;
            deity.convertText1 = EILANDA_CONV;
            deity.altarConvertText1 = EILANDA_ALT;
        } else if(deity.number == DEITY_SHONIN) {
            attributes = DEITY_TYPE_STAR | DEITY_TYPE_THUNDER | DEITY_TYPE_METAL | DEITY_TYPE_HONOR | DEITY_TYPE_WAR;
            deity.convertText1 = SHONIN_CONV;
            deity.altarConvertText1 = SHONIN_ALT;
        } else if(deity.number == DEITY_ASMAN) {
            attributes = DEITY_TYPE_WATER | DEITY_TYPE_ARTISAN | DEITY_TYPE_WISDOM | DEITY_TYPE_MAGIC | DEITY_TYPE_WEALTH;
            deity.convertText1 = ASMAN_CONV;
            deity.altarConvertText1 = ASMAN_ALT;
        } else if(deity.number == DEITY_KIANE) {
            attributes = DEITY_TYPE_SKY | DEITY_TYPE_RAIN | DEITY_TYPE_MOUNTAIN | DEITY_TYPE_CREATOR | DEITY_TYPE_ARTS;
            deity.convertText1 = KIANE_CONV;
            deity.altarConvertText1 = KIANE_ALT;
        } else if(deity.number == DEITY_MIRIDON) {
            attributes = DEITY_TYPE_METAL | DEITY_TYPE_JUSTICE | DEITY_TYPE_WAR | DEITY_TYPE_FIRE | DEITY_TYPE_DESTROYER | DEITY_TYPE_HATE;
            deity.convertText1 = MIRIDON_CONV;
            deity.altarConvertText1 = MIRIDON_ALT;
        } else if(deity.number == DEITY_LAPHANER) {
            attributes = DEITY_TYPE_STAR | DEITY_TYPE_FATE | DEITY_TYPE_ARTS | DEITY_TYPE_TRAVEL | DEITY_TYPE_LOVE | DEITY_TYPE_HATE;
            deity.convertText1 = LAPHANER_CONV;
            deity.altarConvertText1 = LAPHANER_ALT;
        } else if(deity.number == DEITY_DRAKKAR) {
            attributes = DEITY_TYPE_MOON | DEITY_TYPE_WIND | DEITY_TYPE_SLEEP | DEITY_TYPE_DEATH | DEITY_TYPE_CHAOS | DEITY_TYPE_HATE;
            deity.convertText1 = DRAKKAR_CONV;
            deity.altarConvertText1 = DRAKKAR_ALT;
        } else if(deity.number == DEITY_QALESHIN) {
            attributes = DEITY_TYPE_FIRE | DEITY_TYPE_MAGIC | DEITY_TYPE_MOUNTAIN | DEITY_TYPE_WIND | DEITY_TYPE_WISDOM;
            deity.convertText1 = QALESHIN_CONV;
            deity.altarConvertText1 = QALESHIN_ALT;
        } else {
            return false;
        }

        deity.setTemplateDeity(deity.getNumber());

        if((attributes & DEITY_TYPE_TRAVEL) != 0)
            deity.setRoadProtector(true);       /* Weight of carried items on road is 50% */
        if((attributes & DEITY_TYPE_ARTISAN) != 0)
            deity.setBuildWallBonus(20.0f);     /* Alignment gain when building walls */
        //		if((attributes&DEITY_TYPE_DESTROYER)!=0)  deity.destroyWallBonus    = 10.0f;  /* Not used */
        //		if((attributes&DEITY_TYPE_HONOR)!=0)      deity.enemyBonus          = 30.0f;  /* Not used */
        //		if((attributes&DEITY_TYPE_THUNDER)!=0)    deity.monsterBonus        = 20.0f;  /* Not used */
        //		if((attributes&DEITY_TYPE_EARTH)!=0)      deity.butcheringBonus     = 0.3f;   /* Not used */
        //		if((attributes&DEITY_TYPE_SUN)!=0)        deity.treeBonus           = 10.0f;  /* Not used */
        if((attributes & DEITY_TYPE_WAR) != 0) deity.setWarrior(true);             /* Does 25% more damage */
        //		if((attributes&DEITY_TYPE_FATE)!=0)       deity.killEnemy           = true;   /* Not used */
        if((attributes & DEITY_TYPE_ANIMALS) != 0) deity.setBefriendCreature(true);    /* Aggro animals are friendly */
        if((attributes & DEITY_TYPE_WIND) != 0) deity.setStaminaBonus(true);        /* Faster stamina regain */
        if((attributes & DEITY_TYPE_FERTILITY) != 0) deity.setFoodBonus(true);           /* Food feeds more */
        if((attributes & DEITY_TYPE_HEALING) != 0) deity.setHealer(true);              /* Heal more each poll */
        if((attributes & DEITY_TYPE_DEATH) != 0)
            deity.setDeathProtector(true);      /* 50% less skill loss on death, keeps items on death */
        //		if((attributes&DEITY_TYPE_JUSTICE)!=0)    deity.deathItemProtector  = true;   /* Not used */
        if((attributes & DEITY_TYPE_CHAOS) != 0)
            deity.setAllowsButchering(true);    /* Allows butchering human corpses, no burying or create grave */
        if((attributes & DEITY_TYPE_FIRE) != 0)
            deity.setWoodAffinity(true);        /* Double favor when sacrificing wood items */
        if((attributes & DEITY_TYPE_METAL) != 0)
            deity.setMetalAffinity(true);       /* Double favor when sacrificing metal items */
        if((attributes & DEITY_TYPE_LOVE) != 0)
            deity.setClothAffinity(true);       /* Double favor when sacrificing cloth items */
        if((attributes & DEITY_TYPE_ARTS) != 0)
            deity.setClayAffinity(true);        /* Double favor when sacrificing pottery items */
        //		if((attributes&DEITY_TYPE_WEALTH)!=0)     deity.coinAffinity        = true;   /* Not used */
        if((attributes & DEITY_TYPE_HUNTING) != 0)
            deity.setMeatAffinity(true);        /* Higher favor when sacrificing corpses and butchered items */
        if((attributes & DEITY_TYPE_NATURE) != 0)
            deity.setFoodAffinity(true);        /* Higher favor when sacrificing food */
        if((attributes & DEITY_TYPE_WISDOM) != 0) deity.setLearner(true);             /* 10% added to skill gains */
        if((attributes & DEITY_TYPE_STAR) != 0)
            deity.setItemProtector(true);       /* At 70+ faith and 35+ favor only decay tick every 1 in 5 for owned items */
        if((attributes & DEITY_TYPE_ARTISAN) != 0) deity.setRepairer(true);            /* Added bonus when repairing */
        //		if((attributes&DEITY_TYPE_SKY)!=0)        deity.treeProtector       = true;   /* Not used */
        if((attributes & DEITY_TYPE_WATER) != 0)
            deity.setWaterGod(true);            /* God bonus for Vyn-rune, pray on water tile */
        if((attributes & DEITY_TYPE_MOUNTAIN) != 0)
            deity.setMountainGod(true);         /* God bonus for Mag-rune, pray on rock tiles, faith 35+ not burned by lava, on rock and sand items weigh 50% */
        if((attributes & DEITY_TYPE_FOREST) != 0)
            deity.setForestGod(true);           /* God bonus for Fo-rune, pray on tree tiles, faith 35+ not pierced by thorns, faith 70+ items weigh 50% on grass, trees, fields, dirt tiles */
        if((attributes & DEITY_TYPE_HATE) != 0)
            deity.setHateGod(true);             /* God bonus for Lib-rune, pray at human corpses, negative alignment, faith 60+ favor 30+ items weigh 50% on mycelium */


        try {
            deityAttributesField.set(deity, attributes);
        } catch(IllegalAccessException e) {
            logger.log(Level.SEVERE, "Religion.addDeity: " + e.getMessage(), e);
            return false;
        }
        deities.put(deity.number, deity);
        return true;
    }

    /**
     * Restrictions for followers:
     */
    @SuppressWarnings("unused")
    public static boolean isActionFaithful(Deity deity, int num) {
        //		if(num==Actions.DESTROY_PAVE) return !deity.roadProtector;
        //		if(num==Actions.WALL_DESTROY || num==Actions.FENCE_DESTROY || num==Actions.FLOOR_DESTROY) return deity.buildWallBonus<=0.0f;
        return true;
    }

/*	public static boolean isActionFaithful(Deity deity,Action action,long deityAttr,long entryAttr) {
		if(deityAttr>0L && (deityAttr&entryAttr)!=0) return false;
		final int num = action.getNumber();
		if(num==Actions.DESTROY_PAVE) return !deity.roadProtector;
		if(num==Actions.WALL_DESTROY || num==Actions.FENCE_DESTROY || num==Actions.FLOOR_DESTROY) return deity.buildWallBonus<=0.0f;
		return true;
	}*/

    /**
     * Restrictions for adepts:
     */
    @SuppressWarnings("unused")
    public static boolean doAdeptCheck(Action action, ActionEntry entry, Creature performer, Deity deity, Item source, long deityAttr, long entryAttr) {
        if(!performer.isChampion() && (entry.getNumber() < 2000 || entry.getNumber() > 8000)) {
            if(!isAdeptAllowed(action, entry, performer, deity, source, deityAttr, entryAttr)) {
                if(performer.faithful) {
                    performer.getCommunicator().sendNormalServerMessage(performer.getDeity().name + " would not approve of that.");
                    return true;
                } else {
                    performer.modifyFaith(-0.1f);
                }
            }
        }
        return false;
    }

    /**
     * Restrictions for adepts:
     */
    private static boolean isAdeptAllowed(Action action, ActionEntry entry, Creature performer, Deity deity, Item source, long deityAttr, long entryAttr) {
        //      logger.info("isAdeptAllowed: action="+entry.getActionString()+", deityAttr="+deityAttr+", entryAttr="+entryAttr);
        //		if(entry.isNonReligion()) {
        if(entry.getNumber() == Actions.IMPROVE ||
           entry.getNumber() == Actions.CREATE ||
           entry.getNumber() == Actions.CONTINUE_BUILDING ||
           entry.getNumber() == Actions.CONTINUE_BUILDING_FENCE) {
            if(action.getTarget() != -10L && action.getTargetType() == 2) {
                try {
                    final Item target = Items.getItem(action.getTarget());
                    final boolean targetWater = target.getTemplateId() == ItemList.water;
                    final Item toImprove = targetWater? source : target;
                    final Item improveItem = targetWater? target : source;
                    final int skillId = MethodsItems.getImproveSkill(toImprove);
                    final Long skillAttr = skillAttributes.get(skillId);
                    if(skillAttr != null) {
                        //                  logger.info("Improve "+toImprove.getName()+" with "+improveItem.getName()+" using skill "+skillId+", skillAttr="+skillAttr);
                        if((skillAttr & entryAttr) != 0) return false;
                    }
                } catch(NoSuchItemException e) { }
            }
        } else {
            return (deityAttr & entryAttr) == 0;
        }
        //		}
        return true;
    }

    public static int getHolyItem(Deity deity) {
        if(deity != null) {
            switch(deity.number) {
                case Religion.DEITY_SEDAES:
                    return ItemTemplateCreatorAwakening.sedaesSword;
                case Religion.DEITY_PAYREN:
                    return ItemTemplateCreatorAwakening.payrenStaff;
                case Religion.DEITY_EILANDA:
                    return ItemTemplateCreatorAwakening.eilandaSpear;
                case Religion.DEITY_SHONIN:
                    return ItemTemplateCreatorAwakening.shoninSword;
                case Religion.DEITY_ASMAN:
                    return ItemTemplateCreatorAwakening.asmanStaff;
                case Religion.DEITY_KIANE:
                    return ItemTemplateCreatorAwakening.kianeStaff;
                case Religion.DEITY_MIRIDON:
                    return ItemTemplateCreatorAwakening.miridonAxe;
                case Religion.DEITY_LAPHANER:
                    return ItemTemplateCreatorAwakening.laphanerStaff;
                case Religion.DEITY_DRAKKAR:
                    return ItemTemplateCreatorAwakening.drakkarKnife;
                case Religion.DEITY_QALESHIN:
                    return ItemTemplateCreatorAwakening.qaleshinStaff;
            }
        }
        return -1;
    }

    public static boolean isHolyItem(Deity deity, Item item) {
        return item != null && item.getTemplateId() == getHolyItem(deity);
    }

    public static boolean isHolyItem(Item item) {
        if(item == null) return false;
        int id = item.getTemplateId();
        return id == ItemTemplateCreatorAwakening.sedaesSword || id == ItemTemplateCreatorAwakening.payrenStaff ||
               id == ItemTemplateCreatorAwakening.eilandaSpear || id == ItemTemplateCreatorAwakening.shoninSword ||
               id == ItemTemplateCreatorAwakening.asmanStaff || id == ItemTemplateCreatorAwakening.kianeStaff ||
               id == ItemTemplateCreatorAwakening.miridonAxe || id == ItemTemplateCreatorAwakening.laphanerStaff ||
               id == ItemTemplateCreatorAwakening.drakkarKnife || id == ItemTemplateCreatorAwakening.qaleshinStaff;
    }

    @SuppressWarnings("unused")
    public static boolean isHolyItem_journalFix(Creature performer, Item source, long targetId, Item newItem) {
        if(performer.isPlayer() && newItem.getTemplate().getTemplateId() == ItemList.statuette && newItem.getMaterial() == Materials.MATERIAL_ELECTRUM) {
            Item lump = source;
            if(lump.getTemplate().getTemplateId() != ItemList.electrumBar) {
                lump = Items.getItemOptional(targetId).orElse(null);
                if(lump == null || lump.getTemplate().getTemplateId() != ItemList.electrumBar) return false;
            }
            Deity blesser = lump.getBless();
            if(blesser != null && performer.getDeity() != null && blesser.number == performer.getDeity().number) {
                newItem.bless(blesser.number);
                performer.getCommunicator().sendNormalServerMessage("The blessing of the lump transfers to the " + newItem.getName() + ", which is now aligned with " + blesser.getName() + ".");
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("unused")
    public static void setDeityFavor(Deity deity, int newFavor) {
        if(deity.favor < newFavor) {
            deity.favor += Math.round((double) (newFavor - deity.favor) * Config.deityFavorMultiplier);
        } else {
            deity.favor = newFavor;
        }
        if(deity.favor % 20 == 0) {
            Connection dbcon = null;
            PreparedStatement ps = null;
            try {
                dbcon = DbConnector.getDeityDbCon();
                ps = dbcon.prepareStatement("UPDATE DEITIES SET FAVOR=? WHERE ID=?");
                ps.setDouble(1, deity.favor);
                ps.setByte(2, (byte) deity.number);
                ps.executeUpdate();
            } catch(SQLException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            } finally {
                DbUtilities.closeDatabaseObjects(ps, null);
                DbConnector.returnConnection(dbcon);
            }
        }
    }

    //	static {
    //		skillAttributes.put(SkillList.CHARACTERISTICS,                0L);
    //		skillAttributes.put(SkillList.FAITH,                          0L);
    //		skillAttributes.put(SkillList.FAVOR,                          0L);
    //		skillAttributes.put(SkillList.RELIGION,                       0L);
    //		skillAttributes.put(SkillList.ALIGNMENT,                      0L);
    //		skillAttributes.put(SkillList.BODY,                           0L);
    //		skillAttributes.put(SkillList.MIND,                           0L);
    //		skillAttributes.put(SkillList.SOUL,                           0L);
    //		skillAttributes.put(SkillList.MIND_LOGICAL,                   0L);
    //		skillAttributes.put(SkillList.MIND_SPEED,                     0L);
    //		skillAttributes.put(SkillList.BODY_STRENGTH,                  0L);
    //		skillAttributes.put(SkillList.BODY_STAMINA,                   0L);
    //		skillAttributes.put(SkillList.BODY_CONTROL,                   0L);
    //		skillAttributes.put(SkillList.SOUL_STRENGTH,                  0L);
    //		skillAttributes.put(SkillList.SOUL_DEPTH,                     0L);
    //		skillAttributes.put(SkillList.GROUP_SWORDS,                   0L);
    //		skillAttributes.put(SkillList.GROUP_KNIVES,                   0L);
    //		skillAttributes.put(SkillList.GROUP_SHIELDS,                  0L);
    //		skillAttributes.put(SkillList.GROUP_AXES,                     0L);
    //		skillAttributes.put(SkillList.GROUP_MAULS,                    0L);
    //		skillAttributes.put(SkillList.CARPENTRY,                      0L);
    //		skillAttributes.put(SkillList.WOODCUTTING,                    0L);
    //		skillAttributes.put(SkillList.MINING,                         0L);
    //		skillAttributes.put(SkillList.DIGGING,                        0L);
    //		skillAttributes.put(SkillList.FIREMAKING,                     0L);
    //		skillAttributes.put(SkillList.POTTERY,                        0L);
    //		skillAttributes.put(SkillList.GROUP_TAILORING,                0L);
    //		skillAttributes.put(SkillList.MASONRY,                        0L);
    //		skillAttributes.put(SkillList.ROPEMAKING,                     0L);
    //		skillAttributes.put(SkillList.GROUP_SMITHING,                 0L);
    //		skillAttributes.put(SkillList.GROUP_SMITHING_WEAPONSMITHING,  0L);
    //		skillAttributes.put(SkillList.GROUP_SMITHING_ARMOURSMITHING,  0L);
    //		skillAttributes.put(SkillList.GROUP_COOKING,                  0L);
    //		skillAttributes.put(SkillList.GROUP_NATURE,                   0L);
    //		skillAttributes.put(SkillList.MISCELLANEOUS,                  0L);
    //		skillAttributes.put(SkillList.GROUP_ALCHEMY,                  0L);
    //		skillAttributes.put(SkillList.GROUP_TOYS,                     0L);
    //		skillAttributes.put(SkillList.GROUP_FIGHTING,                 0L);
    //		skillAttributes.put(SkillList.GROUP_HEALING,                  0L);
    //		skillAttributes.put(SkillList.GROUP_CLUBS,                    0L);
    //		skillAttributes.put(SkillList.GROUP_RELIGION,                 0L);
    //		skillAttributes.put(SkillList.GROUP_HAMMERS,                  0L);
    //		skillAttributes.put(SkillList.GROUP_THIEVERY,                 0L);
    //		skillAttributes.put(SkillList.GROUP_WARMACHINES,              0L);
    //		skillAttributes.put(SkillList.GROUP_ARCHERY,                  0L);
    //		skillAttributes.put(SkillList.GROUP_BOWYERY,                  0L);
    //		skillAttributes.put(SkillList.GROUP_FLETCHING,                0L);
    //		skillAttributes.put(SkillList.GROUP_POLEARMS,                 0L);
    //		skillAttributes.put(SkillList.ARCHAEOLOGY,                    0L);
    //		skillAttributes.put(SkillList.AXE_SMALL,                      0L);
    //		skillAttributes.put(SkillList.SHOVEL,                         0L);
    //		skillAttributes.put(SkillList.HATCHET,                        0L);
    //		skillAttributes.put(SkillList.RAKE,                           0L);
    //		skillAttributes.put(SkillList.SWORD_LONG,                     0L);
    //		skillAttributes.put(SkillList.SHIELD_MEDIUM_METAL,            0L);
    //		skillAttributes.put(SkillList.KNIFE_CARVING,                  0L);
    //		skillAttributes.put(SkillList.SAW,                            0L);
    //		skillAttributes.put(SkillList.PICKAXE,                        0L);
    //		skillAttributes.put(SkillList.SMITHING_WEAPON_BLADES,         0L);
    //		skillAttributes.put(SkillList.SMITHING_WEAPON_HEADS,          0L);
    //		skillAttributes.put(SkillList.SMITHING_ARMOUR_CHAIN,          0L);
    //		skillAttributes.put(SkillList.SMITHING_ARMOUR_PLATE,          0L);
    //		skillAttributes.put(SkillList.SMITHING_SHIELDS,               0L);
    //		skillAttributes.put(SkillList.SMITHING_BLACKSMITHING,         0L);
    //		skillAttributes.put(SkillList.CLOTHTAILORING,                 0L);
    //		skillAttributes.put(SkillList.LEATHERWORKING,                 0L);
    //		skillAttributes.put(SkillList.TRACKING,                       0L);
    //		skillAttributes.put(SkillList.SHIELD_SMALL_WOOD,              0L);
    //		skillAttributes.put(SkillList.SHIELD_MEDIUM_WOOD,             0L);
    //		skillAttributes.put(SkillList.SHIELD_LARGE_WOOD,              0L);
    //		skillAttributes.put(SkillList.SHIELD_SMALL_METAL,             0L);
    //		skillAttributes.put(SkillList.SHIELD_LARGE_METAL,             0L);
    //		skillAttributes.put(SkillList.AXE_LARGE,                      0L);
    //		skillAttributes.put(SkillList.AXE_HUGE,                       0L);
    //		skillAttributes.put(SkillList.HAMMER,                         0L);
    //		skillAttributes.put(SkillList.SWORD_SHORT,                    0L);
    //		skillAttributes.put(SkillList.SWORD_TWOHANDED,                0L);
    //		skillAttributes.put(SkillList.KNIFE_BUTCHERING,               0L);
    //		skillAttributes.put(SkillList.STONE_CHISEL,                   0L);
    //		skillAttributes.put(SkillList.PAVING,                         0L);
    //		skillAttributes.put(SkillList.PROSPECT,                       0L);
    //		skillAttributes.put(SkillList.FISHING,                        0L);
    //		skillAttributes.put(SkillList.SMITHING_LOCKSMITHING,          0L);
    //		skillAttributes.put(SkillList.REPAIR,                         0L);
    //		skillAttributes.put(SkillList.COALING,                        0L);
    //		skillAttributes.put(SkillList.COOKING_DAIRIES,                0L);
    //		skillAttributes.put(SkillList.COOKING_STEAKING,               0L);
    //		skillAttributes.put(SkillList.COOKING_BAKING,                 0L);
    //		skillAttributes.put(SkillList.MILLING,                        0L);
    //		skillAttributes.put(SkillList.SMITHING_METALLURGY,            0L);
    //		skillAttributes.put(SkillList.ALCHEMY_NATURAL,                0L);
    //		skillAttributes.put(SkillList.SMITHING_GOLDSMITHING,          0L);
    //		skillAttributes.put(SkillList.CARPENTRY_FINE,                 0L);
    //		skillAttributes.put(SkillList.GARDENING,                      0L);
    //		skillAttributes.put(SkillList.SICKLE,                         0L);
    //		skillAttributes.put(SkillList.SCYTHE,                         0L);
    //		skillAttributes.put(SkillList.FORESTRY,                       0L);
    //		skillAttributes.put(SkillList.FARMING,                        0L);
    //		skillAttributes.put(SkillList.YOYO,                           0L);
    //		skillAttributes.put(SkillList.TOYMAKING,                      0L);
    //		skillAttributes.put(SkillList.WEAPONLESS_FIGHTING,            0L);
    //		skillAttributes.put(SkillList.FIGHT_AGGRESSIVESTYLE,          0L);
    //		skillAttributes.put(SkillList.FIGHT_DEFENSIVESTYLE,           0L);
    //		skillAttributes.put(SkillList.FIGHT_NORMALSTYLE,              0L);
    //		skillAttributes.put(SkillList.FIRSTAID,                       0L);
    //		skillAttributes.put(SkillList.TAUNTING,                       0L);
    //		skillAttributes.put(SkillList.SHIELDBASHING,                  0L);
    //		skillAttributes.put(SkillList.BUTCHERING,                     0L);
    //		skillAttributes.put(SkillList.MILKING,                        0L);
    //		skillAttributes.put(SkillList.MAUL_LARGE,                     0L);
    //		skillAttributes.put(SkillList.MAUL_MEDIUM,                    0L);
    //		skillAttributes.put(SkillList.MAUL_SMALL,                     0L);
    //		skillAttributes.put(SkillList.CLUB_HUGE,                      0L);
    //		skillAttributes.put(SkillList.PREACHING,                      0L);
    //		skillAttributes.put(SkillList.PRAYER,                         0L);
    //		skillAttributes.put(SkillList.CHANNELING,                     0L);
    //		skillAttributes.put(SkillList.EXORCISM,                       0L);
    //		skillAttributes.put(SkillList.WARHAMMER,                      0L);
    //		skillAttributes.put(SkillList.FORAGING,                       0L);
    //		skillAttributes.put(SkillList.BOTANIZING,                     0L);
    //		skillAttributes.put(SkillList.CLIMBING,                       0L);
    //		skillAttributes.put(SkillList.STONECUTTING,                   0L);
    //		skillAttributes.put(SkillList.STEALING,                       0L);
    //		skillAttributes.put(SkillList.LOCKPICKING,                    0L);
    //		skillAttributes.put(SkillList.CATAPULT,                       0L);
    //		skillAttributes.put(SkillList.TAMEANIMAL,                     0L);
    //		skillAttributes.put(SkillList.BOW_SHORT,                      0L);
    //		skillAttributes.put(SkillList.BOW_MEDIUM,                     0L);
    //		skillAttributes.put(SkillList.BOW_LONG,                       0L);
    //		skillAttributes.put(SkillList.SHIPBUILDING,                   0L);
    //		skillAttributes.put(SkillList.COOKING_BEVERAGES,              0L);
    //		skillAttributes.put(SkillList.TRAPS,                          0L);
    //		skillAttributes.put(SkillList.BREEDING,                       0L);
    //		skillAttributes.put(SkillList.MEDITATING,                     0L);
    //		skillAttributes.put(SkillList.PUPPETEERING,                   0L);
    //		skillAttributes.put(SkillList.SPEAR_LONG,                     0L);
    //		skillAttributes.put(SkillList.HALBERD,                        0L);
    //		skillAttributes.put(SkillList.STAFF,                          0L);
    //		skillAttributes.put(SkillList.PAPYRUSMAKING,                  0L);
    //		skillAttributes.put(SkillList.THATCHING,                      0L);
    //		skillAttributes.put(SkillList.BALLISTA,                       0L);
    //		skillAttributes.put(SkillList.TREBUCHET,                      0L);
    //	}

    private static final String[] SEDAES_CONV = {
            "Who is Sedaes you ask?",
            "I will tell you...",
            "An ageless woman dressed in brilliant white robes,",
            "her eyes green as emeralds.",
            "She carries a bronze bow,",
            "and above her head is the eternal flame as bright as the sun.",
            "Hers is the wisdom of the ages,",
            "and hers is the power of life and death.",
            "Join Sedaes and she will guide your path.",
            "A Witch you may become one day."
    };

    private static final String[] SEDAES_ALT = {
            "I am Sedaes, and mine is the wisdom of the ages and the power of life and death.",
            "You will see me as a woman beyond the ages, in white I am dressed, and my eyes are emerald green.",
            "I carry the bronze bow, and above my head is the eternal flame as bright as the sun.",
            "Join me and I will guide your path. A Witch you may become one day."
    };

    private static final String[] PAYREN_CONV = {
            "Who is Payren you ask?",
            "I will tell you...",
            "A male youth he is, on his head large black horns,",
            "one eye is entirely black, the other entirely white.",
            "His body is dressed in vines and leaves,",
            "and at his foot a large wolf.",
            "His is the understanding of the animals,",
            "and the forest, the wilderness and the hunt.",
            "Join Payren and he will guide your path.",
            "A Druid you may become one day."
    };

    private static final String[] PAYREN_ALT = {
            "I am Payren, and mine is the understanding of the animals, the forest, the wilderness and the hunt.",
            "You will see me as a male youth, crowned with two black horns because the king of the forest am I.",
            "My eyes are like the day and the night, and I wear the forest on my body. The wolf my trusted companion is.",
            "Join me and I will guide your path. A Druid you may become one day."
    };

    private static final String[] EILANDA_CONV = {
            "Who is Eilanda you ask?",
            "I will tell you...",
            "You see before you a red haired woman, with purple eyes.",
            "She is dressed in furs and leather.",
            "She is carrying a silver spear and a silver shield,",
            "and on her head a silver circlet with a diamond shaped amethyst.",
            "Hers is the vision into the unknown,",
            "the power to travel the spirit world and to command the spirits.",
            "Join Eilanda and she will guide your path.",
            "A Shaman you may become one day."
    };

    private static final String[] EILANDA_ALT = {
            "I am Eilanda, and mine is the vision into the unknown, and the power to travel the spirit world and I command the spirits.",
            "To your eyes I am a woman and my hair is red, and my eyes are purple. I am dressed in furs and leather.",
            "In my right hand I carry a silver spear, in my left a silver shield, on my head a silver circlet with a diamond shaped amethyst.",
            "Join me and I will guide your path. A Shaman you may become one day."
    };

    private static final String[] SHONIN_CONV = {
            "Who is Shonin you ask?",
            "I will tell you...",
            "A woman in full armour, wielding a greatsword,",
            "on the cheast plate a golden hawk is spreading its wings.",
            "Her hair is black like the night, and stars glitter there.",
            "Her eyes are like the lightning and her voice like the thunder.",
            "She commands the heavenly armies,",
            "hers is the power to kill her enemies swiftly, effortlessly and with honour.",
            "Join Shonin and she will guide your path.",
            "A Paladin you may become one day."
    };

    private static final String[] SHONIN_ALT = {
            "I am Shonin, and I command the heavenly armies, I kill my enemies swiftly, effortlessly and with honour.",
            "You see me as a woman with hair like the starry night sky, and my eyes are like the lightning and my voice like the thunder.",
            "I wear a full armour, and on the chest plate a golden hawk spreads its wings. In my right hand I wield a greatsword.",
            "Join me and I will guide your path. A Paladin you may become one day."
    };

    private static final String[] ASMAN_CONV = {
            "Who is Asman you ask?",
            "I will tell you...",
            "You see before you a man dressed in red robes,",
            "his hair completely white, and his eyes shining sapphire blue.",
            "He is pointing the right hand upward to the sky,",
            "and the left hand down towards the earth.",
            "His is the wisdom of the sky and the earth,",
            "his is the power to control the sky and the earth.",
            "Join Asman and he will guide your path.",
            "A Magus you may become one day."
    };

    private static final String[] ASMAN_ALT = {
            "I an Asman, mine is the wisdom of the sky and the earth, and mine is the power to control the sky and the earth.",
            "To your eyes I appear a man dressed in red robes, with brilliant white hair, and eyes of shining blue sapphires.",
            "My right hand pointing upwards to the sky, my left hand pointing down towards the earth.",
            "Join me and I will guide your path. A Magus you may become one day."
    };

    private static final String[] KIANE_CONV = {
            "Who is Kiane you ask?",
            "I will tell you...",
            "A man you see, wearing jet black robes and long grey hair.",
            "Also called 'The Old Man of the Mountain'.",
            "In his right hand he carries a staff with a black crystal,",
            "and in his left hand a lantern to see the hidden mysteries.",
            "His is the understanding of nature and life,",
            "and his is the sight of the oracle.",
            "Join Kiane and he will guide your path.",
            "A Mystic you may become one day."
    };

    private static final String[] KIANE_ALT = {
            "I am Kiane, mine is the understanding of nature and of life, and mine is the sight of the oracle.",
            "You will see me as an old man, dressed in jet black robes, my hair is long and grey.",
            "My right hand holds a staff with a black crystal, and my left hand holds a lantern for seeing hidden mysteries.",
            "Join me and I will guide your path. A Mystic you may become one day."
    };

    private static final String[] MIRIDON_CONV = {
            "Who is Miridon you ask?",
            "I will tell you...",
            "A muscular man, with glowing red eyes,",
            "and long black hair, you see before you.",
            "Dressed in molten lava, in his right hand he wields a fiery axe,",
            "and in his left the sickle with which to reap the lives of his victims.",
            "He is the bringer of justice, his is the might of the strong,",
            "and the will to dominate the weak and lawless.",
            "Join Miridon and he will guide your path.",
            "A Warlock you may become one day."
    };

    private static final String[] MIRIDON_ALT = {
            "I am Miridon, and I bring justice to the lawless, vengeance to the weak, and death to the oathbreakers.",
            "To your eyes I appear a man of strength, my eyes are glowing red, and my hair long and black.",
            "I am dressed in fire and molten lava, and I wield a flaming axe in my right hand, and in my left a sickle to reap the lives of my victims.",
            "Join me and I will guide your path. A Warlock you may become one day."
    };

    private static final String[] LAPHANER_CONV = {
            "Who is Laphaner you ask?",
            "I will tell you...",
            "She is beautiful beyond words, look at her and despair,",
            "her love perfect but something you will never have.",
            "Her gown is silken and pink, like her skin, and her hair pale like the Seris moon.",
            "Above her a starry sky, and below a pool of frozen water.",
            "In her right hand she holds a golden goblet, and in her left a jade apple.",
            "Her eyes are endlessly deep.",
            "Join Laphaner and she will guide your path.",
            "A Sorcerer you may become one day."
    };

    private static final String[] LAPHANER_ALT = {
            "I am Laphaner, my beauty is beyond your comprehension, look at me and despair, love me and give me your heart.",
            "I wear a silken gown, pink like the colour of my skin, and my hair is pale like the Seris moon. Above me is the starry sky, and below me a pool of frozen water.",
            "In my right hand I hold a golden goblet, and in my left lays a jade apple. Look into my eyes and fall endlessly.",
            "Join me and I will guide your path. A Sorcerer you may become one day."
    };

    private static final String[] DRAKKAR_CONV = {
            "Who is Drakkar you ask?",
            "I will tell you...",
            "You see before you neither man nor woman, a being beyond time, and beyond life and death.",
            "Skin marble white, eyes black, ears pointed and teeth sharp.",
            "The back has wings like those of a bat,",
            "and each finger and toe has a razor sharp claw of black crystal.",
            "The song of Drakkar is soothing and sweet, it melts all resistance of will,",
            "it makes dreams come true, it makes the dead come to life.",
            "Join Drakkar who will guide your path.",
            "A Necromancer you may become one day."
    };

    private static final String[] DRAKKAR_ALT = {
            "I am Drakkar, my song is soothing and sweet, all your resistance of will melts away, it makes your dreams come true, it makes the dead come to life.",
            "I am neither man nor woman, I am beyond time, and beyond life and death. My skin is marble white, my eyes black, and my ears pointed and teeth sharp.",
            "On my back I have wings like those of a bat, and each of my fingers and toes have razor sharp claws of black crystal.",
            "Join me and I will guide your path. A Necromancer you may become one day."
    };

    private static final String[] QALESHIN_CONV = {
            "Who is Qaleshin you ask?",
            "I will tell you...",
            "The great Dragon Queen, majestic and all powerful. Wisdom manifest.",
            "Her scales are golden, and the age of years beyond counting.",
            "Look into her draconian eyes and tremble,",
            "her breath will burn you to ash and purify your soul.",
            "The words of Qaleshin is the knowledge of ages,",
            "the truth of the world, and the understanding thereof.",
            "Join Qaleshin who will guide your path.",
            "A Dragonlord you may become one day."
    };

    private static final String[] QALESHIN_ALT = {
            "I am Qaleshin, my words speaks the knowledge and truth of the world, and the understanding thereof.",
            "I am the Queen of all dragons, but the ruler of none. My scales are golden, as I am the eldest of my kind.",
            "My wings fly me across the sky, and my fiery breath purifies all that it touches.",
            "Join me and I will guide your path. A Dragonlord you may become one day."
    };
}
