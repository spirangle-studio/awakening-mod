package com.wurmonline.server.spells;

import com.wurmonline.mesh.Tiles;
import com.wurmonline.mesh.Tiles.TileBorderDirection;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemTemplate;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.structures.*;
import com.wurmonline.server.villages.Village;
import com.wurmonline.server.villages.Villages;
import com.wurmonline.server.zones.VolaTile;
import com.wurmonline.server.zones.Zones;
import org.gotti.wurmunlimited.modsupport.actions.ActionEntryBuilder;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.logging.Logger;

public class HeartStone extends ReligiousSpell {

    private static final Logger logger = Logger.getLogger(HeartStone.class.getName());

    public static boolean isProtectedLocation(int tilex, int tiley, boolean surfaced) {
        Village deed = Villages.getVillage(tilex, tiley, surfaced);
        return deed != null && deed.isPermanent;
    }

    public HeartStone() {
        super("Heart Stone", ModActions.getNextActionId(), 180, 70, 80, 70, 0L);
        this.targetItem = true;
        this.targetTile = true;
        this.description = "shift a location outside of time, making items indestructible";
        ActionEntry actionEntry = new ActionEntryBuilder((short) this.number, this.name, "casting", new int[]{
                2,  // ACTION_TYPE_SPELL
                36, // ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
                48  // ACTION_TYPE_ENEMY_ALWAYS
        }).build();
        ModActions.registerAction(actionEntry);
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, Item target) {
        if(!isHeartStoneItem(target)) {
            performer.getCommunicator().sendNormalServerMessage("This item has too much temporal karma, you can't cast the spell.");
            return false;
        }
        return precondition(castSkill, performer, target.getTileX(), target.getTileY(), target.isOnSurface()? 0 : -1);
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, int tilex, int tiley, int layer) {
        if(isProtectedLocation(tilex, tiley, layer >= 0)) {
            performer.getCommunicator().sendNormalServerMessage("This location has too much temporal karma, you can't cast the spell here.");
            return false;
        }
        return true;
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, int tilex, int tiley, int layer, int heightOffset, Tiles.TileBorderDirection dir) {
        return precondition(castSkill, performer, tilex, tiley, layer);
    }

    @Override
    void doEffect(Skill castSkill, double power, Creature performer, Item target) {
        if(isHeartStoneItem(target)) {
            boolean exists = StructureService.hasHeartStone(target);
            if(!exists) {
                StructureService.setHeartStonePermissions(target);
                performer.getCommunicator().sendNormalServerMessage("You enchant the " + target.getName() + ", shifting it slightly outside of time.");
            } else {
                StructureService.removeHeartStonePermissions(target);
                performer.getCommunicator().sendNormalServerMessage("You disenchant the " + target.getName() + ", shifting it back into time.");
            }
        } else {
            performer.getCommunicator().sendNormalServerMessage("The spell fizzles.");
        }

    }

    @Override
    void doEffect(Skill castSkill, double power, Creature performer, int tilex, int tiley, int layer, int heightOffset) {
        VolaTile tile = Zones.getTileOrNull(tilex, tiley, layer >= 0);
        if(tile != null) {
            Item[] items = tile.getItems();
            Fence[] fences = tile.getAllFences();
            Wall[] walls = tile.getWalls();
            Floor[] floors = tile.getFloors();
            BridgePart[] bridgeParts = tile.getBridgeParts();
            boolean exists = checkForExtistingEnchant(items, fences, walls, floors, bridgeParts);
            if(!exists) {
                for(Item item : items) {
                    if(isHeartStoneItem(item)) {
                        StructureService.setHeartStonePermissions(item);
                    }
                }
                for(Fence fence : fences) {
                    StructureService.setHeartStonePermissions(fence);
                }
                for(Wall wall : walls) {
                    StructureService.setHeartStonePermissions(wall);
                }
                for(Floor floor : floors) {
                    StructureService.setHeartStonePermissions(floor);
                }
                for(BridgePart bridgePart : bridgeParts) {
                    StructureService.setHeartStonePermissions(bridgePart);
                }
                performer.getCommunicator().sendNormalServerMessage("You enchant the area around you, shifting items here slightly outside of time.");
            } else {
                for(Item item : items) {
                    if(isHeartStoneItem(item)) {
                        StructureService.removeHeartStonePermissions(item);
                    }
                }
                for(Fence fence : fences) {
                    StructureService.removeHeartStonePermissions(fence);
                }
                for(Wall wall : walls) {
                    StructureService.removeHeartStonePermissions(wall);
                }
                for(Floor floor : floors) {
                    StructureService.removeHeartStonePermissions(floor);
                }
                for(BridgePart bridgePart : bridgeParts) {
                    StructureService.removeHeartStonePermissions(bridgePart);
                }
                performer.getCommunicator().sendNormalServerMessage("You disenchant the area around you, shifting items here back into time.");
            }
        }
    }

    @Override
    void doEffect(Skill castSkill, double power, Creature performer, int tilex, int tiley, int layer, int heightOffset, TileBorderDirection dir) {
        doEffect(castSkill, power, performer, tilex, tiley, layer, heightOffset);
    }

    private boolean isHeartStoneItem(Item item) {
        ItemTemplate template = item.getTemplate();
        return item.isStreetLamp() || template.isBrazier() || item.isFlag() || template.isStatue();
    }

    private boolean checkForExtistingEnchant(Item[] items, Fence[] fences, Wall[] walls, Floor[] floors, BridgePart[] bridgeParts) {
        for(Item item : items)
            if(isHeartStoneItem(item) && StructureService.hasHeartStone(item))
                return true;
        for(Fence fence : fences)
            if(StructureService.hasHeartStone(fence))
                return true;
        for(Wall wall : walls)
            if(StructureService.hasHeartStone(wall))
                return true;
        for(Floor floor : floors)
            if(StructureService.hasHeartStone(floor))
                return true;
        for(BridgePart bridgePart : bridgeParts)
            if(StructureService.hasHeartStone(bridgePart))
                return true;
        return false;
    }
}
