package com.wurmonline.server.spells;

import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.SpellEffects;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.skills.Skill;
import org.gotti.wurmunlimited.modsupport.actions.ActionEntryBuilder;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.logging.Logger;


public class Invisibility extends ReligiousSpell {

    private static final Logger logger = Logger.getLogger(Invisibility.class.getName());

    public static final byte INVISIBILITY_EFFECT_ID = 101;
    public static final String INVISIBILITY_EFFECT_DESCRIPTION = "invisibility.";
    public static final String INVISIBILITY_DESCRIPTION = "cloak of invisibility";

    float durationModifier;

    public Invisibility() {
        super("Invisibility", ModActions.getNextActionId(), 10, 40, 60, 60, 0L);
        this.enchantment = INVISIBILITY_EFFECT_ID;
        this.effectdesc = INVISIBILITY_EFFECT_DESCRIPTION;
        this.description = INVISIBILITY_DESCRIPTION;
        this.durationModifier = 20.0f;
        this.targetCreature = true;
        ActionEntry actionEntry = new ActionEntryBuilder((short) this.number, this.name, "casting", new int[]{
                2,  // ACTION_TYPE_SPELL
                36, // ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
                48  // ACTION_TYPE_ENEMY_ALWAYS
        }).build();
        ModActions.registerAction(actionEntry);
    }

    @Override
    boolean precondition(final Skill castSkill, final Creature performer, final Creature target) {
        if(target != performer) return false;
        if(performer.getTarget() != null) {
            performer.getCommunicator().sendNormalServerMessage("You cannot cast " + this.description + " while in combat.");
            return false;
        }
        return true;
    }

    @Override
    boolean precondition(final Skill castSkill, final Creature performer, final Item target) {
        return false;
    }

    @Override
    void doEffect(final Skill castSkill, final double power, final Creature performer, final Creature target) {
        SpellEffects effs = performer.getSpellEffects();
        if(effs == null) effs = performer.createSpellEffects();
        SpellEffect eff = effs.getSpellEffect(this.enchantment);
        if(eff == null) {
            performer.getCommunicator().sendNormalServerMessage("You now have " + this.effectdesc);
            eff = new SpellEffect(performer.getWurmId(), this.enchantment, (float) power, (int) Math.max(1.0, Math.max(1.0, power) * (this.durationModifier + performer.getNumLinks())), (byte) 9, (byte) 0, true);
            effs.addSpellEffect(eff);
        } else {
            performer.getCommunicator().sendNormalServerMessage("You will now receive improved " + this.effectdesc);
            eff.setPower((float) power);
            eff.setTimeleft(eff.timeleft + (int) Math.max(1.0, Math.max(1.0, power) * (this.durationModifier + performer.getNumLinks())));
            performer.sendUpdateSpellEffect(eff);
        }
    }

    public static void addEffect(final Creature creature, final SpellEffect effect) {
        if(creature.isVisible()) {
            creature.setVisible(false);
        }
    }

    public static void removeEffect(final Creature creature, final SpellEffect effect) {
        if(!creature.isVisible()) {
            creature.setVisible(true);
        }
    }

    @SuppressWarnings("unused")
    public static boolean handle_TARGET_and_TARGET_HOSTILE(final Creature performer, final Creature target) {
        if(performer.isPlayer() && !performer.isVisible()) {
            SpellEffects effs = performer.getSpellEffects();
            SpellEffect eff = effs.getSpellEffect(INVISIBILITY_EFFECT_ID);
            if(eff != null) effs.removeSpellEffect(eff);
        }
        return false;
    }
}
