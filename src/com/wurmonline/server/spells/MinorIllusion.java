package com.wurmonline.server.spells;

import com.google.common.base.Strings;
import com.wurmonline.server.FailedException;
import com.wurmonline.server.Items;
import com.wurmonline.server.MiscConstants;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.creatures.CreatureTemplateIds;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.Skill;
import net.spirangle.awakening.Config;
import net.spirangle.awakening.items.ItemTemplateCreatorAwakening;
import org.gotti.wurmunlimited.modsupport.actions.ActionEntryBuilder;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.logging.Level;
import java.util.logging.Logger;


public class MinorIllusion extends ReligiousSpell {

    private static final Logger logger = Logger.getLogger(MinorIllusion.class.getName());

    public MinorIllusion() {
        super("Minor illusion", ModActions.getNextActionId(), 180, 80, 80, 70, 180000L);
        this.targetItem = true;
        this.description = "creates a minor illusion";
        ActionEntry actionEntry = new ActionEntryBuilder((short) this.number, this.name, "casting", new int[]{
                2,  // ACTION_TYPE_SPELL
                36, // ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
                48  // ACTION_TYPE_ENEMY_ALWAYS
        }).build();
        ModActions.registerAction(actionEntry);
    }

    @Override
    public int getCost(final Item target) {
        ItemTemplate template = target.getTemplate();
        int templId = template.getTemplateId();
        if(templId == ItemList.corpse ||
           templId == ItemList.sheetSilver ||
           templId == ItemList.sheetGold ||
           templId == ItemList.sourceSalt) return this.getCost();
        return 30;
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, Item target) {
        ItemTemplate template = target.getTemplate();
        int templId = template.getTemplateId();
        if(templId == ItemList.corpse || templId == ItemList.sheetSilver || templId == ItemList.sheetGold || templId == ItemList.sourceSalt ||
           template.isFood() || template.isVegetable()) {
            int karmaCost = 0;
            if(templId == ItemList.corpse) karmaCost = Config.minorIllusionCreatureKarmaCost;
            else if(templId == ItemList.sheetSilver) karmaCost = Config.silverMirrorKarmaCost;
            else if(templId == ItemList.sheetGold) karmaCost = Config.goldMirrorKarmaCost;
            else if(templId == ItemList.sourceSalt) karmaCost = Config.yellowPotionKarmaCost;
            else if(template.isFood() || template.isVegetable()) karmaCost = Config.minorIllusionItemKarmaCost;
            else return false;
            if(performer.getKarma() < karmaCost) {
                performer.getCommunicator().sendNormalServerMessage("You don't have enough karma to cast this spell.");
                return false;
            }
            return true;
        }
        performer.getCommunicator().sendNormalServerMessage("The spell will not work on that.");
        return false;
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, Creature target) {
        return false;
    }

    @Override
    void doEffect(Skill castSkill, double power, Creature performer, Item target) {
        ItemTemplate template = target.getTemplate();
        int templId = template.getTemplateId();
        if(templId == ItemList.corpse || templId == ItemList.sheetSilver || templId == ItemList.sheetGold || templId == ItemList.sourceSalt ||
           template.isFood() || template.isVegetable()) {
            try {
                Item item = null;
                int karmaCost = 0;
                if(templId == ItemList.corpse) {
                    logger.info("Target: aux=" + target.getAuxData() + ", data1=" + target.getData1() + ", data2=" + target.getData2() + ", extra1=" + target.getExtra1() + ", extra2=" + target.getExtra2() + ", color=" + target.getColor());
                    float scale = (float) target.getSizeY() / (float) target.getTemplate().getSizeY();
                    int t = target.getData1();
                    boolean foal = false;
                    if(t == CreatureTemplateIds.HORSE_CID) {
                        t = CreatureTemplateIds.FOAL_CID;
                        scale *= 1.5f;
                        foal = true;
                    } else if(t == CreatureTemplateIds.HELL_HORSE_CID) {
                        t = CreatureTemplateIds.HELL_FOAL_CID;
                        scale *= 1.5f;
                        foal = true;
                    } else if(t == CreatureTemplateIds.UNICORN_CID) {
                        t = CreatureTemplateIds.UNICORN_FOAL_CID;
                        scale *= 1.5f;
                        foal = true;
                    }
                    int x1 = target.getExtra1();
                    if(x1 < 0) x1 = 0;
                    item = ItemFactory.createItem(ItemTemplateCreatorAwakening.minorIllusionCreature, (float) power, MiscConstants.COMMON, performer.getName());
                    item.setAuxData((byte) 0);
                    item.setData1(t | (target.getAuxData() << 16));
                    item.setData2((int) (scale * 100.0f) | (x1 << 16));
                    item.setFemale(target.female);
                    item.setName(target.getName().toLowerCase().replace("corpse of ", "").replace("The ", "") + " illusion");
                    item.setColor(target.getColor());
                    String msg = "You create an animated miniature illusion";
                    if(foal) msg += ", but for some reason it reverts to an earlier memory state of the creature";
                    else if(item.getModelName().equals("model.creature.humanoid.human.player."))
                        msg += ", but are unable to capture the soul of the being";
                    msg += ".";
                    performer.getCommunicator().sendNormalServerMessage(msg);
                    logger.info("Minor illusion: aux=" + item.getAuxData() + ", data1=" + item.getData1() + ", data2=" + item.getData2() + ", extra1=" + item.getExtra1() + ", extra2=" + item.getExtra2() + ", color=" + item.getColor());
                    karmaCost = Config.minorIllusionCreatureKarmaCost;
                } else if(templId == ItemList.sheetSilver) {
                    item = ItemFactory.createItem(ItemList.handMirror, (float) power, MiscConstants.COMMON, performer.getName());
                    karmaCost = Config.silverMirrorKarmaCost;
                } else if(templId == ItemList.sheetGold) {
                    item = ItemFactory.createItem(ItemList.goldenMirror, (float) power, MiscConstants.COMMON, performer.getName());
                    karmaCost = Config.goldMirrorKarmaCost;
                } else if(templId == ItemList.sourceSalt) {
                    item = ItemFactory.createItem(ItemList.potionIllusion, (float) power, MiscConstants.COMMON, performer.getName());
                    karmaCost = Config.yellowPotionKarmaCost;
                } else if(template.isFood() || template.isVegetable()) {
                    logger.info("Target: aux=" + target.getAuxData() + ", data1=" + target.getData1() + ", data2=" + target.getData2() + ", extra1=" + target.getExtra1() + ", extra2=" + target.getExtra2() + ", color=" + target.getColor());
                    item = ItemFactory.createItem(ItemTemplateCreatorAwakening.minorIllusionItem, (float) power, MiscConstants.COMMON, performer.getName());
                    item.setAuxData((byte) 1);
                    item.setData1(target.getAuxData());
                    item.setData2(templId);
                    item.setName(Strings.isNullOrEmpty(target.getDescription())? target.getName() + " illusion" : target.getDescription());
                    item.setColor(target.getColor());
                    String msg = "You create an illusion of the " + target.getName() + ".";
                    performer.getCommunicator().sendNormalServerMessage(msg);
                    logger.info("Minor illusion: aux=" + item.getAuxData() + ", data1=" + item.getData1() + ", data2=" + item.getData2() + ", extra1=" + item.getExtra1() + ", extra2=" + item.getExtra2() + ", color=" + item.getColor());
                    karmaCost = Config.minorIllusionItemKarmaCost;
                }
                performer.getInventory().insertItem(item, true);
                performer.getCommunicator().sendNormalServerMessage("The " + target.getAuxData() + " is destroyed by the spell.");
                Items.destroyItem(target.getWurmId());
                if(karmaCost > 0) performer.setKarma(performer.getKarma() - karmaCost);
            } catch(FailedException | NoSuchTemplateException e) {
                logger.log(Level.SEVERE, "Error creating minor illusion: " + e.getMessage(), e);
                performer.getCommunicator().sendAlertServerMessage("Something went wrong, please contact staff.");
            }
        } else {
            performer.getCommunicator().sendNormalServerMessage("The spell fizzles.");
        }
    }

    @Override
    void doNegativeEffect(final Skill castSkill, final double power, final Creature performer, final Item target) {
        this.checkDestroyItem(power, performer, target);
    }
}
