package com.wurmonline.server.spells;

import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.deities.Religion;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemList;
import com.wurmonline.server.skills.Skill;
import org.gotti.wurmunlimited.modsupport.actions.ActionEntryBuilder;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.logging.Logger;

import static com.wurmonline.server.deities.Religion.*;

public class Consecrate extends ReligiousSpell {

    private static final Logger logger = Logger.getLogger(Consecrate.class.getName());

    public Consecrate() {
        super("Consecrate", ModActions.getNextActionId(), 30, 30, 20, 30, 0L);
        this.targetItem = true;
        this.description = "consecrates a holy item";
        this.hasDynamicCost = true;
        ActionEntry actionEntry = new ActionEntryBuilder((short) this.number, this.name, "casting", new int[]{
                2,  // ACTION_TYPE_SPELL
                36, // ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
                48  // ACTION_TYPE_ENEMY_ALWAYS
        }).build();
        ModActions.registerAction(actionEntry);
    }

    @Override
    public int getCost(final Item target) {
        int tt = target.getTemplateId();
        if(tt == ItemList.sourceCrystal || tt == ItemList.sourceSalt) {
            return 10;
        }
        return this.getCost();
    }

    private boolean hasDeityStyle(Item target) {
        byte aux = target.getAuxData();
        return aux >= DEITY_SEDAES && aux <= DEITY_QALESHIN;
    }

    private boolean canSetDeityStyle(Creature performer, Item target, boolean deityCheck) {
        if(performer.getDeity() == null) return false;
        int tt = target.getTemplateId();
        int d = performer.getDeity().getNumber();
        if(tt == ItemList.flagKingdom || tt == ItemList.bannerKingdom || tt == ItemList.wagon) return true;
        if(tt == ItemList.tallKingdomBanner) return !deityCheck || d != DEITY_SEDAES;
        if(tt == ItemList.tentMilitary)
            return !deityCheck || d == DEITY_PAYREN || d == DEITY_SHONIN || d == DEITY_KIANE || d == DEITY_MIRIDON || d == DEITY_DRAKKAR;
        return false;
    }

    @Override
    boolean precondition(Skill castSkill, Creature performer, Item target) {
        if(Religion.isHolyItem(performer.getDeity(), target)) return true;
        int tt = target.getTemplateId();
        if(canSetDeityStyle(performer, target, false)) {
            if(canSetDeityStyle(performer, target, true)) {
                if(!hasDeityStyle(target)) return true;
                int aux = target.getAuxData();
                Deity deity = Deities.getDeity(aux);
                performer.getCommunicator().sendNormalServerMessage("This " + target.getName() + " has been consecrated to the Order of " + deity.getName() + ".");
            } else {
                performer.getCommunicator().sendNormalServerMessage("The Order of " + performer.getDeity().getName() + " does not consecrate " + target.getTemplate().getPlural() + ".");
            }
        } else if((tt == ItemList.sourceCrystal || tt == ItemList.sourceSalt) && target.getActualAuxData() == 0) {
            return true;
        } else {
            performer.getCommunicator().sendNormalServerMessage("The spell will not work on that.");
        }
        return false;
    }

    @Override
    void doEffect(Skill castSkill, double power, Creature performer, Item target) {
        int tt = target.getTemplateId();
        if(Religion.isHolyItem(performer.getDeity(), target)) {
            target.setQualityLevel(performer.getFaith());
            performer.getCommunicator().sendNormalServerMessage("You consecrate the " + target.getName() + ".");
        } else if((tt == ItemList.sourceCrystal || tt == ItemList.sourceSalt) && target.getActualAuxData() == 0) {
            Deity deity = performer.getDeity();
            target.bless(deity.getNumber());
            target.setAuxData((byte) 1);
            target.setName((deity.isHateGod()? "shade " : "sacred ") + (tt == ItemList.sourceCrystal? "crystal" : "salt"));
        } else if(canSetDeityStyle(performer, target, true)) {
            int d = performer.getDeity().getNumber();
            if(tt == ItemList.wagon) {
                target.setData1(d);
            }
            target.setAuxData((byte) d);
            target.updateModelNameOnGroundItem();
            target.updateName();
            performer.getCommunicator().sendNormalServerMessage("You consecrate the " + target.getName() + ", it now wears the colours of the Order of " + performer.getDeity().getName() + ".");
        } else {
            performer.getCommunicator().sendNormalServerMessage("The spell fizzles.");
        }
    }
}
