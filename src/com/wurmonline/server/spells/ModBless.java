package com.wurmonline.server.spells;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deity;

@SuppressWarnings("unused")
public class ModBless {

    public static void blessCreature(final Creature performer, final Creature target) {
        Deity deity = performer.getDeity();
        boolean isHateGod = deity.isHateGod();
        boolean isCorrupt = target.hasTrait(22);
        if(isHateGod && !isCorrupt) {
            target.getStatus().setTraitBit(22, true);
            performer.getCommunicator().sendNormalServerMessage("The dark energies of " + deity.getName() + " flows through " + target.getNameWithGenus() + " corrupting " + target.getHimHerItString() + ".");
        } else if(!isHateGod && isCorrupt) {
            target.getStatus().setTraitBit(22, false);
            performer.getCommunicator().sendNormalServerMessage("The cleansing power of " + deity.getName() + " courses through " + target.getNameWithGenus() + " purifying " + target.getHimHerItString() + ".");
        }
    }
}
