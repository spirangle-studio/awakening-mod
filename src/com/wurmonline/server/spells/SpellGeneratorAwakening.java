package com.wurmonline.server.spells;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;

import java.util.Arrays;
import java.util.logging.Logger;


public class SpellGeneratorAwakening {

    private static final Logger logger = Logger.getLogger(SpellGeneratorAwakening.class.getName());

    public static final byte TYPE_CHAPLAIN = 1;
    public static final byte TYPE_MISSIONARY = 2;
    public static final byte TYPE_BOTH = 0;

    public static final Spell SPELL_CONSECRATE = new Consecrate();
    public static final Spell SPELL_HEART_STONE = new HeartStone();
    public static final Spell SPELL_INVISIBILITY = new Invisibility();
    public static final Spell SPELL_MINOR_ILLUSION = new MinorIllusion();
    public static final Spell SPELL_RITUAL_OF_THE_MOONS = new RitualMoons();

    static {
        addSpell(SPELL_CONSECRATE);
        addSpell(SPELL_HEART_STONE);
        addSpell(SPELL_INVISIBILITY);
        addSpell(SPELL_MINOR_ILLUSION);
        addSpell(SPELL_RITUAL_OF_THE_MOONS);
        SPELL_CONSECRATE.setType(TYPE_CHAPLAIN);
        SPELL_HEART_STONE.setType(TYPE_CHAPLAIN);
        SPELL_INVISIBILITY.setType(TYPE_MISSIONARY);
        SPELL_MINOR_ILLUSION.setType(TYPE_CHAPLAIN);
        SPELL_RITUAL_OF_THE_MOONS.setType(TYPE_MISSIONARY);
    }

    @SuppressWarnings("unused")
    public static void createSpells() {
        logger.info("Starting to create spells");
        final long start = System.nanoTime();

        final Deity fo = Deities.getDeity(1);
        final Deity magranon = Deities.getDeity(2);
        final Deity vynora = Deities.getDeity(3);
        final Deity libila = Deities.getDeity(4);
        final Deity sedaes = Deities.getDeity(21);
        final Deity payren = Deities.getDeity(22);
        final Deity eilanda = Deities.getDeity(23);
        final Deity shonin = Deities.getDeity(24);
        final Deity asman = Deities.getDeity(25);
        final Deity kiane = Deities.getDeity(26);
        final Deity miridon = Deities.getDeity(27);
        final Deity laphaner = Deities.getDeity(28);
        final Deity drakkar = Deities.getDeity(29);
        final Deity qaleshin = Deities.getDeity(30);

        EnchantUtil.initializeEnchantGroups();
        final Spell[] SPELLS_COMMON = {
                Spells.SPELL_BLESS,
                Spells.SPELL_BREAK_ALTAR,
                SPELL_CONSECRATE,
                Spells.SPELL_DISINTEGRATE,
                Spells.SPELL_DISPEL,
                SPELL_HEART_STONE,
                Spells.SPELL_LOCATE_SOUL,
                Spells.SPELL_NOLOCATE,
                Spells.SPELL_TANGLEWEAVE,
                Spells.SPELL_VESSEL
        };
        for(final Deity deity : Deities.getDeities()) {
            for(final Spell spell : SPELLS_COMMON) {
                deity.addSpell(spell);
            }
        }
        SpellGenerator.ignoreSpells.addAll(Arrays.asList(SPELLS_COMMON));

        if(sedaes != null) {
            sedaes.addSpell(Spells.SPELL_AURA_SHARED_PAIN);
            sedaes.addSpell(Spells.SPELL_BLAZE);
            sedaes.addSpell(Spells.SPELL_BLESSINGS_OF_THE_DARK);
            sedaes.addSpell(Spells.SPELL_COURIER);
            sedaes.addSpell(Spells.SPELL_CURE_LIGHT);
            sedaes.addSpell(Spells.SPELL_CURE_MEDIUM);
            sedaes.addSpell(Spells.SPELL_CURE_SERIOUS);
            sedaes.addSpell(Spells.SPELL_DOMINATE);
            sedaes.addSpell(Spells.SPELL_DRAIN_STAMINA);
            sedaes.addSpell(Spells.SPELL_EXCEL);
            sedaes.addSpell(Spells.SPELL_FIRE_PILLAR);
            sedaes.addSpell(Spells.SPELL_PROTECT_FIRE);
            sedaes.addSpell(Spells.SPELL_FLAMING_AURA);
            sedaes.addSpell(Spells.SPELL_FOCUSED_WILL);
            sedaes.addSpell(Spells.SPELL_FROSTBRAND);
            sedaes.addSpell(Spells.SPELL_HEAL);
            sedaes.addSpell(Spells.SPELL_HYPOTHERMIA);
            sedaes.addSpell(Spells.SPELL_ICE_PILLAR);
            sedaes.addSpell(Spells.SPELL_LIGHT_TOKEN);
            sedaes.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            sedaes.addSpell(Spells.SPELL_MASS_STAMINA);
            sedaes.addSpell(Spells.SPELL_REFRESH);
            sedaes.addSpell(Spells.SPELL_RITUAL_OF_THE_SUN);
            sedaes.addSpell(Spells.SPELL_SHARD_OF_ICE);
            sedaes.addSpell(Spells.SPELL_SIXTH_SENSE);
            sedaes.addSpell(Spells.SPELL_TORNADO);
            sedaes.addSpell(Spells.SPELL_TRUEHIT);
        }

        if(payren != null) {
            payren.addSpell(Spells.SPELL_BEARPAWS);
            payren.addSpell(Spells.SPELL_BLOODTHIRST);
            payren.addSpell(Spells.SPELL_CHARM_ANIMAL);
            payren.addSpell(Spells.SPELL_CLEANSE);
            payren.addSpell(Spells.SPELL_DIRT);
            payren.addSpell(Spells.SPELL_FOREST_GIANT_STRENGTH);
            payren.addSpell(Spells.SPELL_FRANTIC_CHARGE);
            payren.addSpell(Spells.SPELL_GENESIS);
            payren.addSpell(Spells.SPELL_GOAT_SHAPE);
            payren.addSpell(Spells.SPELL_HOLY_CROP);
            payren.addSpell(Spells.SPELL_HUMID_DRIZZLE);
            payren.addSpell(SPELL_INVISIBILITY);
            payren.addSpell(Spells.SPELL_LURKER_IN_THE_DARK);
            payren.addSpell(Spells.SPELL_LURKER_IN_THE_WOODS);
            payren.addSpell(Spells.SPELL_MASS_STAMINA);
            payren.addSpell(Spells.SPELL_DEMISE_MONSTER);
            payren.addSpell(Spells.SPELL_MORNING_FOG);
            payren.addSpell(Spells.SPELL_NIMBLENESS);
            payren.addSpell(Spells.SPELL_OAKSHELL);
            payren.addSpell(Spells.SPELL_PHANTASMS);
            payren.addSpell(Spells.SPELL_PROTECT_POISON);
            payren.addSpell(Spells.SPELL_REVEAL_CREATURES);
            payren.addSpell(Spells.SPELL_TENTACLES);
            payren.addSpell(Spells.SPELL_TOXIN);
            payren.addSpell(Spells.SPELL_VENOM);
            payren.addSpell(Spells.SPELL_WARD);
            payren.addSpell(Spells.SPELL_WILD_GROWTH);
        }

        if(eilanda != null) {
            eilanda.addSpell(Spells.SPELL_CIRCLE_OF_CUNNING);
            eilanda.addSpell(Spells.SPELL_COURIER);
            eilanda.addSpell(Spells.SPELL_CURE_LIGHT);
            eilanda.addSpell(Spells.SPELL_ESSENCE_DRAIN);
            eilanda.addSpell(Spells.SPELL_FIREHEART);
            eilanda.addSpell(Spells.SPELL_PROTECT_FROST);
            eilanda.addSpell(Spells.SPELL_GLACIAL);
            eilanda.addSpell(Spells.SPELL_LIFE_TRANSFER);
            eilanda.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            eilanda.addSpell(Spells.SPELL_LURKER_IN_THE_DARK);
            eilanda.addSpell(Spells.SPELL_LURKER_IN_THE_DEEP);
            eilanda.addSpell(Spells.SPELL_MIND_STEALER);
            eilanda.addSpell(SPELL_MINOR_ILLUSION);
            eilanda.addSpell(Spells.SPELL_MOLE_SENSES);
            eilanda.addSpell(Spells.SPELL_OPULENCE);
            eilanda.addSpell(Spells.SPELL_PURGE);
            eilanda.addSpell(Spells.SPELL_REVEAL_SETTLEMENTS);
            eilanda.addSpell(SPELL_RITUAL_OF_THE_MOONS);
            eilanda.addSpell(Spells.SPELL_SIXTH_SENSE);
            eilanda.addSpell(Spells.SPELL_STRONGWALL);
            eilanda.addSpell(Spells.SPELL_SUMMON_SOUL);
            eilanda.addSpell(Spells.SPELL_SUNDER);
            eilanda.addSpell(Spells.SPELL_WEAKNESS);
            eilanda.addSpell(Spells.SPELL_WEB_ARMOUR);
            eilanda.addSpell(Spells.SPELL_WILLOWSPINE);
            eilanda.addSpell(Spells.SPELL_WIND_OF_AGES);
            eilanda.addSpell(Spells.SPELL_WISDOM_OF_VYNORA);
        }

        if(shonin != null) {
            shonin.addSpell(Spells.SPELL_CIRCLE_OF_CUNNING);
            shonin.addSpell(Spells.SPELL_CLEANSE);
            shonin.addSpell(Spells.SPELL_DOMINATE);
            shonin.addSpell(Spells.SPELL_DRAIN_HEALTH);
            shonin.addSpell(Spells.SPELL_EXCEL);
            shonin.addSpell(Spells.SPELL_FIRE_PILLAR);
            shonin.addSpell(Spells.SPELL_FIREHEART);
            shonin.addSpell(Spells.SPELL_FOCUSED_WILL);
            shonin.addSpell(Spells.SPELL_FRANTIC_CHARGE);
            shonin.addSpell(Spells.SPELL_GOAT_SHAPE);
            shonin.addSpell(Spells.SPELL_HELL_STRENGTH);
            shonin.addSpell(Spells.SPELL_INFERNO);
            shonin.addSpell(Spells.SPELL_DEMISE_LEGENDARY);
            shonin.addSpell(Spells.SPELL_MASS_STAMINA);
            shonin.addSpell(Spells.SPELL_MORNING_FOG);
            shonin.addSpell(Spells.SPELL_OAKSHELL);
            shonin.addSpell(Spells.SPELL_REFRESH);
            shonin.addSpell(Spells.SPELL_RITE_OF_SPRING);
            shonin.addSpell(Spells.SPELL_SHARD_OF_ICE);
            shonin.addSpell(Spells.SPELL_SIXTH_SENSE);
            shonin.addSpell(Spells.SPELL_SMITE);
            shonin.addSpell(Spells.SPELL_TENTACLES);
            shonin.addSpell(Spells.SPELL_TORNADO);
            shonin.addSpell(Spells.SPELL_TRUEHIT);
            shonin.addSpell(Spells.SPELL_VENOM);
            shonin.addSpell(Spells.SPELL_WEAKNESS);
            shonin.addSpell(Spells.SPELL_WILLOWSPINE);
        }

        if(asman != null) {
            asman.addSpell(Spells.SPELL_PROTECT_ACID);
            asman.addSpell(Spells.SPELL_BLESSINGS_OF_THE_DARK);
            asman.addSpell(Spells.SPELL_BLOODTHIRST);
            asman.addSpell(Spells.SPELL_CORROSION);
            asman.addSpell(Spells.SPELL_COURIER);
            asman.addSpell(Spells.SPELL_ESSENCE_DRAIN);
            asman.addSpell(Spells.SPELL_FLAMING_AURA);
            asman.addSpell(Spells.SPELL_FROSTBRAND);
            asman.addSpell(Spells.SPELL_PROTECT_FROST);
            asman.addSpell(Spells.SPELL_GLACIAL);
            asman.addSpell(Spells.SPELL_HYPOTHERMIA);
            asman.addSpell(Spells.SPELL_ICE_PILLAR);
            asman.addSpell(Spells.SPELL_LIFE_TRANSFER);
            asman.addSpell(Spells.SPELL_LIGHT_TOKEN);
            asman.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            asman.addSpell(Spells.SPELL_LURKER_IN_THE_DARK);
            asman.addSpell(Spells.SPELL_MEND);
            asman.addSpell(Spells.SPELL_MIND_STEALER);
            asman.addSpell(Spells.SPELL_DEMISE_MONSTER);
            asman.addSpell(Spells.SPELL_NIMBLENESS);
            asman.addSpell(Spells.SPELL_REVEAL_SETTLEMENTS);
            asman.addSpell(Spells.SPELL_RITUAL_OF_THE_SUN);
            asman.addSpell(Spells.SPELL_STRONGWALL);
            asman.addSpell(Spells.SPELL_SUNDER);
            asman.addSpell(Spells.SPELL_WEB_ARMOUR);
            asman.addSpell(Spells.SPELL_WISDOM_OF_VYNORA);
        }

        if(kiane != null) {
            kiane.addSpell(Spells.SPELL_AURA_SHARED_PAIN);
            kiane.addSpell(Spells.SPELL_CHARM_ANIMAL);
            kiane.addSpell(Spells.SPELL_CIRCLE_OF_CUNNING);
            kiane.addSpell(Spells.SPELL_COURIER);
            kiane.addSpell(Spells.SPELL_CURE_LIGHT);
            kiane.addSpell(Spells.SPELL_CURE_MEDIUM);
            kiane.addSpell(Spells.SPELL_CURE_SERIOUS);
            kiane.addSpell(Spells.SPELL_DIRT);
            kiane.addSpell(Spells.SPELL_GENESIS);
            kiane.addSpell(Spells.SPELL_HEAL);
            kiane.addSpell(Spells.SPELL_HUMID_DRIZZLE);
            kiane.addSpell(SPELL_INVISIBILITY);
            kiane.addSpell(Spells.SPELL_LIGHT_TOKEN);
            kiane.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            kiane.addSpell(Spells.SPELL_LURKER_IN_THE_DEEP);
            kiane.addSpell(Spells.SPELL_LURKER_IN_THE_WOODS);
            kiane.addSpell(SPELL_MINOR_ILLUSION);
            kiane.addSpell(Spells.SPELL_MOLE_SENSES);
            kiane.addSpell(Spells.SPELL_OPULENCE);
            kiane.addSpell(Spells.SPELL_PURGE);
            kiane.addSpell(Spells.SPELL_REVEAL_CREATURES);
            kiane.addSpell(SPELL_RITUAL_OF_THE_MOONS);
            kiane.addSpell(Spells.SPELL_SIXTH_SENSE);
            kiane.addSpell(Spells.SPELL_SUMMON_SOUL);
            kiane.addSpell(Spells.SPELL_WARD);
            kiane.addSpell(Spells.SPELL_WILD_GROWTH);
            kiane.addSpell(Spells.SPELL_WIND_OF_AGES);
        }

        if(miridon != null) {
            miridon.addSpell(Spells.SPELL_BLAZE);
            miridon.addSpell(Spells.SPELL_BLOODTHIRST);
            miridon.addSpell(Spells.SPELL_CORRUPT);
            miridon.addSpell(Spells.SPELL_DRAIN_HEALTH);
            miridon.addSpell(Spells.SPELL_EXCEL);
            miridon.addSpell(Spells.SPELL_FIRE_PILLAR);
            miridon.addSpell(Spells.SPELL_FIREHEART);
            miridon.addSpell(Spells.SPELL_PROTECT_FIRE);
            miridon.addSpell(Spells.SPELL_FLAMING_AURA);
            miridon.addSpell(Spells.SPELL_FOCUSED_WILL);
            miridon.addSpell(Spells.SPELL_FRANTIC_CHARGE);
            miridon.addSpell(Spells.SPELL_GOAT_SHAPE);
            miridon.addSpell(Spells.SPELL_HELL_STRENGTH);
            miridon.addSpell(Spells.SPELL_DEMISE_HUMAN);
            miridon.addSpell(Spells.SPELL_ICE_PILLAR);
            miridon.addSpell(Spells.SPELL_INFERNO);
            miridon.addSpell(Spells.SPELL_DEMISE_LEGENDARY);
            miridon.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            miridon.addSpell(Spells.SPELL_MASS_STAMINA);
            miridon.addSpell(SPELL_RITUAL_OF_THE_MOONS);
            miridon.addSpell(Spells.SPELL_SCORN_OF_LIBILA);
            miridon.addSpell(Spells.SPELL_SHARD_OF_ICE);
            miridon.addSpell(Spells.SPELL_SMITE);
            miridon.addSpell(Spells.SPELL_STRONGWALL);
            miridon.addSpell(Spells.SPELL_TENTACLES);
            miridon.addSpell(Spells.SPELL_WEAKNESS);
            miridon.addSpell(Spells.SPELL_WEB_ARMOUR);
            miridon.addSpell(Spells.SPELL_WILLOWSPINE);
        }

        if(laphaner != null) {
            laphaner.addSpell(Spells.SPELL_PROTECT_ACID);
            laphaner.addSpell(Spells.SPELL_BLESSINGS_OF_THE_DARK);
            laphaner.addSpell(Spells.SPELL_CHARM_ANIMAL);
            laphaner.addSpell(Spells.SPELL_CLEANSE);
            laphaner.addSpell(Spells.SPELL_CORROSION);
            laphaner.addSpell(Spells.SPELL_CORRUPT);
            laphaner.addSpell(Spells.SPELL_DARK_MESSENGER);
            laphaner.addSpell(Spells.SPELL_DOMINATE);
            laphaner.addSpell(Spells.SPELL_DRAIN_STAMINA);
            laphaner.addSpell(Spells.SPELL_GENESIS);
            laphaner.addSpell(Spells.SPELL_HEAL);
            laphaner.addSpell(Spells.SPELL_HOLY_CROP);
            laphaner.addSpell(Spells.SPELL_HUMID_DRIZZLE);
            laphaner.addSpell(Spells.SPELL_LIFE_TRANSFER);
            laphaner.addSpell(Spells.SPELL_LIGHT_TOKEN);
            laphaner.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            laphaner.addSpell(Spells.SPELL_MIND_STEALER);
            laphaner.addSpell(SPELL_MINOR_ILLUSION);
            laphaner.addSpell(Spells.SPELL_NIMBLENESS);
            laphaner.addSpell(Spells.SPELL_OPULENCE);
            laphaner.addSpell(Spells.SPELL_PURGE);
            laphaner.addSpell(Spells.SPELL_REFRESH);
            laphaner.addSpell(Spells.SPELL_REVEAL_CREATURES);
            laphaner.addSpell(Spells.SPELL_SIXTH_SENSE);
            laphaner.addSpell(Spells.SPELL_SUMMON_SOUL);
            laphaner.addSpell(Spells.SPELL_TORNADO);
            laphaner.addSpell(Spells.SPELL_VENOM);
            laphaner.addSpell(Spells.SPELL_WARD);
            laphaner.addSpell(Spells.SPELL_WILD_GROWTH);
        }

        if(drakkar != null) {
            drakkar.addSpell(Spells.SPELL_DEMISE_ANIMAL);
            drakkar.addSpell(Spells.SPELL_AURA_SHARED_PAIN);
            drakkar.addSpell(Spells.SPELL_BLESSINGS_OF_THE_DARK);
            drakkar.addSpell(Spells.SPELL_BLOODTHIRST);
            drakkar.addSpell(Spells.SPELL_CORRUPT);
            drakkar.addSpell(Spells.SPELL_DARK_MESSENGER);
            drakkar.addSpell(Spells.SPELL_DIRT);
            drakkar.addSpell(Spells.SPELL_DRAIN_HEALTH);
            drakkar.addSpell(Spells.SPELL_DRAIN_STAMINA);
            drakkar.addSpell(Spells.SPELL_ESSENCE_DRAIN);
            drakkar.addSpell(Spells.SPELL_FROSTBRAND);
            drakkar.addSpell(Spells.SPELL_FUNGUS_TRAP);
            drakkar.addSpell(Spells.SPELL_DEMISE_HUMAN);
            drakkar.addSpell(Spells.SPELL_HYPOTHERMIA);
            drakkar.addSpell(SPELL_INVISIBILITY);
            drakkar.addSpell(Spells.SPELL_LAND_OF_THE_DEAD);
            drakkar.addSpell(Spells.SPELL_MOLE_SENSES);
            drakkar.addSpell(Spells.SPELL_PAIN_RAIN);
            drakkar.addSpell(Spells.SPELL_PHANTASMS);
            drakkar.addSpell(Spells.SPELL_PROTECT_POISON);
            drakkar.addSpell(Spells.SPELL_REBIRTH);
            drakkar.addSpell(Spells.SPELL_RITE_OF_DEATH);
            drakkar.addSpell(Spells.SPELL_ROTTING_GUT);
            drakkar.addSpell(Spells.SPELL_ROTTING_TOUCH);
            drakkar.addSpell(Spells.SPELL_TOXIN);
            drakkar.addSpell(Spells.SPELL_TRUEHIT);
            drakkar.addSpell(Spells.SPELL_WORM_BRAINS);
            drakkar.addSpell(Spells.SPELL_ZOMBIE_INFESTATION);
        }

        if(qaleshin != null) {
            qaleshin.addSpell(Spells.SPELL_BLAZE);
            qaleshin.addSpell(Spells.SPELL_CIRCLE_OF_CUNNING);
            qaleshin.addSpell(Spells.SPELL_CLEANSE);
            qaleshin.addSpell(Spells.SPELL_COURIER);
            qaleshin.addSpell(Spells.SPELL_DOMINATE);
            qaleshin.addSpell(Spells.SPELL_DRAIN_STAMINA);
            qaleshin.addSpell(Spells.SPELL_FIRE_PILLAR);
            qaleshin.addSpell(Spells.SPELL_PROTECT_FIRE);
            qaleshin.addSpell(Spells.SPELL_FLAMING_AURA);
            qaleshin.addSpell(Spells.SPELL_GENESIS);
            qaleshin.addSpell(Spells.SPELL_HEAL);
            qaleshin.addSpell(Spells.SPELL_INFERNO);
            qaleshin.addSpell(SPELL_INVISIBILITY);
            qaleshin.addSpell(Spells.SPELL_LIFE_TRANSFER);
            qaleshin.addSpell(Spells.SPELL_LIGHT_TOKEN);
            qaleshin.addSpell(Spells.SPELL_LOCATE_ARTIFACT);
            qaleshin.addSpell(Spells.SPELL_MEND);
            qaleshin.addSpell(Spells.SPELL_MORNING_FOG);
            qaleshin.addSpell(Spells.SPELL_NIMBLENESS);
            qaleshin.addSpell(Spells.SPELL_OAKSHELL);
            qaleshin.addSpell(Spells.SPELL_PURGE);
            qaleshin.addSpell(Spells.SPELL_REFRESH);
            qaleshin.addSpell(Spells.SPELL_RITE_OF_SPRING);
            qaleshin.addSpell(Spells.SPELL_STRONGWALL);
            qaleshin.addSpell(Spells.SPELL_SUMMON_SOUL);
            qaleshin.addSpell(Spells.SPELL_SUNDER);
            qaleshin.addSpell(Spells.SPELL_TORNADO);
            qaleshin.addSpell(Spells.SPELL_WEAKNESS);
            qaleshin.addSpell(Spells.SPELL_WIND_OF_AGES);
            qaleshin.addSpell(Spells.SPELL_WISDOM_OF_VYNORA);
        }

        final long end = System.nanoTime();
        logger.info("Generating spells took " + (end - start) / 1000000.0f + " ms");
    }

    private static Spell addSpell(Spell spell) {
        logger.info("Spell " + spell.name + " [" + spell.number + "] religious=" + spell.religious + ", offensive=" + spell.offensive + ", healing=" + spell.healing + ", level=" + spell.level + ", cooldown=" + spell.getCooldown());
        Spells.addSpell(spell);
        return spell;
    }

    @SuppressWarnings("unused")
    public static void addEffect(final Creature creature, final SpellEffect effect) {
        if(effect.type == 101) Invisibility.addEffect(creature, effect);
    }

    @SuppressWarnings("unused")
    public static void removeEffect(final Creature creature, final SpellEffect effect) {
        if(effect.type == 101) Invisibility.removeEffect(creature, effect);
    }
}
