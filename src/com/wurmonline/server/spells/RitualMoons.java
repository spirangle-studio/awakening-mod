package com.wurmonline.server.spells;


import com.wurmonline.server.*;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.deities.Deities;
import com.wurmonline.server.deities.Deity;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.items.ItemFactory;
import com.wurmonline.server.items.NoSuchTemplateException;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.skills.Skill;
import com.wurmonline.server.skills.SkillList;
import org.gotti.wurmunlimited.modsupport.actions.ActionEntryBuilder;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

import java.util.logging.Logger;

import static net.spirangle.awakening.AwakeningConstants.DAY_AS_MILLIS;
import static net.spirangle.awakening.AwakeningConstants.moonMetals;


public class RitualMoons extends ReligiousSpell {

    private static final Logger logger = Logger.getLogger(RitualMoons.class.getName());

    RitualMoons() {
        super("Ritual of the Moons", ModActions.getNextActionId(), 100, 300, 60, 50, 7200000L);
        this.isRitual = true;
        this.targetItem = true;
        this.description = "blessing from the moons";
        this.type = 0;
        ActionEntry actionEntry = new ActionEntryBuilder((short) this.number, this.name, "casting", new int[]{
                2,  // ACTION_TYPE_SPELL
                36, // ACTION_TYPE_ALWAYS_USE_ACTIVE_ITEM
                48  // ACTION_TYPE_ENEMY_ALWAYS
        }).build();
        ModActions.registerAction(actionEntry);
    }

    @Override
    boolean precondition(final Skill castSkill, final Creature performer, final Item target) {
        if(performer.getDeity() != null) {
            final Deity deity = performer.getDeity();
            final Deity templateDeity = Deities.getDeity(deity.getTemplateDeity());
            if(templateDeity.getFavor() < 100000 && !Servers.isThisATestServer()) {
                performer.getCommunicator().sendNormalServerMessage(deity.getName() + " can not grant that power right now.", (byte) 3);
                return false;
            }
            if(target.getBless() == deity && target.isDomainItem()) return true;
            performer.getCommunicator().sendNormalServerMessage("You need to cast this spell at an altar of " + deity.getName() + ".", (byte) 3);
        }
        return false;
    }

    @Override
    void doEffect(final Skill castSkill, final double power, final Creature performer, final Item target) {
        final Deity deity = performer.getDeity();
        final Deity templateDeity = Deities.getDeity(deity.getTemplateDeity());
        performer.getCommunicator().sendNormalServerMessage(performer.getDeity().getName() + " graces all followers extended awareness and a small gift!", (byte) 2);
        Server.getInstance().broadCastSafe("As the Ritual of the Moons is completed, followers of " + deity.getName() + " may now receive a lunar blessing!");
        HistoryManager.addHistory(performer.getName(), "casts " + this.name + ". Followers of " + deity.getName() + " receive a lunar blessing!");
        templateDeity.setFavor(templateDeity.getFavor() - 100000);
        performer.achievement(635);
        for(final Creature c : performer.getLinks()) {
            c.achievement(635);
        }
        new RiteOfMoonsEvent(-10, performer.getWurmId(), this.getNumber(), deity.getNumber(), System.currentTimeMillis(), DAY_AS_MILLIS);
    }

    public static class RiteOfMoonsEvent extends RiteEvent {
        public RiteOfMoonsEvent(final int id, final long casterId, final int spellId, final int deityNum, final long castTime, final long duration) {
            super(id, casterId, spellId, deityNum, castTime, duration);
        }

        @Override
        public boolean claimRiteReward(final Player player) {
            if(!super.claimRiteReward(player)) return false;
            player.getCommunicator().sendSafeServerMessage("You experience an altered state of consciousness!", (byte) 2);
            this.awardBasicBonuses(player, player.getSkills().getSkillOrLearn(SkillList.BODY_STAMINA));
            try {
                int templId = moonMetals[Server.rand.nextInt(moonMetals.length)];
                float ql = Server.rand.nextFloat() * 100.0f;
                int weight = 50 + Server.rand.nextInt(150);
                Item lump = ItemFactory.createItem(templId, ql, MiscConstants.COMMON, null);
                lump.setWeight(weight, true);
                player.getInventory().insertItem(lump, true);
            } catch(FailedException | NoSuchTemplateException e) { }
            return true;
        }
    }
}
