package com.wurmonline.server.skills;

import com.wurmonline.server.NoSuchPlayerException;
import com.wurmonline.server.Players;
import com.wurmonline.server.WurmId;
import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.players.Player;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;
import org.gotti.wurmunlimited.modsupport.ModSupportDb;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ModSkills {

    private static final Logger logger = Logger.getLogger(ModSkills.class.getName());

    public static final int MOD_SKILL_DIVINATION = 20001;

    private static final SkillTemplate divination = new SkillTemplate(MOD_SKILL_DIVINATION, "Divination", 2000.0f,
                                                                      new int[]{ SkillList.GROUP_RELIGION }, 1209600000L, SkillList.TYPE_NORMAL);

    public static final SkillTemplate[] modSkills = {
            divination
    };

    public static void init() {
        try {
            Method addSkillTemplate = ReflectionUtil.getMethod(SkillSystem.class, "addSkillTemplate");
            addSkillTemplate.setAccessible(true);
            addSkillTemplate.invoke(null, divination);
        } catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused")
    private static void setSkillDifficulty(int id, float difficulty) {
        SkillTemplate st = SkillSystem.templates.get(id);
        if(st != null) {
            try {
                ReflectionUtil.setPrivateField(st, ReflectionUtil.getField(st.getClass(), "difficulty"), difficulty);
                logger.info("Modified skill " + st.getName() + "[" + id + "], and set difficulty to " + difficulty + ".");
            } catch(NoSuchFieldException | IllegalAccessException e) {
                logger.log(Level.SEVERE, "Edit template private field error, for template ID " + id + " (skill).", e);
            }
        }
    }

    @SuppressWarnings("unused")
    public static void loadSkills(Creature creature, Skills skills) {
        final Player player = (Player) creature;
        final Communicator comm = player.getCommunicator();
        Skill skill;
        try {
            try(Connection con = ModSupportDb.getModSupportDb();
                PreparedStatement ps = con.prepareStatement("SELECT ID,NUMBER,VALUE,MINVALUE,LASTUSED FROM SKILLS WHERE OWNER=?")) {
                ps.setLong(1, player.getWurmId());
                try(ResultSet rs = ps.executeQuery()) {
                    while(rs.next()) {
                        long id = rs.getLong(1);
                        int number = rs.getInt(2);
                        double knowledge = rs.getDouble(3);
                        double minimum = rs.getDouble(4);
                        long lastUsed = rs.getLong(5);
                        skill = skills.skills.get(number);
                        if(skill == null) {
                            skill = new ModSkill(id, skills, number, knowledge, minimum, lastUsed);
                            skill.id = id;
                            skills.skills.put(number, skill);
                        }
                    }
                }
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save skill data: " + e.getMessage(), e);
        }
        skill = skills.skills.get(divination.getNumber());
        if(skill == null) {
            skill = new ModSkill(divination.getNumber(), 1.0, skills);
            skill.touch();
            skills.skills.put(skill.number, skill);
        }
    }

    @SuppressWarnings("unused")
    public static Skill learn(final Skills skills, final int number, final float startValue, final boolean sendAdd) {
        final Skill skill = new ModSkill(number, startValue, skills);

        final int[] dependencies;
        final int[] needed = dependencies = skill.getDependencies();
        for(final int aNeeded : dependencies)
            if(!skills.skills.containsKey(aNeeded))
                skills.learn(aNeeded, 1.0f);

        if(skills.id != -10L && WurmId.getType(skills.id) == 0) {
            int parentSkillId = 0;
            if(needed.length > 0) parentSkillId = needed[0];
            try {
                if(parentSkillId != 0) {
                    final int parentType = SkillSystem.getTypeFor(parentSkillId);
                    if(parentType == 0) parentSkillId = Integer.MAX_VALUE;
                } else {
                    parentSkillId = skill.getType() == 1? SkillList.CHARACTERISTICS : Integer.MAX_VALUE;
                }

                for(final Affinity aff : Affinities.getAffinities(skills.id))
                    if(aff.skillNumber == number) skill.affinity = aff.number;

                final Player player = Players.getInstance().getPlayer(skills.id);
                final Communicator comm = player.getCommunicator();
                if(sendAdd)
                    comm.sendAddSkill(number, parentSkillId, skill.getName(), startValue, startValue, skill.affinity);
                else comm.sendUpdateSkill(number, startValue, skill.affinity);
            } catch(NoSuchPlayerException e) {
                logger.log(Level.WARNING, "skillNumber=" + number + ", startValue=" + startValue + " : " + e.getMessage(), e);
            }
        }
        skill.touch();
        try {
            skill.save();
        } catch(IOException e) { }
        skills.skills.put(number, skill);
        return skill;
    }

    private static ModSkills instance = null;

    public static ModSkills getInstance() {
        if(instance == null) instance = new ModSkills();
        return instance;
    }

    private final Set<ModSkill> saveSkills;

    private ModSkills() {
        saveSkills = new HashSet<>();
    }

    public void saveSkill(ModSkill skill) {
        saveSkills.add(skill);
    }

    @SuppressWarnings("unused")
    public void saveAllSkills() {
        if(saveSkills.isEmpty()) return;
        try(Connection con = ModSupportDb.getModSupportDb()) {
            while(!saveSkills(con)) ;
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save skill data: " + e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused")
    public boolean saveSkills() {
        if(saveSkills.isEmpty()) return true;
        try(Connection con = ModSupportDb.getModSupportDb()) {
            return saveSkills(con);
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save skill data: " + e.getMessage(), e);
        }
        return true;
    }

    public boolean saveSkills(Connection con) {
        if(saveSkills.isEmpty()) return true;
        boolean done = true;
        try(PreparedStatement ps1 = con.prepareStatement("INSERT INTO SKILLS (ID,OWNER,NUMBER,VALUE,MINVALUE,LASTUSED) VALUES (NULL,?,?,?,?,?)");
            PreparedStatement ps2 = con.prepareStatement("UPDATE SKILLS SET NUMBER=?,VALUE=?,MINVALUE=?,LASTUSED=? WHERE ID=?")) {
            List<ModSkill> saved = new ArrayList<>(saveSkills.size());
            int i = 0;
            for(ModSkill skill : saveSkills) {
                if(skill.getId() == -10L) {
                    ps1.setLong(1, skill.parent.getId());
                    ps1.setInt(2, skill.number);
                    ps1.setDouble(3, skill.knowledge);
                    ps1.setDouble(4, skill.minimum);
                    ps1.setLong(5, skill.lastUsed);
                    int rows = ps1.executeUpdate();
                    if(rows == 1) {
                        try(ResultSet rs = ps1.getGeneratedKeys()) {
                            if(rs.next()) {
                                skill.id = rs.getInt(1);
                                saved.add(skill);
                            }
                        } catch(SQLException e) {
                            logger.log(Level.SEVERE, "Failed to save skill data: " + e.getMessage(), e);
                        }
                    }
                } else if(i < 20) {
                    ps2.setInt(1, skill.number);
                    ps2.setDouble(2, skill.knowledge);
                    ps2.setDouble(3, skill.minimum);
                    ps2.setLong(4, skill.lastUsed);
                    ps2.setLong(5, skill.id);
                    ps2.addBatch();
                    saved.add(skill);
                    ++i;
                } else {
                    break;
                }
            }
            if(!saved.isEmpty()) {
                if(i > 0) ps2.executeBatch();
                logger.info("Saved data for " + saved.size() + " skills.");
                saveSkills.removeAll(saved);
                done = saveSkills.isEmpty();
            }
        } catch(SQLException e) {
            logger.log(Level.SEVERE, "Failed to save skill data: " + e.getMessage(), e);
        }
        return done;
    }
}
