package com.wurmonline.server.skills;

import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;

@SuppressWarnings("unused")
public class Divination {

    public static void locateSpring(final Creature performer, final Item pendulum, final Skill primSkill) {
        Skill divination = performer.getSkills().getSkillOrLearn(ModSkills.MOD_SKILL_DIVINATION);
        divination.skillCheck(1.0, pendulum, 0.0, false, 10.0f);
    }

    public static void useLocateItem(final Creature performer, final Item pendulum, final Skill primSkill) {
        Skill divination = performer.getSkills().getSkillOrLearn(ModSkills.MOD_SKILL_DIVINATION);
        divination.skillCheck(1.0, pendulum, 0.0, false, 10.0f);
    }
}
