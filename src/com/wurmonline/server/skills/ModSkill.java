package com.wurmonline.server.skills;

import java.io.IOException;
import java.util.logging.Logger;

public class ModSkill extends Skill {

    private static final Logger logger = Logger.getLogger(ModSkill.class.getName());

    ModSkill(int number, double startValue, Skills parent) {
        super(number, startValue, parent);
        this.id = -10L;
    }

    @SuppressWarnings("unused")
    ModSkill(long id, Skills parent) throws IOException {
        super(id, parent);
        this.id = -10L;
    }

    ModSkill(long id, Skills parent, int number, double knowledge, double minimum, long lastused) {
        super(id, parent, number, knowledge, minimum, lastused);
        this.id = -10L;
    }

    @SuppressWarnings("unused")
    ModSkill(long id, int number, double knowledge, double minimum, long lastused) {
        super(id, number, knowledge, minimum, lastused);
        this.id = -10L;
    }

    @Override
    void save() {
        ModSkills.getInstance().saveSkill(this);
    }

    @Override
    void saveValue(boolean player) {
        ModSkills.getInstance().saveSkill(this);
    }

    @Override
    public void setJoat(boolean joat) {
    }

    @Override
    public void setNumber(int number) {
        this.number = number;
        ModSkills.getInstance().saveSkill(this);
    }

    @Override
    void load() {
    }
}
