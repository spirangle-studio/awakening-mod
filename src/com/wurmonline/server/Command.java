package com.wurmonline.server;

import com.wurmonline.server.creatures.Communicator;
import com.wurmonline.server.players.Player;
import org.gotti.wurmunlimited.modloader.interfaces.MessagePolicy;

import javax.annotation.Nullable;
import java.util.function.BiFunction;

public class Command {

    public static final int HELP_POWER_1 = 0x00000001;
    public static final int HELP_POWER_2 = 0x00000002;
    public static final int HELP_POWER_3 = 0x00000004;
    public static final int HELP_POWER_4 = 0x00000008;
    public static final int HELP_POWER_5 = 0x00000010;
    public static final int HELP_IS_CA = 0x00000100;
    public static final int HELP_MAY_MUTE = 0x00000200;
    public static final int HELP_MAY_APPOINT_CA = 0x00000400;
    public static final int HELP_MAY_HEAR_DEV_TALK = 0x00000800;
    public static final int HELP_CAN_SIGN_IN = 0x00001000;
    public static final int HELP_IS_KING = 0x00002000;
    public static final int HELP_IS_CHAMPION = 0x00004000;
    public static final int HELP_TEST_SERVER = 0x00010000;
    public static final int HELP_NOT_TEST_SERVER = 0x00020000;
    public static final int HELP_POWER_IS_1 = 0x00100000;
    public static final int HELP_POWER_IS_2 = 0x00200000;
    public static final int HELP_POWER_IS_3 = 0x00400000;
    public static final int HELP_POWER_IS_4 = 0x00800000;
    public static final int HELP_GM = 0x00F0001F;
    public static final int HELP_CUSTOM = 0x01000000;

    private final String command;
    private final String parameters;
    private final String description;
    private final String[] extraLines;
    private final int stats;
    private final BiFunction<Communicator, String, MessagePolicy> action;

    public Command(String command, String description) {
        this(command, null, description, null, 0, null);
    }

    public Command(String command, String description, int stats) {
        this(command, null, description, null, stats, null);
    }

    public Command(String command, String parameters, String description) {
        this(command, parameters, description, null, 0, null);
    }

    public Command(String command, String parameters, String description, int stats) {
        this(command, parameters, description, null, stats, null);
    }

    public Command(String command, String description, int stats, BiFunction<Communicator, String, MessagePolicy> action) {
        this(command, null, description, null, stats, action);
    }

    public Command(String command, String parameters, String description, int stats, BiFunction<Communicator, String, MessagePolicy> action) {
        this(command, parameters, description, null, stats, action);
    }

    public Command(String command, String parameters, String description, String[] extraLines, int stats) {
        this(command, parameters, description, extraLines, stats, null);
    }

    public Command(ServerTweaksHandler.Tweak tweak) {
        this(tweak.command, tweak.parameterString, tweak.helpDescription, null, 0, null);
    }

    public Command(String command, @Nullable String parameters, String description, @Nullable String[] extraLines, int stats, @Nullable BiFunction<Communicator, String, MessagePolicy> action) {
        this.command = command;
        this.parameters = parameters;
        this.description = description;
        this.extraLines = extraLines;
        this.stats = stats;
        this.action = action;
    }

    public boolean isGMCommand() {
        return (stats & HELP_GM) != 0;
    }

    public boolean isCustom() {
        return (stats & HELP_CUSTOM) != 0;
    }

    public boolean canSeeCommand(Player player) {
        if(stats == 0) return true;
        boolean pass = false;
        if(!isGMCommand()) pass = true;
        else {
            if((stats & HELP_POWER_1) != 0) {
                if(player.getPower() >= MiscConstants.POWER_HERO) pass = true;
            } else if((stats & HELP_POWER_2) != 0) {
                if(player.getPower() >= MiscConstants.POWER_DEMIGOD) pass = true;
            } else if((stats & HELP_POWER_3) != 0) {
                if(player.getPower() >= MiscConstants.POWER_HIGH_GOD) pass = true;
            } else if((stats & HELP_POWER_4) != 0) {
                if(player.getPower() >= MiscConstants.POWER_ARCHANGEL) pass = true;
            } else if((stats & HELP_POWER_5) != 0) {
                if(player.getPower() >= MiscConstants.POWER_IMPLEMENTOR) pass = true;
            } else if((stats & HELP_POWER_IS_1) != 0) {
                if(player.getPower() == MiscConstants.POWER_HERO) pass = true;
            } else if((stats & HELP_POWER_IS_2) != 0) {
                if(player.getPower() == MiscConstants.POWER_DEMIGOD) pass = true;
            } else if((stats & HELP_POWER_IS_3) != 0) {
                if(player.getPower() == MiscConstants.POWER_HIGH_GOD) pass = true;
            } else if((stats & HELP_POWER_IS_4) != 0) {
                if(player.getPower() == MiscConstants.POWER_IMPLEMENTOR) pass = true;
            }
        }
        if((stats & HELP_IS_CA) != 0 && player.isPlayerAssistant()) pass = true;
        if((stats & HELP_MAY_MUTE) != 0 && player.mayMute()) pass = true;
        if(!pass) return false;
        if((stats & HELP_CAN_SIGN_IN) != 0 && !player.canSignIn()) return false;
        if((stats & HELP_MAY_APPOINT_CA) != 0 && !player.mayAppointPlayerAssistant()) return false;
        if((stats & HELP_MAY_HEAR_DEV_TALK) != 0 && !player.mayHearDevTalk()) return false;
        if((stats & HELP_IS_KING) != 0 && !player.isKing()) return false;
        if((stats & HELP_IS_CHAMPION) != 0 && !player.isChampion()) return false;
        if((stats & HELP_TEST_SERVER) != 0 && !Servers.isThisATestServer()) return false;
        return (stats & HELP_NOT_TEST_SERVER) == 0 || !Servers.isThisATestServer();
    }

    public void sendHelpMessage(Communicator com) {
        StringBuilder sb = new StringBuilder();
        if(parameters == null) sb.append(command);
        else {
            if(parameters.charAt(0) == '/' || parameters.charAt(0) == '#') sb.append(parameters);
            else sb.append(command).append(" ").append(parameters);
        }
        if(description != null) sb.append(" - ").append(description);
        com.sendHelpMessage(sb.toString());
        if(extraLines != null) {
            if(extraLines.length == 1) com.sendHelpMessage("              - " + extraLines[0]);
            else {
                for(int i = 0; i < extraLines.length; ++i) {
                    if(extraLines[i] == null) continue;
                    if(i == 0) com.sendHelpMessage("           - " + extraLines[i]);
                    else com.sendHelpMessage("             " + extraLines[i]);
                }
            }
        }
    }

    public String getCommand() {
        return command;
    }

    public String getParameters() {
        return parameters;
    }

    public int compareTo(Command command) {
        int cmp = this.command.compareTo(command.command);
        if(cmp != 0) return cmp;
        return parameters == null? -1 : (command.parameters == null? 1 : parameters.compareTo(command.parameters));
    }

    public MessagePolicy perform(Communicator communicator, String message) {
        if(action != null) return action.apply(communicator, message);
        return MessagePolicy.PASS;
    }
}
