package com.wurmonline.server.structures;

import com.wurmonline.server.DbConnector;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Permissions;
import com.wurmonline.server.players.Permissions.IAllow;
import com.wurmonline.server.spells.HeartStone;
import com.wurmonline.server.utils.DbUtilities;
import com.wurmonline.shared.constants.BridgeConstants;
import com.wurmonline.shared.constants.StructureConstants.FloorMaterial;
import com.wurmonline.shared.constants.StructureConstants.FloorType;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wurmonline.server.players.Permissions.Allow.*;

public class StructureService {

    private static final Logger logger = Logger.getLogger(StructureService.class.getName());

    @SuppressWarnings("unused")
    public static final void loadAllFloors() throws IOException {
        logger.log(Level.INFO, "Loading all floors.");
        long s = System.nanoTime();
        Map<Long, Set<Floor>> floors = null;
        Connection dbcon = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Field field = Floor.class.getDeclaredField("floors");
            field.setAccessible(true);
            floors = (Map<Long, Set<Floor>>) field.get(null);

            dbcon = DbConnector.getZonesDbCon();
            ps = dbcon.prepareStatement("SELECT * FROM FLOORS");
            rs = ps.executeQuery();
            while(rs.next()) {
                FloorType floorType = FloorType.fromByte(rs.getByte("TYPE"));
                FloorMaterial floorMaterial = FloorMaterial.fromByte(rs.getByte("MATERIAL"));
                long sid = rs.getLong("STRUCTURE");
                Set<Floor> flset = floors.get(sid);
                if(flset == null) {
                    flset = new HashSet<>();
                    floors.put(sid, flset);
                }
                Floor floor = new DbFloor(rs.getInt("ID"), floorType, rs.getInt("TILEX"), rs.getInt("TILEY"), rs.getByte("STATE"), rs.getInt("HEIGHTOFFSET"), rs.getFloat("CURRENTQL"), sid, floorMaterial, rs.getInt("LAYER"), rs.getFloat("ORIGINALQL"), rs.getFloat("DAMAGE"), rs.getLong("LASTMAINTAINED"), rs.getByte("DIR"));
                floor.permissions.setPermissionBits(rs.getInt("SETTINGS"));
                flset.add(floor);
            }
        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Failed to access floors map! " + e.getMessage(), e);
        } catch(SQLException e) {
            logger.log(Level.WARNING, "Failed to load walls! " + e.getMessage(), e);
            throw new IOException(e);
        } finally {
            DbUtilities.closeDatabaseObjects(ps, rs);
            DbConnector.returnConnection(dbcon);
            long e = System.nanoTime();
            int n = floors != null? floors.size() : 0;
            logger.log(Level.INFO, "Loaded " + n + " floors. That took " + (e - s) / 1000000.0f + " ms.");
        }
    }

    @SuppressWarnings("unused")
    public static final void loadAllBridgeParts() throws IOException {
        logger.log(Level.INFO, "Loading all bridge parts.");
        long s = System.nanoTime();
        Set<DbBridgePart> bridgeParts = null;
        Connection dbcon = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Field field = BridgePart.class.getDeclaredField("bridgeParts");
            field.setAccessible(true);
            bridgeParts = (Set<DbBridgePart>) field.get(null);

            dbcon = DbConnector.getZonesDbCon();
            ps = dbcon.prepareStatement("SELECT * FROM BRIDGEPARTS");
            rs = ps.executeQuery();
            while(rs.next()) {
                final int id = rs.getInt("ID");
                final BridgeConstants.BridgeType floorType = BridgeConstants.BridgeType.fromByte(rs.getByte("TYPE"));
                final BridgeConstants.BridgeMaterial floorMaterial = BridgeConstants.BridgeMaterial.fromByte(rs.getByte("MATERIAL"));
                final byte state = rs.getByte("STATE");
                final int stageCount = rs.getInt("STAGECOUNT");
                final int x = rs.getInt("TILEX");
                final int y = rs.getInt("TILEY");
                final long structureId = rs.getLong("STRUCTURE");
                final int h = rs.getInt("HEIGHTOFFSET");
                final float currentQL = rs.getFloat("CURRENTQL");
                final float origQL = rs.getFloat("ORIGINALQL");
                final float dam = rs.getFloat("DAMAGE");
                final byte dir = rs.getByte("DIR");
                final byte slope = rs.getByte("SLOPE");
                final long last = rs.getLong("LASTMAINTAINED");
                final int northExit = rs.getInt("NORTHEXIT");
                final int eastExit = rs.getInt("EASTEXIT");
                final int southExit = rs.getInt("SOUTHEXIT");
                final int westExit = rs.getInt("WESTEXIT");
                final byte roadType = rs.getByte("ROADTYPE");
                final int layer = rs.getInt("LAYER");
                final int settings = rs.getInt("SETTINGS");
                DbBridgePart bridgePart = new DbBridgePart(id, floorType, x, y, state, h, currentQL, structureId, floorMaterial, origQL, dam, stageCount, last, dir, slope, northExit, eastExit, southExit, westExit, roadType, layer);
                bridgePart.permissions.setPermissionBits(rs.getInt("SETTINGS"));
                bridgeParts.add(bridgePart);
            }
        } catch(NoSuchFieldException | IllegalAccessException e) {
            logger.log(Level.SEVERE, "Failed to access bridge parts set! " + e.getMessage(), e);
        } catch(SQLException sqx) {
            logger.log(Level.WARNING, "Failed to load bridge parts!" + sqx.getMessage(), sqx);
            throw new IOException(sqx);
        } finally {
            DbUtilities.closeDatabaseObjects(ps, rs);
            DbConnector.returnConnection(dbcon);
            long e = System.nanoTime();
            int n = bridgeParts != null? bridgeParts.size() : 0;
            logger.log(Level.INFO, "Loaded " + n + " bridge parts. That took " + (e - s) / 1000000.0f + " ms.");
        }
    }

    public static boolean hasHeartStone(Item item) {
        return hasHeartStone(item, item.getSettings());
    }

    public static boolean hasHeartStone(Fence fence) {
        if(!fence.isFinished()) return false;
        return hasHeartStone(fence, fence.permissions);
    }

    public static boolean hasHeartStone(Wall wall) {
        if(!wall.isFinished()) return false;
        return hasHeartStone(wall, wall.permissions);
    }

    public static boolean hasHeartStone(Floor floor) {
        if(!floor.isFinished()) return false;
        return hasHeartStone(floor, floor.permissions);
    }

    public static boolean hasHeartStone(BridgePart bridgePart) {
        if(!bridgePart.isFinished()) return false;
        return hasHeartStone(bridgePart, bridgePart.permissions);
    }

    public static boolean hasHeartStone(IAllow ia, Permissions permissions) {
        int p = permissions.getPermissions();
        int n = getHeartStonePermissions(ia);
        return (n & p) == n;
    }

    public static void setHeartStonePermissions(Item item) {
        setHeartStonePermissions(item, item.getSettings());
    }

    public static void setHeartStonePermissions(Fence fence) {
        if(!fence.isFinished()) return;
        setHeartStonePermissions(fence, fence.permissions);
    }

    public static void setHeartStonePermissions(Wall wall) {
        if(!wall.isFinished()) return;
        setHeartStonePermissions(wall, wall.permissions);
    }

    public static void setHeartStonePermissions(Floor floor) {
        if(!floor.isFinished()) return;
        setHeartStonePermissions(floor, floor.permissions);
    }

    public static void setHeartStonePermissions(BridgePart bridgePart) {
        if(!bridgePart.isFinished()) return;
        setHeartStonePermissions(bridgePart, bridgePart.permissions);
    }

    public static void setHeartStonePermissions(IAllow ia, Permissions permissions) {
        int p = permissions.getPermissions();
        int n = getHeartStonePermissions(ia);
        p |= n;
        permissions.setPermissionBits(p);
        ia.savePermissions();
    }

    public static void removeHeartStonePermissions(Item item) {
        removeHeartStonePermissions(item, item.getSettings());
    }

    public static void removeHeartStonePermissions(Fence fence) {
        if(!fence.isFinished()) return;
        removeHeartStonePermissions(fence, fence.permissions);
    }

    public static void removeHeartStonePermissions(Wall wall) {
        if(!wall.isFinished()) return;
        removeHeartStonePermissions(wall, wall.permissions);
    }

    public static void removeHeartStonePermissions(Floor floor) {
        if(!floor.isFinished()) return;
        removeHeartStonePermissions(floor, floor.permissions);
    }

    public static void removeHeartStonePermissions(BridgePart bridgePart) {
        if(!bridgePart.isFinished()) return;
        removeHeartStonePermissions(bridgePart, bridgePart.permissions);
    }

    public static void removeHeartStonePermissions(IAllow ia, Permissions permissions) {
        int p = permissions.getPermissions();
        int n = getHeartStonePermissions(ia);
        p &= ~n;
        permissions.setPermissionBits(p);
        ia.savePermissions();
    }

    public static int getHeartStonePermissions(IAllow ia) {
        int p = 0;
        if(ia.canDisableRuneing()) {
            p |= NOT_RUNEABLE.getValue();
        }
        if(ia.canDisableEatAndDrink()) {
            p |= NO_EAT_OR_DRINK.getValue();
        }
        if(ia.canDisableDrag()) {
            p |= NO_DRAG.getValue();
        }
        if(ia.canDisableImprove()) {
            p |= NO_IMPROVE.getValue();
        }
        if(ia.canDisableRepair()) {
            p |= NO_REPAIR.getValue();
        }
        if(ia.canDisableDecay()) {
            p |= DECAY_DISABLED.getValue();
        }
        if(ia.canDisableTake()) {
            p |= NO_TAKE.getValue();
        }
        if(ia.canDisableSpellTarget()) {
            p |= NO_SPELLS.getValue();
        }
        if(ia.canDisableDestroy()) {
            p |= NO_BASH.getValue();
        }
        if(ia.canDisableLocking()) {
            p |= NOT_LOCKABLE.getValue();
        }
        if(ia.canDisableLockpicking()) {
            p |= NOT_LOCKPICKABLE.getValue();
        }
        if(ia.canDisableMoveable()) {
            p |= NOT_MOVEABLE.getValue();
        }
        if(ia.canDisableTurning()) {
            p |= NOT_TURNABLE.getValue();
        }
        return p;
    }

    @SuppressWarnings("unused")
    public static void examineItem(Creature performer, Item item) {
        if(HeartStone.isProtectedLocation(item.getTileX(), item.getTileY(), true)) return;
        examineHeartStoneEnchant(performer, item, item.getSettings());
    }

    @SuppressWarnings("unused")
    public static void examineFence(Creature performer, Fence fence) {
        if(!fence.isFinished() || HeartStone.isProtectedLocation(fence.getTileX(), fence.getTileY(), true)) return;
        examineHeartStoneEnchant(performer, fence, fence.permissions);
    }

    @SuppressWarnings("unused")
    public static void examineWall(Creature performer, Wall wall) {
        if(!wall.isFinished() || HeartStone.isProtectedLocation(wall.getTileX(), wall.getTileY(), true)) return;
        examineHeartStoneEnchant(performer, wall, wall.permissions);
    }

    @SuppressWarnings("unused")
    public static void examineFloor(Creature performer, Floor floor) {
        if(!floor.isFinished() || HeartStone.isProtectedLocation(floor.getTileX(), floor.getTileY(), true)) return;
        examineHeartStoneEnchant(performer, floor, floor.permissions);
    }

    @SuppressWarnings("unused")
    public static void examineBridgePart(Creature performer, BridgePart bridgePart) {
        if(!bridgePart.isFinished() || HeartStone.isProtectedLocation(bridgePart.getTileX(), bridgePart.getTileY(), true))
            return;
        examineHeartStoneEnchant(performer, bridgePart, bridgePart.permissions);
    }

    public static void examineHeartStoneEnchant(Creature performer, IAllow ia, Permissions permissions) {
        if(hasHeartStone(ia, permissions)) {
            performer.getCommunicator().sendNormalServerMessage("You recognize an aura of timeless quality.");
        }
    }
}
